﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Data.Odbc
Imports System.Configuration
Imports System.Globalization
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data

Module General

#Region "Variables de uso global"

    REM Credenciales SQL
    Public SQLBaseDatos As String = String.Empty
    Public SQLServidor As String = String.Empty
    Public SQLUsuario As String = String.Empty
    Public SQLPassword As String = String.Empty

    REM Credenciales SAP
    Public SAPUsuario As String = String.Empty
    Public SAPPassword As String = String.Empty

    REM Manejo de errores
    Public ErrorMsg As String = String.Empty
    Public errorNbr As Integer = 0

    Public sQuery As String = ""

    Public sSepDecimal As String = ""
    Public sSepDecimalB1 As String = ""
    Public sSepDecOpue As String = ""

    Public gVersion As String = "1.0.00"
    Public gFechaVersion As String = "07-Marzo-2014"
    REM Variable para concatenación de consultas
    Public mySQL As String = String.Empty
    Public DT As DataTable

    REM Control del consumo de recursos
    Public controlMemoria As New MemoryControl

    REM Otros...
    Public idCapturador As String
    Public versionSAP As Integer = 0

    Public version As Integer = 0
    Public Motor As String = "SQL"
    Public BaseHANA As String = ""

    Public GenErr As String

    REM Librería SAP BO
    Public SBO As New clsSAP

    Public BICLASS As Object

    Public xClassDB As RClass = New RClass()

    Public notLostFocusCFl As Boolean
    Public PublicMatrixUID As String
    Public PublicColumnUID As String
    Public PublicRow As Integer

    'Public oExxCulture As Exxis.AddOnCulture.AddOnCulture

#End Region

#Region "Variables locales"
    Public CONEXION As Odbc.OdbcConnection
    Public CONEXINT As Odbc.OdbcConnection


    Public dsMod As DataSet = New DataSet("Modifica")
#End Region

#Region "Variables Globales"

    'Declaracion de controles de Formulario
    Public chk As SAPbouiCOM.CheckBox

    Public txtcardcod, txtcardnam, txtcat, txtperiodo, txtanio, txtdocent, txtactivo, txtcommen As SAPbouiCOM.EditText
    Public col0, colcat, colitemcod, colitemnam, colum, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, colstatus As SAPbouiCOM.Column

    Public cmbmes As SAPbouiCOM.ComboBox
    Public cmbslp As SAPbouiCOM.ComboBox
    Public oMatrix As SAPbouiCOM.Matrix
    Public Grid As SAPbouiCOM.Grid
    Public oForm As SAPbouiCOM.Form
    Public oColumns As SAPbouiCOM.Columns
    Public oColumn As SAPbouiCOM.Column
    Public Uds As SAPbouiCOM.UserDataSources
    Public oDataTable, oDataT As SAPbouiCOM.DataTable
    Public C1 As String = ""
    Public C2 As String = ""
    Public C3 As String = ""
    Public C4 As String = ""

    Public lblInicio As Boolean = True
    Public blCargarFCC As Boolean = False
    Public formulariocuentas As String

    Public cmbpm As SAPbouiCOM.ComboBox
    Public oMatrix2 As SAPbouiCOM.Matrix
    Public colcod, colven, colchk As SAPbouiCOM.Column
    Public txtslp As SAPbouiCOM.EditText

    'Variables Globales
    Public SlpCode As Integer ' Codigo de Vendedor
    Public SUser As String 'Super usuario Yes or No
    Public sISPManager As String ' si es ProductManager Yes or No
    Public sPM As String 'Codigo de Product Manager

    Public Sap As SAPbouiCOM.SboGuiApi
    Public AppSap As SAPbouiCOM.Application
    Public DiApp As SAPbobsCOM.Company
    Public oCompany As SAPbobsCOM.Company
    Public TClass As Principal
    Public TClassCon As Conexiones
    Public oCompSer As SAPbobsCOM.CompanyService
    Public oBridge As SAPbobsCOM.SBObob

    Public SepDecimal As String = String.Empty
    Public SepDecimalB1 As String = String.Empty

    Public BaseDato As String
    Public Servidor As String

    Public Const sUsuMSSQL As String = "sa"
    Public Const spasMSSQL As String = "B1Admin"  'cnp

    'Public sUsuHANA As String = "ADDONEXXISBO"
    'Public spasHANA As String = "ExxisB0livia" 'Password HANA

    Public sUsuHANA As String = "SYSTEM"
    Public spasHANA As String = "Grup0b1gB0" 'Password HANA

    'Public sUsuHANA As String = "SYSTEM"
    'Public spasHANA As String = "3XX1s19." 'Password HANA

    'valores validos, dst_MSSQL2008 = 6, dst_HANADB = 9
    Public sDbServerType As String = ""

    Public UsuSBO As String = ""
    Public CnnADO As SqlConnection

    Public ItmsGrpCod As Integer
    Public CardCode As String
    Public Fecha As String
    Public DocEntry As Integer
    Public UserSign As Integer
    Public FileName As String = ""

    Public SQLScritpFinal As String = ""

    Public oMatrixFiles As SAPbouiCOM.Matrix
    Public colN0, colFile, colExt, colCheck, colXml As SAPbouiCOM.Column
    Public lblMensaje As SAPbouiCOM.StaticText
    Public rss As SAPbobsCOM.Recordset

    Public strCodMoneda As String = ""
    Public linIndex As Integer = 0

    Public MonedaLocal As String = ""
    Public MonedaSis As String = ""
    Public monSC As String = ""

    Public HasER As Boolean = False
    Public VehcTC As Boolean = False
    Public EXXDEBUGMODE As String = ""
    Public ci As CultureInfo

#End Region

#Region "Prpiedades"

    Private Sub GetMonedas()
        Dim recSet As SAPbobsCOM.Recordset

        'se realiza el chekeo de monedas
        oBridge = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge)
        recSet = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        recSet = oBridge.GetLocalCurrency
        MonedaLocal = recSet.Fields.Item(0).Value
        recSet = oBridge.GetSystemCurrency
        MonedaSis = recSet.Fields.Item(0).Value

        System.Runtime.InteropServices.Marshal.ReleaseComObject(recSet)
    End Sub

    Private Function GetDatosCompania() As SAPbobsCOM.Recordset
        Dim SqlQuery As String
        Dim rss As SAPbobsCOM.Recordset

        Try
            SqlQuery = " Select top 1 Decsep, Sumdec , Pricedec , QtyDec, MainCurncy , SysCurrncy from OADM "
            rss = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rss.DoQuery(SqlQuery)
            Return rss

        Catch ex As Exception
            MessageBox.Show("Datos de compañía:" & ex.Message)
            Return rss
        End Try
    End Function

    Public Function Config_Decimales() As Boolean
        Try
            Dim rss As DataTable
            Dim año As String = CType(Today.Year, String)
            Dim prueba As Single = "1.0"
            If prueba = 1 Then
                sSepDecimal = "."
                sSepDecOpue = ","
            Else
                sSepDecimal = ","
                sSepDecOpue = "."
            End If
            Select Case sDbServerType
                Case "6"
                    sQuery = "Select top 1 Decsep, Sumdec , Pricedec , QtyDec from OADM"
                Case "9"
                    'RCG
                    sQuery = "select * FROM """ & SQLBaseDatos.ToUpper & """.""OADM"""
            End Select

            Dim conexi As Conexiones = New Conexiones
            Dim dtls As DataTable
            dtls = conexi.obtenerColeccion(sQuery, False)

            For Each row As DataRow In dtls.Rows
                sSepDecimalB1 = row("Decsep")

            Next

            If sSepDecimal = sSepDecimalB1 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try

    End Function


#End Region

#Region "Main"

    Public Sub Main()
        Dim strConnect As String
        Dim sErr As Long, msg As String


        Try
            Dim NumOfParams As Integer
            NumOfParams = Environment.GetCommandLineArgs.Length
            If NumOfParams = 2 Then
                Try
                    strConnect = Environment.GetCommandLineArgs.GetValue(1)
                Catch ex As Exception
                    MessageBox.Show("No se Pudo Rescatar Argumento de Sesión de B1", "de Addon", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End Try
            Else
               strConnect = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056"
                'strConnect = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056"
            End If

            Sap = New SAPbouiCOM.SboGuiApi
            Sap.Connect(strConnect)
            DiApp = New SAPbobsCOM.Company
            oCompany = New SAPbobsCOM.Company

            TClass = New Principal
            TClassCon = New Conexiones
            AppSap = TClass.oApp
            sErr = TClassCon.ConectaDI
            If sErr <> 0 Then
                Call DiApp.GetLastError(sErr, msg)
                AppSap.MessageBox(msg & vbCrLf & "El Addons se Desconectará.")
                Application.Exit()
            End If
            BaseDato = DiApp.CompanyDB
            Servidor = DiApp.Server
            UsuSBO = DiApp.UserName

            SQLServidor = Servidor
            SQLBaseDatos = BaseDato

            'oExxCulture = New Exxis.AddOnCulture.AddOnCulture

            Select Case DiApp.language
                Case SAPbobsCOM.BoSuppLangs.ln_Spanish_La
                    'oExxCulture.Culture = "Spanish (Latin America)"
                Case SAPbobsCOM.BoSuppLangs.ln_English
                    'oExxCulture.Culture = "English (United States)"
                Case SAPbobsCOM.BoSuppLangs.ln_Portuguese_Br
                    'oExxCulture.Culture = "Portuguese (Brazil)"
                Case Else
                    'oExxCulture.Culture = "English (United States)"
            End Select

            If LeeAppConfig() = False Then
                Throw New Exception("Error al leer archivo config")
            End If

            Select Case sDbServerType
                Case "6"
                    If TClassCon.ConectaCnnMSSQL = False Then
                        Throw New System.Exception("No fue posible conectar SqlClient a base de datos..." & BaseDato)
                    End If
                Case "9"
                    If TClassCon.ConectaCnnHANA = False Then
                        Throw New System.Exception("No fue posible conectar ODBC HANA a base de datos..." & BaseDato)
                    End If
            End Select

            'If Config_Decimales() = False Then
            '    AppSap.MessageBox("Separador de Decimales de B1 no es Igual a la Configuración Regional" & vbCrLf & " de su equipo. Deben estar sincronizados. Verifique ...")
            '    Application.Exit()
            '    Exit Sub
            'End If

            GetMonedas()


            'EventFilters
            TClass.SetFilters() 'modifi

            'Menu XML
            TClass.AddMenuXML(AppSap) 'modifi
            'TClass.AddMenuItems()
            Application.Run()

        Catch ex As Exception
            MessageBox.Show("Problemas al conectar con SAP, Favor verifique que la apliación utilizada es de x86 o x64. Error presentado : " & ex.Message & " - " & ex.ToString())
        End Try

    End Sub


    Public Function LeeAppConfig() As Boolean
        Dim lsRuta As String = ""
        Try

            lsRuta = Application.StartupPath & "\XFinanciamiento.exe.config"
            If File.Exists(lsRuta) = True Then
                sDbServerType = ConfigurationSettings.AppSettings("DbServerType")
                If sDbServerType.Trim = "" Then
                    Throw New System.Exception("Faltan Datos en archivo configuracion. Verifique ...")
                End If
            Else
                Throw New System.Exception("No existe archivo de configuracion. Verifique ...")
            End If
            Return True

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

#End Region

End Module

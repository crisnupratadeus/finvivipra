﻿

Namespace Utiles

    Public Class Util

        ''' <summary>
        ''' OBTIENE DESCRIPCION DE ENUMERADOR
        ''' </summary>
        Public Shared Function ObtenerError(ByVal codigo As String) As String

            Dim serror As String = CType(codigo, eErrorTest).ToString()

            Return serror.Replace("_", " ")

        End Function
        Public Shared Sub CargarComboDinamico(ByVal cmbobox As SAPbouiCOM.ComboBox, hasValores As DataTable)
            Try
                Dim count As Integer = 0
                Dim inicial As String = String.Empty

                For Each row As DataRow In hasValores.Rows
                    cmbobox.ValidValues.Add(row(0).ToString(), row(1).ToString())
                    If count = 0 Then
                        inicial = row(0).ToString()
                    End If
                    count = count + 1
                Next row

                If count > 0 Then
                    cmbobox.Select(inicial, SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub CargarComboDinamicoEx(ByVal cmbobox As SAPbouiCOM.ComboBox, hasValores As DataTable, Optional DefNew As Boolean = False)
            Try
                Dim count As Integer = 0
                Dim inicial As String = String.Empty

                cmbobox.ValidValues.Add("-999", "          ")
                For Each row As DataRow In hasValores.Rows
                    cmbobox.ValidValues.Add(row(0).ToString(), row(1).ToString())
                    If count = 0 Then
                        inicial = row(0).ToString()
                    End If
                    count = count + 1
                Next row
                If DefNew = True Then
                    cmbobox.ValidValues.Add("9999", "Definir nuevo ...")
                End If

                cmbobox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                If count > 0 Then
                    cmbobox.Select(inicial, SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub CargarComboDinamicoEx(ByVal cmbobox As SAPbouiCOM.Column, hasValores As DataTable, Optional DefNew As Boolean = False)
            Try
                Dim count As Integer = 0
                Dim inicial As String = String.Empty

                If DefNew = True Then
                    cmbobox.ValidValues.Add("-1", "          ")
                End If
                For Each row As DataRow In hasValores.Rows
                    cmbobox.ValidValues.Add(row(0).ToString(), row(1).ToString())
                    If count = 0 Then
                        inicial = row(0).ToString()
                    End If
                    count = count + 1
                Next row
                If DefNew = True Then
                    cmbobox.ValidValues.Add("9999", "Definir nuevo ...")
                End If

                cmbobox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Shared Sub LimpiarCombo(ByVal cmbobox As SAPbouiCOM.ComboBox)
            Try
                While cmbobox.ValidValues.Count > 0
                    cmbobox.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                End While
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub LimpiarCombo(ByVal cmbobox As SAPbouiCOM.ButtonCombo)
            Try
                While cmbobox.ValidValues.Count > 0
                    cmbobox.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                End While
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''' <summary>
        ''' PARCEA FECHA
        ''' </summary>
        Public Shared Function StringTodate(ByVal s As String) As Date

            Try
                Dim fecha As DateTime = DateTime.Parse(s)
                Return fecha
            Catch ex As Exception

                Return Date.Today
            End Try
        End Function

        ''' <summary>
        ''' OBTIENE UN CAMPO DE UNA GRILLA
        ''' </summary>
        Public Shared Function ObtenerCampoGrilla(ByVal ColId As String, ByVal mtx As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByVal oform As SAPbouiCOM.Form) As String
            Dim IdDoc As String = Nothing
            Try

                Dim uds As SAPbouiCOM.UserDataSources
                Dim mtxDoc As SAPbouiCOM.Matrix = oform.Items.Item("gridx").Specific
                uds = oform.DataSources.UserDataSources

                If (pVal.Row = 0) Then
                    Exit Try
                End If

                If (pVal.Row > mtxDoc.RowCount) Then
                    Exit Try
                End If

                Dim Row As Integer = 0
                'Obtenemos el ID
                If (pVal.Row > 0) Then
                    mtxDoc.GetLineData(pVal.Row)
                    Row = pVal.Row
                Else
                    mtxDoc.GetLineData(mtxDoc.RowCount)
                    mtxDoc.SelectRow(mtxDoc.RowCount, True, True)
                    Row = mtxDoc.RowCount
                End If

                Dim oEdit As SAPbouiCOM.EditText = mtxDoc.Columns.Item(ColId).Cells.Item(Row).Specific
                oEdit = mtxDoc.Columns.Item(ColId).Cells.Item(Row).Specific
                IdDoc = oEdit.Value

            Catch ex As Exception
                IdDoc = Nothing
            End Try

            Return IdDoc

        End Function

        ''' <summary>
        ''' SETEA CHOOSEFROMLIST
        ''' </summary>
        Public Shared Function f_GetDataTableFromCFL(ByRef oEvent As SAPbouiCOM.ItemEvent, ByVal oForm As SAPbouiCOM.Form) As SAPbouiCOM.DataTable

            Dim oChooseFromListEvent As SAPbouiCOM.ChooseFromListEvent
            Dim oChooseFromList As SAPbouiCOM.ChooseFromList
            Dim ItemUID As String

            oChooseFromListEvent = oEvent
            ItemUID = oChooseFromListEvent.ChooseFromListUID
            oChooseFromList = oForm.ChooseFromLists.Item(ItemUID)

            Return oChooseFromListEvent.SelectedObjects

        End Function

        ''' <summary>
        ''' CREA ITEM CHECKBOX EN UN FORMULARIO SAP CON ENLACE A UN ITEM X
        ''' </summary>
        Public Shared Sub CreaBotonEnFormularioSap(ByVal oFrm As SAPbouiCOM.Form, ByVal NombreNewItem As String, ByVal ItemsEnlace As String, ByVal ItemTop As Integer, ByVal ItemLeft As Integer, ByVal ItemWidth As Integer, ByVal ItemHeight As Integer, ByVal blnvisible As Boolean, ByVal Caption As String, Optional pane As Integer = 0)
            Try
                Dim itm As SAPbouiCOM.Item
                Dim itmC As SAPbouiCOM.Item
                Dim Btn As SAPbouiCOM.Button

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_BUTTON)
                itmC = oFrm.Items.Item(ItemsEnlace)

                'itm.LinkTo = ItemsEnlace
                itm.Left = itmC.Left + ItemLeft
                itm.Top = itmC.Top + ItemTop
                itm.Width = ItemWidth
                itm.Height = ItemHeight

                itm.FromPane = pane
                itm.ToPane = pane

                'itm.Visible = blnvisible
                Btn = itm.Specific
                Btn.Caption = Caption

            Catch ex As Exception
                Throw ex
            End Try
        End Sub



        ''' <summary>
        ''' CREA ITEM CHECKBOX EN UN FORMULARIO SAP CON ENLACE A UN ITEM X
        ''' </summary>
        Public Shared Sub CreaChkEnFormularioSap(ByVal oFrm As SAPbouiCOM.Form, ByVal NombreNewItem As String, ByVal ItemsEnlace As String, ByVal ItemTop As Integer, ByVal ItemLeft As Integer, ByVal ItemWidth As Integer, ByVal blnvisible As Boolean, ByVal Caption As String)
            Try
                Dim itm As SAPbouiCOM.Item
                Dim itmC As SAPbouiCOM.Item
                Dim chk As SAPbouiCOM.CheckBox

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
                itmC = oFrm.Items.Item(ItemsEnlace)

                itm.LinkTo = ItemsEnlace
                itm.Left = itmC.Left + ItemLeft
                itm.Top = itmC.Top + ItemTop
                itm.Width = ItemWidth

                itm.Visible = blnvisible
                chk = itm.Specific
                chk.Caption = Caption

                'chk.ValOff = "N"
                'chk.ValOn = "Y"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' <summary>
        ''' devuelve fecha formato universal
        ''' </summary>
        Public Shared Function Getfecha(ByVal fecha As String) As String
            Dim pais As String = System.Globalization.RegionInfo.CurrentRegion.DisplayName
            Dim aa As System.Globalization.DateTimeFormatInfo = System.Globalization.DateTimeFormatInfo.CurrentInfo
            Dim formatofecha As String
            Dim separador As String
            formatofecha = aa.ShortDatePattern
            separador = aa.DateSeparator
            Dim arreglo As Array
            Dim i As Integer
            Dim dia As String
            Dim mes As String
            Dim año As String
            Dim hora As String
            Dim min As String
            Dim fechasal As String
            Dim tipofecha As Date = CType(fecha, Date)
            año = Strings.Right("00" & CType(tipofecha.Year, String), 4)
            mes = Strings.Right("00" & CType(tipofecha.Month, String), 2)
            dia = Strings.Right("00" & CType(tipofecha.Day, String), 2)
            hora = Strings.Right("00" & CType(tipofecha.Hour, String), 2)
            min = Strings.Right("00" & CType(tipofecha.Minute, String), 2)
            If formatofecha = "dd-MM-yyyy" And formatofecha = "dd/MM/yyyy" And formatofecha <> "M/d/yyyy" Then
                MessageBox.Show(formatofecha & " : Formato de Fecha no válido" & vbCrLf & vbCrLf & "Formatos válidos : " & vbCrLf & vbCrLf & "   - dd-MM-yyyy" & vbCrLf & "   - dd/MM/yyyy" & vbCrLf & "   - M/d/yyyy", "Verifique Config. Regional", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Function
            End If
            fechasal = "{ts'" & año & "-" & mes & "-" & dia & " " & "00" & ":" & "00" & ":00'}"
            Return fechasal
        End Function
        Public Shared Function GetfechaYYMMDD(ByVal fecha As String) As String
            Dim pais As String = System.Globalization.RegionInfo.CurrentRegion.DisplayName
            Dim aa As System.Globalization.DateTimeFormatInfo = System.Globalization.DateTimeFormatInfo.CurrentInfo
            Dim formatofecha As String
            Dim separador As String
            formatofecha = aa.ShortDatePattern
            separador = aa.DateSeparator
            Dim arreglo As Array
            Dim i As Integer
            Dim dia As String
            Dim mes As String
            Dim año As String
            Dim hora As String
            Dim min As String
            Dim fechasal As String
            Dim tipofecha As Date = CType(fecha, Date)
            año = Strings.Right("00" & CType(tipofecha.Year, String), 4)
            mes = Strings.Right("00" & CType(tipofecha.Month, String), 2)
            dia = Strings.Right("00" & CType(tipofecha.Day, String), 2)
            hora = Strings.Right("00" & CType(tipofecha.Hour, String), 2)
            min = Strings.Right("00" & CType(tipofecha.Minute, String), 2)
            If formatofecha = "dd-MM-yyyy" And formatofecha = "dd/MM/yyyy" And formatofecha <> "M/d/yyyy" Then
                MessageBox.Show(formatofecha & " : Formato de Fecha no válido" & vbCrLf & vbCrLf & "Formatos válidos : " & vbCrLf & vbCrLf & "   - dd-MM-yyyy" & vbCrLf & "   - dd/MM/yyyy" & vbCrLf & "   - M/d/yyyy", "Verifique Config. Regional", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Function
            End If
            fechasal = año & mes & dia
            Return fechasal
        End Function
        Public Shared Function GetfechaYYMM(ByVal fecha As String) As String
            Dim pais As String = System.Globalization.RegionInfo.CurrentRegion.DisplayName
            Dim aa As System.Globalization.DateTimeFormatInfo = System.Globalization.DateTimeFormatInfo.CurrentInfo
            Dim formatofecha As String
            Dim separador As String
            formatofecha = aa.ShortDatePattern
            separador = aa.DateSeparator
            Dim arreglo As Array
            Dim i As Integer
            Dim dia As String
            Dim mes As String
            Dim año As String
            Dim hora As String
            Dim min As String
            Dim fechasal As String
            Dim tipofecha As Date = CType(fecha, Date)
            año = Strings.Right("00" & CType(tipofecha.Year, String), 4)
            mes = Strings.Right("00" & CType(tipofecha.Month, String), 2)
            dia = Strings.Right("00" & CType(tipofecha.Day, String), 2)
            hora = Strings.Right("00" & CType(tipofecha.Hour, String), 2)
            min = Strings.Right("00" & CType(tipofecha.Minute, String), 2)
            If formatofecha = "dd-MM-yyyy" And formatofecha = "dd/MM/yyyy" And formatofecha <> "M/d/yyyy" Then
                MessageBox.Show(formatofecha & " : Formato de Fecha no válido" & vbCrLf & vbCrLf & "Formatos válidos : " & vbCrLf & vbCrLf & "   - dd-MM-yyyy" & vbCrLf & "   - dd/MM/yyyy" & vbCrLf & "   - M/d/yyyy", "Verifique Config. Regional", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Function
            End If
            fechasal = año & mes
            Return fechasal
        End Function
        Public Shared Function GetfechafromStringDataSource(sdate As String) As Date
            sdate = Trim(sdate)
            Dim MyDateTime As Date
            If sdate <> "" Then
                MyDateTime = New Date
                MyDateTime = DateTime.ParseExact(sdate, "yyyyMMdd", Nothing)
            Else
                Dim odate As New Date
                MyDateTime = odate
            End If

            Return MyDateTime
        End Function
        Public Shared Function GetUser(ByVal oForm As SAPbouiCOM.Form, ByVal oApp As SAPbouiCOM.Application) As KeyValuePair(Of String, String)
            Dim userCode As String = String.Empty
            Dim userName As String = String.Empty
            Dim usuario As KeyValuePair(Of String, String)
            Try
                userCode = Trim(DiApp.UserSignature)
                userName = Trim(DiApp.UserName)
                usuario = New KeyValuePair(Of String, String)(userCode, userName)
                Return usuario
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                usuario = New KeyValuePair(Of String, String)("", "")
                Return (usuario)
            End Try
        End Function

        Public Shared Sub CreaLabelEnFormularioSap(ByVal oFrm As SAPbouiCOM.Form, ByVal NombreNewItem As String, ByVal ItemsEnlace As String, ByVal ItemTop As Integer, ByVal ItemLeft As Integer, ByVal ItemWidth As Integer, ByVal ItemHeight As Integer, ByVal blnvisible As Boolean, ByVal Caption As String, Optional pane As Integer = 0)
            Try
                Dim itm As SAPbouiCOM.Item
                Dim itmC As SAPbouiCOM.Item
                Dim StaticText As SAPbouiCOM.StaticText

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_STATIC)
                itmC = oFrm.Items.Item(ItemsEnlace)


                'itm.LinkTo = ItemsEnlace
                itm.Left = itmC.Left + ItemLeft
                itm.Top = itmC.Top + ItemTop
                itm.Width = ItemWidth
                itm.Height = ItemHeight

                itm.FromPane = pane
                itm.ToPane = pane

                ''    itm.Visible = False

                StaticText = itm.Specific
                StaticText.Caption = Caption

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CreaTextEnFormularioSap(ByVal oFrm As SAPbouiCOM.Form, ByVal NombreNewItem As String, ByVal ItemsEnlace As String, ByVal ItemTop As Integer, ByVal ItemLeft As Integer, ByVal ItemWidth As Integer, ByVal ItemHeight As Integer, ByVal blnvisible As Boolean, ByVal Texto As String, Optional pane As Integer = 0)
            Try
                'Util.CreaTextEnFormularioSap(oFrm, "TxOVNCu", "lblOVNCu", 0, 120, 21, 15, True, "", 7)

                Dim itm As SAPbouiCOM.Item
                Dim itmC As SAPbouiCOM.Item
                Dim StaticText As SAPbouiCOM.IStaticText

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_STATIC)
                itmC = oFrm.Items.Item(ItemsEnlace)


                'itm.LinkTo = ItemsEnlace
                itm.Left = itmC.Left + ItemLeft
                itm.Top = itmC.Top + ItemTop
                itm.Width = ItemWidth
                itm.Height = ItemHeight

                itm.FromPane = pane
                itm.ToPane = pane

                ''    itm.Visible = False

                StaticText = itm.Specific
                'StaticText.Item.
                'StaticText.Caption = Caption

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub CreaEdtTextEnFormularioSap(ByVal oFrm As SAPbouiCOM.Form, ByVal NombreNewItem As String, ByVal ItemsEnlace As String, ByVal ItemTop As Integer, ByVal ItemLeft As Integer, ByVal ItemWidth As Integer, ByVal ItemHeight As Integer, ByVal blnvisible As Boolean, ByVal Texto As String, Optional pane As Integer = 0)
            Try
                'Util.CreaTextEnFormularioSap(oFrm, "TxOVNCu", "lblOVNCu", 0, 120, 21, 15, True, "", 7)

                Dim itm As SAPbouiCOM.Item
                Dim itmC As SAPbouiCOM.Item
                Dim StaticText As SAPbouiCOM.EditText


                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                itmC = oFrm.Items.Item(ItemsEnlace)


                'itm.LinkTo = ItemsEnlace
                itm.Left = itmC.Left + ItemLeft
                itm.Top = itmC.Top + ItemTop
                itm.Width = ItemWidth
                itm.Height = ItemHeight

                itm.FromPane = pane
                itm.ToPane = pane

                ''    itm.Visible = False

                StaticText = itm.Specific
                'StaticText.Item.
                'StaticText.Caption = Caption

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace

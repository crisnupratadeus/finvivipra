﻿Imports System.Data.SqlClient

Namespace Utiles
    Public Class Funciones

        '#Region "Manejo de la Conexion a la Base de Datos"


        '    ' Según pruebas de concurrencia, dos posibles opciones de optimización:
        '    '
        '    ' 1. Verificar si la conexión trans esta abierta, esperar (Thread.Sleep(1000)) y volver a preguntar y establecer politica.
        '    ' 2. Instanciar nuevas conexiones trans, de todas formas estas deberían perdurar solo el bloque de cada transacction

        '    Public Function GetDBConexion() As SqlConnection
        '        If IsOpenDBConexion() Then
        '            Return ClsConecta.Cntrans
        '        Else
        '            If ClsConecta.Cntrans.ConnectionString Is Nothing Or ClsConecta.Cntrans.ConnectionString = "" Then
        '                ClsConecta.ConectaADO()
        '            Else

        '                If ClsConecta.CnADO.State = ConnectionState.Closed Then
        '                    ClsConecta.CnADO.Open()
        '                Else

        '                End If
        '                Return ClsConecta.CnADO
        '            End If
        '        End If
        '    End Function

        '    Public Function IsOpenDBConexion() As Boolean
        '        If ClsConecta.Cntrans.State = ConnectionState.Open Then
        '            Return True
        '        Else
        '            Return False
        '        End If
        '    End Function

        '    Public Function OpenDBConexion() As SqlConnection
        '        CloseDBConexion()
        '        If ClsConecta.Cntrans.ConnectionString Is Nothing Or ClsConecta.Cntrans.ConnectionString = "" Then
        '            ClsConecta.ConectaADO()
        '        End If
        '        ClsConecta.Cntrans.Open()


        '    End Function

        '    Public Function CloseDBConexion() As SqlConnection
        '        If IsOpenDBConexion() Then
        '            ClsConecta.Cntrans.Close()
        '        End If
        '    End Function
        '#End Region


        '    Public Function GetDocentryDeDocnum(ByVal Tabla As String, ByVal DocNum As Integer) As Integer
        '        Dim str As String
        '        Dim valor As Integer
        '        str = "select DocEntry from dbo.[" & Tabla & "]  WHERE DocNum = " & DocNum
        '        Select Case Tabla
        '            Case "OPOR"
        '                str += " and ObjType=22"

        '        End Select
        '        Dim cmd As New SqlCommand(str, Me.GetDBConexion)
        '        cmd.CommandTimeout = 0
        '        cmd.CommandText = str
        '        Try
        '            valor = cmd.ExecuteScalar
        '            If valor = Nothing Then
        '                valor = 0
        '            End If
        '        Catch ex As Exception
        '            valor = 0
        '        End Try
        '        If ClsConecta.CnADO.State = ConnectionState.Open Then
        '            ClsConecta.CnADO.Close()
        '        End If
        '        Return valor
        '    End Function

        '    Public Function GetMonedas(ByVal Condicion As String) As Data.DataTable
        '        Dim str As String
        '        Dim dtt As New DataTable
        '        If Condicion <> "" Then
        '            str = "select CurrCode,CurrName from dbo.OCRN with (nolock) where " & Condicion
        '        Else
        '            str = "select CurrCode,CurrName from dbo.OCRN with (nolock) "
        '        End If
        '        Try
        '            Dim DapTable As New SqlDataAdapter(str, Me.GetDBConexion)
        '            DapTable.SelectCommand.CommandTimeout = 0
        '            DapTable.Fill(dtt)
        '        Catch ex As Exception
        '            MessageBox.Show("No se puede rescatar Monedas de Tabla OCRN" & vbCrLf & ex.Message)
        '        End Try
        '        Return dtt
        '    End Function

        '    Public Function Get_OSLPName(ByVal UserID As String) As String
        '        Dim str As String
        '        Dim valor As String = ""
        '        str = "select SlpName FROM [dbo].[OSLP] (NOLOCK) WHERE U_User_ID= '" & UserID & "'"
        '        Dim cmd As New SqlCommand(str, Me.GetDBConexion)
        '        cmd.CommandTimeout = 0
        '        cmd.CommandText = str
        '        Try
        '            valor = cmd.ExecuteScalar
        '            If valor = Nothing Then
        '                valor = ""
        '            End If
        '        Catch ex As Exception
        '            valor = ""
        '        End Try
        '        If ClsConecta.CnADO.State = ConnectionState.Open Then
        '            ClsConecta.CnADO.Close()
        '        End If
        '        Return valor
        '    End Function

        '    Public Function GetQuerySqlXml(ByVal xmlStr As String, ByVal xmlFields() As String, ByVal xmlNode As String, ByVal Company As SAPbobsCOM.Company, Optional ByVal emptyField As String = "") As String
        '        Dim strSql As String = ""
        '        Try
        '            '"USE[" & Company.CompanyDB & "]" + vbCrLf +

        '            strSql += "DECLARE @myXml AS XML " & _
        '                      " SET @myXml = '" & xmlStr & "' " + vbCrLf +
        '                      "CREATE TABLE #TMP " + vbCrLf +
        '                      "(" + vbCrLf +
        '                          "name VARCHAR(100) " + vbCrLf +
        '                          ",ext VARCHAR(10) " + vbCrLf +
        '                          ",strxml VARCHAR(MAX)" + vbCrLf +
        '                          ",chk VARCHAR(10) " + vbCrLf +
        '                      ")" & _
        '                      "INSERT INTO #TMP " + vbCrLf +
        '                      " SELECT "
        '            For n As Integer = 0 To xmlFields.Length - 1
        '                strSql += " T.c.value('" & xmlFields(n).ToString & "[1]','nvarchar(max)') AS [" & xmlFields(n).ToString & "] "
        '                'respuesta = client.XmlFileList("xml", strXml)
        '                'strSql += ",'" & xmlStr & "'" & "  AS [strxml] "
        '                If (n <> xmlFields.Length - 1) Then
        '                    strSql += ","
        '                End If

        '            Next

        '            'agregamos el xml
        '            'strSql += ",'" & xmlStr & "'" & "  AS [strxml] "

        '            If (emptyField <> "") Then
        '                If (xmlFields.Length > 0) Then
        '                    strSql += ","
        '                End If
        '                strSql += " '' AS [" & emptyField.ToString & "]"
        '            End If

        '            strSql += " FROM @myXml.nodes('" & xmlNode & "') AS T(c)"

        '            strSql += "SELECT " + vbCrLf +
        '                           "T.name " + vbCrLf +
        '                           ",T.ext " + vbCrLf +
        '                           ",T.strxml " + vbCrLf +
        '                           ",T.chk " + vbCrLf +
        '                      "FROM #TMP T " & _
        '            "DROP TABLE #TMP "
        '            '"LEFT JOIN [@PROCXML] D ON D.Name = T.name COLLATE SQL_Latin1_General_CP1_CI_AS " + vbCrLf +
        '            '",ISNULL(D.U_EstadoProceso,'-1') AS estado " + vbCrLf +
        '            Return strSql

        '        Catch ex As Exception

        '            Return ""

        '        End Try

        '    End Function

    End Class
End Namespace


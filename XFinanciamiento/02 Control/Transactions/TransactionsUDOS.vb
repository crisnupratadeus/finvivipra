﻿
Imports XFinanciamiento.Control.Negocio
Imports SAPbobsCOM
Imports System.Transactions

Namespace Control.Transactions

    Public Class TransactionsUDOS
        Public Shared Sub CrearUDOS(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Dim progress As SAPbouiCOM.ProgressBar = Nothing
            Try
                progress = oApp.StatusBar.CreateProgressBar("Creando UDOs", 100, False)
                progress.Value = 10
                NegocioUDOS.CreaUDOLC()
                progress.Value = 20
                NegocioUDOS.CreaUDOFIPLA()
                progress.Value = 30
                NegocioUDOS.CreaUDOEtapasAut()
                progress.Value = 40
                NegocioUDOS.CreaUDOModelosAut()
                progress.Value = 50
                NegocioUDOS.CreaUDOCondicionesAut()
                progress.Value = 60
                NegocioUDOS.CreaUDOCuotas()
                progress.Value = 70
                NegocioUDOS.CreaUDORefinanciamiento()
                progress.Value = 80
                NegocioUDOS.CrearUDOIntereses()
                progress.Value = 100
                progress.Stop()
                System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                progress = Nothing
            Catch ex As Exception
                If Not progress Is Nothing Then
                    progress.Stop()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                    progress = Nothing
                End If
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub
    End Class
End Namespace



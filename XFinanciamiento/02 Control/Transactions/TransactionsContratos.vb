﻿Imports XFinanciamiento.Data.Datos
Imports XFinanciamiento.Control.Negocio

Public Class TransactionsContratos


    Public Shared Sub CreaUDOContratos()
        GC.Collect()
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim lRetCode As Integer
        Dim sErrMsg As String
        Dim xmlDoc As New Xml.XmlDocument
        Dim strPath As String = String.Empty

        'Se verifica que el UDO no exista
        If NegocioUDOS.VerificarUDOs("CNTT") = -1 Then

            strPath = Application.StartupPath & "\CNTT.srf" 'se pasa la ruta del formulario
            xmlDoc.Load(strPath) 'se carga el fomulario

            'oExxCulture.Id = "UDO_F_CNTT"
            'xmlDoc = oExxCulture.getCultureSrf(xmlDoc)

            'Se introducen las propiedades del UDO
            oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
            'oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.Code = "CNTT"
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.Name = "Contratos"
            oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
            oUserObjectMD.TableName = "CNTT"
            'oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

            'Se introduce el campo de busqueda original
            oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
            oUserObjectMD.FindColumns.ColumnDescription = "Contrato"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
            oUserObjectMD.FindColumns.ColumnDescription = "Cliente"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_BoxPatent"
            oUserObjectMD.FindColumns.ColumnDescription = "Cajón Patente"
            oUserObjectMD.FindColumns.Add()

            'Se crea una tabla de LOG
            oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

            'Se crea el formulario default con las columnas basicas
            oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
            oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
            oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "CreateDate"
            oUserObjectMD.FormColumns.FormColumnDescription = "CreateDate"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "UpdateDate"
            oUserObjectMD.FormColumns.FormColumnDescription = "UpdateDate"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "Canceled"
            oUserObjectMD.FormColumns.FormColumnDescription = "Canceled"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()


            'Se declara la tabla de Lineas
            oUserObjectMD.ChildTables.TableName = "NTT1"
            oUserObjectMD.ChildTables.Add()
            oUserObjectMD.ChildTables.TableName = "NTT2"
            oUserObjectMD.ChildTables.Add()
            oUserObjectMD.ChildTables.TableName = "NTT3"
            oUserObjectMD.ChildTables.Add()
            oUserObjectMD.ChildTables.TableName = "NTT4"
            oUserObjectMD.ChildTables.Add()
            oUserObjectMD.ChildTables.TableName = "NTT5"
            oUserObjectMD.ChildTables.Add()
            oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

            ''Se introducen las columnas
            'oUserObjectMD.FormColumns.FormColumnAlias = "U_ItemCode"
            'oUserObjectMD.FormColumns.FormColumnDescription = "Código"
            'oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.FormColumns.SonNumber = 1
            'oUserObjectMD.FormColumns.Add()


            oUserObjectMD.EnhancedFormColumns.Add()
            oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
            oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
            oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
            oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

            oUserObjectMD.EnhancedFormColumns.Add()
            oUserObjectMD.EnhancedFormColumns.SetCurrentLine(1)
            oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_ItemCode"
            oUserObjectMD.EnhancedFormColumns.ColumnDescription = "Código"
            oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.EnhancedFormColumns.ColumnNumber = 2
            oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

            oUserObjectMD.EnhancedFormColumns.Add()
            oUserObjectMD.EnhancedFormColumns.SetCurrentLine(3)
            oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
            oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
            oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            oUserObjectMD.EnhancedFormColumns.ChildNumber = 2

            oUserObjectMD.EnhancedFormColumns.Add()

            ' ''oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_ItemCode"
            ' ''oUserObjectMD.EnhancedFormColumns.ColumnDescription = "Codigo"
            ' ''oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            ' ''oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            ' ''oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            ' ''oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

            ' ''oUserObjectMD.EnhancedFormColumns.Add()
            'oUserObjectMD.EnhancedFormColumns.ColumnAlias = "Code"
            'oUserObjectMD.EnhancedFormColumns.ColumnDescription = "Nombre de Actividad"
            'oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.ColumnNumber = 2
            'oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
            'oUserObjectMD.EnhancedFormColumns.Add()

            'oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            ''oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            'oUserObjectMD.EnhancedFormColumns.ChildNumber = 2
            'oUserObjectMD.EnhancedFormColumns.Add()
            'oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_ItemCode"
            'oUserObjectMD.EnhancedFormColumns.ColumnDescription = "Código"
            'oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.ColumnNumber = 2
            'oUserObjectMD.EnhancedFormColumns.ChildNumber = 2
            'oUserObjectMD.EnhancedFormColumns.Add()

            'oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            ''oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            'oUserObjectMD.EnhancedFormColumns.ChildNumber = 3
            'oUserObjectMD.EnhancedFormColumns.Add()



            'oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
            'oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
            ''oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
            'oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
            'oUserObjectMD.EnhancedFormColumns.ChildNumber = 4
            'oUserObjectMD.EnhancedFormColumns.Add()

            'Se setea el formulario modificado para que este por defecto

            oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString


            'Se crea el UDO
            Dim aux As String = oUserObjectMD.GetAsXML
            lRetCode = oUserObjectMD.Add()

            '// check for errors in the process
            If lRetCode <> 0 Then
                DiApp.GetLastError(lRetCode, sErrMsg)
                MsgBox(sErrMsg)
                Exit Sub
            End If

            oUserObjectMD = Nothing
        Else
            'Se prepara a modificar el UDO existente
            oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

            If oUserObjectMD.GetByKey("CNTT") Then
                strPath = Application.StartupPath & "\CNTT.srf"
                xmlDoc.Load(strPath)
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO

                'oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                'oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                'oUserObjectMD.FindColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
                'oUserObjectMD.FindColumns.ColumnDescription = "Cliente"
                'oUserObjectMD.FindColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_BoxPatent"
                'oUserObjectMD.FindColumns.ColumnDescription = "Caja Patente"
                'oUserObjectMD.FindColumns.Add()
                lRetCode = oUserObjectMD.Update()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If
            End If
            oUserObjectMD = Nothing
        End If

        GC.Collect() 'Release the handle to the table
    End Sub



    Public Shared Sub CreaUDOAsistenteEntregas()
        GC.Collect()
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim lRetCode As Integer
        Dim sErrMsg As String
        Dim xmlDoc As New Xml.XmlDocument
        Dim strPath As String = String.Empty

        'Se verifica que el UDO no exista
        If NegocioUDOS.VerificarUDOs("EXXASISENT") = -1 Then

            strPath = Application.StartupPath & "\EXXASISENT.srf" 'se pasa la ruta del formulario
            xmlDoc.Load(strPath) 'se carga el fomulario

            'oExxCulture.Id = "UDO_F_CNTT"
            'xmlDoc = oExxCulture.getCultureSrf(xmlDoc)

            'Se introducen las propiedades del UDO
            oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
            'oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.Code = "EXXASISENT"
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.Name = "EXXASISENT"
            oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
            oUserObjectMD.TableName = "EXX_ASISENT"
            'oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

            'Se introduce el campo de busqueda original
            oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
            oUserObjectMD.FindColumns.ColumnDescription = "Nro"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
            oUserObjectMD.FindColumns.ColumnDescription = "Proveedor"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_CardName"
            oUserObjectMD.FindColumns.ColumnDescription = "Nombre"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_Status"
            oUserObjectMD.FindColumns.ColumnDescription = "Estado"
            oUserObjectMD.FindColumns.Add()
            oUserObjectMD.FindColumns.ColumnAlias = "U_Comments"
            oUserObjectMD.FindColumns.ColumnDescription = "Comentarios"
            oUserObjectMD.FindColumns.Add()
            'Se crea una tabla de LOG
            oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

            'Se crea el formulario default con las columnas basicas
            oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
            oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
            oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "CreateDate"
            oUserObjectMD.FormColumns.FormColumnDescription = "CreateDate"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "UpdateDate"
            oUserObjectMD.FormColumns.FormColumnDescription = "UpdateDate"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()
            oUserObjectMD.FormColumns.FormColumnAlias = "Canceled"
            oUserObjectMD.FormColumns.FormColumnDescription = "Canceled"
            oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
            oUserObjectMD.FormColumns.Add()


            'Se declara la tabla de Lineas
            oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO


            'Se setea el formulario modificado para que este por defecto

            oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString


            'Se crea el UDO
            Dim aux As String = oUserObjectMD.GetAsXML
            lRetCode = oUserObjectMD.Add()

            '// check for errors in the process
            If lRetCode <> 0 Then
                DiApp.GetLastError(lRetCode, sErrMsg)
                MsgBox(sErrMsg)
                Exit Sub
            End If

            oUserObjectMD = Nothing
        Else
            'Se prepara a modificar el UDO existente
            oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

            If oUserObjectMD.GetByKey("EXXASISENT") Then
                strPath = Application.StartupPath & "\EXXASISENT.srf"
                xmlDoc.Load(strPath)
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO


                lRetCode = oUserObjectMD.Update()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If
            End If
            oUserObjectMD = Nothing
        End If

        GC.Collect() 'Release the handle to the table
    End Sub

End Class

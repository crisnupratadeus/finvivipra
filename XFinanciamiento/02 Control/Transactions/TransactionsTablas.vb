﻿
Imports XFinanciamiento.Data
Imports SAPbobsCOM
Imports System.Transactions
Imports SAPbouiCOM

Namespace Control.Transactions

    Public Class TransactionsTablas

        Public Shared conexi As Conexiones = New Conexiones

        ''' <summary>
        ''' Crea Tablas
        ''' </summary>
        Public Shared Sub CreaTablas(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Dim progress As SAPbouiCOM.ProgressBar = Nothing
            Try
                progress = oApp.StatusBar.CreateProgressBar("Creando tablas de usuario", 100, False)
                NegocioTablasUser.Creatabla("EXX_GAR", "Maestro de Garantías", "Financiamiento", "NO", True)
                progress.Value = 10
                NegocioTablasUser.Creatabla("EXX_FIPARAM", "CONFIGURACION_FINANCIAMIENTO", "")
                progress.Value = 15
                NegocioTablasUser.Creatabla("EXX_FIPLAN", "Plantilla ReqGar", "Financiamiento", "D", True)
                progress.Value = 20
                NegocioTablasUser.Creatabla("EXX_FIPLA1", "Lineas Plantilla", "Financiamiento", "DL", True)
                progress.Value = 25
                NegocioTablasUser.Creatabla("EXX_LICRE", "Lineas de Crédito", "Financiamiento", "M", True)
                progress.Value = 30
                NegocioTablasUser.Creatabla("EXX_LICR1", "Lineas Checklist", "Financiamiento", "NO")
                progress.Value = 35
                NegocioTablasUser.Creatabla("EXX_LICR2", "Lineas Tasas", "Financiamiento", "NO")
                progress.Value = 37
                NegocioTablasUser.Creatabla("EXX_LICHK", "Checklist", "Financiamiento", "NO")
                progress.Value = 40
                NegocioTablasUser.Creatabla("EXX_IATT", "Adjuntos de Imágenes y Docs", "Financiamiento", "NO")
                progress.Value = 45
                NegocioTablasUser.Creatabla("EXX_FIPLD1", "Detalle valores de plantilla", "Financiamiento", "NO")
                progress.Value = 50
                NegocioTablasUser.Creatabla("EXX_CUOTA", "Cuotas", "Financiamiento", "D")
                progress.Value = 55
                NegocioTablasUser.Creatabla("EXX_CUOIN", "Plan de Cuotas", "Financiamiento", "NO")
                progress.Value = 65
                NegocioTablasUser.Creatabla("EXX_INTERESES", "Intereses a pagar", "Financiamiento", "D", True)
                progress.Value = 75
                NegocioTablasUser.Creatabla("EXX_FACT1", "Lineas Factura", "Financiamiento", "DL", True)
                progress.Value = 85
                NegocioTablasUser.Creatabla("EXX_CUOT1", "Lineas Cuotas", "Financiamiento", "DL", True)
                progress.Value = 90
                NegocioTablasUser.Creatabla("EXX_REFIN", "Refinanciamiento", "Financiamiento", "D")
                progress.Value = 100

                System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                progress = Nothing
            Catch ex As Exception
                If Not progress Is Nothing Then
                    progress.Stop()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                    progress = Nothing
                End If
                Exit Sub
            End Try
        End Sub
        Public Shared Sub CamposTablaGABL(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_GABL", "Addon", "Addon", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "Y")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_GABL", "Activa", "Activa", BoFieldTypes.db_Alpha, "Y", 1, ht, "Y")
        End Sub
        Public Shared Sub CamposTablaFIPLAN(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_FIPLAN", "Codigo", "Codigo Plantilla", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLAN", "Descr", "Descripcion", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 200, , "N")
            ht.Clear()
            ht.Add("R", "Requisito")
            ht.Add("G", "Garantia")
            NegocioTablasUser.CreaCampoConValores("EXX_FIPLAN", "Tipo", "Tipo Plantilla", BoFieldTypes.db_Alpha, "R", 1, ht, "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLAN", "Comments", "Comentarios", SAPbobsCOM.BoFieldTypes.db_Memo, String.Empty, 254, , "N")
        End Sub
        Public Shared Sub CamposTablaFIPLA1(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_FIPLA1", "Codigo", "Codigo Campo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLA1", "Descr", "Descripcion Campo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 200, , "Y")
            ht.Clear()
            ht.Add("A", "Alfanumerico")
            ht.Add("N", "Numerico")
            ht.Add("B", "Booleano")
            ht.Add("F", "Fecha")
            NegocioTablasUser.CreaCampoConValores("EXX_FIPLA1", "TipoC", "Tipo Campo", BoFieldTypes.db_Alpha, "A", 1, ht, "Y")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_FIPLA1", "Oblig", "Obligatorio", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "Y")
        End Sub
        Public Shared Sub CamposTablaLICR1(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_LICR1", "CodLC", "Codigo LC", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICR1", "CodChk", "Codigo Check", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICR1", "DesChk", "Descripción Check", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 150, , "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_LICR1", "Realizado", "Realizado", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N")

        End Sub

        Public Shared Sub CamposTablaLICR2(ByRef oForm As SAPbouiCOM.Form) 'ssh
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_LICR2", "CodLC", "Codigo LC", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICR2", "CodGrupArt", "Codigo Grupo Articulo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICR2", "TasaNor", "Tasa Normal", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N")
            NegocioTablasUser.CreaCampo("EXX_LICR2", "TasaLeg", "Tasa Legal", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            'NegocioTablasUser.CreaCampoConValores("EXX_LICR1", "Realizado", "Realizado", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N")

        End Sub

        Public Shared Sub CamposLinkeados(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("OCTG", "UsaLc", "Obligatorio", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "Y", "Y")
            NegocioTablasUser.CreaCampo("ORDR", "DocCuota", "DocEntry Cuotas", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 50, , "N", , "Y")
            NegocioTablasUser.CreaCampo("ORDR", "CuotFi", "Cuotas Financiamiento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , "Y")
            NegocioTablasUser.CreaCampo("ORDR", "IntMor", "Interes Legal", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N", , "Y")
            NegocioTablasUser.CreaCampo("ORDR", "Interes", "Interes", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N", , "Y")
            NegocioTablasUser.CreaCampo("ORDR", "DocFactPre", "Numero Documento Factura Pre", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 50, , "N", , "Y")
            NegocioTablasUser.CreaCampo("ORDR", "OrdenVenta", "Numero de Orden de Venta", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , "Y")
            NegocioTablasUser.CreaCampo("ORCT", "CodUd", "Codigo UDO", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N", "INTERESES_GEN", "N")
            NegocioTablasUser.CreaCampo("ORCT", "CodUdIG", "Codigo Intereses Generados", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N", "INTERESES_GEN", "N")
            NegocioTablasUser.CreaCampo("ORCT", "ORDV", "DocEntry Orden de Venta", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 50, , "N", , "Y")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("ORDR", "Refinancia", "Refinanciamiento", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 10, ht, "N", "Y")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("OINV", "Refinancia", "Refinanciamiento", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 10, ht, "N", "Y")

            'ht.Clear()
            'ht.Add("Y", "Si")
            'ht.Add("N", "No")
            'NegocioTablasUser.CreaCampoConValores("OCRD", "Sobregiro", "Sobregiro", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N", "Y")
            'ht.Clear()
            'ht.Add("0", "0%")
            'ht.Add("1", "10%")
            'ht.Add("2", "20%")
            'ht.Add("3", "30%")
            'ht.Add("4", "40%")
            'ht.Add("5", "50%")
            'ht.Add("6", "60%")
            'ht.Add("7", "70%")
            'ht.Add("8", "80%")
            'ht.Add("9", "90%")
            'ht.Add("10", "100%")
            'NegocioTablasUser.CreaCampoConValores("OCRD", "PrcSG", "Porcentaje Sobregiro", SAPbobsCOM.BoFieldTypes.db_Alpha, "0", 10, ht, "N", "Y")
            'NegocioTablasUser.CreaCampo("OCRD", "UsrAut", "Autorizador", SAPbobsCOM.BoFieldTypes.db_Numeric, 0, 6, , "N", , "Y", , 12)

            'NegocioTablasUser.CreaCampo("ORDR", "DocCuota", "DocEntry Cuotas", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 50)
            'NegocioTablasUser.CreaCampo("ORDR", "CuotFi", "Cuotas Financiamiento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
        End Sub

        Public Shared Sub CamposTablaLICRE(ByRef oForm As SAPbouiCOM.Form)

            Dim ht As New Hashtable

            NegocioTablasUser.CreaCampo("EXX_LICRE", "CardCode", "Socio de Negocio", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Nombre", "Nombre Socio de Negocio", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "FechaCon", "Fecha de Contrato", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 8, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Interes", "Intereses", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "InteresM", "Interes Moratorio", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Percentage, "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "FechaDe", "Fecha Desde", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "FechaHa", "Fecha Hasta", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Observ", "Observaciones", SAPbobsCOM.BoFieldTypes.db_Memo, String.Empty, 250, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "CondP", "Condicion de Pago", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Estado", "Estado de Formulario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Project", "Proyecto", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "PrjName", "PrjName", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_LICRE", "Inactive", "Inactivo", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N")
            ht.Clear()
            ht.Add("-", "-")
            ht.Add("P", "Pendiente")
            ht.Add("A", "Aprobado")
            ht.Add("R", "Rechazado")
            NegocioTablasUser.CreaCampoConValores("EXX_LICRE", "EstadoAp", "Estado de autorización", SAPbobsCOM.BoFieldTypes.db_Alpha, "-", 1, ht, "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "Etapa", "Etapa", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "LinEtapa", "Linea Etapa", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_LICRE", "CorreSol", "Correlativo Solicitud", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 32, , "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_LICRE", "Intlgl", "Inactivo", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N")

        End Sub
        Public Shared Sub CamposTablaFIPLD1(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Codigo", "Codigo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Name", "Nombre", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Codigo", "Codigo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            'NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Name", "Nombre", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "Y") mauricio
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Plantilla", "Identificador @EXX_FIPLAN Plantilla", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Codigo", "Identificador @EXX_FIPLA1", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 32, , "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "Valor", "Valor del campo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "Y")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "CodeLineaCred", "Codigo de linea de credito", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "CodeDoc", "Codigo de Documento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_FIPLD1", "TipoDoc", "Tipo de Documento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            
        End Sub

        Public Shared Sub CamposTablasParam(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable

            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 0, 10)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "TypeDscr", "TypeDscr", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value1", "Value1", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value2", "Value2", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value3", "Value3", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value4", "Value4", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value5", "Value5", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value6", "Value6", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value7", "Value7", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value8", "Value8", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value9", "Value9", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Value10", "Value10", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Numeric, 0, 1)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "UserName", "User Name", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100)
            NegocioTablasUser.CreaCampo("EXX_FIPARAM", "CreateDate", "CreateDate", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 0)

        End Sub

        ''' <summary>
        '''Crea Procedimientos
        ''' </summary>
        Public Shared Sub CreaProcedimientos(ByVal nombreSp As String, ByVal SpText As String, ByRef tip As String)

            Try

                If NegocioTablasUser.VerificaSP(nombreSp, tip) Then
                    If tip = 2 Then
                        NegocioTablasUser.EliminaFX(nombreSp)
                    ElseIf tip = 3 Then
                        NegocioTablasUser.EliminaSP(nombreSp)
                    End If
                    NegocioTablasUser.CreateSP(SpText)
                Else
                    NegocioTablasUser.CreateSP(SpText)
                End If

            Catch ex As Exception

            End Try

        End Sub
        Public Shared Sub CreaTF(ByVal nombreSp As String, ByVal TfText As String)

            Try
                If NegocioTablasUser.VerificaTF(nombreSp) Then
                    NegocioTablasUser.EliminaTF(nombreSp)
                    NegocioTablasUser.CreateSP(TfText)
                Else
                    NegocioTablasUser.CreateSP(TfText)
                End If

            Catch ex As Exception

            End Try

        End Sub
        Public Shared Sub CamposTablaIATT(form As Form)

            NegocioTablasUser.CreaCampo("EXX_IATT", "ObjCode", "Código de Objeto", BoFieldTypes.db_Alpha, String.Empty, 100, , "Y")
            NegocioTablasUser.CreaCampo("EXX_IATT", "Path", "Ruta Adjunto", BoFieldTypes.db_Alpha, String.Empty, 254, , "Y")
            NegocioTablasUser.CreaCampo("EXX_IATT", "AttDate", "Fecha Adjunto", BoFieldTypes.db_Date, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_IATT", "DocCode", "Código de Documento", BoFieldTypes.db_Alpha, String.Empty, 100, , "Y")

        End Sub
        Public Shared Sub CamposTablasCuotas(ByRef oForm As SAPbouiCOM.Form)
            NegocioTablasUser.CreaCampo("EXX_CUOTA", "Cuotas", "Numero de Cuotas", BoFieldTypes.db_Numeric, 1, 30, , "Y", , ,)
            NegocioTablasUser.CreaCampo("EXX_CUOTA", "TasaInteres", "Tasa de Interes", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_CUOTA", "InteresLegal", "Tasa de Interes Legal", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_CUOTA", "Anticipo", "Anticipo o Reserva", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Price, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_CUOTA", "DocEntry", "DocEntry de la OV", BoFieldTypes.db_Numeric, String.Empty, 10)
        End Sub
        Public Shared Sub CamposTablasLCuotas(ByRef oForm As SAPbouiCOM.Form)
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "DocEntryOV", "DocEntry OV", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "FechaV", "Fecha de Vencimiento", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "DocEntryPG", "DocEntry Plan Pago", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 10)
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "TotalCuota", "Total Monto Cuota", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "CapitalxPagar", "Total Capital por Pagar", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_CUOIN", "Amortizacion", "Total Amortizado", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
        End Sub

        Public Shared Sub CreaTType(ByVal nombreSp As String, ByVal SpText As String)

            Try
                If NegocioTablasUser.VerificaTType(nombreSp) Then
                    NegocioTablasUser.EliminaTType(nombreSp)
                    NegocioTablasUser.CreateTType(SpText)
                Else
                    NegocioTablasUser.CreateTType(SpText)
                End If

            Catch ex As Exception

            End Try

        End Sub

        Public Shared Sub CamposTablaINTERESES(ByRef oForm As SAPbouiCOM.Form)

            Dim ht As New Hashtable

            NegocioTablasUser.CreaCampo("EXX_INTERESES", "CardCode", "Socio de Negocio", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_INTERESES", "FechaPa", "Fecha Pago", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            NegocioTablasUser.CreaCampo("EXX_INTERESES", "FechaVa", "Fecha Valor", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_INTERESES", "Pagado", "Pagado", SAPbobsCOM.BoFieldTypes.db_Alpha, "N", 1, ht, "N")

        End Sub

        Public Shared Sub CamposTablaEXX_FACT1(ByRef oForm As SAPbouiCOM.Form)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "DocEntryF", "DocEntry Factura", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "NumFact", "Numero Factura", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "DiasMor", "Dias Mora", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "InteresAp", "Int Aplicado", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "Interes", "Monto Interes", BoFieldTypes.db_Float, String.Empty, 100, BoFldSubTypes.st_Rate, "N", , ,)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "DocEntryG", "DocEntry Gen", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_FACT1", "NumFactG", "Num Fact Gen", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
        End Sub

        Public Shared Sub CamposTablaEXX_CUOT1(ByRef oForm As SAPbouiCOM.Form)
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "DocEntryF", "DocEntry F", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "NroFac", "Factura", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "Cuota", "Cuota", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "Monto", "Monto", SAPbobsCOM.BoFieldTypes.db_Float, 0, 18, BoFldSubTypes.st_Price, "N")
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "FechaV", "Fecha de Vencimiento", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 10, , "N")
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "DocEntryPG", "DocEntry Plan Pago", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50)
            NegocioTablasUser.CreaCampo("EXX_CUOT1", "TotalCuota", "Total Monto Cuota", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50) 'mauricio
        End Sub

    End Class

End Namespace


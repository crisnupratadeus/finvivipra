﻿
Imports XFinanciamiento.Data
Imports SAPbobsCOM
Imports System.Transactions

Namespace Control.Transactions
    Public Class TransactionSearches


        Public Shared Sub CreaCategoria(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Dim res As Integer
            Dim err As String
            Try

                Dim oQueryCategory As SAPbobsCOM.QueryCategories = DiApp.GetBusinessObject(BoObjectTypes.oQueryCategories)

                oQueryCategory.Name = "AddonCardealer"
                res = oQueryCategory.Add()
                If res = 0 Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Categoria creada exitosamente")
                Else
                    DiApp.GetLastError(res, err)
                    If Not res = "-5002" Then
                        oApp.MessageBox(err & "_" & res)
                    End If
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try
        End Sub

        Public Shared Sub CreaBusquedasUsr(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Dim res As Integer
            Dim err As String
            Dim oRec As SAPbobsCOM.Recordset
            Dim oUserQuery As SAPbobsCOM.UserQueries
            Dim idCategoria As Integer
            Try
                oRec = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                oRec.DoQuery("select ISNULL(CategoryId,0) As Categoria from OQCN where CatName = 'AddonCardealer'")
                If oRec.RecordCount > 0 Then
                    oRec.MoveFirst()
                    idCategoria = oRec.Fields.Item("Categoria").Value
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontró la categoria de consultas para el addon")
                    Exit Sub
                End If

                oUserQuery = DiApp.GetBusinessObject(BoObjectTypes.oUserQueries)
                oUserQuery.QueryCategory = idCategoria
                oUserQuery.QueryDescription = "Busqueda de Proveedores"
                oUserQuery.Query = "SELECT CardCode,CardName FROM OCRD"

                res = oUserQuery.Add()
                If Not res = 0 Then
                    DiApp.GetLastError(res, err)
                    If Not res = "-5002" Then
                        oApp.MessageBox(err & "_" & res)
                    End If
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try
        End Sub

        Public Shared Sub CreaAsociacionBusqueda(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Dim res As Integer
            Dim err As String
            Dim oRec As SAPbobsCOM.Recordset
            Dim oFormatted As SAPbobsCOM.FormattedSearches
            Dim idCategoria As String
            Dim idBusqueda As String
            Dim idFormatted As String
            Try
                oRec = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                oRec.DoQuery("SELECT ISNULL(A.CategoryId,'') as CategoryId,ISNULL(B.IntrnalKey,'') as IntrnalKey,ISNULL(C.FormID,'') as Busqueda FROM OQCN A INNER JOIN OUQR B ON A.CategoryId = B.QCategory LEFT JOIN CSHS C ON C.QueryId = B.IntrnalKey WHERE CatName = 'AddonCardealer'AND QName = 'Busqueda de Proveedores'")
                If oRec.RecordCount > 0 Then
                    oRec.MoveFirst()
                    idCategoria = oRec.Fields.Item("CategoryId").Value
                    idBusqueda = oRec.Fields.Item("IntrnalKey").Value
                    idFormatted = oRec.Fields.Item("Busqueda").Value

                    If Trim(idFormatted) = "" Then
                        If Not Trim(idBusqueda) = "" Then
                            oFormatted = DiApp.GetBusinessObject(BoObjectTypes.oFormattedSearches)
                            oFormatted.FormID = "721"
                            oFormatted.ItemID = "13"
                            oFormatted.ColumnID = "U_ProvVeh"
                            oFormatted.Action = BoFormattedSearchActionEnum.bofsaQuery
                            oFormatted.Refresh = BoYesNoEnum.tNO
                            oFormatted.ByField = BoYesNoEnum.tNO
                            oFormatted.ForceRefresh = BoYesNoEnum.tNO
                            oFormatted.QueryID = Trim(idBusqueda)
                            res = oFormatted.Add()
                            If Not res = 0 Then
                                DiApp.GetLastError(res, err)
                                If Not res = "-2035" Then
                                    oApp.MessageBox(err & "_" & res)
                                End If
                            End If
                        End If
                    End If
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try
        End Sub
    End Class
End Namespace




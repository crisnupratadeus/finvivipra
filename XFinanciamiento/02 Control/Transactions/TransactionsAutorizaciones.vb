﻿
Imports XFinanciamiento.Data
Imports SAPbobsCOM
Imports System.Transactions

Namespace Control.Transactions
    Public Class TransactionsAutorizaciones
        'Crea las tablas necesarias para la autorizacion de actualizacion de datos
        Public Shared Sub CreaTablasAUTO(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            NegocioTablasUser.Creatabla("EXX_AUTOFOR", "Formularios", "Autorizacion", "NO") 'Formularios
            NegocioTablasUser.Creatabla("EXX_AUTOFEST", "Estados Formulario", "Autorizacion", "NO") 'Estados de formulario
            NegocioTablasUser.Creatabla("EXX_AUTOLOG", "Log Autorizaciones", "Autorizacion", "NO") 'Log de Autorizaciones
            NegocioTablasUser.Creatabla("EXX_AUTOMOD", "Modelo Autorización", "Autorizacion", "D") 'Modelo Autorizacion Cabeza
            NegocioTablasUser.Creatabla("EXX_AUTOMOE", "Lineas Etapas", "Autorizacion", "DL") 'Modelo Autorizacion Lineas Etapas
            NegocioTablasUser.Creatabla("EXX_AUTOMOC", "Lineas Condiciones", "Autorizacion", "DL") 'Modelo Autorizacion Lineas Condiciones
            NegocioTablasUser.Creatabla("EXX_AUTOETAP", "Etapas Autorización", "Autorizacion", "D") 'Etapas autorizacion
            NegocioTablasUser.Creatabla("EXX_AUTOETAL", "Lineas Etapa", "Autorizacion", "DL") 'Etapas autorizacion
            NegocioTablasUser.Creatabla("EXX_AUTOCON", "Condiciones Autorización", "Autorizacion", "D") 'Modelo Autorizacion Cabeza
            NegocioTablasUser.Creatabla("EXX_AUTOCOL", "Lineas Condiciones", "Autorizacion", "DL") 'Modelo Autorizacion Cabeza
            NegocioTablasUser.Creatabla("EXX_AUTOUSRA", "Alarmas Usuarios", "Autorizacion", "NO") 'Log de Autorizaciones
            NegocioTablasUser.Creatabla("EXX_MODFECLOG", "Log Modificaciones Fechas", "Autorizacion", "NO") 'Log de Modificaciones en fecha


        End Sub
        Public Shared Sub CamposTablasAutorizaciones(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable

            '***  CAMPOS TABLAS AUTORIZACION ***
            'Campos Tabla Formularios
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOFOR", "Deshab", "Deshabilitado", BoFieldTypes.db_Alpha, "N", 1, ht, "N")

            'Campos Tabla Condiciones
            NegocioTablasUser.CreaCampo("EXX_AUTOCON", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
            NegocioTablasUser.CreaCampo("EXX_AUTOCON", "Funcion", "Funcion Tabla", BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOCON", "Deshab", "Deshabilitado", BoFieldTypes.db_Alpha, "N", 1, ht, "N")

            'Campos Lineas Condiciones
            NegocioTablasUser.CreaCampo("EXX_AUTOCOL", "Campo", "Campo Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            ht.Clear()
            ht.Add("-", "-")
            ht.Add("MEN", "Menor a")
            ht.Add("MAY", "Mayor a")
            ht.Add("MEI", "Menor igual a")
            ht.Add("MAI", "Mayor igual a")
            ht.Add("IGU", "Igual a")
            ht.Add("DIF", "Diferente a")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOCOL", "Condt", "Condiciones", BoFieldTypes.db_Alpha, "-", 3, ht, "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOCOL", "Valor", "Valor Campo", BoFieldTypes.db_Alpha, String.Empty, 150, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOCOL", "CampoVal", "Campo de Valor", BoFieldTypes.db_Alpha, String.Empty, 150, , "N", , , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOCOL", "EsCampo", "Es Campo", BoFieldTypes.db_Alpha, "N", 1, ht, "N")

            'Campos Tabla Estados Formulario
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Code", "Codigo Estado", BoFieldTypes.db_Alpha, String.Empty, 50, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Estado", "Estado Formulario", BoFieldTypes.db_Alpha, String.Empty, 100, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")

            'Campos tabla grupos autorizacion Cabecera
            NegocioTablasUser.CreaCampo("EXX_AUTOMOD", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
            NegocioTablasUser.CreaCampo("EXX_AUTOMOD", "Nombre", "Nombre Modelo", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOMOD", "Descr", "Descripción Modelo", BoFieldTypes.db_Alpha, String.Empty, 250, , "N", , , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOMOD", "EsConCod", "Con Condiciones", BoFieldTypes.db_Alpha, "N", 1, ht, "N", , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOMOD", "Activo", "Activo", BoFieldTypes.db_Alpha, "Y", 1, ht, "N")
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOMOD", "EtpCon", "Etapas Consecutivas", BoFieldTypes.db_Alpha, "Y", 1, ht, "N", , )

            'Campos tabla modelos autorizacion lineas Etapas
            NegocioTablasUser.CreaCampo("EXX_AUTOMOE", "CodEtp", "Codigo Etapa", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOMOE", "NomEtp", "Nombre Etapa", BoFieldTypes.db_Alpha, String.Empty, 100, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOMOE", "EstadoF", "Estado Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOMOE", "EtpFin", "Etapa Final", BoFieldTypes.db_Alpha, "N", 1, ht, "N", , )

            'Campos tabla modelos autorizacion lineas Condiciones
            NegocioTablasUser.CreaCampo("EXX_AUTOMOC", "CodCon", "Codigo Condicion", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOMOC", "CodEtp", "Codigo Etapa", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )

            'Campos tabla etapas autorizacion Cabecera
            NegocioTablasUser.CreaCampo("EXX_AUTOETAP", "Nombre", "Nombre Etapa", BoFieldTypes.db_Alpha, String.Empty, 100, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOETAP", "Descr", "Descripción Etapa", BoFieldTypes.db_Alpha, String.Empty, 250, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOETAP", "NroA", "Número Autorización", BoFieldTypes.db_Numeric, 1, 30, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOETAP", "NroR", "Número Rechazos", BoFieldTypes.db_Numeric, 1, 30, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOETAP", "Obs", "Observaciones", BoFieldTypes.db_Memo, String.Empty, 250, , "N", , , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOETAP", "Activo", "Activo", BoFieldTypes.db_Alpha, "Y", 1, ht, "N")

            'Campos tabla etapas autorizacion lineas
            NegocioTablasUser.CreaCampo("EXX_AUTOETAL", "CodUsr", "Codigo Usuario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOETAL", "NomUsr", "Nombre Usuario", BoFieldTypes.db_Alpha, String.Empty, 155, , "N", , , )
            ht.Clear()
            ht.Add("Y", "Si")
            ht.Add("N", "No")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOETAL", "Alerta", "Alerta", BoFieldTypes.db_Alpha, "Y", 1, ht, "N")

            'campos tabla usuarios alarmas
            NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "CodUsr", "Codigo Usuario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "NomUsr", "Nombre Usuario", BoFieldTypes.db_Alpha, String.Empty, 155, , "N", , , )
            ' NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "PresEntry", "Codigo Presupuesto", BoFieldTypes.db_Alpha, String.Empty, 30, , "N", , , ) 'cnp20190710
            NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
            NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "EtapaC", "Codigo Etapa", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOUSRA", "EtapaN", "Nombre Etapa", BoFieldTypes.db_Alpha, String.Empty, 150, , "N", , , )

            'Campos tabla de log autorizaciones
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "DocEntry", "Codigo Documento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 30, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "PresEntry", "Codigo Presupuesto", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "Correlativo", "Correlativo", SAPbobsCOM.BoFieldTypes.db_Numeric, String.Empty, 32, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "CodEstado", "Codigo Estado", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 50, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "DesEstado", "Descripción Estado", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "CodeAcc", "Codigo Acción", BoFieldTypes.db_Alpha, String.Empty, 1, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "DesAcc", "Descripción Acción", BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "Etapa", "Etapa", BoFieldTypes.db_Alpha, String.Empty, 30, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "AutID", "Codigo Usuario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "AutNom", "Nombre Usuario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "FechaAut", "Fecha Autorización", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "CorreSol", "Correlativo Solicitud", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 32, , "N")
            NegocioTablasUser.CreaCampo("EXX_AUTOLOG", "MotivoRec", "Motivo Rechazo", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 200, , "N")

            'Campos tabla de log de modificaciones en fechas
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "DocEntry", "Codigo Documento", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 30, , "N")
            'NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "ModID", "Codigo Usuario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 20, , "N")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "AutID", "codigo Usuario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "AutNom", "Nombre Usuario", SAPbobsCOM.BoFieldTypes.db_Alpha, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "FechaAnt", "Fecha Anterior", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "FechaFin", "Fecha Final", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 100, , "N")
            NegocioTablasUser.CreaCampo("EXX_MODFECLOG", "FechaMod", "Fecha de la Modificacion", SAPbobsCOM.BoFieldTypes.db_Date, String.Empty, 100, , "N")




        End Sub
        Public Shared Sub CamposTablaAUTOFEST(ByRef oForm As SAPbouiCOM.Form)
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Code", "Codigo Estado", BoFieldTypes.db_Alpha, String.Empty, 50, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Estado", "Estado Formulario", BoFieldTypes.db_Alpha, String.Empty, 100, , "Y", , , )
            NegocioTablasUser.CreaCampo("EXX_AUTOFEST", "Form", "Formulario", BoFieldTypes.db_Alpha, String.Empty, 50, , "N", , , "EXX_AUTOFOR")
        End Sub
        Public Shared Sub CamposTablaAUTOFOR(ByRef oForm As SAPbouiCOM.Form)
            Dim ht As New Hashtable
            ht.Clear()
            ht.Add("N", "No")
            ht.Add("Y", "Sí")
            NegocioTablasUser.CreaCampoConValores("EXX_AUTOFOR", "Deshab", "Deshabilitado", SAPbobsCOM.BoFieldTypes.db_Alpha, "-", 1, ht, "N", , )

        End Sub

        Public Shared Function BorraDatosAUTOI(ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oUserTable As SAPbobsCOM.UserTable
            Dim rs As SAPbobsCOM.Recordset
            Try
                oUserTable = DiApp.UserTables.Item("EXX_AUTOI")
                rs = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                rs.DoQuery("SELECT Code FROM [@EXX_AUTOI]")
                While (Not rs.EoF)
                    Dim key As String = rs.Fields.Item("Code").Value.ToString().Trim()
                    If (oUserTable.GetByKey(key)) Then
                        Try
                            oUserTable.Remove()
                        Catch ex As Exception

                        End Try
                    End If
                    rs.MoveNext()
                End While
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs)
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function BorraDatosUAUTO(ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oUserTable As SAPbobsCOM.UserTable
            Dim rs As SAPbobsCOM.Recordset
            Try
                oUserTable = DiApp.UserTables.Item("EXX_UAUTO")
                rs = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                rs.DoQuery("SELECT Code FROM [@EXX_UAUTO]")
                While (Not rs.EoF)
                    Dim key As String = rs.Fields.Item("Code").Value.ToString().Trim()
                    If (oUserTable.GetByKey(key)) Then
                        Try
                            oUserTable.Remove()
                        Catch ex As Exception

                        End Try
                    End If
                    rs.MoveNext()
                End While
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs)
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function BorraDatosPAUTO(ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oUserTable As SAPbobsCOM.UserTable
            Dim rs As SAPbobsCOM.Recordset
            Try
                oUserTable = DiApp.UserTables.Item("EXX_PAUTO")
                rs = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                rs.DoQuery("SELECT Code FROM [@EXX_PAUTO]")
                While (Not rs.EoF)
                    Dim key As String = rs.Fields.Item("Code").Value.ToString().Trim()
                    If (oUserTable.GetByKey(key)) Then
                        Try
                            oUserTable.Remove()
                        Catch ex As Exception

                        End Try
                    End If
                    rs.MoveNext()
                End While
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs)
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function

    End Class
End Namespace



﻿Imports SAPbobsCOM
Imports System.Transactions
Imports XFinanciamiento.Data

Namespace Control
    Public Class NegocioAutorizaciones

        'Función que obtendrá una lista de UID de items del tipo EditText desde un documento XML y devuelve un DataTable
        Public Shared Function ObtieneEditNItemsUIDFromDoc(ByRef Documento As XDocument) As DataTable
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String

            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 118 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "118")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList
            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function

        'Función que obtendrá una lista de UID de items del tipo EditText desde una ruta y devuelve un DataTable
        Public Shared Function ObtieneEditNItemsUIDFromPath(ByRef xmlDocPath As String) As DataTable

            'definiciones
            Dim Documento As XDocument
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String

            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))



                'se carga el documento XML desde el path del parametro
                Documento = XDocument.Load(xmlDocPath)

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 118 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "118")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList

            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function


        'Función que obtendrá una lista de UID de items del tipo EditText desde un documento XML y devuelve un DataTable
        Public Shared Function ObtieneEditItemsUIDFromDoc(ByRef Documento As XDocument) As DataTable
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String

            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 16 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "16")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList
            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function

        'Función que obtendrá una lista de UID de items del tipo EditText desde una ruta y devuelve un DataTable
        Public Shared Function ObtieneEditItemsUIDFromPath(ByRef xmlDocPath As String) As DataTable

            'definiciones
            Dim Documento As XDocument
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String

            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))



                'se carga el documento XML desde el path del parametro
                Documento = XDocument.Load(xmlDocPath)

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 16 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "16")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList

            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function

        'Función que obtendrá una lista de UID de items del tipo ComboBox desde una ruta y devuelve un DataTable
        Public Shared Function ObtieneComboUIDFromPath(ByRef xmlDocPath As String) As DataTable

            'definiciones
            Dim Documento As XDocument
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String


            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))

                'se carga el documento XML desde el path del parametro
                Documento = XDocument.Load(xmlDocPath)

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 113 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "113")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList

            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function

        'Función que obtendrá una lista de UID de items del tipo ComboBox desde un documento xml y devuelve un ArrayList
        Public Shared Function ObtieneComboUIDFromDoc(ByRef Documento As XDocument) As DataTable
            Dim itemsGral As IEnumerable(Of XElement)
            Dim itemsAdd As IEnumerable(Of XElement)
            Dim items As IEnumerable(Of XElement)
            Dim specific As IEnumerable(Of XElement)
            Dim databind As IEnumerable(Of XElement)
            Dim oString As String = String.Empty
            Dim itemList As New DataTable
            Dim att As String
            Dim uid As String
            Dim desc As String
            Dim tableField As String

            Try
                itemList.Columns.Add("UID", GetType(String))
                itemList.Columns.Add("Desc", GetType(String))
                itemList.Columns.Add("Alias", GetType(String))
                itemList.Columns.Add("Type", GetType(String))

                'se filtran los elementos dentro de items
                itemsGral = From item In Documento.Descendants("items") Select item

                'se filtran los elementos que esten dentro de Action = Add
                itemsAdd = (From itm In itemsGral.Descendants("action") Where itm.Attribute("type").Value = "add" Select itm)

                'se filtran los elementos que sean de tipo item
                items = (From itm1 In itemsAdd.Descendants("item") Where itm1.Attribute("type").Value = 113 Select itm1)

                'se añaden los valores UID de los elementos a una lista
                For Each xEle As XElement In items
                    'itemList.Add(xEle.Attribute("uid").Value)
                    uid = xEle.Attribute("uid").Value
                    desc = xEle.Attribute("description").Value
                    specific = (From spf In xEle.Descendants("specific") Select spf)
                    databind = (From dtb In specific.Descendants("databind") Select dtb)
                    For Each xDtb As XElement In databind
                        att = xDtb.Attribute("alias").Value
                        tableField = xDtb.Attribute("table").Value
                    Next
                    If Not tableField = "" Then
                        If Left(att, 2) = "U_" Then
                            itemList.Rows.Add(uid, desc, att, "113")
                        End If
                    End If
                Next

                'se retorna una lista
                Return itemList
            Catch ex As Exception
                MsgBox(ex.ToString)
                itemList = Nothing
                Return itemList
            End Try
        End Function

        Public Shared Function CargaDesdeRuta(ByRef oApp As SAPbouiCOM.Application, ByVal ruta As String, ByVal NombreForm As String) As Boolean
            Dim lstArray As New DataTable

            Try

                'la lista recibe todos los edittext
                lstArray = ObtieneEditItemsUIDFromPath(ruta)

                For Each row As DataRow In lstArray.Rows
                    NegocioParam.InsertAutoIRecord(NombreForm, row.Field(Of String)(0), row.Field(Of String)(1), row.Field(Of String)(2), row.Field(Of String)(3))
                Next

                'se limpia la lista para añadir nuevos items
                lstArray.Clear()

                'la lista recibe todos los edittext
                lstArray = ObtieneEditNItemsUIDFromPath(ruta)

                For Each row As DataRow In lstArray.Rows
                    NegocioParam.InsertAutoIRecord(NombreForm, row.Field(Of String)(0), row.Field(Of String)(1), row.Field(Of String)(2), row.Field(Of String)(3))
                Next

                'se limpia la lista para añadir nuevos items
                lstArray.Clear()

                lstArray = ObtieneComboUIDFromPath(ruta)
                For Each row As DataRow In lstArray.Rows
                    NegocioParam.InsertAutoIRecord(NombreForm, row.Field(Of String)(0), row.Field(Of String)(1), row.Field(Of String)(2), row.Field(Of String)(3))
                Next

                Return True
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

    End Class
End Namespace



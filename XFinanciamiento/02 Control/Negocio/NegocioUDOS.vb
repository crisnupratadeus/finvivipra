﻿Imports XFinanciamiento.Data.Datos

Namespace Control.Negocio
    Public Class NegocioUDOS

        Public Shared Function VerificarUDOs(ByVal NombreUDO As String) As String

            Return DatosUDOS.VerificaUDOS(NombreUDO)

        End Function

        Public Shared Sub CreaUDOAuto()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("AUTO") = -1 Then

                strPath = Application.StartupPath & "\UDO_F_AUTO.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "AUTO"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "Formulario de Autorización"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_AUTO"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "CreateDate"
                oUserObjectMD.FormColumns.FormColumnDescription = "CreateDate"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "UpdateDate"
                oUserObjectMD.FormColumns.FormColumnDescription = "UpdateDate"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "U_UsrCode"
                oUserObjectMD.FormColumns.FormColumnDescription = "Código Usuario"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "U_UsrName"
                oUserObjectMD.FormColumns.FormColumnDescription = "Nombre Usuario"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "U_AuthType"
                oUserObjectMD.FormColumns.FormColumnDescription = "Tipo Autorización"
                oUserObjectMD.FormColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If

                oUserObjectMD = Nothing

            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        Public Shared Sub CreaUDOVehu()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty


            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("VEHU") = -1 Then

                strPath = Application.StartupPath & "\SBOFormIns.srf" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = "VEHU"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Name = "Usados"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_VEHU"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                oUserObjectMD.FormColumns.Add()

                'Se declara la tabla de Lineas
                oUserObjectMD.ChildTables.TableName = "EXX_EHU1"
                oUserObjectMD.ChildTables.Add()
                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se introducen las columnas
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                oUserObjectMD.EnhancedFormColumns.Add()

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("VEHU") Then
                    strPath = Application.StartupPath & "\SBOFormIns.srf" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se setea el formulario modificado para que este por defecto
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        Public Shared Sub CreaUDOIVEH()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty


            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("IVEH") = -1 Then

                strPath = Application.StartupPath & "\UDO_F_IVEH.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = "IVEH"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Name = "Creación masiva de vehículos"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_IVEH"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                oUserObjectMD.FormColumns.Add()

                'Se declara la tabla de Lineas
                oUserObjectMD.ChildTables.TableName = "EXX_IVE1"
                oUserObjectMD.ChildTables.Add()
                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se introducen las columnas
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                oUserObjectMD.EnhancedFormColumns.Add()

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("IVEH") Then
                    strPath = Application.StartupPath & "\UDO_F_IVEH.xml" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se setea el formulario modificado para que este por defecto
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
            End If

            GC.Collect() 'Release the handle to the table
        End Sub


        Public Shared Sub CreaUDOREVA()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty


            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("REVA") = -1 Then

                strPath = Application.StartupPath & "\SBOFormReva.srf" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = "REVA"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Name = "Revalorización Vehículos"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_REVA"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                oUserObjectMD.FormColumns.Add()

                'Se declara la tabla de Lineas
                oUserObjectMD.ChildTables.TableName = "EXX_EVA1"
                oUserObjectMD.ChildTables.Add()
                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se introducen las columnas
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                oUserObjectMD.EnhancedFormColumns.Add()

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("REVA") Then
                    strPath = Application.StartupPath & "\SBOFormReva.srf" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se setea el formulario modificado para que este por defecto
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        Public Shared Sub CreaUDOGar()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("GARA") = -1 Then

                strPath = Application.StartupPath & "\UDO_F_VEHC.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "VEHC"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "Vehiculos"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData
                oUserObjectMD.TableName = "EXX_VEHC"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "Code"
                oUserObjectMD.FindColumns.ColumnDescription = "Code"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Name"
                oUserObjectMD.FindColumns.ColumnDescription = "Name"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("GARA") Then
                    strPath = Application.StartupPath & "\UDO_F_VEHC.xml" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se setea el formulario modificado para que este por defecto
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        Public Shared Sub CreaUDOFAMI()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs("FAMI") = -1 Then

                strPath = Application.StartupPath & "\UDO_F_FAMI.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "FAMI"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "EstiloVehiculos"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData
                oUserObjectMD.TableName = "EXX_FAMI"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "Code"
                oUserObjectMD.FindColumns.ColumnDescription = "Code"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Name"
                oUserObjectMD.FindColumns.ColumnDescription = "Name"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "U_Brand"
                oUserObjectMD.FindColumns.ColumnDescription = "U_Brand"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "U_Brand"
                oUserObjectMD.FormColumns.FormColumnDescription = "U_Brand"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing

            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        ''' <summary>
        ''' CREA UDOS Genericos
        ''' </summary>
        Public Shared Sub CreaUDOMaster(ByVal Codigo As String, ByVal Nombre As String, ByVal TablaCab As String, Optional ByVal TablaLin As String = "", Optional ByVal TablaLog As String = "")
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String

            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs(Codigo) = -1 Then

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = Codigo
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = Nombre
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData
                oUserObjectMD.TableName = TablaCab
                'oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                'oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Code"
                oUserObjectMD.FindColumns.ColumnDescription = "Code"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Name"
                oUserObjectMD.FindColumns.ColumnDescription = "Name"
                oUserObjectMD.FindColumns.Add()

                'Se verifica si existira una tabla de lineas asociada al UDO
                If TablaLin <> "" Then
                    oUserObjectMD.ChildTables.TableName = TablaLin
                End If

                'Se verifica si existira una tabla de LOG para el UDO
                If TablaLog <> "" Then
                    oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.LogTableName = TablaLog
                End If

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()
                If TablaLin <> "" Then
                    oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES

                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_ActCode"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "Codigo"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                    oUserObjectMD.EnhancedFormColumns.Add()

                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_StartDate"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "StartDate"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 2
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                    oUserObjectMD.EnhancedFormColumns.Add()

                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "U_EndDate"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "EndDate"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 3
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1
                    oUserObjectMD.EnhancedFormColumns.Add()
                End If

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing

            End If

            GC.Collect() 'Release the handle to the table

        End Sub
        Public Shared Sub CreaUDODocument(ByVal Codigo As String, ByVal Nombre As String, ByVal TablaCab As String, Optional ByVal TablaLin As String = "")
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String

            'Se verifica que el UDO no exista
            If NegocioUDOS.VerificarUDOs(Codigo) = -1 Then

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                If TablaLin <> "" Then
                    oUserObjectMD.ChildTables.TableName = TablaLin
                End If
                oUserObjectMD.Code = Codigo
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = Nombre
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = TablaCab

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                Else
                    MsgBox("UDO: " & oUserObjectMD.Name & " fue añadido correctamente")
                End If

                oUserObjectMD = Nothing

            End If

            GC.Collect() 'Release the handle to the table

        End Sub
        Public Shared Sub CreaUDOModelosAut()
            Try
                GC.Collect()
                Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
                Dim lRetCode As Integer
                Dim sErrMsg As String
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String = String.Empty

                'Se verifica que el UDO no exista
                If NegocioUDOS.VerificarUDOs("AUTMOD") = "" Then

                    strPath = Application.StartupPath & "\UDO_F_AUTOMOD.xml" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se introducen las propiedades del UDO
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                    oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Code = "AUTMOD"
                    oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Name = "Modelos Autorización"
                    oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                    oUserObjectMD.TableName = "EXX_AUTOMOD"
                    oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tNO

                    'Se introduce el campo de busqueda original
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Form"
                    oUserObjectMD.FindColumns.ColumnDescription = "Formulario"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Nombre"
                    oUserObjectMD.FindColumns.ColumnDescription = "Nombre Etapa"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Activo"
                    oUserObjectMD.FindColumns.ColumnDescription = "Activo"
                    oUserObjectMD.FindColumns.Add()

                    'Se crea una tabla de LOG
                    oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                    'Se crea el formulario default con las columnas basicas
                    oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                    oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_Form"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Formulario"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.ChildTables.TableName = "EXX_AUTOMOE"
                    oUserObjectMD.ChildTables.Add()

                    oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                    oUserObjectMD.EnhancedFormColumns.Add()
                    oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

                    oUserObjectMD.ChildTables.TableName = "EXX_AUTOMOC"
                    oUserObjectMD.ChildTables.Add()

                    oUserObjectMD.EnhancedFormColumns.Add()
                    oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 2

                    oUserObjectMD.EnhancedFormColumns.Add()


                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    Dim aux As String = oUserObjectMD.GetAsXML
                    lRetCode = oUserObjectMD.Add()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObjectMD)

                Else
                    'Se prepara a modificar el UDO existente
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                    If oUserObjectMD.GetByKey("AUTMOD") Then

                        strPath = Application.StartupPath & "\UDO_F_AUTOMOD.xml"
                        xmlDoc.Load(strPath)
                        oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                        lRetCode = oUserObjectMD.Update()

                        '// check for errors in the process
                        If lRetCode <> 0 Then
                            DiApp.GetLastError(lRetCode, sErrMsg)
                            MsgBox(sErrMsg)
                            Exit Sub
                        End If
                    End If
                    oUserObjectMD = Nothing
                End If

                GC.Collect() 'Release the handle to the table
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CreaUDOEtapasAut()
            Try
                GC.Collect()
                Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
                Dim lRetCode As Integer
                Dim sErrMsg As String
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String = String.Empty

                'Se verifica que el UDO no exista
                If NegocioUDOS.VerificarUDOs("AUTETAP") = "" Then

                    strPath = Application.StartupPath & "\UDO_F_AUTOETAP.xml" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se introducen las propiedades del UDO
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                    oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Code = "AUTETAP"
                    oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Name = "Etapas Autorización"
                    oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                    oUserObjectMD.TableName = "EXX_AUTOETAP"
                    oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tNO

                    'Se introduce el campo de busqueda original
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Nombre"
                    oUserObjectMD.FindColumns.ColumnDescription = "Nombre Etapa"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Descr"
                    oUserObjectMD.FindColumns.ColumnDescription = "Descripción Etapa"
                    oUserObjectMD.FindColumns.Add()

                    'Se crea una tabla de LOG
                    oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                    'Se crea el formulario default con las columnas basicas
                    oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                    oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_Nombre"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Nombre Etapa"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_Descr"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Descripción Etapa"
                    oUserObjectMD.FormColumns.Add()


                    oUserObjectMD.FormColumns.FormColumnAlias = "U_NroA"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Nro. Autorización"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_NroR"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Nro. Rechazo"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.ChildTables.TableName = "EXX_AUTOETAL"
                    oUserObjectMD.ChildTables.Add()

                    oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                    oUserObjectMD.EnhancedFormColumns.Add()
                    oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

                    oUserObjectMD.EnhancedFormColumns.Add()

                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    Dim aux As String = oUserObjectMD.GetAsXML
                    lRetCode = oUserObjectMD.Add()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObjectMD)

                Else
                    'Se prepara a modificar el UDO existente
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                    If oUserObjectMD.GetByKey("AUTETAP") Then

                        strPath = Application.StartupPath & "\UDO_F_AUTOETAP.xml"
                        xmlDoc.Load(strPath)
                        oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                        lRetCode = oUserObjectMD.Update()

                        '// check for errors in the process
                        If lRetCode <> 0 Then
                            DiApp.GetLastError(lRetCode, sErrMsg)
                            MsgBox(sErrMsg)
                            Exit Sub
                        End If
                    End If
                    oUserObjectMD = Nothing
                End If

                GC.Collect() 'Release the handle to the table
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CreaUDOCondicionesAut()
            Try
                GC.Collect()
                Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
                Dim lRetCode As Integer
                Dim sErrMsg As String
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String = String.Empty

                'Se verifica que el UDO no exista
                If NegocioUDOS.VerificarUDOs("AUTCOND") = "" Then

                    strPath = Application.StartupPath & "\UDO_F_AUTOCOND.xml" 'se pasa la ruta del formulario
                    xmlDoc.Load(strPath) 'se carga el fomulario

                    'Se introducen las propiedades del UDO
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                    oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Code = "AUTCOND"
                    oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                    oUserObjectMD.Name = "Modelos Autorización"
                    oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                    oUserObjectMD.TableName = "EXX_AUTOCON"
                    oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tNO

                    'Se introduce el campo de busqueda original
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Form"
                    oUserObjectMD.FindColumns.ColumnDescription = "Formulario"
                    oUserObjectMD.FindColumns.Add()

                    oUserObjectMD.FindColumns.ColumnAlias = "U_Funcion"
                    oUserObjectMD.FindColumns.ColumnDescription = "Funcion Tabla"
                    oUserObjectMD.FindColumns.Add()

                    'Se crea una tabla de LOG
                    oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                    'Se crea el formulario default con las columnas basicas
                    oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                    oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_Form"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Formulario"
                    oUserObjectMD.FormColumns.Add()

                    oUserObjectMD.FormColumns.FormColumnAlias = "U_Funcion"
                    oUserObjectMD.FormColumns.FormColumnDescription = "Funcion Tabla"
                    oUserObjectMD.FormColumns.Add()


                    oUserObjectMD.ChildTables.TableName = "EXX_AUTOCOL"
                    oUserObjectMD.ChildTables.Add()

                    oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                    oUserObjectMD.EnhancedFormColumns.Add()
                    oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                    oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                    oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

                    oUserObjectMD.EnhancedFormColumns.Add()

                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    'Se crea el UDO
                    Dim aux As String = oUserObjectMD.GetAsXML
                    lRetCode = oUserObjectMD.Add()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObjectMD)

                Else
                    'Se prepara a modificar el UDO existente
                    oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                    If oUserObjectMD.GetByKey("AUTCOND") Then

                        strPath = Application.StartupPath & "\UDO_F_AUTOCOND.xml"
                        xmlDoc.Load(strPath)
                        oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                        lRetCode = oUserObjectMD.Update()

                        '// check for errors in the process
                        If lRetCode <> 0 Then
                            DiApp.GetLastError(lRetCode, sErrMsg)
                            MsgBox(sErrMsg)
                            Exit Sub
                        End If
                    End If
                    oUserObjectMD = Nothing
                End If

                GC.Collect() 'Release the handle to the table
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CreaUDOLC()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista.
            If NegocioUDOS.VerificarUDOs("LICRE") = "" Then

                strPath = Application.StartupPath & "\UDO_FT_LICRE.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "LICRE"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "LineaCredito"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_MasterData
                oUserObjectMD.TableName = "EXX_LICRE"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Code"
                oUserObjectMD.FindColumns.ColumnDescription = "Code"
                oUserObjectMD.FindColumns.Add()
                oUserObjectMD.FindColumns.ColumnAlias = "Name"
                oUserObjectMD.FindColumns.ColumnDescription = "Name"
                oUserObjectMD.FindColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
                'oUserObjectMD.FindColumns.ColumnDescription = "Socio de Negocio"
                'oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
                'oUserObjectMD.FindColumns.ColumnDescription = "Socio de Negocio"
                'oUserObjectMD.FindColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("LICRE") Then

                    strPath = Application.StartupPath & "\UDO_FT_LICRE.xml"
                    xmlDoc.Load(strPath)
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
                oUserObjectMD = Nothing
            End If

            GC.Collect() 'Release the handle to the table
        End Sub
        Public Shared Sub CreaUDOCuotas()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista.
            If NegocioUDOS.VerificarUDOs("CUOTA") = "" Then

                strPath = Application.StartupPath & "\UDO_CUOTA.srf" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO 
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "CUOTA"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "Cuotas"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_CUOTA"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()


                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("CUOTA") Then

                    strPath = Application.StartupPath & "\UDO_CUOTA.srf"
                    xmlDoc.Load(strPath)
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
                oUserObjectMD = Nothing
            End If

            GC.Collect() 'Release the handle to the table
        End Sub
        Public Shared Sub CreaUDOFIPLA()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista.
            If NegocioUDOS.VerificarUDOs("FIPLA") = "" Then

                strPath = Application.StartupPath & "\UDO_FT_FIPLA.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = "FIPLA"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Name = "PlantillaReqGar"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_FIPLAN"


                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                'oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                'oUserObjectMD.FindColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
                'oUserObjectMD.FindColumns.ColumnDescription = "Socio de Negocio"
                'oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()

                'oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                'oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                'oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                'oUserObjectMD.FormColumns.Add()
                'oUserObjectMD.FindColumns.ColumnAlias = "U_CardCode"
                'oUserObjectMD.FindColumns.ColumnDescription = "Socio de Negocio"
                'oUserObjectMD.FindColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES


                oUserObjectMD.ChildTables.TableName = "EXX_FIPLA1"
                oUserObjectMD.ChildTables.Add()

                oUserObjectMD.EnhancedFormColumns.Add()
                oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

                oUserObjectMD.EnhancedFormColumns.Add()

                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("FIPLA") Then
                    strPath = Application.StartupPath & "\UDO_FT_FIPLA.xml"
                    xmlDoc.Load(strPath)
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
                oUserObjectMD = Nothing
            End If

            GC.Collect() 'Release the handle to the table
        End Sub
        Public Shared Sub CrearUDOIntereses()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista.
            If NegocioUDOS.VerificarUDOs("INTERESES_GEN") = "" Then

                strPath = Application.StartupPath & "\UDO_FT_INTERES.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Code = "INTERESES_GEN"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.Name = "Intereses Generados"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_INTERESES"

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.FindColumns.Add()

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Add()

                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.ChildTables.TableName = "EXX_FACT1"
                oUserObjectMD.ChildTables.Add()

                oUserObjectMD.EnhancedFormColumns.Add()
                oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 1

                oUserObjectMD.ChildTables.TableName = "EXX_CUOT1"
                oUserObjectMD.ChildTables.Add()

                oUserObjectMD.EnhancedFormColumns.Add()
                oUserObjectMD.EnhancedFormColumns.SetCurrentLine(0)
                oUserObjectMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                oUserObjectMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.EnhancedFormColumns.ColumnNumber = 1
                oUserObjectMD.EnhancedFormColumns.ChildNumber = 2

                oUserObjectMD.EnhancedFormColumns.Add()

                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("INTERESES_GEN") Then
                    strPath = Application.StartupPath & "\UDO_FT_INTERES.xml"
                    xmlDoc.Load(strPath)
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
                oUserObjectMD = Nothing
            End If

            GC.Collect() 'Release the handle to the table
        End Sub

        Public Shared Sub CreaUDORefinanciamiento()
            GC.Collect()
            Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
            Dim lRetCode As Integer
            Dim sErrMsg As String
            Dim xmlDoc As New Xml.XmlDocument
            Dim strPath As String = String.Empty

            'Se verifica que el UDO no exista.
            If NegocioUDOS.VerificarUDOs("REFIN") = "" Then

                strPath = Application.StartupPath & "\UDO_REFIN.srf" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario

                'Se introducen las propiedades del UDO 
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Code = "REFIN"
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.Name = "Refinanciamiento"
                oUserObjectMD.ObjectType = SAPbobsCOM.BoUDOObjType.boud_Document
                oUserObjectMD.TableName = "EXX_REFIN"
                oUserObjectMD.UseUniqueFormType = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.OverwriteDllfile = SAPbobsCOM.BoYesNoEnum.tYES

                'Se introduce el campo de busqueda original
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.FindColumns.ColumnAlias = "DocEntry"
                oUserObjectMD.FindColumns.ColumnDescription = "DocEntry"

                'Se crea una tabla de LOG
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES

                'Se crea el formulario default con las columnas basicas
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.FormColumns.FormColumnAlias = "DocEntry"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocEntry"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "DocNum"
                oUserObjectMD.FormColumns.FormColumnDescription = "DocNum"
                oUserObjectMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.FormColumns.Add()


                oUserObjectMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO

                'Se setea el formulario modificado para que este por defecto
                oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                'Se crea el UDO
                lRetCode = oUserObjectMD.Add()

                '// check for errors in the process
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lRetCode, sErrMsg)
                    MsgBox(sErrMsg)
                End If

                oUserObjectMD = Nothing
            Else
                'Se prepara a modificar el UDO existente
                oUserObjectMD = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)

                If oUserObjectMD.GetByKey("REFIN") Then

                    strPath = Application.StartupPath & "\UDO_REFIN.srf"
                    xmlDoc.Load(strPath)
                    oUserObjectMD.FormSRF = xmlDoc.InnerXml.ToString

                    lRetCode = oUserObjectMD.Update()

                    '// check for errors in the process
                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lRetCode, sErrMsg)
                        MsgBox(sErrMsg)
                        Exit Sub
                    End If
                End If
                oUserObjectMD = Nothing
            End If

            GC.Collect() 'Release the handle to the table
        End Sub

    End Class
End Namespace



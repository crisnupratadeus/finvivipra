﻿Imports SAPbobsCOM
Imports System.Transactions
Imports XFinanciamiento.Data

Namespace Control

    Public Class NegocioParam

        Public Shared Function GetGruposAutorizacionUser(usuario As String) As DataTable
            Try
                Return DatosParam.GetGruposAutorizacionUser(usuario)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetItemsAutorizacion(Formulario As String) As DataTable
            Try
                Return DatosParam.GetItemsAutorizacion(Formulario)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetUDOAutorizacion() As DataTable
            Try
                Return DatosParam.GetUDOAutorizacion()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetManufacturers() As DataTable
            Try
                Return DatosParam.GetManufacturers()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetManufacturersLst(Optional usuario As String = "") As DataTable
            Try
                Return DatosParam.GetManufacturersLst(usuario)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetManufacturersStk(Optional usuario As String = "") As DataTable
            Try
                Return DatosParam.GetManufacturersStk(usuario)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetConceptosV() As DataTable
            Try
                Return DatosParam.GetConceptosV()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetBranchsUsr(ByVal Usr As String) As DataTable
            Try
                Return DatosParam.GetBranchsUsr(Usr)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetItmGroups() As DataTable
            Try
                Return DatosParam.GetItmGroups()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetMarcaMotor() As DataTable
            Try
                Return DatosParam.GetMarcaMotor()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetCabina() As DataTable
            Try
                Return DatosParam.GetCabina()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstVen() As DataTable
            Try
                Return DatosParam.GetEstVen()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstVenStk() As DataTable
            Try
                Return DatosParam.GetEstVenStk()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstVenLst() As DataTable
            Try
                Return DatosParam.GetEstVenLst()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstPat() As DataTable
            Try
                Return DatosParam.GetEstPat()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstVeh() As DataTable
            Try
                Return DatosParam.GetEstVeh()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetColorE() As DataTable
            Try
                Return DatosParam.GetColorE()
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function GetColorI() As DataTable
            Try
                Return DatosParam.GetColorI()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetTecho() As DataTable
            Try
                Return DatosParam.GetTecho()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetTipVen() As DataTable
            Try
                Return DatosParam.GetTipVen()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetCombustible() As DataTable
            Try
                Return DatosParam.GetCombustible()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetTraccion() As DataTable
            Try
                Return DatosParam.GetTraccion()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetTransmision() As DataTable
            Try
                Return DatosParam.GetTransmision()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetCategorias() As DataTable
            Try
                Return DatosParam.GetCategorias()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetNormas() As DataTable
            Try
                Return DatosParam.GetNormas()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetForms(Addon As String) As DataTable
            Try
                Return DatosParam.GetForms(Addon)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetModelosLst(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                If Not Marca = Nothing Then
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelosLst(Marca, Estilo)
                    Else
                        Return DatosParam.GetModelosLst(Marca)
                    End If
                Else
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelosLst(, Estilo)
                    Else
                        Return DatosParam.GetModelos()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetModelosStk(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                If Not Marca = Nothing Then
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelosStk(Marca, Estilo)
                    Else
                        Return DatosParam.GetModelosStk(Marca)
                    End If
                Else
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelosStk(, Estilo)
                    Else
                        Return DatosParam.GetModelos()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetModelos(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                If Not Marca = Nothing Then
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelos(Marca, Estilo)
                    Else
                        Return DatosParam.GetModelos(Marca)
                    End If
                Else
                    If Not Estilo = Nothing Then
                        Return DatosParam.GetModelos(, Estilo)
                    Else
                        Return DatosParam.GetModelos()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetVehProp() As DataTable
            Try
                Return DatosParam.GetVehProp()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetCuentasReva(Concepto As String) As DataTable
            Try
                Return DatosParam.GetCuentasReva(Concepto)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetSeriesReva() As DataTable
            Try
                Return DatosParam.GetSeriesReva()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetSeries(Sucursal As String) As DataTable
            Try
                Return DatosParam.GetSeries(Sucursal)
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function GetDiasVen() As Integer
            Try
                Return DatosParam.GetDiasVen()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstilos(Marca As String) As DataTable
            Try
                Return DatosParam.GetEstilos(Marca)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstilosLst(Marca As String) As DataTable
            Try
                Return DatosParam.GetEstilosLst(Marca)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetEstilosStk(Marca As String) As DataTable
            Try
                Return DatosParam.GetEstilosStk(Marca)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetPriceLists() As DataTable
            Try
                Return DatosParam.GetPriceLists()
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function LoadOACT_UDF() As DataTable
            Try
                Return DatosParam.LoadOACT_UDF()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadJDT1_UDF() As DataTable
            Try
                Return DatosParam.LoadJDT1_UDF()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadOITM_UDF() As DataTable
            Try
                Return DatosParam.LoadOITM_UDF()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetPerfilesUser(usuario As String, Form As String) As DataTable
            Try
                Return DatosParam.GetPerfilesUser(usuario, Form)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadODIM() As DataTable
            Try
                Return DatosParam.LoadODIM()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getCodeUserTable(tableName As String) As String
            Try
                Return DatosParam.getCodeUserTable(tableName)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getRateImpMar(Impuesto As String) As Double
            Dim rate As Double
            Dim valor As String = String.Empty

            Try
                valor = DatosParam.getRateImpMar(Trim(Impuesto))

                If Double.TryParse(valor, rate) Then
                    Return rate
                Else
                    Return 0
                End If

            Catch ex As Exception
                Throw ex
                Return 0
            End Try
        End Function


        Public Shared Function getEsImpMar() As String
            Try
                Return DatosParam.getEsImpMar()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getImpMar() As String
            Try
                Return DatosParam.getImpMar()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getSeriesUsados() As String
            Try
                Return DatosParam.getSeriesUsados()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub InsertUAutoRecord(UDO As String, GrpAuto As String, Item As String)
            Try
                DatosParam.InsertUAutoRecord(UDO, GrpAuto, Item)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub InsertPmarca(usuario As String, marca As String)
            Try
                DatosParam.InsertPmarca(usuario, marca)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub InsertPAutoRecord(UDO As String, GrpAuto As String, Item As String, Descripcion As String)
            Try
                DatosParam.InsertPAutoRecord(UDO, GrpAuto, Item, Descripcion)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub InsertAutoIRecord(UDO As String, Item As String, Desc As String, DBAlias As String, Type As String)
            Try
                DatosParam.InsertAutoIRecord(UDO, Item, Desc, DBAlias, Type)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub UpdateParameter(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "")

            Try
                DatosParam.UpdateParameter(Type, valor1, valor2, valor3, valor4, valor5, valor6)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub EliminaUAutoRecord(UDO As String, GrpAuto As String, Item As String)
            Try
                DatosParam.EliminaUAutoRecord(UDO, GrpAuto, Item)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub EliminaPAutoRecord(UDO As String, GrpAuto As String, Item As String)
            Try
                DatosParam.EliminaPAutoRecord(UDO, GrpAuto, Item)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub EliminaPMarca(usuario As String, marca As String)
            Try
                DatosParam.EliminaPMarca(usuario, marca)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub BorraConcepto(Concepto As String)
            Try
                DatosParam.BorraConcepto(Concepto)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub UpdateParameterReva(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "", Optional valor7 As String = "", Optional valor8 As String = "", Optional valor9 As String = "", Optional valor10 As String = "")

            Try
                DatosParam.UpdateParameterReva(Type, valor1, valor2, valor3, valor4, valor5, valor6, valor7, valor8, valor9, valor10)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function GetFilesGar(codeLC As String, CodigoPC As String, NumDoc As String) As DataTable
            Try
                Return DatosParam.CargaDatos(codeLC, CodigoPC, NumDoc)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class

End Namespace

﻿Namespace Data
    Public Class clsSQL

#Region "Consultas SAP - SQL SERVER"

        Public con As Conexiones = New Conexiones
        Public Function RescataVersionSQL() As Integer

            Dim texto As String = con.obtenerValor("SELECT @@VERSION", True)
            Dim arr As Array = texto.Split(" ")

            Return Val(arr(3))

        End Function

#End Region


    End Class
End Namespace



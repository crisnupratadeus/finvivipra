﻿Imports XFinanciamiento.Data.Datos

Namespace Data


    Public Class NegocioTablasUser

        ''' <summary>
        ''' VERIFICA SI EISTE CAMPO
        ''' </summary>
        Public Shared Function Verifica_Existe_Campo(ByVal tabla As String, ByVal Campo As String) As Integer

            Return DatosTablasUser.Verifica_Existe_Campo(tabla, Campo)

        End Function

        ''' <summary>
        ''' RESCATA LA ULTIMA SECUENCIA INSERTADA
        ''' </summary>
        Public Shared Function Rescata_ultima_Secuencia(ByVal tabla As String) As Integer

            Return DatosTablasUser.Rescata_ultima_Secuencia(tabla)

        End Function

        ''' <summary>
        ''' VERIFICA TABLAS DE USUARIOS
        ''' </summary>
        Public Shared Function Verificatabla(ByVal Tabla As String) As Boolean
            GC.Collect()
            Dim existe As Boolean = False
            Dim tab As SAPbobsCOM.UserTablesMD
            Dim i As Integer
            Dim msg As String = String.Empty
            Application.DoEvents()

            tab = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)
            existe = tab.GetByKey(Tabla)
            tab = Nothing
            GC.Collect()
            Return existe
        End Function

        Public Shared Sub AnexaTablaMaestra(ByVal Addon As String, ByVal Code As String, ByVal Name As String, ByVal Activa As Boolean)
            Dim oUT As SAPbobsCOM.UserTable
            Dim res As Integer = 0
            Dim sErrMsg As String = String.Empty
            Try
                oUT = DiApp.UserTables.Item("EXX_GABL")
                oUT.Code = Code
                oUT.Name = Name
                oUT.UserFields.Fields.Item("U_Addon").Value = Addon
                If Activa = True Then
                    oUT.UserFields.Fields.Item("U_Activa").Value = "Y"
                Else
                    oUT.UserFields.Fields.Item("U_Activa").Value = "N"
                End If

                res = oUT.Add()
                If res <> 0 Then
                    DiApp.GetLastError(res, sErrMsg)
                    MsgBox(sErrMsg)
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End Sub


        ''' <summary>
        ''' CREA TABLAS DE USUARIOS
        ''' </summary>
        Public Shared Sub Creatabla(ByVal Tabla As String, ByVal Descripcion As String, ByVal sistema As String, Optional ByVal tipo As String = "NO", Optional CreaAnexoMaestro As Boolean = False)
            GC.Collect()
            Dim tab As SAPbobsCOM.UserTablesMD
            Dim i As Integer
            Dim msg As String = String.Empty
            Application.DoEvents()

            tab = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)
            'Se verifica que la tabla no exista
            If tab.GetByKey(Tabla) = False Then

                'Se setea los datos de la tabla
                tab.TableName = Tabla
                tab.TableDescription = Descripcion

                'Se define el tipo de tabla que sera en base a los siguientes strings
                If tipo = "M" Then
                    tab.TableType = SAPbobsCOM.BoUTBTableType.bott_MasterData
                ElseIf tipo = "D" Then
                    tab.TableType = SAPbobsCOM.BoUTBTableType.bott_Document
                ElseIf tipo = "DL" Then
                    tab.TableType = SAPbobsCOM.BoUTBTableType.bott_DocumentLines
                ElseIf tipo = "ML" Then
                    tab.TableType = SAPbobsCOM.BoUTBTableType.bott_MasterDataLines
                ElseIf tipo = "NO" Then
                    tab.TableType = SAPbobsCOM.BoUTBTableType.bott_NoObject
                End If

                Try
                    i = tab.Add()
                    If i <> 0 Then
                        DiApp.GetLastError(i, msg)
                        MessageBox.Show("Error al crear tabla: " + msg, "Creación de Tablas", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    Else
                        If CreaAnexoMaestro = True Then
                            Try
                                AnexaTablaMaestra("XFinanciamiento", Tabla, Descripcion, True)
                            Catch ex As Exception
                                Throw ex
                                Exit Sub
                            End Try
                        End If
                    End If
                Catch ex As Exception
                    Exit Sub
                End Try
            End If
            tab = Nothing
            GC.Collect()
        End Sub

        ''' <summary>
        ''' ELIMINA TABLAS DE USUARIOS
        ''' </summary>
        Public Shared Sub Eliminatabla(ByVal Tabla As String)
            GC.Collect()
            Dim tab As SAPbobsCOM.UserTablesMD
            Dim i As Integer
            Dim msg As String = String.Empty
            Application.DoEvents()

            tab = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)
            If tab.GetByKey(Tabla) Then
                tab.Remove()
            End If
            tab = Nothing
            GC.Collect()
        End Sub
        ''' <summary>
        ''' CREA CAMPOS DE USUARIOS A TABLAS DE USUARIOS
        ''' </summary>
        Public Shared Sub CreaCampo(ByVal tabla As String,
                                    ByVal campo As String,
                                    ByVal descripcion As String,
                                    ByVal type As SAPbobsCOM.BoFieldTypes,
                                    ByVal _default As String,
                                    ByVal Size As Integer,
                                    Optional ByVal subtype As SAPbobsCOM.BoFldSubTypes = SAPbobsCOM.BoFldSubTypes.st_None,
                                    Optional ByVal mandatorio As String = "N",
                                    Optional ByVal Tabla_UDO As String = "",
                                    Optional ByVal Nativo As String = "",
                                    Optional ByVal Tabla_Lnk As String = "",
                                    Optional ByVal LinkedSysObj As Integer = -1)
            GC.Collect()
            Dim i As Integer
            Dim sErr As String = ""
            Dim msg As String = ""

            Dim Campos As SAPbobsCOM.UserFieldsMD
            Application.DoEvents()
            Campos = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)

            If Nativo <> "" Then
                If NegocioTablasUser.Verifica_Existe_Campo(tabla, campo) = -1 Then
                    If Campos.GetByKey(tabla, NegocioTablasUser.Rescata_ultima_Secuencia(tabla) + 1) = False Then
                        'Se setean las propiedades del campo
                        Campos.TableName = tabla
                        Campos.Name = campo
                        Campos.Description = descripcion

                        'Se setea el tipo de campo que se adicionara
                        Campos.Type = type
                        Select Case type
                            Case SAPbobsCOM.BoFieldTypes.db_Alpha
                                Campos.EditSize = Size
                                Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                                Campos.DefaultValue = _default

                            Case SAPbobsCOM.BoFieldTypes.db_Float
                                Campos.EditSize = Size
                                Campos.DefaultValue = _default
                                Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_Quantity

                            Case SAPbobsCOM.BoFieldTypes.db_Memo
                                Campos.DefaultValue = _default

                            Case Else
                                Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None

                        End Select

                        'Se setea el subtipo del campo que se adicionara
                        If subtype <> SAPbobsCOM.BoFldSubTypes.st_None Then
                            Campos.SubType = subtype
                        Else
                            Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None
                        End If

                        'Se setea la tabla UDO a la que se deberia relacionar el campo
                        If Tabla_UDO <> "" Then
                            Campos.LinkedUDO = Tabla_UDO
                        End If

                        'Se setea la condicion de mandatorio para el campo dependiendo el parametro
                        If mandatorio = "Y" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                        ElseIf mandatorio = "N" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        Else
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        End If

                        'se setea la tabla linkeada de ser necesario
                        If Tabla_Lnk <> "" Then
                            Campos.LinkedTable = Tabla_Lnk
                        End If
                        If LinkedSysObj <> -1 Then
                            Campos.LinkedSystemObject = LinkedSysObj
                        End If
                        Try
                            i = Campos.Add
                            If i <> 0 Then
                                'Dim str As String
                                DiApp.GetLastError(i, sErr)
                                MessageBox.Show("Imposible crear campo " & campo & " en " & tabla, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'resp = False
                            End If
                        Catch EXX As Exception
                            MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & "." & EXX.Message, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'resp = False
                        End Try
                    End If
                End If
            Else
                If NegocioTablasUser.Verifica_Existe_Campo("@" & tabla, campo) = -1 Then
                    If Campos.GetByKey("@" & tabla, NegocioTablasUser.Rescata_ultima_Secuencia("@" & tabla) + 1) = False Then
                        'Se setean las propiedades del campo
                        Campos.TableName = tabla
                        Campos.Name = campo
                        Campos.Description = descripcion

                        'Se setea el tipo de campo que se adicionara
                        Campos.Type = type
                        Select Case type
                            Case SAPbobsCOM.BoFieldTypes.db_Alpha
                                Campos.EditSize = Size
                                Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                                Campos.DefaultValue = _default

                            Case SAPbobsCOM.BoFieldTypes.db_Float
                                Campos.EditSize = Size
                                Campos.DefaultValue = _default
                                Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_Quantity

                            Case SAPbobsCOM.BoFieldTypes.db_Memo
                                Campos.DefaultValue = _default

                            Case Else
                                Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None

                        End Select

                        'Se setea el subtipo del campo que se adicionara
                        If subtype <> SAPbobsCOM.BoFldSubTypes.st_None Then
                            Campos.SubType = subtype
                        Else
                            Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None
                        End If

                        'Se setea la tabla UDO a la que se deberia relacionar el campo
                        If Tabla_UDO <> "" Then
                            Campos.LinkedUDO = Tabla_UDO
                        End If

                        'Se setea la condicion de mandatorio para el campo dependiendo el parametro
                        If mandatorio = "Y" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                        ElseIf mandatorio = "N" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        Else
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        End If

                        'se setea la tabla linkeada de ser necesario
                        If Tabla_Lnk <> "" Then
                            Campos.LinkedTable = Tabla_Lnk
                        End If
                        If LinkedSysObj <> -1 Then
                            Campos.LinkedSystemObject = LinkedSysObj
                        End If
                        Try
                            i = Campos.Add
                            If i <> 0 Then
                                'Dim str As String
                                DiApp.GetLastError(i, sErr)
                                MessageBox.Show("Imposible crear campo " & campo & " en " & tabla, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'resp = False
                            End If
                        Catch EXX As Exception
                            MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & "." & EXX.Message, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'resp = False
                        End Try
                    End If
                End If

            End If


            Campos = Nothing
            GC.Collect()
        End Sub

        ''' <summary>
        ''' CREA CAMPOS DE USUARIOS A TABLAS DE USUARIOS CON VALORES
        ''' </summary>
        Public Shared Sub CreaCampoConValores(ByVal tabla As String, ByVal campo As String, ByVal descripcion As String,
                                              ByVal type As SAPbobsCOM.BoFieldTypes, ByVal _default As String, ByVal Size As Integer,
                                              ByVal hasValores As Hashtable, Optional ByVal mandatorio As String = "N", Optional ByVal Nativo As String = "N", Optional UDOLinked As String = "")
            GC.Collect()
            Dim i As Integer
            Dim sErr As String = ""
            Dim msg As String = ""

            Dim Campos As SAPbobsCOM.UserFieldsMD
            Application.DoEvents()
            Campos = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)

            If Nativo = "Y" Then

                If NegocioTablasUser.Verifica_Existe_Campo(tabla, campo) = -1 Then
                    If Campos.GetByKey(tabla, NegocioTablasUser.Rescata_ultima_Secuencia(tabla) + 1) = False Then

                        'Se setean las propiedades del campo a adicionar
                        Campos.TableName = tabla
                        Campos.Name = campo
                        Campos.Description = descripcion
                        Campos.Type = type

                        'Se verifica el tipo del campo y se añaden los valores por defecto
                        Select Case type
                            Case SAPbobsCOM.BoFieldTypes.db_Alpha
                                Campos.EditSize = Size
                                Campos.DefaultValue = _default
                                'Se añaden los valores validos para el campo
                                For Each elemento As DictionaryEntry In hasValores
                                    Campos.ValidValues.Value = elemento.Key
                                    Campos.ValidValues.Description = elemento.Value
                                    Campos.ValidValues.Add()
                                Next

                        End Select

                        'Se verifica si el campo sera mandatorio
                        Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None
                        If mandatorio = "Y" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                        ElseIf mandatorio = "N" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        Else
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        End If
                        'Se verifica si el campo tendra asociado un UDO
                        If UDOLinked <> "" Then
                            Campos.LinkedUDO = UDOLinked
                        End If

                        Try
                            i = Campos.Add
                            If i <> 0 Then
                                'Dim str As String
                                DiApp.GetLastError(i, sErr)
                                MessageBox.Show("Imposible crear campo " & campo & " en " & tabla, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'resp = False
                            End If
                        Catch EXX As Exception
                            MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & "." & EXX.Message, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'resp = False
                        End Try
                    End If
                End If
            Else
                If NegocioTablasUser.Verifica_Existe_Campo("@" & tabla, campo) = -1 Then
                    If Campos.GetByKey("@" & tabla, NegocioTablasUser.Rescata_ultima_Secuencia("@" & tabla) + 1) = False Then

                        'Se setean las propiedades del campo a adicionar
                        Campos.TableName = tabla
                        Campos.Name = campo
                        Campos.Description = descripcion
                        Campos.Type = type

                        'Se verifica el tipo del campo y se añaden los valores por defecto
                        Select Case type
                            Case SAPbobsCOM.BoFieldTypes.db_Alpha
                                Campos.EditSize = Size
                                Campos.DefaultValue = _default
                                'Se añaden los valores validos para el campo
                                For Each elemento As DictionaryEntry In hasValores
                                    Campos.ValidValues.Value = elemento.Key
                                    Campos.ValidValues.Description = elemento.Value
                                    Campos.ValidValues.Add()
                                Next

                        End Select

                        'Se verifica si el campo sera mandatorio
                        Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None
                        If mandatorio = "Y" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES
                        ElseIf mandatorio = "N" Then
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        Else
                            Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO
                        End If

                        Try
                            i = Campos.Add
                            If i <> 0 Then
                                'Dim str As String
                                DiApp.GetLastError(i, sErr)
                                MessageBox.Show("Imposible crear campo " & campo & " en " & tabla, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'resp = False
                            End If
                        Catch EXX As Exception
                            MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & "." & EXX.Message, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'resp = False
                        End Try
                    End If
                End If
            End If

            Campos = Nothing
            GC.Collect()
        End Sub
        Public Shared Sub CreaCamposLinked(ByVal tabla As String, ByVal campo As String, ByVal descripcion As String,
                                      ByVal type As SAPbobsCOM.BoFieldTypes, ByVal _default As String, ByVal Size As Integer,
                                      ByVal hasValores As Hashtable, Optional UDOLinked As String = "", Optional NoEsTablaNativa As Boolean = False)
            GC.Collect()
            Dim i As Integer
            Dim sErr As String = ""
            Dim msg As String = ""

            Dim Campos As SAPbobsCOM.UserFieldsMD
            Application.DoEvents()
            Campos = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)
            Dim TablaAux As String
            TablaAux = tabla
            If NoEsTablaNativa = True Then
                TablaAux = "@" & tabla
            End If
            If NegocioTablasUser.Verifica_Existe_Campo(TablaAux, campo) = -1 Then

                If Campos.GetByKey(tabla, NegocioTablasUser.Rescata_ultima_Secuencia(tabla) + 1) = False Then

                    Campos.TableName = tabla
                    Campos.Name = campo
                    Campos.Description = descripcion
                    Campos.Type = type

                    Select Case type

                        Case SAPbobsCOM.BoFieldTypes.db_Alpha
                            Campos.EditSize = Size
                            Campos.DefaultValue = _default

                            For Each elemento As DictionaryEntry In hasValores
                                Campos.ValidValues.Value = elemento.Key
                                Campos.ValidValues.Description = elemento.Value
                                Campos.ValidValues.Add()
                            Next
                        Case SAPbobsCOM.BoFieldTypes.db_Numeric
                            Campos.EditSize = Size
                    End Select


                    Campos.SubType = SAPbobsCOM.BoFldSubTypes.st_None
                    Campos.Mandatory = SAPbobsCOM.BoYesNoEnum.tNO

                    If UDOLinked <> "" Then
                        Campos.LinkedUDO = UDOLinked
                    End If

                    Try
                        i = Campos.Add
                        If i <> 0 Then
                            'Dim str As String
                            DiApp.GetLastError(i, sErr)
                            MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & ": " & sErr, "Creación de campos. ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'resp = False
                        End If
                    Catch EXX As Exception
                        MessageBox.Show("Imposible crear campo " & campo & " en " & tabla & "." & EXX.Message, "Creación de campos", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'resp = False
                    End Try
                End If
            End If
            Campos = Nothing
            GC.Collect()
        End Sub

        ''' <summary>
        ''' ELIMINA SP DE USUARIOS
        ''' </summary>
        Public Shared Sub EliminaSP(ByVal nombreSp As String)

            DatosTablasUser.EliminaSP(nombreSp)

        End Sub

        Public Shared Sub EliminaFX(ByVal nombreSp As String)

            DatosTablasUser.EliminaFX(nombreSp)

        End Sub
        Public Shared Sub EliminaTF(ByVal nombreTf As String)

            DatosTablasUser.EliminaTF(nombreTf)

        End Sub

        ''' <summary>
        ''' VERIFICA SP
        ''' </summary>
        Public Shared Function VerificaSP(ByVal nombreSp As String, ByRef tip As String) As Boolean

            Return DatosTablasUser.VerificaSP(nombreSp, tip)

        End Function
        Public Shared Function VerificaTF(ByVal nombreTf As String) As Boolean

            Return DatosTablasUser.VerificaTF(nombreTf)

        End Function

        ''' <summary>
        ''' CREA SP SEGUN TEXTO ENTREGADO
        ''' </summary>
        Public Shared Function CreateSP(ByVal SpText As String) As Boolean

            Return DatosTablasUser.CreateSP(SpText)

        End Function

        Public Shared Function VerificaTType(ByVal nombreSp As String) As Boolean

            Return DatosTablasUser.VerificaTType(nombreSp)

        End Function

        Public Shared Sub EliminaTType(ByVal nombreSp As String)

            DatosTablasUser.EliminaTType(nombreSp)

        End Sub
        Public Shared Function CreateTType(ByVal SpText As String) As Boolean

            Return DatosTablasUser.CreateTType(SpText)

        End Function

        ''' <summary>
        ''' LEE USUSARIOS CONECTADOS A BD
        ''' </summary>
        Public Shared Function LeeUsuariosConectadosDB() As Hashtable
            Try

                Return DatosTablasUser.LeeUsuariosConectadosDB()

            Catch ex As Exception
                Throw ex
            End Try

        End Function


    End Class

End Namespace

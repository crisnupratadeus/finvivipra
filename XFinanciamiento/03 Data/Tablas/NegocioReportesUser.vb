﻿Imports XFinanciamiento.Data.Datos

Namespace Data
    Public Class NegocioReportesUser

        Public Shared Function VerificaReporte(ByVal Reporte As String) As Boolean
            Dim rptTypeService As SAPbobsCOM.ReportTypesService
            Dim ReportTypeParams As SAPbobsCOM.ReportTypesParams

            Try
                GC.Collect()
                'Get a ReportTypesService object  
                rptTypeService = DiApp.GetCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ReportTypesService)

                'First check if the link already exist  
                ReportTypeParams = rptTypeService.GetReportTypeList()

                For Each rpt As SAPbobsCOM.ReportTypeParams In ReportTypeParams                   
                    If Trim(rpt.TypeName) = Reporte Then
                        Return True
                        Exit For
                    End If
                Next
                Return False
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End Function

        Public Shared Function CreaNuevoTipoRpt(MenuId As String, TypeName As String, AddonName As String, AddonFormType As String) As Boolean

            Dim rptTypeService As SAPbobsCOM.ReportTypesService
            Dim newType As SAPbobsCOM.ReportType
            Dim LinkExist As Boolean = False
            Dim newTypeParam As SAPbobsCOM.ReportTypeParams

            Try
                GC.Collect()
                'Get a ReportTypesService object  
                rptTypeService = DiApp.GetCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ReportTypesService)

                If Not VerificaReporte(TypeName) Then

                    'creamos uno nuevo tipo
                    newType = rptTypeService.GetDataInterface(SAPbobsCOM.ReportTypesServiceDataInterfaces.rtsReportType)
                    newType.TypeName = TypeName
                    newType.AddonName = AddonName
                    newType.AddonFormType = AddonFormType
                    newType.MenuID = MenuId
                    Try
                        newTypeParam = rptTypeService.AddReportType(newType)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                        Return False
                    End Try
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return False
            End Try
        End Function
        Public Shared Function RelacionaRptForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Nombre As String) As Boolean
            Dim rptTypeService As SAPbobsCOM.ReportTypesService
            Dim ReportTypeParams As SAPbobsCOM.ReportTypesParams
            Dim CodigoRpt As String = ""

            Try
                'Get a ReportTypesService object  
                rptTypeService = DiApp.GetCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ReportTypesService)

                'First check if the link already exist  
                ReportTypeParams = rptTypeService.GetReportTypeList()

                For Each rpt As SAPbobsCOM.ReportTypeParams In ReportTypeParams
                    If Trim(rpt.TypeName) = Nombre Then
                        CodigoRpt = rpt.TypeCode
                        Exit For
                    End If
                Next

                If CodigoRpt = "" Then
                    Return False
                Else
                    oForm.ReportType = "A001" ' CodigoRpt
                    Return True
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function
    End Class
End Namespace



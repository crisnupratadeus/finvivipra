﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml
Imports System.Globalization

Namespace Data
    Public Class DatosDB
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable
        Public Shared Function GetParam(Name As String) As String
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlData.GetParam_TEXT(Name)
                Else
                    ostr = hanaData.GetParam_TEXT(Name)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function UpdateParameter(Type As String, valor1 As String, Optional valor2 As String = "",
                                              Optional valor3 As String = "", Optional valor4 As String = "",
                                              Optional valor5 As String = "", Optional valor6 As String = "") As String
            Try
                Dim ostr As String

                Dim sSqlCommand As String
                Dim sSqlInsert As String
                If valor2 <> "" Then
                    If Motor = "SQL" Then
                        sSqlCommand = sqlData.sSqlCommandDatosDB1_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                    Else
                        sSqlCommand = hanaData.sSqlCommandDatosDB1_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                    End If
                Else
                    If Motor = "SQL" Then
                        sSqlCommand = sqlData.sSqlCommandDatosDB2_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                    Else
                        sSqlCommand = hanaData.sSqlCommandDatosDB2_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                    End If
                End If
                If Motor = "SQL" Then
                    sSqlInsert = sqlData.SqlInsertDatosDB_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                Else
                    sSqlInsert = hanaData.SqlInsertDatosDB_TEXT(Type, valor1, valor2, valor3, valor4, valor5, valor6)
                End If

                conexi.creaRegistro(sSqlCommand, False)
                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function PopulaGridChecklist(DocEntry As String) As String

            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = sqlData.PopulaGridChecklist(DocEntry)
                Else
                    ostr = hanaData.PopulaGridChecklist(DocEntry)
                End If


                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function PopulaGridTasas(DocEntry As String) As String 'ssh

            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = sqlData.PopulaGridTasas(DocEntry)
                Else
                    ostr = hanaData.PopulaGridTasas(DocEntry)
                End If


                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function InsertaChecklist(CodigoLC As String, CodChk As String, DesChk As String, Realizado As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return sqlData.InsertaChecklist(Trim(CodigoLC), Trim(CodChk), Trim(DesChk), Trim(Realizado))

                Else
                    Return hanaData.InsertaChecklist(Trim(CodigoLC), Trim(CodChk), Trim(DesChk), Trim(Realizado))

                End If

            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        ' ssh
        Public Shared Function LimpiarTasasGrupArt(CodigoLC As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return sqlData.LimpiarTasasGrupArt(Trim(CodigoLC))

                Else
                    Return hanaData.LimpiarTasasGrupArt(Trim(CodigoLC))

                End If

            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        'ssh
        Public Shared Function InsertaTasasGrupArt(CodigoLC As String, CodGrupArt As String, TasaNor As String, TasaLeg As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return sqlData.InsertaTasasGrupArt(Trim(CodigoLC), Trim(CodGrupArt), Trim(TasaNor), Trim(TasaLeg))

                Else
                    Return hanaData.InsertaTasasGrupArt(Trim(CodigoLC), Trim(CodGrupArt), Trim(TasaNor), Trim(TasaLeg))

                End If

            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function GetDescGrupoArt(CodGrupArt As String) As String
            Dim DescGrupoArt As String
            Try
                If Motor = "SQL" Then
                    DescGrupoArt = sqlData.GetDescGrupoArt(Trim(CodGrupArt))

                Else
                    DescGrupoArt = hanaData.GetDescGrupoArt(Trim(CodGrupArt))

                End If
                Return conexi.obtenerValor(DescGrupoArt)
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function UpdateChecklist(CodeTBL As String, Realizado As String) As Boolean
            Try
                If Motor = "SQL" Then

                    Return sqlData.UpdateChecklist(Trim(CodeTBL), Trim(Realizado))
                Else
                    Return hanaData.UpdateChecklist(Trim(CodeTBL), Trim(Realizado))

                End If



            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function GetEstadosCmb(Formulario As String) As DataTable

            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = sqlData.GetEstadosCmb(Formulario)
                Else

                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
        Public Shared Function PopulaFest(Formulario As String) As DataTable
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.PopulaFest(Formulario)
                Else
                    ostr = HanaAutorizaciones.PopulaFest(Formulario)
                End If

                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
        Public Shared Function GetGruposAutorizacion() As DataTable
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlParamAutori.GetGruposAutorizacion_TEXT
                Else
                    ostr = hanaParamAutori.GetGruposAutorizacion_TEXT
                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function PerteneceGrupoAutorizacion(ByRef oApp As SAPbouiCOM.Application, CodUsuario As String, Param As String) As Boolean
            Try
                Dim GrupoReset As String = String.Empty
                Dim oDT As DataTable
                Dim grpUsr As String = String.Empty

                GrupoReset = Trim(GetParam(Param))
                oDT = DatosAutorizaciones.GetGruposUsuario(Trim(CodUsuario))
                If Not oDT Is Nothing Then
                    If oDT.Rows.Count > 0 Then
                        For Each row As DataRow In oDT.Rows
                            grpUsr = row.Item("GRUPO")
                            If Trim(grpUsr) = Trim(GrupoReset) Then
                                Return True
                            End If
                        Next
                    Else
                        'oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                        Return False
                    End If
                Else
                    'oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                    Return False
                End If

                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBDS_IN(Valor As Decimal) As String
            Try
                'Valor = Math.Round(Valor, 9)
                Return Valor.ToString(CultureInfo.InvariantCulture).Replace(",", ".")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBDS_OUT(Valor As String) As Decimal
            Try
                Return Valor.Replace(".", ",")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ParseDecimalFormat(Valor As String) As Decimal
            Dim cantidadDecimal As Decimal

            Try
                cantidadDecimal = Convert.ToDecimal(Valor, CultureInfo.InvariantCulture)

            Catch ex As Exception

                Valor = Valor.Replace(",", "")
                cantidadDecimal = Decimal.Parse(Val(Valor))
                'cantidadDecimal = CDec(Val(Valor))  ALTERNATIVA EN CASO DE ERROR
            End Try

            Return cantidadDecimal

        End Function

        Public Shared Function UpdateFormDinamico(CodeLC As String, CodigoPC As String, Valor As String, Codigo As String) As Boolean
            Try
                Dim sSqlUpdate As String
                If Motor = "SQL" Then
                    sSqlUpdate = "UPDATE [@EXX_FIPLD1]	SET U_Valor = '" & Valor & "' WHERE U_CodeLineaCred = '" & CodeLC & "' AND U_Plantilla = '" & CodigoPC & "' AND U_Codigo = '" & Codigo & "'"
                Else
                    sSqlUpdate = "UPDATE """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"" SET ""U_Valor"" = '" & Valor & "' WHERE ""U_CodeLineaCred"" = '" & CodeLC & "' AND ""U_Plantilla"" = '" & CodigoPC & "' AND ""U_Codigo"" = '" & Codigo & "'" 'mauricio
                End If

                Try
                    conexi.creaRegistro(sSqlUpdate, False)
                Catch ex As Exception
                    Throw ex
                    Return False
                End Try

                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function InsertaCuotasInteres(DocEntryOV As String, FechaVencimiento As String, DocEntryPG As String, TotalCuota As String, CapitalxPagar As String, Amortizacion As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return sqlData.InsertaCuotaInteres(Trim(DocEntryOV), Trim(FechaVencimiento), Trim(DocEntryPG), Trim(TotalCuota), Trim(CapitalxPagar), Trim(Amortizacion))

                Else
                    Return hanaData.InsertaCuotaInteres(Trim(DocEntryOV), Trim(FechaVencimiento), Trim(DocEntryPG), Trim(TotalCuota), Trim(CapitalxPagar), Trim(Amortizacion))

                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function GetParamOV(DocEntryOV As String) As DataTable
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = sqlData.GetParamOV_TEXT(DocEntryOV)
                Else
                    ostr = hanaData.GetParamOV_TEXT(DocEntryOV)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function ValidaCodigoPlantilla(Codigo As String) As Integer
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = sqlData.ValidaCodigoPlantilla(Codigo)
                Else
                    ostr = hanaData.ValidaCodigoPlantilla(Codigo)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return -1
            End Try
        End Function
        Public Shared Function PopulaAlarmas(Formulario As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.PopulaAlarmas(Formulario)
                Else
                    ostr = HanaAutorizaciones.PopulaAlarmas(Formulario)
                End If
                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try

        End Function

        Public Shared Function GetProcedureCuotas(Cuotas As String, Tasa As String, Anticipo As String, Lapso As Integer, Original As Integer, CapitalTotal As String, FechaDoc As String, CapPago As Integer) As DataTable
            Try
                Dim query As String = String.Empty
                Return conexi.obtenerColeccion(query)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
        Public Shared Function GetParamCuotasFecha(DocEntryOV As String, DocEntryPG As String) As DataTable
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = sqlData.GetParamFechaCuota_TEXT(DocEntryOV, DocEntryPG)
                Else
                    ostr = hanaData.GetParamFechaCuota_TEXT(DocEntryOV, DocEntryPG)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetInfoLineasOV(DocEntryOV As String) As DataTable
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = sqlData.GetInfoLineasOV(DocEntryOV)
                Else
                    ostr = hanaData.GetInfoLineasOV(DocEntryOV)
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function
    End Class
End Namespace

﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class BDGarantia
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable
         Public Shared Function getDetalleGarantiaSQL(U_Codigo As String) As String
            Try
                Dim query As String = ""

                If Motor = "SQL" Then
                    query = sqlGarLinea.getDetalleGarantiaSQL(U_Codigo)
                Else
                    query = hanaGarLinea.getDetalleGarantiaSQL(U_Codigo)
                End If
                Return query

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function getListaFipla1(DocEntry As String, Operacion As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlGarLinea.getListaFipla1(DocEntry, Operacion)
                Else
                    query = hanaGarLinea.getListaFipla1(DocEntry, Operacion)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function insertaDetalleGarFIPLD1(U_Plantilla As String, U_Valor As String, U_CodeLineaCred As String, U_CodeDoc As String, U_TipoDoc As String) As Boolean

            Dim sSqlInsert As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    sSqlInsert = sqlGarLinea.insertaDetalleGarFIPLD1(U_Plantilla, U_Valor, U_CodeLineaCred, U_CodeDoc, U_TipoDoc)
                Else
                    sSqlInsert = hanaGarLinea.insertaDetalleGarFIPLD1(U_Plantilla, U_Valor, U_CodeLineaCred, U_CodeDoc, U_TipoDoc)
                End If

                Return conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

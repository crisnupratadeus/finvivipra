﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class BDGarLinea
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable

        Public Shared Function ListaBrachBD() As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    'query = sqlLineaCredito.getBranchBdSQL()

                Else
                    'query = hanaLineaCredito.getBranchBdHANA()

                End If
                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetAnexosLineaDeCredito(objeto As String, nroDoc As String) As Object
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    'ostr = sqlData.GetAnexosLineaDeCredito(objeto,nroDoc)

                Else
                    'ostr = hanaData.GetAnexosLineaDeCredito(objeto,nroDoc)

                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetMaxCodeLinCred() As DataTable
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    'ostr = sqlData.GetMaxCodeLinCred()
                Else
                    'ostr = hanaData.GetMaxCodeLinCred()
                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetParam(Name As String) As String
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    'ostr = sqlData.GetParam_TEXT(Name)
                Else
                    'ostr = hanaData.GetParam_TEXT(Name)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class
End Namespace

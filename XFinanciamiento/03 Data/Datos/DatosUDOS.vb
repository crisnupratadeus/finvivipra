﻿Imports System.Data.SqlClient
Imports XFinanciamiento.Data

Namespace Data.Datos
    Public Class DatosUDOS
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable

        ''' <summary>
        ''' VERIDICA SI EL CAMPO EXISTE
        ''' </summary>

        Public Shared Function VerificaUDOS(ByVal NombreUDO As String) As String
            Dim sSqlQuery As String = String.Empty
            Dim valor As String
            Try
                If Motor = "SQL" Then
                    sSqlQuery = " SELECT ISNULL(Code, '-1') FROM OUDO WHERE Code = '" & NombreUDO & "'"
                Else
                    sSqlQuery = "SELECT IFNULL(""Code"",'-1') FROM """ & SQLBaseDatos.ToUpper & """.""OUDO"" WHERE ""Code"" = '" & NombreUDO & "'" 'CNPHANA
                End If

                valor = conexi.obtenerValor(sSqlQuery, False)

                Return valor

            Catch ex As Exception
                Return "-2"
            End Try


        End Function

    End Class
End Namespace



﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class BDCuotas
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable


        Public Shared Function GetCuotas(DocEntry As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlCuotasLineas.getCuotasLineas(DocEntry)
                Else
                    query = hanaGarLinea.getCuotasLineas(DocEntry)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetExxCuota(docEntryOV As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlCuotasLineas.getCuotasCabecera(docEntryOV)
                Else
                    query = hanaGarLinea.getCuotasCabecera(docEntryOV)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetExxLicre(SocioNegocio As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlLineaCredito.GetRegistroLiCre(SocioNegocio)
                Else
                    query = hanaLineaCredito.GetRegistroLiCre(SocioNegocio)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetExxLicre2(ItemCode As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlLineaCredito.GetRegistroLiCre2(ItemCode)
                Else
                    query = hanaLineaCredito.GetRegistroLiCre2(ItemCode)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class BDContratos
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable
        Public Shared Function GetRate(sDate As String) As Double
            Try

                Dim ostr As String
                ostr = "select Rate from ORTT WITH (NOLOCK) where Currency='" & MonedaSis & "' and rateDate='" & sDate & "'"
                Dim dRes As Double = conexi.obtenerValor(ostr)
                If MonedaLocal = MonedaSis Then
                    dRes = 1
                End If
                If Trim(Convert.ToString(dRes)) = "" Then
                    dRes = 0
                End If
                Return dRes
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getCodeUserTable(tableName As String) As String
            Try
                Dim ostr As String
                ostr = "SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) from " & tableName
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CuentaDeSocio(Socio As String) As String
            Try
                Dim ostr As String
                ostr = "select DebPayAcct from ocrd WITH (NOLOCK) where CardCode='" & Socio & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GrupoDeSocio(Socio As String) As String
            Try
                Dim ostr As String
                ostr = "select GroupCode from ocrd WITH (NOLOCK) where CardCode='" & Socio & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Descripcion_modelo(ItemCode As String) As String
            Try
                Dim ostr As String
                ostr = "select ItemName from OITM WITH (NOLOCK) where ItemCode='" & ItemCode & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getTotalLocal(Moneda As String, Monto As Double, Fecha As String, Optional RateOptional As Decimal = 0) As Double
            Try
                Dim ostr As String
                Dim res As Double
                Dim srate As String
                Dim rate As Double

                If MonedaLocal = Moneda Then
                    res = Monto
                Else
                    ostr = "select Rate from ORTT WITH (NOLOCK) where RateDate='" & Fecha & "' and Currency='" & Moneda & "'"
                    srate = conexi.obtenerValor(ostr)
                    If Not srate Is Nothing Then
                        rate = srate.Replace(".", sSepDecimal)                        
                        res = rate * Monto
                        If RateOptional <> 0 Then
                            res = RateOptional * Monto
                        End If
                    Else
                        res = 0

                    End If

                End If

                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_Monto_ME_de_Monto_en_ML(Moneda As String, Monto As Double, Fecha As String) As Double
            Try
                Dim ostr As String
                Dim res As Double
                Dim srate As String
                Dim rate As Double
                ostr = "select Rate from ORTT WITH (NOLOCK) where RateDate='" & Fecha & "' and Currency='" & Moneda & "'"
                srate = conexi.obtenerValor(ostr)
                If Not srate Is Nothing Then
                    rate = srate.Replace(".", sSepDecimal)
                    res = Monto / rate
                Else
                    res = 0
                End If

                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getTotalSystem(Moneda As String, Monto As Double, Fecha As String, Optional RateOptional As Decimal = 0) As Double
            Try
                Dim ostr As String
                Dim res As Double
                Dim srate As String
                Dim rate As Double

                If MonedaSis = Moneda Then
                    res = Monto
                ElseIf MonedaLocal = Moneda Then
                    ostr = "select Rate from ORTT WITH (NOLOCK) where RateDate='" & Fecha & "' and Currency='" & MonedaSis & "'"
                    srate = conexi.obtenerValor(ostr)
                    If Not srate Is Nothing Then
                        rate = srate.Replace(".", sSepDecimal)
                        res = Monto / rate

                        If RateOptional <> 0 Then
                            res = Monto / RateOptional
                        End If

                    Else
                        res = 0
                    End If

                Else
                    ostr = "select Rate from ORTT WITH (NOLOCK) where RateDate='" & Fecha & "' and Currency='" & Moneda & "'"
                    srate = conexi.obtenerValor(ostr)
                    If Not srate Is Nothing Then
                        rate = srate.Replace(".", sSepDecimal)
                        res = rate * Monto

                        If RateOptional <> 0 Then
                            res = RateOptional * Monto
                        End If

                    Else
                        res = 0
                    End If
                    ostr = "select Rate from ORTT WITH (NOLOCK) where RateDate='" & Fecha & "' and Currency='" & MonedaSis & "'"
                    srate = conexi.obtenerValor(ostr)
                    If Not srate Is Nothing Then
                        rate = srate.Replace(".", sSepDecimal)
                        res = res / rate

                        If RateOptional <> 0 Then
                            res = RateOptional / rate
                        End If
                    Else
                        res = 0
                    End If

                End If

                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getSlpCode() As String
            Try
                Dim ostr As String
                Dim userIK As String = DiApp.UserSignature
                Dim res As String
                ostr = "select salesprson from OHEM WITH (NOLOCK) where userId='" & userIK & "'"
                res = conexi.obtenerValor(ostr)
                If res = "" Then
                    res = "-1"
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getMenuIDContratos() As String
            Try
                Dim ostr As String
                Dim userIK As String = DiApp.UserSignature
                Dim res As String
                ostr = "select top 1 U_Value1 from [@EXX_CDPARAM]  WITH (NOLOCK)"
                res = conexi.obtenerValor(ostr)
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getNextDocentry() As String
            Try
                Dim ostr As String
                Dim res As String
                ostr = "select isnull(max(DocEntry)+1,1) from [@CNTT]"
                res = conexi.obtenerValor(ostr)
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getSlpList() As DataTable
            Try
                Dim ostr As String
                ostr = "select slpCode, slpName from OSLP WITH (NOLOCK) order by 2"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getGroupList() As DataTable
            Try
                Dim ostr As String
                ostr = "select GroupCode,GroupName from OCRG WITH (NOLOCK) where GroupType='C'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function BranchList() As DataTable
            Try
                Dim ostr As String
                Dim userIK As String = DiApp.UserName
                If Motor = "SQL" Then
                    ostr = "select BPLId, BPLName from OBPL WITH (NOLOCK) where Disabled='N' and BPLId in(select BPLId from USR6 where UserCode='" & userIK & "')"
                Else
                    ostr = "select ""BPLId"", ""BPLName"" from """ & SQLBaseDatos.ToUpper & """.OBPL where ""Disabled"" = 'N' and ""BPLId"" in (select ""BPLId"" from """ & SQLBaseDatos.ToUpper & """.USR6 where ""UserCode"" = '" & userIK & "')"
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function OldBranchList() As DataTable
            Try
                Dim ostr As String
                Dim userIK As String = DiApp.UserName
                ostr = "select Code,Name from OUBR WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetPayTermsList() As DataTable
            Try
                Dim ostr As String
                ostr = "select  GroupNum,PymntGroup from octg WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetCurrencyPayTerm() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT CurrCode, CurrName from OCRN WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetEntidadFinanciera() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT Code, Name from [@EXX_ENFI] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetMarcas() As DataTable
            Try
                Dim ostr As String
                ostr = "select FirmCode,FirmName from [OMRC] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetFamilia() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_FAMI] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTransmision() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_TRAN] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetColor() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_COLE] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ExistCotizacion(docentry As String) As Boolean
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select count(Docentry) from OQUT WITH (NOLOCK) where DocStatus<>'C' and U_CntrtId='" & docentry & "'"
                ostr = conexi.obtenerValor(ostr)
                If Convert.ToInt32(ostr) > 0 Then
                    res = True
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Existe_patente(Patente As String) As Boolean
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "SELECT Code from [@EXX_VEHC] where Code='" & Trim(Patente) & "'"
                ostr = conexi.obtenerValor(ostr)
                If ostr <> "" Then
                    res = True
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ExisteSerie(SerieCode As String, ItemCode As String) As Boolean
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select count(DistNumber) from OSRN WITH (NOLOCK) where ItemCode='" & ItemCode & "' and DistNumber='" & SerieCode & "'"
                ostr = conexi.obtenerValor(ostr)
                If Convert.ToInt32(ostr) > 0 Then
                    res = True
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Shared Function AlmacenEmpleadoVentas(EmpVentas As String) As String
        '    Try
        '        Dim ostr As String
        '        Dim res As Boolean = False
        '        ostr = "select WhsCode from OWHS where U_Branch=( " & vbCrLf & _
        '        "select top 1 B.Name from OHEM O  " & vbCrLf & _
        '        "inner join OUSR C on O.userId=C.INTERNAL_K " & vbCrLf & _
        '        "inner join OUBR B on C.Branch=B.Code " & vbCrLf & _
        '        " where O.salesPrson='" & EmpVentas & "')"
        '        ostr = conexi.obtenerValor(ostr)
        '        Return ostr                
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Shared Function AlmacenEmpleadoVentas(EmpVentas As String, tipo_venta As String, marca As String) As String
            Try
                Dim ostr As String
                Dim es_gestion_por_almacen As String = Trim(GetParam("SaleWare"))
                Dim EsUnAlmacenPorMarcaYPorTipoVentaEnCadSucursal As String = Trim(GetParam("UsaMarca"))
                If es_gestion_por_almacen <> "Y" Then
                    ostr = "select WhsCode from OWHS where U_Branch=( " & vbCrLf & _
                                    "select top 1 B.Name from OHEM O  " & vbCrLf & _
                                    "inner join OUSR C on O.userId=C.INTERNAL_K " & vbCrLf & _
                                    "inner join OUBR B on C.Branch=B.Code " & vbCrLf & _
                                    " where O.salesPrson='" & EmpVentas & "')"
                Else
                    If EsUnAlmacenPorMarcaYPorTipoVentaEnCadSucursal <> "2" Then
                        ostr = "select WhsCode from OWHS where U_Branch=( " & vbCrLf & _
                        "select top 1 B.Name from OHEM O  " & vbCrLf & _
                        "inner join OUSR C on O.userId=C.INTERNAL_K " & vbCrLf & _
                        "inner join OUBR B on C.Branch=B.Code " & vbCrLf & _
                        " where O.salesPrson='" & EmpVentas & "') and U_SaleTypeWh='" & tipo_venta & "'"
                    Else
                        marca = GetDescripcionMarca(marca)
                        ostr = "select WhsCode from OWHS where U_Branch=( " & vbCrLf & _
                        "select top 1 B.Name from OHEM O  " & vbCrLf & _
                        "inner join OUSR C on O.userId=C.INTERNAL_K " & vbCrLf & _
                        "inner join OUBR B on C.Branch=B.Code " & vbCrLf & _
                        " where O.salesPrson='" & EmpVentas & "') and U_SaleTypeWh='" & tipo_venta & "' and U_Brand='" & Trim(marca) & "'"
                    End If

                End If

                ostr = conexi.obtenerValor(ostr)
                Return ostr
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function TipoVenta_Almacen(Almacen As String) As String
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "Select U_SaleTypeWh from OWHS where WhsCode='" & Almacen & "'"
                ostr = conexi.obtenerValor(ostr)

                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function AlmacenDeSerie(SerieCode As String, ItemCode As String) As String
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select A.WhsCode as WhsCode,A.Quantity as Quantity from OSRQ A inner join OSRI B " & _
                "on (A.ItemCode = B.ItemCode AND A.SysNumber = B.SysSerial) " & _
                "where A.ItemCode = '" & ItemCode & "' and B.IntrSerial = '" & SerieCode & "' and A.Quantity > 0"
                ostr = conexi.obtenerValor(ostr)

                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Cuenta_SocioNegocio(SN As String) As String
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select DebPayAcct from OCRD where CardCode='" & SN & "'"
                ostr = conexi.obtenerValor(ostr)

                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDescripcionMarca(MarcaCode As String) As String
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select FirmName from ""OMRC"" WITH (NOLOCK) where FirmCode='" & Trim(MarcaCode) & "'"
                ostr = conexi.obtenerValor(ostr)

                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function AntiguoDueno(BoxPatent As String, itemCode As String) As String
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select isnull(U_ProvCod,'') from [@EXX_VEHC] WITH (NOLOCK) " & _
                " where U_Model='" & itemCode & "' and code='" & BoxPatent & "'"
                ostr = conexi.obtenerValor(ostr)
                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GrupoAutorizacion(UserId As String, GrupoA As String) As String
            Try
                Dim ostr As String = String.Empty
                Dim res As Boolean = False
                ostr = "SELECT GroupId from USR7 where UserId='" & UserId & "' and GroupId='" & GrupoA & "'"
                ostr = conexi.obtenerValor(ostr)
                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CostoDeSerie(SerieCode As String, ItemCode As String) As Double
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select CostTotal from OSRN WITH (NOLOCK) where ItemCode='" & ItemCode & "' and DistNumber='" & SerieCode & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ExistOV(docentry As String) As Boolean
            Try
                Dim ostr As String
                Dim res As Boolean = False
                ostr = "select count(Docentry) from ORDR WITH (NOLOCK) where DocStatus<>'C' and U_CntrtId='" & docentry & "'"
                ostr = conexi.obtenerValor(ostr)
                If Convert.ToInt32(ostr) > 0 Then
                    res = True
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetEstadoVentas() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLSS] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTipoVenta() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLST] WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetSeriesUDO() As DataTable
            Try
                Dim ostr As String
                ostr = "select Series, SeriesName from NNM1 WITH (NOLOCK) where ObjectCode='CNTT' and  Locked = 'N'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetSeriesObj(objectType As String) As DataTable
            Try
                Dim ostr As String
                ostr = "select Series, SeriesName from NNM1 WITH (NOLOCK) where ObjectCode='" & objectType & "' and  Locked = 'N' and DocSubType='--'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetGrupos() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT GroupCode, GroupName FROM OCRG "
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetGrupos_Autorizacion() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT GroupId,GroupName FROM OUGR"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function MonedasLcSys() As DataTable
            Try
                Dim ostr As String
                ostr = "select 'Local' as [Code],MainCurncy from OADM union all select 'System' as [Code],SysCurrncy from OADM"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetSeriesObj(objectType As String, subtype As String) As DataTable
            Try
                Dim ostr As String
                ostr = "select Series, SeriesName from NNM1 WITH (NOLOCK) where ObjectCode='" & objectType & "' and  Locked = 'N' and DocSubType='" & subtype & "'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function FacturaVehiculoAbierta(CntrId As String) As String
            Try
                Dim ostr As String
                ostr = "select top 1 DocStatus from OINV where U_CntrtId='" & CntrId & "' and DocStatus='C'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function TablaDocumentosRelacionados(query As String) As DataTable
            Try
                Dim ostr As String
                ostr = query
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DateToString(dDate As Date, Optional withHour As Boolean = False) As String
            Try
                Dim strDate As String = dDate.Year & Right(CStr("0" & dDate.Month), 2) & Right(CStr("0" & dDate.Day), 2)
                If withHour = True Then
                    strDate = " " & strDate & Right(CStr("0" & dDate.Hour), 2) & ":" & Right(CStr("0" & dDate.Minute), 2)
                End If

                Return strDate

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDIMs() As DataTable
            Try
                Dim ostr As String
                ostr = "select DimCode, DimDesc from odim WITH (NOLOCK) where DimActive='Y'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetImage(docEntry As String) As String
            Try
                Dim ostr As String
                ostr = "select top 1 U_path from [@EXX_IATT] WITH (NOLOCK) where U_ObjCode='VEHC' and U_DocCode='" & docEntry & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocTotalPorPago(docEntryPago As String, Contrato As String) As String
            Try
                Dim ostr As String
                ostr = "select DocTotal,DocTotalSy from [ORCT] WITH (NOLOCK) where U_CntrtId='" & Contrato & "' and docentry='" & docEntryPago & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocNumFactura(docEntry As String) As String
            Try
                Dim ostr As String
                ostr = "select DocNum from OINV WITH (NOLOCK) where DocEntry='" & docEntry & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocNum_Oferta_Venta(docEntry As String) As String
            Try
                Dim ostr As String
                ostr = "select DocNum from OQUT WITH (NOLOCK) where DocEntry='" & docEntry & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocNumOV(docEntry As String) As String
            Try
                Dim ostr As String
                ostr = "select DocNum from ORDR WITH (NOLOCK) where DocEntry='" & docEntry & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetBranchs() As DataTable
            Try
                Dim ostr As String
                ostr = "select slpCode, slpName from OSLP WITH (NOLOCK) order by 2"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetProperties() As DataTable
            Try
                Dim ostr As String
                ostr = "select ItmsTypCod,ItmsGrpNam from OITG WITH (NOLOCK) order by 1"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetUserBranchs(Usuario As String) As DataTable
            Try
                Dim ostr As String
                ostr = "select B.BPLId as Code, B.BPLName as Name from crd8 A inner join obpl B on A.BPLId = B.BPLId where CardCode = '" & Usuario & "' and B.Disabled = 'N'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function LoadOACT_UDF() As DataTable
            Try
                Dim ostr As String
                ostr = "select AliasID, Descr  from CUFD WITH (NOLOCK) where TableID='OACT'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function LoadODIM() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT DimCode, DimDesc FROM ODIM  WITH (NOLOCK)"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DfltWh(itemCode As String) As String
            Try
                Dim ostr As String
                ostr = "select isnull(DfltWH,'') from oitm  WITH (NOLOCK) where ItemCode='" & itemCode & "'"
                Dim WhsCode As String = conexi.obtenerValor(ostr)
                If Trim(WhsCode) = "" Then
                    ostr = "select isnull(DfltWhs,'') from OADM  WITH (NOLOCK)"
                    WhsCode = conexi.obtenerValor(ostr)
                End If
                Return Trim(WhsCode)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CurrencyLine(SN As String, itemCode As String) As String
            Try
                Dim res As String = ""
                If SN <> "" And itemCode <> "" Then
                    Dim ostr As String
                    ostr = "SELECT ListNum FROM OCRD  WITH (NOLOCK) where CardCode='" & SN & "'"
                    Dim sList As String = conexi.obtenerValor(ostr)
                    If Trim(sList) <> "" Then
                        ostr = "SELECT Currency  FROM ITM1 WITH (NOLOCK)  where ItemCode='" & itemCode & "' and PriceList='" & sList & "'"
                        res = conexi.obtenerValor(ostr)
                    End If
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function liberarUnidad(BoxPatent As String) As String
            Try
                Dim res As String = ""

                BDContratos.SetVehStatus(BoxPatent, "U_SaleSts", "1")

                'BDContratos.SetVehStatus(BoxPatent, "U_VehSts", "-2")
                'BDContratos.SetVehStatus(BoxPatent, "U_ResDate", "")
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function PaymentGroup(SN As String) As String
            Try
                Dim ostr As String
                ostr = "SELECT GroupNum FROM OCRD WITH (NOLOCK) where CardCode='" & SN & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function PostDate(Contrato As String) As String
            Try
                Dim ostr As String
                ostr = "select convert(varchar(4),Year(U_PostDate))+ " & _
                "right('00' + convert(varchar(2),month(U_PostDate)),2)+ " & _
                "right('00' + convert(varchar(2),day(U_PostDate)) ,2) " & _
                "from [@CNTT]  WITH (NOLOCK) where DocEntry='" & Contrato & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DocDate(Contrato As String) As String
            Try
                Dim ostr As String
                ostr = "select convert(varchar(4),Year(U_DocDate))+ " & _
                "right('00' + convert(varchar(2),month(U_DocDate)),2)+ " & _
                "right('00' + convert(varchar(2),day(U_DocDate)) ,2) " & _
                "from [@CNTT]  WITH (NOLOCK) where DocEntry='" & Contrato & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CampoStringCNTT(Contrato As String, CampoString As String) As String
            Try
                Dim ostr As String
                ostr = "select " & CampoString & _
                " from [@CNTT] WITH (NOLOCK) where DocEntry='" & Contrato & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTipoVentaPorContrato(Contrato As String) As String
            Try
                Dim ostr As String
                ostr = "select U_SaleType from [@CNTT] WITH (NOLOCK) where DocEntry='" & Contrato & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function PriceLine(SN As String, itemCode As String) As String
            Try
                Dim res As String = ""
                If SN <> "" And itemCode <> "" Then
                    Dim ostr As String
                    ostr = "SELECT ListNum FROM OCRD WITH (NOLOCK) where CardCode='" & SN & "'"
                    Dim sList As String = conexi.obtenerValor(ostr)
                    If Trim(sList) <> "" Then
                        ostr = "SELECT Price  FROM ITM1 WITH (NOLOCK)  where ItemCode='" & itemCode & "' and PriceList='" & sList & "'"
                        res = conexi.obtenerValor(ostr)
                    End If
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Public Shared Function TaxRate(TaxCode As String) As String
        '    Try
        '        Dim ostr As String
        '        ostr = "select Rate  from OSTC where Code='" & TaxCode & "'"
        '        Return conexi.obtenerValor(ostr)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Shared Sub LlenarDatosSLST()
            Try

                Dim ostr As String
                ostr = "select count(*) from [@EXX_SLST]"

                Dim res As String = conexi.obtenerValor(ostr)
                If Trim(res) = "0" Then
                    ostr = "insert into [@EXX_SLST](Code, Name) " & vbCrLf & _
                    "select '1','Vehículo transferido' union " & vbCrLf & _
                    "select '2','Vehículo no transferido' union " & vbCrLf & _
                    "select '3','Vehículo en consignación' union " & vbCrLf & _
                    "select '4','Vehículo nuevo' union " & vbCrLf & _
                    "select '6','Fact. Exenta' union " & vbCrLf & _
                    "select '7','Fact. Exportación' union " & vbCrLf & _
                    "select '8','Activo Fijo' "
                    conexi.creaRegistro(ostr)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function GetParam(Name As String) As String
            Try

                Dim ostr As String
                ostr = "select U_Value1 from [@EXX_CDPARAM] WITH (NOLOCK) where U_type='" & Name & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function tiene_todos_los_centros_de_costo(Optional costo1 As String = "", _
                                                                Optional costo2 As String = "", _
                                                                Optional costo3 As String = "", _
                                                                Optional costo4 As String = "", _
                                                                Optional costo5 As String = "" _
                                                                ) As String
            Try
                Dim lista_dimensiones As DataTable = GetDIMs()
                Dim res As String = ""
                For Each row As DataRow In lista_dimensiones.Rows
                    Dim codigo_dimension As String = Trim(Convert.ToString(row("DimCode")))
                    If codigo_dimension = "1" And Trim(costo1) = "" Then
                        res = res & Trim(Convert.ToString(row("DimDesc")))
                    End If
                    If codigo_dimension = "2" And Trim(costo2) = "" Then
                        res = res & " -  " & Trim(Convert.ToString(row("DimDesc")))
                    End If
                    If codigo_dimension = "3" And Trim(costo3) = "" Then
                        res = res & " -  " & Trim(Convert.ToString(row("DimDesc")))
                    End If
                    If codigo_dimension = "4" And Trim(costo4) = "" Then
                        res = res & " -  " & Trim(Convert.ToString(row("DimDesc")))
                    End If
                    If codigo_dimension = "5" And Trim(costo5) = "" Then
                        res = res & " -  " & Trim(Convert.ToString(row("DimDesc")))
                    End If
                Next
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub UpdateParameter(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "")
            Try

                Dim sSqlCommand As String
                Dim sSqlInsert As String
                If valor2 <> "" Then
                    sSqlCommand = "DELETE [@EXX_CDPARAM]  WHERE U_Type = '" & Type & "' and U_Value1='" & valor1 & "' and U_Value3='" & valor2 & "'"
                Else
                    sSqlCommand = "DELETE [@EXX_CDPARAM]  WHERE U_Type = '" & Type & "'" ' and U_Value1='" & valor1 & "'"
                End If
                sSqlInsert = "             INSERT INTO dbo.[@EXX_CDPARAM]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_Type,                                                                                            " + vbCrLf +
                           "              U_Value1,                                                                                           " + vbCrLf +
                           "              U_Value2,                                                                                            " + vbCrLf +
                           "              U_Active,                                                                                           " + vbCrLf +
                           "              U_UserName,                                                                                           " + vbCrLf +
                           "              U_CreateDate                                                                                           " + vbCrLf +
                            "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & Type & "', '" & valor1 & "', '" & valor2 & "'," & "1, '" & DiApp.UserName & "', '" & DateToString(Today) & "' from dbo.[@EXX_CDPARAM]"

                conexi.creaRegistro(sSqlCommand, False)
                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub UpdateParameterCampos(User As String, Form As String, Optional ItemUID As String = "")

            Try

                Dim sSqlInsert As String

                sSqlInsert = "             INSERT INTO dbo.[@EXX_CAMPOS]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_User,                                                                                            " + vbCrLf +
                           "              U_Form,                                                                                           " + vbCrLf +
                           "              U_ItemUID                                                                                            " + vbCrLf +
                            "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & User & "', '" & Form & "', '" & ItemUID & "' from dbo.[@EXX_CAMPOS]"


                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteParameterCampos(User As String, Optional PorCode As Boolean = False)

            Try

                Dim campo As String = "U_User"
                If PorCode = True Then
                    campo = "Code"
                End If
                Dim sSqlCommand As String
                sSqlCommand = "DELETE [@EXX_CAMPOS]  WHERE " & campo & " = '" & User & "'"
                conexi.creaRegistro(sSqlCommand, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Function DBDS_IN(Valor As Decimal) As String
            Try
                'Valor = Math.Round(Valor, 9)
                Return Valor.ToString.Replace(",", ".")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBDS_IN(Valor As Double, tamanioMax As Integer) As String
            Try
                Valor = Math.Round(Valor, tamanioMax)
                Return Valor.ToString.Replace(",", ".")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBDS_IN(Valor As String) As String
            Try

                Return Valor.Replace(",", ".")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBDS_OUT(Valor As String) As Decimal
            Try
                Return Valor.Replace(".", ",")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub SetCnttStatus(DocNum As String, Campo As String, valor As String)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService("CNTT")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("DocEntry", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, valor)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SetCnttStatus(DocNum As String, Campo As String, valor As Date)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService("CNTT")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("DocEntry", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, valor)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SetUDoDocumentStatus(UDOId As String, DocNum As String, Campo As String, valor As Date)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService(UDOId)
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("DocEntry", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, valor)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub SetUDoDocumentStatus(UDOId As String, DocNum As String, Campo As String, valor As String)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService(UDOId)
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("DocEntry", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, valor)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub CerrarOV(DocEntry As String)
            Try
                Dim oOV As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                oOV.GetByKey(DocEntry)
                oOV.Close()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SetVehStatus(DocNum As String, Campo As String, type As String)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService("VEHC")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("Code", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, type)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SetVehStatus(DocNum As String, Campo As String, type As Decimal)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService("VEHC")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("Code", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, DBDS_IN(type))
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SetVehStatus(DocNum As String, Campo As String, type As Date)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService
                sCmp = DiApp.GetCompanyService
                oGeneralService = sCmp.GetGeneralService("VEHC")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                oGeneralParams.SetProperty("Code", DocNum)
                oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                oGeneralData.SetProperty(Campo, type)
                oGeneralService.Update(oGeneralData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function TaxRate(TaxCode As String) As Double
            Try
                Dim ostr As String
                ostr = "select isnull(Rate,0) as [Rate]  from OSTC where Code='" & TaxCode & "'"
                Dim res As String = conexi.obtenerValor(ostr)
                If res = "" Then res = 0
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTaxCodePorTipoVenta(TipoVenta As String) As String
            Try
                Dim ostr As String
                Dim res As String = ""
                Select Case TipoVenta
                    Case "1"
                        res = GetParam("taxcode1")
                    Case "2"
                        res = GetParam("taxcode2")
                    Case "3"
                        res = "0"  'GetParam("TaxComi3")
                    Case "4"
                        res = GetParam("taxcode4")
                    Case "5"
                        res = GetParam("Tax4")
                    Case "6"
                        res = GetParam("taxcode6")
                    Case "7"
                        res = GetParam("taxcode7")
                    Case "8"
                        res = GetParam("taxcode8")
                End Select


                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function UltimaCotizacion(CntrtId As String, SN As String) As String
            Try
                Dim ostr As String
                ostr = "select Docentry from OQUT WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "' " & vbCrLf & _
                    "and docentry=(select max(docentry) from OQUT WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "' and DocStatus<>'C' and isnull(U_Closed,'')<>'Y')"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function TieneAutorizacion(CntrtId As String) As String
            Try
                Dim ostr As String
                ostr = "select Docentry from ODLN WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' " & vbCrLf & _
                    "and docentry=(select max(docentry) from ODLN WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and DocStatus<>'C')"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Shared Function OVPorContrato(CntrtId As String, SN As String) As String
            Try
                Dim ostr As String
                ostr = "select Docentry from ORDR WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "' " & vbCrLf & _
                    "and docentry=(select max(docentry) from ORDR WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "')"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function UltimaOV(CntrtId As String, SN As String) As String
            Try
                Dim ostr As String
                ostr = "select Docentry from ORDR WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "' " & vbCrLf & _
                    "and docentry=(select max(docentry) from ORDR WITH (NOLOCK) where U_CntrtId='" & CntrtId & "' and CardCode='" & SN & "' and DocStatus<>'C')"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CotizacionNumLines(docentry As String) As String
            Try
                Dim ostr As String
                ostr = "select count(*) from QUT1 WITH (NOLOCK) where docentry='" & docentry & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CotizacionSNCode(docentry As String) As String
            Try
                Dim ostr As String
                ostr = "select CardCode from OQUT WITH (NOLOCK) where docentry='" & docentry & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function OVNumLines(docentry As String) As String
            Try
                Dim ostr As String
                ostr = "select count(*) from RDR1 WITH (NOLOCK) where docentry='" & docentry & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function OVSNCode(docentry As String) As String
            Try
                Dim ostr As String
                ostr = "select CardCode from ORDR WITH (NOLOCK) where docentry='" & docentry & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Moneda(AcctCode As String) As String
            Try
                Dim ostr As String
                ostr = "select ActCurr from OACT WITH (NOLOCK) where AcctCode='" & AcctCode & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCuentaFinanciadora(Financiadora As String) As String
            Try
                Dim ostr As String
                ostr = "Select AcctCode from OACT where isnull(Formatcode,'')=" & vbCr & _
                        "(select U_AcctCode from [@EXX_ENFI] where Code='" & Financiadora & "')"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTotalPagos(Contratos As String, cuentaFinanciadora As String) As String
            Try
                Dim ostr As String
                ostr = "select isnull(Sum(DocTotal),0.0) as [Total(ML)] from ORCT  WITH (NOLOCK) where  Canceled<>'Y' and U_CntrtId='" & Contratos & "' and isnull(cashAcct,'')<>'" & cuentaFinanciadora & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTotalPagosSys(Contratos As String, cuentaFinanciadora As String) As String
            Try
                Dim ostr As String
                ostr = "select isnull(Sum(DocTotalSy),0.0) as [Total(ML)] from ORCT  WITH (NOLOCK) where  Canceled<>'Y' and U_CntrtId='" & Contratos & "' and isnull(cashAcct,'')<>'" & cuentaFinanciadora & "'"

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function




        Public Shared Function GetTotalPagos(Contratos As String) As String
            Try
                Dim ostr As String
                ostr = "select isnull(Sum(DocTotal),0.0) as [Total(ML)] from ORCT  WITH (NOLOCK) where  Canceled<>'Y' and U_CntrtId='" & Contratos & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTotalPagosSys(Contratos As String) As String
            Try
                Dim ostr As String
                ostr = "select isnull(Sum(DocTotalSy),0.0) as [Total(ML)] from ORCT  WITH (NOLOCK) where  Canceled<>'Y' and U_CntrtId='" & Contratos & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetPriceOferta(DocentryOferta As String) As String
            Try
                Dim ostr As String
                ostr = "select top 1 L.LineTotal from OQUT C WITH (NOLOCK) inner join QUT1 L WITH (NOLOCK) " & _
                    "on C.DocEntry=L.DocEntry where C.Docentry='" & DocentryOferta & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetVehiculoOferta(DocentryOferta As String) As String
            Try
                Dim ostr As String
                ostr = "select top 1 L.U_BoxPatent from OQUT C WITH (NOLOCK) inner join QUT1 L WITH (NOLOCK) " & _
                    "on C.DocEntry=L.DocEntry where C.Docentry='" & DocentryOferta & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetPriceOfertaFC(DocentryOferta As String) As String
            Try
                Dim ostr As String
                ostr = "select top 1 L.TotalSumSy from OQUT C WITH (NOLOCK) inner join QUT1 L WITH (NOLOCK) " & _
                    "on C.DocEntry=L.DocEntry where C.Docentry='" & DocentryOferta & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Get_TC_Oferta(DocentryOferta As String) As String
            Try
                Dim ostr As String
                ostr = "select  DocRate from OQUT where Docentry='" & DocentryOferta & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetMonedaOferta(DocentryOferta As String) As String
            Try
                Dim ostr As String
                ostr = "select  DocCur from OQUT where Docentry='" & DocentryOferta & "'"
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function GetTotalCondicionPago(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim entidadFinanciadora As String = Trim(oDBDS.GetValue("U_BankCode", 0))
                    Dim cuentaEntidadFinanciadora As String = Trim(BDContratos.GetCuentaFinanciadora(entidadFinanciadora))
                    If cuentaEntidadFinanciadora = "" Then cuentaEntidadFinanciadora = "-9999"
                    Dim sTotalPagos As String = BDContratos.DBDS_OUT(BDContratos.GetTotalPagos(Contrato, cuentaEntidadFinanciadora))
                    If sTotalPagos = "" Then sTotalPagos = 0
                    Dim TotalPagos As Decimal = sTotalPagos.Replace(".", sSepDecimalB1)
                    Dim Financiamiento As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalFi", 0))
                    res = Math.Round(TotalPagos + Financiamiento, 0)
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTotalCondicionPagoSys(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim entidadFinanciadora As String = Trim(oDBDS.GetValue("U_BankCode", 0))
                    Dim cuentaEntidadFinanciadora As String = Trim(BDContratos.GetCuentaFinanciadora(entidadFinanciadora))
                    If cuentaEntidadFinanciadora = "" Then cuentaEntidadFinanciadora = "-9999"
                    Dim sTotalPagos As String = BDContratos.DBDS_OUT(BDContratos.GetTotalPagosSys(Contrato, cuentaEntidadFinanciadora))
                    If sTotalPagos = "" Then sTotalPagos = 0
                    Dim TotalPagos As Decimal = sTotalPagos.Replace(".", sSepDecimalB1)
                    Dim Financiamiento As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalFiSys", 0))
                    res = TotalPagos + Financiamiento
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetBalanceFinal(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim TotalPrecioVenta As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalVATLc", 0))
                    Dim TotalPagos As Decimal = BDContratos.DBDS_OUT(oForm.DataSources.UserDataSources.Item("TotalPay").ValueEx)
                    Dim TramitesxCobrar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxC", 0))
                    Dim TramitesxPagar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxP", 0))
                    Dim VehiculoUsado As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalUs", 0))

                    'res = Math.Round(TotalPrecioVenta + TramitesxCobrar - TramitesxPagar - VehiculoUsado - TotalPagos, 0)
                    res = Math.Round(TotalPrecioVenta - VehiculoUsado - TotalPagos, 0)
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetBalanceFinalSys(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim TotalPrecioVenta As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalVATSys", 0))
                    Dim TotalPagos As Decimal = BDContratos.DBDS_OUT(oForm.DataSources.UserDataSources.Item("ToPaySys").ValueEx)
                    Dim TramitesxCobrar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxCSys", 0))
                    Dim TramitesxPagar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxPSys", 0))
                    Dim VehiculoUsado As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalUsSys", 0))

                    'res = TotalPrecioVenta + TramitesxCobrar - TramitesxPagar - VehiculoUsado - TotalPagos
                    res = TotalPrecioVenta - VehiculoUsado - TotalPagos
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetBalanceFinalContable(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim TotalPrecioVenta As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalVATLc", 0))

                    Dim sTotalPagosReales As String = BDContratos.DBDS_OUT(BDContratos.GetTotalPagos(Contrato))
                    If sTotalPagosReales = "" Then sTotalPagosReales = 0
                    Dim TotalPagosReales As Decimal = sTotalPagosReales.Replace(".", sSepDecimalB1)


                    'Dim TotalPagos As Decimal = BDContratos.DBDS_OUT(oForm.DataSources.UserDataSources.Item("TotalPay").ValueEx)
                    Dim TotalPagos As Decimal = TotalPagosReales
                    Dim TramitesxCobrar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxC", 0))
                    Dim TramitesxPagar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxP", 0))
                    Dim Financiamiento As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalFi", 0))

                    Dim VehiculoUsado As Decimal
                    TOTALESVehiculoUsadoDefinitivo(oForm, VehiculoUsado, 0)

                    'res = Math.Round(TotalPrecioVenta + TramitesxCobrar - TramitesxPagar - VehiculoUsado - TotalPagos, 0)
                    'res = Math.Round(TotalPrecioVenta - VehiculoUsado - TotalPagos + Financiamiento, 0)
                    res = Math.Round(TotalPrecioVenta - VehiculoUsado - TotalPagos, 0)
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub TOTALESVehiculoUsadoDefinitivo(ByRef oForm As SAPbouiCOM.Form, _
                                                 ByRef TotalUs As Decimal, ByRef TotalUsSys As Decimal)
            Try
                Dim oDBdatasource As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim oDBds As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@NTT5")
                Dim omatrix As SAPbouiCOM.Matrix = oForm.Items.Item("4_U_G").Specific
                omatrix.FlushToDataSource()
                Dim valor As Decimal = 0
                Dim valorSys As Decimal = 0
                For i As Integer = 0 To oDBds.Size - 1

                    If Trim(oDBds.GetValue("U_DocType", i)) = "Definitivo" Then
                        valor = valor + BDContratos.DBDS_OUT(oDBds.GetValue("U_LineTotal", i))
                        valorSys = valorSys + BDContratos.DBDS_OUT(oDBds.GetValue("U_LineTotalSys", i))
                    End If
                Next


                TotalUs = valor
                TotalUsSys = valorSys

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function GetBalanceFinalContableSys(ByRef oForm As SAPbouiCOM.Form) As Decimal
            Try
                Dim oDBDS As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim Contrato As String = Trim(oDBDS.GetValue("DocEntry", 0))
                Dim res As Decimal = 0
                If Contrato <> "" Then
                    Dim TotalPrecioVenta As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalVATSys", 0))

                    Dim sTotalPagosReales As String = BDContratos.DBDS_OUT(BDContratos.GetTotalPagosSys(Contrato))
                    If sTotalPagosReales = "" Then sTotalPagosReales = 0
                    Dim TotalPagosReales As Decimal = sTotalPagosReales.Replace(".", sSepDecimalB1)


                    Dim TotalPagos As Decimal = TotalPagosReales
                    Dim TramitesxCobrar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxCSys", 0))
                    Dim TramitesxPagar As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalTxPSys", 0))

                    Dim Financiamiento As Decimal = BDContratos.DBDS_OUT(oDBDS.GetValue("U_TotalFiSys", 0))

                    Dim VehiculoUsado As Decimal
                    TOTALESVehiculoUsadoDefinitivo(oForm, 0, VehiculoUsado)

                    'res = TotalPrecioVenta + TramitesxCobrar - TramitesxPagar - VehiculoUsado - TotalPagos
                    'res = TotalPrecioVenta - VehiculoUsado - TotalPagos + Financiamiento
                    res = TotalPrecioVenta - VehiculoUsado - TotalPagos
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetSumDec() As String
            Try
                Dim ostr As String

                ostr = "select sumdec from oadm"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetLineId(Contrato As String, Serie As String, tabla As String) As String
            Try
                Dim ostr As String

                ostr = "select LineId from [@" & tabla & "] where U_UsedCode='" & Serie & "' and DocEntry='" & Contrato & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetComprador(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_ProvRut from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetCompradorNombre(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_ProvNam from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetAnio(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_YrModl from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetTipoVenta(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_SlsTyp from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        
        Public Shared Function GetEstadoVenta(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_SaleSts from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocEntryVehiculo(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select DocEntry from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetPatent(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select RTRIM(LTRIM(LEFT(ISNULL(U_Patent, ''), 30))) from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetCarpetaAnexos() As String
            Try
                Dim ostr As String

                ostr = "select AttachPath from OADP"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function get_U_CardCode(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_OwnrCod from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getEstadoDeContrato(CNTTDocEntry As String) As String
            Try
                Dim ostr As String

                ostr = " select U_StatusCnt from ""@CNTT""  where DocEntry='" & CNTTDocEntry & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getFacturasReservaAsistente(CardCodeProveedor As String) As String
            Try
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = sqlReserva.getFacturasReservaAsistente_TEXT(CardCodeProveedor)
                Else
                    ostr = hanaReserva.getFacturasReservaAsistente_TEXT(CardCodeProveedor)
                End If
                Return ostr


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getCampoOCDR(CarCode As String, Campo As String) As String
            Try
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = sqlReserva.getCampoOCDR_TEXT(CarCode, Campo)
                Else
                    ostr = hanaReserva.getCampoOCDR_TEXT(CarCode, Campo)
                End If
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ObtenerDatosParaRevaluo(DocEntryEntrada As Integer, _
                                                       U_BoxPatent As String, _
                                                        itemCode As String, _
                                                        Whs As String, _
                                                        OcrCode As String, _
                                                        OcrCode2 As String, _
                                                        OcrCode3 As String, _
                                                        OcrCode4 As String, _
                                                        OcrCode5 As String, _
                                                        FechaContabilizacion As String, _
                                                        BaseEntry As String) As DataTable
            Try

                Dim LineTotal As Decimal = 0

                Dim iSumdec As Integer = DatosReserva.GetSumDec()
                Dim EsPrimeralinea As Boolean = False
                Dim CuentaA As String = ""
                Dim CuentaD As String = ""
                Dim SerieRev As String = BDContratos.GetParam("SerieRev")


                Dim AbsEntry As String

                Dim oData As DataTable = New DataTable("Datos")
                oData.Columns.Add("CuentaA", GetType(System.String))
                oData.Columns.Add("CuentaD", GetType(System.String))
                oData.Columns.Add("EsDebito", GetType(System.Boolean))
                oData.Columns.Add("Monto", GetType(System.Decimal))
                oData.Columns.Add("ItemCode", GetType(System.String))
                oData.Columns.Add("BoxPatent", GetType(System.String))
                oData.Columns.Add("OcrCode", GetType(System.String))
                oData.Columns.Add("OcrCode2", GetType(System.String))
                oData.Columns.Add("OcrCode3", GetType(System.String))
                oData.Columns.Add("OcrCode4", GetType(System.String))
                oData.Columns.Add("OcrCode5", GetType(System.String))
                oData.Columns.Add("FechaContabilizacion", GetType(System.String))
                oData.Columns.Add("Glosa", GetType(System.String))
                oData.Columns.Add("Moneda", GetType(System.String))
                oData.Columns.Add("iSumdec", GetType(System.Int32))
                oData.Columns.Add("SerieRev", GetType(System.String))
                oData.Columns.Add("DocEntryEntrada", GetType(System.String))
                oData.Columns.Add("DocEntryReserva", GetType(System.String))
                oData.Columns.Add("AbsEntry", GetType(System.String))
                oData.Columns.Add("Whs", GetType(System.String))
                oData.Columns.Add("BaseEntry", GetType(System.String))

                'If DatosReserva.EsVehiculo(itemCode) = True Then

                CuentaA = Trim(BDContratos.GetCuenta_Stock_en_transito(itemCode, Whs))
                CuentaD = CuentaA
                LineTotal = DatosReserva.getAjusteEntrada(BaseEntry, itemCode, U_BoxPatent, FechaContabilizacion)

                If DocEntryEntrada = -1 Then
                    Throw New Exception("[CD] Problema al obtener el DocEntry de la entrada: " + DocEntryEntrada)
                End If
                'If LineTotal <> 0 Then
                Dim oLine As DataRow = oData.NewRow
                oLine("CuentaA") = CuentaA
                oLine("CuentaD") = CuentaD
                oLine("EsDebito") = False
                oLine("Monto") = LineTotal
                oLine("BoxPatent") = U_BoxPatent
                oLine("OcrCode") = OcrCode
                oLine("OcrCode2") = OcrCode2
                oLine("OcrCode3") = OcrCode3
                oLine("OcrCode4") = OcrCode4
                oLine("OcrCode5") = OcrCode5
                oLine("FechaContabilizacion") = FechaContabilizacion
                oLine("Glosa") = ""
                oLine("Moneda") = MonedaLocal
                oLine("iSumdec") = iSumdec
                oLine("SerieRev") = SerieRev
                oLine("DocEntryEntrada") = DocEntryEntrada
                oLine("DocEntryReserva") = DocEntryEntrada  'sólo para reutilizar la clase datos reserva
                oLine("BaseEntry") = BaseEntry
                oLine("ItemCode") = itemCode
                oLine("Whs") = Whs
                oLine("AbsEntry") = DatosReserva.GetAbsEntry(U_BoxPatent, itemCode)

                oData.Rows.Add(oLine)

                'End If

                Return oData


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function nuevoNCVehiculoUsado(DocEntryNC As String) As String
            Try
                Dim ostr As String = ""
                If Motor = "SQL" Then
                    ostr = sqlReserva.nuevoNCVehiculoUsado_TEXT(DocEntryNC)
                Else
                    ostr = hanaReserva.nuevoNCVehiculoUsado_TEXT(DocEntryNC)
                End If
                ostr = conexi.obtenerValor(ostr)
                If ostr Is Nothing Then
                    ostr = ""
                End If
                Return ostr
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function EstadoNCVehiculoUsado(DocEntryNC As String) As String
            Try
                Dim ostr As String = ""
                If Motor = "SQL" Then
                    ostr = sqlReserva.EstadoNCVehiculoUsado_TEXT(DocEntryNC)
                Else
                    ostr = hanaReserva.EstadoNCVehiculoUsado_TEXT(DocEntryNC)
                End If
                ostr = conexi.obtenerValor(ostr)
                If ostr Is Nothing Then
                    ostr = ""
                End If
                Return ostr
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getEntradasAsistente(CardCodeProveedor As String, DocEntry As String) As String
            Try
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = sqlReserva.getEntradasAsistente_TEXT(CardCodeProveedor, DocEntry)
                Else
                    ostr = hanaReserva.getEntradasAsistente_TEXT(CardCodeProveedor, DocEntry)
                End If
                Return ostr


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function getValorSalida(Docentry As String) As String
            Try
                Dim ostr As String

                ostr = "select Sum(Debit) from JDT1  WITH (NOLOCK) where transId=(select TransID from OIGE " & _
                    " WITH (NOLOCK) where docentry='" & Docentry & "')"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_CardName(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_OwnrNam from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_ExCardCode(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_UsrCod from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_ExCardName(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_UsrNam from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_CardCodeOld(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_OwnrCodOld from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_ConditOld(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_ConditOld from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_CardNameOld(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_OwnrNamOld from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_ExCardCodeOld(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_UsrCodOld from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function get_U_ExCardNameOld(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select U_UsrNamOld from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetKilometraje(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select isnull(U_Mileage,0) from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCondicion(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select isnull(U_Condit,'') from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Get_Valor_compra(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = " select isnull(U_PurPrc,0) from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetFecha_arribo(BoxPatent As String) As String
            Try
                Dim ostr As String

                ostr = "select cast(year(U_ArrDate) as nvarchar(4))+ " & _
                        "RIGHT('0'+cast(MONTH(U_ArrDate) as nvarchar(2)),2)+ " & _
                        "RIGHT('0'+cast(DAY(U_ArrDate) as nvarchar(2)),2) " & _
                        "from [@EXX_VEHC] WITH (NOLOCK) where Code='" & BoxPatent & "'"
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DescColorExterior(Code As String) As String
            Try

                Dim sQuery As String = "Select U_ComDesc from [@EXX_COLE]  where Code='" & Code & "'"

                Return conexi.obtenerValor(sQuery)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub SetToCNNTChild(Contrato As String, Child As String, LineNum As Integer, _
                                                 Campo As String, Valor As String)
            Try
                Dim U_BoxPatent As String = ""
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim oChildren As SAPbobsCOM.GeneralDataCollection
                Dim sCmp As SAPbobsCOM.CompanyService
                If Valor <> "" And Contrato <> "" Then
                    Try
                        sCmp = DiApp.GetCompanyService
                        oGeneralService = sCmp.GetGeneralService("CNTT")
                        oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                        oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                        oGeneralParams.SetProperty("DocEntry", Contrato)
                        oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                        oChildren = oGeneralData.Child(Child)
                        oChildren.Item(LineNum - 1).SetProperty(Campo, Valor)
                        oGeneralService.Update(oGeneralData)
                    Catch ex As Exception
                        Throw ex
                    End Try


                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub DeleteToCNNTChild(Contrato As String, Child As String, LineNum As Integer)
            Try
                Dim U_BoxPatent As String = ""
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim oChildren As SAPbobsCOM.GeneralDataCollection
                Dim sCmp As SAPbobsCOM.CompanyService
                If Contrato <> "" Then
                    Try
                        sCmp = DiApp.GetCompanyService
                        oGeneralService = sCmp.GetGeneralService("CNTT")
                        oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                        oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                        oGeneralParams.SetProperty("DocEntry", Contrato)
                        oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                        oChildren = oGeneralData.Child(Child)
                        oChildren.Remove(LineNum)
                        oGeneralService.Update(oGeneralData)
                    Catch ex As Exception
                        Throw ex
                    End Try


                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub habilitarColumna(formUID As String, columna As String, switch As Boolean)
            Try
                Dim CompanyService As SAPbobsCOM.CompanyService = DiApp.GetCompanyService
                Dim FormSettingsService As SAPbobsCOM.FormPreferencesService
                Dim ColumnSettingsData As SAPbobsCOM.ColumnsPreferences
                Dim ColumnSettingsPara As SAPbobsCOM.ColumnsPreferencesParams

                FormSettingsService = CompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.FormPreferencesService)

                ColumnSettingsPara = FormSettingsService.GetDataInterface(SAPbobsCOM.FormPreferencesServiceDataInterfaces.fpsdiColumnsPreferencesParams)
                ColumnSettingsPara.FormID = "149"
                ColumnSettingsPara.User = DiApp.UserSignature
                ColumnSettingsData = FormSettingsService.GetColumnsPreferences(ColumnSettingsPara)


                For lngCount As Integer = 0 To ColumnSettingsData.Count - 1
                    If ColumnSettingsData.Item(lngCount).Column = "20" Then
                        ColumnSettingsData.Item(lngCount).EditableInForm = SAPbobsCOM.BoYesNoEnum.tYES
                        ColumnSettingsData.Item(lngCount).EditableInExpanded = SAPbobsCOM.BoYesNoEnum.tYES
                        Exit For
                    End If
                Next

                FormSettingsService.UpdateColumnsPreferences(ColumnSettingsPara, ColumnSettingsData)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function QueryDocumentosRelacionados(nroContrato As String) As String
            Try
                Dim sqr As String = "declare @Contrato nvarchar(6) " & vbCrLf & _
            "set @Contrato='" & nroContrato & "' " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType as [Tipo],ObjType as [Documento],Comments as [Comentario], DocDate as [Fecha],CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OQUT where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType as [Tipo],ObjType as [Documento],Comments as [Comentario], DocDate as [Fecha],CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from ORDR where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType as [Tipo],ObjType as [Documento],Comments as [Comentario], DocDate as [Fecha],CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OINV where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "select TransId as [Nro],U_DocType as [Tipo],ObjType as [Documento],Memo as [Comentario],RefDate as [Fecha], '','',LocTotal as [Total(ML)],SysTotal as [Total(MS)] " & vbCrLf & _
            "from OJDT where  U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OIGE where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OIGN where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from ORIN where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from ODLN where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OWTR where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf & _
            "union all " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType,ObjType,Comments as [Comentario],DocDate as [Fecha], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OWTQ where  Canceled<>'Y' and U_CntrtId=@Contrato "
                Return sqr
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function RelacionadosImportacion() As String
            Try
                Dim sqr As String = "" & vbCrLf &
        "--facturas de reserva  docentryReserva " & vbCrLf &
        "--asientos contables  U_jrnlPurchase " & vbCrLf &
        "--asientos de cierre   U_Docentry " & vbCrLf &
        "--entradas				BaseEntry " & vbCrLf &
        "--revaluos				docentry de la entrada en cada revaluo " & vbCrLf &
        "Select distinct C.DocEntry as ""Nro"",'Factura de reserva' as ""Tipo"",C.ObjType as ""Documento"",C.Comments as ""Comentario"",  " & vbCrLf &
        "C.DocDate as ""Fecha"",C.CardCode,C.CardName,C.DocTotal as ""Total(ML)"",C.DocTotalSy as ""Total(MS)""  " & vbCrLf &
        "from OPCH C  " & vbCrLf &
        "where  C.Canceled<>'Y'  " & vbCrLf &
        "and C.DocEntry in (@Reservas) " & vbCrLf &
        "   union all  " & vbCrLf &
        "select TransId as ""Nro"",'Apertura lineas' as ""Tipo"",ObjType as ""Documento"",Memo as ""Comentario"",RefDate as ""Fecha"", '','',LocTotal as ""Total(ML)"",SysTotal as ""Total(MS)""  " & vbCrLf &
        "from OJDT where  U_BaseEntry in ( @Reservas) " & vbCrLf &
        "--and TransId not in (SELECT StornoToTr FROM OJDT WITH (NOLOCK) where isnull(StornoToTr,'')<>'')  " & vbCrLf &
        "--and isnull(StornoToTr,'')='' " & vbCrLf &
        "   union all " & vbCrLf &
        "select distinct C.TransId as ""Nro"",'Cierre' as ""Tipo"",C.ObjType as ""Documento"",C.Memo as ""Comentario"",C.RefDate as ""Fecha"", '','',C.LocTotal as ""Total(ML)"",SysTotal as ""Total(MS)""  " & vbCrLf &
        "from OJDT C " & vbCrLf &
        "inner join JDT1 L on C.TransId=L.TransId  " & vbCrLf &
        "where  isnull(C.U_RefTC,'')<> '' " & vbCrLf &
        "and L.U_BaseEntry in (@Reservas) " & vbCrLf &
        "and C.TransId not in (SELECT StornoToTr FROM OJDT WITH (NOLOCK) where isnull(StornoToTr,'')<>'')  " & vbCrLf &
        "and isnull(C.StornoToTr,'')='' " & vbCrLf &
        "   union all  " & vbCrLf &
        "Select distinct C.DocEntry as [Nro],'Entradas de mercancía' as [Tipo],C.ObjType as [Documento],C.Comments as [Comentario],  " & vbCrLf &
        "C.DocDate as [Fecha],C.CardCode,C.CardName,C.DocTotal as [Total(ML)],C.DocTotalSy as [Total(MS)]  " & vbCrLf &
        "from OPDN C  " & vbCrLf &
        "inner join PDN1 L on C.DocEntry=L.DocEntry " & vbCrLf &
        "where  C.Canceled<>'Y' and L.BaseType='18' " & vbCrLf &
        "and L.BaseEntry in (@Reservas) " & vbCrLf &
        "   union all  " & vbCrLf &
        "select Docentry as ""Nro"", 'Revalorización' as  ""Tipo"",ObjType as ""Documento"",Comments as ""Comentario"", " & vbCrLf &
        "DocDate as ""Fecha"",''  as  ""CardCode"", '' as ""CardName"",0 as ""Total(ML)"", 0 as ""Total(MS)"" " & vbCrLf &
        "from OMRV  where  U_RefImp='Y' and isnull(U_ResEntry,'') in (@Reservas) " & vbCrLf &
        "union all " & vbCrLf &
        "Select distinct C.DocEntry as [Nro],'Notas de crédito' as [Tipo],C.ObjType as [Documento],C.Comments as [Comentario],  " & vbCrLf &
        "C.DocDate as [Fecha],C.CardCode,C.CardName,C.DocTotal as [Total(ML)],C.DocTotalSy as [Total(MS)]  " & vbCrLf &
        "from ORPC C  " & vbCrLf &
        "inner join RPC1 L on C.DocEntry=L.DocEntry " & vbCrLf &
        "where  C.Canceled<>'Y' and L.BaseType='18' " & vbCrLf &
        "and L.BaseEntry in (@Reservas) "
                Return sqr
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function QueryOfertaVenta(nroContrato As String) As String
            Try
                Dim sqr As String = "declare @Contrato nvarchar(6) " & vbCrLf & _
            "set @Contrato='" & nroContrato & "' " & vbCrLf & _
            "Select DocEntry as [Nro],U_DocType as [Tipo],ObjType as [Documento],Comments as [Comentario], CardCode,CardName,DocTotal as [Total(ML)],DocTotalSy as [Total(MS)] " & vbCrLf & _
            "from OQUT WITH (NOLOCK) where  Canceled<>'Y' and U_CntrtId=@Contrato " & vbCrLf
                Return sqr
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function anular_cotizacion(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim cotizacion As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oQuotations)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = cotizacion.GetByKey(docentry)
                If res = True Then
                    If cotizacion.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = cotizacion.Cancel()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                        Else
                            'res = cotizacion.GetByKey(docentry)
                            'If res = True Then
                            '    cotizacion.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = cotizacion.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If



                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function Cerrar_cotizacion(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim cotizacion As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oQuotations)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = cotizacion.GetByKey(docentry)
                If res = True Then
                    If cotizacion.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = cotizacion.Close()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function anular_orden_venta(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim order As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = order.GetByKey(docentry)
                If res = True Then
                    If order.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = order.Cancel()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                        Else
                            'res = order.GetByKey(docentry)
                            'If res = True Then
                            '    order.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = order.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function cerrar_orden_venta(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim order As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = order.GetByKey(docentry)
                If res = True Then
                    If order.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = order.Close()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                        Else
                            'res = order.GetByKey(docentry)
                            'If res = True Then
                            '    order.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = order.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If                            

                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function cerrar_Entrega(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim order As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = order.GetByKey(docentry)
                If res = True Then
                    If order.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = order.Close()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)          
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function


        Public Shared Function anular_entrega(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim entrega As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = entrega.GetByKey(docentry)
                If res = True Then
                    If entrega.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        RetVal = entrega.Close()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                        Else
                            'res = entrega.GetByKey(docentry)
                            'If res = True Then
                            '    entrega.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = entrega.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function anular_salida(docentry As String, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim salida As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = salida.GetByKey(docentry)
                If res = True Then
                    If Trim(salida.UserFields.Fields.Item("U_SaleType").Value) <> "Anulado" Then
                        Dim entrada_creada As Boolean = False
                        Dim fecha As Date = salida.DocDate
                        Dim contrato As String = Trim(salida.UserFields.Fields.Item("U_CntrtId").Value)
                        Dim BoxPatent As String = Trim(salida.UserFields.Fields.Item("U_BoxPatent").Value)
                        salida.Lines.SetCurrentLine(0)
                        Dim ItemCode As String = Trim(salida.Lines.ItemCode)
                        Dim Almacen As String = Trim(salida.Lines.WarehouseCode)
                        Dim OcrdCode As String = Trim(salida.Lines.CostingCode)
                        Dim OcrdCode2 As String = Trim(salida.Lines.CostingCode2)
                        Dim OcrdCode3 As String = Trim(salida.Lines.CostingCode3)
                        Dim OcrdCode4 As String = Trim(salida.Lines.CostingCode4)
                        Dim OcrdCode5 As String = Trim(salida.Lines.CostingCode5)

                        entrada_creada = Crear_Entrada(oApp, fecha, ItemCode, contrato, BoxPatent, Almacen, _
                                                       OcrdCode, OcrdCode2, OcrdCode3, OcrdCode4, OcrdCode5, , docentry)
                        If entrada_creada = True Then
                            salida.UserFields.Fields.Item("U_SaleType").Value = "Anulado"
                            RetVal = salida.Update()
                            If RetVal <> 0 Then
                                DiApp.GetLastError(ErrCode, ErrMsg)
                                oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            End If
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function Crear_Entrada(ByRef oApp As SAPbouiCOM.Application, fecha As Date, _
                                             ItemCode As String, CntrtID As String, BoxPatent As String, _
                                             AlmacenDeVendedor As String, OcrCode As String, OcrCode2 As String, OcrCode3 _
                                             As String, OcrCode4 As String, OcrCode5 As String, Optional tipoVenta As String = "", _
                                             Optional DocEntrySalida As String = "") As Boolean
            Try
                Dim oInvoice As SAPbobsCOM.Documents
                Dim sErr As String = String.Empty
                Dim lErr As Integer
                Dim lRetCode As Integer
                oInvoice = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry)
                oInvoice.DocDueDate = fecha
                oInvoice.DocDate = fecha

                Dim serie As String = "-1"
                If serie <> "-1" And serie <> "" Then oInvoice.Series = serie
                oInvoice.UserFields.Fields.Item("U_CntrtId").Value = CntrtID
                oInvoice.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                oInvoice.UserFields.Fields.Item("U_DocType").Value = "Anulación salida"
                oInvoice.Reference2 = BoxPatent
                Dim Patente_nominal As String = Trim(BDContratos.GetPatent(BoxPatent))
                If Patente_nominal <> "" Then oInvoice.UserFields.Fields.Item("U_Patent").Value = Patente_nominal

                oInvoice.Lines.ItemCode = ItemCode
                oInvoice.Lines.Quantity = 1
                'oInvoice.Lines.Currency = MonedaComision
                Dim price As Double = 0
                If DocEntrySalida <> "" Then
                    Dim sPrice As String = BDContratos.getValorSalida(DocEntrySalida)
                    If sPrice = "" Then sPrice = 0
                    price = BDContratos.DBDS_OUT(sPrice)
                End If

                oInvoice.Lines.Currency = MonedaLocal
                oInvoice.Lines.Price = price
                oInvoice.Lines.LineTotal = price

                If OcrCode <> "" Then oInvoice.Lines.CostingCode = OcrCode
                If OcrCode2 <> "" Then oInvoice.Lines.CostingCode2 = OcrCode2
                If OcrCode3 <> "" Then oInvoice.Lines.CostingCode3 = OcrCode3
                If OcrCode4 <> "" Then oInvoice.Lines.CostingCode4 = OcrCode4
                If OcrCode5 <> "" Then oInvoice.Lines.CostingCode5 = OcrCode5

                oInvoice.Lines.WarehouseCode = AlmacenDeVendedor
                oInvoice.Lines.SerialNumbers.ManufacturerSerialNumber = BoxPatent
                oInvoice.Lines.SerialNumbers.InternalSerialNumber = BoxPatent
                oInvoice.Lines.SerialNumbers.Quantity = 1

                oInvoice.Lines.SerialNumbers.Add()


                lRetCode = oInvoice.Add()

                If lRetCode <> 0 Then
                    'Dim sds As String = oInvoice.GetAsXML()
                    DiApp.GetLastError(lErr, sErr)
                    oApp.MessageBox(lErr & " " & sErr)
                    Return False

                Else
                    Dim sNewObjCode As String = String.Empty
                    DiApp.GetNewObjectCode(sNewObjCode)
                    Return True

                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function anular_factura(docentry As String, ByRef oApp As SAPbouiCOM.Application, _
                                         BoxPatent As String, fecha As Date, ByRef EsVendido As Boolean) As Boolean
            Try
                Dim factura As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = factura.GetByKey(docentry)
                If res = True Then
                    If factura.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        Dim nota_credito As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes)
                        nota_credito.CardCode = factura.CardCode
                        nota_credito.DocDueDate = fecha
                        nota_credito.DocDate = fecha
                        nota_credito.Reference2 = BoxPatent
                        Dim tipo_venta As String = Trim(factura.UserFields.Fields.Item("U_SaleType").Value)
                        Dim DocType As String = Trim(factura.UserFields.Fields.Item("U_DocType").Value)
                        Dim serie As String = ""
                        Select Case DocType
                            Case "Factura comisión"
                                serie = Trim(BDContratos.GetParam("NCCo" & tipo_venta))
                                EsVendido = True
                            Case "Factura accesorios"
                                serie = Trim(BDContratos.GetParam("SNota"))
                            Case "Factura tramites"
                                serie = Trim(BDContratos.GetParam("SNota"))
                            Case Else
                                serie = Trim(BDContratos.GetParam("NCVe" & tipo_venta))
                                EsVendido = True
                        End Select

                        If serie <> "-1" And serie <> "" Then nota_credito.Series = serie
                        nota_credito.UserFields.Fields.Item("U_CntrtId").Value = Trim(factura.UserFields.Fields.Item("U_CntrtId").Value)
                        nota_credito.UserFields.Fields.Item("U_DocType").Value = "Anulación"

                        Dim Patente_nominal As String = Trim(BDContratos.GetPatent(BoxPatent))
                        If Patente_nominal <> "" Then nota_credito.UserFields.Fields.Item("U_Patent").Value = Patente_nominal

                        Dim i As Integer
                        i = 0
                        Do
                            nota_credito.Lines.BaseEntry = docentry
                            nota_credito.Lines.BaseLine = i
                            nota_credito.Lines.BaseType = SAPbobsCOM.BoObjectTypes.oInvoices
                            If i = 0 Then
                                nota_credito.Lines.SerialNumbers.ManufacturerSerialNumber = BoxPatent
                                nota_credito.Lines.SerialNumbers.InternalSerialNumber = BoxPatent
                                nota_credito.Lines.SerialNumbers.Quantity = 1
                                nota_credito.Lines.SerialNumbers.Add()
                            End If
                            i += 1
                            If i <> factura.Lines.Count() Then
                                nota_credito.Lines.Add()
                            End If
                        Loop Until i = factura.Lines.Count()

                        RetVal = nota_credito.Add()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] Facturas. " & ErrCode & " " & ErrMsg)
                        Else
                            'res = factura.GetByKey(docentry)
                            'If res = True Then
                            '    factura.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = factura.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function anular_Ingreso(docentry As String, ByRef oApp As SAPbouiCOM.Application, fecha As Date _
                                         ) As Boolean
            Try
                Dim nota_credito As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes)

                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                Dim BoxPatent As String = ""
                res = nota_credito.GetByKey(docentry)
                If res = True Then
                    If nota_credito.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                        Dim nota_debito As SAPbobsCOM.Documents = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                        nota_debito.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo
                        nota_debito.CardCode = nota_credito.CardCode
                        nota_debito.DocDate = fecha 'nota_credito.DocDate
                        nota_debito.PaymentGroupCode = -1
                        'nota_debito.DocDueDate = DateTime.Today
                        nota_debito.Comments = "anulacion nota credito " & nota_credito.DocNum
                        nota_debito.UserFields.Fields.Item("U_IsVehc").Value = "Y"
                        nota_debito.UserFields.Fields.Item("U_CntrtId").Value = docentry
                        nota_debito.UserFields.Fields.Item("U_DocType").Value = "Veh. usado"

                        Dim tipo_venta As String = Trim(nota_credito.UserFields.Fields.Item("U_SaleType").Value)
                        Dim DocType As String = Trim(nota_credito.UserFields.Fields.Item("U_DocType").Value)
                        Dim serie As String = ""
                        Select Case DocType
                            Case "Veh. usado"
                                serie = Trim(BDContratos.GetParam("SNotaDeb"))

                        End Select

                        If serie <> "-1" And serie <> "" Then nota_debito.Series = serie
                        nota_debito.UserFields.Fields.Item("U_CntrtId").Value = Trim(nota_credito.UserFields.Fields.Item("U_CntrtId").Value)
                        nota_debito.UserFields.Fields.Item("U_DocType").Value = "Anulación"
                        Dim i As Integer
                        i = 0
                        Do
                            nota_credito.Lines.SetCurrentLine(i)
                            nota_debito.Lines.ItemCode = nota_credito.Lines.ItemCode
                            nota_debito.Lines.SerialNum = nota_credito.Lines.SerialNum
                            nota_debito.Lines.Quantity = nota_credito.Lines.Quantity
                            nota_debito.Lines.Currency = nota_credito.Lines.Currency
                            nota_debito.Lines.Price = nota_credito.Lines.Price()
                            nota_debito.Lines.TaxCode = nota_credito.Lines.TaxCode
                            nota_debito.Lines.WarehouseCode = nota_credito.Lines.WarehouseCode
                            nota_debito.Lines.AccountCode = nota_credito.Lines.AccountCode
                            nota_debito.Lines.COGSAccountCode = nota_credito.Lines.COGSAccountCode
                            nota_debito.Lines.CostingCode = nota_credito.Lines.CostingCode
                            nota_debito.Lines.CostingCode2 = nota_credito.Lines.CostingCode2
                            nota_debito.Lines.CostingCode3 = nota_credito.Lines.CostingCode3
                            nota_debito.Lines.CostingCode4 = nota_credito.Lines.CostingCode4
                            nota_debito.Lines.CostingCode5 = nota_credito.Lines.CostingCode5


                            BoxPatent = nota_credito.Lines.UserFields.Fields.Item("U_BoxPatent").Value

                            nota_debito.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                            Dim Patente_nominal As String = Trim(BDContratos.GetPatent(BoxPatent))
                            If Patente_nominal <> "" Then nota_debito.UserFields.Fields.Item("U_Patent").Value = Patente_nominal

                            
                            nota_debito.Lines.SerialNumbers.ManufacturerSerialNumber = BoxPatent
                            nota_debito.Lines.SerialNumbers.InternalSerialNumber = BoxPatent
                            nota_debito.Lines.SerialNumbers.Quantity = 1
                            nota_debito.Lines.SerialNumbers.Add()

                            If i = 0 Then
                                nota_debito.Reference2 = BoxPatent
                            End If

                            i += 1
                            If i <> nota_credito.Lines.Count() Then
                                nota_debito.Lines.Add()
                            End If
                        Loop Until i = nota_credito.Lines.Count()
                        RetVal = nota_debito.Add()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] Ingreso. " & ErrCode & " " & ErrMsg)
                        Else

                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function anular_asiento(docentry As String, ByRef oApp As SAPbouiCOM.Application, fecha As Date, Optional BoxPatent As String = "") As Boolean
            Try
                Dim asiento As SAPbobsCOM.JournalEntries = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
                Dim res As Boolean
                Dim RetVal As Integer
                Dim ErrCode As Integer
                Dim ErrMsg As String
                res = asiento.GetByKey(docentry)
                If res = True Then
                    If ElAsientoestaAnulado(docentry) = False Then
                        asiento.ReferenceDate = fecha
                        If BoxPatent <> "" Then asiento.Reference2 = BoxPatent
                        RetVal = asiento.Cancel()
                        If RetVal <> 0 Then
                            DiApp.GetLastError(ErrCode, ErrMsg)
                            oApp.MessageBox("[CD] Asientos contables. " & ErrCode & " " & ErrMsg)
                        Else
                            'res = asiento.GetByKey(docentry)
                            'If res = True Then
                            '    asiento.UserFields.Fields.Item("U_CntrtId").Value = ""
                            '    RetVal = asiento.Update()
                            '    If RetVal <> 0 Then
                            '        DiApp.GetLastError(ErrCode, ErrMsg)
                            '        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                            '    End If
                            'End If
                        End If
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Shared Function ElAsientoestaAnulado(docEntry As String) As Boolean
            Try
                Dim res As Boolean = False

                Dim query As String = "" & _
                                    "SELECT T0.TransId FROM OJDT T0 WITH (NOLOCK) " & _
                                    "where T0.TransId not in (SELECT StornoToTr FROM OJDT WITH (NOLOCK) where isnull(StornoToTr,'')<>'') " & _
                                    "and isnull(StornoToTr,'')='' and T0.TransId='" & docEntry & "'"
                Dim valor As String = Trim(conexi.obtenerValor(query))
                If valor <> "" Then
                    res = False
                Else
                    res = True
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub Genera_transferencia_vendedor(oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application)
            Try
                Dim odb As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@CNTT")
                Dim CntrtID As String = Trim(odb.GetValue("DocEntry", 0))
                Dim BoxPatent As String = Trim(odb.GetValue("U_BoxPatent", 0))
                Dim EmpleadoVentas As String = Trim(odb.GetValue("U_SlpCode", 0))
                Dim TipoVenta As String = Trim(odb.GetValue("U_SaleType", 0))
                Dim ItemCode As String = Trim(odb.GetValue("U_Model", 0))
                Dim OcrCode As String = Trim((odb.GetValue("U_OcrCode", 0)))
                Dim OcrCode2 As String = Trim((odb.GetValue("U_OcrCode2", 0)))
                Dim OcrCode3 As String = Trim((odb.GetValue("U_OcrCode3", 0)))
                Dim OcrCode4 As String = Trim((odb.GetValue("U_OcrCode4", 0)))
                Dim OcrCode5 As String = Trim((odb.GetValue("U_OcrCode5", 0)))

                Dim Marca1 As String = Trim((odb.GetValue("U_Brand", 0)))

                If CntrtID <> "" Then
                    Dim res As Integer = oApp.MessageBox("[CD] Esta seguro de crear un documento de transferencia del vehículo " & BoxPatent & " al almacén del vendedor?", 1, "Si", "No")
                    If res = 1 Then

                        Dim transferencia As SAPbobsCOM.StockTransfer
                        Dim TransStock As String = Trim(GetParam("TransStock").ToString)
                        If Trim(TransStock) = "Y" Then
                            transferencia = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer)
                        Else
                            transferencia = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryTransferRequest)
                        End If

                        Dim RetVal As Integer
                        Dim ErrCode As Integer
                        Dim ErrMsg As String

                        Dim AlmacenDeVendedor As String = Trim(BDContratos.AlmacenEmpleadoVentas(EmpleadoVentas, TipoVenta, Marca1))
                        Dim AlmacenEnSBO As String = Trim(BDContratos.AlmacenDeSerie(BoxPatent, ItemCode))
                        If Trim(AlmacenEnSBO) <> "" Then
                            If Trim(AlmacenDeVendedor) <> "" Then
                                If AlmacenDeVendedor <> AlmacenEnSBO Then
                                    transferencia.FromWarehouse = AlmacenEnSBO
                                    transferencia.ToWarehouse = AlmacenDeVendedor
                                    transferencia.Lines.ItemCode = ItemCode
                                    transferencia.Lines.Quantity = 1
                                    If OcrCode <> "" Then transferencia.Lines.DistributionRule = OcrCode
                                    If OcrCode2 <> "" Then transferencia.Lines.DistributionRule2 = OcrCode2
                                    If OcrCode3 <> "" Then transferencia.Lines.DistributionRule3 = OcrCode3
                                    If OcrCode4 <> "" Then transferencia.Lines.DistributionRule4 = OcrCode4
                                    If OcrCode5 <> "" Then transferencia.Lines.DistributionRule5 = OcrCode5
                                    transferencia.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent

                                    transferencia.Lines.SerialNumbers.ManufacturerSerialNumber = BoxPatent
                                    transferencia.Lines.SerialNumbers.InternalSerialNumber = BoxPatent

                                    transferencia.Lines.SerialNumbers.Quantity = 1
                                    transferencia.Lines.SerialNumbers.Add()

                                    transferencia.UserFields.Fields.Item("U_CntrtId").Value = CntrtID
                                    transferencia.UserFields.Fields.Item("U_DocType").Value = "Transferencia"
                                    Dim Patente_nominal As String = Trim(BDContratos.GetPatent(BoxPatent))
                                    If Patente_nominal <> "" Then transferencia.UserFields.Fields.Item("U_Patent").Value = Patente_nominal
                                    transferencia.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent


                                    RetVal = transferencia.Add()
                                    If RetVal <> 0 Then
                                        DiApp.GetLastError(ErrCode, ErrMsg)
                                        oApp.MessageBox("[CD] " & ErrCode & " " & ErrMsg)
                                    Else
                                        'If Trim(TransStock) = "Y" Then
                                        '    Dim TipoVenta_almacen As String = Trim(BDContratos.TipoVenta_Almacen(AlmacenDeVendedor))
                                        '    If TipoVenta_almacen <> "" Then
                                        '        SetVehStatus(BoxPatent, "U_SlsTyp", TipoVenta_almacen)
                                        '    End If
                                        'End If
                                        oApp.MessageBox("[CD] Transferencia realizada con éxito")
                                    End If
                                Else
                                    oApp.MessageBox("[CD] El vehículo " & BoxPatent & " se encuentra en el almacén del vendedor, no se creo documento de trasferencia")
                                End If

                            Else
                                oApp.MessageBox("[Aviso] No existe un almacén asociado al vendedor y al tipo de venta.")
                            End If                            
                        Else
                            oApp.MessageBox("[Aviso] El vehículo " & BoxPatent & " no tiene stock en ningún almacén. No se creará el documento de transferencia.")
                        End If


                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function Esta_reservado(BoxPatent As String, tipo_documento As String) As Boolean
            Try
                Dim res As Boolean
                Dim query As String = "" & _
                                   "select Code,U_SaleSts from [@EXX_VEHC] where (U_SaleSts='3' OR U_SaleSts='4' OR U_SaleSts='2') and Code='" & BoxPatent & "'"
                Dim valor As String = Trim(conexi.obtenerValor(query))
                If valor <> "" Then
                    res = True
                Else
                    res = False
                End If
                Return res


            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function Tiene_pago(contrato As String) As Boolean
            Try
                Dim res As Boolean
                Dim moneda_monto_minimo As String = GetParam("Moneda4")
                Dim monto_minimo As Double = BDContratos.DBDS_OUT(GetParam("PagoMin"))
                Dim monto_minimo_contrato As Double = 0
                If Trim(moneda_monto_minimo) = "System" Then
                    monto_minimo_contrato = GetTotalPagosSys(contrato)
                Else
                    monto_minimo_contrato = GetTotalPagos(contrato)
                End If
                If monto_minimo_contrato >= monto_minimo Then
                    res = True
                Else
                    res = False
                End If

                Return res


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function reserva_en_contrato(BoxPatent As String, tipo_documento As String) As String
            Try
                Dim query As String = ""
                If tipo_documento = "oferta_venta" Then
                    query = "select max(U_CntrtId) as [U_CntrtId],o.U_BoxPatent,o.DocStatus " & vbCrLf & _
                "from oqut o " & vbCrLf & _
                "inner join qut1 l on o.DocEntry=l.DocEntry " & vbCrLf & _
                "inner join ""@CNTT"" C on C.DocEntry=o.U_CntrtId " & vbCrLf & _
                "where o.U_BoxPatent='" & BoxPatent & "' and isnull(U_CntrtId,0)<>0 " & vbCrLf & _
                "and (o.DocStatus<>'C' or (o.DocStatus='C'  and l.TargetType='17') ) and C.U_StatusCnt<>'9' " & vbCrLf & _
                "group by o.U_BoxPatent,o.DocStatus,o.CANCELED"

                Else
                    query = "select max(U_CntrtId) as [U_CntrtId],o.U_BoxPatent,o.DocStatus " & vbCrLf & _
                "from ordr o " & vbCrLf & _
                "inner join qut1 l on o.DocEntry=l.DocEntry " & vbCrLf & _
                "inner join ""@CNTT"" C on C.DocEntry=o.U_CntrtId " & vbCrLf & _
                "where o.U_BoxPatent='" & BoxPatent & "' and isnull(U_CntrtId,0)<>0 " & vbCrLf & _
                "and (o.DocStatus<>'C' or (o.DocStatus='C'  and l.TargetType='13') ) and C.U_StatusCnt<>'9' " & vbCrLf & _
                "group by o.U_BoxPatent,o.DocStatus,o.CANCELED"

                End If

                Return Trim(conexi.obtenerValor(query))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub CheckReservedStatus()
            Try
                'query buscando el la ultima fecha entre las Cotizaciones generadas asociadas al contrato i
                'si hay resultado para cada fila realizar un update


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function GetCuenta_PENT(ItemCode As String, WhsCode As String) As String
            Try
                Dim query As String = ""
                Dim res As String = ""

                Dim ItmsGrpCod As String = ""

                Dim tipoCuentasItem As String = ""
                If Motor = "SQL" Then
                    tipoCuentasItem = Trim(conexi.obtenerValor(sqlAjusteTC.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                Else
                    tipoCuentasItem = Trim(conexi.obtenerValor(hanaAjusteTC.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                End If
                If tipoCuentasItem <> "" Then
                    Select Case tipoCuentasItem
                        Case "W"
                            If Motor = "SQL" Then
                                query = Trim(sqlAjusteTC.Get_Cuenta_ItemPENT_de_almacen_TEXT(WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_almacen_TEXT(WhsCode))
                            End If
                        Case "C"

                            If Motor = "SQL" Then
                                ItmsGrpCod = Trim(conexi.obtenerValor(sqlAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(sqlAjusteTC.Get_Cuenta_Item_de_GrupoPENT_TEXT(ItmsGrpCod))
                            Else
                                ItmsGrpCod = Trim(conexi.obtenerValor(hanaAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod))
                            End If
                        Case "L"
                            If Motor = "SQL" Then
                                query = Trim(sqlAjusteTC.Get_Cuenta_Item_de_PENT_TEXT(ItemCode, WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Item_TEXT(ItemCode, WhsCode))
                            End If
                    End Select

                    res = Trim(conexi.obtenerValor(query))
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCuenta_Stock_en_transito(ItemCode As String, WhsCode As String) As String
            Try
                Dim query As String = ""
                Dim res As String = ""

                Dim ItmsGrpCod As String = ""

                Dim tipoCuentasItem As String = ""
                If Motor = "SQL" Then
                    tipoCuentasItem = Trim(conexi.obtenerValor(sqlAjusteTC.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                Else
                    tipoCuentasItem = Trim(conexi.obtenerValor(hanaAjusteTC.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                End If
                If tipoCuentasItem <> "" Then
                    Select Case tipoCuentasItem
                        Case "W"
                            If Motor = "SQL" Then
                                query = Trim(sqlAjusteTC.Get_Cuenta_Item_de_almacen_TEXT(WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_almacen_TEXT(WhsCode))
                            End If
                        Case "C"

                            If Motor = "SQL" Then
                                ItmsGrpCod = Trim(conexi.obtenerValor(sqlAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(sqlAjusteTC.Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod))
                            Else
                                ItmsGrpCod = Trim(conexi.obtenerValor(hanaAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod))
                            End If
                        Case "L"
                            If Motor = "SQL" Then
                                query = Trim(sqlAjusteTC.Get_Cuenta_Item_de_Item_TEXT(ItemCode, WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Item_TEXT(ItemCode, WhsCode))
                            End If
                    End Select

                    res = Trim(conexi.obtenerValor(query))
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Buscar_AsientoApertura_nocancelado(DocEntryReserva As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlReserva.Buscar_AsientoApertura_nocancelado_TEXT(DocEntryReserva)
                Else
                    query = hanaReserva.Buscar_AsientoApertura_nocancelado_TEXT(DocEntryReserva)
                End If
                Return conexi.obtenerColeccion(query)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Tiene_almenos_una_entrada(DocEntryReserva As String) As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlReserva.Tiene_almenos_una_entrada_TEXT(DocEntryReserva)
                Else
                    query = hanaReserva.Tiene_almenos_una_entrada_TEXT(DocEntryReserva)
                End If
                Return Trim(conexi.obtenerValor(query))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetDocEntryEntrada(Contrato As String) As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlReserva.GetDocEntryEntrada_TEXT(Contrato)
                Else
                    query = hanaReserva.GetDocEntryEntrada_TEXT(Contrato)
                End If
                Return Trim(conexi.obtenerValor(query))
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function Lista_de_revaluos(DocEntryReserva As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlReserva.Lista_de_revaluos_TEXT(DocEntryReserva)
                Else
                    query = hanaReserva.Lista_de_revaluos_TEXT(DocEntryReserva)
                End If
                Return conexi.obtenerColeccion(query)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ListaContratosDuplicadosNoAnulados(DocEntryContratos As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlReserva.ListaContratosDuplicadosNoAnulados_TEXT(DocEntryContratos)
                Else
                    query = hanaReserva.ListaContratosDuplicadosNoAnulados_TEXT(DocEntryContratos)
                End If
                Return conexi.obtenerColeccion(query)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function anular_revaluo(docentry As String, ByRef oApp As SAPbouiCOM.Application, fecha As Date) As Boolean
            Try
                Dim res As Boolean = False
                Dim odata As DataTable
                Dim absEntry As String
                Dim dmonto As Decimal
                Dim oRevalInvBase As SAPbobsCOM.MaterialRevaluation
                oRevalInvBase = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMaterialRevaluation)
                Dim oRevalInv As SAPbobsCOM.MaterialRevaluation
                oRevalInv = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMaterialRevaluation)
                If oRevalInvBase.GetByKey(docentry) = True Then

                    odata = DatosReserva.GetAbsEntry_BoxPatent(docentry)
                    If Not odata Is Nothing Then
                        For Each row As DataRow In odata.Rows
                            absEntry = row.Field(Of Integer)("SnbAbsEnt")
                            'U_BoxPatent = row.Field(Of String)("SNBNum")
                            dmonto = row.Field(Of Decimal)("DebCred")
                        Next
                    End If

                    oRevalInv.DocDate = oRevalInvBase.DocDate
                    oRevalInv.TaxDate = oRevalInvBase.TaxDate
                    oRevalInv.Series = oRevalInvBase.Series

                    oRevalInv.InflationRevaluation = SAPbobsCOM.BoYesNoEnum.tNO
                    oRevalInv.RevalType = "M"

                    oRevalInv.Comments = Left("Anulación " & oRevalInvBase.Comments, 50)
                    'vJE.Reference = comentario
                    oRevalInv.UserFields.Fields.Item("U_RefImp").Value = oRevalInvBase.UserFields.Fields.Item("U_RefImp").Value
                    oRevalInv.UserFields.Fields.Item("U_BaseEntry").Value = oRevalInvBase.UserFields.Fields.Item("U_BaseEntry").Value
                    oRevalInv.UserFields.Fields.Item("U_ResEntry").Value = oRevalInvBase.UserFields.Fields.Item("U_ResEntry").Value
                    oRevalInv.UserFields.Fields.Item("U_BoxPatent").Value = oRevalInvBase.UserFields.Fields.Item("U_BoxPatent").Value

                    oRevalInvBase.Lines.SetCurrentLine(0)
                    oRevalInv.Lines.ItemCode = oRevalInvBase.Lines.ItemCode
                    oRevalInv.Lines.Quantity = 1
                    oRevalInv.Lines.WarehouseCode = oRevalInvBase.Lines.WarehouseCode
                    oRevalInv.Lines.RevaluationIncrementAccount = oRevalInvBase.Lines.RevaluationIncrementAccount
                    oRevalInv.Lines.RevaluationDecrementAccount = oRevalInvBase.Lines.RevaluationDecrementAccount

                    oRevalInv.Lines.DistributionRule = oRevalInvBase.Lines.DistributionRule
                    oRevalInv.Lines.DistributionRule2 = oRevalInvBase.Lines.DistributionRule2
                    oRevalInv.Lines.DistributionRule3 = oRevalInvBase.Lines.DistributionRule3
                    oRevalInv.Lines.DistributionRule4 = oRevalInvBase.Lines.DistributionRule4
                    oRevalInv.Lines.DistributionRule5 = oRevalInvBase.Lines.DistributionRule5

                    Dim aux As String = oRevalInvBase.Lines.ItemCode
                    Dim Srscount As Integer
                    Dim SNBService As SAPbobsCOM.MaterialRevaluationSNBService
                    Dim SNBParams As SAPbobsCOM.MaterialRevaluationSNBParams
                    Dim SNBParamCol As SAPbobsCOM.MaterialRevaluationSNBParamsCollection
                    Dim SNBParam As SAPbobsCOM.MaterialRevaluationSNBParam
                    Dim compService As SAPbobsCOM.CompanyService
                    Dim SNBLines As SAPbobsCOM.SNBLines

                    SNBLines = oRevalInv.Lines.SNBLines
                    compService = DiApp.GetCompanyService()
                    SNBService = compService.GetBusinessService(SAPbobsCOM.ServiceTypes.MaterialRevaluationSNBService)
                    SNBParams = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParams)
                    SNBParam = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParam)
                    SNBParam.ItemCode = oRevalInvBase.Lines.ItemCode
                    SNBParamCol = SNBService.GetList(SNBParam)
                    Srscount = SNBParamCol.Count

                    For i As Integer = 0 To SNBParamCol.Count - 1
                        SNBParams = SNBParamCol.Item(i)
                        'se valida que sea la serie que esta siendo usada y se llena la informacion a las lineas faltantes de serie
                        If absEntry = SNBParams.SnbAbsEntry Then
                            SNBLines.SnbAbsEntry = SNBParams.SnbAbsEntry
                            SNBLines.DebitCredit = -dmonto

                            Exit For
                        End If
                    Next
                    'Call oRevalInv.Lines.Add()

                    Dim iError As Integer = oRevalInv.Add()

                    If iError <> 0 Then
                        'Dim strxml As String
                        'strxml = oRevalInv.GetAsXML
                        Dim sError As String = String.Empty
                        DiApp.GetLastError(iError, sError)
                        res = False
                        Throw New Exception("[CD] Problema al anular documento de revalúo: " + sError)
                    Else
                        'DiApp.GetNewObjectCode(transID)
                        res = True
                    End If


                End If
                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

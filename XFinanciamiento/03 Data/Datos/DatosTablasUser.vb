﻿
Imports System.Data.SqlClient
Imports XFinanciamiento.Data.Constantes

Namespace Data.Datos

    Public Class DatosTablasUser

        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable

        ''' <summary>
        ''' VERIDICA SI EL CAMPO EXISTE
        ''' </summary>
        Public Shared Function Verifica_Existe_Campo(ByVal tabla As String, ByVal Campo As String) As Integer
            Dim sSqlQuery As String
            Dim valor As Integer
            Try

                If Motor = "SQL" Then
                    sSqlQuery = "Select ISNULL(max(FieldId), -1) from CUFD where TableID = '" & tabla & "' AND AliasId = '" & Campo & "'"
                Else
                    sSqlQuery = "Select IFNULL(max(""FieldID""), -1) from """ & SQLBaseDatos & """.""CUFD"" where ""TableID"" = '" & tabla & "' AND ""AliasID"" = '" & Campo & "'"
                End If


                valor = conexi.obtenerValor(sSqlQuery, False)

                Return valor

            Catch ex As Exception
                Return -2
            End Try

        End Function

        ''' <summary>
        ''' RESCATA ULTIMA SECUENCIA
        ''' </summary>
        Public Shared Function Rescata_ultima_Secuencia(ByVal tabla As String) As Integer
            Dim sSqlQuery As String
            Dim valor As Integer
            Try

                If Motor = "SQL" Then
                    sSqlQuery = "Select ISNULL(max(FieldId), 0) from CUFD where TableID = '" & tabla & "'"
                Else
                    sSqlQuery = "Select IFNULL(max(""FieldID""), 0) from """ & SQLBaseDatos & """.""CUFD"" where ""TableID"" = '" & tabla & "'"
                End If

                valor = conexi.obtenerValor(sSqlQuery, False)

                Return valor

            Catch ex As Exception
                Return 0
            End Try

        End Function

        ''' <summary>
        ''' ELIMINA PROCEDIMIENTOS ALMACENADOS
        ''' </summary>
        Public Shared Sub EliminaSP(ByVal nombreSp As String)

            Dim sSqlQuery As String
            Try
                If Motor = "SQL" Then
                    sSqlQuery = "DROP PROCEDURE [" & nombreSp & "]"
                Else
                    sSqlQuery = "DROP PROCEDURE """ & SQLBaseDatos & """.""" & nombreSp & """"
                End If


                conexi.creaRegistro(sSqlQuery, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub EliminaFX(ByVal nombreSp As String)

            Dim sSqlQuery As String
            Try
                If Motor = "SQL" Then
                    sSqlQuery = "DROP FUNCTION [" & nombreSp & "]"
                Else
                    sSqlQuery = "DROP FUNCTION """ & SQLBaseDatos & """.""" & nombreSp & """"
                End If


                conexi.creaRegistro(sSqlQuery, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        ''' <summary>
        ''' ELIMINA function
        ''' </summary>
        Public Shared Sub EliminaTF(ByVal nombreTf As String)

            Dim sSqlQuery As String
            Try
                If Motor = "SQL" Then
                    sSqlQuery = "DROP FUNCTION  [" & nombreTf & "]"
                Else
                    sSqlQuery = "DROP FUNCTION  """ & SQLBaseDatos & """.""" & nombreTf & """"
                End If


                conexi.creaRegistro(sSqlQuery, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' <summary>
        ''' VERIFICA PROCEDIMIENTOS ALMACENADOS
        ''' </summary>
        Public Shared Function VerificaSP(ByVal nombreSp As String, ByRef tip As String) As Boolean

            Dim hayvalores As Boolean
            Try

                Dim resultado As Boolean = False
                Dim dttConsulta As New DataTable
                Dim sSqlQuery As String = String.Empty

                If Motor = "SQL" Then
                    sSqlQuery = "SELECT * FROM sys.objects WHERE type = 'P' AND name = '" & nombreSp & "'"
                Else
                    sSqlQuery = "Call """ & SQLBaseDatos & """.""VERIFICA_EXISTS_ALL"" ('" & nombreSp & "', '" & SQLBaseDatos & "', '" & tip & "')"
                End If

                dtls = conexi.obtenerColeccion(sSqlQuery, False)

                If dtls.Rows.Count > 0 Then
                    hayvalores = True
                End If

                Return hayvalores

            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function

        Public Shared Function VerificaTF(ByVal nombreTf As String) As Boolean

            Dim hayvalores As Boolean
            Try

                Dim resultado As Boolean = False
                Dim dttConsulta As New DataTable
                Dim sSqlQuery As String = String.Empty

                If Motor = "SQL" Then
                    sSqlQuery = "SELECT * FROM sys.objects WHERE type = 'TF' AND name = '" & nombreTf & "'"
                Else
                    sSqlQuery = "Call """ & SQLBaseDatos & """.""" + cTSQLAFI.NAME_VERIFICA_EXISTS_PROCEDURE_HANA + """ ('" & nombreTf & "', '" & SQLBaseDatos & "')"
                End If

                dtls = conexi.obtenerColeccion(sSqlQuery, False)

                If dtls.Rows.Count > 0 Then
                    hayvalores = True
                End If

                Return hayvalores

            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function


        ''' <summary>
        ''' CREA SP SEGUN TEXTO ENTREGADO
        ''' </summary>
        Public Shared Function CreateSP(ByVal SpText As String) As Boolean

            Dim sSqlQuery As String

            Try
                sSqlQuery = SpText

                conexi.creaRegistro(sSqlQuery, False)

            Catch ex As Exception
                Throw ex
            End Try

            Return True
        End Function

        Public Shared Function CreateTType(ByVal SpText As String) As Boolean

            Dim sSqlQuery As String
            Try
                sSqlQuery = SpText


                conexi.creaRegistro(sSqlQuery, False)
            Catch ex As Exception
                Throw ex
            End Try

            Return True
        End Function
        Public Shared Sub EliminaTType(ByVal nombreSp As String)

            Dim sSqlQuery As String
            Try
                If Motor = "SQL" Then
                    sSqlQuery = "" 'no aplica en sql
                Else
                    sSqlQuery = "DROP  type  """ & SQLBaseDatos & """.""" & nombreSp & """"
                End If


                conexi.creaRegistro(sSqlQuery, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function VerificaTType(ByVal nombreSp As String) As Boolean

            Dim hayvalores As Boolean
            Try

                Dim resultado As Boolean = False
                Dim dttConsulta As New DataTable
                Dim sSqlQuery As String = String.Empty

                If Motor = "SQL" Then
                    sSqlQuery = "" 'no aplica sql

                Else
                    sSqlQuery = "SELECT TABLE_NAME FROM tables " & vbCr &
                "WHERE is_user_defined_type ='TRUE' and SCHEMA_NAME='" & SQLBaseDatos.ToUpper & "' and TABLE_NAME='" & nombreSp.ToUpper & "'"
                End If

                dtls = conexi.obtenerColeccion(sSqlQuery, False)

                If dtls.Rows.Count > 0 Then
                    hayvalores = True
                End If

                Return hayvalores

            Catch ex As Exception
                Return False
            End Try

            Return True

        End Function
        ''' <summary>
        ''' LEE USUSARIOS CONECTADOS A BD
        ''' </summary>
        Public Shared Function LeeUsuariosConectadosDB() As Hashtable

            Dim hasValores As Hashtable = New Hashtable()

            Try
                Dim sSqlQuery As String

                If Motor = "SQL" Then
                    sSqlQuery = cTSQLAFI.TEXT_SEL_AFI_SELECT_USUCONECT
                Else
                    sSqlQuery = " " + vbCrLf +
                               "      CREATE TABLE """ + SQLBaseDatos + """.""tbl_usuarios_conectados""                         " + vbCrLf +
                               "      (                                                                                         " + vbCrLf +
                               "       ""spid"" smallint,                                                                           " + vbCrLf +
                               "       ""ecid"" smallint,                                                                           " + vbCrLf +
                               "       ""status"" nchar(30),                                                                      " + vbCrLf +
                               "       ""loginname"" nchar(128),                                                                    " + vbCrLf +
                               "       ""hostname"" nchar(128),                                                                     " + vbCrLf +
                               "       ""blk"" char(5),                                                                             " + vbCrLf +
                               "       ""dbname"" nchar(128),                                                                       " + vbCrLf +
                               "       ""cmd"" nchar(16),                                                                           " + vbCrLf +
                               "       ""request_id"" nchar(16)                                                                     " + vbCrLf +
                               "      )                                                                                         " + vbCrLf +
                               "                                                                                                " + vbCrLf +
                               "              INSERT """ + SQLBaseDatos + """.""tbl_usuarios_conectados""                       " + vbCrLf +
                               "              CALL """ + SQLBaseDatos + """.""sp_who""                                          " + vbCrLf +
                               "                                                                                                " + vbCrLf +
                               "      Select DISTINCT                                                                           " + vbCrLf +
                               "              ""loginname"",                                                                    " + vbCrLf +
                               "              ""hostname""                                                                      " + vbCrLf +
                               "      FROM  """ & SQLBaseDatos & """.""tbl_usuarios_conectados""                                " + vbCrLf +
                               "      WHERE ""dbname"" = '" + SQLBaseDatos + "'                                                 " + vbCrLf +
                               "                                                                                                " + vbCrLf +
                               "DROP TABLE """ & SQLBaseDatos & """.""tbl_usuarios_conectados""                                 " + vbCrLf

                End If


                dtls = conexi.obtenerColeccion(sSqlQuery, False)

                For Each row As DataRow In dtls.Rows
                    hasValores.Add(row(0), row(1))
                Next

                Return hasValores

            Catch ex As Exception
                Throw ex
            End Try

        End Function


    End Class

End Namespace
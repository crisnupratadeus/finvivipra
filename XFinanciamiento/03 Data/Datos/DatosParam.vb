﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Xml
Imports System.Globalization


Namespace Data
    Public Class DatosParam
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable
        Public Shared Function getCodeUserTable(tableName As String) As String
            Try
                Dim ostr As String
                ostr = "SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) from " & tableName
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function getRateImpMar(Impuesto As String) As String

            Dim ostr As String = String.Empty
            Try
                ostr = "SELECT ISNULL(Rate,0) AS RATE FROM OSTC WHERE Code = '" & Trim(Impuesto) & "'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return "0"
            End Try

        End Function

        Public Shared Function getEsImpMar() As String
            Try
                Dim ostr As String
                ostr = "select ISNULL(U_Value1,'N') as EsImpMar from [@EXX_CDPARAM] where U_Type = 'EsImpMar'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function




        Public Shared Function getImpMar() As String
            Try
                Dim ostr As String
                ostr = "select ISNULL(U_Value1,'') as ImpMar from [@EXX_CDPARAM] where U_Type = 'ImpMar'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getSeriesUsados() As String
            Try
                Dim ostr As String
                ostr = "select U_Value1 as Serie from [@EXX_CDPARAM] where U_Type = 'SNota4'"
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub InsertAutoIRecord(UDO As String, Item As String, Desc As String, DBAlias As String, Type As String)

            Try
                Dim sSqlInsert As String

                sSqlInsert = "             INSERT INTO dbo.[@EXX_AUTOI]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_UDO,                                                                                            " + vbCrLf +
                            "              U_Item,                                                                                            " + vbCrLf +
                             "              U_Desc,                                                                                            " + vbCrLf +
                              "              U_Alias,                                                                                            " + vbCrLf +
                               "              U_Type                                                                                            " + vbCrLf +
                           "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & UDO & "', '" & Item & "','" & Desc & "','" & DBAlias & "','" & Type & "' from dbo.[@EXX_AUTOI]"

                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub InsertUAutoRecord(UDO As String, GrpAuto As String, Item As String)

            Try
                Dim sSqlInsert As String

                sSqlInsert = "             INSERT INTO dbo.[@EXX_UAUTO]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_UDO,                                                                                            " + vbCrLf +
                            "              U_GrpAuto,                                                                                            " + vbCrLf +
                             "              U_Item                                                                                            " + vbCrLf +
                           "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & UDO & "', '" & GrpAuto & "','" & Item & "' from dbo.[@EXX_UAUTO]"

                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub InsertPmarca(usuario As String, marca As String)
            Try
                Dim sSqlInsert As String

                sSqlInsert = "INSERT INTO dbo.[@EXX_PMAR] " + vbCrLf +
                             "( " + vbCrLf +
                             "Code, " + vbCrLf +
                             "Name, " + vbCrLf +
                             "U_User, " + vbCrLf +
                             "U_Marca " + vbCrLf +
                             ") " + vbCrLf +
                             "SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code " + vbCrLf +
                             ",right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & usuario & "', '" & marca & "' from dbo.[@EXX_PMAR]"


                conexi.creaRegistro(sSqlInsert, False)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub InsertPAutoRecord(UDO As String, GrpAuto As String, Item As String, Descripcion As String)

            Try
                Dim sSqlInsert As String

                sSqlInsert = "             INSERT INTO dbo.[@EXX_PAUTO]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_UDO,                                                                                            " + vbCrLf +
                           "              U_GrpAuto,                                                                                            " + vbCrLf +
                           "              U_Item,                                                                                            " + vbCrLf +
                           "              U_ItemD                                                                                            " + vbCrLf +
                           "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & UDO & "', '" & GrpAuto & "','" & Item & "','" & Descripcion & "' from dbo.[@EXX_PAUTO]"

                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function GetGruposAutorizacionUser(usuario As String) As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT UserId,GroupId FROM USR7 where UserId = '" & usuario & "'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetPerfilesUser(usuario As String, Form As String) As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT DISTINCT A.U_Item FROM [@EXX_PAUTO] A INNER JOIN [USR7] B ON A.U_GrpAuto = B.GroupId WHERE B.UserId = '" & usuario & "' AND A.U_UDO = '" & Form & "'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub UpdateParameter(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "")
            Try

                Dim sSqlCommand As String
                Dim sSqlInsert As String
                If valor3 <> "" Then
                    sSqlCommand = "DELETE [@EXX_PARAM]  WHERE U_Type = '" & Type & "' and U_Value1='" & valor1 & "' and U_Value3='" & valor3 & "'"
                Else
                    sSqlCommand = "DELETE [@EXX_PARAM]  WHERE U_Type = '" & Type & "' and U_Value1='" & valor1 & "'"
                End If
                sSqlInsert = "             INSERT INTO dbo.[@EXX_PARAM]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_Type,                                                                                            " + vbCrLf +
                           "              U_Value1,                                                                                           " + vbCrLf +
                           "              U_Value2,                                                                                            " + vbCrLf +
         "              U_Value3,                                                                                            " + vbCrLf +
                           "              U_Active,                                                                                           " + vbCrLf +
                           "              U_UserName,                                                                                           " + vbCrLf +
                           "              U_CreateDate                                                                                           " + vbCrLf +
                            "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & Type & "', '" & valor1 & "', '" & valor2 & "', '" & valor3 & "'," & "1, '" & DiApp.UserName & "', '" & DateToString(Today) & "' from dbo.[@EXX_PARAM]"

                conexi.creaRegistro(sSqlCommand, False)
                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub EliminaUAutoRecord(UDO As String, GrpAuto As String, Item As String)
            Try
                Dim sSqlCommand As String

                sSqlCommand = "DELETE A FROM [@EXX_UAUTO] A INNER JOIN [OUGR] B ON A.U_GrpAuto = B.GroupId WHERE B.GroupName = '" & GrpAuto & "' AND A.U_UDO = '" & UDO & "' AND A.U_Item = '" & Item & "'"
                conexi.creaRegistro(sSqlCommand, False)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Shared Sub EliminaPAutoRecord(UDO As String, GrpAuto As String, Item As String)
            Try
                Dim sSqlCommand As String

                sSqlCommand = "DELETE A FROM [@EXX_PAUTO] A INNER JOIN [OUGR] B ON A.U_GrpAuto = B.GroupId WHERE B.GroupName = '" & GrpAuto & "' AND A.U_UDO = '" & UDO & "' AND A.U_Item = '" & Item & "'"
                conexi.creaRegistro(sSqlCommand, False)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Shared Sub EliminaPMarca(usuario As String, marca As String)
            Try
                Dim sSqlCommand As String

                sSqlCommand = "DELETE A FROM [@EXX_PMAR] A INNER JOIN [OMRC] B ON A.U_Marca = B.FirmCode WHERE A.U_User = '" & usuario & "' AND B.FirmName = '" & marca & "'"
                conexi.creaRegistro(sSqlCommand, False)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub


        Public Shared Sub BorraConcepto(Concepto As String)

            Try
                Dim sSqlCommand As String

                sSqlCommand = "delete from [@EXX_CDPARAM] where U_Type = 'REVAV' and U_Value1 = '" & Concepto & "'"
                conexi.creaRegistro(sSqlCommand, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Shared Sub UpdateParameterReva(Type As String, valor1 As String, Optional valor2 As String = "",
                                                 Optional valor3 As String = "", Optional valor4 As String = "",
                                                 Optional valor5 As String = "", Optional valor6 As String = "", Optional valor7 As String = "", Optional valor8 As String = "", Optional valor9 As String = "", Optional valor10 As String = "")
            Try

                Dim sSqlCommand As String
                Dim sSqlInsert As String

                sSqlCommand = "DELETE [@EXX_CDPARAM]  WHERE U_Type = '" & Type & "' and U_Value1='" & valor1 & "'"

                sSqlInsert = "             INSERT INTO dbo.[@EXX_CDPARAM]                                                                 " + vbCrLf +
                           "             (                                                                                                    " + vbCrLf +
                           "               Code,                                                                                              " + vbCrLf +
                           "  	            Name,                                                                                              " + vbCrLf +
                           "              U_Type,                                                                                            " + vbCrLf +
                           "              U_Value1,                                                                                           " + vbCrLf +
                           "              U_Value2,                                                                                            " + vbCrLf +
                           "              U_Value3,                                                                                            " + vbCrLf +
                            "              U_Value4,                                                                                            " + vbCrLf +
                             "              U_Value5,                                                                                            " + vbCrLf +
                              "              U_Value6,                                                                                            " + vbCrLf +
                               "              U_Value7,                                                                                            " + vbCrLf +
                                "              U_Value8,                                                                                            " + vbCrLf +
                                 "              U_Value9,                                                                                            " + vbCrLf +
                                  "              U_Value10,                                                                                            " + vbCrLf +
                           "              U_Active,                                                                                           " + vbCrLf +
                           "              U_UserName,                                                                                           " + vbCrLf +
                           "              U_CreateDate                                                                                           " + vbCrLf +
                            "             )                                                                                                    " + vbCrLf +
                           "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                           "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

                sSqlInsert = sSqlInsert & "'" & Type & "','" & valor1 & "','" & valor2 & "','" & valor3 & "','" & valor4 & "','" & valor5 & "','" & valor6 & "','" & valor7 & "','" & valor8 & "','" & valor9 & "','" & valor10 & "','" & "1" & "','" & DiApp.UserName & "', '" & DateToString(Today) & "' from dbo.[@EXX_CDPARAM]"

                conexi.creaRegistro(sSqlCommand, False)
                conexi.creaRegistro(sSqlInsert, False)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function GetMaxSysNbrSer() As Integer
            Try
                Dim ostr As String
                ostr = ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function GetVehProp() As DataTable
            Try
                Dim ostr As String
                ostr = "select U_value1 as Prop from [@EXX_CDPARAM] where U_Type = 'Vehc'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
     

        Public Shared Function GetSeriesReva() As DataTable
            Try
                Dim ostr As String
                ostr = "select Series,SeriesName from NNM1 where ObjectCode = '162' and Locked = 'N'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetSeries(Optional Sucursal As String = "") As DataTable
            Try
                Dim ostr As String
                ostr = "select Series,SeriesName from NNM1 where ObjectCode = '14' and Locked = 'N'"
                If Sucursal <> "" Then
                    ostr = ostr & " and BPLid = '" & Sucursal & "'"
                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCuentasReva(Concepto As String) As DataTable
            Dim ostr As String
            ostr = "select ISNULL(U_Value2,'') as CtaA,ISNULL(U_Value3,'') as CtaD,ISNULL(U_Value4,'') as Dim1,ISNULL(U_Value5,'') as Dim2,ISNULL(U_Value6,'') as Dim3,ISNULL(U_Value7,'') as Dim4,ISNULL(U_Value8,'') as Dim5 from [@EXX_CDPARAM] where U_Type = 'REVAV' and U_Value1 = '" & Concepto & "'"

            Return conexi.obtenerColeccion(ostr)
        End Function


        Public Shared Function GetDiasVen() As Integer
            Try
                Dim oRec As SAPbobsCOM.Recordset
                Dim ostr As String
                Dim dias As Integer
                oRec = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                ostr = "select ISNULL(U_Value1,0) as Dias from [@EXX_CDPARAM] where U_Type = 'Term'"
                oRec.DoQuery(ostr)
                If oRec.RecordCount > 0 Then
                    If Trim(oRec.Fields.Item("Dias").Value) = "" Then
                        Return 0
                    Else
                        dias = Convert.ToInt64(Trim(oRec.Fields.Item("Dias").Value), CultureInfo.InvariantCulture)
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec)
                        Return dias
                    End If
                Else
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRec)
                    Return 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function DateToString(dDate As Date, Optional withHour As Boolean = False) As String
            Try
                Dim strDate As String = dDate.Year & Right(CStr("0" & dDate.Month), 2) & Right(CStr("0" & dDate.Day), 2)
                If withHour = True Then
                    strDate = " " & strDate & Right(CStr("0" & dDate.Hour), 2) & ":" & Right(CStr("0" & dDate.Minute), 2)
                End If

                Return strDate

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetItmGroups() As DataTable
            Try
                Dim ostr As String
                ostr = "select ItmsGrpCod,ItmsGrpNam from OITB"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetModelosStk(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                Dim ostr As String
                Dim cond As Boolean = False

                ostr = "SELECT DISTINCT ISNULL(A.FrgnName, '') AS ItemCode, ISNULL(A.FrgnName, '') AS ItemName FROM OITM A INNER JOIN [@EXX_VEHC] B ON A.ItemCode=B.U_Model  WHERE A.QryGroup64='Y' AND B.U_SaleSts IN('1','3') "
                If Not (Marca = Nothing) Or Not (Estilo = Nothing) Then
                    ostr = ostr + " AND "
                End If

                If Not Marca = Nothing Then
                    ostr = ostr + " A.FirmCode = '" & Marca & "'"
                    cond = True
                End If

                If Not Estilo = Nothing Then
                    If cond = True Then
                        ostr = ostr + " AND "
                    End If
                    ostr = ostr + " A.U_Family = '" & Estilo & "'"
                    cond = True
                End If

                ostr = ostr + " ORDER BY 2 ASC"

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function GetModelosLst(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                Dim ostr As String
                Dim cond As Boolean = False

                ostr = "SELECT DISTINCT ISNULL(A.FrgnName, '') AS ItemCode, ISNULL(A.FrgnName, '') AS ItemName FROM OITM A INNER JOIN [@EXX_VEHC] B ON A.ItemCode=B.U_Model  WHERE A.QryGroup64='Y' AND B.U_SaleSts IN('1', '2', '3') "
                If Not (Marca = Nothing) Or Not (Estilo = Nothing) Then
                    ostr = ostr + " AND "
                End If

                If Not Marca = Nothing Then
                    ostr = ostr + " A.FirmCode = '" & Marca & "'"
                    cond = True
                End If

                If Not Estilo = Nothing Then
                    If cond = True Then
                        ostr = ostr + " AND "
                    End If
                    ostr = ostr + " A.U_Family = '" & Estilo & "'"
                    cond = True
                End If

                ostr = ostr + " ORDER BY 2 ASC"

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function GetModelos(Optional Marca As String = Nothing, Optional Estilo As String = Nothing) As DataTable
            Try
                Dim ostr As String
                Dim cond As Boolean = False

                ostr = "select ISNULL(ItemCode,'') as ItemCode, ISNULL(ItemName,'') as ItemName from OITM WHERE QryGroup64 = 'Y'  "
                If Not (Marca = Nothing) Or Not (Estilo = Nothing) Then
                    ostr = ostr + " AND "
                End If

                If Not Marca = Nothing Then
                    ostr = ostr + " FirmCode = '" & Marca & "'"
                    cond = True
                End If

                If Not Estilo = Nothing Then
                    If cond = True Then
                        ostr = ostr + " AND "
                    End If
                    ostr = ostr + " U_Family = '" & Estilo & "'"
                    cond = True
                End If

                ostr = ostr + " ORDER BY ItemName ASC"

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetItemsAutorizacion(Formulario As String) As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT U_Item,U_Alias FROM [@EXX_AUTOI] WHERE U_UDO = '" & Formulario & "' ORDER BY U_Alias ASC"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetUDOAutorizacion() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT DISTINCT U_UDO FROM [@EXX_AUTOI] ORDER BY U_UDO ASC"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetManufacturers() As DataTable
            Try
                Dim ostr As String
                ostr = "select FirmCode,FirmName from OMRC ORDER BY FirmName ASC"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetManufacturersLst(Optional usuario As String = "") As DataTable
            Try
                Dim ostr As String

                If usuario = "" Then
                    ostr = "select DISTINCT A.FirmCode, A.FirmName from OMRC A INNER JOIN [@EXX_VEHC] B on A.FirmCode = B.U_Brand WHERE B.U_SaleSts in ('1','2','3') ORDER BY A.FirmName ASC"
                Else
                    ostr = "SELECT DISTINCT B.FirmCode,B.FirmName FROM [@EXX_PMAR] A INNER JOIN [OMRC] B ON A.U_Marca = B.FirmCode INNER JOIN [@EXX_VEHC] C ON B.FirmCode = C.U_Brand WHERE A.U_User = '" & Trim(usuario) & "' AND C.U_SaleSts IN ('1','2','3')"
                End If


                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetManufacturersStk(Optional usuario As String = "") As DataTable
            Try
                Dim ostr As String

                If usuario = "" Then
                    ostr = "select DISTINCT A.FirmCode, A.FirmName from OMRC A INNER JOIN [@EXX_VEHC] B on A.FirmCode = B.U_Brand WHERE B.U_SaleSts in ('1','3') ORDER BY A.FirmName ASC"
                Else
                    ostr = "SELECT DISTINCT B.FirmCode,B.FirmName FROM [@EXX_PMAR] A INNER JOIN [OMRC] B ON A.U_Marca = B.FirmCode INNER JOIN [@EXX_VEHC] C ON B.FirmCode = C.U_Brand WHERE A.U_User = '" & Trim(usuario) & "' AND C.U_SaleSts IN ('1','3')"
                End If


                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstilos(Marca As String) As DataTable
            Try
                Dim ostr As String
                If Marca = "" Then
                    ostr = "select Code,Name from [@EXX_FAMI] ORDER BY Name ASC"
                Else
                    ostr = "select Code,Name from [@EXX_FAMI] where U_Brand = '" & Marca & "' ORDER BY Name ASC"
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetEstilosLst(Marca As String) As DataTable
            Try
                Dim ostr As String
                If Marca = "" Then
                    ostr = "SELECT DISTINCT A.Code, A.Name FROM [@EXX_FAMI] A INNER JOIN [@EXX_VEHC] B ON A.Code=B.U_FamCod WHERE B.U_SaleSts IN('1', '2', '3') ORDER BY A.Name ASC"
                Else
                    ostr = "SELECT DISTINCT A.Code, A.Name FROM [@EXX_FAMI] A INNER JOIN [@EXX_VEHC] B ON A.Code=B.U_FamCod  WHERE B.U_SaleSts IN('1', '2', '3')  AND A.U_Brand='" & Marca & "' ORDER BY A.Name ASC"
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetEstilosStk(Marca As String) As DataTable
            Try
                Dim ostr As String
                If Marca = "" Then
                    ostr = "SELECT DISTINCT A.Code, A.Name FROM [@EXX_FAMI] A INNER JOIN [@EXX_VEHC] B ON A.Code=B.U_FamCod WHERE B.U_SaleSts IN('1','3') ORDER BY A.Name ASC"
                Else
                    ostr = "SELECT DISTINCT A.Code, A.Name FROM [@EXX_FAMI] A INNER JOIN [@EXX_VEHC] B ON A.Code=B.U_FamCod WHERE B.U_SaleSts IN('1','3') AND A.U_Brand='" & Marca & "' ORDER BY A.Name ASC"
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetUDTMenu(ByRef oApp As SAPbouiCOM.Application, ByVal sVal As String, MenuID As String) As String
            Dim oMenu As SAPbouiCOM.MenuItem
            Dim sMenuUID As String = ""
            Dim s As String
            Dim i As Integer
            Try
                oMenu = oApp.Menus.Item(MenuID)
                For i = 0 To oMenu.SubMenus.Count - 1
                    If oMenu.SubMenus.Item(i).String.Length < sVal.Length Then
                        s = oMenu.SubMenus.Item(i).String
                        If String.Compare(s, sVal, True) = 0 Then
                            sMenuUID = oMenu.SubMenus.Item(i).UID
                            Return sMenuUID
                        End If
                    Else
                        s = oMenu.SubMenus.Item(i).String.Substring(0, sVal.Length)
                        If String.Compare(s, sVal, True) = 0 Then
                            sMenuUID = oMenu.SubMenus.Item(i).UID
                            Return sMenuUID
                        End If
                    End If
                Next
                Return ""
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return ""
            End Try
        End Function
        Public Shared Function GetCategorias() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_CATG]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetMarcaMotor() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_MOTB]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetConceptosV() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT U_Value1 as 'Concepto',U_Value2 as 'CuentaA',U_Value3 as 'CuentaD',U_Value4 as 'Dim 1',U_Value5 as 'Dim 2',U_Value6 as 'Dim 3',U_Value7 as 'Dim 4',U_Value8 as 'Dim 5' FROM [@EXX_CDPARAM] WHERE U_Type = 'REVAV'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetColorE() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_COLE]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetColorI() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_COLI]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstVen() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLSS] WHERE U_Visual = 'Y'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstVenStk() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLSS] WHERE U_Visual = 'Y' AND Code in ('1','3')"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstVenLst() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLSS] WHERE U_Visual = 'Y' AND Code in ('1','2','3')"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstVeh() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_VEHS] WHERE U_Visual = 'Y'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetEstPat() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_PATS]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTecho() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_ROOF]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTipVen() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_SLST]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCombustible() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_FUEL]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCabina() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_COPT]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTraccion() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_TRAC]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetTransmision() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_TRAN]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetNormas() As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_EMNR]"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetForms(Addon As String) As DataTable
            Try
                Dim ostr As String
                ostr = "select Code,Name from [@EXX_TABL] where U_Addon = '" & Addon & "' and U_Activa = 'Y'"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetBranchsUsr(ByVal Usr As String) As DataTable
            Try
                Dim ostr As String
                ostr = "select A.BPLId, A.BPLName from OBPL A inner join USR6 B on A.BPLId = B.BPLId where A.Disabled='N' and B.UserCode = '" & Usr & "'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetPriceLists() As DataTable
            Try
                Dim ostr As String
                ostr = "select ListNum, ListName from OPLN"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetBranchs() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT BPLId, BPLName  FROM OBPL T0 where Disabled='N'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Private Shared Function ConvertToRecordset(ByVal inTable As DataTable) As ADODB.Recordset
        '    Dim result As New ADODB.Recordset()
        '    result.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        '    Dim resultFields As ADODB.Fields = result.Fields
        '    Dim inColumns As System.Data.DataColumnCollection = inTable.Columns

        '    For Each inColumn As DataColumn In inColumns
        '        resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), _
        '             inColumn.MaxLength, IIf(inColumn.AllowDBNull, _
        '             ADODB.FieldAttributeEnum.adFldIsNullable, _
        '             ADODB.FieldAttributeEnum.adFldUnspecified), Nothing)
        '    Next

        '    result.Open(System.Reflection.Missing.Value, _
        '           System.Reflection.Missing.Value, ADODB.CursorTypeEnum.adOpenStatic, _
        '           ADODB.LockTypeEnum.adLockOptimistic, 0)

        '    For Each dr As DataRow In inTable.Rows
        '        result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value)
        '        For columnIndex As Integer = 0 To inColumns.Count - 1
        '            resultFields(columnIndex).Value = dr(columnIndex)
        '        Next
        '    Next
        '    Return result
        'End Function

        'Private Shared Function TranslateType(ByVal columnType As Type) As ADODB.DataTypeEnum
        '    Select Case columnType.UnderlyingSystemType.ToString()
        '        Case "System.Boolean"
        '            Return ADODB.DataTypeEnum.adBoolean
        '        Case "System.Byte"
        '            Return ADODB.DataTypeEnum.adUnsignedTinyInt
        '        Case "System.Char"
        '            Return ADODB.DataTypeEnum.adChar
        '        Case "System.DateTime"
        '            Return ADODB.DataTypeEnum.adDate
        '        Case "System.Decimal"
        '            Return ADODB.DataTypeEnum.adCurrency
        '        Case "System.Double"
        '            Return ADODB.DataTypeEnum.adDouble
        '        Case "System.Int16"
        '            Return ADODB.DataTypeEnum.adSmallInt
        '        Case "System.Int32"
        '            Return ADODB.DataTypeEnum.adInteger
        '        Case "System.Int64"
        '            Return ADODB.DataTypeEnum.adBigInt
        '        Case "System.SByte"
        '            Return ADODB.DataTypeEnum.adTinyInt
        '        Case "System.Single"
        '            Return ADODB.DataTypeEnum.adSingle
        '        Case "System.UInt16"
        '            Return ADODB.DataTypeEnum.adUnsignedSmallInt
        '        Case "System.UInt32"
        '            Return ADODB.DataTypeEnum.adUnsignedInt
        '        Case "System.UInt64"
        '            Return ADODB.DataTypeEnum.adUnsignedBigInt
        '        Case "System.String"
        '            Return ADODB.DataTypeEnum.adVarChar
        '        Case Else
        '            Return ADODB.DataTypeEnum.adVarChar
        '    End Select
        'End Function

        Public Shared Function LoadOACT_UDF() As DataTable
            Try
                Dim ostr As String
                ostr = "select AliasID, Descr  from CUFD where TableID='OACT'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadJDT1_UDF() As DataTable
            Try
                Dim ostr As String
                ostr = "select AliasID, Descr  from CUFD where TableID='JDT1'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadOITM_UDF() As DataTable
            Try
                Dim ostr As String
                ostr = "select AliasID, Descr  from CUFD where TableID='OITM'"
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function LoadODIM() As DataTable
            Try
                Dim ostr As String
                ostr = "SELECT DimCode, DimDesc FROM ODIM T0"
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CargaDatos(codeLC As String, CodigoPC As String, NumDoc As String) As DataTable
            Try
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = "SELECT U_Codigo, U_Plantilla, U_Valor FROM	[@EXX_FIPLD1] WHERE " &
                       " U_CodeLineaCred = '" & codeLC & "' And U_Plantilla = '" & CodigoPC & "'"
                Else
                    ostr = "SELECT ""U_Codigo"", ""U_Plantilla"", ""U_Valor"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"" WHERE " &
                      " ""U_CodeLineaCred"" = '" & codeLC & "' And ""U_Plantilla"" = '" & CodigoPC & "' ORDER BY  ""Code"" ASC" 'mauricio
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

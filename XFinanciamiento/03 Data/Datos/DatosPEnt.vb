﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

Namespace Data


    Public Class DatosPEnt
        Public Shared conexi As Conexiones = New Conexiones

        Public Shared Function GetCuenta_Diferencia_Precio(ItemCode As String, WhsCode As String) As String
            Try
                Dim query As String = ""
                Dim res As String = ""

                Dim ItmsGrpCod As String = ""

                Dim tipoCuentasItem As String = ""
                If Motor = "SQL" Then
                    tipoCuentasItem = Trim(conexi.obtenerValor(sqlPEnt.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                Else
                    tipoCuentasItem = Trim(conexi.obtenerValor(hanaAjusteTC.Get_tipo_de_cuenta_item_TEXT(ItemCode)))
                End If
                If tipoCuentasItem <> "" Then
                    Select Case tipoCuentasItem
                        Case "W"
                            If Motor = "SQL" Then
                                query = Trim(sqlPEnt.Get_Cuenta_Item_de_almacen_TEXT(WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_almacen_TEXT(WhsCode))
                            End If
                        Case "C"

                            If Motor = "SQL" Then
                                ItmsGrpCod = Trim(conexi.obtenerValor(sqlAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(sqlPEnt.Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod))
                            Else
                                ItmsGrpCod = Trim(conexi.obtenerValor(hanaAjusteTC.Get_grupo_de_item_TEXT(ItmsGrpCod)))
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod))
                            End If
                        Case "L"
                            If Motor = "SQL" Then
                                query = Trim(sqlPEnt.Get_Cuenta_Item_de_Item_TEXT(ItemCode, WhsCode))
                            Else
                                query = Trim(hanaAjusteTC.Get_Cuenta_Item_de_Item_TEXT(ItemCode, WhsCode))
                            End If
                    End Select

                    res = Trim(conexi.obtenerValor(query))
                End If
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function EsDocumentoDeVehiculos(ItemCodes As String) As Boolean
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlPEnt.EsDocumentoDeVehiculos_TEXT(ItemCodes)
                Else

                End If

                Dim dRes As Integer = conexi.obtenerValor(ostr)
                If dRes > 0 Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function EsVehiculo(ItemCode As String) As Boolean
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlPEnt.EsVehiculo_TEXT(ItemCode)
                Else

                End If
                Dim Res As String = Trim(conexi.obtenerValor(ostr))
                If Res = "Y" Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetDecimalsMoneda(Moneda As String) As Integer
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetDecimalsMoneda(Moneda)
                Else

                End If
                Try
                    Return conexi.obtenerValor(ostr)
                Catch ex As Exception
                    Throw ex
                    Return -2
                End Try

            Catch ex As Exception
                Throw ex
                Return -2
            End Try
        End Function


        Public Shared Function GetSumDec() As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetSumDec_TEXT
                Else

                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub ActualizaCampoUsuario(Docentry As String, CampoUsuario As String, Valor As String, Tabla As String)
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.ActualizaCampoUsuario_TEXT(Docentry, CampoUsuario, Valor, Tabla)
                Else

                End If
                conexi.creaRegistro(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function GetDocEntryFromXML(sXML As String) As Integer
            Dim xDoc As XmlDocument = New XmlDocument()
            xDoc.LoadXml(sXML)
            Dim sDocEntry As String = xDoc.SelectSingleNode("DocumentParams/DocEntry").InnerText
            If sDocEntry <> "" Then
                Return Convert.ToInt32(sDocEntry)
            Else
                Return -1
            End If
        End Function

        Public Shared Function GetExistenciaVehc(itemcode As String, serie As String) As Integer
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetExistenciaVehc(itemcode, serie)
                Else

                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return -1
            End Try
        End Function


        Public Shared Function GetBoxPatentFromDoc(DocType As String, DocEntry As String, ItemCode As String, LineaBase As Double) As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetBoxPatentFromDoc(DocType, DocEntry, ItemCode, LineaBase)
                Else

                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GetPEntDocEntry(DocNum As String, Serie As String) As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetPEntDocEntry(DocNum, Serie)
                Else

                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function CrearAsiento(ByRef oData As DataTable, ByRef transID As String) As Boolean
            Try
                Dim vJE As SAPbobsCOM.JournalEntries
                vJE = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)

                Dim res As Boolean
                Dim itemCode As String
                Dim lastitemCode As String
                Dim Cuenta As String
                Dim lastCuenta As String = String.Empty
                Dim ContCuenta As String
                Dim EsDebito As Boolean
                Dim Monto As Decimal
                Dim BoxPatent As String
                Dim OcrCode As String
                Dim OcrCode2 As String
                Dim OcrCode3 As String
                Dim OcrCode4 As String
                Dim OcrCode5 As String
                Dim FechaContabilizacion As String
                Dim Glosa As String
                Dim Moneda As String
                Dim iSumdec As Integer
                Dim Serie As String
                Dim BaseEntry As String
                Dim ref2 As String

                Dim AcumuladorMonto As Decimal = 0
                Dim AcumuladorMontoTotal As Decimal = 0
                Dim LineNum As Integer = 0
                Dim dmonto As Decimal = 0
                Dim montoReal As Decimal = 0

                Dim SegundaCuenta As String
                Dim lastSegundaCuenta As String

                Dim oDataView As New DataView(oData)
                oDataView.Sort = "Cuenta ASC"


                For Each row As DataRowView In oDataView
                    FechaContabilizacion = row("FecCont")
                    Glosa = row("Glosa")
                    iSumdec = row("iSumdec")
                    Serie = row("Serie")
                    BaseEntry = row("PreEntry")

                    If LineNum = 0 Then
                        vJE.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.ReferenceDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Series = Serie
                        If Glosa <> "" Then vJE.Memo = Glosa
                        'vJE.Reference = comentario
                        vJE.UserFields.Fields.Item("U_RefImp").Value = "Y"
                        vJE.UserFields.Fields.Item("U_BaseEntry").Value = CStr(BaseEntry)
                        lastCuenta = row("Cuenta")
                    End If

                    itemCode = row("ItemCode")
                    Cuenta = row("Cuenta")
                    SegundaCuenta = row("ContCue")

                    EsDebito = row("EsDebit")
                    Moneda = row("Moneda")
                    If Moneda = MonedaLocal Or Moneda = "##" Then
                        Monto = row("CostTLC")
                    Else
                        Monto = row("CostTFC")
                    End If
                    BoxPatent = row("BoxPatent")
                    ref2 = row("BoxPatent")
                    OcrCode = row("OcrCode")
                    OcrCode2 = row("OcrCode2")
                    OcrCode3 = row("OcrCode3")
                    OcrCode4 = row("OcrCode4")
                    OcrCode5 = row("OcrCode5")

                    If Cuenta = lastCuenta Then

                        'Dim ssppss As String = vJE.GetAsXML()

                        dmonto = Monto
                        If dmonto <> 0 Then

                            Call vJE.Lines.Add()
                            Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                            LineNum = LineNum + 1

                            vJE.Lines.AccountCode = Cuenta
                            If dmonto <> 0 Then
                                If Moneda = MonedaLocal Or Moneda = "##" Then
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    AcumuladorMonto = AcumuladorMonto + montoReal
                                    AcumuladorMontoTotal = AcumuladorMontoTotal + montoReal
                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.Debit = montoReal
                                        Else
                                            vJE.Lines.Credit = -montoReal
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.Credit = montoReal
                                        Else
                                            vJE.Lines.Debit = -montoReal
                                        End If
                                    End If
                                Else
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    AcumuladorMonto = AcumuladorMonto + montoReal
                                    AcumuladorMontoTotal = AcumuladorMontoTotal + montoReal
                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.FCDebit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCCredit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.FCCredit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCDebit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    End If
                                End If
                            End If


                            vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ShortName = Cuenta
                            vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                            vJE.Lines.LineMemo = Left(Glosa, 50)

                            If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                            If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                            If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                            If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                            If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                            If ref2 <> "" Then vJE.Lines.Reference2 = ref2

                        End If
                        '-------------------------------
                        If dmonto <> 0 Then

                            Call vJE.Lines.Add()
                            Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                            LineNum = LineNum + 1

                            vJE.Lines.AccountCode = SegundaCuenta
                            EsDebito = Not EsDebito
                            If dmonto <> 0 Then
                                If Moneda = MonedaLocal Or Moneda = "##" Then
                                    montoReal = Math.Round(dmonto, iSumdec)

                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.Debit = montoReal
                                        Else
                                            vJE.Lines.Credit = -montoReal
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.Credit = montoReal
                                        Else
                                            vJE.Lines.Debit = -montoReal
                                        End If
                                    End If
                                Else
                                    montoReal = Math.Round(dmonto, iSumdec)

                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.FCDebit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCCredit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.FCCredit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCDebit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    End If
                                End If
                            End If


                            vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ShortName = SegundaCuenta
                            vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                            vJE.Lines.LineMemo = Left(Glosa, 50)

                            If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                            If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                            If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                            If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                            If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                            If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                        End If
                    Else
                        dmonto = AcumuladorMonto


                        If dmonto <> 0 Then
                            EsDebito = True
                            Call vJE.Lines.Add()
                            Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                            LineNum = LineNum + 1

                            vJE.Lines.AccountCode = lastCuenta
                            If dmonto <> 0 Then
                                If Moneda = MonedaLocal Or Moneda = "##" Then
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.Debit = montoReal
                                        Else
                                            vJE.Lines.Credit = -montoReal
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.Credit = montoReal
                                        Else
                                            vJE.Lines.Debit = -montoReal
                                        End If
                                    End If
                                Else
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.FCDebit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCCredit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.FCCredit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCDebit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    End If
                                End If
                            End If


                            vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ShortName = lastCuenta
                            vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)

                            If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                            If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                            If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                            If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                            If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                            If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                        End If

                        AcumuladorMonto = 0

                        'SegundaCuenta = row("ContCue")
                        'EsDebito = row("EsDebit")
                        'Moneda = row("Moneda")
                        'If Moneda = MonedaLocal Or Moneda = "##" Then
                        '    Monto = row("CostTLC")
                        'Else
                        '    Monto = row("CostTFC")
                        'End If
                        'BoxPatent = row("BoxPatent")
                        'OcrCode = row("OcrCode")
                        'OcrCode2 = row("OcrCode2")
                        'OcrCode3 = row("OcrCode3")
                        'OcrCode4 = row("OcrCode4")
                        'OcrCode5 = row("OcrCode5")


                        'Dim ssppss As String = vJE.GetAsXML()


                        dmonto = Monto
                        If dmonto <> 0 Then

                            Call vJE.Lines.Add()
                            Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                            LineNum = LineNum + 1

                            vJE.Lines.AccountCode = Cuenta
                            If dmonto <> 0 Then
                                If Moneda = MonedaLocal Or Moneda = "##" Then
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    AcumuladorMonto = AcumuladorMonto + montoReal
                                    AcumuladorMontoTotal = AcumuladorMontoTotal + montoReal
                                    If Not EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.Debit = montoReal
                                        Else
                                            vJE.Lines.Credit = -montoReal
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.Credit = montoReal
                                        Else
                                            vJE.Lines.Debit = -montoReal
                                        End If
                                    End If
                                Else
                                    montoReal = Math.Round(dmonto, iSumdec)
                                    AcumuladorMonto = AcumuladorMonto + montoReal
                                    AcumuladorMontoTotal = AcumuladorMontoTotal + montoReal
                                    If Not EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.FCDebit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCCredit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.FCCredit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCDebit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    End If
                                End If
                            End If


                            vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ShortName = Cuenta
                            vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                            vJE.Lines.LineMemo = Left(Glosa, 50)

                            If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                            If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                            If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                            If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                            If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                            If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                        End If
                        '-------------------------------
                        If dmonto <> 0 Then

                            Call vJE.Lines.Add()
                            Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                            LineNum = LineNum + 1

                            vJE.Lines.AccountCode = SegundaCuenta
                            EsDebito = EsDebito
                            If dmonto <> 0 Then
                                If Moneda = MonedaLocal Or Moneda = "##" Then
                                    montoReal = Math.Round(dmonto, iSumdec)

                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.Debit = montoReal
                                        Else
                                            vJE.Lines.Credit = -montoReal
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.Credit = montoReal
                                        Else
                                            vJE.Lines.Debit = -montoReal
                                        End If
                                    End If
                                Else
                                    montoReal = Math.Round(dmonto, iSumdec)

                                    If EsDebito = False Then 'CNP
                                        If montoReal > 0 Then
                                            vJE.Lines.FCDebit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCCredit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    Else
                                        If montoReal > 0 Then
                                            vJE.Lines.FCCredit = montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        Else
                                            vJE.Lines.FCDebit = -montoReal
                                            vJE.Lines.FCCurrency = Moneda
                                        End If
                                    End If
                                End If
                            End If


                            vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.ShortName = SegundaCuenta
                            vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                            vJE.Lines.LineMemo = Left(Glosa, 50)

                            If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                            If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                            If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                            If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                            If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                            If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                        End If

                    End If
                    lastCuenta = Cuenta
                Next

                dmonto = AcumuladorMonto


                If dmonto <> 0 Then
                    EsDebito = True
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = Cuenta
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = False Then 'CNP
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = False Then 'CNP
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If


                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ShortName = Cuenta
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                End If
                '----------------------------------------------

                dmonto = AcumuladorMontoTotal

                If dmonto <> 0 Then
                    EsDebito = False
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = SegundaCuenta
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = False Then 'CNP
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = False Then 'CNP
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If


                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ShortName = SegundaCuenta
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    If ref2 <> "" Then vJE.Lines.Reference2 = ref2
                End If


                Dim iError As Integer = vJE.Add()

                If iError <> 0 Then
                    'Dim strxml As String
                    'strxml = vJE.GetAsXML
                    Dim sError As String = String.Empty
                    DiApp.GetLastError(iError, sError)
                    res = False
                    Throw New Exception("[CD] Problema al crear asiento contable: " + sError)
                Else
                    'Dim strxml As String
                    'strxml = vJE.GetAsXML
                    DiApp.GetNewObjectCode(transID)
                    res = True
                End If
                If Not IsNothing(vJE) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(vJE)
                    vJE = Nothing
                    oMc = Nothing
                End If
                Return res

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Shared Function GetCuentaCostosPEnt() As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetCuentaCostosPEnt()
                Else

                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Shared Function GrabaDTPEntLog(PreEntry As String, DocOri As String, OriType As String, ItemCode As String, ItemName As String, Almacen As String, Precio As Double, Cuenta As String, ContCue As String, EsDebit As String, CostTLC As Double, CostTFC As Double, BoxPatent As String, OcrCode As String, OcrCode2 As String, OcrCode3 As String, OcrCode4 As String, OcrCode5 As String, Moneda As String, Serie As String, FecCont As String, iSumdec As String, Glosa As String) As Boolean

            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GrabaDTPEntLog(PreEntry, DocOri, OriType, ItemCode, ItemName, Almacen, Precio, Cuenta, ContCue, EsDebit, CostTLC, CostTFC, BoxPatent, OcrCode, OcrCode2, OcrCode3, OcrCode4, OcrCode5, Moneda, Serie, FecCont, iSumdec, Glosa)
                Else

                End If

                Try
                    conexi.creaRegistro(ostr)
                Catch ex As Exception
                    Throw ex
                    Return False
                End Try

                Return True

            Catch ex As Exception
                Throw ex
                Return False
            End Try

        End Function

        Public Shared Function GetCostosOALC(AlcCode As String) As DataTable

            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlPEnt.GetCostosOALC(AlcCode)
                Else

                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try


        End Function
    End Class
End Namespace



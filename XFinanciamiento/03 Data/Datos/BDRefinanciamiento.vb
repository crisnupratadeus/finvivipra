﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class BDRefinanciamiento
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared Function GetFacturasVencidas(CardCodeSN As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then
                    query = sqlFacturasVencidasLinea.getFacturaVencidasLinea(CardCodeSN)
                Else
                    query = hanaFacturasVencidasLinea.getFacturaVencidasLinea(CardCodeSN)
                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

Namespace Data
    Public Class DatosAjusteTC
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared Function GetdtPeriodoFecha(fechas As String) As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlAjusteTC.GetdtPeriodoFecha_TEXT(fechas)
                Else
                    ostr = hanaAjusteTC.GetdtPeriodoFecha_TEXT(fechas)
                End If
                Return ostr

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetAjustesAnteriores() As String
            Try

                If Motor = "SQL" Then
                    GetAjustesAnteriores = sqlAjusteTC.GetAjustesAnteriores_TEXT
                Else
                    GetAjustesAnteriores = hanaAjusteTC.GetAjustesAnteriores_TEXT
                End If

                Return GetAjustesAnteriores

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetMetodo1(FechaCierre As String, FechaUltimoCierre As String, ByRef oStr As String) As DataTable
            Try

                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.GetMetodo1_TEXT(FechaCierre, FechaUltimoCierre)
                Else
                    oStr = hanaAjusteTC.GetMetodo1_TEXT(FechaCierre, FechaUltimoCierre)
                End If
                Return conexi.obtenerColeccion(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetMetodo2(FechaCierre As String, FechaUltimoCierre As String, SumDec As Integer, ByRef oStr As String) As DataTable
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.GetMetodo2_TEXT(FechaCierre, FechaUltimoCierre, SumDec)
                Else
                    oStr = hanaAjusteTC.GetMetodo2_TEXT(FechaCierre, FechaUltimoCierre, SumDec)
                End If
                Return conexi.obtenerColeccion(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Valida_tipos_de_cambio_Método1(Fecha As String, ByRef oStr As String) As DataTable
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_tipos_de_cambio_Metodo1_TEXT(Fecha)
                Else
                    oStr = hanaAjusteTC.Valida_tipos_de_cambio_Metodo1_TEXT(Fecha)
                End If
                Return conexi.obtenerColeccion(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Valida_tipos_de_cambio_Método2(Fecha As String, FechaInicio As String, ByRef oStr As String) As DataTable
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_tipos_de_cambio_Metodo2_TEXT(Fecha, FechaInicio)
                Else
                    oStr = hanaAjusteTC.Valida_tipos_de_cambio_Metodo2_TEXT(Fecha, FechaInicio)
                End If
                Return conexi.obtenerColeccion(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function FechaInicio_Metodo2(FechaPeriodo As String) As String
            Try
                Dim oStr As String
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.FechaInicio_Metodo2_TEXT(FechaPeriodo)
                Else
                    oStr = hanaAjusteTC.FechaInicio_Metodo2_TEXT(FechaPeriodo)
                End If
                Return conexi.obtenerValor(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Valida_existen_datos(Fecha As String, ByRef oStr As String) As String
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_existen_datos_TEXT(Fecha)
                Else
                    oStr = hanaAjusteTC.Valida_existen_datos_TEXT(Fecha)
                End If
                Return conexi.obtenerValor(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Valida_existen_datos2(Fecha As String, ByRef oStr As String) As String
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_existen_datos2_TEXT(Fecha)
                Else
                    oStr = hanaAjusteTC.Valida_existen_datos2_TEXT(Fecha)
                End If
                Return conexi.obtenerValor(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Valida_existen_datos3(Fecha As String, ByRef oStr As String) As String
            Try
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_existen_datos3_TEXT(Fecha)
                Else
                    oStr = hanaAjusteTC.Valida_existen_datos3_TEXT(Fecha)
                End If
                Return conexi.obtenerValor(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Valida_correlativo(Fecha As String, PeriodoInicial As String, ByRef oStr As String) As DataTable
            Try
                If PeriodoInicial = "" Then PeriodoInicial = "190001"
                If Motor = "SQL" Then
                    oStr = sqlAjusteTC.Valida_correlativo_TEXT(Fecha, PeriodoInicial)
                Else
                    oStr = hanaAjusteTC.Valida_correlativo_TEXT(Fecha, PeriodoInicial)
                End If
                Return conexi.obtenerColeccion(oStr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function Dims() As DataTable
            Try
                Dim ostr As String
                Dim res As DataTable
                If Motor = "SQL" Then
                    ostr = sqlAjusteTC.Dims_TEXT
                Else
                    ostr = hanaAjusteTC.Dims_TEXT
                End If                
                res = conexi.obtenerColeccion(ostr)

                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CreaAsientosContables(ByRef oData As DataTable, Glosa As String, fecha As String, ByRef transID As String) As Boolean
            Try
                Dim resultado As Boolean = True
                Dim oSocios As DataTable = oData.DefaultView.ToTable(True, "Socio")
                Dim Socio As String = ""
                For Each row As DataRow In oSocios.Rows
                    Socio = row.Field(Of String)("Socio")
                    Dim childRows As DataTable = oData.[Select]("Socio = '" & Socio & "'").CopyToDataTable
                    Dim res As Boolean = CrearAsiento(childRows, Glosa, fecha, transID)
                    If res = False Then
                        resultado = False
                        Exit For
                    End If
                Next
                Return resultado
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function CrearAsiento(ByRef oData As DataTable, Glosa As String, fecha As String, ByRef transID As String) As Boolean
            Try
                Dim vJE As SAPbobsCOM.JournalEntries
                vJE = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)

                Dim res As Boolean
                Dim Cuenta As String
                Dim EsDebito As String = ""
                Dim Monto As Decimal
                Dim BoxPatent As String
                Dim OcrCode As String
                Dim OcrCode2 As String
                Dim OcrCode3 As String
                Dim OcrCode4 As String
                Dim OcrCode5 As String
                Dim Socio As String
                Dim SocioAnterior As String = ""


                Dim Moneda As String
                Dim BaseEntry As String

                Dim AcumuladorMonto As Decimal = 0
                Dim LineNum As Integer = 0
                Dim dmonto As Decimal = 0
                Dim montoReal As Decimal = 0
                Dim iSumdec As Integer = DatosReserva.GetSumDec()
                Dim Serie As String = BDContratos.GetParam("SerieImp")
                Dim CuentaTC As String = BDContratos.GetParam("CtaDC")

                Moneda = MonedaLocal
                For Each row As DataRow In oData.Rows
                    Cuenta = row.Field(Of String)("Cuenta")
                    EsDebito = row.Field(Of String)("EsDebito")
                    Monto = row.Field(Of Decimal)("Ajuste")
                    BoxPatent = row.Field(Of String)("U_BoxPatent")
                    OcrCode = row.Field(Of String)("OcrCode")
                    OcrCode2 = row.Field(Of String)("OcrCode2")
                    OcrCode3 = row.Field(Of String)("OcrCode3")
                    OcrCode4 = row.Field(Of String)("OcrCode4")
                    OcrCode5 = row.Field(Of String)("OcrCode5")
                    'Moneda = row.Field(Of String)("Currency")
                    BaseEntry = row.Field(Of Integer)("DocEntry")
                    Socio = row.Field(Of String)("Socio")

                    If LineNum = 0 Then
                        vJE.TaxDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.DueDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.ReferenceDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.Series = Serie
                        If Glosa <> "" Then vJE.Memo = Trim(Left(Glosa & "- SN:" & Socio, 50))
                        'vJE.Reference = comentario
                        vJE.UserFields.Fields.Item("U_RefTC").Value = Left(fecha, 6)
                        vJE.UserFields.Fields.Item("U_SocioAjusteTC").Value = Socio
                    End If

                    dmonto = Monto
                    If dmonto <> 0 Then

                        Call vJE.Lines.Add()
                        Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                        LineNum = LineNum + 1

                        vJE.Lines.AccountCode = Cuenta
                        If dmonto <> 0 Then
                            If Moneda = MonedaLocal Or Moneda = "##" Then
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = "Y" Then
                                    If montoReal > 0 Then
                                        vJE.Lines.Debit = montoReal
                                    Else
                                        vJE.Lines.Credit = -montoReal
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.Credit = montoReal
                                    Else
                                        vJE.Lines.Debit = -montoReal
                                    End If
                                End If
                            Else
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = "Y" Then
                                    If montoReal > 0 Then
                                        vJE.Lines.FCDebit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCCredit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.FCCredit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCDebit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                End If
                            End If
                        End If


                        vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.Lines.ShortName = Cuenta
                        vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                        vJE.Lines.Reference2 = BoxPatent
                        vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                        vJE.Lines.UserFields.Fields.Item("U_BaseEntry").Value = CStr(BaseEntry)



                        If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                        If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                        If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                        If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                        If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    End If



                    SocioAnterior = Socio
                Next

                dmonto = AcumuladorMonto


                If dmonto <> 0 Then
                    EsDebito = "N"
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = CuentaTC
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = "Y" Then
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = "Y" Then
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If

                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(fecha)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(fecha)
                    vJE.Lines.ShortName = CuentaTC
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(fecha)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                End If

                Dim iError As Integer = vJE.Add()

                If iError <> 0 Then
                    'Dim strxml As String
                    'strxml = vJE.GetAsXML
                    Dim sError As String = String.Empty
                    DiApp.GetLastError(iError, sError)
                    res = False
                    Throw New Exception("[CD] Problema al crear asiento contable: " + sError)
                Else
                    DiApp.GetNewObjectCode(transID)
                    res = True
                    InsertData(oData, transID, fecha)
                    oForm.DataSources.UserDataSources.Item("JrnlId").ValueEx = transID
                    oForm.Items.Item("GenAjuste").Visible = False
                    oForm.Items.Item("2").Left = 10
                    oForm.Refresh()

                End If
                If Not IsNothing(vJE) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(vJE)
                    vJE = Nothing
                    oMc = Nothing
                End If
                Return res

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Shared Function Periodoanterior(fechaPeriodo As String) As String
            Try
                Dim res As String = ""
                Dim Fecha As Date = Left(fechaPeriodo, 4) & Mid(fechaPeriodo, 5, 2) & "01"
                Fecha = DateAdd(DateInterval.Day, -1, Fecha)
                res = Year(Fecha) & Right("00" & Month(Fecha), 2)
                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function UltimoCierre(UltimoTransId As String) As String
            Try
                Dim res As String = ""
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = sqlAjusteTC.UltimoCierre_TEXT(UltimoTransId)
                Else
                    ostr = hanaAjusteTC.UltimoCierre_TEXT(UltimoTransId)
                End If
                res = conexi.obtenerValor(ostr)

                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function FechaJrnlAnterior(TransId As String) As String
            Try
                Dim res As String = ""
                Dim ostr As String
                If Motor = "SQL" Then
                    ostr = sqlAjusteTC.FechaJrnlAnterior_TEXT(TransId)
                Else
                    ostr = hanaAjusteTC.FechaJrnlAnterior_TEXT(TransId)
                End If
                res = conexi.obtenerValor(ostr)

                Return res

            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Shared Function InsertData(ByRef oData As DataTable, transId As String, fecha As String) As Boolean
            Try
                Dim res As Boolean = False

                Dim U_UserName As String
                Dim U_AccountDate As String
                Dim U_Periodo As String
                Dim U_BoxPatent As String
                Dim U_ItemCode As String
                Dim U_Currency As String
                Dim U_Price As Decimal
                Dim U_DocDate As String
                Dim U_TC_Final As Decimal
                Dim U_TC_Inicial As Decimal
                Dim U_Ajuste As Decimal
                Dim U_OcrCode As String
                Dim U_OcrCode2 As String
                Dim U_OcrCode3 As String
                Dim U_OcrCode4 As String
                Dim U_OcrCode5 As String
                Dim U_JrnlId As String
                Dim U_CreateDate As String
                Dim U_AGlobal As Decimal
                Dim U_APrevio As Decimal
                Dim U_Tipo As String

                Dim fecha_inicial As String
                Dim fecha_final As String
                Dim U_Cuenta As String
                Dim DocEntryReserva As String

                Dim U_Socio As String

                Dim Periodo As String = Left(Trim(fecha), 6)
                U_UserName = Trim(Left(DiApp.UserName, 100))
                U_AccountDate = fecha
                U_CreateDate = Utiles.Util.Getfecha(Date.Now.Date)
                U_Periodo = Periodo
                U_JrnlId = transId
                For Each row As DataRow In oData.Rows

                    U_BoxPatent = row.Field(Of String)("U_BoxPatent")
                    U_ItemCode = row.Field(Of String)("ItemCode")
                    U_Currency = row.Field(Of String)("Currency")
                    U_Price = row.Field(Of Decimal)("Price")
                    U_DocDate = row.Field(Of Date)("DocDate")
                    DocEntryReserva = row.Field(Of Integer)("DocEntry")
                    U_TC_Final = row.Field(Of Decimal)("TC Final")
                    U_TC_Inicial = row.Field(Of Decimal)("TC Inicial")
                    U_Ajuste = row.Field(Of Decimal)("Ajuste")
                    U_OcrCode = row.Field(Of String)("OcrCode")
                    U_OcrCode2 = row.Field(Of String)("OcrCode2")
                    U_OcrCode3 = row.Field(Of String)("OcrCode3")
                    U_OcrCode4 = row.Field(Of String)("OcrCode4")
                    U_OcrCode5 = row.Field(Of String)("OcrCode5")

                    fecha_inicial = row.Field(Of Date)("fecha inicial")
                    fecha_inicial = Utiles.Util.Getfecha(fecha_inicial)
                    fecha_final = row.Field(Of Date)("fecha final")
                    fecha_final = Utiles.Util.Getfecha(fecha_final)

                    U_AGlobal = row.Field(Of Decimal)("AGlobal")
                    U_APrevio = row.Field(Of Decimal)("APrevio")
                    U_Tipo = row.Field(Of String)("Tipo")
                    U_Cuenta = row.Field(Of String)("Cuenta")
                    U_Socio = row.Field(Of String)("Socio")

                    U_DocDate = Utiles.Util.Getfecha(U_DocDate)

                    InsertCamposTRAZATC(U_UserName, U_AccountDate, U_Periodo, U_BoxPatent, U_ItemCode, U_Currency, _
                                        U_Price, U_DocDate, DocEntryReserva, U_TC_Final, U_TC_Inicial, U_Ajuste, _
                                        U_OcrCode, U_OcrCode2, U_OcrCode3, U_OcrCode4, U_OcrCode5, U_JrnlId, _
                                        U_CreateDate, fecha_inicial, fecha_final, U_AGlobal, U_APrevio, U_Tipo, _
                                        U_Cuenta)
                Next
                res = True
                Return res
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub InsertCamposTRAZATC(U_UserName As String, U_AccountDate As String, U_Periodo As String, _
                                                    U_BoxPatent As String, U_ItemCode As String, U_Currency As String, _
                                                    U_Price As Decimal, U_DocDate As String, U_DocEntry As String, _
                                                    U_TC_Final As Decimal, U_TC_Inicial As Decimal, U_Ajuste As Decimal, _
                                                    U_OcrCode As String, U_OcrCode2 As String, U_OcrCode3 As String, _
                                                    U_OcrCode4 As String, U_OcrCode5 As String, U_JrnlId As String, _
                                                    U_CreateDate As String, fecha_inicial As String, fecha_final As String, _
                                                    U_AGlobal As Decimal, U_APrevio As Decimal, U_Tipo As String, U_Cuenta As String)
            Try

                Dim sSqlInsert As String
                If Motor = "SQL" Then
                    sSqlInsert = sqlAjusteTC.InsertCamposTRAZATC_TEXT(U_UserName, U_AccountDate, U_Periodo, _
                                                    U_BoxPatent, U_ItemCode, U_Currency, _
                                                    U_Price, U_DocDate, U_DocEntry, _
                                                    U_TC_Final, U_TC_Inicial, U_Ajuste, _
                                                    U_OcrCode, U_OcrCode2, U_OcrCode3, _
                                                     U_OcrCode4, U_OcrCode5, U_JrnlId, _
                                                     U_CreateDate, fecha_inicial, fecha_final, U_AGlobal, U_APrevio, U_Tipo, U_Cuenta)
                Else
                    sSqlInsert = hanaAjusteTC.InsertCamposTRAZATC_TEXT(U_UserName, U_AccountDate, U_Periodo, _
                                                    U_BoxPatent, U_ItemCode, U_Currency, _
                                                    U_Price, U_DocDate, U_DocEntry, _
                                                    U_TC_Final, U_TC_Inicial, U_Ajuste, _
                                                    U_OcrCode, U_OcrCode2, U_OcrCode3, _
                                                     U_OcrCode4, U_OcrCode5, U_JrnlId, _
                                                     U_CreateDate, fecha_inicial, fecha_final, U_AGlobal, U_APrevio, U_Tipo, U_Cuenta)
                End If
                conexi.creaRegistro(sSqlInsert, False)


            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace



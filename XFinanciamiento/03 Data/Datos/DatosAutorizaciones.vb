﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml
'Imports XFinanciamiento.View
Imports System.Globalization
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.View

Namespace Data
    Public Class DatosAutorizaciones
        Private Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable

        'Public Shared Function VerificaDataTableExists(tablename As String) As Boolean
        '    Try
        '        If dsMod IsNot Nothing Then
        '            If dsMod.Tables.Count > 0 Then
        '                For Each table As DataTable In dsMod.Tables
        '                    If table.TableName = tablename Then
        '                        Return True
        '                    End If
        '                Next
        '                Return False
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '        Return False
        '    End Try
        'End Function

        Public Shared Function GetGrupoAutDias() As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetGrupoAutDias()
                Else
                    ostr = HanaAutorizaciones.GetGrupoAutDias()
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function



        Public Shared Function GetGrupoResetAut() As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetGrupoResetAut()
                Else
                    ostr = HanaAutorizaciones.GetGrupoResetAut()
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function


        Public Shared Function GetFormName(Formulario As String) As String
            Dim ostr As String = String.Empty

            Try

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetFormName(Formulario)
                Else
                    ostr = HanaAutorizaciones.GetFormName(Formulario)
                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function GetForms() As DataTable
            Dim ostr As String = String.Empty

            Try

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetForms()
                Else
                    ostr = HanaAutorizaciones.GetForms()
                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetGruposUsuario(usercode As String) As DataTable
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetGruposUsuario(usercode)
                Else
                    ostr = HanaAutorizaciones.GetGruposUsuario(usercode)
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetEstados(Optional Form As String = "") As DataTable
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetEstados(Form)
                Else
                    ostr = HanaAutorizaciones.GetEstados(Form)

                End If


                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function



        Public Shared Function PopulaGridEtpAut(Optional Formulario As String = "") As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.PopulaGridEtpAut(Formulario)
                Else
                    ostr = HanaAutorizaciones.PopulaGridEtpAut(Formulario)
                End If

                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function


        Public Shared Function GetNroRechEtapa(codEtapa As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetNroRechEtapa(codEtapa)
                Else
                    ostr = HanaAutorizaciones.GetNroRechEtapa(codEtapa)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function GetNroAprEtapa(codEtapa As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetNroAprEtapa(codEtapa)
                Else
                    ostr = HanaAutorizaciones.GetNroAprEtapa(codEtapa)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function UpdateUsrProy(CodeTBL As String, NameTBL As String, Enabled As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.UpdateUsrProy(Trim(CodeTBL), Trim(NameTBL), Trim(Enabled))
                Else
                    Return HanaAutorizaciones.UpdateUsrProy(Trim(CodeTBL), Trim(NameTBL), Trim(Enabled))
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function InsertaUsrProy(UsrCode As String, UsrName As String, Enabled As String, ProjectCode As String, ProjectName As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.InsertaUsrProy(Trim(UsrCode), Trim(UsrName), Trim(Enabled), Trim(ProjectCode), Trim(ProjectName))
                Else
                    Return HanaAutorizaciones.InsertaUsrProy(Trim(UsrCode), Trim(UsrName), Trim(Enabled), Trim(ProjectCode), Trim(ProjectName))
                End If


            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function InsertaUsrAlarmas(CodUsr As String, NomUsr As String, Formulario As String, CodEtapa As String, NomEtapa As String) As Boolean
            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.InsertaUsrAlarmas(Trim(CodUsr), Trim(NomUsr), Trim(Formulario), Trim(CodEtapa), Trim(NomEtapa)) 'cnp20190710
                Else
                    Return HanaAutorizaciones.InsertaUsrAlarmas(Trim(CodUsr), Trim(NomUsr), Trim(Formulario), Trim(CodEtapa), Trim(NomEtapa)) 'cnp20190710
                End If

            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function BorraAlarmaUsr(CodUsuario As String, Etapa As String, Formulario As String) As Boolean
            'cnp20190710
            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.BorraAlarmaUsr(CodUsuario, Etapa, Formulario)
                Else
                    Return HanaAutorizaciones.BorraAlarmaUsr(CodUsuario, Etapa, Formulario)
                End If

                Return False
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function


        Public Shared Function ValidaBtnAutDias(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, CodUsuario As String) As Boolean
            Dim GrupoAutDias As String = String.Empty
            Dim oDT As DataTable
            Dim grpUsr As String = String.Empty


            Try
                GrupoAutDias = GetGrupoAutDias()

                If Trim(GrupoAutDias) = "" Or GrupoAutDias Is Nothing Then
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontró un grupo de autorización para la edición de días trabajados.Verifique.")
                    Return False
                End If

                oDT = GetGruposUsuario(Trim(CodUsuario))
                If Not oDT Is Nothing Then
                    If oDT.Rows.Count > 0 Then
                        For Each row As DataRow In oDT.Rows
                            grpUsr = row.Item("GRUPO")
                            If Trim(grpUsr) = Trim(GrupoAutDias) Then
                                oForm.Freeze(False)
                                Return True
                            End If
                        Next
                    Else
                        oForm.Freeze(False)
                        oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                    Return False
                End If


                oForm.Freeze(False)
                Return False
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Public Shared Function ValidaBtnReset(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, CodUsuario As String) As Boolean
            Dim GrupoReset As String = String.Empty
            Dim oDT As DataTable
            Dim grpUsr As String = String.Empty


            Try
                GrupoReset = GetGrupoResetAut()

                If Trim(GrupoReset) = "" Or GrupoReset Is Nothing Then
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontró un grupo de autorización para el reseteo de autorizaciones.Verifique.")
                    Return False
                End If

                oDT = GetGruposUsuario(Trim(CodUsuario))
                If Not oDT Is Nothing Then
                    If oDT.Rows.Count > 0 Then
                        For Each row As DataRow In oDT.Rows
                            grpUsr = row.Item("GRUPO")
                            If Trim(grpUsr) = Trim(GrupoReset) Then
                                oForm.Freeze(False)
                                Return True
                            End If
                        Next
                    Else
                        oForm.Freeze(False)
                        oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                    Return False
                End If


                oForm.Freeze(False)
                Return False
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Public Shared Function ValidaAlarmasEtapaUsr(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, CodUsuario As String, Etapa As String, Formulario As String) As String
            Dim existe As String

            Try
                existe = GetSiExisteAlarma(CodUsuario, Etapa, Formulario) 'cnp20190710

                If existe > 0 Then
                    oForm.Freeze(False)
                    Return "S"
                Else
                    oForm.Freeze(False)
                    Return "N"
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return "E"
            End Try


        End Function

        Public Shared Function GetUsrCodeFromName(UsrName As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetUsrCodeFromName(UsrName)
                Else
                    ostr = HanaAutorizaciones.GetUsrCodeFromName(UsrName)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function GetProyectosAsociados(UsrCode As String) As String
            Dim ostr As String = String.Empty

            Try

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetProyectosAsociados(UsrCode)
                Else
                    ostr = HanaAutorizaciones.GetProyectosAsociados(UsrCode)
                End If

                Return ostr

            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function GetSiFormModelo(formulario As String) As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiFormModelo(formulario)
                Else
                    ostr = HanaAutorizaciones.GetSiFormModelo(formulario)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function ValidaSiUsrTieneAccesoProy(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, UsrSignature As String, ProyectCode As String) As Boolean
            Dim existe As String = String.Empty


            Try
                'If Trim(ProyectCode) = "" Then
                '    oForm.Freeze(False)
                '    oApp.MessageBox("Se debe tener un código de proyecto cargado en el formulario")
                '    Return False
                'End If

                If Trim(UsrSignature) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al intentar sacar el código de usuario actual")
                    Return False
                End If

                'Try
                '    existe = DatosAutorizaciones.GetProyectoUsrActivo(UsrSignature, ProyectCode)

                '    If existe = "0" Then
                '        oForm.Freeze(False)
                '        oApp.MessageBox("El usuario actual no tiene permisos para este proyecto.")
                '        Return False
                '    ElseIf existe Is Nothing Then
                '        oForm.Freeze(False)
                '        oApp.MessageBox("El usuario actual no tiene permisos para este proyecto.")
                '        Return False
                '    ElseIf existe > 0 Then
                '        oForm.Freeze(False)
                '        Return True
                '    Else
                '        oForm.Freeze(False)
                '        Return False
                '    End If

                'Catch ex As Exception
                '    oForm.Freeze(False)
                '    oApp.MessageBox(ex.Message)
                '    Return False
                'End Try

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function



        Public Shared Function ValidaSiFormModelo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, formulario As String) As String
            Dim existe As String = String.Empty

            Try
                existe = GetSiFormModelo(formulario)

                If existe > 0 Then
                    oForm.Freeze(False)
                    Return "S"
                Else
                    oForm.Freeze(False)
                    Return "N"
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return "E"
            End Try


        End Function

        Public Shared Function ValidaSiTieneModelo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, formulario As String) As String
            Dim existe As String
            Try
                existe = GetSiFormTieneModelo(formulario)

                If existe > 0 Then
                    oForm.Freeze(False)
                    Return "S"
                Else
                    oForm.Freeze(False)
                    Return "N"
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return "E"
            End Try
        End Function

        Public Shared Function ModificaEtapaForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, DocEntry As String, NuevaEtapa As String, formulario As String) As Boolean

            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try


                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService(formulario)
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    oGeneralParams.SetProperty("DocEntry", Trim(DocEntry))
                    oGeneralParams.SetProperty("Code", Trim(DocEntry))
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_Etapa", Trim(NuevaEtapa))
                    oGeneralService.Update(oGeneralData)
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ModificaCorreSolForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, DocEntryForm As String, CorrelativoSol As String, formulario As String) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try

                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService(formulario)
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    'oGeneralParams.SetProperty("DocEntry", DocEntryForm)
                    oGeneralParams.SetProperty("Code", DocEntryForm)
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_CorreSol", CorrelativoSol)
                    oGeneralService.Update(oGeneralData)

                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function ModificaEtapaApForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, DocEntryForm As String, NuevaEtapa As String, NuevaLinEtapa As String, formulario As String) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try

                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService(formulario)
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    'oGeneralParams.SetProperty("DocEntry", Trim(DocEntryForm))
                    oGeneralParams.SetProperty("Code", Trim(DocEntryForm))
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_Etapa", Trim(NuevaEtapa))
                    oGeneralData.SetProperty("U_LinEtapa", Trim(NuevaLinEtapa))
                    oGeneralService.Update(oGeneralData)

                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function ModificaEstadoProcForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, DocEntry As String, NuevoEstado As String, formulario As String) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try


                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService(formulario)
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    'oGeneralParams.SetProperty("DocEntry", Trim(DocEntry))
                    oGeneralParams.SetProperty("Code", Trim(DocEntry))
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_Estado", Trim(NuevoEstado))
                    oGeneralService.Update(oGeneralData)
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Private Shared Function ModificaEstadoApForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, DocEntry As String, NuevoEstado As String, formulario As String) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try


                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService(formulario)
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    ' oGeneralParams.SetProperty("DocEntry", Trim(DocEntry))
                    oGeneralParams.SetProperty("Code", Trim(DocEntry))
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_EstadoAp", Trim(NuevoEstado))
                    oGeneralService.Update(oGeneralData)
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function GetCondicionesModelo(CodModelo As String) As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetCondicionesModelo(CodModelo)
                Else
                    ostr = HanaAutorizaciones.GetCondicionesModelo(CodModelo)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetCondicionesAutorizacion(CodCondicion As String) As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetCondicionesAutorizacion(CodCondicion)
                Else
                    ostr = HanaAutorizaciones.GetCondicionesAutorizacion(CodCondicion)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetEtapasModelo(CodModelo As String) As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetEtapasModelo(CodModelo)
                Else
                    ostr = HanaAutorizaciones.GetEtapasModelo(CodModelo)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetEstadoFinalEtapaMod(CodModelo As String, CodEtapa As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetEstadoFinalEtapaMod(CodModelo, CodEtapa)
                Else
                    ostr = HanaAutorizaciones.GetEstadoFinalEtapaMod(CodModelo, CodEtapa)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try

        End Function

        Public Shared Function GetFunQuery(CodFuncion As String, Optional DocEntry As String = "") As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    If DocEntry = "" Then
                        ostr = SQLAutorizaciones.GetFunQuery(CodFuncion)
                    Else
                        ostr = SQLAutorizaciones.GetFunQuery(CodFuncion, DocEntry)
                    End If

                Else
                    If DocEntry = "" Then
                        ostr = HanaAutorizaciones.GetFunQuery(CodFuncion)
                    Else
                        ostr = HanaAutorizaciones.GetFunQuery(CodFuncion, DocEntry)
                    End If
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try

        End Function


        Public Shared Function GetFuncionesTabla() As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetFuncionesTabla()
                Else
                    ostr = HanaAutorizaciones.GetFuncionesTabla()
                End If

                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try

        End Function

        Public Shared Function GetSiguienteEtapaMod(CodModelo As String, LineaEtapa As String) As DataTable
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiguienteEtapaMod(Trim(CodModelo), Trim(LineaEtapa))
                Else
                    ostr = HanaAutorizaciones.GetSiguienteEtapaMod(Trim(CodModelo), Trim(LineaEtapa))
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetProyectoUsrActivo(UserCode As String, ProjectCode As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then

                    ostr = SQLAutorizaciones.GetProyectoUsrActivo(Trim(UserCode), Trim(ProjectCode))
                Else
                    ostr = HanaAutorizaciones.GetProyectoUsrActivo(Trim(UserCode), Trim(ProjectCode))
                End If

                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try


        End Function

        Public Shared Function GetUsrFromLogAut(Formulario As String, DocEntryForm As String, PresEntry As String, Etapa As String, Usuario As String, CorrelativoSol As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetUsrFromLogAut(Formulario, DocEntryForm, PresEntry, Etapa, Usuario, CorrelativoSol)
                Else
                    ostr = HanaAutorizaciones.GetUsrFromLogAut(Formulario, DocEntryForm, PresEntry, Etapa, Usuario, CorrelativoSol)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetFromLogAut(Formulario As String, DocEntryForm As String, Etapa As String, CodAutorizacion As String, CodEstadoProc As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetFromLogAut(Formulario, DocEntryForm, Etapa, CodAutorizacion, CodEstadoProc)
                Else
                    ostr = HanaAutorizaciones.GetFromLogAut(Formulario, DocEntryForm, Etapa, CodAutorizacion, CodEstadoProc)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetSiCondMod(CodModelo As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiCondMod(CodModelo)
                Else
                    ostr = HanaAutorizaciones.GetSiCondMod(CodModelo)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetEtapasConsMod(CodModelo As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetEtapasConsMod(CodModelo)
                Else
                    ostr = HanaAutorizaciones.GetEtapasConsMod(CodModelo)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function CreaAlerta(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, NombreUsuario As String, ObjForm As String, DocEntryForm As String) As Boolean

            Dim oMsg As SAPbobsCOM.Message
            Dim lngStatus As Integer
            Dim lErrCode As Integer
            Dim sErrMsg As String
            Dim formulario As String

            Dim pMessageDataColumns As SAPbobsCOM.MessageDataColumns
            Dim pMessageDataColumn As SAPbobsCOM.MessageDataColumn
            Dim oLines As SAPbobsCOM.MessageDataLines
            Dim oLine As SAPbobsCOM.MessageDataLine
            Dim oRecipientCollection As SAPbobsCOM.RecipientCollection
            Dim oMessageService As SAPbobsCOM.MessagesService
            Dim oCmpSrv As SAPbobsCOM.CompanyService




            Try
                Try
                    formulario = DatosAutorizaciones.GetFormName(ObjForm)
                Catch ex As Exception
                    Throw ex
                    formulario = ObjForm
                End Try


                oCmpSrv = DiApp.GetCompanyService()
                oMessageService = oCmpSrv.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService)

                oMsg = oMessageService.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage)

                'oMsg = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages)

                oMsg.Text = "Se debe revisar el documento para proceder a su aprobación/rechazo."
                oMsg.Subject = "Solicitación para aprobación de documento"


                oRecipientCollection = oMsg.RecipientCollection

                oRecipientCollection.Add()
                oRecipientCollection.Item(0).SendInternal = SAPbobsCOM.BoYesNoEnum.tYES
                oRecipientCollection.Item(0).SendEmail = SAPbobsCOM.BoYesNoEnum.tYES
                oRecipientCollection.Item(0).UserCode = Trim(NombreUsuario)

                pMessageDataColumns = oMsg.MessageDataColumns
                pMessageDataColumn = pMessageDataColumns.Add()
                pMessageDataColumn.ColumnName = "DOCUMENTO"
                pMessageDataColumn.Link = SAPbobsCOM.BoYesNoEnum.tYES

                oLines = pMessageDataColumn.MessageDataLines()

                oLine = oLines.Add()
                oLine.Value = DocEntryForm
                oLine.Object = Trim(ObjForm)
                oLine.ObjectKey = DocEntryForm

                pMessageDataColumn = pMessageDataColumns.Add()
                pMessageDataColumn.ColumnName = "FORMULARIO"
                pMessageDataColumn.Link = SAPbobsCOM.BoYesNoEnum.tNO

                oLines = pMessageDataColumn.MessageDataLines()

                oLine = oLines.Add()
                oLine.Value = formulario
                oLine.Object = Trim(ObjForm)

                oMessageService.SendMessage(oMsg)


                System.Runtime.InteropServices.Marshal.ReleaseComObject(oMsg)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oMsg)
                Return False
            End Try
        End Function

        Public Shared Function GetLineaEtapaporCond(CodModelo As String, CodEtapa As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetLineaEtapaporCond(CodModelo, CodEtapa)
                Else
                    ostr = HanaAutorizaciones.GetLineaEtapaporCond(CodModelo, CodEtapa)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function CumpleCondicionesModelo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oDTCon As DataTable, DocEntryForm As String) As Boolean
            Dim query As String = String.Empty
            Dim queryWHERE As String = String.Empty
            Dim queryFinal As String = String.Empty
            Dim Formulario As String = String.Empty
            Dim Funcion As String = String.Empty
            Dim Campo As String = String.Empty
            Dim Condicion As String = String.Empty
            Dim Valor As String = String.Empty
            Dim CampoValor As String = String.Empty
            Dim EsCampo As String = String.Empty
            Dim Cond As Boolean = False
            Dim RES As Integer = 0

            Try
                If Motor = "SQL" Then
                    For i As Integer = 0 To oDTCon.Rows.Count - 1
                        If i = 0 Then
                            queryWHERE = "WHERE "
                            ' Else
                            ' queryWHERE = queryWHERE & " AND "
                        End If

                        Formulario = oDTCon.Rows(i).Item("FORMULARIO")
                        Funcion = oDTCon.Rows(i).Item("FUNCION")
                        Campo = oDTCon.Rows(i).Item("CAMPO")
                        Condicion = oDTCon.Rows(i).Item("CONDICION")
                        Valor = oDTCon.Rows(i).Item("VALOR")
                        CampoValor = oDTCon.Rows(i).Item("CAMPOVAL")
                        EsCampo = oDTCon.Rows(i).Item("ESCAMPO")

                        If Trim(Campo) <> "" Then
                            If i > 0 Then
                                queryWHERE = queryWHERE & " AND "
                            End If
                            If Trim(EsCampo) = "Y" Then
                                If Trim(Condicion) = "-" Then
                                    Continue For
                                ElseIf Trim(Condicion) = "MEN" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " < " & Trim(CampoValor) & ""
                                ElseIf Trim(Condicion) = "MAY" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " > " & Trim(CampoValor) & ""
                                ElseIf Trim(Condicion) = "MEI" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " <= " & Trim(CampoValor) & ""
                                ElseIf Trim(Condicion) = "MAI" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " >= " & Trim(CampoValor) & ""
                                ElseIf Trim(Condicion) = "IGU" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " = " & Trim(CampoValor) & ""
                                ElseIf Trim(Condicion) = "DIF" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " <> " & Trim(CampoValor) & ""
                                End If
                            Else
                                If Trim(Condicion) = "-" Then
                                    Continue For
                                ElseIf Trim(Condicion) = "MEN" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " < '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MAY" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " > '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MEI" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " <= '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MAI" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " >= '" & Valor & "' "
                                ElseIf Trim(Condicion) = "IGU" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " = '" & Valor & "' "
                                ElseIf Trim(Condicion) = "DIF" Then
                                    queryWHERE = queryWHERE & " " & "" & Trim(Campo) & "" & " <> '" & Valor & "' "
                                End If
                            End If


                        Else
                            Continue For
                        End If
                    Next

                    query = "SELECT COUNT(1) AS CUMPLE FROM " & Trim(Funcion) & " ('" & Trim(DocEntryForm) & "') "

                    queryFinal = query & " " & queryWHERE

                    RES = conexi.obtenerValor(queryFinal, False)

                    If RES > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    For i As Integer = 0 To oDTCon.Rows.Count - 1
                        If i = 0 Then
                            queryWHERE = "WHERE "
                            ' Else
                            ' queryWHERE = queryWHERE & " AND "
                        End If

                        Formulario = oDTCon.Rows(i).Item("FORMULARIO")
                        Funcion = oDTCon.Rows(i).Item("FUNCION")
                        Campo = oDTCon.Rows(i).Item("CAMPO")
                        Condicion = oDTCon.Rows(i).Item("CONDICION")
                        Valor = oDTCon.Rows(i).Item("VALOR")
                        CampoValor = oDTCon.Rows(i).Item("CAMPOVAL")
                        EsCampo = oDTCon.Rows(i).Item("ESCAMPO")

                        If Trim(Campo) <> "" Then
                            If i > 0 Then
                                queryWHERE = queryWHERE & " AND "
                            End If

                            If Trim(EsCampo) = "Y" Then
                                If Trim(Condicion) = "-" Then
                                    Continue For
                                ElseIf Trim(Condicion) = "MEN" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " < """ & Trim(CampoValor) & """"
                                ElseIf Trim(Condicion) = "MAY" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " > """ & Trim(CampoValor) & """"
                                ElseIf Trim(Condicion) = "MEI" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " <= """ & Trim(CampoValor) & """"
                                ElseIf Trim(Condicion) = "MAI" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " >= """ & Trim(CampoValor) & """"
                                ElseIf Trim(Condicion) = "IGU" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " = """ & Trim(CampoValor) & """"
                                ElseIf Trim(Condicion) = "DIF" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " <> """ & Trim(CampoValor) & """"
                                End If
                            Else
                                If Trim(Condicion) = "-" Then
                                    Continue For
                                ElseIf Trim(Condicion) = "MEN" Then
                                    queryWHERE = queryWHERE & " " & " TO_DOUBLE(""" & Trim(Campo) & """)" & " < '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MAY" Then
                                    queryWHERE = queryWHERE & " " & " TO_DOUBLE(""" & Trim(Campo) & """)" & " > '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MEI" Then
                                    queryWHERE = queryWHERE & " " & " TO_DOUBLE(""" & Trim(Campo) & """)" & " <= '" & Valor & "' "
                                ElseIf Trim(Condicion) = "MAI" Then
                                    queryWHERE = queryWHERE & " " & " TO_DOUBLE(""" & Trim(Campo) & """)" & " >= '" & Valor & "' "
                                ElseIf Trim(Condicion) = "IGU" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " = '" & Valor & "' "
                                ElseIf Trim(Condicion) = "DIF" Then
                                    queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """" & " <> '" & Valor & "' "
                                End If
                            End If
                            'queryWHERE = queryWHERE & " " & """" & Trim(Campo) & """"

                        Else
                            Continue For
                        End If
                    Next

                    query = "SELECT COUNT(1) AS ""CUMPLE"" FROM """ & SQLBaseDatos.ToUpper & """.""" & Trim(Funcion) & """ ('" & Trim(DocEntryForm) & "') "

                    queryFinal = query & " " & queryWHERE

                    RES = conexi.obtenerValor(queryFinal, False)

                    If RES > 0 Then
                        Return True
                    Else
                        Return False
                    End If

                End If



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Public Shared Function ManejaProcesoApr(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Formulario As String, DocEntryForm As String, EstadoProc As String, EstadoAp As String, Etapa As String, CodAprobacion As String, linEtapa As String, Optional CodProyecto As String = "") As Boolean
            Dim CodModelo As String = String.Empty
            Dim oDTEtp As DataTable
            Dim oDTUsr As DataTable
            Dim oDTCon As DataTable
            Dim oDTCondiciones As DataTable
            Dim nroA As String = String.Empty
            Dim nroR As String = String.Empty
            Dim EtapasCons As String = String.Empty
            Dim ConCond As String = String.Empty
            Dim ResValMod As String = String.Empty
            Dim EtapaMod As String = String.Empty
            Dim LinEtapaMod As String = String.Empty
            Dim EstFinEtapaMod As String = String.Empty
            Dim UsrEtp As String = String.Empty
            Dim CodCondicion As String = String.Empty
            Dim CodEtpCondicion As String = String.Empty


            Try

                'Comenzamos viendo que se apretó en el btn de aprobacion
                If Trim(CodAprobacion) = "S" Then

                    'Verificamos la etapa de autorizacion actual del formulario
                    If Trim(Etapa) = "" Then
                        'si esta vacia la etapa se verifica si hay modelo
                        Try
                            ResValMod = ValidaSiTieneModelo(oApp, oForm, Trim(Formulario))
                        Catch ex As Exception
                            ResValMod = ""
                        End Try

                        'Si el form tiene modelo se trae el codigo del modelo
                        If Trim(ResValMod) = "S" Then
                            Try
                                CodModelo = GetModeloAutForm(Trim(Formulario))
                            Catch ex As Exception
                                CodModelo = ""
                            End Try
                            'si se tiene un codigo de modelo
                            If CodModelo <> "" Then
                                Try

                                    'primero se verifica si hay condiciones
                                    ConCond = GetSiCondMod(Trim(CodModelo))

                                    If Trim(ConCond) = "Y" Then
                                        oDTCon = GetCondicionesModelo(Trim(CodModelo))
                                        If Not oDTCon Is Nothing Then
                                            If oDTCon.Rows.Count > 0 Then

                                                For Each row As DataRow In oDTCon.Rows
                                                    CodCondicion = row.Item("CONDICION")
                                                    CodEtpCondicion = row.Item("ETAPA")

                                                    oDTCondiciones = GetCondicionesAutorizacion(Trim(CodCondicion))
                                                    If Not oDTCondiciones Is Nothing Then
                                                        If oDTCondiciones.Rows.Count > 0 Then
                                                            If CumpleCondicionesModelo(oApp, oForm, oDTCondiciones, DocEntryForm) Then
                                                                If Trim(CodEtpCondicion) = "" Then
                                                                    ModificaEstadoApForm(oApp, oForm, DocEntryForm, "A", Trim(Formulario))
                                                                    ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "100", Trim(Formulario))
                                                                    Return True
                                                                End If

                                                                ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))

                                                                'If Not Trim(CodProyecto) = "" Then 'cnp20190710
                                                                oDTUsr = GetUsrEtapaAlarma(CodEtpCondicion)
                                                                If Not oDTUsr Is Nothing Then
                                                                    If oDTUsr.Rows.Count > 0 Then
                                                                        For Each rowUsr As DataRow In oDTUsr.Rows
                                                                            UsrEtp = rowUsr.Item("USRN")
                                                                            CreaAlerta(oApp, oForm, Trim(UsrEtp), Formulario, DocEntryForm)
                                                                        Next
                                                                    End If
                                                                End If
                                                                'End If

                                                                LinEtapaMod = GetLineaEtapaporCond(Trim(CodModelo), Trim(CodEtpCondicion))

                                                                ModificaEtapaApForm(oApp, oForm, DocEntryForm, Trim(CodEtpCondicion), Trim(LinEtapaMod), Trim(Formulario))
                                                                Return True
                                                            Else
                                                                Continue For

                                                            End If
                                                        Else
                                                            oForm.Freeze(False)
                                                            oApp.MessageBox("No se encontraron lineas de condiciones para el codigo de maestro de condiciones. Verifique")
                                                            Return False
                                                        End If
                                                    Else
                                                        oForm.Freeze(False)
                                                        oApp.MessageBox("No se encontraron lineas de condiciones para el codigo de maestro de condiciones. Verifique")
                                                        Return False
                                                    End If
                                                    oDTCondiciones.Clear()
                                                Next
                                                oForm.Freeze(False)
                                                oApp.MessageBox("No se cumplieron ninguna de las condiciones dentro del modelo de autorizacion. Verifique")
                                                Return False
                                            Else
                                                oForm.Freeze(False)
                                                oApp.MessageBox("No se encontraron condiciones para este modelo. Verifique")
                                                Return False
                                            End If
                                        Else
                                            oForm.Freeze(False)
                                            oApp.MessageBox("No se encontraron condiciones para este modelo. Verifique")
                                            Return False
                                        End If

                                    Else
                                        'se sacan las etapas del modelo
                                        oDTEtp = GetEtapasModelo(Trim(CodModelo))
                                        'si se lleno el dt de etapas
                                        If Not oDTEtp Is Nothing Then
                                            'si se tienen etapas
                                            If oDTEtp.Rows.Count > 0 Then
                                                LinEtapaMod = oDTEtp.Rows(0).Item("LINETP")
                                                EtapaMod = oDTEtp.Rows(0).Item("CODETP")
                                                ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))

                                                'If Not Trim(CodProyecto) = "" Then 'cnp20190710    
                                                oDTUsr = GetUsrEtapaAlarma(EtapaMod)

                                                If Not oDTUsr Is Nothing Then
                                                    If oDTUsr.Rows.Count > 0 Then
                                                        For Each row As DataRow In oDTUsr.Rows
                                                            UsrEtp = row.Item("USRN")
                                                            CreaAlerta(oApp, oForm, Trim(UsrEtp), Formulario, DocEntryForm)
                                                        Next
                                                    End If
                                                End If
                                                'End If

                                                ModificaEtapaApForm(oApp, oForm, DocEntryForm, Trim(EtapaMod), Trim(LinEtapaMod), Trim(Formulario))
                                                Return True
                                            Else
                                                oForm.Freeze(False)
                                                oApp.MessageBox("No se encontraron etapas para este modelo. Verifique")
                                                Return False
                                            End If
                                        Else
                                            oForm.Freeze(False)
                                            oApp.MessageBox("No se encontraron etapas para este modelo. Verifique")
                                            Return False
                                        End If


                                    End If

                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    Return False
                                End Try
                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al intentar capturar el modelo de aprobación para este formulario.")
                                Return False
                            End If

                            'si no tiene modelo se cambia el estado de form a Pendiente directamente
                        ElseIf Trim(ResValMod) = "N" Then
                            ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))
                            'If Not Trim(CodProyecto) = "" Then
                            '    oDTUsr = GetUsrEtapaAlarma(EtapaMod, CodProyecto)
                            '    If Not oDTUsr Is Nothing Then
                            '        If oDTUsr.Rows.Count > 0 Then
                            '            For Each row As DataRow In oDTUsr.Rows
                            '                UsrEtp = row.Item("USRN")
                            '                CreaAlerta(oApp, oForm, Trim(UsrEtp), Formulario, DocEntryForm)
                            '            Next
                            '        End If
                            '    End If
                            'End If
                            Return True
                        Else
                            'ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))
                            Return False
                        End If

                    Else
                        'Si se tiene la etapa se la busca
                        CodModelo = GetModeloAutForm(Trim(Formulario))
                        Dim oDTSigEtp As DataTable

                        Try
                            If Trim(linEtapa) = "" Then
                                linEtapa = "0"
                            End If
                            oDTSigEtp = GetSiguienteEtapaMod(CodModelo, linEtapa)
                            If Not oDTSigEtp Is Nothing Then
                                If oDTSigEtp.Rows.Count > 0 Then
                                    LinEtapaMod = oDTSigEtp.Rows(0).Item("LINEA")
                                    EtapaMod = oDTSigEtp.Rows(0).Item("ETAPA")
                                    ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))
                                    'If Trim(CodProyecto) = "" Then

                                    'Else 'cnp20190710
                                    oDTUsr = GetUsrEtapaAlarma(EtapaMod)
                                    If Not oDTUsr Is Nothing Then
                                        If oDTUsr.Rows.Count > 0 Then
                                            For Each row As DataRow In oDTUsr.Rows
                                                UsrEtp = row.Item("USRN")
                                                CreaAlerta(oApp, oForm, Trim(UsrEtp), Formulario, DocEntryForm)
                                            Next
                                        End If

                                    End If
                                    'End If
                                    ModificaEtapaApForm(oApp, oForm, DocEntryForm, Trim(EtapaMod), Trim(LinEtapaMod), Trim(Formulario))
                                    Return True
                                Else
                                    oForm.Freeze(False)
                                    oApp.MessageBox("No se encontraron siguientes etapas para este modelo. Verifique")
                                    Return False
                                End If
                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("No se encontraron siguientes etapas para este modelo. Verifique")
                                Return False
                            End If
                        Catch ex As Exception
                            oForm.Freeze(False)
                            oApp.MessageBox(ex.Message)
                            Return False
                        End Try
                    End If

                ElseIf Trim(CodAprobacion) = "A" Then

                    If Trim(Etapa) = "" Or Trim(Etapa) = "0" Then
                        ModificaEstadoApForm(oApp, oForm, DocEntryForm, "A", Trim(Formulario))
                        If EstFinEtapaMod <> "" Then
                            ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "100", Trim(Formulario))
                        End If
                        Return True
                    Else
                        Dim nroRegLog As String


                        Try

                            Try
                                CodModelo = GetModeloAutForm(Trim(Formulario))
                            Catch ex As Exception
                                CodModelo = ""
                            End Try

                            If Trim(CodModelo) = "" Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al intentar capturar el modelo de aprobación para este formulario.")
                                Return False
                            End If

                            EtapasCons = GetEtapasConsMod(Trim(CodModelo))

                            EstFinEtapaMod = GetEstadoFinalEtapaMod(Trim(CodModelo), Trim(Etapa))

                            nroA = GetNroAprEtapa(Trim(Etapa))

                            nroRegLog = GetFromLogAut(Trim(Formulario), Trim(DocEntryForm), Trim(Etapa), Trim(CodAprobacion), Trim(EstadoProc))

                            If nroRegLog >= nroA Then
                                Dim resHayMas As String

                                resHayMas = GetSiHayMasEtapas(Trim(CodModelo), Trim(linEtapa))

                                If resHayMas > 0 Then
                                    If EtapasCons = "Y" Then
                                        Dim oDTSigEtp As DataTable

                                        Try
                                            oDTSigEtp = GetSiguienteEtapaMod(CodModelo, linEtapa)
                                            If Not oDTSigEtp Is Nothing Then
                                                If oDTSigEtp.Rows.Count > 0 Then
                                                    LinEtapaMod = oDTSigEtp.Rows(0).Item("LINEA")
                                                    EtapaMod = oDTSigEtp.Rows(0).Item("ETAPA")
                                                    ModificaEstadoApForm(oApp, oForm, DocEntryForm, "P", Trim(Formulario))
                                                    'If Not Trim(CodProyecto) = "" Then 'cnp20190710
                                                    oDTUsr = GetUsrEtapaAlarma(EtapaMod)
                                                    If Not oDTUsr Is Nothing Then
                                                        If oDTUsr.Rows.Count > 0 Then
                                                            For Each row As DataRow In oDTUsr.Rows
                                                                UsrEtp = row.Item("USRN")
                                                                CreaAlerta(oApp, oForm, Trim(UsrEtp), Formulario, DocEntryForm)
                                                            Next
                                                        End If
                                                    End If
                                                    'End If
                                                    ModificaEtapaApForm(oApp, oForm, DocEntryForm, Trim(EtapaMod), Trim(LinEtapaMod), Trim(Formulario))
                                                    If EstFinEtapaMod <> "-1" Then
                                                        ModificaEstadoProcForm(oApp, oForm, DocEntryForm, Trim(EstFinEtapaMod), Trim(Formulario))
                                                    End If
                                                    Return True
                                                Else
                                                    oForm.Freeze(False)
                                                    oApp.MessageBox("No se encontraron siguientes etapas para este modelo. Verifique")
                                                    Return False
                                                End If
                                            Else
                                                oForm.Freeze(False)
                                                oApp.MessageBox("No se encontraron siguientes etapas para este modelo. Verifique")
                                                Return False
                                            End If
                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            oApp.MessageBox(ex.Message)
                                            Return False
                                        End Try
                                    Else
                                        ModificaEstadoApForm(oApp, oForm, DocEntryForm, "A", Trim(Formulario))
                                        If EstFinEtapaMod <> "-1" Then
                                            ModificaEstadoProcForm(oApp, oForm, DocEntryForm, Trim(EstFinEtapaMod), Trim(Formulario))
                                        End If
                                        Return True
                                    End If

                                Else
                                    ModificaEstadoApForm(oApp, oForm, DocEntryForm, "A", Trim(Formulario))
                                    If EstFinEtapaMod <> "-1" Then
                                        ModificaEstadoProcForm(oApp, oForm, DocEntryForm, Trim(EstFinEtapaMod), Trim(Formulario))
                                    End If
                                    Return True
                                End If
                            Else
                                Return True
                            End If

                        Catch ex As Exception
                            oForm.Freeze(False)
                            oApp.MessageBox(ex.Message)
                            Return False
                        End Try
                    End If



                    'EN CASO DE QUE LA AUTORIZACION SEA RECHAZO
                ElseIf Trim(CodAprobacion) = "R" Then
                    'SBOFormMotRec.CreaSBOForm(oApp)
                    'SE VERIFICA LA ETAPA
                    If Trim(Etapa) = "" Then
                        'si esta vacia la etapa se verifica si hay modelo
                        Try
                            ResValMod = ValidaSiTieneModelo(oApp, oForm, Trim(Formulario))
                        Catch ex As Exception
                            ResValMod = ""
                        End Try

                        If Trim(ResValMod) = "S" Then

                            Try
                                CodModelo = GetModeloAutForm(Trim(Formulario))
                            Catch ex As Exception
                                CodModelo = ""
                            End Try

                            If CodModelo <> "" Then



                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al intentar capturar el modelo de aprobación para este formulario.")
                                Return False
                            End If
                            'si no tiene modelo
                        ElseIf Trim(ResValMod) = "N" Then
                            ModificaEstadoApForm(oApp, oForm, DocEntryForm, "R", Trim(Formulario))
                            ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "101", Trim(Formulario))
                            'SBOFormMotRec.CreaSBOForm(oApp)
                            Return True
                            'si no resulta en nada se asume que no tiene modelo
                        Else
                            ModificaEstadoApForm(oApp, oForm, DocEntryForm, "R", Trim(Formulario))
                            ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "101", Trim(Formulario))
                            'SBOFormMotRec.CreaSBOForm(oApp)
                            Return True
                        End If

                    Else
                        Dim nroRegLog As String
                        Try
                            nroR = GetNroRechEtapa(Trim(Etapa))

                            If nroR = "1" Then 'cnp20190711
                                ModificaEstadoApForm(oApp, oForm, DocEntryForm, "R", Trim(Formulario))
                                ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "101", Trim(Formulario))
                                Return True
                            End If

                            nroRegLog = GetFromLogAut(Trim(Formulario), Trim(DocEntryForm), Trim(Etapa), Trim(CodAprobacion), Trim(EstadoProc))

                            If nroRegLog >= nroR Then
                                ModificaEstadoApForm(oApp, oForm, DocEntryForm, "R", Trim(Formulario))
                                ModificaEstadoProcForm(oApp, oForm, DocEntryForm, "101", Trim(Formulario))
                                'SBOFormMotRec.CreaSBOForm(oApp)
                                Return True
                            Else
                                'SBOFormMotRec.CreaSBOForm(oApp)
                                Return True
                            End If
                        Catch ex As Exception
                            oForm.Freeze(False)
                            oApp.MessageBox(ex.Message)
                            Return False
                        End Try

                    End If
                    SBOFormMotRec.CreaSBOForm(oApp)

                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No se encontro un proceso para la aprobación seleccionada. Verifique")
                    Return False

                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function



        Public Shared Function ManejaBtnAcc(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, CodUsr As String, Formulario As String, Estado As String, etapa As String, linetapa As String, DocEntryForm As String, PresEntry As String, CorrelativoSol As String) As Boolean
            Dim btnAcc As SAPbouiCOM.ButtonCombo
            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty
            Dim res As String = String.Empty
            Dim resUsrAut As String = String.Empty

            Try
                'oForm = oApp.Forms.Item("UDO_F_LICRE") 'cambio actual
                'Set oForm = SBO_Application.Forms.Add("LICRE", ft_Fixed)
                'oForm.Freeze(True)
                btnAcc = oForm.Items.Item("btnAproba").Specific

                'se verifica si el estado de aprobacion es el inicial
                If Estado = "-" Then
                    'si es inicial se verifica si el formulario tiene un modelo asociado
                    res = ValidaSiTieneModelo(oApp, oForm, Formulario)

                    'SI TIENE UN MODELO
                    If Trim(res) = "S" Then
                        'SE VERIFICA SI EL BOTON TIENE VALORES VALIDOS. sI TIENE SE LOS BORRA Y CARGA. SI NO SOLO SE CARGA
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next

                            btnAcc.ValidValues.Add("S", "Solicitar Autorización")
                            'btnAcc.ValidValues.Add("R", "Rechazar Autorización") 'agregado por Mick
                            btnAcc.Caption = "Autorización"
                            Return True
                        Else
                            btnAcc.ValidValues.Add("S", "Solicitar Autorización")
                            'btnAcc.ValidValues.Add("R", "Rechazar Autorización") 'agregado por Mick
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If

                        'SI NO TIENE UN MODELO ASOCIADO
                    ElseIf Trim(res) = "N" Then
                        'SE VERIFICA LOS VALORES VALIDOS DEL BOTON Y SE CARGA
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.ValidValues.Add("A", "Aprobar")
                            btnAcc.ValidValues.Add("R", "Rechazar")
                            btnAcc.Caption = "Autorización"
                            Return True
                        Else
                            btnAcc.ValidValues.Add("A", "Aprobar")
                            btnAcc.ValidValues.Add("R", "Rechazar")
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                        'SI NO TENEMOS RESPUESTA DE MODELO SE BORRA CUALQUIER VALOR VALIDO
                    Else
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                    End If

                    'si el estado esta en rechazado se borran los valores validos
                ElseIf Estado = "R" Then
                    If btnAcc.ValidValues.Count > 0 Then
                        For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                            btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        Next
                        btnAcc.Caption = "Autorización"
                        Return True
                    Else
                        btnAcc.Caption = "Autorización"
                        Return True
                    End If
                    'si el estado esta pendiente se valida si tiene modelo y etapas para sacar los usuarios
                ElseIf Estado = "P" Then
                    'se valida si tiene modelo
                    res = ValidaSiTieneModelo(oApp, oForm, Formulario)
                    'si tiene modelo
                    If Trim(res) = "S" Then

                        Dim usr As String = String.Empty
                        Dim usrEtp As String = String.Empty
                        Dim oDTUsr As DataTable

                        usr = DiApp.UserSignature
                        'se sacan los usuarios de la etapa actual
                        oDTUsr = GetUsrsEtapa(Trim(etapa))
                        If Not oDTUsr Is Nothing Then
                            If oDTUsr.Rows.Count > 0 Then
                                For Each row As DataRow In oDTUsr.Rows
                                    usrEtp = row.Item("USR")
                                    'SE VALIDA EL USUARIO DE LA ETAPA CON EL USUARIO ACTUAL
                                    If Trim(usr) = Trim(usrEtp) Then

                                        'ACA DEBERIAMOS VALIDAR SI EL USUARIO DE LA ETAPA YA REALIZO TODAS SUS ACCIONES. SI NO, SE SIGUE NORMAL, SI YA REALIZO SE BORRA VALORES VALIDOS

                                        resUsrAut = GetUsrFromLogAut(Formulario, DocEntryForm, PresEntry, etapa, usr, CorrelativoSol)
                                        If Trim(resUsrAut) <> "0" Then
                                            If btnAcc.ValidValues.Count > 0 Then
                                                For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                                    btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                                Next
                                                btnAcc.Caption = "Autorización"
                                                Return True
                                            Else
                                                btnAcc.Caption = "Autorización"
                                                Return True
                                            End If
                                        End If

                                        If btnAcc.ValidValues.Count > 0 Then
                                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                            Next
                                            btnAcc.ValidValues.Add("A", "Aprobar")
                                            btnAcc.ValidValues.Add("R", "Rechazar")
                                            btnAcc.Caption = "Autorización"
                                            Return True
                                        Else
                                            btnAcc.ValidValues.Add("A", "Aprobar")
                                            btnAcc.ValidValues.Add("R", "Rechazar")
                                            btnAcc.Caption = "Autorización"
                                            Return True
                                        End If
                                    End If
                                Next
                                'si no se pudo validar el usuario actual con alguno de la etapa se quitan los valores validos
                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            Else
                                'SI NO SE TIENEN USUARIOS EN LA ETAPA SE BORRA VALORES VALIDOS
                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            End If
                            'si no se tiene usuarios en el DT se vacia el boton
                        Else
                            If btnAcc.ValidValues.Count > 0 Then
                                For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                    btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                Next
                                btnAcc.Caption = "Autorización"
                                Return True
                            Else
                                btnAcc.Caption = "Autorización"
                                Return True
                            End If
                        End If
                        'SI ESTA PENDIENTE PERO NO TIENE MODELO, SE LLENA CON APROBAR O RECHAZAR
                    ElseIf Trim(res) = "N" Then
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.ValidValues.Add("A", "Aprobar")
                            btnAcc.ValidValues.Add("R", "Rechazar")
                            btnAcc.Caption = "Autorización"
                            Return True
                        Else
                            btnAcc.ValidValues.Add("A", "Aprobar")
                            btnAcc.ValidValues.Add("R", "Rechazar")
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                        'SI NO SE TIENE RESPUESTA DEL MODELO SE BORRA TODO VALOR VALIDO
                    Else
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                    End If
                    'SI EL ESTADO ES APROBADO
                ElseIf Estado = "A" Then

                    'se valida si tiene modelo
                    res = ValidaSiTieneModelo(oApp, oForm, Formulario)
                    'si tiene modelo
                    If Trim(res) = "S" Then
                        Dim CodModelo As String = String.Empty

                        Try
                            CodModelo = GetModeloAutForm(Trim(Formulario))
                        Catch ex As Exception
                            CodModelo = ""
                        End Try
                        'si se tiene un codigo de modelo
                        If CodModelo <> "" Then

                            If Trim(etapa) = "" And Trim(linetapa) = "" Then

                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            End If

                            Dim EtpFin As String = String.Empty

                            EtpFin = GetSiEtapaFinal(Trim(CodModelo), Trim(etapa))

                            If Trim(EtpFin) = "Y" Then
                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            End If


                            Dim resHayMas As String

                            resHayMas = GetSiHayMasEtapas(Trim(CodModelo), Trim(linetapa))
                            If resHayMas > 0 Then
                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next

                                    btnAcc.ValidValues.Add("S", "Solicitar Autorización")
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.ValidValues.Add("S", "Solicitar Autorización")
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            Else
                                If btnAcc.ValidValues.Count > 0 Then
                                    For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                        btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                    Next
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                Else
                                    btnAcc.Caption = "Autorización"
                                    Return True
                                End If
                            End If
                        Else
                            If btnAcc.ValidValues.Count > 0 Then
                                For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                    btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                Next
                                btnAcc.Caption = "Autorización"
                                Return True
                            Else
                                btnAcc.Caption = "Autorización"
                                Return True
                            End If
                        End If

                    ElseIf Trim(res) = "N" Then
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.Caption = "Autorización"
                            Return True
                        Else
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                    Else
                        If btnAcc.ValidValues.Count > 0 Then
                            For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                                btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            btnAcc.Caption = "Autorización"
                            Return True
                        Else
                            btnAcc.Caption = "Autorización"
                            Return True
                        End If
                    End If

                    'si el estado es cualquier otro se borran los valores validos
                Else
                    If btnAcc.ValidValues.Count > 0 Then
                        For i As Integer = 0 To btnAcc.ValidValues.Count - 1
                            btnAcc.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        Next
                        btnAcc.Caption = "Autorización"
                        Return True
                    Else
                        btnAcc.Caption = "Autorización"
                        Return True
                    End If
                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function GetUsrEtapaAlarma(CodEtapa As String) As DataTable
            Try 'cnp20190710
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetUsrEtapaAlarma(CodEtapa)
                Else
                    ostr = HanaAutorizaciones.GetUsrEtapaAlarma(CodEtapa)
                End If

                Return conexi.obtenerColeccion(ostr, False)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetUsrsEtapa(codEtapa As String) As DataTable
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetUsrsEtapa(codEtapa)

                Else
                    ostr = HanaAutorizaciones.GetUsrsEtapa(codEtapa)
                End If
                Return conexi.obtenerColeccion(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try

        End Function

        Public Shared Function GetSiExisteAlarma(CodUsuario As String, Etapa As String, Formulario As String) As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.ValidaAlarmasEtapaUsr(CodUsuario, Etapa, Formulario) 'cnp20190710
                Else
                    ostr = HanaAutorizaciones.ValidaAlarmasEtapaUsr(CodUsuario, Etapa, Formulario)
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try

        End Function

        Public Shared Function GetSiFormTieneModelo(formulario As String) As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiFormTieneModelo(formulario)
                Else
                    ostr = HanaAutorizaciones.GetSiFormTieneModelo(formulario)
                End If
                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function

        Public Shared Function GetModeloAutForm(formulario As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetModeloAutForm(Trim(formulario))

                Else
                    ostr = HanaAutorizaciones.GetModeloAutForm(Trim(formulario))
                End If

                Return conexi.obtenerValor(ostr)
            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function


        Public Shared Function PopulaGridHisA(formulario As String, docentry As String) As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.PopulaGridHisA(formulario, docentry)
                Else
                    ostr = HanaAutorizaciones.PopulaGridHisA(formulario, docentry)
                End If
                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

        Public Shared Function PopulaGridGrpAut(Optional Formulario As String = "") As String
            Try
                Dim ostr As String = String.Empty

                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.PopulaGridGrpAut(Formulario)
                Else
                    ostr = HanaAutorizaciones.PopulaGridGrpAut(Formulario)
                End If
                Return ostr
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function


        Public Shared Function InsertaAutorizacionLog(Formulario As String, DocEntry As String, Presupuesto As String, Correlativo As String, CodeEstado As String, DescrEstado As String, CodeAccion As String, DescrAccion As String, Etapa As String, CodUsr As String, NomUsr As String, Fecha As String, CorrelativoSol As String, motivo As String) As Boolean
            If Trim(Correlativo) = "" Then Correlativo = "0"
            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.InsertaAutorizacionLog(Formulario, DocEntry, Presupuesto, Correlativo, CodeEstado, DescrEstado, CodeAccion, DescrAccion, Etapa, CodUsr, NomUsr, Fecha, CorrelativoSol, motivo)
                Else
                    Return HanaAutorizaciones.InsertaAutorizacionLog(Formulario, DocEntry, Presupuesto, Correlativo, CodeEstado, DescrEstado, CodeAccion, DescrAccion, Etapa, CodUsr, NomUsr, Fecha, CorrelativoSol, motivo)
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function InsertaModificacionLog(Formulario As String, DocEntry As String, CodUsr As String, NomUsr As String, FechaAnt As String, FechaFin As String, FechaMod As String) As Boolean

            Try
                If Motor = "SQL" Then
                    Return SQLAutorizaciones.InsertaModificacionLog(Formulario, DocEntry, CodUsr, NomUsr, FechaAnt, FechaFin, FechaMod)
                Else
                    Return HanaAutorizaciones.InsertaModificacionLog(Formulario, DocEntry, CodUsr, NomUsr, FechaAnt, FechaFin, FechaMod)
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function ObtienFechaAnteriorLinCred(code As String) As String
            Dim ostr As String = String.Empty
            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.ObtienFechaAnteriorLinCred(code)
                Else
                    ostr = HanaAutorizaciones.ObtienFechaAnteriorLinCred(code)
                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function


        Public Shared Function GetSiEtapaFinal(CodModelo As String, Etapa As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiEtapaFinal(Trim(CodModelo), Trim(Etapa))
                Else
                    ostr = HanaAutorizaciones.GetSiEtapaFinal(Trim(CodModelo), Trim(Etapa))
                End If

                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try
        End Function


        Public Shared Function GetSiHayMasEtapas(CodModelo As String, LineaEtapa As String) As String
            Dim ostr As String = String.Empty

            Try
                If Motor = "SQL" Then
                    ostr = SQLAutorizaciones.GetSiHayMasEtapas(Trim(CodModelo), Trim(LineaEtapa))
                Else
                    ostr = HanaAutorizaciones.GetSiHayMasEtapas(Trim(CodModelo), Trim(LineaEtapa))
                End If

                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
                Return Nothing
            End Try


        End Function

    End Class
End Namespace




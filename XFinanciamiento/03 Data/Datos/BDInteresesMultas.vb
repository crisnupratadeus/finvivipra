﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data  
    Public Class BDInteresesMultas
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared dtls As DataTable = New DataTable
         Public Shared Function getInteresMulta(FechaPago As String, FechaVencimiento As String, Tipo_Periodo As string, InteresTasa As string , MontoPendienteDePago As string, TasaAnualMoratoria As string, DiasParaTasaAnualMoratoria As string) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then

                    query = sqlInteresesMultas.getInteresMulta(FechaPago,FechaVencimiento,Tipo_Periodo,DatosDB.DBDS_IN(InteresTasa),DatosDB.DBDS_IN(MontoPendienteDePago),DatosDB.DBDS_IN(TasaAnualMoratoria),DiasParaTasaAnualMoratoria)

                Else
                    query = hanaInteresesMultas.getInteresMulta(FechaPago,FechaVencimiento,Tipo_Periodo,DatosDB.DBDS_IN(InteresTasa),DatosDB.DBDS_IN(MontoPendienteDePago),DatosDB.DBDS_IN(TasaAnualMoratoria),DiasParaTasaAnualMoratoria)

                End If

                 Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getDatosFacturaParaPago(DocNum As String, CardCode As String) As DataTable
            Try
                Dim query As String = ""
                If Motor = "SQL" Then

                    query = sqlInteresesMultas.getDatosFacturaParaPago(DocNum, CardCode)

                Else
                    query = hanaInteresesMultas.getDatosFacturaParaPago(DocNum, CardCode)

                End If

                Return conexi.obtenerColeccion(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getCalculo_pagov2(V_oV As String, V_Fecha_Ini As String, V_Fecha_UltPago As String, V_Fecha_pago As String, V_Fecha_valor As String, GastosA As String, PorFactura As String) As String

            Dim query As String = ""
            Try
                If Motor = "SQL" Then

                    query = sqlInteresesMultas.getCalculo_pagov2(V_oV, V_Fecha_Ini, V_Fecha_UltPago, V_Fecha_pago, V_Fecha_valor, GastosA)

                Else
                    query = hanaInteresesMultas.getCalculo_pagov2(V_oV, V_Fecha_Ini, V_Fecha_UltPago, V_Fecha_pago, V_Fecha_valor, GastosA, PorFactura)

                End If

                Return query

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getUltimaFechaPagada(U_DocEntryF As String, U_CardCode As String) As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then

                    query = sqlInteresesMultas.getUltimaFechaPagada(U_DocEntryF, U_CardCode)

                Else
                    query = hanaInteresesMultas.getUltimaFechaPagada(U_DocEntryF, U_CardCode)

                End If

                Return conexi.obtenerValor(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function getFechaInicioCuotas(U_DocEntryOV As String) As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then

                    'query = sqlInteresesMultas.getFechaInicioCuotas(U_DocEntryOV)

                Else

                    query = hanaInteresesMultas.getFechaInicioCuotas(U_DocEntryOV)

                End If

                Return conexi.obtenerValor(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function getDocEntryOV(U_DocEntryOV As String) As String
            Try
                Dim query As String = ""
                If Motor = "SQL" Then

                    'query = sqlInteresesMultas.getFechaInicioCuotas(U_DocEntryOV)

                Else

                    query = hanaInteresesMultas.getDocEntryOV(U_DocEntryOV)

                End If

                Return conexi.obtenerValor(query)

            Catch ex As Exception
                Throw ex
            End Try
        End Function





    End Class
End Namespace

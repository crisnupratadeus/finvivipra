﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml

'Imports CarDealer.Principal

Namespace Data
    Public Class DatosReserva
        Public Shared conexi As Conexiones = New Conexiones
        Public Shared Function EsDocumentoDeVehiculos(ItemCodes As String) As Boolean
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlReserva.EsDocumentoDeVehiculos_TEXT(ItemCodes)
                Else
                    ostr = hanaReserva.EsDocumentoDeVehiculos_TEXT(ItemCodes)
                End If

                Dim dRes As Integer = conexi.obtenerValor(ostr)
                If dRes > 0 Then
                    Return True
                Else
                    Return False
                End If                

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function EsVehiculo(ItemCode As String) As Boolean
            Try
                Dim ostr As String

                If Motor = "SQL" Then
                    ostr = sqlReserva.EsVehiculo_TEXT(ItemCode)
                Else
                    ostr = hanaReserva.EsVehiculo_TEXT(ItemCode)
                End If
                Dim Res As String = Trim(conexi.obtenerValor(ostr))
                If Res = "Y" Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetSumDec() As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlReserva.GetSumDec_TEXT
                Else
                    ostr = hanaReserva.GetSumDec_TEXT
                End If
                Return conexi.obtenerValor(ostr)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub ActualizaCampoUsuario(Docentry As String, CampoUsuario As String, Valor As String, Tabla As String)
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlReserva.ActualizaCampoUsuario_TEXT(Docentry, CampoUsuario, Valor, Tabla)
                Else
                    ostr = hanaReserva.ActualizaCampoUsuario_TEXT(Docentry, CampoUsuario, Valor, Tabla)
                End If
                conexi.creaRegistro(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function CrearAsiento(ByRef oData As DataTable, ByRef transID As String) As Boolean
            Try
                Dim vJE As SAPbobsCOM.JournalEntries
                vJE = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)

                Dim res As Boolean
                Dim Cuenta As String
                Dim EsDebito As Boolean
                Dim Monto As Decimal
                Dim BoxPatent As String
                Dim OcrCode As String = ""
                Dim OcrCode2 As String = ""
                Dim OcrCode3 As String = ""
                Dim OcrCode4 As String = ""
                Dim OcrCode5 As String = ""
                Dim FechaContabilizacion As String = ""
                Dim Glosa As String = ""
                Dim Moneda As String
                Dim iSumdec As Integer
                Dim Serie As String
                Dim BaseEntry As String

                Dim AcumuladorMonto As Decimal = 0
                Dim LineNum As Integer = 0
                Dim dmonto As Decimal = 0
                Dim montoReal As Decimal = 0

                For Each row As DataRow In oData.Rows
                    Cuenta = row.Field(Of String)("Cuenta")
                    EsDebito = row.Field(Of Boolean)("EsDebito")
                    Monto = row.Field(Of Decimal)("Monto")
                    BoxPatent = row.Field(Of String)("BoxPatent")
                    OcrCode = row.Field(Of String)("OcrCode")
                    OcrCode2 = row.Field(Of String)("OcrCode2")
                    OcrCode3 = row.Field(Of String)("OcrCode3")
                    OcrCode4 = row.Field(Of String)("OcrCode4")
                    OcrCode5 = row.Field(Of String)("OcrCode5")
                    FechaContabilizacion = row.Field(Of String)("FechaContabilizacion")
                    Glosa = row.Field(Of String)("Glosa")
                    Moneda = row.Field(Of String)("Moneda")
                    iSumdec = row.Field(Of Integer)("iSumdec")
                    Serie = row.Field(Of String)("Serie")
                    BaseEntry = row.Field(Of String)("DocEntryReserva")

                    If LineNum = 0 Then
                        vJE.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.ReferenceDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Series = Serie
                        If Glosa <> "" Then vJE.Memo = Glosa
                        'vJE.Reference = comentario
                        vJE.UserFields.Fields.Item("U_RefImp").Value = "Y"
                        vJE.UserFields.Fields.Item("U_BaseEntry").Value = CStr(BaseEntry)
                    End If

                    'Dim ssppss As String = vJE.GetAsXML()



                    dmonto = Monto
                    If dmonto <> 0 Then

                        Call vJE.Lines.Add()
                        Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                        LineNum = LineNum + 1

                        vJE.Lines.AccountCode = Cuenta
                        If dmonto <> 0 Then
                            If Moneda = MonedaLocal Or Moneda = "##" Then
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.Debit = montoReal
                                    Else
                                        vJE.Lines.Credit = -montoReal
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.Credit = montoReal
                                    Else
                                        vJE.Lines.Debit = -montoReal
                                    End If
                                End If
                            Else
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.FCDebit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCCredit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.FCCredit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCDebit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                End If
                            End If
                        End If

                        vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ShortName = Cuenta
                        vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                        vJE.Lines.Reference2 = BoxPatent
                        vJE.Lines.LineMemo = Left(Glosa, 50)

                        If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                        If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                        If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                        If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                        If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    End If
                    ' vJE.Lines.Add()

                    'ssppss = vJE.GetAsXML()
                    'ssppss = ssppss
                Next

                dmonto = AcumuladorMonto


                If dmonto <> 0 Then
                    EsDebito = False
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = Cuenta
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If


                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ShortName = Cuenta
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.LineMemo = Left(Glosa, 50)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                End If

                Dim iError As Integer = vJE.Add()

                If iError <> 0 Then
                    Dim strxml As String
                    'strxml = vJE.GetAsXML
                    Dim sError As String = String.Empty
                    DiApp.GetLastError(iError, sError)
                    res = False
                    Throw New Exception("[CD] Problema al crear asiento contable: " + sError)
                Else
                    DiApp.GetNewObjectCode(transID)
                    res = True
                End If
                If Not IsNothing(vJE) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(vJE)
                    vJE = Nothing
                    oMc = Nothing
                End If
                Return res

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Shared Function GetSegundaCuenta(DocentryEntrada As String, PrimeraCuenta As String) As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlReserva.GetSegundaCuenta_TEXT(DocentryEntrada, PrimeraCuenta)
                Else
                    ostr = hanaReserva.GetSegundaCuenta_TEXT(DocentryEntrada, PrimeraCuenta)
                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function CrearAsientoEntrada(ByRef oData As DataTable, ByRef transID As String) As Boolean
            Try
                Dim vJE As SAPbobsCOM.JournalEntries
                vJE = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)

                Dim res As Boolean
                Dim Cuenta As String
                Dim EsDebito As Boolean
                Dim Monto As Decimal
                Dim BoxPatent As String
                Dim OcrCode As String
                Dim OcrCode2 As String
                Dim OcrCode3 As String
                Dim OcrCode4 As String
                Dim OcrCode5 As String
                Dim FechaContabilizacion As String
                Dim Glosa As String
                Dim Moneda As String
                Dim iSumdec As Integer
                Dim Serie As String
                Dim BaseEntry As String
                Dim BaseEntryReserva As String

                Dim AcumuladorMonto As Decimal = 0
                Dim LineNum As Integer = 0
                Dim dmonto As Decimal = 0
                Dim montoReal As Decimal = 0
                Dim SegundaCuenta As String


                For Each row As DataRow In oData.Rows
                    Cuenta = row.Field(Of String)("Cuenta")
                    EsDebito = row.Field(Of Boolean)("EsDebito")
                    Monto = row.Field(Of Decimal)("MontoJrnl")
                    BoxPatent = row.Field(Of String)("BoxPatent")
                    OcrCode = row.Field(Of String)("OcrCode")
                    OcrCode2 = row.Field(Of String)("OcrCode2")
                    OcrCode3 = row.Field(Of String)("OcrCode3")
                    OcrCode4 = row.Field(Of String)("OcrCode4")
                    OcrCode5 = row.Field(Of String)("OcrCode5")
                    FechaContabilizacion = row.Field(Of String)("FechaContabilizacion")
                    Glosa = row.Field(Of String)("Glosa")
                    Moneda = row.Field(Of String)("Moneda")
                    iSumdec = row.Field(Of Integer)("iSumdec")
                    Serie = row.Field(Of String)("Serie")
                    BaseEntry = row.Field(Of String)("DocEntryEntrada")
                    BaseEntryReserva = row.Field(Of String)("BaseEntry")
                    SegundaCuenta = row.Field(Of String)("SegundaCuenta")

                    If LineNum = 0 Then
                        vJE.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.ReferenceDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Series = Serie
                        If Glosa <> "" Then vJE.Memo = Glosa
                        'vJE.Reference = comentario
                        vJE.UserFields.Fields.Item("U_RefImp").Value = "Y"
                        vJE.UserFields.Fields.Item("U_BaseEntry").Value = CStr(BaseEntryReserva)
                        vJE.UserFields.Fields.Item("U_EntryOfGoods").Value = CStr(BaseEntry)
                    End If

                    'Dim ssppss As String = vJE.GetAsXML()


                    dmonto = Monto
                    If dmonto <> 0 Then

                        Call vJE.Lines.Add()
                        Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                        LineNum = LineNum + 1

                        vJE.Lines.AccountCode = Cuenta
                        If dmonto <> 0 Then
                            If Moneda = MonedaLocal Or Moneda = "##" Then
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.Debit = montoReal
                                    Else
                                        vJE.Lines.Credit = -montoReal
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.Credit = montoReal
                                    Else
                                        vJE.Lines.Debit = -montoReal
                                    End If
                                End If
                            Else
                                montoReal = Math.Round(dmonto, iSumdec)
                                AcumuladorMonto = AcumuladorMonto + montoReal
                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.FCDebit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCCredit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.FCCredit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCDebit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                End If
                            End If
                        End If


                        vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ShortName = Cuenta
                        vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                        vJE.Lines.Reference2 = BoxPatent

                        If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                        If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                        If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                        If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                        If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    End If
                    '-------------------------------
                    If dmonto <> 0 Then

                        Call vJE.Lines.Add()
                        Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                        LineNum = LineNum + 1

                        vJE.Lines.AccountCode = SegundaCuenta
                        EsDebito = Not EsDebito
                        If dmonto <> 0 Then
                            If Moneda = MonedaLocal Or Moneda = "##" Then
                                montoReal = Math.Round(dmonto, iSumdec)

                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.Debit = montoReal
                                    Else
                                        vJE.Lines.Credit = -montoReal
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.Credit = montoReal
                                    Else
                                        vJE.Lines.Debit = -montoReal
                                    End If
                                End If
                            Else
                                montoReal = Math.Round(dmonto, iSumdec)

                                If EsDebito = True Then
                                    If montoReal > 0 Then
                                        vJE.Lines.FCDebit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCCredit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                Else
                                    If montoReal > 0 Then
                                        vJE.Lines.FCCredit = montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    Else
                                        vJE.Lines.FCDebit = -montoReal
                                        vJE.Lines.FCCurrency = Moneda
                                    End If
                                End If
                            End If
                        End If


                        vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.ShortName = SegundaCuenta
                        vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                        vJE.Lines.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent
                        vJE.Lines.LineMemo = BoxPatent

                        If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                        If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                        If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                        If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                        If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                    End If



                    ' vJE.Lines.Add()

                    'ssppss = vJE.GetAsXML()
                    'ssppss = ssppss
                Next

                dmonto = AcumuladorMonto


                If dmonto <> 0 Then
                    EsDebito = True
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = Cuenta
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If


                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ShortName = Cuenta
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                End If
                '----------------------------------------------
                If dmonto <> 0 Then
                    EsDebito = False
                    Call vJE.Lines.Add()
                    Call vJE.Lines.SetCurrentLine(LineNum)   'JCA recuerda q van juntos
                    LineNum = LineNum + 1

                    vJE.Lines.AccountCode = SegundaCuenta
                    If dmonto <> 0 Then
                        If Moneda = MonedaLocal Or Moneda = "##" Then
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.Debit = montoReal
                                Else
                                    vJE.Lines.Credit = -montoReal
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.Credit = montoReal
                                Else
                                    vJE.Lines.Debit = -montoReal
                                End If
                            End If
                        Else
                            montoReal = Math.Round(dmonto, iSumdec)
                            If EsDebito = True Then
                                If montoReal > 0 Then
                                    vJE.Lines.FCDebit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCCredit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            Else
                                If montoReal > 0 Then
                                    vJE.Lines.FCCredit = montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                Else
                                    vJE.Lines.FCDebit = -montoReal
                                    vJE.Lines.FCCurrency = Moneda
                                End If
                            End If
                        End If
                    End If


                    vJE.Lines.DueDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ReferenceDate1 = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                    vJE.Lines.ShortName = SegundaCuenta
                    vJE.Lines.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)

                    If OcrCode <> "" Then vJE.Lines.CostingCode = OcrCode
                    If OcrCode2 <> "" Then vJE.Lines.CostingCode2 = OcrCode2
                    If OcrCode3 <> "" Then vJE.Lines.CostingCode3 = OcrCode3
                    If OcrCode4 <> "" Then vJE.Lines.CostingCode4 = OcrCode4
                    If OcrCode5 <> "" Then vJE.Lines.CostingCode5 = OcrCode5
                End If


                Dim iError As Integer = vJE.Add()

                If iError <> 0 Then
                    Dim strxml As String
                    'strxml = vJE.GetAsXML
                    Dim sError As String = String.Empty
                    DiApp.GetLastError(iError, sError)
                    res = False
                    Throw New Exception("[CD] Problema al crear asiento contable: " + sError)
                Else
                    DiApp.GetNewObjectCode(transID)
                    res = True
                End If
                If Not IsNothing(vJE) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(vJE)
                    vJE = Nothing
                    oMc = Nothing
                End If
                Return res

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        

        Public Shared Function CreaRevalorizacion(ByRef oData As DataTable, ByRef transID As String) As Boolean
            Try

                Dim oRevalInv As SAPbobsCOM.MaterialRevaluation
                oRevalInv = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMaterialRevaluation)

               

                Dim res As Boolean = False
                Dim CuentaA As String
                Dim CuentaD As String
                Dim EsDebito As Boolean
                Dim Monto As Decimal
                Dim BoxPatent As String
                Dim OcrCode As String
                Dim OcrCode2 As String
                Dim OcrCode3 As String
                Dim OcrCode4 As String
                Dim OcrCode5 As String
                Dim FechaContabilizacion As String
                Dim Glosa As String
                Dim Moneda As String
                Dim iSumdec As Integer
                Dim Serie As String
                Dim BaseEntry As String
                Dim ItemCode As String
                Dim Whs As String
                Dim AbsEntry As String
                Dim DocEntryEntrada As String

                Dim AcumuladorMonto As Decimal = 0
                Dim LineNum As Integer = 0
                Dim dmonto As Decimal = 0
                Dim montoReal As Decimal = 0

               

                For Each row As DataRow In oData.Rows
                    CuentaA = row.Field(Of String)("CuentaA")
                    CuentaD = row.Field(Of String)("CuentaD")
                    EsDebito = row.Field(Of Boolean)("EsDebito")
                    Monto = row.Field(Of Decimal)("Monto")
                    BoxPatent = row.Field(Of String)("BoxPatent")
                    OcrCode = row.Field(Of String)("OcrCode")
                    OcrCode2 = row.Field(Of String)("OcrCode2")
                    OcrCode3 = row.Field(Of String)("OcrCode3")
                    OcrCode4 = row.Field(Of String)("OcrCode4")
                    OcrCode5 = row.Field(Of String)("OcrCode5")
                    FechaContabilizacion = row.Field(Of String)("FechaContabilizacion")
                    Glosa = row.Field(Of String)("Glosa")
                    Moneda = row.Field(Of String)("Moneda")
                    iSumdec = row.Field(Of Integer)("iSumdec")
                    Serie = row.Field(Of String)("SerieRev")
                    BaseEntry = row.Field(Of String)("BaseEntry")
                    DocEntryEntrada = row.Field(Of String)("DocEntryEntrada")
                    ItemCode = row.Field(Of String)("ItemCode")
                    Whs = row.Field(Of String)("Whs")
                    AbsEntry = row.Field(Of String)("AbsEntry")

                    If Monto <> 0 Then
                       

                        If LineNum = 0 Then
                            oRevalInv.DocDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            oRevalInv.TaxDate = Utiles.Util.GetfechafromStringDataSource(FechaContabilizacion)
                            oRevalInv.Series = Serie

                            oRevalInv.InflationRevaluation = SAPbobsCOM.BoYesNoEnum.tNO
                            oRevalInv.RevalType = "M"

                            If Glosa <> "" Then oRevalInv.Comments = Glosa
                            'vJE.Reference = comentario
                            oRevalInv.UserFields.Fields.Item("U_RefImp").Value = "Y"
                            oRevalInv.UserFields.Fields.Item("U_BaseEntry").Value = CStr(DocEntryEntrada)
                            oRevalInv.UserFields.Fields.Item("U_ResEntry").Value = CStr(BaseEntry)
                            oRevalInv.UserFields.Fields.Item("U_BoxPatent").Value = BoxPatent

                        End If
                        dmonto = Monto

                        oRevalInv.Lines.ItemCode = ItemCode
                        oRevalInv.Lines.WarehouseCode = Whs
                        oRevalInv.Lines.RevaluationIncrementAccount = Trim(CuentaA)
                        oRevalInv.Lines.RevaluationDecrementAccount = Trim(CuentaD)

                        If OcrCode <> "" Then oRevalInv.Lines.DistributionRule = Trim(OcrCode)
                        If OcrCode2 <> "" Then oRevalInv.Lines.DistributionRule2 = Trim(OcrCode2)
                        If OcrCode3 <> "" Then oRevalInv.Lines.DistributionRule3 = Trim(OcrCode3)
                        If OcrCode4 <> "" Then oRevalInv.Lines.DistributionRule4 = Trim(OcrCode4)
                        If OcrCode5 <> "" Then oRevalInv.Lines.DistributionRule5 = Trim(OcrCode5)


                        Dim Srscount As Integer
                        Dim SNBService As SAPbobsCOM.MaterialRevaluationSNBService
                        Dim SNBParams As SAPbobsCOM.MaterialRevaluationSNBParams
                        Dim SNBParamCol As SAPbobsCOM.MaterialRevaluationSNBParamsCollection
                        Dim SNBParam As SAPbobsCOM.MaterialRevaluationSNBParam
                        Dim compService As SAPbobsCOM.CompanyService
                        Dim SNBLines As SAPbobsCOM.SNBLines

                        SNBLines = oRevalInv.Lines.SNBLines
                        compService = DiApp.GetCompanyService()
                        SNBService = compService.GetBusinessService(SAPbobsCOM.ServiceTypes.MaterialRevaluationSNBService)
                        SNBParams = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParams)
                        SNBParam = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParam)
                        SNBParam.ItemCode = ItemCode
                        SNBParamCol = SNBService.GetList(SNBParam)
                        Srscount = SNBParamCol.Count

                        For i As Integer = 0 To SNBParamCol.Count - 1
                            SNBParams = SNBParamCol.Item(i)
                            'se valida que sea la serie que esta siendo usada y se llena la informacion a las lineas faltantes de serie
                            If AbsEntry = SNBParams.SnbAbsEntry Then
                                SNBLines.SnbAbsEntry = SNBParams.SnbAbsEntry
                                SNBLines.DebitCredit = dmonto
                                Exit For
                            End If
                        Next
                        'Call oRevalInv.Lines.Add()

                        Dim iError As Integer = oRevalInv.Add()

                        If iError <> 0 Then
                            Dim strxml As String
                            strxml = oRevalInv.GetAsXML
 
                            Dim sError As String = String.Empty
                            DiApp.GetLastError(iError, sError)
                            res = False


                            '------------
                            Dim strFile As String = Application.StartupPath & "\ErrorLog.txt"                
                            Dim fileExists As Boolean = File.Exists(strFile)
                            Using sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))
                                sw.WriteLine( _
                                    IIf(fileExists, _
                                        "Error Message in  Occured at-- " & DateTime.Now, _
                                        "Start Error Log for today"))
                                sw.WriteLine(strxml)
                            End Using
                            '---------------
                            Throw New Exception("[CD] Problema al crear documento de revalúo: " + sError)
                        Else
                            DiApp.GetNewObjectCode(transID)
                            res = True
                        End If
                    Else
                        res = True
                    End If


                Next

                If Not IsNothing(oRevalInv) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(oRevalInv)
                    oRevalInv = Nothing
                    oMc = Nothing
                End If
                Return res


            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Shared Function GetAbsEntry(sSerie As String, sItem As String) As String
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlReserva.GetAbsEntry_TEXT(sSerie, sItem)
                Else
                    ostr = hanaReserva.GetAbsEntry_TEXT(sSerie, sItem)
                End If
                Return conexi.obtenerValor(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetAbsEntry_BoxPatent(DocEntry As String) As DataTable
            Try
                Dim ostr As String = String.Empty
                If Motor = "SQL" Then
                    ostr = sqlReserva.GetAbsEntry_BoxPatent_TEXT(DocEntry)
                Else
                    ostr = hanaReserva.GetAbsEntry_BoxPatent_TEXT(DocEntry)
                End If
                Return conexi.obtenerColeccion(ostr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CrearRevalorizacions(CrearUltimaLinea As Boolean, _
                                            CtaA As String, _
                                            CtaD As String, _
                                            ItemCode As String, _
                                            Whs As String, _
                                            Patent As String, _
                                            absEntry As String, _
                                            Monto As Decimal, _
                                            OcrCode1 As String, _
                                            OcrCode2 As String, _
                                            OcrCode3 As String, _
                                            OcrCode4 As String, _
                                            OcrCode5 As String, _
                                            FechaContabilizacion As String, _
                                            Glosa As String, _
                                            Moneda As String, _
                                            iSumdec As Integer, _
                                            ByRef oRevalInv As SAPbobsCOM.MaterialRevaluation, _
                                            ByRef transID As String
                                            ) As Boolean
            Try
                Dim res As Boolean = False
                Dim numero_linea As Integer = 0
                If CrearUltimaLinea = True Then
                    numero_linea = oRevalInv.Lines.Count

                    oRevalInv.SetCurrentLine(numero_linea)
                    Dim sMoneda As String = ""
                    Dim dmonto As Decimal

                    dmonto = Monto
                    sMoneda = Moneda

                    oRevalInv.Lines.ItemCode = ItemCode
                    oRevalInv.Lines.WarehouseCode = Whs
                    oRevalInv.Lines.RevaluationIncrementAccount = Trim(CtaA)
                    oRevalInv.Lines.RevaluationDecrementAccount = Trim(CtaD)

                    If OcrCode1 <> "" Then oRevalInv.Lines.DistributionRule = Trim(OcrCode1)
                    If OcrCode2 <> "" Then oRevalInv.Lines.DistributionRule2 = Trim(OcrCode2)
                    If OcrCode3 <> "" Then oRevalInv.Lines.DistributionRule3 = Trim(OcrCode3)
                    If OcrCode4 <> "" Then oRevalInv.Lines.DistributionRule4 = Trim(OcrCode4)
                    If OcrCode5 <> "" Then oRevalInv.Lines.DistributionRule5 = Trim(OcrCode5)
                    oRevalInv.Lines.UserFields.Fields.Item("U_BoxPatent").Value = Patent

                    Dim Srscount As Integer
                    Dim SNBService As SAPbobsCOM.MaterialRevaluationSNBService
                    Dim SNBParams As SAPbobsCOM.MaterialRevaluationSNBParams
                    Dim SNBParamCol As SAPbobsCOM.MaterialRevaluationSNBParamsCollection
                    Dim SNBParam As SAPbobsCOM.MaterialRevaluationSNBParam
                    Dim compService As SAPbobsCOM.CompanyService
                    Dim SNBLines As SAPbobsCOM.SNBLines

                    SNBLines = oRevalInv.Lines.SNBLines
                    compService = DiApp.GetCompanyService()
                    SNBService = compService.GetBusinessService(SAPbobsCOM.ServiceTypes.MaterialRevaluationSNBService)
                    SNBParams = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParams)
                    SNBParam = SNBService.GetDataInterface(SAPbobsCOM.MaterialRevaluationSNBServiceDataInterfaces.mrsnbsMaterialRevaluationSNBParam)
                    SNBParam.ItemCode = ItemCode
                    SNBParamCol = SNBService.GetList(SNBParam)
                    Srscount = SNBParamCol.Count

                    For i As Integer = 0 To SNBParamCol.Count - 1
                        SNBParams = SNBParamCol.Item(i)
                        'se valida que sea la serie que esta siendo usada y se llena la informacion a las lineas faltantes de serie
                        If absEntry = SNBParams.SnbAbsEntry Then
                            SNBLines.SnbAbsEntry = SNBParams.SnbAbsEntry
                            SNBLines.DebitCredit = dmonto
                            Exit For
                        End If
                    Next
                End If

                Dim iError As Integer = oRevalInv.Add()

                If iError <> 0 Then
                    'Dim strxml As String
                    'strxml = vJE.GetAsXML
                    Dim sError As String = String.Empty
                    DiApp.GetLastError(iError, sError)
                    res = False
                    Throw New Exception("[CD] Problema al crear asiento contable: " + sError)
                Else
                    DiApp.GetNewObjectCode(transID)
                    res = True
                End If
                If Not IsNothing(oRevalInv) Then
                    Dim oMc As New MemoryControl
                    oMc.releaseObject(oRevalInv)
                    oRevalInv = Nothing
                    oMc = Nothing
                End If
                Return res

            Catch ex As Exception

                Throw ex
            End Try
        End Function


        Public Shared Function GetDocEntryFromXML(sXML As String) As Integer
            Dim xDoc As XmlDocument = New XmlDocument()
            xDoc.LoadXml(sXML)
            Dim sDocEntry As String = xDoc.SelectSingleNode("DocumentParams/DocEntry").InnerText
            If sDocEntry <> "" Then
                Return Convert.ToInt32(sDocEntry)
            Else
                Return -1
            End If
        End Function

        Public Shared Function getAjusteEntrada(DocEntryBase As String, ItemCode As String, BoxPatent As String, _
                                                FechaEntrega As String) As Decimal
            Try
                Dim ostr As String
                Dim sValor As String
                Dim valor As Decimal

                If Motor = "SQL" Then
                    ostr = sqlReserva.getAjusteEntrada_TEXT(DocEntryBase, ItemCode, BoxPatent, FechaEntrega)
                Else
                    ostr = hanaReserva.getAjusteEntrada_TEXT(DocEntryBase, ItemCode, BoxPatent, FechaEntrega)
                End If
                sValor = conexi.obtenerValor(ostr)
                If Not sValor Is Nothing Then
                    valor = BDLineaCredito.DBDS_OUT(sValor)
                Else
                    valor = 0

                End If
                Return valor
            Catch ex As Exception
                Throw ex
            End Try
        End Function



    End Class
End Namespace



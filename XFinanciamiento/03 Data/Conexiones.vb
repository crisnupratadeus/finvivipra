﻿
Imports System.Transactions
Imports System.Data.Odbc
Imports System.Data.SqlClient

Namespace Data

    Public Class Conexiones

        Public CnnMSSQL As SqlConnection
        Public CnnHANA As OdbcConnection

        Dim sCadenaOdbc As String = ""

        Dim objCompany As SAPbobsCOM.Company = Nothing
        Dim objApplication As SAPbouiCOM.Application = Nothing
        Dim strConnectionString As String
        Dim intConnectionType As Integer ' 1 = SinglesignOn 2 = di-api
        Dim blConnected As Boolean = False

        Class TransactionUtils
            Public Shared Function CreateTransactionScope() As TransactionScope
                Dim TOP = New TransactionOptions()
                TOP.IsolationLevel = IsolationLevel.ReadCommitted
                TOP.Timeout = TimeSpan.Zero
                Return New TransactionScope(System.Transactions.TransactionScopeOption.Required, TOP, EnterpriseServicesInteropOption.Full)
            End Function
        End Class

        Public Sub New()

        End Sub


        Public ReadOnly Property isConnected() As Boolean
            Get
                Return blConnected
            End Get
        End Property
        Public Property Application() As SAPbouiCOM.Application
            Get
                Return objApplication
            End Get
            Set(ByVal Value As SAPbouiCOM.Application)
                objApplication = Value
            End Set
        End Property
        Public Property Company() As SAPbobsCOM.Company
            Get
                Return objCompany
            End Get
            Set(ByVal Value As SAPbobsCOM.Company)
                objCompany = Value
            End Set
        End Property


        ''' <summary>
        ''' CONEXION A LA DI API
        ''' </summary>
        Public Function ConectaDI() As Long
            Try
                Dim strCook As String, strCnn As String
                Dim Valor As Long

                strCook = DiApp.GetContextCookie
                strCnn = AppSap.Company.GetConnectionContext(strCook)
                Valor = DiApp.SetSboLoginContext(strCnn)
                Valor = DiApp.Connect
                ConectaDI = Valor

            Catch ex As Exception
                Company.MessageBox("Conecta DI-API error:" & ex.Message)
            End Try

        End Function

#Region "Funciones de conexión a SQL SERVER"

        Public Function ConectaCnnMSSQL(Optional ByRef mensaje As String = "") As Boolean
            Dim ER As Integer
            Try
                Dim SQLversion As String
                If DiApp.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008 Then
                    SQLversion = "SQL Server Native Client 10.0"
                Else
                    SQLversion = "SQL Server Native Client 11.0"
                End If
                CONEXION = New OdbcConnection("Driver={" & SQLversion & "}; Server= " & SQLServidor & "; database=" & SQLBaseDatos & "; Uid=" & sUsuMSSQL & "; Pwd=" & spasMSSQL)
                If CONEXION.State = ConnectionState.Closed Then
                    CONEXION.Open()
                End If
                If CONEXION.State = ConnectionState.Open Then
                    CONEXION.Close()
                End If
                Motor = "SQL"
                xClassDB = New RClass : BICLASS = xClassDB.RetornaClase()
                Return True

            Catch ex As Exception

                mensaje = "CONEXION : " & ex.Message
                MsgBox(mensaje)
                Return False
            End Try

        End Function

        Public Function ConectaCnnHANA(Optional ByRef mensaje As String = "") As Boolean
            Dim ER As Integer
            Try

                If SQLBaseDatos = "SBO-COMMON" Then
                    SQLBaseDatos = "SBOCOMMON"
                End If

                If Environment.Is64BitProcess Then
                    CONEXION = New OdbcConnection("Driver={HDBODBC}; ServerNode= " & SQLServidor & "; Database=" & SQLBaseDatos & "; Uid=" & sUsuHANA & "; Pwd=" & spasHANA)
                Else
                    CONEXION = New OdbcConnection("Driver={HDBODBC32}; ServerNode= " & SQLServidor & "; Database=" & SQLBaseDatos & "; Uid=" & sUsuHANA & "; Pwd=" & spasHANA)
                End If

                CONEXION.Open()
                CONEXION.Close()
                Motor = "HANA"

                xClassDB = New RClass : BICLASS = xClassDB.RetornaClase()
                Return True

            Catch ex As Exception

                mensaje = "CONEXION : " & ex.Message
                Return False
            End Try

        End Function

        Public Function DesconectaADONET(Optional ByRef mensaje As String = "") As Boolean

            Try

                If CONEXION.State = ConnectionState.Open Then
                    CONEXION.Close()
                End If
                Return True

            Catch ex As Exception
                mensaje = "DESCONEXION SQL SERVER: " & ex.Message
                Return False
            End Try

        End Function


        Public Function obtenerColeccion(ByVal Consulta As String, Optional ByVal KeepOpen As Boolean = False, _
                                         Optional ByRef mensaje As String = "") As DataTable

            Dim DTT As New DataTable
            Dim cmdLC As New System.Data.Odbc.OdbcCommand

            Try

                If CONEXION.State = ConnectionState.Closed Then
                    CONEXION.Open()
                End If

                Dim DapTable As New OdbcDataAdapter(Consulta, CONEXION)

                DapTable.SelectCommand.CommandTimeout = 0
                DapTable.Fill(DTT)

                If Not KeepOpen Then
                    If CONEXION.State = ConnectionState.Open Then
                        CONEXION.Close()
                    End If
                End If
                Return DTT

            Catch ex As System.Data.Odbc.OdbcException

                REM Operación no exitosa
                mensaje = "SQL SERVER: (" & ex.ErrorCode & ") " & ex.Message
                Return Nothing

            End Try

        End Function

        REM Esta función obtiene un registro, dada una consulta SQL.
        Public Function obtenerValor(ByVal Consulta As String, Optional ByVal KeepOpen As Boolean = False, _
                                     Optional ByRef mensaje As String = "") As String

            Try

                If CONEXION.State = ConnectionState.Closed Then
                    CONEXION.Open()
                End If

                Dim Comando As New OdbcCommand(Consulta, CONEXION)
                Comando.CommandTimeout = 0
                Comando.CommandText = Consulta

                Dim Valor As String = String.Empty

                Try
                    Valor = Comando.ExecuteScalar
                Catch ex As Exception
                    Valor = String.Empty
                    'Throw ex
                End Try

                Return Valor

            Catch ex As System.Data.Odbc.OdbcException

                mensaje = "SQL SERVER: (" & ex.ErrorCode & ") " & ex.Message
                MsgBox(mensaje)
                'Throw ex
                Return (False)

            End Try

        End Function

        REM Esta función inserta o modifica registros, dada una consulta SQL.
        Public Function creaRegistro(ByVal Consulta As String, Optional ByVal KeepOpen As Boolean = False, _
                                     Optional ByRef mensaje As String = "") As Boolean

            Try

                Try
                    If CONEXION.State = ConnectionState.Closed Then
                        CONEXION.Open()
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Return False
                End Try


                Dim Comando As New OdbcCommand(Consulta, CONEXION)
                Comando.CommandTimeout = 1000
                Comando.CommandText = Consulta

                Try
                    Comando.ExecuteNonQuery()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Return False
                    Exit Function
                End Try


                If Not KeepOpen Then
                    If CONEXION.State = ConnectionState.Open Then
                        CONEXION.Close()
                    End If
                End If

                REM Retornar el valor verdadero (operación exitosa).
                Return True

            Catch ex As System.Data.Odbc.OdbcException

                REM Operación no exitosa
                mensaje = "SQL : (" & ex.ErrorCode & ") " & ex.Message
                Return (False)

            End Try

        End Function

#End Region

    End Class

End Namespace


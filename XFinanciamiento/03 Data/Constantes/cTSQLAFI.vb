﻿
Namespace Data.Constantes

    Public Class cTSQLAFI
        Public Const TEXT_SEL_AFI_SELECT_USUCONECT As String = " " + vbCrLf +
                                                             "      CREATE TABLE tbl_usuarios_conectados                       " + vbCrLf +
                                                             "      (                                                          " + vbCrLf +
                                                             "       spid smallint,                                            " + vbCrLf +
                                                             "       ecid smallint,                                            " + vbCrLf +
                                                             "       [status] nchar(30),                                       " + vbCrLf +
                                                             "       loginname nchar(128),                                     " + vbCrLf +
                                                             "       hostname nchar(128),                                      " + vbCrLf +
                                                             "       blk char(5),                                              " + vbCrLf +
                                                             "       dbname nchar(128),                                        " + vbCrLf +
                                                             "       cmd nchar(16),                                            " + vbCrLf +
                                                             "       request_id nchar(16)                                      " + vbCrLf +
                                                             "      )                                                          " + vbCrLf +
                                                             "                                                                 " + vbCrLf +
                                                             "              INSERT tbl_usuarios_conectados                     " + vbCrLf +
                                                             "              EXEC sp_who                                        " + vbCrLf +
                                                             "                                                                 " + vbCrLf +
                                                             "      Select DISTINCT                                            " + vbCrLf +
                                                             "              loginname,                                         " + vbCrLf +
                                                             "              hostname                                           " + vbCrLf +
                                                             "      FROM  tbl_usuarios_conectados                              " + vbCrLf +
                                                             "      WHERE dbname = 'Test_FGC' --and cmd <> 'AWAITING COMMAND'  " + vbCrLf +
                                                             "                                                                 " + vbCrLf +
                                                             "      DROP TABLE tbl_usuarios_conectados     "
        ''' <summary>
        ''' VERIFICA_EXISTS_PROCEDURE HANA
        ''' </summary>
        ''' <remarks></remarks>
        Public Const NAME_VERIFICA_EXISTS_PROCEDURE_HANA As String = "VERIFICA_EXISTS_PROCEDURE"
        Public Shared TEXT_VERIFICA_EXISTS_PROCEDURE_HANA As String = " " + vbCrLf +
                                                                    "CREATE PROCEDURE """ & SQLBaseDatos & """.""VERIFICA_EXISTS_PROCEDURE"" ( IN procedurename NVARCHAR(256),	IN schemaname nvarchar(256) )" + vbCrLf +
                                                                    " LANGUAGE SQLSCRIPT AS                   " + vbCrLf +
                                                                    " myrowid integer;                        " + vbCrLf +
                                                                    "                                         " + vbCrLf +
                                                                    "BEGIN myrowid := 0 ;                     " + vbCrLf +
                                                                    "select                                   " + vbCrLf +
                                                                    "	*                                     " + vbCrLf +
                                                                    "from ""SYS"".""PROCEDURES""              " + vbCrLf +
                                                                    "where ""SCHEMA_NAME"" =:schemaname       " + vbCrLf +
                                                                    "and ""PROCEDURE_NAME""=:procedurename;   " + vbCrLf +
                                                                    "End;                                     " + vbCrLf

        Public Const NAME_CRUD_CARDEALER_MODVEHC As String = "SBO_SP_CARDEALER_MODVEHC"
        Public Shared TEXT_CRUD_CARDEALER_MODVEHC As String = " " + vbCrLf +
        "CREATE PROCEDURE SBO_SP_CARDEALER_MODVEHC" + vbCrLf +
        "@UULL AS NVARCHAR(1)," + vbCrLf +
        "@MONEDA AS NVARCHAR(5)," + vbCrLf +
        "@PRECIOLISTA AS NUMERIC(19,6)," + vbCrLf +
        "@FIJOP AS NUMERIC(19,6)," + vbCrLf +
        "@BONO AS NUMERIC(19,6)," + vbCrLf +
        "@FEEP AS NUMERIC(19,6)," + vbCrLf +
        "@PRECIOTRANSF AS NUMERIC(19,6)," + vbCrLf +
        "@DESCMAX AS NUMERIC(19,6)," + vbCrLf +
        "@DESCMAXP AS NUMERIC(19,6)," + vbCrLf +
        "@PRECIOMIN AS NUMERIC(19,6)," + vbCrLf +
        "@MARGENP AS NUMERIC(19,6)," + vbCrLf +
        "@MARGENV AS NUMERIC(19,6)," + vbCrLf +
        "@DOCCOD as NVARCHAR(50)" + vbCrLf +
        "AS " + vbCrLf +
        "BEGIN" + vbCrLf +
        "UPDATE [@EXX_VEHC]" + vbCrLf +
        "SET " + vbCrLf +
        "U_UULL = @UULL," + vbCrLf +
        "U_Currency = @MONEDA," + vbCrLf +
        "U_LstPrice = @PRECIOLISTA," + vbCrLf +
        "U_FixedP = @FIJOP," + vbCrLf +
        "U_Bonus = @BONO," + vbCrLf +
        "U_FeeP = @FEEP," + vbCrLf +
        "U_TrfPrice = @PRECIOTRANSF," + vbCrLf +
        "U_DMACV = @DESCMAX," + vbCrLf +
        "U_DMACP = @DESCMAXP," + vbCrLf +
        "U_SlsMinP = @PRECIOMIN," + vbCrLf +
        "U_MarginP = @MARGENP," + vbCrLf +
        "U_MarginV = @MARGENV" + vbCrLf +
        "WHERE Object = 'VEHC'" + vbCrLf +
        "AND Code = @DOCCOD" + vbCrLf +
        "END"

        Public Const NAME_CRUD_CARDEALER_INTITM As String = "EXX_INT_ITEMS"
        Public Shared TEXT_CRUD_CARDEALER_INTITM As String = " " + vbCrLf +
       "CREATE PROCEDURE [dbo].[EXX_INT_ITEMS](@QRYGRPORI NVARCHAR(50)," + vbCrLf +
                                       "@QRYGRPDES NVARCHAR(50)," + vbCrLf +
                                       "@SERVORI   NVARCHAR(150)," + vbCrLf +
                                       "@DBORI     NVARCHAR(150)," + vbCrLf +
                                       "@SERVDES   NVARCHAR(150)," + vbCrLf +
                                       "@DBDES     NVARCHAR(150))" + vbCrLf +
"AS" + vbCrLf +
     "BEGIN" + vbCrLf +
         "DECLARE @QUERYORI AS NVARCHAR(MAX);" + vbCrLf +
         "DECLARE @QUERYDES AS NVARCHAR(MAX);" + vbCrLf +
         "SET @QUERYORI = 'SELECT A.ItemCode,A.ItemName,A.FrgnName,A.ItmsGrpCod,B.ItmsGrpNam,A.FirmCode,C.FirmName,A.U_Family,A.U_EmiNorm,A.U_PassNbr,A.U_AxisNbr,A.U_DoorNbr,A.U_CyliNbr,A.U_Weight,A.U_Cylinder,A.U_KWPower,A.U_Category,A.U_MotBrd,A.U_Power,A.U_Transm,A.U_Tract,A.U_Cockpit,A.U_Fuel,A.U_KMWarr,A.U_YearWarr,A.U_Roof,A.U_LoadC,A.U_AxisDis,A.U_TEKCode,A.U_TecRepCode,A.PrchseItem,A.SellItem,A.InvntItem,A.ManSerNum,A.SalUnitMsr,A.NumInSale,A.BuyUnitMsr,A.NumInBuy,A.GLMethod,A.ByWh,A.ItemType,A.InvntryUom,A.EvalSystem FROM ['+@SERVORI+'].['+@DBORI+'].[dbo].[OITM] A INNER JOIN ['+@SERVORI+'].['+@DBORI+'].[dbo].[OITB] B ON A.ItmsGrpCod = B.ItmsGrpCod INNER JOIN ['+@SERVORI+'].['+@DBORI+'].[dbo].[OMRC] C ON A.FirmCode = C.FirmCode WHERE '+@QRYGRPORI+'= ''Y''';" + vbCrLf +
         "SET @QUERYDES = 'SELECT A.ItemCode,A.ItemName,A.FrgnName,A.ItmsGrpCod,B.ItmsGrpNam,A.FirmCode,C.FirmName,A.U_Family,A.U_EmiNorm,A.U_PassNbr,A.U_AxisNbr,A.U_DoorNbr,A.U_CyliNbr,A.U_Weight,A.U_Cylinder,A.U_KWPower,A.U_Category,A.U_MotBrd,A.U_Power,A.U_Transm,A.U_Tract,A.U_Cockpit,A.U_Fuel,A.U_KMWarr,A.U_YearWarr,A.U_Roof,A.U_LoadC,A.U_AxisDis,A.U_TEKCode,A.U_TecRepCode,A.PrchseItem,A.SellItem,A.InvntItem,A.ManSerNum,A.SalUnitMsr,A.NumInSale,A.BuyUnitMsr,A.NumInBuy,A.GLMethod,A.ByWh,A.ItemType,A.InvntryUom,A.EvalSystem FROM ['+@SERVDES+'].['+@DBDES+'].[dbo].[OITM] A INNER JOIN ['+@SERVDES+'].['+@DBDES+'].[dbo].[OITB] B ON A.ItmsGrpCod = B.ItmsGrpCod INNER JOIN ['+@SERVDES+'].['+@DBDES+'].[dbo].[OMRC] C ON A.FirmCode = C.FirmCode WHERE '+@QRYGRPDES+'= ''Y''';" + vbCrLf +
         "IF OBJECT_ID('tempdb..#EXX_ITEMSORI') IS NOT NULL" + vbCrLf +
             "DROP TABLE #EXX_ITEMSORI;" + vbCrLf +
         "CREATE TABLE #EXX_ITEMSORI" + vbCrLf +
         "(ItemCode     NVARCHAR(100)," + vbCrLf +
          "ItemName     NVARCHAR(100)," + vbCrLf +
          "FrgnName     NVARCHAR(100)," + vbCrLf +
          "ItmsGrpCod   NVARCHAR(100)," + vbCrLf +
          "ItmsGrpNam   NVARCHAR(100)," + vbCrLf +
          "FirmCode     NVARCHAR(100)," + vbCrLf +
          "FirmName     NVARCHAR(100)," + vbCrLf +
          "U_Family     NVARCHAR(100)," + vbCrLf +
          "U_EmiNorm    NVARCHAR(100)," + vbCrLf +
          "U_PassNbr    NUMERIC(19,6)," + vbCrLf +
          "U_AxisNbr    NUMERIC(19,6)," + vbCrLf +
          "U_DoorNbr    NUMERIC(19,6)," + vbCrLf +
          "U_CyliNbr    NUMERIC(19,6)," + vbCrLf +
          "U_Weight     NUMERIC(19,6)," + vbCrLf +
          "U_Cylinder   NUMERIC(19,6)," + vbCrLf +
          "U_KWPower    NUMERIC(19,6)," + vbCrLf +
          "U_Category   NVARCHAR(100)," + vbCrLf +
          "U_MotBrd     NVARCHAR(100)," + vbCrLf +
          "U_Power      NVARCHAR(100)," + vbCrLf +
          "U_Transm     NVARCHAR(100)," + vbCrLf +
          "U_Tract      NVARCHAR(100)," + vbCrLf +
          "U_Cockpit    NVARCHAR(100)," + vbCrLf +
          "U_Fuel       NVARCHAR(100)," + vbCrLf +
          "U_KMWarr     NUMERIC(19,6)," + vbCrLf +
          "U_YearWarr   NVARCHAR(100)," + vbCrLf +
          "U_Roof       NVARCHAR(100)," + vbCrLf +
          "U_LoadC      NVARCHAR(100)," + vbCrLf +
          "U_AxisDis    NVARCHAR(100)," + vbCrLf +
          "U_TEKCode    NVARCHAR(100)," + vbCrLf +
          "U_TecRepCode NVARCHAR(100)," + vbCrLf +
          "PrchseItem   NVARCHAR(100)," + vbCrLf +
          "SellItem     NVARCHAR(100)," + vbCrLf +
          "InvntItem    NVARCHAR(100)," + vbCrLf +
          "ManSerNum    NVARCHAR(100)," + vbCrLf +
          "SalUnitMsr   NVARCHAR(100)," + vbCrLf +
          "NumInSale    NUMERIC(19,6)," + vbCrLf +
          "BuyUnitMsr   NVARCHAR(100)," + vbCrLf +
          "NumInBuy     NUMERIC(19,6)," + vbCrLf +
          "GLMethod     NVARCHAR(100)," + vbCrLf +
          "ByWh         NVARCHAR(100)," + vbCrLf +
          "ItemType     NVARCHAR(100)," + vbCrLf +
          "InvntryUom   NVARCHAR(100)," + vbCrLf +
          "EvalSystem   NVARCHAR(100)" + vbCrLf +
         ");" + vbCrLf +
         "IF OBJECT_ID('tempdb..#EXX_ITEMSDES') IS NOT NULL" + vbCrLf +
             "DROP TABLE #EXX_ITEMSDES;" + vbCrLf +
         "CREATE TABLE #EXX_ITEMSDES" + vbCrLf +
         "(ItemCode     NVARCHAR(100)," + vbCrLf +
          "ItemName     NVARCHAR(150)," + vbCrLf +
          "FrgnName     NVARCHAR(150)," + vbCrLf +
          "ItmsGrpCod   NVARCHAR(100)," + vbCrLf +
          "ItmsGrpNam   NVARCHAR(100)," + vbCrLf +
          "FirmCode     NVARCHAR(100)," + vbCrLf +
          "FirmName     NVARCHAR(100)," + vbCrLf +
          "U_Family     NVARCHAR(100)," + vbCrLf +
          "U_EmiNorm    NVARCHAR(100)," + vbCrLf +
          "U_PassNbr    NUMERIC(19,6)," + vbCrLf +
          "U_AxisNbr    NUMERIC(19,6)," + vbCrLf +
          "U_DoorNbr    NUMERIC(19,6)," + vbCrLf +
          "U_CyliNbr    NUMERIC(19,6)," + vbCrLf +
          "U_Weight     NUMERIC(19,6)," + vbCrLf +
          "U_Cylinder   NUMERIC(19,6)," + vbCrLf +
          "U_KWPower    NUMERIC(19,6)," + vbCrLf +
          "U_Category   NVARCHAR(100)," + vbCrLf +
          "U_MotBrd     NVARCHAR(100)," + vbCrLf +
          "U_Power      NVARCHAR(100)," + vbCrLf +
          "U_Transm     NVARCHAR(100)," + vbCrLf +
          "U_Tract      NVARCHAR(100)," + vbCrLf +
          "U_Cockpit    NVARCHAR(100)," + vbCrLf +
          "U_Fuel       NVARCHAR(100)," + vbCrLf +
          "U_KMWarr     NUMERIC(19,6)," + vbCrLf +
          "U_YearWarr   NVARCHAR(100)," + vbCrLf +
          "U_Roof       NVARCHAR(100)," + vbCrLf +
          "U_LoadC      NVARCHAR(100)," + vbCrLf +
          "U_AxisDis    NVARCHAR(100)," + vbCrLf +
          "U_TEKCode    NVARCHAR(100)," + vbCrLf +
          "U_TecRepCode NVARCHAR(100)," + vbCrLf +
          "PrchseItem   NVARCHAR(100)," + vbCrLf +
          "SellItem     NVARCHAR(100)," + vbCrLf +
          "InvntItem    NVARCHAR(100)," + vbCrLf +
          "ManSerNum    NVARCHAR(100)," + vbCrLf +
          "SalUnitMsr   NVARCHAR(100)," + vbCrLf +
          "NumInSale    NUMERIC(19,6)," + vbCrLf +
          "BuyUnitMsr   NVARCHAR(100)," + vbCrLf +
          "NumInBuy     NUMERIC(19,6)," + vbCrLf +
          "GLMethod     NVARCHAR(100)," + vbCrLf +
          "ByWh         NVARCHAR(100)," + vbCrLf +
          "ItemType     NVARCHAR(100)," + vbCrLf +
          "InvntryUom   NVARCHAR(100)," + vbCrLf +
          "EvalSystem   NVARCHAR(100)" + vbCrLf +
         ");" + vbCrLf +
         "INSERT INTO #EXX_ITEMSORI" + vbCrLf +
         "EXEC (@QUERYORI);" + vbCrLf +
         "INSERT INTO #EXX_ITEMSDES" + vbCrLf +
         "EXEC (@QUERYDES);" + vbCrLf +
         "INSERT INTO [EXX_INT].[dbo].[EXX_INTITM]" + vbCrLf +
                "SELECT A.*," + vbCrLf +
                       "@SERVORI," + vbCrLf +
                       "@DBORI," + vbCrLf +
                       "@SERVDES," + vbCrLf +
                       "@DBDES," + vbCrLf +
        "'N'" + vbCrLf +
                "FROM #EXX_ITEMSORI A" + vbCrLf +
                     "LEFT JOIN #EXX_ITEMSDES B ON A.ItemCode = B.ItemCode" + vbCrLf +
                     "LEFT JOIN [EXX_INT].[dbo].[EXX_INTITM] C ON(A.ItemCode = C.ItemCode" + vbCrLf +
                                                                 "AND C.SrvOri = @SERVORI" + vbCrLf +
                                                                 "AND C.DBOri = @DBORI" + vbCrLf +
                                                                 "AND C.DBDest = @DBDES)" + vbCrLf +
                "WHERE(B.ItemCode Is NULL)" + vbCrLf +
                     "AND (C.ItemCode IS NULL);" + vbCrLf +
     "END;"


        Public Const NAME_CRUD_CARDEALER_INTRELUPDT As String = "EXX_INT_REL_UPDT"
        Public Shared TEXT_CRUD_CARDEALER_INTRELUPDT As String = " " + vbCrLf +
        "CREATE PROCEDURE [dbo].[EXX_INT_REL_UPDT]" + vbCrLf +
"(@Code INT," + vbCrLf +
"@Tipo NVARCHAR(100)," + vbCrLf +
"@ServOri NVARCHAR(100)," + vbCrLf +
"@BDOri NVARCHAR(100)," + vbCrLf +
"@TblOri NVARCHAR(100)," + vbCrLf +
"@CampOri NVARCHAR(100)," + vbCrLf +
"@ServDes NVARCHAR(100)," + vbCrLf +
"@BDDes NVARCHAR(100)," + vbCrLf +
"@TblDes NVARCHAR(100)," + vbCrLf +
"@CampDes NVARCHAR(100)," + vbCrLf +
"@ValO1 NVARCHAR(100)," + vbCrLf +
"@ValO2 NVARCHAR(100)," + vbCrLf +
"@ValO3 NVARCHAR(100)," + vbCrLf +
"@ValD1 NVARCHAR(100)," + vbCrLf +
"@ValD2 NVARCHAR(100)," + vbCrLf +
"@ValD3 NVARCHAR(100)," + vbCrLf +
"@Deshab NVARCHAR(1)" + vbCrLf +
")" + vbCrLf +
"AS" + vbCrLf +
"BEGIN" + vbCrLf +
"UPDATE [EXX_INT].[dbo].[EXX_INTREL]" + vbCrLf +
"SET" + vbCrLf +
"U_Tipo = @Tipo ," + vbCrLf +
"U_ServOri = @ServOri ," + vbCrLf +
"U_BDOri = @BDOri ," + vbCrLf +
"U_TblOri = @TblOri ," + vbCrLf +
"U_CampOri = @CampOri ," + vbCrLf +
"U_ServDes = @ServDes ," + vbCrLf +
"U_BDDes = @BDDes ," + vbCrLf +
"U_TblDes = @TblDes ," + vbCrLf +
"U_CampDes = @CampDes ," + vbCrLf +
"U_ValO1 = @ValO1 ," + vbCrLf +
"U_ValO2 = @ValO2 ," + vbCrLf +
"U_ValO3 = @ValO3 ," + vbCrLf +
"U_ValD1 = @ValD1 ," + vbCrLf +
"U_ValD2 = @ValD2 ," + vbCrLf +
"U_ValD3 = @ValD3 ," + vbCrLf +
"U_Deshab = @Deshab " + vbCrLf +
"WHERE Code = @Code" + vbCrLf +
"END "

        Public Const NAME_CRUD_CARDEALER_INTRELCREA As String = "EXX_INT_REL_CREA"
        Public Shared TEXT_CRUD_CARDEALER_INTRELCREA As String = " " + vbCrLf +
        "CREATE PROCEDURE [dbo].[EXX_INT_REL_CREA]" + vbCrLf +
"(@Code NVARCHAR(100)," + vbCrLf +
"@Tipo NVARCHAR(100)," + vbCrLf +
"@ServOri NVARCHAR(100)," + vbCrLf +
"@BDOri NVARCHAR(100)," + vbCrLf +
"@TblOri NVARCHAR(100)," + vbCrLf +
"@CampOri NVARCHAR(100)," + vbCrLf +
"@ServDes NVARCHAR(100)," + vbCrLf +
"@BDDes NVARCHAR(100)," + vbCrLf +
"@TblDes NVARCHAR(100)," + vbCrLf +
"@CampDes NVARCHAR(100)," + vbCrLf +
"@ValO1 NVARCHAR(100)," + vbCrLf +
"@ValO2 NVARCHAR(100)," + vbCrLf +
"@ValO3 NVARCHAR(100)," + vbCrLf +
"@ValD1 NVARCHAR(100)," + vbCrLf +
"@ValD2 NVARCHAR(100)," + vbCrLf +
"@ValD3 NVARCHAR(100)," + vbCrLf +
"@Deshab NVARCHAR(1)" + vbCrLf +
")" + vbCrLf +
"AS" + vbCrLf +
"BEGIN" + vbCrLf +
"INSERT INTO [EXX_INT].[dbo].[EXX_INTREL]" + vbCrLf +
"VALUES" + vbCrLf +
"(@Code ," + vbCrLf +
"@Tipo ," + vbCrLf +
"@ServOri ," + vbCrLf +
"@BDOri ," + vbCrLf +
"@TblOri ," + vbCrLf +
"@CampOri ," + vbCrLf +
"@ServDes ," + vbCrLf +
"@BDDes ," + vbCrLf +
"@TblDes ," + vbCrLf +
"@CampDes ," + vbCrLf +
"@ValO1 ," + vbCrLf +
"@ValO2 ," + vbCrLf +
"@ValO3 ," + vbCrLf +
"@ValD1 ," + vbCrLf +
"@ValD2 ," + vbCrLf +
"@ValD3 ," + vbCrLf +
"@Deshab)" + vbCrLf +
"END "



        Public Const NAME_CRUD_CARDEALER_INTVEH As String = "EXX_INT_VEHC"
        Public Shared TEXT_CRUD_CARDEALER_INTVEH As String = " " + vbCrLf +
        "CREATE PROCEDURE [dbo].[EXX_INT_VEHC](@SERVORI   NVARCHAR(150)," + vbCrLf +
                                       "@DBORI     NVARCHAR(150)," + vbCrLf +
                                       "@SERVDES   NVARCHAR(150)," + vbCrLf +
                                       "@DBDES     NVARCHAR(150))" + vbCrLf +
"AS " + vbCrLf +
"BEGIN" + vbCrLf +
"DECLARE @QUERYORI AS NVARCHAR(MAX);" + vbCrLf +
"DECLARE @QUERYDES AS NVARCHAR(MAX);" + vbCrLf +
"SET @QUERYORI ='SELECT ISNULL(A.Code,''''),ISNULL(A.U_Brand,''''),ISNULL(B.FirmName,''''),ISNULL(A.U_FamCod,''''),ISNULL(A.U_FamDes,''''),ISNULL(A.U_Model,''''),ISNULL(A.U_SBOGrpC,''''),ISNULL(A.U_SBOGrp,''''),ISNULL(A.U_ComRef,''''),ISNULL(A.U_TecRef,''''),ISNULL(A.U_Condit,''''),ISNULL(A.U_Mileage,''''),ISNULL(A.U_OwnrCod,''''),ISNULL(A.U_OwnrNam,''''),ISNULL(A.U_UsrCod,''''),ISNULL(A.U_UsrNam,''''),ISNULL(A.U_Patent,''''),ISNULL(A.U_PatStat,''''),ISNULL(A.U_TEKCode,''''),ISNULL(A.U_TecRepCode,''''),ISNULL(A.U_EmiNorm,''''),ISNULL(A.U_PassNbr,0),ISNULL(A.U_AxisNbr,0),ISNULL(A.U_DoorNbr,0),ISNULL(A.U_CyliNbr,0),ISNULL(A.U_KWPower,0),ISNULL(A.U_Category,''''),ISNULL(A.U_Transm,''''),ISNULL(A.U_Tract,''''),ISNULL(A.U_Fuel,''''),ISNULL(A.U_KMWarr,0),ISNULL(A.U_YearWarr,''''),ISNULL(A.U_Roof,''''),ISNULL(A.U_ExtCol,''''),ISNULL(A.U_IntCol,''''),ISNULL(A.U_SaleSts,''''),ISNULL(A.U_VehSts,''''),ISNULL(A.U_Comments,''''),ISNULL(A.U_MotNmb,''''),ISNULL(A.U_VINCod,''''),ISNULL(A.U_YrModl,''''),ISNULL(A.U_Currency,''''),ISNULL(A.U_LstPrice,0),ISNULL(A.U_SlsTyp,'''') FROM ['+ @SERVORI +'].['+ @DBORI +'].[dbo].[@EXX_VEHC] A INNER JOIN ['+ @SERVORI +'].['+ @DBORI +'].[dbo].[OMRC] B ON A.U_Brand = B.FirmCode';" + vbCrLf +
"SET @QUERYDES = 'SELECT ISNULL(A.Code,''''),ISNULL(A.U_Brand,''''),ISNULL(B.FirmName,''''),ISNULL(A.U_FamCod,''''),ISNULL(A.U_FamDes,''''),ISNULL(A.U_Model,''''),ISNULL(A.U_SBOGrpC,''''),ISNULL(A.U_SBOGrp,''''),ISNULL(A.U_ComRef,''''),ISNULL(A.U_TecRef,''''),ISNULL(A.U_Condit,''''),ISNULL(A.U_Mileage,''''),ISNULL(A.U_OwnrCod,''''),ISNULL(A.U_OwnrNam,''''),ISNULL(A.U_UsrCod,''''),ISNULL(A.U_UsrNam,''''),ISNULL(A.U_Patent,''''),ISNULL(A.U_PatStat,''''),ISNULL(A.U_TEKCode,''''),ISNULL(A.U_TecRepCode,''''),ISNULL(A.U_EmiNorm,''''),ISNULL(A.U_PassNbr,0),ISNULL(A.U_AxisNbr,0),ISNULL(A.U_DoorNbr,0),ISNULL(A.U_CyliNbr,0),ISNULL(A.U_KWPower,0),ISNULL(A.U_Category,''''),ISNULL(A.U_Transm,''''),ISNULL(A.U_Tract,''''),ISNULL(A.U_Fuel,''''),ISNULL(A.U_KMWarr,0),ISNULL(A.U_YearWarr,''''),ISNULL(A.U_Roof,''''),ISNULL(A.U_ExtCol,''''),ISNULL(A.U_IntCol,''''),ISNULL(A.U_SaleSts,''''),ISNULL(A.U_VehSts,''''),ISNULL(A.U_Comments,''''),ISNULL(A.U_MotNmb,''''),ISNULL(A.U_VINCod,''''),ISNULL(A.U_YrModl,''''),ISNULL(A.U_Currency,''''),ISNULL(A.U_LstPrice,0),ISNULL(A.U_SlsTyp,'''') FROM ['+ @SERVDES +'].['+ @DBDES +'].[dbo].[@EXX_VEHC] A INNER JOIN ['+ @SERVDES +'].['+ @DBDES +'].[dbo].[OMRC] B ON A.U_Brand = B.FirmCode';" + vbCrLf +
"IF OBJECT_ID('tempdb..#EXX_VEHCORI') IS NOT NULL" + vbCrLf +
             "DROP TABLE #EXX_VEHCORI;" + vbCrLf +
         "CREATE TABLE #EXX_VEHCORI" + vbCrLf +
     "(Code NVARCHAR(100)," + vbCrLf +
     "U_Brand NVARCHAR(100)," + vbCrLf +
     "FirmName NVARCHAR(100)," + vbCrLf +
     "U_FamCod NVARCHAR(100)," + vbCrLf +
     "U_FamDes NVARCHAR(100)," + vbCrLf +
    "U_Model NVARCHAR(100)," + vbCrLf +
    "U_SBOGrpC NVARCHAR(100)," + vbCrLf +
    "U_SBOGrp NVARCHAR(100)," + vbCrLf +
    "U_ComRef NVARCHAR(100)," + vbCrLf +
    "U_TecRef NVARCHAR(100)," + vbCrLf +
    "U_Condit NVARCHAR(100)," + vbCrLf +
    "U_Mileage NUMERIC(19,6)," + vbCrLf +
    "U_OwnrCod NVARCHAR(100)," + vbCrLf +
    "U_OwnrNam NVARCHAR(100)," + vbCrLf +
    "U_UsrCod NVARCHAR(100)," + vbCrLf +
    "U_UsrNam NVARCHAR(100)," + vbCrLf +
    "U_Patent NVARCHAR(100)," + vbCrLf +
    "U_PatStat NVARCHAR(100)," + vbCrLf +
    "U_TEKCode NVARCHAR(100)," + vbCrLf +
    "U_TecRepCode NVARCHAR(100)," + vbCrLf +
    "U_EmiNorm NVARCHAR(100)," + vbCrLf +
    "U_PassNbr NUMERIC(19,6)," + vbCrLf +
    "U_AxisNbr NUMERIC(19,6)," + vbCrLf +
    "U_DoorNbr NUMERIC(19,6)," + vbCrLf +
    "U_CyliNbr NUMERIC(19,6)," + vbCrLf +
    "U_KWPower NUMERIC(19,6)," + vbCrLf +
    "U_Category NVARCHAR(100)," + vbCrLf +
    "U_Transm NVARCHAR(100)," + vbCrLf +
    "U_Tract NVARCHAR(100)," + vbCrLf +
    "U_Fuel NVARCHAR(100)," + vbCrLf +
    "U_KMWarr NUMERIC(19,6)," + vbCrLf +
    "U_YearWarr NVARCHAR(100)," + vbCrLf +
    "U_Roof NVARCHAR(100)," + vbCrLf +
    "U_ExtCol NVARCHAR(100)," + vbCrLf +
    "U_IntCol NVARCHAR(100)," + vbCrLf +
    "U_SaleSts NVARCHAR(100)," + vbCrLf +
    "U_VehSts NVARCHAR(100)," + vbCrLf +
    "U_Comments NVARCHAR(250)," + vbCrLf +
    "U_MotNmb NVARCHAR(100)," + vbCrLf +
    "U_VINCod NVARCHAR(100)," + vbCrLf +
    "U_YrModl NVARCHAR(100)," + vbCrLf +
    "U_Currency NVARCHAR(100)," + vbCrLf +
    "U_LstPrice NUMERIC(19,6)," + vbCrLf +
    "U_SlsTyp NVARCHAR(100)" + vbCrLf +
     ");" + vbCrLf +
"IF OBJECT_ID('tempdb..#EXX_VEHCDES') IS NOT NULL" + vbCrLf +
             "DROP TABLE #EXX_VEHCDES;" + vbCrLf +
         "CREATE TABLE #EXX_VEHCDES" + vbCrLf +
     "(Code NVARCHAR(100)," + vbCrLf +
     "U_Brand NVARCHAR(100)," + vbCrLf +
     "FirmName NVARCHAR(100)," + vbCrLf +
     "U_FamCod NVARCHAR(100)," + vbCrLf +
     "U_FamDes NVARCHAR(100)," + vbCrLf +
    "U_Model NVARCHAR(100)," + vbCrLf +
    "U_SBOGrpC NVARCHAR(100)," + vbCrLf +
    "U_SBOGrp NVARCHAR(100)," + vbCrLf +
    "U_ComRef NVARCHAR(100)," + vbCrLf +
    "U_TecRef NVARCHAR(100)," + vbCrLf +
    "U_Condit NVARCHAR(100)," + vbCrLf +
    "U_Mileage NUMERIC(19,6)," + vbCrLf +
    "U_OwnrCod NVARCHAR(100)," + vbCrLf +
    "U_OwnrNam NVARCHAR(100)," + vbCrLf +
    "U_UsrCod NVARCHAR(100)," + vbCrLf +
    "U_UsrNam NVARCHAR(100)," + vbCrLf +
    "U_Patent NVARCHAR(100)," + vbCrLf +
    "U_PatStat NVARCHAR(100)," + vbCrLf +
    "U_TEKCode NVARCHAR(100)," + vbCrLf +
    "U_TecRepCode NVARCHAR(100)," + vbCrLf +
    "U_EmiNorm NVARCHAR(100)," + vbCrLf +
    "U_PassNbr NUMERIC(19,6)," + vbCrLf +
    "U_AxisNbr NUMERIC(19,6)," + vbCrLf +
    "U_DoorNbr NUMERIC(19,6)," + vbCrLf +
    "U_CyliNbr NUMERIC(19,6)," + vbCrLf +
    "U_KWPower NUMERIC(19,6)," + vbCrLf +
    "U_Category NVARCHAR(100)," + vbCrLf +
    "U_Transm NVARCHAR(100)," + vbCrLf +
    "U_Tract NVARCHAR(100)," + vbCrLf +
    "U_Fuel NVARCHAR(100)," + vbCrLf +
    "U_KMWarr NUMERIC(19,6)," + vbCrLf +
    "U_YearWarr NVARCHAR(100)," + vbCrLf +
    "U_Roof NVARCHAR(100)," + vbCrLf +
    "U_ExtCol NVARCHAR(100)," + vbCrLf +
    "U_IntCol NVARCHAR(100)," + vbCrLf +
    "U_SaleSts NVARCHAR(100)," + vbCrLf +
    "U_VehSts NVARCHAR(100)," + vbCrLf +
    "U_Comments NVARCHAR(250)," + vbCrLf +
    "U_MotNmb NVARCHAR(100)," + vbCrLf +
    "U_VINCod NVARCHAR(100)," + vbCrLf +
    "U_YrModl NVARCHAR(100)," + vbCrLf +
    "U_Currency NVARCHAR(100)," + vbCrLf +
    "U_LstPrice NUMERIC(19,6)," + vbCrLf +
    "U_SlsTyp NVARCHAR(100)" + vbCrLf +
     ");" + vbCrLf +
"INSERT INTO #EXX_VEHCORI" + vbCrLf +
         "EXEC (@QUERYORI);" + vbCrLf +
"INSERT INTO #EXX_VEHCDES" + vbCrLf +
         "EXEC (@QUERYDES);" + vbCrLf +
"INSERT INTO [EXX_INT].[dbo].[EXX_INTVEH]" + vbCrLf +
                "SELECT A.*," + vbCrLf +
                       "@SERVORI," + vbCrLf +
                       "@DBORI," + vbCrLf +
                       "@SERVDES," + vbCrLf +
                       "@DBDES," + vbCrLf +
        "'N'" + vbCrLf +
                "FROM #EXX_VEHCORI A" + vbCrLf +
                     "LEFT JOIN #EXX_VEHCDES B ON A.Code = B.Code" + vbCrLf +
                     "LEFT JOIN [EXX_INT].[dbo].[EXX_INTVEH] C ON(A.Code = C.Code" + vbCrLf +
                                                                 "AND C.SrvOri = @SERVORI" + vbCrLf +
                                                                 "AND C.DBOri = @DBORI" + vbCrLf +
                                                                 "AND C.DBDest = @DBDES)" + vbCrLf +
                "WHERE(B.Code Is NULL)" + vbCrLf +
                     "AND (C.Code IS NULL);" + vbCrLf +
"END;"


    End Class

End Namespace

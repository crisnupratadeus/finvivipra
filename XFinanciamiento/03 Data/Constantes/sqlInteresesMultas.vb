﻿Public Class sqlInteresesMultas
    Public Const getBranchBD As String = "getBranchBD"


    Friend Shared Function insertaDetalleGarFIPLD1(U_Plantilla As String, U_Valor As String, U_CodeLineaCred As String, U_CodeDoc As String, U_TipoDoc As String) As String
    Dim sSqlInsert As String

        'sSqlInsert  = "INSERT INTO [dbo].[@EXX_FIPLD1] ([Code] ,[Name],[U_Codigo] ,[U_Name],[U_Plantilla],[U_Valor] ,[U_CodeLineaCred],[U_CodeDoc],[U_TipoDoc]) VALUES ('" & code & "'," & "'" & Name & "',"  & "'" & U_Codigo & "'," & "'" & U_Name & "',"  & "'" & U_Plantilla & "'," & "'" & U_Valor & "'," & "'" & U_CodeLineaCred & "'," & "'" & U_CodeDoc & "'," & "'" & U_TipoDoc & "')"

        sSqlInsert = "INSERT INTO [@EXX_FIPLD1]" + vbCrLf +
                     "(Code, Name, U_Codigo, U_Plantilla, U_Valor, U_CodeLineaCred, U_CodeDoc,	U_TipoDoc)" + vbCrLf +
                     "SELECT (RIGHT('00000000' + CONVERT(VARCHAR(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), (RIGHT('00000000' + convert(varchar(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), '" & U_Plantilla & "','" & U_CodeDoc & "', '" & U_Valor & "', '" & U_CodeLineaCred & "', '" & U_CodeDoc & "', '" & U_TipoDoc & "' FROM [@EXX_FIPLD1]"
        Return sSqlInsert

    End Function

    Friend Shared Function getInteresMulta(fechaPago As String, fechaVencimiento As String, tipo_Periodo As String, interesTasa As String, montoPendienteDePago As String, tasaAnualMoratoria As String, diasParaTasaAnualMoratoria As string) As String

        Dim oStr As String = "EXEC	[dbo].[sp_Calculo_Interes_Mora] " &
            "@FechaPago = '" & fechaPago & "'" &
            ", @FechaVencimiento = '" & fechaVencimiento & "'" &
            ", @Tipo_Periodo = '" & tipo_Periodo & "'" &
            ", @InteresTasa = " & interesTasa & "" &
            ", @MontoPendienteDePago = " & montoPendienteDePago & "" &
            ", @tasaAnualMoratoria= " & tasaAnualMoratoria & "" &
            ", @DiasParaTasaAnualMoratoria = " & diasParaTasaAnualMoratoria & ""
        Return oStr

    End Function

    Friend Shared Function getDatosFacturaParaPago(DocNum As String, CardCode As String) As String
        Dim sSqlFactura As String

        sSqlFactura = "SELECT A.DocEntry, A.DocNum, A.CardCode, A.U_OrdenVenta, A.DocStatus, A.U_DocCuota, A.U_CuotFi, A.U_Interes, A.U_IntMor, A.DocDueDate, A.DocTotal, A.PaidToDate, A.TaxDate, A.DocDate, B.U_DocEntryF " + vbCrLf +
                      "From OINV AS A LEFT OUTER JOIN  [@EXX_FACT1] AS B ON A.DocEntry = B.U_DocEntryF  Where A.DocNum = " & DocNum & " And A.CardCode = '" & CardCode & "'"
        Return sSqlFactura

    End Function

    Friend Shared Function getCalculo_pagov2(V_oV As String, V_Fecha_Ini As String, V_Fecha_UltPago As String, V_Fecha_pago As String, V_Fecha_valor As String, GastosA As String) As String

        Dim oStr As String = "EXEC	[dbo].[sp_Calculo_pago] " &
            "@DocEntryOV = '" & V_oV & "'" &
            ", @Fecha_Ini = '" & V_Fecha_Ini & "'" &
            ", @FechaUltPago = '" & V_Fecha_UltPago & "'" &
            ", @Fecha_pago = '" & V_Fecha_pago & "'" &
            ", @Fecha_valor = '" & V_Fecha_valor & "'" &
            ", @GastosA = " & GastosA & ""
        Return oStr

    End Function

    Friend Shared Function getUltimaFechaPagada(u_DocEntryF As String, u_CardCode As String) As String

        Dim sSqlFactura As String

        '        "Select Case MAX(it."U_FechaVa") As U_FechaVa
        'FROM            "P_LUIS_AGROTERRA"."@EXX_FACT1" AS fa INNER JOIN "P_LUIS_AGROTERRA"."@EXX_INTERESES" AS it ON fa."DocEntry" = it."DocEntry"
        'WHERE(fa."U_DocEntryF" = '538') AND (it."U_CardCode" = '09.149-200-0')

        sSqlFactura = "SELECT Max([@EXX_INTERESES].U_FechaVa) U_FechaVa " & 
                              "FROM [@EXX_FACT1] INNER JOIN [@EXX_INTERESES] ON [@EXX_FACT1].DocEntry = [@EXX_INTERESES].DocEntry " &
                              "WHERE ([@EXX_FACT1].U_DocEntryF = '" & u_DocEntryF & "' ) and [@EXX_INTERESES].U_CardCode = '" & u_CardCode & "'"
        Return sSqlFactura

    End Function
End Class

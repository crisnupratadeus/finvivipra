﻿Imports XFinanciamiento.Data

Public Class hanaData
    Public Shared conexi As Conexiones = New Conexiones
    Public Const GetParam_TEXT_NAME As String = "GetParam"
    Public Shared Function GetParam_TEXT(Name As String) As String
        Return _
     "select ""U_Value1"" from """ & SQLBaseDatos & """.""@EXX_FIPARAM""  where ""U_Type""='" & Name & "'"

    End Function
    Public Const sSqlCommandDatosDB1_NAME As String = "sSqlCommandDatosDB1"
    Public Shared Function sSqlCommandDatosDB1_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String
        Return _
        "DELETE from """ & SQLBaseDatos & """.""@EXX_FIPARAM""  WHERE ""U_Type"" = '" & Type & "' and ""U_Value1""='" & valor1 & "' and ""U_Value3""='" & valor2 & "'"
    End Function
    Public Const sSqlCommandDatosDB2_NAME As String = "sSqlCommandDatosDB2"
    Public Shared Function sSqlCommandDatosDB2_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String
        Return _
        "DELETE from """ & SQLBaseDatos & """.""@EXX_FIPARAM""  WHERE ""U_Type"" = '" & Type & "'"
    End Function
    Public Const SqlInsertDatosDB_NAME As String = "SqlInsertDatosDB"
    Public Shared Function SqlInsertDatosDB_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String

        Dim sSqlInsert As String = "INSERT INTO """ & SQLBaseDatos & """.""@EXX_FIPARAM""                                                                 " + vbCrLf +
                "             (                                                                                                    " + vbCrLf +
                "               ""Code"",                                                                                              " + vbCrLf +
                "  	            ""Name"",                                                                                              " + vbCrLf +
                "              ""U_Type"",                                                                                            " + vbCrLf +
                "              ""U_Value1"",                                                                                           " + vbCrLf +
                "              ""U_Value2"",                                                                                            " + vbCrLf +
                "              ""U_Active"",                                                                                           " + vbCrLf +
                "              ""U_UserName"",                                                                                           " + vbCrLf +
                "              ""U_CreateDate""                                                                                           " + vbCrLf +
                "             )                                                                                                    " + vbCrLf +
                "             SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8)  --> Code   " + vbCrLf +
                "             ,RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Name"")), 0) + 1), 8), --> Name        " + vbCrLf

        sSqlInsert = sSqlInsert & "'" & Type & "', '" & valor1 & "', '" & valor2 & "'," & "1, '" & DiApp.UserName & "', '" & DateToString(Today) & "' from """ & SQLBaseDatos & """.""@EXX_FIPARAM"" "

        Return sSqlInsert
    End Function
    Public Shared Function DateToString(dDate As Date, Optional withHour As Boolean = False) As String
        Try
            Dim strDate As String = dDate.Year & Right(CStr("0" & dDate.Month), 2) & Right(CStr("0" & dDate.Day), 2)
            If withHour = True Then
                strDate = " " & strDate & Right(CStr("0" & dDate.Hour), 2) & ":" & Right(CStr("0" & dDate.Minute), 2)
            End If

            Return strDate

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function GetAnexosLineaDeCredito(objeto As String, nroDoc As String) As String

        Dim query As String = String.Empty

        query = "select CAST(ROW_NUMBER() over (order by ""Code"" ASC)as int) as ""FilNum"",  ""U_Path"" as ""Ruta"",  ""U_AttDate"" as ""Fecha"", CAST(""Code"" as int) as ""Code"", CAST(""Name"" as int) as ""Name"", ""U_DocCode"" from """ & SQLBaseDatos & """.""@EXX_IATT""  where ""U_ObjCode"" = '" & objeto & "' and ""U_DocCode"" = '" & nroDoc & "'" 'mauricio

        Return query

    End Function

    Public Shared Function GetMaxCodeLinCred() As String
        Dim query As String = String.Empty

        query = "select IFNULL(Max(CAST(""Code"" as int)),0) as ""Code"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_IATT"""

        Return query

    End Function
    Public Shared Function GetParamOV_TEXT(DocEntryOV As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""DocTotal"", ""DocDate"", ""DocTotalFC"", ""DocTotalSy"", ""CurSource"" FROM """ & SQLBaseDatos.ToUpper & """.ORDR WHERE ""DocEntry"" = '" & DocEntryOV & "'" 'mauricio
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function InsertaCuotaInteres(DocEntryOV As String, FechaVencimiento As String, DocEntryPG As String, TotalCuota As String, CapitalxPagar As String, Amortizacion As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN""" + vbCrLf +
                            "(""Code"", ""Name"", ""U_DocEntryOV"", ""U_FechaV"", ""U_DocEntryPG"",""U_TotalCuota"", ""U_CapitalxPagar"", ""U_Amortizacion"")" + vbCrLf +
                            "SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code" + vbCrLf +
                            ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name" + vbCrLf +
                            ",'" & Trim(DocEntryOV) & "'" + vbCrLf +
                            ",TO_DATE('" & Trim(FechaVencimiento) & "', 'DD/MM/YYYY')" + vbCrLf +
                            ",'" & Trim(DocEntryPG) & "'" + vbCrLf +
                            ",'" & Trim(TotalCuota) & "'" + vbCrLf +
                            ",'" & Trim(CapitalxPagar) & "'" + vbCrLf +
			    ",'" & Trim(Amortizacion) & "'" + vbCrLf +
                            "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN""" 'mauricio

            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
                Exit Function
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function


    Public Shared Function ValidaCodigoPlantilla(Codigo As String) As String

        Dim query As String = String.Empty

        Try
            query = "SELECT COUNT(1) ""EXISTE"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLAN"" WHERE IFNULL(""U_Codigo"",'') = '" & Trim(Codigo) & " '"
            Return query
        Catch ex As Exception
            Throw ex
            Return "-1"
        End Try
    End Function

    'AGREGADO POR MICK=====================================================================================================
    ' EJEMPLO===============================================================================================================

    Public Const SBO_SP_CON_SUPECALC_HANA_NAME As String = "SBO_SP_CON_SUPECALC_HANA"
    Public Shared Function SBO_SP_CON_SUPECALC_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SBO_SP_CON_SUPECALC_HANA"" (" & vbCrLf &
"IN PresEntry NVARCHAR(50)," & vbCrLf &
"IN Correlativo NVARCHAR(50)," & vbCrLf &
"IN CodAvance NVARCHAR(50)," & vbCrLf &
"OUT SUPE """ & SQLBaseDatos.ToUpper & """.""SUPECALC"")" & vbCrLf &
"SQL SECURITY INVOKER" & vbCrLf &
"AS" & vbCrLf &
        "BEGIN" & vbCrLf &
"SUPE = SELECT ""A"".""U_PartidaIdPadre"" AS ""SUPEDITADA""," & vbCrLf &
"IFNULL(""B"".""U_UControl"",'') AS ""UNIDAD""," & vbCrLf &
"SUM((IFNULL(""B"".""U_Porcentaje"",0)*(IFNULL(""A"".""U_Porcentaje"",0)/100))) AS ""VALOR""" & vbCrLf &
"FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_PRE5"" ""A""" & vbCrLf &
"INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AVN1"" ""B"" ON(""A"".""U_PartidaIdHijo"" = ""B"".""U_Partida""" & vbCrLf &
"AND ""A"".""U_DocEntry"" = ""B"".""U_PresEntry"")" & vbCrLf &
"WHERE ""B"".""U_PresEntry"" = PresEntry" & vbCrLf &
"AND ""B"".""U_Correlativo"" = Correlativo" & vbCrLf &
"AND ""B"".""U_CodAvnc"" = CodAvance" & vbCrLf &
"GROUP BY ""A"".""U_PartidaIdPadre""," & vbCrLf &
"""B"".""U_UControl"";" & vbCrLf &
"END;"

        Return res
    End Function
    ' FIN DE EJEMPLO=============================================================================================
    'SE DEFINEN LOS NOMBRE DE LAS FUNCIONES Y SP'S

    Public Const FN_FECHAS_CUOTAS_HANA_NAME As String = "FN_FECHAS_CUOTAS_HANA"
    Public Shared Function FN_FECHAS_CUOTAS_HANA_TEXT() As String
        Dim res As String
        res = "CREATE FUNCTION """ & SQLBaseDatos.ToUpper & """.""FN_FECHAS_CUOTAS_HANA""(" & vbCrLf &
"Cuotas smallint," & vbCrLf &
"Lapso smallint, " & vbCrLf &
"Fecha_Ini DATE " & vbCrLf &
")" & vbCrLf &
"returns table (" & vbCrLf &
"""NRO"" smallint," & vbCrLf &
"""FECHA_PAGO"" Date, " & vbCrLf &
"""DIAS"" smallint" & vbCrLf &
")" & vbCrLf &
"LANGUAGE SQLSCRIPT" & vbCrLf &
"Sql Security INVOKER AS" & vbCrLf &
"BEGIN" & vbCrLf &
"If Mod (lapso, 30) = 0 Then" & vbCrLf &
"RETURN" & vbCrLf &
"SELECT 	DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 As ""NRO"" , " & vbCrLf &
"add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ) as ""FECHA_PAGO"", " & vbCrLf &
"DAYS_BETWEEN ( " & vbCrLf &
"add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") ) * (lapso/ 30) ) )" & vbCrLf &
", add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ))   as ""DIAS"" " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas - 1) ;" & vbCrLf &
"else" & vbCrLf &
"RETURN" & vbCrLf &
"SELECT DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 as ""NRO"" , add_days(Fecha_Ini , lapso * (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) ) as ""FECHA_PAGO"", lapso as ""DIAS""" & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION" & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini and add_days(Fecha_Ini , Cuotas -1) ;" & vbCrLf &
"End If;" & vbCrLf &
"END;"
        Return res
    End Function


    Public Const SP_CALCULO_INTERES_HANA_NAME As String = "SP_CALCULO_INTERES_HANA"
    Public Shared Function SP_CALCULO_INTERES_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" ( " & vbCrLf &
"in Saldo        decimal(13,2), " & vbCrLf &
"In Fecha_desde  Date, " & vbCrLf &
"in Fecha_hasta  DATE, " & vbCrLf &
"In Tasa         Decimal(8,4), " & vbCrLf &
"out Interes      decimal(13,2), " & vbCrLf &
"out error_msg    varchar(300), " & vbCrLf &
"out error int " & vbCrLf &
") " & vbCrLf &
"LANGUAGE SQLSCRIPT " & vbCrLf &
"As  " & vbCrLf &
"BEGIN " & vbCrLf &
"Error := 0; " & vbCrLf &
"error_msg := ''; " & vbCrLf &
"If Saldo < 0 Then " & vbCrLf &
"error_msg ='Valor del Saldo invalido, verifique.'; " & vbCrLf &
"Error = -1; " & vbCrLf &
"end IF; " & vbCrLf &
"If Fecha_desde Is null Then " & vbCrLf &
"error_msg ='Fecha Desde para el cálculo inválido, verifique.'; " & vbCrLf &
"Error = -1;  " & vbCrLf &
"end IF; " & vbCrLf &
"If Fecha_hasta Is null Then " & vbCrLf &
"error_msg ='Fecha Hasta para el cálculo inválido, verifique.'; " & vbCrLf &
"Error = -1; " & vbCrLf &
"end IF; " & vbCrLf &
"If Fecha_desde > Fecha_hasta Then " & vbCrLf &
"error_msg ='Fecha Inicio no puede posterior a la Fecha Final, verifique.'; " & vbCrLf &
"Error = -1;  " & vbCrLf &
"end IF; " & vbCrLf &
"If Tasa < 0 Then " & vbCrLf &
"error_msg ='Tasa para el cálculo inválido, verifique.'; " & vbCrLf &
"Error = -1; " & vbCrLf &
"end IF; " & vbCrLf &
"If Tasa <> 0 And Saldo <> 0 Then " & vbCrLf &
"interes = ROUND((saldo * (tasa/100.0000) * DAYS_BETWEEN(fecha_desde, fecha_hasta))/360.0000 , 2); " & vbCrLf &
"else  " & vbCrLf &
"interes = 0; " & vbCrLf &
"End If; " & vbCrLf &
"END ;"
        Return res
    End Function

    Public Const SP_CUOTA_FIJA_HANA_NAME As String = "SP_CUOTA_FIJA_HANA"
    Public Shared Function SP_CUOTA_FIJA_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CUOTA_FIJA_HANA"" ( " & vbCrLf &
"                       Capital        Decimal(13,2), " & vbCrLf &
"					    NroCuotas      Decimal(16, 10), " & vbCrLf &
"                       Lapso          Decimal(16, 10), " & vbCrLf &
"					    Tasa           Decimal(16, 10), " & vbCrLf &
"                       Fecha_Inicial  Date, " & vbCrLf &
"                       out CuotaFija  Decimal(13, 2), " & vbCrLf &
"                       out error_msg  varchar(300) ) " & vbCrLf &
"LANGUAGE SQLSCRIPT AS  " & vbCrLf &
"                    A_factor                      Decimal(16, 10); " & vbCrLf &
"                    A_cuota_cap                   Decimal(12, 2); " & vbCrLf &
"                    A_F1                          Decimal(16, 10); " & vbCrLf &
"                    A_F2                          Decimal(16, 10); " & vbCrLf &
"                    A_cont                        smallint; " & vbCrLf &
"                    TasaAux                       Decimal(16, 10); " & vbCrLf &
"                    TasaEq                        Decimal(16, 10); " & vbCrLf &
"                    errmsg                        varchar(300); " & vbCrLf &
"BEGIN " & vbCrLf &
"       errmsg = ''; " & vbCrLf &
"      A_cont = 1; " & vbCrLf &
"      ----------------------------------------------------------------------------- " & vbCrLf &
"      -- Validar datos Ingresados  " & vbCrLf &
"	   ----------------------------------------------------------------------------- " & vbCrLf &
"       If Capital <= 0 Then " & vbCrLf &
"           error_msg ='Valor del Capital invalido, verifique.'; " & vbCrLf &
"       End If; " & vbCrLf &
"       If NroCuotas <= 0 Then " & vbCrLf &
"           error_msg ='Nro Cuotas inválido, verifique.'; " & vbCrLf &
"       End If; " & vbCrLf &
"       If Lapso <= 0 Then " & vbCrLf &
"           error_msg ='Lapso para el cálculo inválido, verifique.'; " & vbCrLf &
"       End If; " & vbCrLf &
"        If Tasa < 0 Then " & vbCrLf &
"           error_msg ='Tasa para el cálculo inválido, verifique.'; " & vbCrLf &
"       End If; " & vbCrLf &
"     	---------------------- " & vbCrLf &
"       -- Calcular Interes " & vbCrLf &
"		---------------------- " & vbCrLf &
"       If Tasa > 0 Then " & vbCrLf &
"          TasaEq = Tasa; " & vbCrLf &
"		   TasaAux = Tasa / 100.00000000; " & vbCrLf &
"		   TasaEq = Tasa / (360.00000000 / Lapso); " & vbCrLf &
"		   A_F1 = (power(1 + (TasaEq / 100.00000000), NroCuotas)); " & vbCrLf &
"		   A_F2 = (power(1 + (TasaEq / 100.00000000), NroCuotas) - 1); " & vbCrLf &
"		   A_factor = (TasaEq / 100.00000000) * (A_F1 / A_F2); " & vbCrLf &
"		   CuotaFija = round(Capital * A_factor, 2); " & vbCrLf &
"       Else " & vbCrLf &
"           CuotaFija = Capital / NroCuotas; " & vbCrLf &
"       End If; " & vbCrLf &
"End; "
        Return res
    End Function

    Public Const SP_CALCULO_MULTA_HANA_NAME As String = "SP_CALCULO_MULTA_HANA"
    Public Shared Function SP_CALCULO_MULTA_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_MULTA_HANA"" ( " & vbCrLf &
"                     FechaCuotaVen date                                                     " & vbCrLf &
"				      ,Fecha_pago date                                                       " & vbCrLf &
"				      ,out TotalImpMult decimal(13,2) )                                      " & vbCrLf &
"              LANGUAGE SQLSCRIPT As                                                         " & vbCrLf &
"                           PerGra   int;                                                    " & vbCrLf &
"                           PerGraTip char(1);                                               " & vbCrLf &
"		                    TotalPerGra int;                                                 " & vbCrLf &
"		                    Multa decimal(13,2);                                             " & vbCrLf &
"		                    MultaTip char(2);                                                " & vbCrLf &
"		                    CicloMulta int;                                                  " & vbCrLf &
"		                    DiasVen      int;                                                " & vbCrLf &
"		                    aux          int;                                                " & vbCrLf &
"              BEGIN                                                                         " & vbCrLf &
"		                  TotalImpMult = 0;                                                  " & vbCrLf &
"        aux = 0;                                                           " & vbCrLf &
"        SELECT count(""U_Value1"") into aux FROM  """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" where ""U_Type"" = 'UD_Per_Gra'; " & vbCrLf &
"		 select case when aux>0 then (SELECT ""U_Value1""                                     " & vbCrLf &
"			                           FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" " & vbCrLf &
"			                          where ""U_Type"" = 'UD_Per_Gra')                        " & vbCrLf &
"	                else 0 end into PerGra from Dummy;	                                      " & vbCrLf &
"        aux = 0;                                                           " & vbCrLf &
"        SELECT count(""U_Value1"") into aux FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" where ""U_Type"" = 'UD_PG_Tipo';  " & vbCrLf &
"		 select case when aux>0 then (SELECT ""U_Value1""                                     " & vbCrLf &
"			                           FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" " & vbCrLf &
"			                          where ""U_Type"" = 'UD_PG_Tipo')                        " & vbCrLf &
"	                else '' end into PerGraTip from Dummy;	                                  " & vbCrLf &
"        aux = 0;                                                           " & vbCrLf &
"        SELECT count(""U_Value1"") into aux FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" where ""U_Type"" = 'UD_Multa_F';  " & vbCrLf &
"		select case when aux>0 then (SELECT ""U_Value1""                                      " & vbCrLf &
"			                           FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" " & vbCrLf &
"			                          where ""U_Type"" = 'UD_Multa_F')                        " & vbCrLf &
"	                else 0 end into Multa from Dummy;	                                      " & vbCrLf &
"        aux = 0;                                                           " & vbCrLf &
"        SELECT count(""U_Value1"") into aux FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM"" where ""U_Type"" = 'UD_Mult_Ti'; " & vbCrLf &
"		 select case when aux>0 then (SELECT ""U_Value1""                                     " & vbCrLf &
"			                           FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPARAM""               " & vbCrLf &
"			                          where ""U_Type"" = 'UD_Mult_Ti')                        " & vbCrLf &
"	                else '' end into MultaTip from Dummy;                                     " & vbCrLf &
"		select  case PerGraTip when 'D' then 1                                                " & vbCrLf &
"		                       when 'S' then 7                                                " & vbCrLf &
"						       when 'M' then 30                                               " & vbCrLf &
"							   else 1 end into TotalPerGra from Dummy;                        " & vbCrLf &
"        TotalPerGra = TotalPerGra * PerGra;                                                  " & vbCrLf &
"		select case MultaTip when 'D' then 1                                                  " & vbCrLf &
"		                      when 'S' then 7                                                 " & vbCrLf &
"							  when 'M' then 30                                                " & vbCrLf &
"							  when 'T' then 90                                                " & vbCrLf &
"							  when 'SM' then 180                                              " & vbCrLf &
"							  when 'A' then 360                                               " & vbCrLf &
"							  else 1 end into CicloMulta from Dummy;                          " & vbCrLf &
"	   DiasVen = ifnull(DAYS_BETWEEN(FechaCuotaVen, Fecha_pago),0);                           " & vbCrLf &
"	if FechaCuotaVen < Fecha_pago                                                             " & vbCrLf &
"	   and PerGra > 0 and Multa > 0                                                           " & vbCrLf &
"	   and DiasVen > TotalPerGra then                                                         " & vbCrLf &
"	  TotalImpMult = ((DiasVen-TotalPerGra)/CicloMulta) * Multa;                              " & vbCrLf &
"	else                                                                                      " & vbCrLf &
"	  TotalImpMult = 0;                                                                       " & vbCrLf &
"	end if;                                                                                   " & vbCrLf &
"   END; "
        Return res
    End Function

    Public Const SP_CALCULO_CUOTAS_HANA_NAME As String = "SP_CALCULO_CUOTAS_HANA"
    Public Shared Function SP_CALCULO_CUOTAS_HANA_TEXT() As String
        Dim res As String
        res = "CREATE Procedure """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_CUOTAS_HANA""( " & vbCrLf &
"Capital Decimal(13,2)  " & vbCrLf &
",Cuotas smallint " & vbCrLf &
",Tasa Decimal(8,4) " & vbCrLf &
",Lapso smallint " & vbCrLf &
",Fecha_Ini Date  " & vbCrLf &
",Cap_Pagado Decimal(13,2) " & vbCrLf &
",Cap_Anticipo Decimal(13,2) " & vbCrLf &
",CuotaFija  Char(1) " & vbCrLf &
",GastosA  Decimal(13,2) " & vbCrLf &
",Original  tinyint  " & vbCrLf &
",out Error int " & vbCrLf &
",OUT RESTABLE """ & SQLBaseDatos.ToUpper & """.""CALCUOTATYPE"" " & vbCrLf &
") " & vbCrLf &
"LANGUAGE SQLSCRIPT As " & vbCrLf &
"Tabla1 Integer; " & vbCrLf &
"A_capital Decimal(13,2); " & vbCrLf &
"A_tasa Decimal(8,4);      " & vbCrLf &
"A_periodo smallint; " & vbCrLf &
"A_nro_cuotas smallint; " & vbCrLf &
"O_cuota_fija Decimal(13,2); " & vbCrLf &
"A_saldo Decimal(13,2); " & vbCrLf &
"A_plazo_aux smallint; " & vbCrLf &
"A_fecha_Ini_aux Date; " & vbCrLf &
"A_Fecha_hasta Date; " & vbCrLf &
"A_Cap_pagado Decimal(13,2); " & vbCrLf &
"A_Interes Decimal(13,2); " & vbCrLf &
"Tot_Amortizado Decimal(13,2); " & vbCrLf &
"Por_pagar Decimal(13,2); " & vbCrLf &
"Contador smallint; " & vbCrLf &
"error_exec smallint; " & vbCrLf &
"A_factor Decimal(16,10); " & vbCrLf &
"A_cuota_cap Decimal(12,2); " & vbCrLf &
"A_denominador Decimal(16,10); " & vbCrLf &
"A_numerador Decimal(16,10); " & vbCrLf &
"Ajuste Decimal(13,2); " & vbCrLf &
"ress Decimal (10,4); " & vbCrLf &
"Sald int; " & vbCrLf &
"Fecha_desd Date; " & vbCrLf &
"Fecha_hast Date; " & vbCrLf &
"tas Decimal (10,4); " & vbCrLf &
"error_mss varchar(100); " & vbCrLf &
"Inter Decimal(13,2);  " & vbCrLf &
"err int;  " & vbCrLf &
"A_Ajuste Decimal(13,2); " & vbCrLf &
"A_diferencia Decimal(13, 2); " & vbCrLf &
"A_Fin tinyint; " & vbCrLf &
"A_aux Decimal(13, 2); " & vbCrLf &
"A_Tot_Cuota1 Decimal(13, 2); " & vbCrLf &
"A_Tot_Cuota2 Decimal(13, 2); " & vbCrLf &
"BEGIN  " & vbCrLf &
"If Capital <= 0 Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
"If Cuotas <= 0 Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
"If Tasa < 0 Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
"If Lapso <= 0 Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
"If Cap_Pagado < 0 Or Cap_Pagado > Capital Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
"If Cap_Anticipo < 0 Or Cap_Anticipo >= Capital Then " & vbCrLf &
"Error = -1; " & vbCrLf &
"End If; " & vbCrLf &
" If CuotaFija <> 'S' or CuotaFija <> 'N' then " & vbCrLf &
"error = -1; " & vbCrLf &
"end IF; " & vbCrLf &
"A_capital = Capital; " & vbCrLf &
"A_tasa = Tasa;    " & vbCrLf &
"A_nro_cuotas = Cuotas;    " & vbCrLf &
"A_periodo = Cuotas * Lapso;  " & vbCrLf &
"A_saldo = Capital - ifnull(Cap_Anticipo, 0); " & vbCrLf &
"A_plazo_aux = Lapso; " & vbCrLf &
"A_fecha_Ini_aux = Fecha_ini; " & vbCrLf &
"Tot_Amortizado = ifnull(Cap_Anticipo, 0); " & vbCrLf &
"O_cuota_fija = 0; " & vbCrLf &
"Contador = 0; " & vbCrLf &
"A_Fin = 1; " & vbCrLf &
"A_diferencia = 0; " & vbCrLf &
"A_Ajuste = 0; " & vbCrLf &
"CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" ( " & vbCrLf &
"MES smallint " & vbCrLf &
",FECHA_PAGO Date " & vbCrLf &
",SALDO_CAPITAL Decimal(13,2) " & vbCrLf &
",DIAS smallint " & vbCrLf &
",INTERES Decimal(13,2) " & vbCrLf &
",AMORT_CAP Decimal(13,2) " & vbCrLf &
",TOTAL_CUOTA Decimal(13,2) " & vbCrLf &
",POR_PAGAR_CAP   Decimal(13,2) " & vbCrLf &
",GASTOSA Decimal(13,2) " & vbCrLf &
",TOTAL_CAP_AMORT Decimal(13,2) " & vbCrLf &
",CUBIERTA   tinyint " & vbCrLf &
") ; " & vbCrLf &
"If Cap_Anticipo > 0 Then " & vbCrLf &
"Por_pagar = 0; " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(Contador, Fecha_Ini, 0, 0, 0, Cap_Anticipo, Cap_Anticipo, Por_pagar,0, Cap_Anticipo, 1); " & vbCrLf &
"End If; " & vbCrLf &
"ress = 0; " & vbCrLf &
" A_Fin = 1; " & vbCrLf &
"While A_Fin <> 0 " & vbCrLf &
" Do " & vbCrLf &
"A_capital = Capital; " & vbCrLf &
"A_tasa = Tasa;  " & vbCrLf &
"A_nro_cuotas = Cuotas;  " & vbCrLf &
"A_periodo = Cuotas * Lapso; " & vbCrLf &
"A_saldo = Capital - ifnull(Cap_Anticipo, 0); " & vbCrLf &
"A_plazo_aux = Lapso;    " & vbCrLf &
"A_fecha_Ini_aux = Fecha_ini; " & vbCrLf &
"Tot_Amortizado = ifnull(Cap_Anticipo, 0); " & vbCrLf &
"ress = 0; " & vbCrLf &
"Contador = 0; " & vbCrLf &
" If CuotaFija = 'S' and Tasa <> 0 then " & vbCrLf &
"   A_aux = A_saldo + A_Ajuste; " & vbCrLf &
"   Call """ & SQLBaseDatos.ToUpper & """.""SP_CUOTA_FIJA_HANA""(A_aux, Cuotas, Tasa, Lapso, Fecha_ini, ress, error_mss); " & vbCrLf &
"   Select ress  into  O_cuota_fija from Dummy; " & vbCrLf &
" Else " & vbCrLf &
 "    O_cuota_fija = round(A_saldo / Cuotas, 2);   " & vbCrLf &
"End If;   " & vbCrLf &
"Select Fecha_Ini into A_fecha_Ini_aux from Dummy; " & vbCrLf &
"A_Fecha_hasta = ADD_DAYS(A_fecha_Ini_aux,Lapso); " & vbCrLf &
"A_plazo_aux = DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta); " & vbCrLf &
"If Cuotas > 1 Then " & vbCrLf &
"If Mod(lapso, 30) = 0 Then " & vbCrLf &
"Select 	add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ) into A_Fecha_hasta  " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1) And " & vbCrLf &
"DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = 1; " & vbCrLf &
"Select DAYS_BETWEEN (add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") ) * (lapso/ 30) ) ) " & vbCrLf &
", add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ))   into A_Plazo_aux " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1) And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = 1; " & vbCrLf &
"Else " & vbCrLf &
"Select  add_days(Fecha_Ini , lapso * (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) ) into A_Fecha_hasta " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1) And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = 1; " & vbCrLf &
"Select lapso into A_Plazo_Aux " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1) And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = 1; " & vbCrLf &
"End If; " & vbCrLf &
"End If;  " & vbCrLf &
"Sald        = A_saldo; " & vbCrLf &
"Fecha_desd  = A_fecha_Ini_aux; " & vbCrLf &
"Fecha_hast = A_Fecha_hasta; " & vbCrLf &
"tas        = A_tasa; " & vbCrLf &
"Inter      = A_Interes ; " & vbCrLf &
"error_mss    = ''; " & vbCrLf &
"err 			= 0; " & vbCrLf &
"CALL   """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (Sald,Fecha_desd,Fecha_hast,tas,Inter,error_mss,err); " & vbCrLf &
"select Inter INTO A_Interes from dummy; " & vbCrLf &
"  if CuotaFija = 'S' then " & vbCrLf &
"      A_cuota_cap = O_cuota_fija - A_Interes;  " & vbCrLf &
"  else  " & vbCrLf &
"      A_cuota_cap = O_cuota_fija;  " & vbCrLf &
"  end if;  " & vbCrLf &
"Tot_Amortizado = Tot_Amortizado + A_cuota_cap; " & vbCrLf &
"Por_pagar = A_cuota_cap; " & vbCrLf &
"select A_Fecha_hasta into A_fecha_Ini_aux from Dummy;  " & vbCrLf &
"A_Cap_pagado = Cap_pagado; " & vbCrLf &
"if A_Cap_pagado > 0 then " & vbCrLf &
"Contador = Contador + 1; " & vbCrLf &
"if Original = 1 then " & vbCrLf &
"if A_Cap_pagado < A_cuota_cap then " & vbCrLf &
"Por_pagar = A_cuota_cap - A_Cap_pagado; " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(Contador, A_fecha_Ini_aux,A_saldo,A_plazo_aux, 0, A_cuota_cap , (A_cuota_cap+A_Interes), Por_pagar, 0, Tot_Amortizado,  0); " & vbCrLf &
"else  " & vbCrLf &
"Por_pagar = 0; " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(Contador, A_fecha_Ini_aux, A_saldo, A_plazo_aux, 0,  A_cuota_cap, (A_cuota_cap+A_Interes), Por_pagar,0 , Tot_Amortizado,  1); " & vbCrLf &
"end if;   " & vbCrLf &
"end if; " & vbCrLf &
"A_Cap_pagado = A_Cap_pagado - A_cuota_cap; " & vbCrLf &
"else  " & vbCrLf &
"if Tot_Amortizado <> Capital And Cuotas = 1 then " & vbCrLf &
"select A_cuota_cap - (Tot_Amortizado - Capital) into A_cuota_cap from Dummy; " & vbCrLf &
"select Tot_Amortizado - (Tot_Amortizado - Capital) into Tot_Amortizado from Dummy; " & vbCrLf &
"select	A_cuota_cap into 	Por_pagar from Dummy; " & vbCrLf &
"end if; --if Tot_Amortizado <> Capital And Cuotas = 1  " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(1, A_fecha_Ini_aux, A_saldo, A_plazo_aux, A_Interes, A_cuota_cap, A_cuota_cap+A_Interes, Por_pagar,0 , Tot_Amortizado, 0); " & vbCrLf &
"end if;  " & vbCrLf &
"Contador= 2 ; " & vbCrLf &
"While Contador <= A_nro_cuotas " & vbCrLf &
"do " & vbCrLf &
"A_saldo = A_saldo - A_cuota_cap; " & vbCrLf &
"A_plazo_aux = A_plazo_aux + Lapso; " & vbCrLf &
"A_Fecha_hasta = ADD_DAYS(A_fecha_Ini_aux,Lapso); " & vbCrLf &
"IF MOD(lapso, 30) = 0 THEN " & vbCrLf &
"SELECT 	add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ) into A_Fecha_hasta  " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1) And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = Contador; " & vbCrLf &
"SELECT 	DAYS_BETWEEN (add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") ) * (lapso/ 30) ) ) " & vbCrLf &
", add_months(Fecha_Ini ,  ( (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) * (lapso/ 30) ) ))   into A_Plazo_aux " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1)  And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = Contador; " & vbCrLf &
"else " & vbCrLf &
"SELECT  add_days(Fecha_Ini , lapso * (DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1) ) into A_Fecha_hasta " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini And add_days(Fecha_Ini , Cuotas -1)  And DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = Contador; " & vbCrLf &
"SELECT lapso into A_Plazo_Aux " & vbCrLf &
"FROM _SYS_BI.M_TIME_DIMENSION " & vbCrLf &
"WHERE ""DATE_SAP"" BETWEEN Fecha_Ini and add_days(Fecha_Ini , Cuotas -1)  and DAYS_BETWEEN(Fecha_Ini, ""DATE_SAP"") +1 = Contador; " & vbCrLf &
"end if; " & vbCrLf &
"Sald        = A_saldo; " & vbCrLf &
"Fecha_desd = A_fecha_Ini_aux; " & vbCrLf &
"Fecha_hast  = A_Fecha_hasta; " & vbCrLf &
"Tas        = A_tasa; " & vbCrLf &
"Inter      = A_Interes; " & vbCrLf &
"error_mss   = ''; " & vbCrLf &
"err 			= 0; " & vbCrLf &
"CALL   """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA""   " & vbCrLf &
"( " & vbCrLf &
"Sald , " & vbCrLf &
"Fecha_desd, " & vbCrLf &
"Fecha_hast, " & vbCrLf &
"tas, " & vbCrLf &
"Inter, " & vbCrLf &
"error_mss, " & vbCrLf &
"err " & vbCrLf &
"); " & vbCrLf &
"select Inter INTO A_Interes from dummy; " & vbCrLf &
"select A_Fecha_hasta into A_fecha_Ini_aux  from Dummy; " & vbCrLf &
"  if CuotaFija = 'S' then " & vbCrLf &
"      A_cuota_cap = O_cuota_fija - A_Interes;  " & vbCrLf &
"  else  " & vbCrLf &
"      A_cuota_cap = O_cuota_fija;  " & vbCrLf &
"  end if;  " & vbCrLf &
"Tot_Amortizado = Tot_Amortizado + A_cuota_cap; " & vbCrLf &
"Por_pagar = A_cuota_cap; " & vbCrLf &
"if Tot_Amortizado <> Capital And Contador = Cuotas then " & vbCrLf &
"select A_cuota_cap - (Tot_Amortizado - Capital) into A_cuota_cap from Dummy; " & vbCrLf &
"select Tot_Amortizado - (Tot_Amortizado - Capital) into Tot_Amortizado from Dummy; " & vbCrLf &
"end if; " & vbCrLf &
"if A_Cap_pagado > 0 then  " & vbCrLf &
"if Original = 1 then " & vbCrLf &
"if A_Cap_pagado < A_cuota_cap then " & vbCrLf &
"Por_pagar = A_cuota_cap - A_Cap_pagado; " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""  values(Contador, A_fecha_Ini_aux,A_saldo, A_plazo_aux, 0,  A_cuota_cap, (A_cuota_cap+A_Interes), Por_pagar,0 , Tot_Amortizado, 0); " & vbCrLf &
"else " & vbCrLf &
"Por_pagar = 0; " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""  values(Contador, A_fecha_Ini_aux, A_saldo, A_plazo_aux, 0,  A_cuota_cap, (A_cuota_cap+A_Interes), Por_pagar,0 , Tot_Amortizado,  1); " & vbCrLf &
"end if; " & vbCrLf &
"A_Cap_pagado = A_Cap_pagado - A_cuota_cap; " & vbCrLf &
"end if;  " & vbCrLf &
"else  " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""  values(Contador,A_fecha_Ini_aux, A_saldo, A_plazo_aux, A_Interes, A_cuota_cap, (A_cuota_cap+A_Interes), Por_pagar,0, Tot_Amortizado,  0); " & vbCrLf &
"end if;  " & vbCrLf &
"Contador=Contador+1; " & vbCrLf &
"End while; " & vbCrLf &
" If Cuotas > 1 then " & vbCrLf &
"select Total_Cuota into A_Tot_Cuota1 from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes =  Contador-1; " & vbCrLf &
"Select Total_Cuota into A_Tot_Cuota2 from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes =  Contador-2; " & vbCrLf &
"A_diferencia = A_Tot_Cuota1 - A_Tot_Cuota2; " & vbCrLf &
"   If abs(A_diferencia) > 0 And CuotaFija = 'S' then  " & vbCrLf &
"                If A_Ajuste > 0 Then " & vbCrLf &
"                   A_Fin = 0; " & vbCrLf &
"                End If; " & vbCrLf &
"       A_Ajuste = A_Ajuste + (abs(a_diferencia) / power((1.0 + Tasa / 1200.0), Cuotas)); " & vbCrLf &
"       If A_Fin <> 0 Then " & vbCrLf &
"            delete from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes <> 0; " & vbCrLf &
"       End If; " & vbCrLf &
"   Else " & vbCrLf &
"    A_Fin = 0; " & vbCrLf &
"   End If; " & vbCrLf &
" else " & vbCrLf &
"    A_Fin = 0; " & vbCrLf &
" End If; " & vbCrLf &
"End While; " & vbCrLf &
"RESTABLE = Select * FROM """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""; " & vbCrLf &
"DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""; " & vbCrLf &
"End;"
        Return res
    End Function



    Public Const SP_CALCULO_PAGO_HANA_NAME As String = "SP_CALCULO_PAGO_HANA"
    Public Shared Function SP_CALCULO_PAGO_HANA_TEXT() As String
        Dim res As String
        res = "CREATE Procedure """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_PAGO_HANA"" " & vbCrLf &
"                ( V_DocEntryOV  int        -- Orden de Venta                             " & vbCrLf &
"				,V_Fecha_Ini    date       -- Fecha inicio de plan de pagos               " & vbCrLf &
"				,V_FechaUltPago date      -- Fecha de ultimo pago                         " & vbCrLf &
"				,V_Fecha_pago   date       -- Fecha del pago a realizarce                 " & vbCrLf &
"				,V_Fecha_valor  date                                                      " & vbCrLf &
"				,V_GastosA      decimal(13,2)                                             " & vbCrLf &
"				,V_error_msg    int                                                       " & vbCrLf &
"				,V_OV_Fact     smallint   ---- 0: Orden de Venta  1: Factura              " & vbCrLf &
"				,OUT RESTABLE2 """ & SQLBaseDatos.ToUpper & """.""CALPAGOTYPE""           " & vbCrLf &
"                ,OUT RESTABLE3 """ & SQLBaseDatos.ToUpper & """.""CALCPAGOTYPE""         " & vbCrLf &
" )   " & vbCrLf &
"LANGUAGE SQLSCRIPT As                                                                    " & vbCrLf &
"			        A_capital                     decimal(13,2);                          " & vbCrLf &
"					A_tasa                        decimal(8,4);                           " & vbCrLf &
"					A_nro_cuotas                  smallint;                               " & vbCrLf &
"					A_saldo                       decimal(13,2);                          " & vbCrLf &
"					A_plazo_aux                   smallint;                               " & vbCrLf &
"					A_fecha_Ini_aux               date;                                   " & vbCrLf &
"					A_Fecha_hasta                 date;                                   " & vbCrLf &
"					A_Cap_pagado                  decimal(13,2);                          " & vbCrLf &
"					A_Interes                     decimal(13,2);                          " & vbCrLf &
"					Tot_Amortizado                decimal(13,2);                          " & vbCrLf &
"					Por_pagar                     decimal(13,2);                          " & vbCrLf &
"					Contador                      smallint;                               " & vbCrLf &
"					error_exec                    smallint;                               " & vbCrLf &
"			        A_cuota_cap                   decimal(12,2);                          " & vbCrLf &
"					A_tl_Int_Carg                 decimal(13,2);                          " & vbCrLf &
"					U_DocEntryPG                  int;                                    " & vbCrLf &
"					Tasa                          decimal(8,4);                           " & vbCrLf &
"					Tasa_Vcdo                     decimal(8,4);                           " & vbCrLf &
"					U_CardCode                    varchar(50);                            " & vbCrLf &
"					TasaNormalLegal               char(1);                                " & vbCrLf &
"					FechaCuotaVen                 date;                                   " & vbCrLf &
"					TotalImpMult                  decimal(13,2);                          " & vbCrLf &
"					TotalImpMult_aux              decimal(13,2);                          " & vbCrLf &
"					A_DocEntryOF                  int;                                    " & vbCrLf &
"					A_Fecha_min_aux               date;                                   " & vbCrLf &
"					aux                           int;                                    " & vbCrLf &
"					V_error_mss                   varchar(100);                           " & vbCrLf &
"                    V_Inter                       decimal(13,2);                         " & vbCrLf &
"                    V_err                         int;                                   " & vbCrLf &
" BEGIN                                                                                   " & vbCrLf &
"			if V_DocEntryOV = 0 then                                                      " & vbCrLf &
"			    V_error_msg = -1;                                                         " & vbCrLf &
"			 end if;                                                                      " & vbCrLf &
"			 if V_Fecha_pago <= V_Fecha_Ini then                                          " & vbCrLf &
"			     V_error_msg = -1;                                                        " & vbCrLf &
"			 end if;                                                                      " & vbCrLf &
"			 if V_Fecha_valor is null then                                                " & vbCrLf &
"			     V_error_msg = -1;                                                        " & vbCrLf &
"             end if;                                                                     " & vbCrLf &
"			 if V_Fecha_valor > V_Fecha_pago then                                         " & vbCrLf &
"			     V_error_msg = -1;                                                        " & vbCrLf &
"             end if;                                                                     " & vbCrLf &
"			        A_tasa          = Tasa;                                               " & vbCrLf &
"					A_Cap_pagado    = 0;                                                  " & vbCrLf &
"					Tot_Amortizado  = 0;                                                  " & vbCrLf &
"					A_fecha_Ini_aux = V_Fecha_Ini;                                        " & vbCrLf &
"					Tot_Amortizado  = 0;                                                  " & vbCrLf &
"					Contador        = 1;                                                  " & vbCrLf &
"					TotalImpMult    = 0;                                                  " & vbCrLf &
"					TasaNormalLegal = 'N';                                                " & vbCrLf &
"				CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" ( " & vbCrLf &
"					Mes smallint                                                          " & vbCrLf &
"					,Fecha_Pago date                                                      " & vbCrLf &
"					,Saldo_capital decimal(13,2)                                          " & vbCrLf &
"					,Dias smallint                                                        " & vbCrLf &
"					,Interes decimal(13,2)                                                " & vbCrLf &
"					,Amort_Cap decimal(13,2)                                              " & vbCrLf &
"					,Total_Cuota decimal(13,2)                                            " & vbCrLf &
"					,Por_Pagar_Cap   decimal(13,2)                                        " & vbCrLf &
"					,Total_Cap_Amort decimal(13,2)                                        " & vbCrLf &
"					,Cubierta   tinyint                                                   " & vbCrLf &
"					);                                                                    " & vbCrLf &
"			     CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" ( " & vbCrLf &
"					Descripcion varchar(30)                                               " & vbCrLf &
"					,Fecha_Pago date                                                      " & vbCrLf &
"					,Fecha_cuota date                                                     " & vbCrLf &
"					,Fecha_valor date                                                     " & vbCrLf &
"					,DiasCuot smallint                                                    " & vbCrLf &
"					,Interes decimal(13,2)                                                " & vbCrLf &
"					,DiasIntMor  smallint                                                 " & vbCrLf &
"					,IntPorMor decimal(13,2)                                              " & vbCrLf &
"					,Multa decimal(13,2)                                                  " & vbCrLf &
"					,Amort_Cap decimal(13,2)                                              " & vbCrLf &
"					,GastosA decimal(13,2)                                                " & vbCrLf &
"					,Total_Cuota decimal(13,2)                                            " & vbCrLf &
"                   ,FechaUltPago date                                                    " & vbCrLf &
"					,Dias  int                                                            " & vbCrLf &
"					,InteresTl decimal(13,2)                                              " & vbCrLf &
"					);                                                                    " & vbCrLf &
" if V_OV_Fact = 0 then -- Orden de venta                                                 " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""U_DocCuota"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                   " & vbCrLf &
"   select case when aux > 0 then (select ""U_DocCuota""                                  " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                      " & vbCrLf &
"         else 0 end into U_DocEntryPG from Dummy;                                        " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""U_Interes"") into aux                                                  " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                   " & vbCrLf &
"   select case when aux > 0 then (select ""U_Interes""                                   " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                      " & vbCrLf &
"          else 0 end into Tasa from Dummy;                                               " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""U_IntMor"") into aux                                                   " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                   " & vbCrLf &
"   select case when aux > 0 then (select ""U_IntMor""                                    " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                      " & vbCrLf &
"          else 0 end into Tasa_Vcdo from Dummy;                                          " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""CardCode"") into aux                                                   " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV ;                                                  " & vbCrLf &
"   select case when aux > 0 then (select ""CardCode""                                    " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                      " & vbCrLf &
"          else '0' end into U_CardCode from Dummy;                                         " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""TrgetEntry"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""RDR1""  -- Tabla Maestro Orden de Venta                     " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV -- DocEntry Orden de Venta                         " & vbCrLf &
"	  and ""TargetType"" = 13; -- Tipo Factura                                            " & vbCrLf &
"   select case when aux > 0 then (select DISTINCT(""TrgetEntry"")                                  " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""RDR1""  -- Tabla Maestro Orden de Venta " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV -- DocEntry Orden de Venta " & vbCrLf &
"	                                and ""TargetType"" = 13)                              " & vbCrLf &
"         else 0 end into A_DocEntryOF from Dummy;                                        " & vbCrLf &
"  If ifnull(A_DocEntryOF,0) <> 0 then -- Si existe factura obtenemos lo total pagado     " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""PaidToDate"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""DocEntry"" = A_DocEntryOF;                                                   " & vbCrLf &
"   select case when aux > 0 then (select ""PaidToDate""                                  " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""OINV""             " & vbCrLf &
"						          where ""DocEntry"" = A_DocEntryOF)                      " & vbCrLf &
"         else 0 end into A_Cap_Pagado from Dummy;                                        " & vbCrLf &
"  else                                                                                   " & vbCrLf &
"    -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta             " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""DocTotal"") into aux                                                   " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""ORCT""  -- Tabla Maestro Orden de Venta         " & vbCrLf &
"    where ""Canceled"" = 'N' and ""U_ORDV"" = V_DocEntryOV;                              " & vbCrLf &
"   select case when aux > 0 then (select ""DocTotal""                                    " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""ORCT""             " & vbCrLf &
"						          where ""Canceled"" = 'N' and ""U_ORDV"" = V_DocEntryOV) " & vbCrLf &
"         else 0 end into A_Cap_Pagado from Dummy;                                        " & vbCrLf &
"   end if;                                                                               " & vbCrLf &
"   A_Cap_Pagado = ifnull(A_Cap_Pagado, 0);                                               " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                       " & vbCrLf &
"   select 0, ""U_FechaV"", 0, 0, 0, ""U_CapitalxPagar"" , ""U_CapitalxPagar"" , ""U_CapitalxPagar"", 0, 0      " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN""                                " & vbCrLf &
"    where ""U_DocEntryPG"" = U_DocEntryPG                                                " & vbCrLf &
"      and ""U_CapitalxPagar"" <> '0'                                                     " & vbCrLf &
"    order by ""U_FechaV"" asc;                                                           " & vbCrLf &
"   aux = 0;                                                                              " & vbCrLf &
"   select count(""U_Intlgl"") into aux                                                   " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""                                " & vbCrLf &
"    where ""U_CardCode"" = U_CardCode;                                                   " & vbCrLf &
"   select case when aux > 0 then (select ""U_Intlgl""                                    " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""   " & vbCrLf &
"						          where ""U_CardCode"" = U_CardCode)                      " & vbCrLf &
"         else 'N' end into TasaNormalLegal from Dummy;                                     " & vbCrLf &
"  if TasaNormalLegal = 'N' then                                                          " & vbCrLf &
"     A_tasa = Tasa;                                                                      " & vbCrLf &
"  end if;                                                                                " & vbCrLf &
"   A_nro_cuotas = 0;                                                                     " & vbCrLf &
"   select count(Fecha_Pago) into A_nro_cuotas                                            " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                             " & vbCrLf &
"   select case when A_nro_cuotas > 0 then (select sum(Amort_Cap)                         " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"") " & vbCrLf &
"         else 0 end into A_capital from Dummy;                                           " & vbCrLf &
"   select case when A_nro_cuotas > 0 then (select min(Fecha_Pago)                        " & vbCrLf &
"						           from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"") " & vbCrLf &
"         else '20190101' end into A_Fecha_hasta from Dummy;                              " & vbCrLf &
"   while Contador <= A_nro_cuotas                                                        " & vbCrLf &
"   DO                                                                                    " & vbCrLf &
"       select min(Fecha_Pago) into A_Fecha_hasta                                         " & vbCrLf &
"        from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                           " & vbCrLf &
"        where Mes = 0;                                                                   " & vbCrLf &
"       update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                          " & vbCrLf &
"	      set Mes = Contador                                                              " & vbCrLf &
"	   where Fecha_Pago = A_Fecha_hasta;                                                  " & vbCrLf &
"       A_saldo = A_capital - Tot_Amortizado;                                             " & vbCrLf &
"       V_error_mss = '';                                                                 " & vbCrLf &
"       V_err = 0;                                                                        " & vbCrLf &
"       V_Inter = A_Interes;                                                              " & vbCrLf &
"      CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (A_saldo, A_fecha_Ini_aux, A_Fecha_hasta, A_tasa, V_Inter, V_error_mss, V_err );   " & vbCrLf &
"      select V_Inter into A_Interes from Dummy;                                          " & vbCrLf &
"   update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                            " & vbCrLf &
"      set Saldo_capital = A_capital - Tot_Amortizado,                                    " & vbCrLf &
"	      Dias = DAYS_BETWEEN(A_fecha_Ini_aux, Fecha_Pago),                               " & vbCrLf &
"		  Interes = A_Interes,                                                            " & vbCrLf &
"		  Total_Cuota = Amort_Cap + A_Interes,                                            " & vbCrLf &
"		  Por_Pagar_Cap = (case when  Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 0 else case when (A_Cap_pagado - Tot_Amortizado) > 0 then  Amort_Cap - (A_Cap_pagado - Tot_Amortizado) else Amort_Cap end end),   " & vbCrLf &
"		  Total_Cap_Amort = Tot_Amortizado + Amort_Cap,                                   " & vbCrLf &
"		  Cubierta = (case when  Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 1 else 0 end) " & vbCrLf &
"    where Mes = Contador;                                                                " & vbCrLf &
"      select Tot_Amortizado + Amort_Cap into Tot_Amortizado                              " & vbCrLf &
"       from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                          " & vbCrLf &
"      where Mes = Contador;                                                              " & vbCrLf &
"      select Fecha_Pago into A_fecha_Ini_aux                                             " & vbCrLf &
"       from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                          " & vbCrLf &
"      where Mes = Contador;                                                              " & vbCrLf &
"    Contador = Contador + 1;                                                             " & vbCrLf &
" END WHILE;  -- while Contador <= A_nro_cuotas                                           " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                    " & vbCrLf &
"   select concat('Mes ', TO_VARCHAR(Mes)) ,                                              " & vbCrLf &
"          V_Fecha_pago,                                                                  " & vbCrLf &
"		  ifnull(Fecha_Pago,V_Fecha_Ini) ,                                                " & vbCrLf &
"		  V_Fecha_valor,                                                                  " & vbCrLf &
"          ifnull(Dias,0),                                                                " & vbCrLf &
"		  case when V_Fecha_valor > Fecha_Pago then  ifnull(Interes,0)                    " & vbCrLf &
"		  else 0 end,                                                                     " & vbCrLf &
"		  0,--isnull(datediff(d, fecha_pago, V_Fecha_pago),0),                            " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                       " & vbCrLf &
"		  0,                                                                              " & vbCrLf &
"	      -- CALCULO Tasa Multa --------------------------                                " & vbCrLf &
" 		  0,                                                                              " & vbCrLf &
"		  ------------------------------------------------                                " & vbCrLf &
"		  ifnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                 " & vbCrLf &
"		  0, -- Gastos Adicionales                                                        " & vbCrLf &
"         0,                                                                              " & vbCrLf &
"		  V_FechaUltPago," & vbCrLf &
"          0," & vbCrLf &
"		  0                                                                               " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                            " & vbCrLf &
"    where Cubierta = 0                                                                   " & vbCrLf &
"	  and Fecha_Pago <= V_Fecha_pago;                                                     " & vbCrLf &
"  insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                     " & vbCrLf &
"   select 'TOTALES ',V_Fecha_pago,                                                       " & vbCrLf &
"          ifnull(min(Fecha_Cuota),V_Fecha_Ini) ,                                         " & vbCrLf &
"		  V_Fecha_valor,                                                                  " & vbCrLf &
"		  ifnull(sum(DiasCuot),0),                                                        " & vbCrLf &
"		  ifnull(sum(Interes),0),                                                         " & vbCrLf &
"		  ifnull(sum(DiasIntMor),0),                                                      " & vbCrLf &
"		  ifnull(sum(IntPorMor),0),                                                       " & vbCrLf &
"		  ifnull(sum(Multa), 0),                                                          " & vbCrLf &
"		  ifnull(sum(Amort_Cap),0),                                                       " & vbCrLf &
"		  ifnull(V_GastosA,0), -- Gastos Adicionales                                      " & vbCrLf &
"		  ifnull(sum(Amort_Cap),0),                                                        " & vbCrLf &
"         V_FechaUltPago, " & vbCrLf &
"		  0, " & vbCrLf &
"		  0 " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" ;                        " & vbCrLf &
"            Select min(Fecha_cuota) into A_Fecha_min_aux " & vbCrLf &
"   from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""; " & vbCrLf &
"   A_saldo = A_capital - A_Cap_Pagado; -- - @Cap_Anticipo " & vbCrLf &
"   A_fecha_Ini_aux = V_FechaUltPago; " & vbCrLf &
"   A_Fecha_hasta = V_Fecha_valor; " & vbCrLf &
"   If A_tasa > 0 " & vbCrLf &
"     And A_Fecha_min_aux < V_Fecha_pago " & vbCrLf &
"     And TasaNormalLegal = 'N'  " & vbCrLf &
"     And V_FechaUltPago < V_Fecha_valor Then " & vbCrLf &
"      V_error_mss = ''; " & vbCrLf &
"      V_err = 0;                                                                           " & vbCrLf &
"      V_Inter = A_Interes;                                                               " & vbCrLf &
"    CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (A_saldo, A_fecha_Ini_aux, A_Fecha_hasta, A_tasa, V_Inter, V_error_mss, V_err ); " & vbCrLf &
"    Select V_Inter into A_Interes from Dummy;                                           " & vbCrLf &
"	 update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                       " & vbCrLf &
"	    set DiasCuot = DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),                     " & vbCrLf &
"		    Interes = A_Interes,                                                         " & vbCrLf &
"		    Total_Cuota = A_Interes + IntPorMor + Multa + Amort_Cap + V_GastosA,          " & vbCrLf &
"            Dias = Dias + DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta), " & vbCrLf &
"		    InteresTl = InteresTl + A_Interes " & vbCrLf &
"	  where Descripcion like '%TOTALES%';                                                " & vbCrLf &
"	end if;                                                                              " & vbCrLf &
"		 A_saldo = A_capital - A_Cap_Pagado; -- - @Cap_Anticipo                          " & vbCrLf &
"	     A_fecha_Ini_aux = V_FechaUltPago;                                               " & vbCrLf &
"	     A_Fecha_hasta = V_Fecha_valor;                                                  " & vbCrLf &
"     If Tasa_Vcdo > 0  " & vbCrLf &
"	   And A_Fecha_min_aux < V_Fecha_pago " & vbCrLf &
"	   And TasaNormalLegal = 'Y'  " & vbCrLf &
"	   And V_FechaUltPago < V_Fecha_valor then " & vbCrLf &
"		 V_error_mss = '';                                                               " & vbCrLf &
"        V_err = 0;                                                                      " & vbCrLf &
"        V_Inter = A_Interes;                                                            " & vbCrLf &
"         CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (A_saldo, A_fecha_Ini_aux, A_Fecha_hasta, Tasa_Vcdo, V_Inter, V_error_mss, V_err );   " & vbCrLf &
"        select V_Inter into A_Interes from Dummy;                                       " & vbCrLf &
"		update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                    " & vbCrLf &
"		   set IntPorMor = A_Interes,                                                    " & vbCrLf &
"		       DiasIntMor = DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),                " & vbCrLf &
"               Total_Cuota = Interes + A_Interes + Multa + Amort_Cap + V_GastosA, " & vbCrLf &
"			   Dias = Dias + DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta), " & vbCrLf &
"		       InteresTl = InteresTl + A_Interes " & vbCrLf &
"		where Descripcion like '%TOTALES%';       " & vbCrLf &
"	end if;                                                                              " & vbCrLf &
"--- MULTAS                                                                              " & vbCrLf &
"    select min(Fecha_cuota) into FechaCuotaVen                                          " & vbCrLf &
"	   from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                      " & vbCrLf &
"    TotalImpMult_aux = 0;                                                               " & vbCrLf &
"    call """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_MULTA_HANA""( FechaCuotaVen, V_Fecha_pago, TotalImpMult_aux);    " & vbCrLf &
"    select TotalImpMult_aux into TotalImpMult from Dummy;                               " & vbCrLf &
"    update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                       " & vbCrLf &
"       set Multa = ifnull(TotalImpMult,0)                                               " & vbCrLf &
"     where Descripcion like '%TOTALES%';                                                " & vbCrLf &
" else -- FACTURA                                                                        " & vbCrLf &
" aux = 0;                                                                               " & vbCrLf &
"   Select count(""U_Interes"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                  " & vbCrLf &
"   Select Case When aux > 0 Then (Select ""U_Interes""                                  " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"          Else 0 End into Tasa from Dummy;                                              " & vbCrLf &
"  aux = 0;                                                                              " & vbCrLf &
"   Select count(""U_IntMor"") into aux                                                  " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                  " & vbCrLf &
"   Select Case when aux > 0 then (select ""U_IntMor""                                   " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura  " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"          Else 0 End into Tasa_Vcdo from Dummy;                                         " & vbCrLf &
"   aux = 0;                                                                             " & vbCrLf &
"   Select  count(""CardCode"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV ;                                                 " & vbCrLf &
"   Select Case when aux > 0 then (select ""CardCode""                                   " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura  " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"          Else '0' end into U_CardCode from Dummy;                                      " & vbCrLf &
"   Select count(""PaidToDate"") into aux                                                " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                  " & vbCrLf &
"   Select Case when aux > 0 then (select ""PaidToDate""                                 " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""       " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"         Else 0 End into A_Cap_Pagado from Dummy;                                       " & vbCrLf &
"  Select count(""DocDueDate"") into aux                                                 " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                  " & vbCrLf &
"   Select Case when aux > 0 then (select ""DocDueDate""                                 " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"         Else '20190101' end into FechaCuotaVen  from Dummy;                            " & vbCrLf &
"   Select count(""DocTotal"") into aux                                                  " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura           " & vbCrLf &
"    where ""DocEntry"" = V_DocEntryOV;                                                  " & vbCrLf &
"   Select Case when aux > 0 then (select ""DocTotal""                                   " & vbCrLf &
"                                   from """ & SQLBaseDatos.ToUpper & """.""OINV""  -- Tabla Maestro Factura  " & vbCrLf &
"						          where ""DocEntry"" = V_DocEntryOV)                     " & vbCrLf &
"         Else 0 End into A_capital  from Dummy;                                         " & vbCrLf &
" insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(1, :FechaCuotaVen, 0, 0, 0, :A_capital , :A_capital , :A_capital, 0, 0) ;    " & vbCrLf &
" insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                     " & vbCrLf &
"   Select concat('Mes ', TO_VARCHAR(Mes)) ,                                             " & vbCrLf &
"          V_Fecha_pago,                                                                 " & vbCrLf &
"          ifnull(Fecha_Pago, V_Fecha_Ini),                                              " & vbCrLf &
"          V_Fecha_valor,                                                                " & vbCrLf &
"          ifnull(Dias, 0),                                                              " & vbCrLf &
"		  case when V_Fecha_valor > Fecha_Pago then  ifnull(Interes, 0)                  " & vbCrLf &
"          Else 0 End,                                                                   " & vbCrLf &
"		  0,--isnull(datediff(d, fecha_pago, V_Fecha_pago),0),                           " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                      " & vbCrLf &
"		  0,                                                                             " & vbCrLf &
"	      -- CALCULO Tasa Multa --------------------------                               " & vbCrLf &
" 		  0,                                                                             " & vbCrLf &
"		  ------------------------------------------------                               " & vbCrLf &
"		  ifnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                " & vbCrLf &
"		  0, -- Gastos Adicionales                                                       " & vbCrLf &
"         0,                                                                             " & vbCrLf &
"		  V_FechaUltPago,                                                                " & vbCrLf &
"         0,                                                                             " & vbCrLf &
"         0                                                                              " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                           " & vbCrLf &
"    where Cubierta = 0                                                                  " & vbCrLf &
"	  And Fecha_Pago <= V_Fecha_pago;                                                    " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                      " & vbCrLf &
"   Select 'TOTALES ',V_Fecha_pago,                                                      " & vbCrLf &
"          ifnull(min(Fecha_Cuota), V_Fecha_Ini) ,                                       " & vbCrLf &
"		  V_Fecha_valor,                                                                 " & vbCrLf &
"          ifnull(sum(DiasCuot), 0),                                                     " & vbCrLf &
"          ifnull(sum(Interes), 0),                                                      " & vbCrLf &
"          ifnull(sum(DiasIntMor), 0),                                                   " & vbCrLf &
"          ifnull(sum(IntPorMor), 0),                                                    " & vbCrLf &
"          ifnull(sum(Multa), 0),                                                        " & vbCrLf &
"          ifnull(sum(Amort_Cap), 0),                                                    " & vbCrLf &
"          ifnull(V_GastosA, 0), --Gastos Adicionales                                    " & vbCrLf &
"          ifnull(sum(Amort_Cap), 0),                                                    " & vbCrLf &
"          V_FechaUltPago,                                                               " & vbCrLf &
"          0,                                                                            " & vbCrLf &
"          0                                                                             " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" ;                       " & vbCrLf &
"  	Select min(Fecha_cuota) into A_Fecha_min_aux                                         " & vbCrLf &
"       from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                     " & vbCrLf &
"	A_saldo = A_capital  - A_Cap_Pagado; -- - @Cap_Anticipo                              " & vbCrLf &
"	A_fecha_Ini_aux = V_FechaUltPago;                                                    " & vbCrLf &
"	A_Fecha_hasta = V_Fecha_valor;                                                       " & vbCrLf &
"    A_tasa = Tasa;                                                                      " & vbCrLf &
"   If A_tasa > 0                                                                        " & vbCrLf &
"     And A_Fecha_min_aux < V_Fecha_pago                                                 " & vbCrLf &
"     And TasaNormalLegal = 'N'                                                          " & vbCrLf &
"     And V_FechaUltPago < V_Fecha_valor Then                                            " & vbCrLf &
"      V_error_mss = '';                                                                 " & vbCrLf &
"      V_err = 0;                                                                        " & vbCrLf &
"      V_Inter = A_Interes;                                                              " & vbCrLf &
"    CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (A_saldo, A_fecha_Ini_aux, A_Fecha_hasta, A_tasa, V_Inter, V_error_mss, V_err );                    " & vbCrLf &
"    Select V_Inter into A_Interes from Dummy;                                           " & vbCrLf &
"	 update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                       " & vbCrLf &
"	    set DiasCuot = DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),                     " & vbCrLf &
"		    Interes = A_Interes,                                                         " & vbCrLf &
"		    Total_Cuota = A_Interes + IntPorMor + Multa + Amort_Cap + V_GastosA,         " & vbCrLf &
"            Dias = Dias + DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),                 " & vbCrLf &
"		    InteresTl = InteresTl + A_Interes                                            " & vbCrLf &
"	  where Descripcion like '%TOTALES%';                                                " & vbCrLf &
"	end if;                                                                              " & vbCrLf &
"		 A_saldo = A_capital - A_Cap_Pagado; -- - @Cap_Anticipo                          " & vbCrLf &
"	     A_fecha_Ini_aux = V_FechaUltPago;                                               " & vbCrLf &
"	     A_Fecha_hasta = V_Fecha_valor;                                                  " & vbCrLf &
"     If Tasa_Vcdo > 0                                                                   " & vbCrLf &
"	   And A_Fecha_min_aux < V_Fecha_pago                                                " & vbCrLf &
"	   And TasaNormalLegal = 'Y'                                                         " & vbCrLf &
"	   And V_FechaUltPago < V_Fecha_valor then                                           " & vbCrLf &
"		 V_error_mss = '';                                                               " & vbCrLf &
"        V_err = 0;                                                                      " & vbCrLf &
"        V_Inter = A_Interes;                                                            " & vbCrLf &
"         CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (A_saldo, A_fecha_Ini_aux, A_Fecha_hasta, Tasa_Vcdo, V_Inter, V_error_mss, V_err );                    " & vbCrLf &
"        select V_Inter into A_Interes from Dummy;                                       " & vbCrLf &
"		update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                    " & vbCrLf &
"		   set IntPorMor = A_Interes,                                                    " & vbCrLf &
"		       DiasIntMor = DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),                " & vbCrLf &
"               Total_Cuota = Interes + A_Interes + Multa + Amort_Cap + V_GastosA,       " & vbCrLf &
"			   Dias = Dias + DAYS_BETWEEN(A_fecha_Ini_aux, A_Fecha_hasta),               " & vbCrLf &
"		       InteresTl = InteresTl + A_Interes                                         " & vbCrLf &
"		where Descripcion like '%TOTALES%';                                              " & vbCrLf &
"	end if;                                                                              " & vbCrLf &
"End If; -- final V_OV_Fact                                                              " & vbCrLf &
"    Select  Descripcion As DESCRIPCION, Fecha_Pago As FECHA_PAGO, Fecha_cuota As FECHA_CUOTA, Fecha_valor As FECHA_VALOR, DiasCuot As DIASCUOT    " & vbCrLf &
"		  ,Interes As INTERES, DiasIntMor  As DIASINTMOR, IntPorMor As INTPORMOR, Multa As MULTA, Amort_Cap As AMORT_CAP, GastosA As GASTOSA    " & vbCrLf &
"		  ,Total_Cuota As TOTAL_CUOTA, FechaUltPago As FECHAULTPAG, Dias As DIAS, InteresTl As INTERESTL                                                " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                         " & vbCrLf &
"    where Descripcion Like '%TOTALES%';                                                 " & vbCrLf &
"    RESTABLE2 = SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";      " & vbCrLf &
"    RESTABLE3 = SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" as A     " & vbCrLf &
"    where A.Descripcion like '%TOTALES%';                                               " & vbCrLf &
"    DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                     " & vbCrLf &
"    DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                  " & vbCrLf &
" END;"

        Return res
    End Function



    '    Public Const SP_CALCULO_PAGO_HANA_NAME As String = "SP_CALCULO_PAGO_HANA"
    '    Public Shared Function SP_CALCULO_PAGO_HANA_TEXT() As String
    '        Dim res As String
    '        res = "CREATE Procedure """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_PAGO_HANA"" " & vbCrLf &
    '"(  " & vbCrLf &
    '"V_Capital Decimal(13,2)           " & vbCrLf &
    '",V_Cuotas smallint                " & vbCrLf &
    '",V_Tasa Decimal(8,4)               " & vbCrLf &
    '",V_Lapso smallint             " & vbCrLf &
    '",V_Fecha_Ini Date         " & vbCrLf &
    '",V_Cap_Pagado Decimal(13,2)     " & vbCrLf &
    '",V_Cap_Anticipo Decimal(13,2)    " & vbCrLf &
    '",V_Tasa_Vcdo Decimal(8,4)        " & vbCrLf &
    '",V_Tasa_Multa  Decimal(8,4)          " & vbCrLf &
    '",V_Fecha_UltPago Date      " & vbCrLf &
    '",V_Fecha_pago  Date       " & vbCrLf &
    '",V_Fecha_valor Date       " & vbCrLf &
    '",CuotaFija  Char(1) " & vbCrLf &
    '",V_GastosA Decimal(13,2)  " & vbCrLf &
    '",V_Original  tinyint           " & vbCrLf &
    '",V_error_msg varchar  " & vbCrLf &
    '",OUT RESTABLE2 """ & SQLBaseDatos.ToUpper & """.""CALPAGOTYPE""  " & vbCrLf &
    '",OUT RESTABLE3 """ & SQLBaseDatos.ToUpper & """.""CALCPAGOTYPE""  " & vbCrLf &
    '")  " & vbCrLf &
    '"LANGUAGE SQLSCRIPT As  " & vbCrLf &
    '"V_A_capital                     Decimal(13,2);  " & vbCrLf &
    '"V_A_tasa                        Decimal(8,4);       " & vbCrLf &
    '"V_A_periodo                     smallint;  " & vbCrLf &
    '"V_A_nro_cuotas                  smallint;  " & vbCrLf &
    '"V_O_cuota_fija                  Decimal(13,2);  " & vbCrLf &
    '"V_A_saldo                       Decimal(13,2);  " & vbCrLf &
    '"V_A_plazo_aux                   smallint;  " & vbCrLf &
    '"V_A_fecha_Ini_aux               Date;  " & vbCrLf &
    '"V_A_Fecha_hasta                 Date;  " & vbCrLf &
    '"V_A_Cap_pagado                  Decimal(13,2);  " & vbCrLf &
    '"V_A_Interes                     Decimal(13,2);  " & vbCrLf &
    '"V_Tot_Amortizado                Decimal(13,2);  " & vbCrLf &
    '"V_Por_pagar                     Decimal(13,2);  " & vbCrLf &
    '"V_Contador                      smallint;  " & vbCrLf &
    '"V_error_exec                    smallint;  " & vbCrLf &
    '"V_A_cuota_cap                   Decimal(12,2);  " & vbCrLf &
    '"V_aux1 						 Decimal(10,4);  " & vbCrLf &
    '"V_auxdate1						 Date;  " & vbCrLf &
    '"V_Sald                          int;  " & vbCrLf &
    '"V_Fecha_desd                    Date;  " & vbCrLf &
    '"V_Fecha_hast                    Date;  " & vbCrLf &
    '"V_tas                           Decimal (10,4);  " & vbCrLf &
    '"V_error_mss                     varchar(100);  " & vbCrLf &
    '"V_Inter                         Decimal(13,2);   " & vbCrLf &
    '"V_err                           int;   " & vbCrLf &
    '"V_Dias_mor                      smallint;   " & vbCrLf &
    '"V_Cap_Mor                       Decimal(13,2);   " & vbCrLf &
    '"V_Interes_Mor                   Decimal(13,2);  " & vbCrLf &
    '"V_str1                          VARCHAR;  " & vbCrLf &
    '"V_str2                          VARCHAR;  " & vbCrLf &
    '"A_Ajuste                        Decimal(13,2); " & vbCrLf &
    '"A_diferencia                    Decimal(13, 2); " & vbCrLf &
    '"A_Fin                           tinyint; " & vbCrLf &
    '"A_aux                           Decimal(13, 2); " & vbCrLf &
    '"A_Tot_Cuota1                    Decimal(13, 2); " & vbCrLf &
    '"A_Tot_Cuota2                    Decimal(13, 2); " & vbCrLf &
    '"BEGIN  " & vbCrLf &
    '"If V_Capital <= 0 Then   " & vbCrLf &
    '"V_error_mss = 'Capital ingresado inválido, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Cuotas <= 0 then   " & vbCrLf &
    '"V_error_mss = 'Cuotas ingresadas inválido, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Tasa < 0 then   " & vbCrLf &
    '"V_error_mss = 'Tasa ingresada inválida, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Lapso <= 0 then   " & vbCrLf &
    '"V_error_mss = 'Lapso ingresado inválida, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Cap_Pagado < 0 or V_Cap_Pagado > V_Capital then   " & vbCrLf &
    '"V_error_mss = 'Capital Pagado no puede ser menor que 0 o mayor que el Capital, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Cap_Anticipo < 0 or V_Cap_Anticipo >= V_Capital then   " & vbCrLf &
    '"V_error_mss = 'Capital Anticipo no puede ser menor que 0 o mayor/igual que el Capital, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Tasa_Vcdo < 0 then   " & vbCrLf &
    '"V_error_mss = 'Tasa Vcdo no puede ser menor que 0, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Tasa_Multa < 0 then   " & vbCrLf &
    '"V_error_mss = 'Tasa Multa no puede ser menor que 0, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Fecha_pago <= V_Fecha_Ini then   " & vbCrLf &
    '"V_error_mss = 'Fecha Pago no puede ser menor o igual Fecha Inicio, verifique.';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_Fecha_valor is null then    " & vbCrLf &
    '"V_Fecha_valor = V_Fecha_pago;    " & vbCrLf &
    '"end if;    " & vbCrLf &
    '"if V_Fecha_valor > V_Fecha_pago then    " & vbCrLf &
    '"V_error_mss = 'Fecha valor no puede ser mayor a fecha sistema o pago, verifique.';  " & vbCrLf &
    '"end if;   " & vbCrLf &
    '" if CuotaFija <> 'S' or CuotaFija <> 'N' then " & vbCrLf &
    '"V_error_mss = 'Cuota Fija solo pemite los valores S o N, verifique.';  " & vbCrLf &
    '"end IF; " & vbCrLf &
    '" if V_GastosA is null then " & vbCrLf &
    '"   V_GastosA = 0;  " & vbCrLf &
    '" end if;  " & vbCrLf &
    '"V_A_capital       = V_Capital;  " & vbCrLf &
    '"V_A_tasa          = V_Tasa;  " & vbCrLf &
    '"V_A_nro_cuotas    = V_Cuotas;     " & vbCrLf &
    '"V_A_periodo       = V_Cuotas * V_Lapso;  " & vbCrLf &
    '"V_A_saldo         = V_Capital - ifnull(V_Cap_Anticipo, 0);  " & vbCrLf &
    '"V_A_plazo_aux     = V_Lapso;             " & vbCrLf &
    '"V_A_fecha_Ini_aux = V_Fecha_ini;  " & vbCrLf &
    '"V_Tot_Amortizado  = ifnull(V_Cap_Anticipo, 0);  " & vbCrLf &
    '"V_O_cuota_fija    = 0;  " & vbCrLf &
    '"V_Contador        = 0;  " & vbCrLf &
    '"A_Fin = 1; " & vbCrLf &
    '"A_diferencia = 0; " & vbCrLf &
    '"A_Ajuste = 0; " & vbCrLf &
    '"CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" (  " & vbCrLf &
    '"Mes smallint  " & vbCrLf &
    '",Fecha_Pago date  " & vbCrLf &
    '",Saldo_capital decimal(13,2)  " & vbCrLf &
    '",Dias smallint  " & vbCrLf &
    '",Interes decimal(13,2)  " & vbCrLf &
    '",Amort_Cap decimal(13,2)  " & vbCrLf &
    '",Total_Cuota decimal(13,2)  " & vbCrLf &
    '",Por_Pagar_Cap   decimal(13,2)  " & vbCrLf &
    '",Total_Cap_Amort decimal(13,2)  " & vbCrLf &
    '",Cubierta   tinyint  " & vbCrLf &
    '");  " & vbCrLf &
    '"CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" (  " & vbCrLf &
    '"Descripcion varchar(30)   " & vbCrLf &
    '",Fecha_Pago date  " & vbCrLf &
    '",Fecha_cuota date  " & vbCrLf &
    '",Fecha_valor date  " & vbCrLf &
    '",DiasCuot smallint  " & vbCrLf &
    '",Interes decimal(13,2)  " & vbCrLf &
    '",DiasIntMor  smallint  " & vbCrLf &
    '",IntPorMor decimal(13,2)  " & vbCrLf &
    '",Multa decimal(13,2)  " & vbCrLf &
    '",Amort_Cap decimal(13,2)  " & vbCrLf &
    '",GastosA decimal(13,2)  " & vbCrLf &
    '",Total_Cuota decimal(13,2)  " & vbCrLf &
    '");  " & vbCrLf &
    '"if V_Cap_Anticipo > 0 then   " & vbCrLf &
    '"V_Por_pagar = 0;  " & vbCrLf &
    '"insert into  """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador, V_Fecha_Ini, 0, 0, 0, V_Cap_Anticipo, V_Cap_Anticipo,V_Por_pagar, V_Cap_Anticipo, 1);  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"A_Fin = 1; " & vbCrLf &
    '"While A_Fin <> 0 " & vbCrLf &
    '" Do " & vbCrLf &
    '"V_A_capital = V_Capital; " & vbCrLf &
    '"V_A_tasa = V_Tasa;    " & vbCrLf &
    '"V_A_nro_cuotas = V_Cuotas;   " & vbCrLf &
    '"V_A_periodo = V_Cuotas * V_Lapso; " & vbCrLf &
    '"V_A_saldo = V_Capital - ifnull(V_Cap_Anticipo, 0); " & vbCrLf &
    '"V_A_plazo_aux = V_Lapso;     " & vbCrLf &
    '"V_A_fecha_Ini_aux = V_Fecha_ini; " & vbCrLf &
    '"V_Tot_Amortizado = ifnull(V_Cap_Anticipo, 0); " & vbCrLf &
    '"V_aux1 = 0; " & vbCrLf &
    '"V_Contador = 0; " & vbCrLf &
    '"If CuotaFija = 'S' and V_Tasa <> 0 then " & vbCrLf &
    '"   A_aux = V_A_saldo + A_Ajuste; " & vbCrLf &
    '"   Call """ & SQLBaseDatos.ToUpper & """.""SP_CUOTA_FIJA_HANA""(A_aux, V_Cuotas, V_Tasa, V_Lapso, V_Fecha_Ini, V_aux1, V_error_mss); " & vbCrLf &
    '"   Select V_aux1 into V_O_cuota_fija from Dummy; " & vbCrLf &
    '"Else " & vbCrLf &
    '"    V_O_cuota_fija = round(V_A_saldo / V_Cuotas, 2);   " & vbCrLf &
    '"End If;   " & vbCrLf &
    '"Select V_Fecha_Ini into V_A_fecha_Ini_aux from Dummy;  " & vbCrLf &
    '"V_A_Fecha_hasta = ADD_DAYS(V_A_fecha_Ini_aux,V_Lapso);  " & vbCrLf &
    '"V_A_plazo_aux = DAYS_BETWEEN(V_A_fecha_Ini_aux, V_A_Fecha_hasta);  " & vbCrLf &
    '"If V_Cuotas > 1 Then  " & vbCrLf &
    '"If Mod(V_lapso, 30) = 0 Then  " & vbCrLf &
    '"Select 	add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) * (V_lapso/ 30) ) ) into V_A_Fecha_hasta   " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1) And  " & vbCrLf &
    '"DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = 1;  " & vbCrLf &
    '"Select DAYS_BETWEEN (add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") ) * (V_lapso/ 30) ) )  " & vbCrLf &
    '", add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) * (V_lapso/ 30) ) ))   into V_A_Plazo_aux  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1) And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = 1;  " & vbCrLf &
    '"Else  " & vbCrLf &
    '"Select  add_days(V_Fecha_Ini , V_lapso * (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) ) into V_A_Fecha_hasta  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1) And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = 1;  " & vbCrLf &
    '"Select V_lapso into V_A_Plazo_Aux  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1) And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = 1;  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"V_Sald        = V_A_saldo;  " & vbCrLf &
    '"V_Fecha_desd  = V_A_fecha_Ini_aux;  " & vbCrLf &
    '"V_Fecha_hast =V_A_Fecha_hasta;  " & vbCrLf &
    '"V_tas        = V_A_tasa;  " & vbCrLf &
    '"V_Inter      = V_A_Interes ;  " & vbCrLf &
    '"V_error_mss    = '';  " & vbCrLf &
    '"V_err 			= 0;  " & vbCrLf &
    '"CALL   """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (V_Sald,V_Fecha_desd,V_Fecha_hast,V_tas,V_Inter,V_error_mss,V_err);  " & vbCrLf &
    '"select V_Inter into V_A_Interes from dummy;   " & vbCrLf &
    '"  if CuotaFija = 'S' then " & vbCrLf &
    '"      V_A_cuota_cap = V_O_cuota_fija - V_A_Interes;  " & vbCrLf &
    '"  else  " & vbCrLf &
    '"      V_A_cuota_cap = V_O_cuota_fija;  " & vbCrLf &
    '"  end if;  " & vbCrLf &
    '"V_Tot_Amortizado = V_Tot_Amortizado + V_A_cuota_cap;  " & vbCrLf &
    '"V_Por_pagar = V_A_cuota_cap;  " & vbCrLf &
    '"select V_A_Fecha_hasta INTO V_A_fecha_Ini_aux FROM DUMMY;  " & vbCrLf &
    '"V_A_Cap_pagado = V_Cap_pagado;  " & vbCrLf &
    '"if V_A_Cap_pagado > 0 then  " & vbCrLf &
    '"V_Contador =V_Contador + 1;  " & vbCrLf &
    '"if V_Original = 1 then  " & vbCrLf &
    '"if V_A_Cap_pagado < V_A_cuota_cap then   " & vbCrLf &
    '"V_Por_pagar = V_A_cuota_cap - V_A_Cap_pagado;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador, V_A_fecha_Ini_aux, V_A_saldo, V_A_plazo_aux, 0,  V_A_cuota_cap , (V_A_cuota_cap+V_A_Interes), V_Por_pagar, V_Tot_Amortizado,  0);  " & vbCrLf &
    '"Else   " & vbCrLf &
    '"V_Por_pagar = 0;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador, V_A_fecha_Ini_aux, V_A_saldo, V_A_plazo_aux, 0,  V_A_cuota_cap, (V_A_cuota_cap + V_A_Interes), V_Por_pagar, V_Tot_Amortizado,  1);  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"End If;   " & vbCrLf &
    '"V_A_Cap_pagado = V_A_Cap_pagado - V_A_cuota_cap;  " & vbCrLf &
    '"Else   " & vbCrLf &
    '"If V_Tot_Amortizado <> V_Capital And V_Cuotas = 1 Then  " & vbCrLf &
    '"Select V_A_cuota_cap - (V_Tot_Amortizado - V_Capital) into V_A_cuota_cap from Dummy;  " & vbCrLf &
    '"Select V_Tot_Amortizado - (V_Tot_Amortizado - V_Capital) into V_Tot_Amortizado from Dummy;  " & vbCrLf &
    '"Select	V_A_cuota_cap into V_Por_pagar from Dummy;  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(1, V_A_fecha_Ini_aux, V_A_saldo, V_A_plazo_aux, V_A_Interes, V_A_cuota_cap, V_A_cuota_cap + V_A_Interes, V_Por_pagar, V_Tot_Amortizado, 0);  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"V_Contador= 2;  " & vbCrLf &
    '"While V_Contador <= V_A_nro_cuotas   " & vbCrLf &
    '"Do		    " & vbCrLf &
    '"V_A_saldo = V_A_saldo - V_A_cuota_cap;  " & vbCrLf &
    '"V_A_plazo_aux = V_A_plazo_aux + V_Lapso;  " & vbCrLf &
    '"V_A_Fecha_hasta = ADD_DAYS(V_A_fecha_Ini_aux,V_Lapso);  " & vbCrLf &
    '"If Mod(V_lapso, 30) = 0 Then  " & vbCrLf &
    '"Select 	add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) * (V_lapso/ 30) ) ) into V_A_Fecha_hasta   " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1) And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = V_Contador;  " & vbCrLf &
    '"Select DAYS_BETWEEN (add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") ) * (V_lapso/ 30) ) )  " & vbCrLf &
    '", add_months(V_Fecha_Ini ,  ( (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) * (V_lapso/ 30) ) ))   into V_A_Plazo_aux  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1)  And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = V_Contador;  " & vbCrLf &
    '"Else  " & vbCrLf &
    '"Select  add_days(V_Fecha_Ini , V_lapso * (DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1) ) into V_A_Fecha_hasta  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1)  And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = V_Contador;  " & vbCrLf &
    '"Select V_lapso into V_A_Plazo_Aux  " & vbCrLf &
    '"FROM _SYS_BI.M_TIME_DIMENSION  " & vbCrLf &
    '"WHERE ""DATE_SAP"" BETWEEN V_Fecha_Ini And add_days(V_Fecha_Ini , V_Cuotas -1)  And DAYS_BETWEEN(V_Fecha_Ini, ""DATE_SAP"") +1 = V_Contador;  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"V_Sald        = V_A_saldo;  " & vbCrLf &
    '"V_Fecha_desd = V_A_fecha_Ini_aux;  " & vbCrLf &
    '"V_Fecha_hast  = V_A_Fecha_hasta;  " & vbCrLf &
    '"V_Tas        = V_A_tasa;  " & vbCrLf &
    '"V_Inter      = V_A_Interes;  " & vbCrLf &
    '"V_error_mss   = '';  " & vbCrLf &
    '"V_err 			= 0;  " & vbCrLf &
    '"CALL   """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA""    " & vbCrLf &
    '"(  " & vbCrLf &
    '"V_Sald ,  " & vbCrLf &
    '"V_Fecha_desd,  " & vbCrLf &
    '"V_Fecha_hast,  " & vbCrLf &
    '"V_tas,  " & vbCrLf &
    '"V_Inter,  " & vbCrLf &
    '"V_error_mss,  " & vbCrLf &
    '"V_err  " & vbCrLf &
    '");  " & vbCrLf &
    '"select V_Inter into V_A_Interes from Dummy;  " & vbCrLf &
    '"select V_A_Fecha_hasta into V_A_fecha_Ini_aux from Dummy;  " & vbCrLf &
    '"  if CuotaFija = 'S' then " & vbCrLf &
    '"      V_A_cuota_cap = V_O_cuota_fija - V_A_Interes;  " & vbCrLf &
    '"  else  " & vbCrLf &
    '"      V_A_cuota_cap = V_O_cuota_fija;  " & vbCrLf &
    '"  end if;  " & vbCrLf &
    '"V_Tot_Amortizado = V_Tot_Amortizado + V_A_cuota_cap;  " & vbCrLf &
    '"V_Por_pagar = V_A_cuota_cap;  " & vbCrLf &
    '"if V_Tot_Amortizado <> V_Capital and V_Contador = V_Cuotas then   " & vbCrLf &
    '"select V_A_cuota_cap - (V_Tot_Amortizado - V_Capital) into V_A_cuota_cap from Dummy;   " & vbCrLf &
    '"select V_Tot_Amortizado - (V_Tot_Amortizado - V_Capital) into V_Tot_Amortizado from Dummy;  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"if V_A_Cap_pagado > 0 then   " & vbCrLf &
    '"if V_Original = 1 then	  " & vbCrLf &
    '"if V_A_Cap_pagado < V_A_cuota_cap then  " & vbCrLf &
    '"V_Por_pagar = V_A_cuota_cap - V_A_Cap_pagado;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador, V_A_fecha_Ini_aux,V_A_saldo, V_A_plazo_aux, 0, V_A_cuota_cap, (V_A_cuota_cap+V_A_Interes), V_Por_pagar, V_Tot_Amortizado, 0);  " & vbCrLf &
    '"else  " & vbCrLf &
    '"V_Por_pagar = 0;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador,V_A_fecha_Ini_aux, V_A_saldo, V_A_plazo_aux, 0,  V_A_cuota_cap, (V_A_cuota_cap+ V_A_Interes), V_Por_pagar, V_Tot_Amortizado,  1);  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"V_A_Cap_pagado = V_A_Cap_pagado - V_A_cuota_cap;  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"else   " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" values(V_Contador,V_A_fecha_Ini_aux, V_A_saldo, V_A_plazo_aux, V_A_Interes, V_A_cuota_cap, (V_A_cuota_cap+V_A_Interes), V_Por_pagar, V_Tot_Amortizado,  0);  " & vbCrLf &
    '"END IF;  " & vbCrLf &
    '"V_Contador=V_Contador+1;  " & vbCrLf &
    '"END WHILE;   " & vbCrLf &
    '"select Total_Cuota into A_Tot_Cuota1 from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes =  V_Contador-1; " & vbCrLf &
    '"Select Total_Cuota into A_Tot_Cuota2 from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes =  V_Contador-2; " & vbCrLf &
    '"A_diferencia = A_Tot_Cuota1 - A_Tot_Cuota2; " & vbCrLf &
    '"If abs(A_diferencia) > 0 And CuotaFija = 'S' then  " & vbCrLf &
    '"  If A_Ajuste > 0 Then " & vbCrLf &
    '"     A_Fin = 0; " & vbCrLf &
    '"  End If; " & vbCrLf &
    '"  A_Ajuste = A_Ajuste + (abs(A_diferencia) / power((1.0000 + V_Tasa / 1200.0000), V_Cuotas)); " & vbCrLf &
    '"  If A_Fin <> 0 Then " & vbCrLf &
    '"     delete from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Mes <> 0; " & vbCrLf &
    '"  End If; " & vbCrLf &
    '"  Else " & vbCrLf &
    '"     A_Fin = 0; " & vbCrLf &
    '"End If; " & vbCrLf &
    '"End While; " & vbCrLf &
    '"V_Interes_Mor = 0;  " & vbCrLf &
    '"V_Dias_mor = 0;  " & vbCrLf &
    '"Select min(Fecha_Pago) into V_auxdate1 from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Cubierta = 0 ;  " & vbCrLf &
    '"If V_Fecha_pago > V_auxdate1 And V_Tasa_Multa > 0 Then   " & vbCrLf &
    '"Select ifnull(sum(DAYS_BETWEEN(fecha_pago, V_Fecha_pago)),0) into V_Dias_mor  from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Cubierta = 0 And V_Fecha_Pago <= V_Fecha_pago;  " & vbCrLf &
    '"Select ifnull(sum(Amort_Cap),0) into V_Cap_Mor from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" where Cubierta = 0 And V_Fecha_Pago <= Fecha_pago;  " & vbCrLf &
    '"V_Interes_Mor =  V_Tasa_Multa * V_Dias_mor;  " & vbCrLf &
    '"End If;  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" (  " & vbCrLf &
    '"Select 'Mes'||TO_VARCHAR(Mes),   " & vbCrLf &
    '"V_Fecha_pago,   " & vbCrLf &
    '"ifnull(Fecha_Pago,V_Fecha_Ini) ,   " & vbCrLf &
    '"V_Fecha_valor,   " & vbCrLf &
    '"ifnull(Dias,0),   " & vbCrLf &
    '"0,   " & vbCrLf &
    '"ifnull(DAYS_BETWEEN(fecha_pago, V_Fecha_pago),0),   " & vbCrLf &
    '"case when V_Tasa_Vcdo > 0 and ifnull(DAYS_BETWEEN(fecha_pago, V_Fecha_pago),0) > 0   " & vbCrLf &
    '"then 0 " & vbCrLf &
    '"else 0 end,  " & vbCrLf &
    '"round(((V_Tasa_Multa/100)/360)   " & vbCrLf &
    '"* ((case when V_Tasa_Vcdo > 0 and ifnull(DAYS_BETWEEN(fecha_pago, V_Fecha_pago),0) > 0   " & vbCrLf &
    '"then  (round(V_Tasa_Vcdo / round(360 / V_Lapso, 0),0 ) / 100) * Por_Pagar_Cap  " & vbCrLf &
    '"else 0 end) + Por_Pagar_Cap ) , 0)  " & vbCrLf &
    '"* ifnull(DAYS_BETWEEN(fecha_pago, V_Fecha_pago),0) ,  " & vbCrLf &
    '"ifnull(Por_Pagar_Cap, 0) , -- Capital por pagar   " & vbCrLf &
    '"0, -- Gastos Adicionales  " & vbCrLf &
    '"0  " & vbCrLf &
    '"from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""  " & vbCrLf &
    '"where Cubierta = 0   " & vbCrLf &
    '"and Fecha_Pago <= V_Fecha_pago  " & vbCrLf &
    '");  " & vbCrLf &
    '"insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" (  " & vbCrLf &
    '"select 'TOTALES ',V_Fecha_pago,   " & vbCrLf &
    '"ifnull(min(Fecha_Cuota),V_Fecha_Ini) ,  " & vbCrLf &
    '"V_Fecha_Valor,  " & vbCrLf &
    '"ifnull(sum(DiasCuot),0),   " & vbCrLf &
    '"ifnull(sum(Interes),0),   " & vbCrLf &
    '"ifnull(sum(DiasIntMor),0),   " & vbCrLf &
    '"ifnull(sum(IntPorMor),0),   " & vbCrLf &
    '"ifnull(sum(Multa), 0),  " & vbCrLf &
    '"ifnull(sum(Amort_Cap),0),  " & vbCrLf &
    '"ifnull(V_GastosA,0),  " & vbCrLf &
    '"ifnull(sum(Amort_Cap),0)  " & vbCrLf &
    '"from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""  " & vbCrLf &
    '");  " & vbCrLf &
    '" -- Calculo de interes Normal Diario  " & vbCrLf &
    '"if V_Tasa > 0 then  " & vbCrLf &
    '"V_A_saldo = V_Capital - V_Cap_Anticipo - V_Cap_pagado;  " & vbCrLf &
    '"V_A_fecha_Ini_aux = V_Fecha_UltPago;  " & vbCrLf &
    '"V_A_Fecha_hasta = V_Fecha_valor;  " & vbCrLf &
    '"V_Inter      = V_A_Interes;  " & vbCrLf &
    '"V_error_mss   = '';  " & vbCrLf &
    '"V_err 	= 0;  " & vbCrLf &
    '"CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (V_A_saldo, V_A_fecha_Ini_aux, V_A_Fecha_hasta, V_Tasa, V_Inter, V_error_mss, V_err );   " & vbCrLf &
    '"select V_Inter into V_A_Interes from Dummy;  " & vbCrLf &
    '"update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""  " & vbCrLf &
    '"set Interes = V_A_Interes,  " & vbCrLf &
    '"Total_Cuota = V_A_Interes + IntPorMor + Multa + Amort_Cap + V_GastosA " & vbCrLf &
    '"where Descripcion like '%TOTALES%' ;  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"-- Calculo Interes Por Mora " & vbCrLf &
    '"select min(Fecha_cuota) into V_A_fecha_Ini_aux" & vbCrLf &
    '"from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" ;  " & vbCrLf &
    '" if V_Tasa_Vcdo > 0 and  " & vbCrLf &
    '"V_A_fecha_Ini_aux < V_Fecha_pago then  " & vbCrLf &
    '"V_A_saldo = V_Capital - V_Cap_Anticipo - V_Cap_pagado;  " & vbCrLf &
    '"V_A_fecha_Ini_aux = V_Fecha_UltPago;  " & vbCrLf &
    '"V_A_Fecha_hasta = V_Fecha_valor;  " & vbCrLf &
    '"V_Inter = V_A_Interes;  " & vbCrLf &
    '"V_error_mss  = '';  " & vbCrLf &
    '"V_err = 0;  " & vbCrLf &
    '"CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA"" (V_A_saldo, V_A_fecha_Ini_aux,	V_A_Fecha_hasta, V_Tasa_Vcdo, V_Inter, V_error_mss, V_err );  " & vbCrLf &
    '"select V_Inter into V_A_Interes from Dummy;  " & vbCrLf &
    '"update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""  " & vbCrLf &
    '"set IntPorMor = V_A_Interes,  " & vbCrLf &
    '"DiasIntMor = ifnull(DAYS_BETWEEN(V_Fecha_UltPago, V_Fecha_valor),0),  " & vbCrLf &
    '"Total_Cuota = Interes + V_A_Interes + Multa + Amort_Cap + V_GastosA " & vbCrLf &
    '"where Descripcion like '%TOTALES%';  " & vbCrLf &
    '"end if;  " & vbCrLf &
    '"RESTABLE2 = SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";" & vbCrLf &
    '"RESTABLE3 = SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" as A " & vbCrLf &
    '"where A.Descripcion like '%TOTALES%';" & vbCrLf &
    '"DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";  " & vbCrLf &
    '"DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";  " & vbCrLf &
    '"END;"

    '        Return res
    '    End Function
    Public Const SP_CALCULO_INTERES_MORA_HANA_NAME As String = "SP_CALCULO_INTERES_MORA_HANA"
    Public Shared Function SP_CALCULO_INTERES_MORA_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_MORA_HANA""(  " & vbCrLf &
"V_FechaPago Date,   " & vbCrLf &
"V_FechaVencimiento date,    " & vbCrLf &
"V_Tipo_Periodo nvarchar(1),  " & vbCrLf &
"V_InteresTasa decimal(18,2),  " & vbCrLf &
"V_MontoPendienteDePago Decimal(18,2),  " & vbCrLf &
"V_TasaAnualMoratoria decimal(18,2),  " & vbCrLf &
"V_DiasParaTasaAnualMoratoria Integer  " & vbCrLf &
")  " & vbCrLf &
"LANGUAGE SQLSCRIPT As  " & vbCrLf &
"V_InteresTasaCalculada  decimal;  " & vbCrLf &
"V_DiasEnMora  Integer;  " & vbCrLf &
"V_TasaAnualMoratoriaCalculada  decimal(18,6);  " & vbCrLf &
"V_TotalInteres  Decimal(18,2);  " & vbCrLf &
"V_TotalCapitalMasInteres  decimal(18,2);  " & vbCrLf &
"V_TotalMora Decimal(18,2);   " & vbCrLf &
"V_TotalGeneral  decimal(18,2);  " & vbCrLf &
"V_TotalAFacturar decimal(18,2); " & vbCrLf &
"BEGIN   " & vbCrLf &
"V_InteresTasaCalculada  = 0;  " & vbCrLf &
"V_DiasEnMora = 0;  " & vbCrLf &
"V_TasaAnualMoratoriaCalculada  = 0;  " & vbCrLf &
"V_TotalInteres  = 0;  " & vbCrLf &
"V_TotalCapitalMasInteres  = 0;  " & vbCrLf &
"V_TotalMora  = 0;  " & vbCrLf &
"V_TotalGeneral  = 0;  " & vbCrLf &
"V_TotalAFacturar =0; " & vbCrLf &
"If V_FechaPago > V_FechaVencimiento Then  " & vbCrLf &
"If(V_Tipo_Periodo ='S') THEN  " & vbCrLf &
"V_InteresTasaCalculada =  V_InteresTasa/2;  " & vbCrLf &
"END IF;  " & vbCrLf &
"If(V_Tipo_Periodo ='M') THEN  " & vbCrLf &
"V_InteresTasaCalculada =  V_InteresTasa/12;  " & vbCrLf &
"End If;  " & vbCrLf &
"V_DiasEnMora = DAYS_BETWEEN(V_FechaVencimiento,V_FechaPago);  " & vbCrLf &
"End If;  " & vbCrLf &
"V_TotalInteres = V_MontoPendienteDePago *  V_InteresTasaCalculada;  " & vbCrLf &
"V_TotalCapitalMasInteres = (V_MontoPendienteDePago + V_TotalInteres);  " & vbCrLf &
"V_TasaAnualMoratoriaCalculada = V_TasaAnualMoratoria/ V_DiasParaTasaAnualMoratoria;  " & vbCrLf &
"V_TotalMora = V_TotalCapitalMasInteres * V_TasaAnualMoratoriaCalculada * V_DiasEnMora;  " & vbCrLf &
"V_TotalGeneral = V_TotalCapitalMasInteres + V_TotalMora;  " & vbCrLf &
"V_TotalAFacturar = V_TotalInteres + V_TotalMora;" & vbCrLf &
"Select  V_MontoPendienteDePago As ""Capital"",   " & vbCrLf &
"V_InteresTasaCalculada as ""InteresAplicable"",    " & vbCrLf &
"V_TotalInteres As ""MontoInteresReal"" ,    " & vbCrLf &
"V_TotalCapitalMasInteres  as ""TotalCapMasInteres"",   " & vbCrLf &
"V_DiasEnMora As ""DiasMora"",  " & vbCrLf &
"V_TotalMora as ""MontoDeInteresPorMora"",  " & vbCrLf &
"V_TotalGeneral As ""TotalGeneral"",   " & vbCrLf &
"V_TotalAFacturar AS ""TotalAFacturar"" from Dummy;  " & vbCrLf &
"END"

        Return res
    End Function


    Public Const SP_CUOTAS_X_PAGAR_BC_HANA_NAME As String = "SP_CUOTAS_X_PAGAR_BC_HANA"
    Public Shared Function SP_CUOTAS_X_PAGAR_BC_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CUOTAS_X_PAGAR_BC_HANA""(  " & vbCrLf &
"                 DocEntryOV  int          -- Orden de Venta                                     " & vbCrLf &
"                ,V_Fecha_pago  date       -- Fecha del pago a realizarce                        " & vbCrLf &
"                ,out error_msg  int                                                             " & vbCrLf &
"                ,out RESTABLE1 """ & SQLBaseDatos.ToUpper & """.""CALPAGOBC"" )                 " & vbCrLf &
"LANGUAGE SQLSCRIPT As                                                                           " & vbCrLf &
"BEGIN                                                                                           " & vbCrLf &
"   DECLARE CURSOR F_Orden_Venta FOR                                                             " & vbCrLf &
"     select  ""DocEntry"",                                                                      " & vbCrLf &
"             ""DocDate"",                                                                       " & vbCrLf &
"             ""U_DocCuota"",                                                                    " & vbCrLf &
"	          ""U_Interes"",                                                                     " & vbCrLf &
"			  ""U_IntMor"",                                                                      " & vbCrLf &
"			  ""CardCode""                                                                       " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta                 " & vbCrLf &
"   where CANCELED = 'N'                                                                         " & vbCrLf &
"     and ""DocStatus"" = 'O'                                                                    " & vbCrLf &
"     and ""DocEntry"" = (case when ifnull(DocEntryOV,0) = 0 then ""DocEntry"" else ifnull(DocEntryOV,0) end) " & vbCrLf &
"	  and ""U_DocCuota"" is not null;                                                            " & vbCrLf &
"			 -- 0. Declaración de variables                                                      " & vbCrLf &
"			        Declare A_capital                     decimal(13,2);                         " & vbCrLf &
"					Declare A_tasa                        decimal(8,4);                          " & vbCrLf &
"					Declare A_nro_cuotas                  smallint;                              " & vbCrLf &
"					Declare A_saldo                       decimal(13,2);                         " & vbCrLf &
"					Declare A_plazo_aux                   smallint;                              " & vbCrLf &
"					Declare A_fecha_Ini_aux               date;                                  " & vbCrLf &
"					Declare A_Fecha_hasta                 date;                                  " & vbCrLf &
"					Declare A_Cap_pagado                  decimal(13,2);                         " & vbCrLf &
"					Declare A_Interes                     decimal(13,2);                         " & vbCrLf &
"					Declare Tot_Amortizado                decimal(13,2);                         " & vbCrLf &
"					Declare Por_pagar                     decimal(13,2);                         " & vbCrLf &
"					Declare Contador                      smallint;                              " & vbCrLf &
"					Declare error_exec                    smallint;                              " & vbCrLf &
"			        Declare A_cuota_cap                   decimal(12,2);                         " & vbCrLf &
"					Declare A_tl_Int_Carg                 decimal(13,2);                         " & vbCrLf &
"					Declare U_DocEntryPG                  int;                                   " & vbCrLf &
"					Declare Tasa                          decimal(8,4);                          " & vbCrLf &
"					Declare Tasa_Vcdo                     decimal(8,4);                          " & vbCrLf &
"					Declare U_CardCode                    varchar(50);                           " & vbCrLf &
"					Declare TasaNormalLegal               char(1);                               " & vbCrLf &
"					Declare FechaCuotaVen                 date;                                  " & vbCrLf &
"					Declare TotalImpMult                  decimal(13,2);                         " & vbCrLf &
"					Declare DocEntryOF                    int;                                   " & vbCrLf &
"					Declare Fecha_valor                   date;                                  " & vbCrLf &
"					Declare FechaUltPago                  date;                                  " & vbCrLf &
"					Declare Nombre                        nvarchar(100);                         " & vbCrLf &
"					Declare Identificacion                nvarchar(32);                          " & vbCrLf &
"					Declare Contador_aux                  int;                                   " & vbCrLf &
"					Declare V_Inter                       decimal(13,2);                         " & vbCrLf &
"			        Declare V_error_mss                   varchar(100);                          " & vbCrLf &
"                   Declare V_err                         int;                                   " & vbCrLf &
"                   Declare aux                           int;                                   " & vbCrLf &
"                   Declare TotalImpMult_aux              decimal(13,2);                         " & vbCrLf &
"			-----------------------------------------------------------------------              " & vbCrLf &
"			-- Asignacion de valores iniciales --                                                " & vbCrLf &
"			-----------------------------------------------------------------------              " & vbCrLf &
"			        A_tasa          = Tasa;                                                      " & vbCrLf &
"					Tot_Amortizado  = 0;                                                         " & vbCrLf &
"				    Contador        = 2;                                                         " & vbCrLf &
"					Contador_aux    = 1;                                                         " & vbCrLf &
"					A_nro_cuotas    = 0;                                                         " & vbCrLf &
"					Fecha_valor     = V_Fecha_pago;                                              " & vbCrLf &
"					TasaNormalLegal = 'N';                                                       " & vbCrLf &
"					TotalImpMult    = 0;                                                         " & vbCrLf &
"-- 1. Crear tabla amortización                                                                  " & vbCrLf &
"-----------------------------------------------------------------------------------             " & vbCrLf &
"			CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""  (  " & vbCrLf &
"			     Mes int                                                                         " & vbCrLf &
"				,Fecha_Pago date                                                                 " & vbCrLf &
"				,Saldo_capital decimal(13,2)                                                     " & vbCrLf &
"				,Dias smallint                                                                   " & vbCrLf &
"				,Interes decimal(13,2)                                                           " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                         " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                       " & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)                                                   " & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)                                                   " & vbCrLf &
"				,Cubierta   tinyint                                                              " & vbCrLf &
"				);                                                                               " & vbCrLf &
"		    CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" (  " & vbCrLf &
"				 Mes smallint                                                                    " & vbCrLf &
"				,Descripcion varchar(30)                                                         " & vbCrLf &
"				,Fecha_Pago date                                                                 " & vbCrLf &
"				,Fecha_cuota date                                                                " & vbCrLf &
"				,Fecha_valor date                                                                " & vbCrLf &
"				,DiasCuot smallint                                                               " & vbCrLf &
"				,Interes decimal(13,2)                                                           " & vbCrLf &
"				,DiasIntMor  smallint                                                            " & vbCrLf &
"				,IntPorMor decimal(13,2)                                                         " & vbCrLf &
"				,Multa decimal(13,2)                                                             " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                         " & vbCrLf &
"				,GastosA decimal(13,2)                                                           " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                       " & vbCrLf &
"				,Cubierta tinyint                                                                " & vbCrLf &
"				);                                                                               " & vbCrLf &
"			CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc"" ( " & vbCrLf &
"			     CodBc    varchar(50)                                                            " & vbCrLf &
"				,CodEntBc varchar(50)                                                            " & vbCrLf &
"				,DocEntryOV  varchar(15)                                                         " & vbCrLf &
"				,NroCuota    int                                                                 " & vbCrLf &
"				,Descripcion varchar(30)                                                         " & vbCrLf &
"				,Importe    decimal(13,2)                                                        " & vbCrLf &
"				,Fecha_Pago char(10)                                                             " & vbCrLf &
"				,Fecha_cuota date                                                                " & vbCrLf &
"				,Multa     decimal(13,2)                                                         " & vbCrLf &
"				,DiasMulta smallint                                                              " & vbCrLf &
"				,NombreRazonSocial  varchar(70)                                                  " & vbCrLf &
"				,Identificacion varchar(20)                                                      " & vbCrLf &
"				,Numero     int                                                                  " & vbCrLf &
"				,Agencia    varchar(30)                                                          " & vbCrLf &
"				,Datos      varchar(100)                                                         " & vbCrLf &
"				,Cubierta   tinyint                                                              " & vbCrLf &
"				);                                                                               " & vbCrLf &
"CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##resultado"" (                 " & vbCrLf &
"			     Linea varchar(2000)                                                             " & vbCrLf &
"				);                                                                               " & vbCrLf &
"For cursorRow as F_Orden_Venta                                                                  " & vbCrLf &
" do                                                                                             " & vbCrLf &
" DocEntryOV := cursorRow.""DocEntry"";                                                          " & vbCrLf &
" A_fecha_Ini_aux := cursorRow.""DocDate"";                                                      " & vbCrLf &
" U_DocEntryPG := cursorRow.""U_DocCuota"";                                                      " & vbCrLf &
" Tasa := cursorRow.""U_Interes"";                                                               " & vbCrLf &
" Tasa_Vcdo := cursorRow.""U_IntMor"";                                                           " & vbCrLf &
" U_CardCode := cursorRow.""CardCode"";                                                          " & vbCrLf &
" delete from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                               " & vbCrLf &
" delete from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                            " & vbCrLf &
"        Tot_Amortizado = 0;                                                                     " & vbCrLf &
"		A_Cap_pagado = 0;                                                                        " & vbCrLf &
"	    FechaUltPago = A_fecha_Ini_aux; -- OJO hay que obtener la fecha de ultimo pago           " & vbCrLf &
"        DocEntryOF = 0;                                                                         " & vbCrLf &
"        select count(""TrgetEntry"") into aux   -- DocEntry OINV Tabla Factura                  " & vbCrLf &
"          from """ & SQLBaseDatos.ToUpper & """.""RDR1"" -- Tabla Detalle Orden de Venta            " & vbCrLf &
"         where ""DocEntry"" = DocEntryOV -- DocEntry Orden de Venta                             " & vbCrLf &
"	        and ""TargetType"" = 13;                                                             " & vbCrLf &
"        select case when aux>0 then (select ""TrgetEntry""                                      " & vbCrLf &
"                                       from """ & SQLBaseDatos.ToUpper & """.""RDR1"" -- Tabla Detalle Orden de Venta " & vbCrLf &
"                                      where ""DocEntry"" = DocEntryOV -- DocEntry Orden de Venta " & vbCrLf &
"	                                     and ""TargetType"" = 13)                                " & vbCrLf &
"	                else 0 end into DocEntryOF from Dummy;  -- DocEntry OINV Tabla Factura       " & vbCrLf &
"        if ifnull(DocEntryOF,0) <> 0 then -- Si existe factura obtenemos lo total pagado        " & vbCrLf &
"           aux = 0;                                                                             " & vbCrLf &
"           select count(""PaidToDate"") into aux from """ & SQLBaseDatos.ToUpper & """.""OINV"" as A where ""DocEntry"" = DocEntryOF and ""CANCELED"" = 'N';  " & vbCrLf &
"          select case when aux>0 then (select ""PaidToDate""                                    " & vbCrLf &
"                                          from """ & SQLBaseDatos.ToUpper & """.""OINV"" as A       " & vbCrLf &
"                                         where ""DocEntry"" = DocEntryOF                          " & vbCrLf &
"                                           and ""CANCELED"" = 'N')                                " & vbCrLf &
"	                else 0 end into A_Cap_pagado from Dummy;                                     " & vbCrLf &
"        else                                                                                    " & vbCrLf &
"             -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta           " & vbCrLf &
"           aux = 0;                                                                             " & vbCrLf &
"           select sum(""DocTotal"") into aux  from """ & SQLBaseDatos.ToUpper & """.""ORCT"" where ""Canceled"" = 'N' and ""U_ORDV"" = DocEntryOV;   " & vbCrLf &
"           select case when aux>0 then (select ""DocTotal""                                     " & vbCrLf &
"                                          from """ & SQLBaseDatos.ToUpper & """.""ORCT""            " & vbCrLf &
"                                         where ""Canceled"" = 'N'                               " & vbCrLf &
"                                           and ""U_ORDV"" = DocEntryOV)                         " & vbCrLf &
"	                else 0 end into A_Cap_pagado from Dummy;                                     " & vbCrLf &
"        end if ;                                                                                " & vbCrLf &
"        A_Cap_pagado = ifnull(A_Cap_pagado, 0);                                                 " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                              " & vbCrLf &
"   select 0, ""U_FechaV"", 0, 0, 0,                                                             " & vbCrLf &
"          ""U_Amortizacion"" , ""U_Amortizacion"" , ""U_Amortizacion"",                         " & vbCrLf &
"          0, 0                                                                                  " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN""                                        " & vbCrLf &
"    where ""U_DocEntryPG"" = U_DocEntryPG                                                       " & vbCrLf &
"    order by ""U_FechaV"" asc;                                                                  " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""U_Intlgl"") into aux                                                          " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""                                       " & vbCrLf &
"   where ""U_CardCode"" = U_CardCode                                                            " & vbCrLf &
"     and ""Canceled"" = 'N';                                                                    " & vbCrLf &
"   select case when aux>0 then (select ""U_Intlgl""                                             " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""          " & vbCrLf &
"                                 where ""U_CardCode"" = U_CardCode                              " & vbCrLf &
"                                   and ""Canceled"" = 'N')                                      " & vbCrLf &
"	                else 'N' end into TasaNormalLegal from Dummy;                                " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""CardName"") into aux from """ & SQLBaseDatos.ToUpper & """.""OCRD"" where ""CardCode"" = U_CardCode;  " & vbCrLf &
"   select case when aux>0 then (select ""CardName""                                             " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""OCRD""                    " & vbCrLf &
"                                 where ""CardCode"" = U_CardCode)                               " & vbCrLf &
"	                else '' end into Nombre from Dummy;                                          " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""LicTradNum"") into aux from """ & SQLBaseDatos.ToUpper & """.""OCRD"" where ""CardCode"" = U_CardCode; " & vbCrLf &
"   select case when aux>0 then (select ""LicTradNum""                                           " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""OCRD""                    " & vbCrLf &
"                                 where ""CardCode"" = U_CardCode)                               " & vbCrLf &
"	                else '' end into Identificacion from Dummy;                                  " & vbCrLf &
"   if ifnull(TasaNormalLegal,'N') = 'N' then                                                    " & vbCrLf &
"      A_tasa = Tasa;                                                                            " & vbCrLf &
"   end if;                                                                                      " & vbCrLf &
"   select count(*) into A_nro_cuotas from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";  " & vbCrLf &
"   Select sum(Amort_Cap) into A_capital from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";   " & vbCrLf &
"   select min(Fecha_Pago) into A_Fecha_hasta from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""; " & vbCrLf &
"   Contador = 1;                                                                                " & vbCrLf &
"   while Contador <= A_nro_cuotas                                                               " & vbCrLf &
"   Do                                                                                           " & vbCrLf &
"      select min(Fecha_Pago) into A_Fecha_hasta                                                 " & vbCrLf &
"        from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                " & vbCrLf &
"       where Mes = 0;                                                                           " & vbCrLf &
"	  update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                 " & vbCrLf &
"	      set Mes = Contador                                                                     " & vbCrLf &
"	 where Fecha_Pago = A_Fecha_hasta;                                                           " & vbCrLf &
"      A_saldo = A_capital - Tot_Amortizado;                                                     " & vbCrLf &
"     CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA""                          " & vbCrLf &
"             ( A_saldo ,                                                                        " & vbCrLf &
"               A_fecha_Ini_aux,                                                                 " & vbCrLf &
"               A_Fecha_hasta,                                                                   " & vbCrLf &
"               A_tasa,                                                                          " & vbCrLf &
"               A_Interes,                                                                       " & vbCrLf &
"               V_error_mss,                                                                     " & vbCrLf &
"               V_err                                                                            " & vbCrLf &
"               );                                                                               " & vbCrLf &
"   select V_Inter into A_Interes from Dummy;                                                    " & vbCrLf &
"   update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"      set Saldo_capital = A_capital - Tot_Amortizado,                                           " & vbCrLf &
"	      Dias = ifnull(DAYS_BETWEEN(A_fecha_Ini_aux, Fecha_Pago),0),                            " & vbCrLf &
"		  Interes = A_Interes,                                                                   " & vbCrLf &
"		  Total_Cuota = Amort_Cap + A_Interes,                                                   " & vbCrLf &
"		  Por_Pagar_Cap = (case when Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 0 else case when (A_Cap_pagado - Tot_Amortizado) > 0 then  Amort_Cap - (A_Cap_pagado - Tot_Amortizado) else Amort_Cap end end), " & vbCrLf &
"		  Total_Cap_Amort = Tot_Amortizado + Amort_Cap,                                          " & vbCrLf &
"		  Cubierta = (case when Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 1 else 0 end)    " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"   select Tot_Amortizado + Amort_Cap into Tot_Amortizado                                        " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"   select Fecha_Pago into A_fecha_Ini_aux                                                       " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"    Contador = Contador + 1;                                                                    " & vbCrLf &
"   end while; -- while @Contador <= @A_nro_cuotas                                               " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                           " & vbCrLf &
"   select Mes,                                                                                  " & vbCrLf &
"          concat('PAGO DE CUOTA No. ', TO_VARCHAR(Mes)),                                        " & vbCrLf &
"          V_Fecha_pago,                                                                         " & vbCrLf &
"		   Fecha_Pago,                                                                           " & vbCrLf &
"		   Fecha_Valor,                                                                          " & vbCrLf &
"          ifnull(Dias,0),                                                                       " & vbCrLf &
"		   0,                                                                                    " & vbCrLf &
"		   0,--isnull(datediff(d, fecha_pago, @Fecha_pago),0),                                   " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                              " & vbCrLf &
"		   0,                                                                                    " & vbCrLf &
"	      -- CALCULO Tasa Multa ---------------------------------                                " & vbCrLf &
"  		   0,                                                                                    " & vbCrLf &
"		  -------------------------------------------------------                                " & vbCrLf &
"		  ifnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                        " & vbCrLf &
"		   0, -- Gastos Adicionales                                                              " & vbCrLf &
"	 	   0,                                                                                    " & vbCrLf &
"		   Cubierta                                                                              " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                                  " & vbCrLf &
"     --- MULTAS                                                                                 " & vbCrLf &
"    select min(Fecha_cuota) into FechaCuotaVen                                                  " & vbCrLf &
"	   from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                              " & vbCrLf &
"  TotalImpMult_aux = 0 ;                                                                        " & vbCrLf &
"  call """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_MULTA_HANA"" ( FechaCuotaVen, V_Fecha_pago, TotalImpMult_aux);  " & vbCrLf &
"  select TotalImpMult_aux into TotalImpMult from Dummy;                                         " & vbCrLf &
"   update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                                " & vbCrLf &
"      set Multa = ifnull(TotalImpMult,0)                                                        " & vbCrLf &
"    where Fecha_cuota  = FechaCuotaVen;                                                         " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc""                        " & vbCrLf &
"    select 'CodBc' , DocEntryOV , DocEntryOV, Mes, Descripcion, Amort_Cap, TO_VARCHAR (V_Fecha_pago, 'YYYYMMDD'), Fecha_cuota, Multa, 0, Nombre, Identificacion, 0, 'Agencia', 'XXXXXXXXXXX',Cubierta  " & vbCrLf &
"	  from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                               " & vbCrLf &
" end for;                                                                                       " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##resultado""                                    " & vbCrLf &
" Select concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(   " & vbCrLf &
"        rtrim(cast(CodBc as char(50))),'|'),                                                    " & vbCrLf &
"        rtrim(cast(concat('RECAU',TO_VARCHAR(CodEntBc)) as char(50))) ),'|' ), --+'|'+          " & vbCrLf &
"	    rtrim(cast(DocEntryOV as char(50))) ),'|' ), --  +'|'+                                   " & vbCrLf &
"		ltrim(TO_VARCHAR(NroCuota)) ),'|' ),                                                     " & vbCrLf &
"        rtrim(cast(Descripcion as char(50))) ),'|' ), --  +'|'+                                 " & vbCrLf &
"		ltrim(TO_VARCHAR(Importe+Multa)) ),'|' ), --+'|' --+                                     " & vbCrLf &
"		rtrim(cast(Fecha_Pago as char(10))) ),'|' ), -- +'|' +                                   " & vbCrLf &
"		ltrim(TO_VARCHAR(Multa)) ), '|'), -- +'|' +                                              " & vbCrLf &
"	    rtrim(cast(NombreRazonSocial as char(70))) ),'|' ), --  +'|'+                            " & vbCrLf &
"	    rtrim(cast(Identificacion as char(20))) ),'|' ), --  +'|'+                               " & vbCrLf &
"		ltrim(TO_VARCHAR(Numero)) ), '|'), -- +'|' +                                             " & vbCrLf &
"	    rtrim(cast(Agencia as char(30))) ), '|'), -- +'|'+                                       " & vbCrLf &
"	    rtrim(cast(Datos as char(100))) )                                                        " & vbCrLf &
"   from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc"";                              " & vbCrLf &
" RESTABLE1 = Select * from """ & SQLBaseDatos.ToUpper & """.""##resultado"" ;                   " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                                " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                             " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc"";                          " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##resultado"";                                   " & vbCrLf &
"END;"
        Return res
    End Function


    Public Const SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA_NAME As String = "SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA"
    Public Shared Function SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA_TEXT() As String
        Dim res As String
        res = "CREATE PROCEDURE """ & SQLBaseDatos.ToUpper & """.""SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA""(  " & vbCrLf &
"                 DocEntryOV  int          -- Orden de Venta                                     " & vbCrLf &
"                ,V_Fecha_pago  date       -- Fecha del pago a realizarce                        " & vbCrLf &
"                ,out error_msg  int                                                             " & vbCrLf &
"                ,out RESTABLE1 """ & SQLBaseDatos.ToUpper & """.""CALPAGOBC"" )                 " & vbCrLf &
"LANGUAGE SQLSCRIPT As                                                                           " & vbCrLf &
"BEGIN                                                                                           " & vbCrLf &
"   DECLARE CURSOR F_Orden_Venta FOR                                                             " & vbCrLf &
"     select  ""DocEntry"",                                                                      " & vbCrLf &
"             ""DocDate"",                                                                       " & vbCrLf &
"             ""U_DocCuota"",                                                                    " & vbCrLf &
"	          ""U_Interes"",                                                                     " & vbCrLf &
"			  ""U_IntMor"",                                                                      " & vbCrLf &
"			  ""CardCode""                                                                       " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""ORDR""  -- Tabla Maestro Orden de Venta                 " & vbCrLf &
"   where CANCELED = 'N'                                                                         " & vbCrLf &
"     and ""DocStatus"" = 'O'                                                                    " & vbCrLf &
"     and ""DocEntry"" = (case when ifnull(DocEntryOV,0) = 0 then ""DocEntry"" else ifnull(DocEntryOV,0) end) " & vbCrLf &
"	  and ""U_DocCuota"" is not null;                                                            " & vbCrLf &
"			 -- 0. Declaración de variables                                                      " & vbCrLf &
"			        Declare A_capital                     decimal(13,2);                         " & vbCrLf &
"					Declare A_tasa                        decimal(8,4);                          " & vbCrLf &
"					Declare A_nro_cuotas                  smallint;                              " & vbCrLf &
"					Declare A_saldo                       decimal(13,2);                         " & vbCrLf &
"					Declare A_plazo_aux                   smallint;                              " & vbCrLf &
"					Declare A_fecha_Ini_aux               date;                                  " & vbCrLf &
"					Declare A_Fecha_hasta                 date;                                  " & vbCrLf &
"					Declare A_Cap_pagado                  decimal(13,2);                         " & vbCrLf &
"					Declare A_Interes                     decimal(13,2);                         " & vbCrLf &
"					Declare Tot_Amortizado                decimal(13,2);                         " & vbCrLf &
"					Declare Por_pagar                     decimal(13,2);                         " & vbCrLf &
"					Declare Contador                      smallint;                              " & vbCrLf &
"					Declare error_exec                    smallint;                              " & vbCrLf &
"			        Declare A_cuota_cap                   decimal(12,2);                         " & vbCrLf &
"					Declare A_tl_Int_Carg                 decimal(13,2);                         " & vbCrLf &
"					Declare U_DocEntryPG                  int;                                   " & vbCrLf &
"					Declare Tasa                          decimal(8,4);                          " & vbCrLf &
"					Declare Tasa_Vcdo                     decimal(8,4);                          " & vbCrLf &
"					Declare U_CardCode                    varchar(50);                           " & vbCrLf &
"					Declare TasaNormalLegal               char(1);                               " & vbCrLf &
"					Declare FechaCuotaVen                 date;                                  " & vbCrLf &
"					Declare TotalImpMult                  decimal(13,2);                         " & vbCrLf &
"					Declare DocEntryOF                    int;                                   " & vbCrLf &
"					Declare Fecha_valor                   date;                                  " & vbCrLf &
"					Declare FechaUltPago                  date;                                  " & vbCrLf &
"					Declare Nombre                        nvarchar(100);                         " & vbCrLf &
"					Declare Identificacion                nvarchar(32);                          " & vbCrLf &
"					Declare Contador_aux                  int;                                   " & vbCrLf &
"					Declare V_Inter                       decimal(13,2);                         " & vbCrLf &
"			        Declare V_error_mss                   varchar(100);                          " & vbCrLf &
"                   Declare V_err                         int;                                   " & vbCrLf &
"                   Declare aux                           int;                                   " & vbCrLf &
"                   Declare TotalImpMult_aux              decimal(13,2);                         " & vbCrLf &
"			-----------------------------------------------------------------------              " & vbCrLf &
"			-- Asignacion de valores iniciales --                                                " & vbCrLf &
"			-----------------------------------------------------------------------              " & vbCrLf &
"			        A_tasa          = Tasa;                                                      " & vbCrLf &
"					Tot_Amortizado  = 0;                                                         " & vbCrLf &
"				    Contador        = 2;                                                         " & vbCrLf &
"					Contador_aux    = 1;                                                         " & vbCrLf &
"					A_nro_cuotas    = 0;                                                         " & vbCrLf &
"					Fecha_valor     = V_Fecha_pago;                                              " & vbCrLf &
"					TasaNormalLegal = 'N';                                                       " & vbCrLf &
"					TotalImpMult    = 0;                                                         " & vbCrLf &
"-- 1. Crear tabla amortización                                                                  " & vbCrLf &
"-----------------------------------------------------------------------------------             " & vbCrLf &
"			CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"" (   " & vbCrLf &
"			     Mes int                                                                         " & vbCrLf &
"				,Fecha_Pago date                                                                 " & vbCrLf &
"				,Saldo_capital decimal(13,2)                                                     " & vbCrLf &
"				,Dias smallint                                                                   " & vbCrLf &
"				,Interes decimal(13,2)                                                           " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                         " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                       " & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)                                                   " & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)                                                   " & vbCrLf &
"				,Cubierta   tinyint                                                              " & vbCrLf &
"				);                                                                               " & vbCrLf &
"		    CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"" (  " & vbCrLf &
"				 Mes smallint                                                                    " & vbCrLf &
"				,Descripcion varchar(30)                                                         " & vbCrLf &
"				,Fecha_Pago date                                                                 " & vbCrLf &
"				,Fecha_cuota date                                                                " & vbCrLf &
"				,Fecha_valor date                                                                " & vbCrLf &
"				,DiasCuot smallint                                                               " & vbCrLf &
"				,Interes decimal(13,2)                                                           " & vbCrLf &
"				,DiasIntMor  smallint                                                            " & vbCrLf &
"				,IntPorMor decimal(13,2)                                                         " & vbCrLf &
"				,Multa decimal(13,2)                                                             " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                         " & vbCrLf &
"				,GastosA decimal(13,2)                                                           " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                       " & vbCrLf &
"				,Cubierta tinyint                                                                " & vbCrLf &
"				);                                                                               " & vbCrLf &
"			CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc"" ( " & vbCrLf &
"			     CodBc    varchar(50)                                                            " & vbCrLf &
"				,CodEntBc varchar(50)                                                            " & vbCrLf &
"				,DocEntryOV  varchar(15)                                                         " & vbCrLf &
"				,NroCuota    int                                                                 " & vbCrLf &
"				,Descripcion varchar(30)                                                         " & vbCrLf &
"				,Importe    decimal(13,2)                                                        " & vbCrLf &
"				,Fecha_Pago char(10)                                                             " & vbCrLf &
"				,Fecha_cuota date                                                                " & vbCrLf &
"				,Multa     decimal(13,2)                                                         " & vbCrLf &
"				,DiasMulta smallint                                                              " & vbCrLf &
"				,NombreRazonSocial  varchar(70)                                                  " & vbCrLf &
"				,Identificacion varchar(20)                                                      " & vbCrLf &
"				,Numero     int                                                                  " & vbCrLf &
"				,Agencia    varchar(30)                                                          " & vbCrLf &
"				,Datos      varchar(100)                                                         " & vbCrLf &
"				,Cubierta   tinyint                                                              " & vbCrLf &
"				);                                                                               " & vbCrLf &
"CREATE LOCAL TEMPORARY TABLE """ & SQLBaseDatos.ToUpper & """.""##resultado"" (                 " & vbCrLf &
"			     Linea varchar(2000)                                                             " & vbCrLf &
"				);                                                                               " & vbCrLf &
"For cursorRow as F_Orden_Venta                                                                  " & vbCrLf &
" do                                                                                             " & vbCrLf &
" DocEntryOV := cursorRow.""DocEntry"";                                                          " & vbCrLf &
" A_fecha_Ini_aux := cursorRow.""DocDate"";                                                      " & vbCrLf &
" U_DocEntryPG := cursorRow.""U_DocCuota"";                                                      " & vbCrLf &
" Tasa := cursorRow.""U_Interes"";                                                               " & vbCrLf &
" Tasa_Vcdo := cursorRow.""U_IntMor"";                                                           " & vbCrLf &
" U_CardCode := cursorRow.""CardCode"";                                                          " & vbCrLf &
" delete from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                               " & vbCrLf &
" delete from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                            " & vbCrLf &
"        Tot_Amortizado = 0;                                                                     " & vbCrLf &
"		A_Cap_pagado = 0;                                                                        " & vbCrLf &
"	    FechaUltPago = A_fecha_Ini_aux; -- OJO hay que obtener la fecha de ultimo pago           " & vbCrLf &
"        DocEntryOF = 0;                                                                         " & vbCrLf &
"        select count(""TrgetEntry"") into aux   -- DocEntry OINV Tabla Factura                  " & vbCrLf &
"          from """ & SQLBaseDatos.ToUpper & """.""RDR1"" -- Tabla Detalle Orden de Venta            " & vbCrLf &
"         where ""DocEntry"" = DocEntryOV -- DocEntry Orden de Venta                             " & vbCrLf &
"	        and ""TargetType"" = 13;                                                             " & vbCrLf &
"        select case when aux>0 then (select ""TrgetEntry""                                      " & vbCrLf &
"                                       from """ & SQLBaseDatos.ToUpper & """.""RDR1"" -- Tabla Detalle Orden de Venta " & vbCrLf &
"                                      where ""DocEntry"" = DocEntryOV -- DocEntry Orden de Venta " & vbCrLf &
"	                                     and ""TargetType"" = 13)                                " & vbCrLf &
"	                else 0 end into DocEntryOF from Dummy;  -- DocEntry OINV Tabla Factura       " & vbCrLf &
"        if ifnull(DocEntryOF,0) <> 0 then -- Si existe factura obtenemos lo total pagado        " & vbCrLf &
"           aux = 0;                                                                             " & vbCrLf &
"           select count(""PaidToDate"") into aux from """ & SQLBaseDatos.ToUpper & """.""OINV"" as A where ""DocEntry"" = DocEntryOF and ""CANCELED"" = 'N';  " & vbCrLf &
"          select case when aux>0 then (select ""PaidToDate""                                    " & vbCrLf &
"                                          from """ & SQLBaseDatos.ToUpper & """.""OINV"" as A       " & vbCrLf &
"                                         where ""DocEntry"" = DocEntryOF                          " & vbCrLf &
"                                           and ""CANCELED"" = 'N')                                " & vbCrLf &
"	                else 0 end into A_Cap_pagado from Dummy;                                     " & vbCrLf &
"        else                                                                                    " & vbCrLf &
"             -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta           " & vbCrLf &
"           aux = 0;                                                                             " & vbCrLf &
"           select sum(""DocTotal"") into aux  from """ & SQLBaseDatos.ToUpper & """.""ORCT"" where ""Canceled"" = 'N' and ""U_ORDV"" = DocEntryOV;   " & vbCrLf &
"           select case when aux>0 then (select ""DocTotal""                                     " & vbCrLf &
"                                          from """ & SQLBaseDatos.ToUpper & """.""ORCT""            " & vbCrLf &
"                                         where ""Canceled"" = 'N'                               " & vbCrLf &
"                                           and ""U_ORDV"" = DocEntryOV)                         " & vbCrLf &
"	                else 0 end into A_Cap_pagado from Dummy;                                     " & vbCrLf &
"        end if ;                                                                                " & vbCrLf &
"        A_Cap_pagado = ifnull(A_Cap_pagado, 0);                                                 " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                              " & vbCrLf &
"   select 0, ""U_FechaV"", 0, 0, 0,                                                             " & vbCrLf &
"          ""U_Amortizacion"" , ""U_Amortizacion"" , ""U_Amortizacion"",                         " & vbCrLf &
"          0, 0                                                                                  " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN""                                        " & vbCrLf &
"    where ""U_DocEntryPG"" = U_DocEntryPG                                                       " & vbCrLf &
"    order by ""U_FechaV"" asc;                                                                  " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""U_Intlgl"") into aux                                                          " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""                                       " & vbCrLf &
"   where ""U_CardCode"" = U_CardCode                                                            " & vbCrLf &
"     and ""Canceled"" = 'N';                                                                    " & vbCrLf &
"   select case when aux>0 then (select ""U_Intlgl""                                             " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""          " & vbCrLf &
"                                 where ""U_CardCode"" = U_CardCode                              " & vbCrLf &
"                                   and ""Canceled"" = 'N')                                      " & vbCrLf &
"	                else 'N' end into TasaNormalLegal from Dummy;                                " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""CardName"") into aux from """ & SQLBaseDatos.ToUpper & """.""OCRD"" where ""CardCode"" = U_CardCode;  " & vbCrLf &
"   select case when aux>0 then (select ""CardName""                                             " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""OCRD""                    " & vbCrLf &
"                                 where ""CardCode"" = U_CardCode)                               " & vbCrLf &
"	                else '' end into Nombre from Dummy;                                          " & vbCrLf &
"   aux = 0;                                                                                     " & vbCrLf &
"   select count(""LicTradNum"") into aux from """ & SQLBaseDatos.ToUpper & """.""OCRD"" where ""CardCode"" = U_CardCode; " & vbCrLf &
"   select case when aux>0 then (select ""LicTradNum""                                           " & vbCrLf &
"                                  from """ & SQLBaseDatos.ToUpper & """.""OCRD""                    " & vbCrLf &
"                                 where ""CardCode"" = U_CardCode)                               " & vbCrLf &
"	                else '' end into Identificacion from Dummy;                                  " & vbCrLf &
"   if ifnull(TasaNormalLegal,'N') = 'N' then                                                    " & vbCrLf &
"      A_tasa = Tasa;                                                                            " & vbCrLf &
"   end if;                                                                                      " & vbCrLf &
"   select count(*) into A_nro_cuotas from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";  " & vbCrLf &
"   Select sum(Amort_Cap) into A_capital from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";   " & vbCrLf &
"   select min(Fecha_Pago) into A_Fecha_hasta from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""; " & vbCrLf &
"   Contador = 1;                                                                                " & vbCrLf &
"   while Contador <= A_nro_cuotas                                                               " & vbCrLf &
"   Do                                                                                           " & vbCrLf &
"      select min(Fecha_Pago) into A_Fecha_hasta                                                 " & vbCrLf &
"        from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                " & vbCrLf &
"       where Mes = 0;                                                                           " & vbCrLf &
"	  update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                 " & vbCrLf &
"	      set Mes = Contador                                                                     " & vbCrLf &
"	 where Fecha_Pago = A_Fecha_hasta;                                                           " & vbCrLf &
"      A_saldo = A_capital - Tot_Amortizado;                                                     " & vbCrLf &
"     CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_HANA""                          " & vbCrLf &
"             ( A_saldo ,                                                                        " & vbCrLf &
"               A_fecha_Ini_aux,                                                                 " & vbCrLf &
"               A_Fecha_hasta,                                                                   " & vbCrLf &
"               A_tasa,                                                                          " & vbCrLf &
"               A_Interes,                                                                       " & vbCrLf &
"               V_error_mss,                                                                     " & vbCrLf &
"               V_err                                                                            " & vbCrLf &
"               );                                                                               " & vbCrLf &
"   select V_Inter into A_Interes from Dummy;                                                    " & vbCrLf &
"   update """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"      set Saldo_capital = A_capital - Tot_Amortizado,                                           " & vbCrLf &
"	      Dias = ifnull(DAYS_BETWEEN(A_fecha_Ini_aux, Fecha_Pago),0),                            " & vbCrLf &
"		  Interes = A_Interes,                                                                   " & vbCrLf &
"		  Total_Cuota = Amort_Cap + A_Interes,                                                   " & vbCrLf &
"		  Por_Pagar_Cap = (case when Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 0 else case when (A_Cap_pagado - Tot_Amortizado) > 0 then  Amort_Cap - (A_Cap_pagado - Tot_Amortizado) else Amort_Cap end end), " & vbCrLf &
"		  Total_Cap_Amort = Tot_Amortizado + Amort_Cap,                                          " & vbCrLf &
"		  Cubierta = (case when Tot_Amortizado + Amort_Cap <= A_Cap_pagado then 1 else 0 end)    " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"   select Tot_Amortizado + Amort_Cap into Tot_Amortizado                                        " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"   select Fecha_Pago into A_fecha_Ini_aux                                                       " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion""                                   " & vbCrLf &
"    where Mes = Contador;                                                                       " & vbCrLf &
"    Contador = Contador + 1;                                                                    " & vbCrLf &
"   end while; -- while @Contador <= @A_nro_cuotas                                               " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                           " & vbCrLf &
"   select Mes,                                                                                  " & vbCrLf &
"          concat('PAGO DE CUOTA No. ', TO_VARCHAR(Mes)),                                        " & vbCrLf &
"          V_Fecha_pago,                                                                         " & vbCrLf &
"		   Fecha_Pago,                                                                           " & vbCrLf &
"		   Fecha_Valor,                                                                          " & vbCrLf &
"          ifnull(Dias,0),                                                                       " & vbCrLf &
"		   0,                                                                                    " & vbCrLf &
"		   0,--isnull(datediff(d, fecha_pago, @Fecha_pago),0),                                   " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                              " & vbCrLf &
"		   0,                                                                                    " & vbCrLf &
"	      -- CALCULO Tasa Multa ---------------------------------                                " & vbCrLf &
"  		   0,                                                                                    " & vbCrLf &
"		  -------------------------------------------------------                                " & vbCrLf &
"		  ifnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                        " & vbCrLf &
"		   0, -- Gastos Adicionales                                                              " & vbCrLf &
"	 	   0,                                                                                    " & vbCrLf &
"		   Cubierta                                                                              " & vbCrLf &
"     from """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                                  " & vbCrLf &
"     --- MULTAS                                                                                 " & vbCrLf &
"    select min(Fecha_cuota) into FechaCuotaVen                                                  " & vbCrLf &
"	   from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                              " & vbCrLf &
"  TotalImpMult_aux = 0 ;                                                                        " & vbCrLf &
"  call """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_MULTA_HANA"" ( FechaCuotaVen, V_Fecha_pago, TotalImpMult_aux);  " & vbCrLf &
"  select TotalImpMult_aux into TotalImpMult from Dummy;                                         " & vbCrLf &
"   update """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada""                                " & vbCrLf &
"      set Multa = ifnull(TotalImpMult,0)                                                        " & vbCrLf &
"    where Fecha_cuota  = FechaCuotaVen;                                                         " & vbCrLf &
"   insert into """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc""                        " & vbCrLf &
"    select 'CodBc' , DocEntryOV , DocEntryOV, Mes, Descripcion, Amort_Cap, TO_VARCHAR (V_Fecha_pago, 'YYYYMMDD'), Fecha_cuota, Multa, 0, Nombre, Identificacion, 0, 'Agencia', 'XXXXXXXXXXX',Cubierta  " & vbCrLf &
"	  from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                               " & vbCrLf &
" end for;                                                                                       " & vbCrLf &
"insert into """ & SQLBaseDatos.ToUpper & """.""##resultado""                                    " & vbCrLf &
"select concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(rtrim(cast(CodBc as char(50))),'|' ), -- +'|'+   " & vbCrLf &
"       rtrim(cast(concat('RECAU',TO_VARCHAR(CodEntBc)) as char(50))) ), '|'), --+'|'+           " & vbCrLf &
"		ltrim(TO_VARCHAR(NroCuota)) ),'|' ), --+'|'+                                             " & vbCrLf &
"       '1' ), '|'), --+'|'+                                                                     " & vbCrLf &
"       '1'), '|'), --+'|'+                                                                      " & vbCrLf &
"       'capital'), '|'),  -- +'|'+                                                              " & vbCrLf &
"       ltrim(TO_VARCHAR(Importe)) )                                                             " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc""                              " & vbCrLf &
"	where Importe > 0                                                                            " & vbCrLf &
"	  and Cubierta = 0                                                                           " & vbCrLf &
"	union all                                                                                    " & vbCrLf &
"	select concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(concat(rtrim(cast(CodBc as char(50))),'|' ), --+'|'+  " & vbCrLf &
"          rtrim(cast(concat('RECAU',TO_VARCHAR(CodEntBc)) as char(50))) ),'|' ),  --+'|'+       " & vbCrLf &
"		   ltrim(TO_VARCHAR(NroCuota)) ), '|'),                                                  " & vbCrLf &
"           '2'), '|'), --+'|'+                                                                  " & vbCrLf &
"		   '2'), '|'), --+'|'+                                                                   " & vbCrLf &
"		   'intetes'), '|' ), --+'|'+                                                            " & vbCrLf &
"          ltrim(TO_VARCHAR(Multa)) )                                                            " & vbCrLf &
"    from """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc""                              " & vbCrLf &
"	where Multa > 0                                                                              " & vbCrLf &
"	  and Cubierta = 0                                                                           " & vbCrLf &
"	order by 1;                                                                                  " & vbCrLf &
" RESTABLE1 = Select * from """ & SQLBaseDatos.ToUpper & """.""##resultado"" ;                   " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Amortizacion"";                                " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada"";                             " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##Cuota_calculada_bc"";                          " & vbCrLf &
" DROP TABLE """ & SQLBaseDatos.ToUpper & """.""##resultado"";                                   " & vbCrLf &
"END;"
        Return res
    End Function



    'CREACION DE TABLAS TIPO

    'EJEMPLO
    '=============================================================================
    Public Const QUANTITYCALCULATEDTYPE_NAME As String = "QUANTITYCALCULATEDTYPE"
    Public Shared Function QUANTITYCALCULATEDTYPE_TEXT() As String
        Dim res As String
        res = "create type """ & SQLBaseDatos.ToUpper & """.QUANTITYCALCULATEDTYPE AS table   " & vbCrLf &
"(   " & vbCrLf &
"   ""U_Object"" nvarchar(50),  " & vbCrLf &
"   ""U_Code"" nvarchar(50),  " & vbCrLf &
"   ""U_Description"" nvarchar(150),  " & vbCrLf &
"   ""U_Unit"" nvarchar(100),  " & vbCrLf &
"   ""U_Quantity"" decimal(19,6),   " & vbCrLf &
"   ""U_Price"" decimal(19,6),   " & vbCrLf &
"   ""U_Father"" nvarchar(50), " & vbCrLf &
"   ""Partida"" nvarchar(50)   " & vbCrLf &
") " & vbCrLf
        Return res
    End Function

    'FIN DE EJEMPLO
    '=============================================================================
    Public Const CALCUOTATYPE_NAME As String = "CALCUOTATYPE"
    Public Shared Function CALCUOTATYPE_TEXT() As String
        Dim res As String
        res = "CREATE TYPE  """ & SQLBaseDatos.ToUpper & """.CALCUOTATYPE AS TABLE " & vbCrLf &
"(" & vbCrLf &
"    ""MES"" SMALLINT , " & vbCrLf &
"	 ""FECHA_PAGO"" Date , " & vbCrLf &
"	 ""SALDO_CAPITAL"" Decimal(13,2) , " & vbCrLf &
"	 ""DIAS"" SMALLINT , " & vbCrLf &
"	 ""INTERES"" Decimal(13,2) , " & vbCrLf &
"	 ""AMORT_CAP"" Decimal(13,2) , " & vbCrLf &
"	 ""TOTAL_CUOTA"" Decimal(13,2) , " & vbCrLf &
"	 ""POR_PAGAR_CAP"" Decimal(13,2) , " & vbCrLf &
"    ""GASTOSA"" Decimal(13,2), " & vbCrLf &
"	 ""TOTAL_CAP_AMORT"" Decimal(13,2) , " & vbCrLf &
"	 ""CUBIERTA"" TINYINT  " & vbCrLf &
")" & vbCrLf
        Return res
    End Function

    Public Const CALPAGOTYPE_NAME As String = "CALPAGOTYPE"
    Public Shared Function CALPAGOTYPE_TEXT() As String
        Dim res As String
        res = "CREATE TYPE """ & SQLBaseDatos.ToUpper & """.CALPAGOTYPE AS TABLE  " & vbCrLf &
"(  " & vbCrLf &
"	 ""MES"" SMALLINT , " & vbCrLf &
"	 ""FECHA_PAGO"" DATE , " & vbCrLf &
"	 ""SALDO_CAPITAL"" DECIMAL(18,2) , " & vbCrLf &
"	 ""DIAS"" SMALLINT , " & vbCrLf &
"	 ""INTERES"" DECIMAL(18,2) , " & vbCrLf &
"	 ""AMORT_CAP"" DECIMAL(18,2) , " & vbCrLf &
"	 ""TOTAL_CUOTA"" DECIMAL(18,2) , " & vbCrLf &
"	 ""POR_PAGAR_CAP"" DECIMAL(18,2) , " & vbCrLf &
"	 ""TOTAL_CAP_AMORT"" DECIMAL(18,2) , " & vbCrLf &
"	 ""CUBIERTA"" TINYINT   " & vbCrLf &
")" & vbCrLf
        Return res
    End Function

    Public Const CALCPAGOTYPE_NAME As String = "CALCPAGOTYPE"
    Public Shared Function CALCPAGOTYPE_TEXT() As String
        Dim res As String
        res = "CREATE TYPE """ & SQLBaseDatos.ToUpper & """.CALCPAGOTYPE AS TABLE  " & vbCrLf &
"( " & vbCrLf &
"	 ""DESCRIPCION"" VARCHAR(30) , " & vbCrLf &
"	 ""FECHA_PAGO"" Date , " & vbCrLf &
"	 ""FECHA_CUOTA"" Date , " & vbCrLf &
"	 ""FECHA_VALOR"" Date , " & vbCrLf &
"	 ""DIASCUOT"" SMALLINT , " & vbCrLf &
"	 ""INTERES"" Decimal(18,2) , " & vbCrLf &
"	 ""DIASINTMOR"" SMALLINT , " & vbCrLf &
"	 ""INTPORMOR"" Decimal(18,2) , " & vbCrLf &
"	 ""MULTA"" Decimal(18,2) , " & vbCrLf &
"	 ""AMORT_CAP"" Decimal(18,2) , " & vbCrLf &
"	 ""GASTOSA"" Decimal(18,2) , " & vbCrLf &
"	 ""TOTAL_CUOTA"" Decimal(18,2), " & vbCrLf &
"    ""FECHAULTPAGO""  Date, " & vbCrLf &
"    ""DIAS""  int, " & vbCrLf &
"    ""INTERESTL"" decimal(18,2) " & vbCrLf &
")" & vbCrLf
        Return res

    End Function

    Public Const CALPAGOBC_NAME As String = "CALPAGOBC"
    Public Shared Function CALPAGOBC_TEXT() As String
        Dim res As String
        res = "CREATE TYPE """ & SQLBaseDatos.ToUpper & """.CALPAGOBC AS TABLE  " & vbCrLf &
"             (  " & vbCrLf &
"	              Linea varchar(2000)    " & vbCrLf &
"             ) " & vbCrLf
        Return res

    End Function

    Public Const CALPAGODETALLEBC_NAME As String = "CALPAGODETALLEBC"
    Public Shared Function CALPAGODETALLEBC_TEXT() As String
        Dim res As String
        res = "CREATE TYPE """ & SQLBaseDatos.ToUpper & """.CALPAGODETALLEBC AS TABLE  " & vbCrLf &
"             (  " & vbCrLf &
"	              Linea varchar(2000)    " & vbCrLf &
"             ) " & vbCrLf
        Return res

    End Function

    Public Shared Function PopulaGridChecklist(DocEntry As String) As String

        Dim query As String = String.Empty

        Try

            If Trim(DocEntry) = "" Then
                query = "SELECT A.""Code"" AS ""CODIGO"",A.""Name"" AS ""DESCRIPCION"",IFNULL(B.""U_Realizado"",'N') AS ""REALIZADO"",IFNULL(B.""Code"",'-1') AS ""LINEABD"",IFNULL(B.""U_CodChk"",'') AS ""CODIGOBD"",IFNULL(B.""U_DesChk"",'') AS ""DESCRBD"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICHK"" A LEFT JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR1"" B ON (A.""Code"" = IFNULL(B.""U_CodChk"",'')  AND IFNULL(B.""U_CodLC"",'-1') = '-2') ORDER BY 1 ASC"

            Else
                query = "SELECT A.""Code"" AS ""CODIGO"",A.""Name"" AS ""DESCRIPCION"",IFNULL(B.""U_Realizado"",'N') AS ""REALIZADO"",IFNULL(B.""Code"",'-1') AS ""LINEABD"",IFNULL(B.""U_CodChk"",'') AS ""CODIGOBD"",IFNULL(B.""U_DesChk"",'') AS ""DESCRBD"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICHK"" A LEFT JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR1"" B ON (A.""Code"" = IFNULL(B.""U_CodChk"",'')  AND IFNULL(B.""U_CodLC"",'-1') = '" & Trim(DocEntry) & "') ORDER BY 1 ASC"

            End If



            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function PopulaGridTasas(DocEntry As String) As String ' ssh

        Dim query As String = String.Empty

        Try

            If Trim(DocEntry) = "" Then

                query = "SELECT A.""U_CodLC"" AS ""CODIGO"",A.""U_CodGrupArt"" AS ""CODGRUPART"", IFNULL(B.""ItmsGrpNam"",'') AS DESCRIPCION, IFNULL(A.""U_TasaNor"",0) AS ""TASANOR"",IFNULL(A.""U_TasaLeg"",0) AS ""TASALEG"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"" A INNER JOIN """ & SQLBaseDatos.ToUpper & """.""OITB"" B ON B.""ItmsGrpCod"" = A.""U_CodGrupArt"" AND IFNULL(A.""U_CodLC"",'-1') = '-2' ORDER BY 1 ASC"

            Else
                query = "SELECT A.""U_CodLC"" AS ""CODIGO"",A.""U_CodGrupArt"" AS ""CODGRUPART"",IFNULL(B.""ItmsGrpNam"",'') AS DESCRIPCION, IFNULL(A.""U_TasaNor"",0) AS ""TASANOR"",IFNULL(A.""U_TasaLeg"",0) AS ""TASALEG"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"" A INNER JOIN """ & SQLBaseDatos.ToUpper & """.""OITB"" B ON B.""ItmsGrpCod"" = A.""U_CodGrupArt"" AND IFNULL(A.""U_CodLC"",'-1') = '" & Trim(DocEntry) & "' ORDER BY 1 ASC"

            End If

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function InsertaChecklist(CodigoLC As String, CodChk As String, DesChk As String, Realizado As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR1""" + vbCrLf +
"(""Code"",""Name"",""U_CodLC"",""U_CodChk"",""U_DesChk"",""U_Realizado"")" + vbCrLf +
"SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code" + vbCrLf +
                            ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name" + vbCrLf +
",'" & Trim(CodigoLC) & "'" + vbCrLf +
",'" & Trim(CodChk) & "'" + vbCrLf +
",'" & Trim(DesChk) & "'" + vbCrLf +
",'" & Trim(Realizado) & "'" + vbCrLf +
"FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR1"""


            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    ' ssh
    Public Shared Function GetDescGrupoArt(CodGrupArt As String) As String
        Return _
     "SELECT ""ItmsGrpNam"" FROM """ & SQLBaseDatos.ToUpper & """.""OITB"" where ""ItmsGrpCod"" ='" & Trim(CodGrupArt) & "'"

    End Function
    Public Shared Function LimpiarTasasGrupArt(CodigoLC As String) As Boolean ' ssh
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "DELETE FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"" WHERE ""U_CodLC"" = '" & Trim(CodigoLC) & "'"
            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    ' ssh
    Public Shared Function InsertaTasasGrupArt(CodigoLC As String, CodGrupArt As String, TasaNor As String, TasaLeg As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"" " + vbCrLf +
"(""Code"",""Name"",""U_CodLC"",""U_CodGrupArt"",""U_TasaNor"",""U_TasaLeg"")" + vbCrLf +
"SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code" + vbCrLf +
                            ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name" + vbCrLf +
",'" & Trim(CodigoLC) & "'" + vbCrLf +
",'" & Trim(CodGrupArt) & "'" + vbCrLf +
",'" & Trim(TasaNor) & "'" + vbCrLf +
",'" & Trim(TasaLeg) & "'" + vbCrLf +
"FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"""

            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Shared Function UpdateChecklist(CodeTBL As String, Realizado As String) As Boolean
        Dim sSQLUpdate As String = String.Empty

        Try
            sSQLUpdate = "UPDATE """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR1""" + vbCrLf +
                         "SET ""U_Realizado"" = '" & Trim(Realizado) & "'" + vbCrLf +
                         "WHERE ""Code"" = '" & Trim(CodeTBL) & "'"

            Try
                conexi.creaRegistro(sSQLUpdate, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Shared Function GetParamProcedureCuotas_TEXT(Cuotas As String, Tasa As String, Anticipo As String, Lapso As Integer, Original As Integer, CapitalTotal As String, FechaDoc As String, CapPago As Integer) As String
        Dim query As String = String.Empty
        Try
            Dim V_error_msg As String
            Dim CuotaFija As String
            Dim V_GastosA As Decimal

            V_error_msg = 0
            CuotaFija = "S"
            V_GastosA = 0

            query = "DO " + vbCrLf +
                           "BEGIN " + vbCrLf +
                           "DECLARE tabla """ & SQLBaseDatos.ToUpper & """.""CALCUOTATYPE"";" + vbCrLf +
                           "DECLARE err int = 0;" + vbCrLf +
                           "CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_CUOTAS_HANA""" + vbCrLf +
                           "('" & Trim(CapitalTotal) & "','" & Trim(Cuotas) & "','" & Trim(Tasa) & "'," + vbCrLf +
                           "'" & Trim(Lapso) & "', '" & Trim(FechaDoc) & "', '" & Trim(CapPago) & "', '" & Trim(Anticipo) & "', '" & CuotaFija & "', '" & V_error_msg & "', '" & V_GastosA & "'" + vbCrLf +
                           "'" & Trim(Original) & "', :err, :tabla );" + vbCrLf +
                           "SELECT ""MES"" AS ""MES"", ""SALDO_CAPITAL"" AS ""SALDO CAPITAL""" + vbCrLf +
                           ",""DIAS"" AS ""DIAS"", ""INTERES"" AS ""INTERES"", ""AMORT_CAP"" AS ""AMORTIZACION"", ""TOTAL_CUOTA"" AS ""CUOTA TOTAL""" + vbCrLf +
                           ",""POR_PAGAR_CAP"" AS ""CAPITAL POR PAGAR"", ""TOTAL_CAP_AMORT"" AS ""CAPITAL AMORTIZADO"", ""CUBIERTA"", ""GASTOSA"" AS ""GASTOS ADICIONALES"" FROM :tabla;" + vbCrLf +
                           "END"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetParamFechaCuota_TEXT(DocEntryOV As String, DocEntryPG As String) As String
        Return _
        "SELECT TO_VARCHAR (TO_DATE(""U_FechaV""), 'DD/MM/YYYY') AS ""U_FechaV"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN"" WHERE 
        ""U_DocEntryOV"" = '" & DocEntryOV & "' AND ""U_DocEntryPG"" = '" & DocEntryPG & "' ORDER BY ""Code"" ASC"
    End Function
    Public Shared Function GetInfoLineasOV(DocEntryOV As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT R1.""LineNum"", R1.""ItemCode"", R1.""Dscription"",	R1.""Quantity"", R1.""WhsCode"", R1.""Price"", R1.""PriceAfVAT"", R1.""GTotal"", R1.""Currency""," &
                    " R1.""CogsOcrCod"", R1.""CogsOcrCo2"", R1.""CogsOcrCo3"", R1.""CogsOcrCo4"", R1.""CogsOcrCo5"", R1.""Project""" &
                    " FROM """ & SQLBaseDatos.ToUpper & """.ORDR OD INNER JOIN """ & SQLBaseDatos.ToUpper & """.RDR1 R1 ON OD.""DocEntry"" = R1.""DocEntry"" WHERE OD.""DocEntry"" = '" & DocEntryOV & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

End Class

﻿Public Class sqlPEnt
    Public Const EsDocumentoDeVehiculos_NAME As String = "EsDocumentoDeVehiculos"
    Public Shared Function EsDocumentoDeVehiculos_TEXT(ItemCodes As String) As String
        Return _
    "select count(QryGroup64) from ""OITM"" where ItemCode in  (" & vbCrLf &
    ItemCodes & vbCrLf &
    ") and QryGroup64='Y'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const EsVehiculo_NAME As String = "EsVehiculo"
    Public Shared Function EsVehiculo_TEXT(ItemCode As String) As String
        Return _
    "select QryGroup64 from ""OITM"" where ItemCode ='" & ItemCode & "' and QryGroup64='Y'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const GetSumDec_NAME As String = "GetSumDec"
    Public Shared GetSumDec_TEXT As String = vbCrLf +
    "select sumdec from oadm"
    '---------------------------------------------------------------------------------------------------------------------------

    Public Shared Function GetDecimalsMoneda(Moneda As String) As String
        Return _
            "SELECT ISNULL(Decimals,0) AS ISUMDEC FROM OCRN WHERE CurrCode = '" & Moneda & "'"
    End Function


    Public Const ActualizaCampoUsuario_NAME As String = "ActualizaCampoUsuario"
    Public Shared Function ActualizaCampoUsuario_TEXT(Docentry As String, CampoUsuario As String, Valor As String, Tabla As String) As String
        Return _
        "UPDATE """ & Tabla & """ set " & CampoUsuario & "='" & Valor & "' where DocEntry='" & Docentry & "'"
    End Function

    '-------------------------------------------------------------------------------------------------------------------
    Public Const GetSegundaCuenta_NAME As String = "GetSegundaCuenta_NAME"
    Public Shared Function GetSegundaCuenta_TEXT(Docentry As String, PrimeraCuenta As String) As String
        Return _
        "select top 1 Account  from JDT1 where transID=(select TransID from OPDN where docentry='" & Docentry & "') and Account not in ('" & PrimeraCuenta & "')"
    End Function

    '-------------------------------------------------------------------------------------------------------------------

    Public Shared Function ActualizaCampoUsuario_TEXT(Docentry As String, CampoUsuario As String, Valor As Decimal, Tabla As String) As String
        Return _
        "UPDATE """ & Tabla & """ set " & CampoUsuario & "=" & Valor & " where DocEntry='" & Docentry & "'"
    End Function

    Public Shared Function GetExistenciaVehc(itemcode As String, serie As String) As String
        Dim query As String = String.Empty

        query = "SELECT ISNULL(A.Quantity,0) AS EXISTE FROM OSRQ A INNER JOIN OSRI B ON (A.ItemCode = B.ItemCode AND A.SysNumber = B.SysSerial) WHERE A.ItemCode = '" & itemcode & "' AND B.IntrSerial = '" & serie & "'"

        Return query

    End Function

    Public Shared Function Get_tipo_de_cuenta_item_TEXT(itemCode As String) As String
        Return _
    "Select GLMethod from OITM where ItemCode='" & itemCode & "'"
    End Function

    Public Shared Function Get_Cuenta_Item_de_almacen_TEXT(WhsCode As String) As String
        Return _
    "SELECT PriceDifAc  FROM OWHS T0 where WhsCode='" & WhsCode & "'"
    End Function

    Public Shared Function Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod As String) As String
        Return _
    "SELECT PriceDifAc  FROM OITB T0 where ItmsGrpCod='" & ItmsGrpCod & "'"
    End Function

    Public Shared Function Get_Cuenta_Item_de_Item_TEXT(ItemCode As String, WhsCode As String) As String
        Return _
    "Select PriceDifAc from OITW where ItemCode='" & ItemCode & "' and WhsCode='" & WhsCode & "'"
    End Function

    Public Shared Function GetBoxPatentFromDoc(DocType As String, DocEntry As String, ItemCode As String, LineaBase As Double) As String
        Dim query As String = String.Empty

        Select Case DocType

            Case "18" 'Factura de Proveedores
                query = "SELECT U_BoxPatent AS BoxPatent FROM [PCH1] WHERE DocEntry = '" & DocEntry & "' AND ItemCode = '" & ItemCode & "' AND LineNum   = '" & LineaBase & "'"

                Return query
            Case "20" 'Entrada de Mercancias
                query = "SELECT U_BoxPatent AS BoxPatent FROM [PDN1] WHERE DocEntry = '" & DocEntry & "' AND ItemCode = '" & ItemCode & "' AND LineNum = '" & LineaBase & "'"

                Return query

            Case "69" 'Precio de entrega
                query = "SELECT U_BoxPatent AS BoxPatent FROM [IPF1] WHERE DocEntry = '" & DocEntry & "' AND ItemCode = '" & ItemCode & "' AND LineNum = '" & LineaBase & "'"

                Return query

            Case Else
                Return ""

        End Select

    End Function


    Public Shared Function GetCuentaCostosPEnt() As String
        Dim query As String = String.Empty

        query = "SELECT TOP 1 LaCAllcAcc AS CUENTA FROM [OALC] WHERE U_IsVehc ='Y'"

        Return query
    End Function

    Public Shared Function GetCostosOALC(AlcCode As String) As String
        Dim query As String = String.Empty

        query = "SELECT AlcCode AS CODE,ISNULL(LaCAllcAcc,'') AS CUENTA,ISNULL(U_IsVehc,'') AS VEHC FROM [OALC] WHERE AlcCode = '" & AlcCode & "'"

        Return query

    End Function

    Public Shared Function GetPEntCuentaItem(ItemCode As String, WhsCode As String) As String
        Return _
    "Select StkInTnAct from OITW where ItemCode='" & ItemCode & "' and WhsCode='" & WhsCode & "'"
    End Function


    Public Shared Function GetPEntDocEntry(DocNum As String, Serie As String) As String
        Dim query As String = String.Empty

        query = "SELECT DocEntry  FROM OIPF WHERE DocNum = '" & DocNum & "' AND Series = '" & Serie & "'"

        Return query
    End Function

    Public Shared Function GrabaDTPEntLog(PreEntry As String, DocOri As String, OriType As String, ItemCode As String, ItemName As String, Almacen As String, Precio As Double, Cuenta As String, ContCue As String, EsDebit As String, CostTLC As Double, CostTFC As Double, BoxPatent As String, OcrCode As String, OcrCode2 As String, OcrCode3 As String, OcrCode4 As String, OcrCode5 As String, Moneda As String, Serie As String, FecCont As String, iSumdec As String, Glosa As String) As String

        Dim query As String = String.Empty

        query = "INSERT INTO [@EXX_PENT]" + vbCrLf +
           "(Code" + vbCrLf +
           ",Name" + vbCrLf +
           ",U_PreEntry" + vbCrLf +
           ",U_DocOri" + vbCrLf +
           ",U_OriType" + vbCrLf +
           ",U_ItemCode" + vbCrLf +
           ",U_ItemName" + vbCrLf +
           ",U_Almacen" + vbCrLf +
           ",U_Precio" + vbCrLf +
           ",U_Cuenta" + vbCrLf +
           ",U_ContCue" + vbCrLf +
           ",U_EsDebit" + vbCrLf +
           ",U_CostTLC" + vbCrLf +
           ",U_CostTFC" + vbCrLf +
           ",U_BoxPatent" + vbCrLf +
           ",U_OcrCode" + vbCrLf +
           ",U_OcrCode2" + vbCrLf +
           ",U_OcrCode3" + vbCrLf +
           ",U_OcrCode4" + vbCrLf +
           ",U_OcrCode5" + vbCrLf +
           ",U_Moneda" + vbCrLf +
           ",U_Serie" + vbCrLf +
           ",U_FecCont" + vbCrLf +
           ",U_Glosa)" + vbCrLf +
           " SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code " + vbCrLf +
           " ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Name " + vbCrLf +
           ",'" & PreEntry & "'" + vbCrLf +
           ",'" & DocOri & "'" + vbCrLf +
           ",'" & OriType & "'" + vbCrLf +
           ",'" & ItemCode & "'" + vbCrLf +
           ",'" & ItemName & "'" + vbCrLf +
           ",'" & Almacen & "'" + vbCrLf +
           "," & Replace(Precio, ",", ".") & "" + vbCrLf +
           ",'" & Cuenta & "'" + vbCrLf +
           ",'" & ContCue & "'" + vbCrLf +
           ",'" & EsDebit & "'" + vbCrLf +
           "," & Replace(CostTLC, ",", ".") & "" + vbCrLf +
           "," & Replace(CostTFC, ",", ".") & "" + vbCrLf +
           ",'" & BoxPatent & "'" + vbCrLf +
           ",'" & OcrCode & "'" + vbCrLf +
           ",'" & OcrCode2 & "'" + vbCrLf +
           ",'" & OcrCode3 & "'" + vbCrLf +
           ",'" & OcrCode4 & "'" + vbCrLf +
           ",'" & OcrCode5 & "'" + vbCrLf +
           ",'" & Moneda & "'" + vbCrLf +
           ",'" & Serie & "'" + vbCrLf +
           ",'" & FecCont & "'" + vbCrLf +
           ",'" & Glosa & "'" + vbCrLf +
           "FROM [@EXX_PENT]"

        Return query

    End Function


End Class

﻿Public Class hanaGarLinea

    Public Shared Function getDetalleGarantiaSQL(U_Codigo As String) As String

        Dim query As String = String.Empty

        query = "SELECT ""DocEntry"", ""LineId"", ""VisOrder"", ""Object"", ""LogInst"", ""U_Codigo"", ""U_Fiplan_codigo"", ""U_Fipla1_codigo"", ""U_TipoC"", ""U_Oblig"", ""U_Value"" FROM ""@EXX_FIPLD1"" WHERE '" & U_Codigo & "'"
        Return query

    End Function

    Public Shared Function getListaFipla1(DocEntry As String, Operacion As String) As String

        Dim query As String = String.Empty

        query = "SELECT ""@EXX_FIPLA1"".""DocEntry"", ""@EXX_FIPLA1"".""LineId"", ""@EXX_FIPLA1"".""VisOrder""," &
                " ""@EXX_FIPLA1"".""Object"", ""@EXX_FIPLA1"".""LogInst"", ""@EXX_FIPLAN"".""U_Codigo"" AS ""U_Fiplan_codigo""," &
                " ""@EXX_FIPLA1"".""U_Codigo"" AS ""U_Fipla1_codigo"", ""@EXX_FIPLA1"".""U_Descr"", ""@EXX_FIPLA1"".""U_TipoC"", ""@EXX_FIPLA1"".""U_Oblig""" &
                " FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLAN"" ON ""@EXX_FIPLA1"".""DocEntry"" = ""@EXX_FIPLAN"".""DocEntry""" &
                " WHERE ""@EXX_FIPLA1"".""DocEntry"" = ""@EXX_FIPLAN"".""DocEntry"" AND ""@EXX_FIPLAN"".""DocEntry"" = '" & Operacion & "'" 'mauricio

        Return query

    End Function

    Public Shared Function insertaDetalleGarFIPLD1(U_Plantilla As String, U_Valor As String, U_CodeLineaCred As String, U_CodeDoc As String, U_TipoDoc As String) As String
        Dim sSqlInsert As String

        sSqlInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1""" + vbCrLf +
                    "(""Code"", ""Name"", ""U_Codigo"", ""U_Plantilla"", ""U_Valor"", ""U_CodeLineaCred"", ""U_CodeDoc"", ""U_TipoDoc"")" + vbCrLf +
                    "SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8)" + vbCrLf +
                    ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8)" + vbCrLf +
                    ", '" & U_Plantilla & "','" & U_CodeDoc & "', '" & U_Valor & "', '" & U_CodeLineaCred & "', '" & U_CodeDoc & "', '" & U_TipoDoc & "' FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"""
        Return sSqlInsert

    End Function
    Public Shared Function getCuotasLineas(DocEntry As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT CU.""U_FechaV"", CU.""U_TotalCuota"", CU.""U_DocEntryPG"", CU.""U_CapitalxPagar"", CU.""U_Amortizacion"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN"" CU WHERE CU.""U_DocEntryOV"" = '" & Trim(DocEntry) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function getCuotasCabecera(docEntryOV As Integer) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT EC.""U_DocEntry"", EC.""U_Cuotas"", EC.""U_Anticipo"", EC.""U_TasaInteres"", EC.""U_InteresLegal"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOTA"" EC WHERE EC.""U_DocEntry"" = '" & Trim(docEntryOV) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
End Class

﻿Imports Proyectos.Data
Imports XFinanciamiento.Data

Public Class HanaAutorizaciones
    Public Shared conexi As Conexiones = New Conexiones

    Public Shared Function UpdateDiasTrabajados(Presupuesto As String, Periodo As String, tipo As String, rut As String, dias As Integer) As Boolean
        Try
            Dim sSqlUpdate As String

            sSqlUpdate = "UPDATE """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""" + vbCrLf +
                         "SET ""U_DiasTrabajados"" = " & dias & "" + vbCrLf +
                         "WHERE ""U_PresEntry"" = '" & Presupuesto & "' AND ""U_Periodo"" = '" & Periodo & "' AND ""U_Tipo"" = '" & tipo & "' AND ""U_LicTradNum"" = '" & rut & "'"

            Try
                conexi.creaRegistro(sSqlUpdate, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function


    Public Shared Function InsertDiaLog(Presupuesto As String, CardCode As String, CardName As String, RUT As String, Periodo As String, AFecha As String, Tipo As String, Dias As String, Usuario As String, Fecha As String, Optional Metodo As String = "A") As Boolean

        Try

            Dim sSqlUpdate As String
            sSqlUpdate = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE""" + vbCrLf +
           "(""Code""" + vbCrLf +
           ",""Name""" + vbCrLf +
           ",""U_PresEntry""" + vbCrLf +
           ",""U_CardCode""" + vbCrLf +
           ",""U_CardName""" + vbCrLf +
           ",""U_LicTradNum""" + vbCrLf +
           ",""U_Periodo""" + vbCrLf +
           ",""U_A_Fecha""" + vbCrLf +
           ",""U_Tipo""" + vbCrLf +
           ",""U_DiasTrabajados""" + vbCrLf +
           ",""U_Usuario""" + vbCrLf +
           ",""U_FechaLog""" + vbCrLf +
           ",""U_Metodo"")" + vbCrLf +
           "SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code    " + vbCrLf +
            ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name " + vbCrLf +
           ",'" & Replace(Presupuesto, ",", ".") & "'" + vbCrLf +
            ",'" & CardCode & "'" + vbCrLf +
            ",'" & CardName & "'" + vbCrLf +
            ",'" & Replace(RUT, ",", ".") & "'" + vbCrLf +
            ",'" & Periodo & "'" + vbCrLf +
           ",'" & AFecha & "'" + vbCrLf +
           ",'" & Tipo & "'" + vbCrLf +
           ",'" & Dias & "'" + vbCrLf +
           ",'" & Replace(Usuario, ",", ".") & "'" + vbCrLf +
           ",'" & Fecha & "'" + vbCrLf +
           ",'" & Metodo & "'" + vbCrLf +
           "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"""

            Try
                conexi.creaRegistro(sSqlUpdate, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function


    Public Shared Function GetFunQuery(CodFuncion As String, Optional DocEntry As String = "") As String
        Dim query As String = String.Empty

        Try
            If DocEntry = "" Then
                query = "SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""" & Trim(CodFuncion) & """ ('1')"
            Else
                query = "SELECT * FROM """ & SQLBaseDatos.ToUpper & """.""" & Trim(CodFuncion) & """ ('" & Trim(DocEntry) & "')"
            End If



            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function GetUsrCodeFromName(UsrName As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""USERID"" FROM """ & SQLBaseDatos.ToUpper & """.""OUSR"" WHERE ""USER_CODE"" = '" & Trim(UsrName) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetProyectosAsociados(UsrCode As String) As String
        Dim query As String = String.Empty

        Try

            query = "SELECT ""A"".""PrjCode"" AS ""CODE"",IFNULL(""A"".""PrjName"",'') AS ""NAME"" , IFNULL(""B"".""U_Enabled"",'N') AS ""ACTIVO"",IFNULL(""B"".""Code"",'') AS ""CODETBL"",IFNULL(""B"".""Name"",'') AS ""NAMETBL"" FROM """ & SQLBaseDatos.ToUpper & """.""OPRJ"" ""A"" LEFT JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY"" ""B"" ON (""A"".""PrjCode"" = ""B"".""U_ProjectC"" AND ""B"".""U_UsrCode"" = '" & Trim(UsrCode) & "') WHERE ""A"".""Active"" = 'Y' "

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function


    Public Shared Function GetGruposUsuario(usercode As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT IFNULL(""GroupId"",0) AS ""GRUPO"" FROM """ & SQLBaseDatos.ToUpper & """.""USR7"" WHERE ""UserId"" = '" & Trim(usercode) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function GetGrupoAutDias() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""U_Value1"",'') AS ""GRUPO"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_PCPARAM"" WHERE ""U_Type"" = 'GrpDias'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetGrupoResetAut() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""U_Value1"",'') AS ""GRUPO"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_PCPARAM"" WHERE ""U_Type"" = 'GRUPOR'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetFuncionesTabla() As String

        Dim query As String = String.Empty

        Try
            query = "SELECT ""FUNCTION_NAME"" AS ""name"" FROM ""SYS"".""FUNCTIONS"" WHERE ""SCHEMA_NAME"" = '" & SQLBaseDatos.ToUpper & "' AND ""FUNCTION_NAME"" LIKE ('EXXFUN%')"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function



    Public Shared Function GetSiFormModelo(formulario As String) As String

        Dim query As String = String.Empty

        Try

            query = "SELECT COUNT(1) AS ""EXISTE"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" WHERE ""U_Form"" = '" & Trim(formulario) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Public Shared Function GetFormName(Formulario As String) As String
        Dim query As String = String.Empty

        Try

            query = "SELECT ""Name"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFOR"" WHERE ""Code"" = '" & Trim(Formulario) & "'"

            Return query

        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetForms() As String
        Dim query As String = String.Empty

        Try

            query = "SELECT ""Code"",""Name"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFOR"" WHERE ""U_Deshab"" = 'N'"

            Return query

        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetCondicionesModelo(CodModelo As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""B"".""U_CodCon"",'') AS ""CONDICION"",IFNULL(""B"".""U_CodEtp"",'') AS ""ETAPA"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOC"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodModelo) & "' ORDER BY ""B"".""LineId"" ASC"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Public Shared Function GetCondicionesAutorizacion(CodCondicion As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""A"".""U_Form"" AS ""FORMULARIO"",""A"".""U_Funcion"" AS ""FUNCION"",""B"".""U_Campo"" AS ""CAMPO"",""B"".""U_Condt"" AS ""CONDICION"",IFNULL(""B"".""U_Valor"",'') AS ""VALOR"",IFNULL(""B"".""U_CampoVal"",'') AS ""CAMPOVAL"",""B"".""U_EsCampo"" AS ""ESCAMPO""  FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOCON"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOCOL"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodCondicion) & "' ORDER BY ""B"".""LineId"" ASC"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Public Shared Function GetEtapasModelo(CodModelo As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""A"".""U_EtpCon"" AS ""ETPCON"",""B"".""LineId"" AS ""LINETP"",""B"".""U_CodEtp"" AS ""CODETP"",IFNULL(""B"".""U_EstadoF"",'') AS ""ESTADOFIN"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & CodModelo & "' ORDER BY ""B"".""LineId"" ASC"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Public Shared Function GetLineaEtapaporCond(CodModelo As String, CodEtapa As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT TOP 1 IFNULL(""B"".""LineId"",0) AS ""LINEA"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodModelo) & "' AND ""B"".""U_CodEtp"" = '" & Trim(CodEtapa) & "' ORDER BY ""B"".""LineId"" ASC"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetSiguienteEtapaMod(CodModelo As String, LineaEtapa As String) As String
        Dim query As String = String.Empty
        Try
            If Trim(LineaEtapa) = "" Then
                LineaEtapa = "0"
            End If

            query = "SELECT ""B"".""LineId"" AS ""LINEA"",""B"".""U_CodEtp"" AS ""ETAPA"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodModelo) & "' AND ""B"".""LineId"" > '" & Trim(LineaEtapa) & "' ORDER BY ""B"".""LineId"" ASC LIMIT 1"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetSiEtapaFinal(CodModelo As String, Etapa As String) As String
        Dim query As String
        Try
            query = "SELECT IFNULL(""B"".""U_EtpFin"",'N') AS ""ESFIN"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodModelo) & "' AND ""B"".""U_CodEtp"" = '" & Trim(Etapa) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function


    Public Shared Function GetSiHayMasEtapas(CodModelo As String, LineaEtapa As String) As String
        Dim query As String
        Try
            If Trim(LineaEtapa) = "" Then
                LineaEtapa = "0"
            End If

            query = "SELECT COUNT(1) AS ""EXISTE"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & CodModelo & "' AND ""LineId"" > '" & LineaEtapa & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetEstados(Optional Form As String = "") As String
        Dim query As String = String.Empty

        If Trim(Form) = "" Then
            query = "SELECT ""U_Code"" AS ""Code"", ""U_Estado"" AS ""Estado"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFEST"" ORDER BY ""U_Code"" ASC"
        Else
            query = "SELECT ""U_Code"" AS ""Code"", ""U_Estado"" AS ""Estado"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFEST"" WHERE ""U_Form"" = '" & Form & "' ORDER BY ""U_Code"" ASC"
        End If

        Return query

    End Function


    Public Shared Function GetGruposAutorizacion() As String
        Dim query As String

        query = "SELECT ""GroupId"" AS ""Code"",""GroupName"" AS ""Name"" FROM """ & SQLBaseDatos.ToUpper & """.""OUGR"" ORDER BY 1 ASC"

        Return query
    End Function

    Public Shared Function ValidaAlarmasEtapaUsr(CodUsuario As String, Etapa As String, Formulario As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT COUNT(1) AS ""EXISTE"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA"" WHERE ""U_CodUsr"" = '" & CodUsuario & "' AND ""U_EtapaC"" = '" & Etapa & "' AND ""U_Form"" = '" & Formulario & "'"
            'cnp20190710
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function PopulaAlarmas(Formulario As String) As String
        Dim query As String = String.Empty

        Try

            query = "SELECT IFNULL(""U_CodUsr"",'') AS ""CODIGOUSUARIO"",IFNULL(""U_NomUsr"",'') AS ""NOMBREUSUARIO"",IFNULL(""U_EtapaC"",'') AS ""CODIGOETAPA"",IFNULL(""U_EtapaN"",'') AS ""NOMBREETAPA"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA"" WHERE ""U_Form"" = '" & Formulario & "'"
            'cnp20190710
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Public Shared Function PopulaGridEtpAut(Optional Formulario As String = "") As String
        Dim query As String = String.Empty
        Dim cond As Boolean = False

        Try
            query = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY ""A"".""Code"" ASC) AS INT) AS ""FilNum"", ""A"".""U_Form"" AS ""Formulario"", ""B"".""U_Estado"" AS ""Estado"", ""C"".""U_Accion"" AS ""Acción_Permitida"", ""A"".""U_NroOrd"" AS ""Orden"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAP"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFEST"" ""B"" ON ""A"".""U_CodeEst"" = ""B"".""Code"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOACCN"" ""C"" ON ""A"".""U_CodeAcc"" = ""C"".""Code"""

            If Not (Formulario = Nothing Or Formulario = "") Then
                cond = True
                If cond = True Then
                    query = query + " WHERE "
                End If
                query = query + " ""A"".""U_Form"" = '" & Formulario & "' "
            Else
                cond = False
            End If

            Return query

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function GetUsrEtapaAlarma(CodEtapa As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT IFNULL(""B"".""U_CodUsr"",'') AS ""USR"" , IFNULL(""B"".""U_NomUsr"",'') AS ""USRN"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAP"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAL"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA"" ""C"" ON (""A"".""DocEntry"" = ""C"".""U_EtapaC"" AND ""B"".""U_CodUsr"" = ""C"".""U_CodUsr"") WHERE ""A"".""U_Activo"" = 'Y' AND ""B"".""U_Alerta"" = 'Y' AND ""A"".""DocEntry"" = '" & Trim(CodEtapa) & "'"
            'cnp20190710

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function



    Public Shared Function GetUsrsEtapa(codEtapa As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""B"".""U_CodUsr"",'') AS ""USR"" , IFNULL(""B"".""U_NomUsr"",'') AS ""USRN"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAP"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAL"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""U_Activo"" = 'Y' AND ""A"".""DocEntry"" = '" & codEtapa & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function


    Public Shared Function GetNroAprEtapa(codEtapa As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""A"".""U_NroA"",0) AS ""NROAPR"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAP"" ""A"" WHERE ""A"".""U_Activo"" = 'Y' AND ""A"".""DocEntry"" = '" & codEtapa & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetNroRechEtapa(codEtapa As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""A"".""U_NroR"",0) AS ""NRORECH"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOETAP"" ""A"" WHERE ""A"".""U_Activo"" = 'Y' AND ""A"".""DocEntry"" = '" & codEtapa & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function


    Public Shared Function GetSiFormTieneModelo(formulario As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT COUNT(1) AS ""EXISTE"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" WHERE ""U_Form"" = '" & formulario & "' AND ""U_Activo"" = 'Y'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetModeloAutForm(formulario As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""DocEntry"",0) AS ""CodModelo"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" WHERE ""U_Form"" = '" & formulario & "' AND ""U_Activo"" = 'Y' LIMIT 1"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetSiCondMod(CodModelo As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""U_EsConCod"",'N') AS ""CONCOND"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" WHERE ""DocEntry"" = '" & Trim(CodModelo) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function


    Public Shared Function GetEtapasConsMod(CodModelo As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT IFNULL(""U_EtpCon"",'N') AS ""ETPCON"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" WHERE ""DocEntry"" = '" & Trim(CodModelo) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetEstadoFinalEtapaMod(CodModelo As String, CodEtapa As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT IFNULL(""B"".""U_EstadoF"",'') AS ""ESTADOFIN"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOE"" ""B"" ON ""A"".""DocEntry"" = ""B"".""DocEntry"" WHERE ""A"".""DocEntry"" = '" & Trim(CodModelo) & "' AND ""B"".""U_CodEtp"" = '" & Trim(CodEtapa) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function GetUsrFromLogAut(Formulario As String, DocEntryForm As String, PresEntry As String, Etapa As String, Usuario As String, CorrelativoSol As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT  COUNT(1) AS ""VECES"" " + vbCrLf +
            "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOLOG""" + vbCrLf +
"WHERE ""U_Form"" = '" & Formulario & "' " + vbCrLf +
"AND ""U_DocEntry"" = '" & DocEntryForm & "' " + vbCrLf +
"AND ""U_PresEntry"" = '" & PresEntry & "' " + vbCrLf +
"AND ""U_Etapa"" = '" & Etapa & "' " + vbCrLf +
"AND ""U_AutID"" = '" & Usuario & "' " + vbCrLf +
"AND ""U_CorreSol"" = '" & CorrelativoSol & "' " + vbCrLf +
"AND ""U_CodeAcc"" <> 'S'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function GetFromLogAut(Formulario As String, DocEntryForm As String, Etapa As String, CodAutorizacion As String, CodEstadoProc As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT COUNT(1) AS ""AUTORIZACIONES"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOLOG"" WHERE ""U_Form"" = '" & Trim(Formulario) & "' AND ""U_DocEntry"" = '" & Trim(DocEntryForm) & "' AND ""U_Etapa"" = '" & Trim(Etapa) & "' AND ""U_CodeAcc"" = '" & Trim(CodAutorizacion) & "' AND ""U_CodEstado"" = '" & Trim(CodEstadoProc) & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function

    Public Shared Function PopulaGridHisA(formulario As String, docentry As String) As String
        Dim query As String = String.Empty
        Try

            query = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY ""Code"" ASC) AS INT) AS ""FilNum"", ""U_PresEntry"" AS ""Obra"",""U_Correlativo"" AS ""Correlativo"",""U_CodEstado"" AS ""CodigoEstado"",""U_DesEstado"" AS ""Estado"",""U_CodeAcc"" AS ""CodigoAccion"",""U_DesAcc"" AS ""Accion"",""U_Etapa"" AS ""Etapa"",""U_AutID"" AS ""CodigoUsr"",""U_AutNom"" AS ""NombreUsr"",""U_FechaAut"" AS ""Fecha"",""U_CorreSol"" AS ""CorrelativoSol"",""U_MotivoRec"" AS ""MotivoRechazo"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOLOG"" WHERE ""U_Form"" = '" & formulario & "' AND ""U_DocEntry"" = '" & docentry & "'"

            Return query

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function PopulaGridGrpAut(Optional Formulario As String = "") As String
        Dim query As String = String.Empty
        Dim cond As Boolean = False
        Try
            query = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY ""A"".""Code"" ASC) AS INT) AS ""FilNum"", ""A"".""U_Form"" as ""Formulario"", ""B"".""U_Accion"" AS ""Accion"", ""C"".""GroupName"" AS ""Grupo"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOMOD"" ""A"" INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOACCN"" ""B"" ON (""A"".""U_CodeAcc"" = ""B"".""Code"") INNER JOIN """ & SQLBaseDatos.ToUpper & """.""OUGR"" ""C"" ON ""A"".""U_GrpUser"" = ""C"".""GroupId"""


            If Not (Formulario = Nothing Or Formulario = "") Then
                cond = True
                If cond = True Then
                    query = query + " WHERE "
                End If
                query = query + " ""A"".""U_Form"" = '" & Formulario & "' "
            Else
                cond = False
            End If

            Return query
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function GetProyectoUsrActivo(UserCode As String, ProjectCode As String) As String
        Dim query As String = String.Empty

        Try
            query = "SELECT COUNT(1) AS ""EXISTE""  FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY"" WHERE ""U_UsrCode"" = '" & Trim(UserCode) & "' AND ""U_ProjectC"" = '" & Trim(ProjectCode) & "' AND ""U_Enabled"" = 'Y'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function



    Public Shared Function UpdateUsrProy(CodeTBL As String, NameTBL As String, Enabled As String) As Boolean
        Dim sSQLUpdate As String = String.Empty

        Try
            sSQLUpdate = "UPDATE """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY""" + vbCrLf +
                         "SET ""U_Enabled"" = '" & Trim(Enabled) & "'" + vbCrLf +
                         "WHERE ""Code"" = '" & Trim(CodeTBL) & "' AND ""Name"" = '" & Trim(NameTBL) & "'"

            Try
                conexi.creaRegistro(sSQLUpdate, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function InsertaUsrProy(UsrCode As String, UsrName As String, Enabled As String, ProjectCode As String, ProjectName As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY""" + vbCrLf +
"(""Code"",""Name"",""U_UsrCode"",""U_UsrName"",""U_Enabled"",""U_ProjectC"",""U_ProjectN"")" + vbCrLf +
"SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code " + vbCrLf +
",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name" + vbCrLf +
",'" & Trim(UsrCode) & "'" + vbCrLf +
",'" & Trim(UsrName) & "'" + vbCrLf +
",'" & Trim(Enabled) & "'" + vbCrLf +
",'" & Trim(ProjectCode) & "'" + vbCrLf +
",'" & Trim(ProjectName) & "'" + vbCrLf +
"FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY"""


            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function InsertaUsrAlarmas(CodUsr As String, NomUsr As String, Formulario As String, CodEtapa As String, NomEtapa As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA""" + vbCrLf +
"(""Code"",""Name"",""U_CodUsr"",""U_NomUsr"",""U_Form"",""U_EtapaC"",""U_EtapaN"")" + vbCrLf +
"SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code " + vbCrLf +
",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name" + vbCrLf +
",'" & CodUsr & "'" + vbCrLf +
",'" & NomUsr & "'" + vbCrLf +
",'" & Formulario & "'" + vbCrLf +
",'" & CodEtapa & "'" + vbCrLf +
",'" & NomEtapa & "'" + vbCrLf +
"FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA"""

            'cnp20190710
            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function BorraAlarmaUsr(CodUsuario As String, Etapa As String, Formulario As String) As Boolean
        Dim sSQLDelete As String = String.Empty

        Try

            sSQLDelete = "DELETE FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOUSRA"" WHERE ""U_CodUsr"" = '" & Trim(CodUsuario) & "' AND ""U_EtapaC"" = '" & Trim(Etapa) & "'  AND ""U_Form"" = '" & Trim(Formulario) & "'"
            'cnp20190710
            Try
                conexi.creaRegistro(sSQLDelete, False)

            Catch ex As Exception
                Throw ex
                Return False
            End Try
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function PopulaFest(Formulario As String) As String
        Return _
            "SELECT ""U_Code"" AS ""Code"", ""U_Estado"" AS ""Estado"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOFEST"" WHERE ""U_Form"" = '" & Formulario & "' ORDER BY ""U_Code"" ASC"

    End Function

    Public Shared Function InsertaAutorizacionLog(Formulario As String, DocEntry As String, Presupuesto As String, Correlativo As String, CodeEstado As String, DescrEstado As String, CodeAccion As String, DescrAccion As String, Etapa As String, CodUsr As String, NomUsr As String, Fecha As String, CorrelativoSol As String, motivo As String) As Boolean
        Try
            Dim sSqlInsert As String

            sSqlInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOLOG""" + vbCrLf +
                         "(""Code"",""Name"",""U_Form"",""U_DocEntry"",""U_PresEntry"",""U_Correlativo"",""U_CodEstado"",""U_DesEstado"",""U_CodeAcc"",""U_DesAcc"",""U_Etapa"",""U_AutID"",""U_AutNom"",""U_FechaAut"",""U_CorreSol"",""U_MotivoRec"")" + vbCrLf +
                         "SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code   " + vbCrLf +
                         ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name " + vbCrLf +
                         ",'" & Formulario & "'" + vbCrLf +
                             ",'" & DocEntry & "'" + vbCrLf +
                             ",'" & Presupuesto & "'" + vbCrLf +
                             ",'" & Correlativo & "'" + vbCrLf +
                             ",'" & CodeEstado & "'" + vbCrLf +
                             ",'" & DescrEstado & "'" + vbCrLf +
                             ",'" & CodeAccion & "'" + vbCrLf +
                             ",'" & DescrAccion & "'" + vbCrLf +
                             ",'" & Etapa & "'" + vbCrLf +
                             ",'" & CodUsr & "'" + vbCrLf +
                             ",'" & NomUsr & "'" + vbCrLf +
                             ",'" & Fecha & "'" + vbCrLf +
                             ",'" & CorrelativoSol & "'" + vbCrLf +
                             ",'" & motivo & "'" + vbCrLf +
                             "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_AUTOLOG"""

            Try
                conexi.creaRegistro(sSqlInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function InsertaModificacionLog(Formulario As String, DocEntry As String, CodUsr As String, NomUsr As String, FechaAnt As String, FechaFin As String, FechaMod As String) As Boolean
        Try
            Dim sSqlInsert As String

            sSqlInsert = "INSERT INTO """ & SQLBaseDatos.ToUpper & """.""@EXX_MODFECLOG""" + vbCrLf +
                         "(""Code"",""Name"",""U_Form"",""U_DocEntry"",""U_AutID"",""U_AutNom"",""U_FechaAnt"",""U_FechaFin"",""U_FechaMod"")" + vbCrLf +
                         "SELECT RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Code   " + vbCrLf +
                         ",RIGHT('00000000' || TO_VARCHAR(IFNULL(MAX(TO_INT(""Code"")), 0) + 1), 8) --> Name " + vbCrLf +
                         ",'" & Formulario & "'" + vbCrLf +
                             ",'" & DocEntry & "'" + vbCrLf +
                             ",'" & CodUsr & "'" + vbCrLf +
                             ",'" & NomUsr & "'" + vbCrLf +
                             ",'" & FechaAnt & "'" + vbCrLf +
                              ",'" & FechaFin & "'" + vbCrLf +
                               ",'" & FechaMod & "'" + vbCrLf +
                             "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_MODFECLOG"""

            Try
                conexi.creaRegistro(sSqlInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Friend Shared Function ObtienFechaAnteriorLinCred(code As String) As String
        Dim query As String = String.Empty

        query = "SELECT """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaHa"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"" where """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""Code"" = '" & code & "';"
        Return query

    End Function
End Class

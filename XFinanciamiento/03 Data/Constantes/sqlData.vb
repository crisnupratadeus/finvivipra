﻿Imports XFinanciamiento.Data
Public Class sqlData
    Public Shared conexi As Conexiones = New Conexiones

    Public Const GetParam_TEXT_NAME As String = "GetParam"
    Public Shared Function GetParam_TEXT(Name As String) As String
        Return _
     "select U_Value1 from [@EXX_FIPARAM] WITH (NOLOCK) where U_type='" & Name & "'"

    End Function
    Public Const sSqlCommandDatosDB1_NAME As String = "sSqlCommandDatosDB1"
    Public Shared Function sSqlCommandDatosDB1_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String
        Return _
        "DELETE  ""@EXX_FIPARAM""  WHERE U_Type = '" & Type & "' and U_Value1='" & valor1 & "' and U_Value3='" & valor2 & "'"
    End Function
    Public Const sSqlCommandDatosDB2_NAME As String = "sSqlCommandDatosDB2"
    Public Shared Function sSqlCommandDatosDB2_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String
        Return _
        "DELETE  [@EXX_FIPARAM] WHERE U_Type = '" & Type & "'"
    End Function
    Public Const SqlInsertDatosDB_NAME As String = "SqlInsertDatosDB"
    Public Shared Function SqlInsertDatosDB_TEXT(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "") As String

        Dim sSqlInsert As String = "INSERT INTO dbo.[@EXX_FIPARAM]                                                                 " + vbCrLf +
                "             (                                                                                                    " + vbCrLf +
                "               Code,                                                                                              " + vbCrLf +
                "  	            Name,                                                                                              " + vbCrLf +
                "              U_Type,                                                                                            " + vbCrLf +
                "              U_Value1,                                                                                           " + vbCrLf +
                "              U_Value2,                                                                                            " + vbCrLf +
                "              U_Active,                                                                                           " + vbCrLf +
                "              U_UserName,                                                                                           " + vbCrLf +
                "              U_CreateDate                                                                                           " + vbCrLf +
                "             )                                                                                                    " + vbCrLf +
                "             SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code   " + vbCrLf +
                "             ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name        " + vbCrLf

        sSqlInsert = sSqlInsert & "'" & Type & "', '" & valor1 & "', '" & valor2 & "'," & "1, '" & DiApp.UserName & "', '" & DateToString(Today) & "' from dbo.[@EXX_FIPARAM]"

        Return sSqlInsert
    End Function
    Public Shared Function DateToString(dDate As Date, Optional withHour As Boolean = False) As String
        Try
            Dim strDate As String = dDate.Year & Right(CStr("0" & dDate.Month), 2) & Right(CStr("0" & dDate.Day), 2)
            If withHour = True Then
                strDate = " " & strDate & Right(CStr("0" & dDate.Hour), 2) & ":" & Right(CStr("0" & dDate.Minute), 2)
            End If

            Return strDate

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetEstadosCmb(Formulario As String) As String
        Dim query As String = String.Empty

        Try

            query = "SELECT U_Code Code,U_Estado ""Estado"" FROM [@EXX_AUTOFEST] WHERE U_Form = '" & Trim(Formulario) & "'"

            Return query

        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetAnexosLineaDeCredito(objeto As String, nroDoc As String) As String

        Dim query As String = String.Empty

        query = "select CAST(ROW_NUMBER() over (order by Code ASC)as int) as FilNum,U_Path as Ruta,U_AttDate as Fecha,CAST(Code as int) as Code, CAST(Name as int) as Name, U_DocCode from [@EXX_IATT] where U_ObjCode = '" & objeto & "' and U_DocCode = '" & nroDoc & "'"

        Return query

    End Function

    Public Shared Function GetMaxCodeLinCred() As String
        Dim query As String = String.Empty

        query = "select ISNULL(Max(CAST(Code as int)),0) as Code from [@EXX_IATT]"

        Return query
    End Function

    Public Shared Function PopulaGridChecklist(DocEntry As String) As String

        Dim query As String = String.Empty

        Try

            If Trim(DocEntry) = "" Then
                query = "SELECT A.Code AS CODIGO,A.Name AS DESCRIPCION,ISNULL(B.U_Realizado,'N') AS REALIZADO,ISNULL(B.Code,-1) AS LINEABD,ISNULL(B.U_CodChk,'') AS CODIGOBD,ISNULL(B.U_DesChk,'') AS DESCRBD FROM [@EXX_LICHK] A LEFT JOIN [@EXX_LICR1] B ON (A.Code = ISNULL(B.U_CodChk,'')  AND ISNULL(B.U_CodLC,'-1') = '-2') ORDER BY 1 ASC"

            Else
                query = "SELECT A.Code AS CODIGO,A.Name AS DESCRIPCION,ISNULL(B.U_Realizado,'N') AS REALIZADO,ISNULL(B.Code,-1) AS LINEABD,ISNULL(B.U_CodChk,'') AS CODIGOBD,ISNULL(B.U_DesChk,'') AS DESCRBD FROM [@EXX_LICHK] A LEFT JOIN [@EXX_LICR1] B ON (A.Code = ISNULL(B.U_CodChk,'')  AND ISNULL(B.U_CodLC,'-1') = '" & Trim(DocEntry) & "') ORDER BY 1 ASC"

            End If



            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function
    ' ssh
    Public Shared Function PopulaGridTasas(DocEntry As String) As String ' ssh

        Dim query As String = String.Empty

        Try

            If Trim(DocEntry) = "" Then
                query = "SELECT A.U_CodLC AS CODIGO, A.U_CodGrupArt AS CODGRUPART, ISNULL(B.ItmsGrpNam,'') AS DESCRIPCION, ISNULL(A.U_TasaNor,0) AS TASANOR, ISNULL(A.U_TasaLeg,0) AS TASALEG FROM [@EXX_LICR2] A INNER JOIN OITB B ON B.ItmsGrpCod = A.U_CodGrupArt AND ISNULL(A.U_CodLC,'-1') = '-2' ORDER BY 1 ASC"
            Else
                query = "SELECT A.U_CodLC AS CODIGO, A.U_CodGrupArt AS CODGRUPART, ISNULL(B.ItmsGrpNam,'') AS DESCRIPCION, ISNULL(A.U_TasaNor,0) AS TASANOR, ISNULL(A.U_TasaLeg,0) AS TASALEG FROM [@EXX_LICR2] A INNER JOIN OITB B ON B.ItmsGrpCod = A.U_CodGrupArt AND ISNULL(A.U_CodLC,'-1') = '" & Trim(DocEntry) & "' ORDER BY 1 ASC"
            End If

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try


    End Function
    ' ssh
    Public Shared Function GetDescGrupoArt(CodGrupArt As String) As String
        Return _
     "SELECT ItmsGrpNam FROM OITB where ItmsGrpCod='" & Trim(CodGrupArt) & "'"

    End Function

    Public Shared Function InsertaChecklist(CodigoLC As String, CodChk As String, DesChk As String, Realizado As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO [@EXX_LICR1]" + vbCrLf +
"(Code,Name,U_CodLC,U_CodChk,U_DesChk,U_Realizado)" + vbCrLf +
"SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code" + vbCrLf +
",right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Name" + vbCrLf +
",'" & Trim(CodigoLC) & "'" + vbCrLf +
",'" & Trim(CodChk) & "'" + vbCrLf +
",'" & Trim(DesChk) & "'" + vbCrLf +
",'" & Trim(Realizado) & "'" + vbCrLf +
"FROM [@EXX_LICR1]"


            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    ' ssh
    Public Shared Function LimpiarTasasGrupArt(CodigoLC As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "DELETE [@EXX_LICR2] WHERE U_CodLC = '" & Trim(CodigoLC) & "'"
            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    ' ssh
    Public Shared Function InsertaTasasGrupArt(CodigoLC As String, CodGrupArt As String, TasaNor As String, TasaLeg As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO [@EXX_LICR2]" + vbCrLf +
"(Code,Name,U_CodLC,U_CodGrupArt,U_TasaNor,U_TasaLeg)" + vbCrLf +
"SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code" + vbCrLf +
",right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Name" + vbCrLf +
",'" & Trim(CodigoLC) & "'" + vbCrLf +
",'" & Trim(CodGrupArt) & "'" + vbCrLf +
",'" & Trim(TasaNor) & "'" + vbCrLf +
",'" & Trim(TasaLeg) & "'" + vbCrLf +
"FROM [@EXX_LICR2]"

            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function UpdateChecklist(CodeTBL As String, Realizado As String) As Boolean
        Dim sSQLUpdate As String = String.Empty

        Try
            sSQLUpdate = "UPDATE [@EXX_LICR1]" + vbCrLf +
                         "SET U_Realizado = '" & Trim(Realizado) & "'" + vbCrLf +
                         "WHERE Code = '" & Trim(CodeTBL) & "'"

            Try
                conexi.creaRegistro(sSQLUpdate, False)
            Catch ex As Exception
                Throw ex
                Return False
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Shared Function InsertaCuotaInteres(DocEntryOV As String, FechaVencimiento As String, DocEntryPG As String, TotalCuota As String, CapitalxPagar As String, Amortizacion As String) As Boolean
        Dim sSQLInsert As String = String.Empty

        Try
            sSQLInsert = "INSERT INTO [@EXX_CUOIN]" + vbCrLf +
                            "(Code, Name, U_DocEntryOV, U_FechaV, U_DocEntryPG, U_TotalCuota, U_CapitalxPagar, U_Amortizacion)" + vbCrLf +
                            "SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code" + vbCrLf +
                            ",right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Name" + vbCrLf +
                            ",'" & Trim(DocEntryOV) & "'" + vbCrLf +
                            ",CONVERT(DATETIME,'" & Trim(FechaVencimiento) & "',103)" + vbCrLf +
                            ",'" & Trim(DocEntryPG) & "'" + vbCrLf +
                            ",'" & Trim(TotalCuota) & "'" + vbCrLf +
                            ",'" & Trim(CapitalxPagar) & "'" + vbCrLf +
			    ",'" & Trim(Amortizacion) & "'" + vbCrLf +
                            "FROM [@EXX_CUOIN]"

            Try
                conexi.creaRegistro(sSQLInsert, False)
            Catch ex As Exception
                Throw ex
                Return False
                Exit Function
            End Try

            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function ValidaCodigoPlantilla(Codigo As String) As String

        Dim query As String = String.Empty

        Try
            query = "SELECT COUNT(1) EXISTE FROM [@EXX_FIPLAN] WHERE ISNULL(U_Codigo,'') = '" & Trim(Codigo) & " '"
            Return query
        Catch ex As Exception
            Throw ex
            Return "-1"
        End Try


    End Function
    Public Shared Function GetParamOV_TEXT(DocEntryOV As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT DocTotal, DocDate, DocTotalFC, DocTotalSy, CurSource FROM ORDR WHERE DocEntry = '" & DocEntryOV & "'" 'mauricio
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetParamProcedureCuotas_TEXT(Cuotas As String, Tasa As String, Anticipo As String, Lapso As Integer, Original As Integer, CapitalTotal As String, FechaDoc As String, CapPago As Integer) As String
        Return _
        "EXEC [dbo].sp_Calculo_Cuotas " &
                          " @Capital = '" & CapitalTotal & "'" &
                          ", @Cuotas = '" & Cuotas & "'" &
                          ", @Tasa = '" & Tasa & "'" &
                          ", @Lapso = '" & Lapso & "'" &
                          ", @Fecha_Ini = '" & FechaDoc & "'" &
                          ", @Cap_pagado = '" & CapPago & "'" &
                          ", @Cap_Anticipo = '" & Anticipo & "'" &
                          ", @Original = '" & Original & "'" &
                          ", @GastosA = 0"
    End Function
    Public Shared Function GetParamFechaCuota_TEXT(DocEntryOV As String, DocEntryPG As String) As String
        Return _
        "SELECT  CONVERT(varchar(12),U_FechaV, 103) AS U_FechaV FROM [@EXX_CUOIN] WHERE U_DocEntryOV = '" & DocEntryOV & "' AND U_DocEntryPG = '" & DocEntryPG & "' ORDER BY Code ASC"
    End Function
    Public Const fn_Fechas_Cuotas_name As String = "fn_Fechas_Cuotas"
    Public Shared Function fn_Fechas_Cuotas_text() As String
        Dim res As String
        res = "CREATE function fn_Fechas_Cuotas(   " & vbCrLf &
"				 @Cuotas smallint               -- Cantidad de cuotas                  " & vbCrLf &
"				,@Lapso smallint                -- Lapso en días entre cuotas          " & vbCrLf &
"				,@Fecha_Ini smalldatetime       -- Fecha inicio de plan de pagos       " & vbCrLf &
"				)                                                                      " & vbCrLf &
"	    returns  @Fechas_Cuotas table (                                                " & vbCrLf &
"				 Nro smallint                                                          " & vbCrLf &
"				,Fecha_Pago smalldatetime                                              " & vbCrLf &
"				,Dias smallint                                                         " & vbCrLf &
"				)                                                                      " & vbCrLf &
" as                                                                                   " & vbCrLf &
" BEGIN                                                                                " & vbCrLf &
"-- 0. Declaración de variables                                                        " & vbCrLf &
"   Declare @A_plazo_aux          smallint,                                            " & vbCrLf &
"		    @A_fecha_Ini_aux      smalldatetime,                                       " & vbCrLf &
"		    @A_Fecha_hasta        smalldatetime,                                       " & vbCrLf &
"		    @A_dia_aux            smallint,                                            " & vbCrLf &
"		    @Contador             smallint                                             " & vbCrLf &
" -- 1. Asignacion de valores indiciales                                               " & vbCrLf &
"   select @Contador        = 1,                                                       " & vbCrLf &
"          @A_plazo_aux     = @Lapso,                                                  " & vbCrLf &
"		   @A_fecha_Ini_aux = @Fecha_ini,                                              " & vbCrLf &
"		   @A_Fecha_hasta   = DATEADD(day,@Lapso,@A_fecha_Ini_aux),                    " & vbCrLf &
"		   @A_dia_aux       = DAY(@Fecha_ini)                                          " & vbCrLf &
"-- 2.- Cuando es solo una cuota                                                       " & vbCrLf &
"   if @Cuotas = 1                                                                     " & vbCrLf &
"   begin                                                                              " & vbCrLf &
"      set @A_Fecha_hasta = DATEADD(d,@Lapso,@Fecha_Ini)                               " & vbCrLf &
"      insert into @Fechas_Cuotas values(1, @A_Fecha_hasta, @A_plazo_aux)              " & vbCrLf &
"      return                                                                          " & vbCrLf &
"   end                                                                                " & vbCrLf &
"-- 3.- Cuando va más de una cuota                                                     " & vbCrLf &
"   While @Contador <= @Cuotas                                                         " & vbCrLf &
"   Begin		                                                                       " & vbCrLf &
"      set @A_Fecha_hasta = DATEADD(day,@Lapso,@A_fecha_Ini_aux)                       " & vbCrLf &
"      if @Lapso % 30 = 0 -- multiplo de 30                                            " & vbCrLf &
"      begin                                                                           " & vbCrLf &
"	     if DAY(@Fecha_ini) > 30                                                       " & vbCrLf &
"		   set @A_dia_aux =  day(dateadd(ms,-3,DATEADD(mm, DATEDIFF(m,0,cast(-- Año    " & vbCrLf &
"		   case when month(@A_Fecha_hasta) = month(@A_fecha_Ini_aux) And  month(@A_Fecha_hasta) = 12 then  str(year(@A_Fecha_hasta)+1,4) else str(year(@A_Fecha_hasta),4) end + ''  " & vbCrLf &
"           -- Mes                      " & vbCrLf &
"          + case when month(@A_Fecha_hasta) <> month(@A_fecha_Ini_aux) Or @Lapso = 360 " & vbCrLf &
"          then (case when month(@A_Fecha_hasta) < 10 then '0'+str(month(@A_Fecha_hasta),1) else str(month(@A_Fecha_hasta),2) end)                                                   " & vbCrLf &
"          else case when month(@A_Fecha_hasta) < 12 then case when month(@A_Fecha_hasta)+1 < 10 then  '0' + str(month(@A_Fecha_hasta)+1,1) else  str(month(@A_Fecha_hasta)+1,2)  end  else '01' end end + ''  " & vbCrLf &
"          -- Dia                       " & vbCrLf &
"          + '01'                       " & vbCrLf &
"          as smalldatetime) )+1, 0)))  " & vbCrLf &
"          select @A_Fecha_hasta = cast(-- Año                                            " & vbCrLf &
"		    case when month(@A_Fecha_hasta) = month(@A_fecha_Ini_aux) and  month(@A_Fecha_hasta) = 12 then  str(year(@A_Fecha_hasta)+1,4) else str(year(@A_Fecha_hasta),4) end + ''  " & vbCrLf &
"			-- Mes                        " & vbCrLf &
"            + case when month(@A_Fecha_hasta) <> month(@A_fecha_Ini_aux) or @Lapso = 360  " & vbCrLf &
"           then (case when month(@A_Fecha_hasta) < 10 then '0'+str(month(@A_Fecha_hasta),1) else str(month(@A_Fecha_hasta),2) end)                                                  " & vbCrLf &
"           else case when month(@A_Fecha_hasta) < 12 then case when month(@A_Fecha_hasta)+1 < 10 then  '0' + str(month(@A_Fecha_hasta)+1,1) else  str(month(@A_Fecha_hasta)+1,2)  end  else '01' end end + ''  " & vbCrLf &
"           -- Dia                        " & vbCrLf &
"           + case when ((month(@A_Fecha_hasta) <> month(@A_fecha_Ini_aux) and month(@A_Fecha_hasta) = 2) or (month(@A_Fecha_hasta) = month(@A_fecha_Ini_aux) and month(@A_Fecha_hasta)+ 1 = 2)) and @A_dia_aux > 28 then '28'  " & vbCrLf &
"           else                          " & vbCrLf &
"			case when @A_dia_aux < 10 then '0'+ str(@A_dia_aux,1) else  str(@A_dia_aux,2) end end  " & vbCrLf &
"           as smalldatetime)            " & vbCrLf &
"         set @A_plazo_aux = datediff(d, @A_fecha_Ini_aux, @A_Fecha_hasta)	 " & vbCrLf &
"         if @A_plazo_aux < @Lapso - 5   " & vbCrLf &
"         begin                          " & vbCrLf &
"            select @A_Fecha_hasta = cast(-- Año   " & vbCrLf &
"            case when year(@A_Fecha_hasta) = year(@A_fecha_Ini_aux) And  month(@A_Fecha_hasta) = 12 then  str(year(@A_Fecha_hasta)+1) else str(year(@A_Fecha_hasta)) end +''  " & vbCrLf &
"            -- Mes                      " & vbCrLf &
"            + case when month(@A_Fecha_hasta) <> month(@A_fecha_Ini_aux) Or @Lapso = 360  " & vbCrLf &
"              then case when month(@A_Fecha_hasta) < 12 then case when month(@A_Fecha_hasta)+1 < 10 then '0' + str(month(@A_Fecha_hasta)+1,1) else str(month(@A_Fecha_hasta)+1,2) end  " & vbCrLf &
"              else '01' end              " & vbCrLf &
"              else case when month(@A_Fecha_hasta) < 12 then case when month(@A_Fecha_hasta)+2 < 10 then '0' + str(month(@A_Fecha_hasta)+2,1) else str(month(@A_Fecha_hasta)+2,2) end  " & vbCrLf &
"              else '02' end end + ''     " & vbCrLf &
"              -- Dia                     " & vbCrLf &
"              + case when ((month(@A_Fecha_hasta) <> month(@A_fecha_Ini_aux) And month(@A_Fecha_hasta)+1 = 2) Or  (month(@A_Fecha_hasta) = month(@A_fecha_Ini_aux) And month(@A_Fecha_hasta)+2 = 2) )    And day(@A_fecha_Ini_aux) > 28 then '28'  " & vbCrLf &
"              else case when  day(@A_fecha_Ini_aux) < 10 then '0'+ str(day(@A_fecha_Ini_aux),1) else  str(day(@A_fecha_Ini_aux),2) end end  " & vbCrLf &
"              as smalldatetime)          " & vbCrLf &
"            set @A_plazo_aux = datediff(d, @A_fecha_Ini_aux, @A_Fecha_hasta)     " & vbCrLf &
"        end -- if @A_plazo_aux < @Lapso - 5                                      " & vbCrLf &
"		 else                                                                     " & vbCrLf &
"		     if ((MONTH(@A_fecha_Ini_aux) = 1 And @Lapso = 30) Or (MONTH(@A_fecha_Ini_aux) = 2 And @Lapso = 360)    " & vbCrLf &
"			   or (MONTH(@A_fecha_Ini_aux) = 12 and @Lapso = 60) or (MONTH(@A_fecha_Ini_aux) = 11 and @Lapso = 90)  " & vbCrLf &
"			   Or (MONTH(@A_fecha_Ini_aux) = 8 And @Lapso = 180) )                                                  " & vbCrLf &
"			  and DAY (@Fecha_ini) in (30, 31)                                                                      " & vbCrLf &
"		       begin                                                               " & vbCrLf &
"                 select @A_Fecha_hasta = cast(str(year(@A_Fecha_hasta),4) +'0228'  as smalldatetime)  " & vbCrLf &
"                 set @A_plazo_aux = datediff(d, @A_fecha_Ini_aux, @A_Fecha_hasta)                     " & vbCrLf &
"               end                                                                " & vbCrLf &
"      end -- if @Lapso % 30 = 0                                                   " & vbCrLf &
"      insert into @Fechas_Cuotas values(@Contador, @A_Fecha_hasta, @A_plazo_aux)  " & vbCrLf &
"      select @A_fecha_Ini_aux = @A_Fecha_hasta                                    " & vbCrLf &
"      set @Contador = @Contador + 1                                               " & vbCrLf &
"   End -- While @Contador <= @Cuotas                                              " & vbCrLf &
"  return  " & vbCrLf &
"END   "
        Return res
    End Function


    Public Const calculo_interes_name As String = "calculo_interes"
    Public Shared Function calculo_interes_text() As String
        Dim res As String
        res = "CREATE PROCEDURE calculo_interes (" & vbCrLf &
"                       @Saldo        Decimal(13,2) = 0," & vbCrLf &
"					   @Fecha_desde  smalldatetime," & vbCrLf &
"					   @Fecha_hasta  smalldatetime," & vbCrLf &
"					   @Tasa         Decimal(8,4)  = 0," & vbCrLf &
"                      @Interes      Decimal(13,2) = 0  OUTPUT," & vbCrLf &
"                       @error_msg    varchar(300)  = '' OUTPUT)" & vbCrLf &
"       AS " & vbCrLf &
"       set nocount on" & vbCrLf &
"       select @error_msg     = ''" & vbCrLf &
"  IF @Saldo < 0" & vbCrLf &
"          begin" & vbCrLf &
"            set @error_msg ='Valor del Saldo invalido, verifique.'" & vbCrLf &
"            GOTO LINEA_Error " & vbCrLf &
"          end" & vbCrLf &
"       if @Fecha_desde Is null" & vbCrLf &
"	    begin" & vbCrLf &
"            set @error_msg ='Fecha Desde para el cálculo inválido, verifique.'" & vbCrLf &
"            GOTO LINEA_Error " & vbCrLf &
"        end" & vbCrLf &
"		if @Fecha_hasta is null" & vbCrLf &
"	    begin" & vbCrLf &
"            set @error_msg ='Fecha Hasta para el cálculo inválido, verifique.'" & vbCrLf &
"            GOTO LINEA_Error " & vbCrLf &
"        end" & vbCrLf &
"		if @Fecha_desde > @Fecha_hasta" & vbCrLf &
"		 begin" & vbCrLf &
"            set @error_msg ='Fecha Inicio no puede posterior a la Fecha Final, verifique.'" & vbCrLf &
"            GOTO LINEA_Error " & vbCrLf &
"        end" & vbCrLf &
"        		if @Tasa < 0 " & vbCrLf &
"		begin" & vbCrLf &
"            set @error_msg ='Tasa para el cálculo inválido, verifique.'" & vbCrLf &
"            GOTO LINEA_Error " & vbCrLf &
"        end" & vbCrLf &
"if @Tasa <> 0 And @Saldo <> 0" & vbCrLf &
"		SET @interes = ROUND((@saldo * (@tasa/100.0000) * DATEDIFF(DAY, @fecha_desde, @fecha_hasta)) /360.0000, 2)" & vbCrLf &
"		else " & vbCrLf &
"		SET @interes = 0" & vbCrLf &
"LINEA_salir_procedimiento:" & vbCrLf &
"    return 0" & vbCrLf &
"LINEA_error:" & vbCrLf &
"    set @error_msg = rtrim(@error_msg) " & vbCrLf &
"    return -1"


        Return res
    End Function

    Public Const Cuota_Fija_name As String = "Cuota_Fija"
    Public Shared Function Cuota_Fija_text() As String
        Dim res As String
        res = "CREATE PROCEDURE Cuota_Fija ( " & vbCrLf &
        "              @Capital        decimal(13,2) = 0, " & vbCrLf &
        "			   @NroCuotas      decimal(16,10) = 0, " & vbCrLf &
        "			   @Lapso          decimal(16,10) = 0, " & vbCrLf &
        "			   @Tasa           decimal(16,10), " & vbCrLf &
        "			   @Fecha_Inicial  smalldatetime, " & vbCrLf &
        "              @CuotaFija      decimal(13,2) = 0  OUTPUT, " & vbCrLf &
        "              @error_msg      varchar(300)  = '' OUTPUT) " & vbCrLf &
      "  AS " & vbCrLf &
      " set nocount on " & vbCrLf &
      "       Declare @A_factor                      decimal(16,10), " & vbCrLf &
      "				 @A_cuota_cap                   decimal(12,2),  " & vbCrLf &
      "				 @A_F1                          decimal(16,10), " & vbCrLf &
      "			     @A_F2                          decimal(16,10), " & vbCrLf &
      "              @A_cont                        smallint = 1, " & vbCrLf &
      "              @TasaAux                       decimal(16,10), " & vbCrLf &
      "				 @TasaEq                        decimal(16,10) " & vbCrLf &
      "    Select @error_msg     = '' " & vbCrLf &
      " ----------------------------------------------------------------------------- " & vbCrLf &
      " -- Validar datos Ingresados -- " & vbCrLf &
      " ----------------------------------------------------------------------------- " & vbCrLf &
      " If @Capital <= 0 " & vbCrLf &
      "   begin " & vbCrLf &
      "     set @error_msg ='Valor del Capital invalido, verifique.' " & vbCrLf &
      "     GOTO LINEA_Error  " & vbCrLf &
      "   end " & vbCrLf &
     "  If @NroCuotas <= 0 " & vbCrLf &
     "  begin " & vbCrLf &
     "      set @error_msg ='Nro Cuotas inválido, verifique.' " & vbCrLf &
     "      GOTO LINEA_Error  " & vbCrLf &
     "  end " & vbCrLf &
     "  If @Lapso <= 0 " & vbCrLf &
     "  begin " & vbCrLf &
     "      set @error_msg ='Lapso para el cálculo inválido, verifique.' " & vbCrLf &
     "      GOTO LINEA_Error  " & vbCrLf &
     "  end " & vbCrLf &
     "  If @Tasa < 0 " & vbCrLf &
     "    begin  " & vbCrLf &
     "       set @error_msg ='Tasa para el cálculo inválido, verifique.' " & vbCrLf &
     "      GOTO LINEA_Error  " & vbCrLf &
     "  end " & vbCrLf &
     "	---------------------- " & vbCrLf &
     "   -- Calcular Interes " & vbCrLf &
     "  ---------------------- " & vbCrLf &
     "  If @Tasa > 0 " & vbCrLf &
     "	begin " & vbCrLf &
     "	   set @TasaEq = @Tasa " & vbCrLf &
     "     set @TasaAux = @Tasa/100.00000000 " & vbCrLf &
     "     set @TasaEq = @Tasa / (360.00000000/@Lapso) " & vbCrLf &
     "	   select @A_F1 = ( power(1 + (@TasaEq/100.0000000), @NroCuotas)) " & vbCrLf &
     "     select @A_F2 = (power(1 + (@TasaEq/100.00000000), @NroCuotas) - 1) " & vbCrLf &
     "     select @A_factor = (@TasaEq/100.00000000)* (@A_F1 / @A_F2) " & vbCrLf &
     "	   set @CuotaFija = round(@Capital * @A_factor,2) " & vbCrLf &
     "  End " & vbCrLf &
     "	else " & vbCrLf &
     "	   set @CuotaFija = @Capital / @NroCuotas " & vbCrLf &
    " LINEA_salir_procedimiento: " & vbCrLf &
    " return 0 " & vbCrLf &
    " LINEA_error: " & vbCrLf &
    " set @error_msg = rtrim(@error_msg) " & vbCrLf &
    " return -1 "
        Return res
    End Function

    Public Const Calculo_Multa_name As String = "SP_Calculo_Multa"
    Public Shared Function Calculo_Multa_text() As String
        Dim res As String
        res = " CREATE Procedure SP_Calculo_Multa                   " & vbCrLf &
"                  @FechaCuotaVen smalldatetime                     " & vbCrLf &
"				 ,@Fecha_pago smalldatetime                         " & vbCrLf &
"				 ,@TotalImpMult decimal(13,2) = 0 output            " & vbCrLf &
"As                                                                 " & vbCrLf &
"-- 0. Declaración de variables                                     " & vbCrLf &
"declare @PerGra   int,                                             " & vbCrLf &
"        @PerGraTip char(1),                                        " & vbCrLf &
"		 @TotalPerGra int,                                          " & vbCrLf &
"		 @Multa decimal(13,2),                                      " & vbCrLf &
"		 @MultaTip char(2),                                         " & vbCrLf &
"		 @CicloMulta int,                                           " & vbCrLf &
" 		 @DiasVen      int                                          " & vbCrLf &
"		set @TotalImpMult= 0                                        " & vbCrLf &
"			SELECT @PerGra = U_Value1                               " & vbCrLf &
"			FROM [@EXX_FIPARAM]                                     " & vbCrLf &
"			where U_Type = 'UD_Per_Gra'                             " & vbCrLf &
"			SELECT @PerGraTip = U_Value1                            " & vbCrLf &
"			FROM [@EXX_FIPARAM]                                     " & vbCrLf &
"			where U_Type = 'UD_PG_Tipo'                             " & vbCrLf &
"			SELECT @Multa = U_Value1                                " & vbCrLf &
"			FROM  [@EXX_FIPARAM]                                    " & vbCrLf &
"			where U_Type = 'UD_Multa_F'                             " & vbCrLf &
"			SELECT @MultaTip = U_Value1                             " & vbCrLf &
"			FROM  [@EXX_FIPARAM]                                    " & vbCrLf &
"			where U_Type = 'UD_Mult_Ti'                             " & vbCrLf &
"		select @TotalPerGra = case @PerGraTip when 'D' then 1       " & vbCrLf &
"		                                      when 'S' then 7       " & vbCrLf &
"											  when 'M' then 30      " & vbCrLf &
"											  else 1 end            " & vbCrLf &
"        set @TotalPerGra = @TotalPerGra * @PerGra                  " & vbCrLf &
"		select @CicloMulta = case @MultaTip when 'D' then 1         " & vbCrLf &
"		                                     when 'S' then 7        " & vbCrLf &
"											 when 'M' then 30       " & vbCrLf &
"											 when 'T' then 90       " & vbCrLf &
"											 when 'SM' then 180     " & vbCrLf &
"											 when 'A' then 360      " & vbCrLf &
"											 else 1 end             " & vbCrLf &
"	set @DiasVen = DATEDIFF (d, @FechaCuotaVen, @Fecha_pago)        " & vbCrLf &
"	if @FechaCuotaVen < @Fecha_pago                                 " & vbCrLf &
"	   and @PerGra > 0 and @Multa > 0                               " & vbCrLf &
"	   and @DiasVen > @TotalPerGra                                  " & vbCrLf &
"	begin	                                                        " & vbCrLf &
"		set @TotalImpMult = ((@DiasVen-@TotalPerGra)/@CicloMulta) * @Multa  " & vbCrLf &
"	end                                                             " & vbCrLf &
"  return 0                                                         " & vbCrLf &
" LINEA_error:                                                      " & vbCrLf &
"    return -1 "
        Return res
   End Function

    Public Const sp_Calculo_Cuotas_name As String = "sp_Calculo_Cuotas"
    Public Shared Function sp_Calculo_Cuotas_text() As String
        Dim res As String
        res = "CREATE Procedure [dbo].[sp_Calculo_Cuotas] " & vbCrLf &
"				 @Capital Decimal(13,2) " & vbCrLf &
"				,@Cuotas smallint      " & vbCrLf &
"				,@Tasa decimal(8,4)    " & vbCrLf &
"				,@Lapso smallint           = 30 " & vbCrLf &
"				,@Fecha_Ini smalldatetime       " & vbCrLf &
"				,@Cap_Pagado decimal(13,2) = 0  " & vbCrLf &
"				,@Cap_Anticipo decimal(13,2) = 0  " & vbCrLf &
"               ,@CuotaFija  char(1)         = 'N'  " & vbCrLf &
"               ,@GastosA decimal(13,2) = 0  " & vbCrLf &
"				,@Original  tinyint = 1        " & vbCrLf &
"				,@error_msg   varchar(100) = '' output " & vbCrLf &
"As" & vbCrLf &
"			Declare @A_capital                     decimal(13,2)," & vbCrLf &
"					@A_tasa                        decimal(8,4),      " & vbCrLf &
"					@A_periodo                     smallint," & vbCrLf &
"					@A_nro_cuotas                  smallint," & vbCrLf &
"					@O_cuota_fija                  decimal(13,2)," & vbCrLf &
"					@A_saldo                       decimal(13,2)," & vbCrLf &
"					@A_plazo_aux                   smallint," & vbCrLf &
"					@A_fecha_Ini_aux               smalldatetime," & vbCrLf &
"					@A_Fecha_hasta                 smalldatetime," & vbCrLf &
"					@A_Cap_pagado                  decimal(13,2)," & vbCrLf &
"					@A_Interes                     decimal(13,2)," & vbCrLf &
"					@Tot_Amortizado                decimal(13,2)," & vbCrLf &
"					@Por_pagar                     decimal(13,2)," & vbCrLf &
"					@Contador                      smallint," & vbCrLf &
"					@error_exec                    smallint, " & vbCrLf &
"					@A_cuota_cap                   decimal(12,2), " & vbCrLf &
"					@Ajuste                        decimal(13,2), " & vbCrLf &
"                   @diferencia                    decimal(13,2), " & vbCrLf &
"					@Fin                           tinyint " & vbCrLf &
"			if @Capital <= 0" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Capital ingresado inválido, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 END" & vbCrLf &
"			if @Cuotas <= 0" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Cuotas ingresadas inválido, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 end" & vbCrLf &
"			if @Tasa < 0" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Tasa ingresada inválida, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 end" & vbCrLf &
"			 if @Lapso <= 0" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Lapso ingresado inválida, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 end" & vbCrLf &
"			 if @Cap_Pagado < 0 Or @Cap_Pagado > @Capital" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Capital Pagado no puede ser menor que 0 o mayor que el Capital, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 end" & vbCrLf &
"        	 if @Cap_Anticipo < 0 or @Cap_Anticipo >= @Capital" & vbCrLf &
"			 Begin" & vbCrLf &
"			     select @error_msg = 'Capital Anticipo no puede ser menor que 0 o mayor/igual que el Capital, verifique.'" & vbCrLf &
"				 goto Linea_error" & vbCrLf &
"			 end" & vbCrLf &
"           If @CuotaFija Not in('N','S')  " & vbCrLf &
"			 Begin " & vbCrLf &
"                 Select @error_msg = 'Cuota Fija solo puede recibir los valores de S o N, verifique.' " & vbCrLf &
"                 GoTo Linea_error " & vbCrLf &
"            End " & vbCrLf &
"           select  @A_capital       = @Capital," & vbCrLf &
"					@A_tasa          = @Tasa,   " & vbCrLf &
"					@A_nro_cuotas    = @Cuotas,   " & vbCrLf &
"					@A_periodo       = @Cuotas * @Lapso, " & vbCrLf &
"					@A_saldo         = @Capital - isnull(@Cap_Anticipo, 0)," & vbCrLf &
"					@A_plazo_aux     = @Lapso,       " & vbCrLf &
"					@A_fecha_Ini_aux = @Fecha_ini, " & vbCrLf &
"					@Tot_Amortizado  = isnull(@Cap_Anticipo, 0), " & vbCrLf &
"					@O_cuota_fija    = 0," & vbCrLf &
"					@Contador        = 0, " & vbCrLf &
"                   @Fin             = 1, " & vbCrLf &
"					@diferencia      = 0, " & vbCrLf &
"					@Ajuste          = 0 " & vbCrLf &
"If object_id('tempdb..@Amortizacion') is null" & vbCrLf &
"			declare @Amortizacion table (" & vbCrLf &
"				 Mes smallint" & vbCrLf &
"				,Fecha_Pago smalldatetime" & vbCrLf &
"				,Saldo_capital decimal(13,2)" & vbCrLf &
"				,Dias smallint" & vbCrLf &
"				,Interes decimal(13,2)" & vbCrLf &
"				,Amort_Cap decimal(13,2)" & vbCrLf &
"				,Total_Cuota decimal(13,2)" & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)" & vbCrLf &
"               ,GastosA decimal(13,2)" & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)" & vbCrLf &
"				,Cubierta   tinyint" & vbCrLf &
"				)" & vbCrLf &
"if @Cap_Anticipo > 0" & vbCrLf &
"begin" & vbCrLf &
"   set @Por_pagar = 0" & vbCrLf &
"   insert into @Amortizacion values(@Contador, @Fecha_Ini, 0, 0, 0, @Cap_Anticipo, @Cap_Anticipo, @Por_pagar, 0, @Cap_Anticipo, 1)  " & vbCrLf &
"end " & vbCrLf &
" set @Fin = 1 " & vbCrLf &
"While @Fin <> 0  " & vbCrLf &
"BEGIN  " & vbCrLf &
"            Select @A_capital       = @Capital, " & vbCrLf &
"					@A_tasa          = @Tasa,   " & vbCrLf &
"					@A_nro_cuotas    = @Cuotas,    " & vbCrLf &
"					@A_periodo       = @Cuotas * @Lapso, " & vbCrLf &
"					@A_saldo         = @Capital - isnull(@Cap_Anticipo, 0), " & vbCrLf &
"					@A_plazo_aux     = @Lapso,  " & vbCrLf &
"					@A_fecha_Ini_aux = @Fecha_ini, " & vbCrLf &
"					@Tot_Amortizado  = isnull(@Cap_Anticipo, 0), " & vbCrLf &
"					@O_cuota_fija    = 0, " & vbCrLf &
"					@Contador        = 0 " & vbCrLf &
"  If @CuotaFija = 'S'  " & vbCrLf &
"  begin " & vbCrLf &
"  Declare @aux decimal(13,2) = @A_saldo + @Ajuste " & vbCrLf &
"  exec  Cuota_Fija " & vbCrLf &
"                       @Capital        = @aux, " & vbCrLf &
"					    @NroCuotas      = @Cuotas, " & vbCrLf &
"					    @Tasa           = @A_tasa, " & vbCrLf &
"					    @Lapso          = @Lapso, " & vbCrLf &
"					    @Fecha_Inicial  = @Fecha_ini, " & vbCrLf &
"                       @CuotaFija      = @O_cuota_fija  OUTPUT, " & vbCrLf &
"                       @error_msg      = @error_msg OUTPUT " & vbCrLf &
"		If @@error <> 0  " & vbCrLf &
"		BEGIN " & vbCrLf &
"		   Set @error_msg='Error al llamar Cuota_Fija.' " & vbCrLf &
"		   GoTo linea_error " & vbCrLf &
"       End " & vbCrLf &
"       If @error_exec <> 0 GOTO linea_error " & vbCrLf &
 "  End " & vbCrLf &
"   Else " & vbCrLf &
"       Set @O_cuota_fija = round(@A_saldo /@Cuotas, 2) -- Cuota no fija " & vbCrLf &
"select @A_fecha_Ini_aux = @Fecha_Ini" & vbCrLf &
"set @A_Fecha_hasta = DATEADD(day,@Lapso,@A_fecha_Ini_aux)" & vbCrLf &
"set @A_plazo_aux = datediff(d,@A_fecha_Ini_aux, @A_Fecha_hasta)" & vbCrLf &
"if @Cuotas > 1" & vbCrLf &
"select @A_Fecha_hasta = Fecha_Pago," & vbCrLf &
"       @A_plazo_aux = Dias" & vbCrLf &
" from dbo.fn_Fechas_Cuotas(@Cuotas,@Lapso,@Fecha_Ini) where Nro = 1 " & vbCrLf &
"exec @error_exec = calculo_interes " & vbCrLf &
"                       @Saldo        = @A_saldo," & vbCrLf &
"					   @Fecha_desde  = @A_fecha_Ini_aux," & vbCrLf &
"					   @Fecha_hasta  = @A_Fecha_hasta," & vbCrLf &
"					   @Tasa         = @A_tasa," & vbCrLf &
"                      @Interes      = @A_Interes OUTPUT," & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT" & vbCrLf &
"	IF @@error <> 0 " & vbCrLf &
"				       BEGIN" & vbCrLf &
"				       SET @error_msg='Error al llamar calculo_interes.'" & vbCrLf &
"				       GOTO linea_error" & vbCrLf &
"				       END" & vbCrLf &
"	    IF @error_exec <> 0 GOTO linea_error" & vbCrLf &
"   if @CuotaFija = 'S'  " & vbCrLf &
"      set @A_cuota_cap = @O_cuota_fija - @A_Interes -- Cuota de capital  " & vbCrLf &
"   else  " & vbCrLf &
"      set @A_cuota_cap = @O_cuota_fija  " & vbCrLf &
"set @Tot_Amortizado = @Tot_Amortizado + @A_cuota_cap " & vbCrLf &
"set @Por_pagar = @A_cuota_cap " & vbCrLf &
"select @A_fecha_Ini_aux = @A_Fecha_hasta " & vbCrLf &
"set @A_Cap_pagado = @Cap_pagado  " & vbCrLf &
"if @A_Cap_pagado > 0 " & vbCrLf &
"begin" & vbCrLf &
"    set @Contador = @Contador + 1" & vbCrLf &
"	if @Original = 1" & vbCrLf &
"	if @A_Cap_pagado < @A_cuota_cap " & vbCrLf &
"	begin " & vbCrLf &
"	    set @Por_pagar = @A_cuota_cap - @A_Cap_pagado" & vbCrLf &
"		insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap , (@A_cuota_cap+@A_Interes), @Por_pagar, 0, @Tot_Amortizado,  0) " & vbCrLf &
"   end " & vbCrLf &
"    else " & vbCrLf &
"	begin" & vbCrLf &
"	    set @Por_pagar = 0 " & vbCrLf &
"	    insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, 0, @Tot_Amortizado,  1) " & vbCrLf &
"   end " & vbCrLf &
"	set @A_Cap_pagado = @A_Cap_pagado - @A_cuota_cap" & vbCrLf &
"End" & vbCrLf &
"else " & vbCrLf &
"begin" & vbCrLf &
"  if @Tot_Amortizado <> @Capital and @Cuotas = 1" & vbCrLf &
"	     select @A_cuota_cap = @A_cuota_cap - (@Tot_Amortizado - @Capital), " & vbCrLf &
"	            @Tot_Amortizado = @Tot_Amortizado - (@Tot_Amortizado - @Capital), " & vbCrLf &
"				@Por_pagar = @A_cuota_cap " & vbCrLf &
"  insert into @Amortizacion values(1, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, @A_Interes, @A_cuota_cap, @A_cuota_cap+@A_Interes, @Por_pagar, 0, @Tot_Amortizado, 0)" & vbCrLf &
"end " & vbCrLf &
"  Set @Contador= 2 " & vbCrLf &
"   While @Contador <= @A_nro_cuotas " & vbCrLf &
"   Begin		  " & vbCrLf &
"        set @A_saldo = @A_saldo - @A_cuota_cap  " & vbCrLf &
"      set @A_plazo_aux = @A_plazo_aux + @Lapso  " & vbCrLf &
"      set @A_Fecha_hasta = DATEADD(day,@Lapso,@A_fecha_Ini_aux)  " & vbCrLf &
"	  select @A_Fecha_hasta = Fecha_Pago," & vbCrLf &
"             @A_plazo_aux = Dias" & vbCrLf &
"       from dbo.fn_Fechas_Cuotas(@Cuotas,@Lapso,@Fecha_Ini) where Nro = @Contador " & vbCrLf &
"      exec @error_exec = calculo_interes " & vbCrLf &
"                       @Saldo        = @A_saldo," & vbCrLf &
"					    @Fecha_desde  = @A_fecha_Ini_aux," & vbCrLf &
"					    @Fecha_hasta  = @A_Fecha_hasta," & vbCrLf &
"					    @Tasa         = @A_tasa," & vbCrLf &
"                       @Interes      = @A_Interes OUTPUT," & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT" & vbCrLf &
"      IF @@error <> 0 " & vbCrLf &
"				       BEGIN" & vbCrLf &
"				       SET @error_msg='Error al llamar calculo_interes.'" & vbCrLf &
"				       GOTO linea_error" & vbCrLf &
"				       END" & vbCrLf &
"      IF @error_exec <> 0 GOTO linea_error" & vbCrLf &
"      select @A_fecha_Ini_aux = @A_Fecha_hasta " & vbCrLf &
"	  if @CuotaFija = 'S'  " & vbCrLf &
"         set @A_cuota_cap = @O_cuota_fija - @A_Interes -- Cuota de capital  " & vbCrLf &
"     else  " & vbCrLf &
"         set @A_cuota_cap = @O_cuota_fija" & vbCrLf &
"	  set @Tot_Amortizado = @Tot_Amortizado + @A_cuota_cap" & vbCrLf &
"	  set @Por_pagar = @A_cuota_cap" & vbCrLf &
"	  if @Tot_Amortizado <> @Capital And @Contador = @Cuotas" & vbCrLf &
"	     select @A_cuota_cap = @A_cuota_cap - (@Tot_Amortizado - @Capital), " & vbCrLf &
"	            @Tot_Amortizado = @Tot_Amortizado - (@Tot_Amortizado - @Capital), " & vbCrLf &
"               @Por_pagar = @A_cuota_cap " & vbCrLf &
"     if @A_Cap_pagado > 0 " & vbCrLf &
"     begin" & vbCrLf &
"         if @Original = 1" & vbCrLf &
"            If @A_Cap_pagado < @A_cuota_cap " & vbCrLf &
"            begin" & vbCrLf &
"	           set @Por_pagar = @A_cuota_cap - @A_Cap_pagado" & vbCrLf &
"               insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar,0, @Tot_Amortizado, 0)" & vbCrLf &
"            end" & vbCrLf &
"            else" & vbCrLf &
"		     begin" & vbCrLf &
"		       set @Por_pagar = 0" & vbCrLf &
"		       insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, 0,@Tot_Amortizado,  1)" & vbCrLf &
"		     end" & vbCrLf &
"   	     set @A_Cap_pagado = @A_Cap_pagado - @A_cuota_cap" & vbCrLf &
"	   end" & vbCrLf &
"	   else " & vbCrLf &
"	      insert into @Amortizacion values(@Contador,@A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, @A_Interes, @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, 0, @Tot_Amortizado,  0)" & vbCrLf &
"   Set @Contador=@Contador+1 " & vbCrLf &
"   End " & vbCrLf &
"  set @diferencia = (select top 1 Total_Cuota from @Amortizacion where Mes =  @Contador-1) - (select top 1 Total_Cuota from @Amortizacion where Mes =  @Contador-2) " & vbCrLf &
"   If abs(@diferencia) > 0 " & vbCrLf &
"      And @CuotaFija = 'S' " & vbCrLf &
"   begin " & vbCrLf &
"     If @Ajuste > 0 " & vbCrLf &
"	     set @Fin = 0 " & vbCrLf &
"        Select @Ajuste = @Ajuste + (abs(@diferencia) / power((1.000+@Tasa/1200.000), @Cuotas)) " & vbCrLf &
"     If @Fin <> 0 " & vbCrLf &
"	     delete @Amortizacion where Mes <> 0 " & vbCrLf &
"   End " & vbCrLf &
"   Else " & vbCrLf &
"     set @Fin = 0 " & vbCrLf &
" End  " & vbCrLf &
"LINEA_salir_procedimiento:" & vbCrLf &
"  SELECT Mes AS 'MES', Fecha_Pago AS 'FECHA DE PAGO', Saldo_capital AS 'SALDO CAPITAL', Dias AS 'DIAS', Interes AS 'INTERES', Amort_Cap AS 'AMORTIZACION'," & vbCrLf &
"  Total_Cuota AS 'CUOTA TOTAL', Por_Pagar_Cap AS 'CAPITAL POR PAGAR', GastosA AS 'GASTOS ADICIONALES',  Total_Cap_Amort AS 'CAPITAL AMORTIZADO', Cubierta" & vbCrLf &
"  FROM @Amortizacion" & vbCrLf &
"  return 0" & vbCrLf &
"LINEA_error:" & vbCrLf &
"    set @error_msg = rtrim(@error_msg) " & vbCrLf &
"    return -1"

        Return res
    End Function




    Public Const sp_Calculo_pago_name As String = "sp_Calculo_pago"
    Public Shared Function sp_Calculo_pago_text() As String
        Dim res As String
        res = "CREATE Procedure dbo.sp_Calculo_pago                                        " & vbCrLf &
"                 @DocEntryOV  int = 0   -- Orden de Venta                                 " & vbCrLf &
"				,@Fecha_Ini smalldatetime       -- Fecha inicio de plan de pagos           " & vbCrLf &
"				,@FechaUltPago smalldatetime      -- Fecha de ultimo pago                  " & vbCrLf &
"				,@Fecha_pago  smalldatetime       -- Fecha del pago a realizarce           " & vbCrLf &
"				,@Fecha_valor smalldatetime  = null                                        " & vbCrLf &
"				,@GastosA    decimal(13,2) = 0                                             " & vbCrLf &
"				,@error_msg   varchar(100) = '' output                                     " & vbCrLf &
"          As                                                                              " & vbCrLf &
"-- 0. Declaración de variables                                                            " & vbCrLf &
"            Declare @A_capital                     decimal(13,2),                         " & vbCrLf &
"					@A_tasa                        decimal(8,4),                           " & vbCrLf &
"					@A_nro_cuotas                  smallint,                               " & vbCrLf &
"					@A_saldo                       decimal(13,2),                          " & vbCrLf &
"					@A_plazo_aux                   smallint,                               " & vbCrLf &
"					@A_fecha_Ini_aux               smalldatetime,                          " & vbCrLf &
"					@A_Fecha_hasta                 smalldatetime,                          " & vbCrLf &
"					@A_Cap_pagado                  decimal(13,2),                          " & vbCrLf &
"					@A_Interes                     decimal(13,2),                          " & vbCrLf &
"					@Tot_Amortizado                decimal(13,2),                          " & vbCrLf &
"					@Por_pagar                     decimal(13,2),                          " & vbCrLf &
"					@Contador                      smallint,                               " & vbCrLf &
"					@error_exec                    smallint,                               " & vbCrLf &
"			        @A_cuota_cap                   decimal(12,2),                          " & vbCrLf &
"					@A_tl_Int_Carg                 decimal(13,2),                          " & vbCrLf &
"					@U_DocEntryPG                  int,                                    " & vbCrLf &
"					@Tasa                          decimal(8,4),                           " & vbCrLf &
"					@Tasa_Vcdo                     decimal(8,4),                           " & vbCrLf &
"					@U_CardCode                    varchar(50),                            " & vbCrLf &
"					@TasaNormalLegal              char(1) = 'N',                           " & vbCrLf &
"					@FechaCuotaVen                smalldatetime,                           " & vbCrLf &
"					@TotalImpMult                 decimal(13,2) = 0,                       " & vbCrLf &
"					@DocEntryOF                   int,                                     " & vbCrLf &
"					@Cap_Pagado                   decimal(13,2)                            " & vbCrLf &
"			if @DocEntryOV = 0                                                             " & vbCrLf &
"			 Begin                                                                         " & vbCrLf &
"			     select @error_msg = '@DocEntry invalido, verifique.'                      " & vbCrLf &
"				 goto Linea_error                                                          " & vbCrLf &
"			 end                                                                           " & vbCrLf &
"			 if @Cap_Pagado < 0 --or @Cap_Pagado > @Capital                                " & vbCrLf &
"			 Begin                                                                         " & vbCrLf &
"			     select @error_msg = 'Capital Pagado no puede ser menor que 0 o mayor que el Capital, verifique.' " & vbCrLf &
"				 goto Linea_error                                                          " & vbCrLf &
"			 end                                                                           " & vbCrLf &
"			 if @Fecha_pago <= @Fecha_Ini                                                  " & vbCrLf &
"			 Begin                                                                         " & vbCrLf &
"			     select @error_msg = 'Fecha Pago no puede ser menor o igual Fecha Inicio, verifique.'             " & vbCrLf &
"				 goto Linea_error                                                          " & vbCrLf &
"			 end                                                                           " & vbCrLf &
"			 if @Fecha_valor is null                                                       " & vbCrLf &
"			   set @Fecha_valor = @Fecha_pago                                              " & vbCrLf &
"			 if @Fecha_valor > @Fecha_pago                                                 " & vbCrLf &
"			 Begin                                                                         " & vbCrLf &
"			     select @error_msg = 'Fecha valor no puede ser mayor a fecha sistema o pago, verifique.'          " & vbCrLf &
"				 goto Linea_error                                                          " & vbCrLf &
"			 end                                                                           " & vbCrLf &
"           select  @A_tasa          = @Tasa,                                              " & vbCrLf &
"					@A_Cap_pagado    = @Cap_Pagado,                                        " & vbCrLf &
"					@Tot_Amortizado  = 0,                                                  " & vbCrLf &
"					@A_fecha_Ini_aux = @Fecha_ini,                                         " & vbCrLf &
"					@Tot_Amortizado  = 0, --isnull(@Cap_Anticipo, 0),                      " & vbCrLf &
"					@Contador        = 1                                                   " & vbCrLf &
"			If object_id('tempdb..@Amortizacion') is null                                  " & vbCrLf &
"			declare @Amortizacion table (                                                  " & vbCrLf &
"				 Mes int-- NOT NULL IDENTITY(1,1)                                          " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                  " & vbCrLf &
"				,Saldo_capital decimal(13,2)                                               " & vbCrLf &
"				,Dias smallint                                                             " & vbCrLf &
"				,Interes decimal(13,2)                                                     " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                   " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                 " & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)                                             " & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)                                             " & vbCrLf &
"				,Cubierta   tinyint                                                        " & vbCrLf &
"				)                                                                          " & vbCrLf &
"		    If object_id('tempdb..@Cuota_calculada') is null                               " & vbCrLf &
"			declare @Cuota_calculada table (                                               " & vbCrLf &
"				 Descripcion varchar(30)                                                   " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                  " & vbCrLf &
"				,Fecha_cuota smalldatetime                                                 " & vbCrLf &
"				,Fecha_valor smalldatetime                                                 " & vbCrLf &
"				,DiasCuot smallint                                                         " & vbCrLf &
"				,Interes decimal(13,2)                                                     " & vbCrLf &
"				,DiasIntMor  smallint                                                      " & vbCrLf &
"				,IntPorMor decimal(13,2)                                                   " & vbCrLf &
"				,Multa decimal(13,2)                                                       " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                   " & vbCrLf &
"				,GastosA decimal(13,2)                                                     " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                 " & vbCrLf &
"				)                                                                          " & vbCrLf &
"   if (select count(U_DocCuota)                                                           " & vbCrLf &
"     from ORDR  -- Tabla Maestro Orden de Venta                                           " & vbCrLf &
"   where DocEntry = @DocEntryOV ) > 0                                                     " & vbCrLf &
"  select @U_DocEntryPG  = U_DocCuota,                                                     " & vbCrLf &
"         @Tasa          = U_Interes,                                                      " & vbCrLf &
"		 @Tasa_Vcdo     = U_IntMor,                                                        " & vbCrLf &
"		 @U_CardCode    = CardCode                                                         " & vbCrLf &
"    from ORDR  -- Tabla Maestro Orden de Venta                                            " & vbCrLf &
"   where DocEntry = @DocEntryOV                                                           " & vbCrLf &
"   else                                                                                   " & vbCrLf &
"     begin                                                                                " & vbCrLf &
"	    select @error_msg = 'no existe Doc entry orden de venta.'                          " & vbCrLf &
"	    goto Linea_error                                                                   " & vbCrLf &
"	 end                                                                                   " & vbCrLf &
"  select @DocEntryOF  = TrgetEntry -- DocEntry OINV Tabla Factura                         " & vbCrLf &
"     from RDR1 -- Tabla Detalle Orden de Venta                                            " & vbCrLf &
"    where DocEntry = @DocEntryOV -- DocEntry Orden de Venta                               " & vbCrLf &
"	  and TargetType = 13 -- Tipo Factura                                                  " & vbCrLf &
"  if isnull(@DocEntryOF,0) <> 0 -- Si existe factura obtenemos lo total pagado            " & vbCrLf &
"   begin                                                                                  " & vbCrLf &
"	select @Cap_Pagado = PaidToDate from OINV as A  where DocEntry = @DocEntryOF           " & vbCrLf &
"   end                                                                                    " & vbCrLf &
"  else                                                                                    " & vbCrLf &
"   begin -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta         " & vbCrLf &
"      select @Cap_Pagado = sum(DocTotal) from ORCT where Canceled = 'N' and U_ORDV = @DocEntryOV  " & vbCrLf &
"   end                                                                                    " & vbCrLf &
"   set @Cap_Pagado = isnull(@Cap_Pagado, 0)                                               " & vbCrLf &
"   if  (select count(U_FechaV)                                                            " & vbCrLf &
"     from [@EXX_CUOIN]                                                                    " & vbCrLf &
"    where U_DocEntryPG = @U_DocEntryPG) = 0                                               " & vbCrLf &
"	begin                                                                                  " & vbCrLf &
"	    select @error_msg = 'no existe cuotas.'                                            " & vbCrLf &
"	    goto Linea_error                                                                   " & vbCrLf &
"	 end                                                                                   " & vbCrLf &
"   insert into @Amortizacion                                                              " & vbCrLf &
"   select 0, U_FechaV, 0, 0, 0, U_CapitalxPagar , U_CapitalxPagar , U_CapitalxPagar, 0, 0 " & vbCrLf &
"     from [@EXX_CUOIN]                                                                    " & vbCrLf &
"    where U_DocEntryPG = @U_DocEntryPG                                                    " & vbCrLf &
"	  and U_CapitalxPagar <> '0'                                                           " & vbCrLf &
"    order by U_FechaV asc                                                                 " & vbCrLf &
"  select @TasaNormalLegal = U_Intlgl                                                      " & vbCrLf &
"    from [@EXX_LICRE]                                                                     " & vbCrLf &
"   where U_CardCode = @U_CardCode  -- N,Y                                                 " & vbCrLf &
"  if @TasaNormalLegal = 'N'                                                               " & vbCrLf &
"   set @A_tasa = @Tasa                                                                    " & vbCrLf &
"   select @A_nro_cuotas = count(*) ,                                                      " & vbCrLf &
"          @A_capital = sum(Amort_Cap),                                                    " & vbCrLf &
"	      @A_Fecha_hasta  = min(Fecha_Pago)                                                " & vbCrLf &
"     from @Amortizacion                                                                   " & vbCrLf &
"   while @Contador <= @A_nro_cuotas                                                       " & vbCrLf &
" begin                                                                                  " & vbCrLf &
" select @A_Fecha_hasta= min(Fecha_Pago)                                             " & vbCrLf &
" from @Amortizacion                                                                " & vbCrLf &
" where Mes= 0                                                                      " & vbCrLf &
" update @Amortizacion                                                               " & vbCrLf &
" set Mes= @Contador                                                              " & vbCrLf &
" where Fecha_Pago= @A_Fecha_hasta                                                   " & vbCrLf &
" select @A_saldo= @A_capital - @Tot_Amortizado                                      " & vbCrLf &
" exec @error_exec= calculo_interes                                                  " & vbCrLf &
" @Saldo= @A_saldo,                                          " & vbCrLf &
" @Fecha_desde= @A_fecha_Ini_aux,                                   " & vbCrLf &
" @Fecha_hasta= @A_Fecha_hasta,                                     " & vbCrLf &
" @Tasa= @A_tasa,                                            " & vbCrLf &
" @Interes= @A_Interes OUTPUT,                                 " & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT                                  " & vbCrLf &
"      IF @@error <> 0                                                                     " & vbCrLf &
"		BEGIN                                                                              " & vbCrLf &
"		set @error_msg='Error al llamar calculo_interes.'                                  " & vbCrLf &
"		goto linea_error                                                                   " & vbCrLf &
"		END                                                                                " & vbCrLf &
"      IF @error_exec <> 0 goto linea_error                                                " & vbCrLf &
"   update @Amortizacion                                                                   " & vbCrLf &
"      set Saldo_capital = @A_capital - @Tot_Amortizado,                                   " & vbCrLf &
"	      Dias = datediff(d, @A_fecha_Ini_aux, Fecha_Pago),                                " & vbCrLf &
"		  Interes = @A_Interes,                                                            " & vbCrLf &
"		  Total_Cuota = Amort_Cap + @A_Interes,                                            " & vbCrLf &
"		  Por_Pagar_Cap = (case when  @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 0 else case when (@A_Cap_pagado - @Tot_Amortizado) > 0 then  Amort_Cap - (@A_Cap_pagado - @Tot_Amortizado) else Amort_Cap end end), " & vbCrLf &
"		  Total_Cap_Amort = @Tot_Amortizado + Amort_Cap,                                   " & vbCrLf &
"		  Cubierta = (case when  @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 1 else 0 end)  " & vbCrLf &
"    where Mes = @Contador                                                                 " & vbCrLf &
"   select @Tot_Amortizado = @Tot_Amortizado + Amort_Cap,                                  " & vbCrLf &
"          @A_fecha_Ini_aux = Fecha_Pago                                                   " & vbCrLf &
"     from @Amortizacion                                                                   " & vbCrLf &
"    where Mes = @Contador                                                                 " & vbCrLf &
"   set @Contador = @Contador + 1                                                          " & vbCrLf &
" end -- while @Contador <= @A_nro_cuotas                                                  " & vbCrLf &
" insert into @Cuota_calculada                                                           " & vbCrLf &
" select 'Mes ' + str(Mes,2),                                                             " & vbCrLf &
"          @Fecha_pago,                                                                    " & vbCrLf &
"		  isnull(Fecha_Pago,@Fecha_Ini) ,                                                  " & vbCrLf &
"		  @Fecha_valor,                                                                    " & vbCrLf &
"          isnull(Dias,0),                                                                 " & vbCrLf &
"		  case when @Fecha_Valor > Fecha_Pago then  isnull(Interes,0)                      " & vbCrLf &
"		  else 0 end,                                                                      " & vbCrLf &
"		  0,--isnull(datediff(d, fecha_pago, @Fecha_pago),0),                              " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                        " & vbCrLf &
"		  0,                                                                               " & vbCrLf &
"	      -- CALCULO Tasa Multa -------------------------------------------                " & vbCrLf &
"  		  0,                                                                               " & vbCrLf &
"		  ------------------------------------------------------------------               " & vbCrLf &
"		  isnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                  " & vbCrLf &
"		  0, -- Gastos Adicionales                                                         " & vbCrLf &
"		  0                                                                                " & vbCrLf &
"     from @Amortizacion                                                                   " & vbCrLf &
"    where Cubierta = 0                                                                    " & vbCrLf &
"	  and Fecha_Pago <= @Fecha_pago                                                        " & vbCrLf &
" insert into @Cuota_calculada                                                            " & vbCrLf &
" select 'TOTALES ',@Fecha_pago,                                                         " & vbCrLf &
"          isnull(min(Fecha_Cuota),@Fecha_Ini) ,                                           " & vbCrLf &
"		  @Fecha_valor,                                                                    " & vbCrLf &
"		  isnull(sum(DiasCuot),0),                                                         " & vbCrLf &
"		  isnull(sum(Interes),0),                                                          " & vbCrLf &
"		  isnull(sum(DiasIntMor),0),                                                       " & vbCrLf &
"		  isnull(sum(IntPorMor),0),                                                        " & vbCrLf &
"		  isnull(sum(Multa), 0),                                                           " & vbCrLf &
"		  isnull(sum(Amort_Cap),0),                                                        " & vbCrLf &
"		  isnull(@GastosA,0), -- Gastos Adicionales                                        " & vbCrLf &
"		  isnull(sum(Amort_Cap),0)                                                         " & vbCrLf &
"     from @Cuota_calculada                                                                " & vbCrLf &
"   if @A_tasa > 0 and @TasaNormalLegal = 'N'                                              " & vbCrLf &
"   begin                                                                                  " & vbCrLf &
"	 select @A_saldo = @A_capital  - @Cap_pagado, -- - @Cap_Anticipo                       " & vbCrLf &
"	        @A_fecha_Ini_aux = @FechaUltPago,                                              " & vbCrLf &
"			@A_Fecha_hasta = @Fecha_valor                                                  " & vbCrLf &
"	  exec @error_exec = calculo_interes                                                   " & vbCrLf &
"                       @Saldo        = @A_saldo,                                          " & vbCrLf &
"					    @Fecha_desde  = @A_fecha_Ini_aux,                                  " & vbCrLf &
"					    @Fecha_hasta  = @A_Fecha_hasta,                                    " & vbCrLf &
"					    @Tasa         = @A_tasa,                                           " & vbCrLf &
"                       @Interes      = @A_Interes OUTPUT,                                 " & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT                                  " & vbCrLf &
"      IF @@error <> 0                                                                     " & vbCrLf &
"				       BEGIN                                                               " & vbCrLf &
"				       SET @error_msg='Error al llamar calculo_interes.'                   " & vbCrLf &
"				       GOTO linea_error                                                    " & vbCrLf &
"				       END                                                                 " & vbCrLf &
"      IF @error_exec <> 0 GOTO linea_error                                                " & vbCrLf &
"	 update @Cuota_calculada                                                               " & vbCrLf &
"	    set DiasCuot = datediff(d,@A_fecha_Ini_aux, @A_Fecha_hasta),                       " & vbCrLf &
"		    Interes = @A_Interes,                                                          " & vbCrLf &
"		    Total_Cuota = @A_Interes + IntPorMor + Multa + Amort_Cap + @GastosA            " & vbCrLf &
"	  where Descripcion like '%TOTALES%'                                                   " & vbCrLf &
"	end                                                                                    " & vbCrLf &
"  if @Tasa_Vcdo > 0 and                                                                   " & vbCrLf &
"	 (select min(Fecha_cuota)                                                              " & vbCrLf &
"	   from @Cuota_calculada) <@Fecha_pago                                                " & vbCrLf &
" and @TasaNormalLegal='Y'                                                           " & vbCrLf &
" begin                                                                                  " & vbCrLf &
" select @A_saldo= @A_capital  - @Cap_pagado,                                        " & vbCrLf &
"	        @A_fecha_Ini_aux = @FechaUltPago,                                              " & vbCrLf &
"			@A_Fecha_hasta = @Fecha_valor                                                  " & vbCrLf &
"	   exec @error_exec = calculo_interes                                                  " & vbCrLf &
"                       @Saldo        = @A_saldo,                                          " & vbCrLf &
"					   @Fecha_desde  = @A_fecha_Ini_aux,                                   " & vbCrLf &
"					   @Fecha_hasta  = @A_Fecha_hasta,                                     " & vbCrLf &
"					   @Tasa         = @Tasa_Vcdo,                                         " & vbCrLf &
"                       @Interes      = @A_Interes OUTPUT,                                 " & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT                                  " & vbCrLf &
"      IF @@error <> 0                                                                     " & vbCrLf &
"		 BEGIN                                                                             " & vbCrLf &
"	        SET @error_msg='Error al llamar calculo_interes.'                              " & vbCrLf &
"		    GOTO linea_error                                                               " & vbCrLf &
"	    END                                                                                " & vbCrLf &
"      IF @error_exec <> 0 GOTO linea_error                                                " & vbCrLf &
"		update @Cuota_calculada                                                            " & vbCrLf &
"		   set IntPorMor = @A_Interes,                                                     " & vbCrLf &
"		       DiasIntMor = datediff(d, @A_fecha_Ini_aux, @A_Fecha_hasta),                 " & vbCrLf &
"			   Total_Cuota = Interes + @A_Interes + Multa + Amort_Cap + @GastosA           " & vbCrLf &
"		where Descripcion like '%TOTALES%'                                                 " & vbCrLf &
"	end                                                                                    " & vbCrLf &
"--- MULTAS                                                                                " & vbCrLf &
"    select @FechaCuotaVen = min(Fecha_cuota)                                              " & vbCrLf &
"	   from @Cuota_calculada                                                               " & vbCrLf &
"    exec @error_exec = dbo.sp_Calculo_Multa                                               " & vbCrLf &
"            @FechaCuotaVen = @FechaCuotaVen                                               " & vbCrLf &
"			,@Fecha_pago = @Fecha_pago                                                     " & vbCrLf &
"			,@TotalImpMult = @TotalImpMult output                                          " & vbCrLf &
"    IF @@error <> 0                                                                       " & vbCrLf &
"		BEGIN                                                                              " & vbCrLf &
"			SET @error_msg='Error al llamar calculo_interes.'                              " & vbCrLf &
"			GOTO linea_error                                                               " & vbCrLf &
"		END                                                                                " & vbCrLf &
"    IF @error_exec <> 0 GOTO linea_error                                                  " & vbCrLf &
"    update @Cuota_calculada                                                               " & vbCrLf &
"       set Multa = isnull(@TotalImpMult,0)                                                " & vbCrLf &
"     where Descripcion like '%TOTALES%'                                                   " & vbCrLf &
" LINEA_salir_procedimiento:                                                               " & vbCrLf &
"  select  Descripcion as DESCRIPCION, Fecha_Pago as FECHA_PAGO, Fecha_cuota as FECHA_CUOTA, Fecha_valor as FECHA_VALOR, DiasCuot as DIASCUOT  " & vbCrLf &
"		  ,Interes as INTERES, DiasIntMor  as DIASINTMOR, IntPorMor as INTPORMOR, Multa as MULTA, Amort_Cap as AMORT_CAP, GastosA as GASTOSA  " & vbCrLf &
"		  ,Total_Cuota as TOTAL_CUOTA                                                      " & vbCrLf &
"  from @Cuota_calculada                                                                   " & vbCrLf &
"  where Descripcion like '%TOTALES%'                                                      " & vbCrLf &
"  return 0                                                                                " & vbCrLf &
" LINEA_error:                                                                             " & vbCrLf &
"    set @error_msg = rtrim(@error_msg)                                                    " & vbCrLf &
"    return -1"
        Return res
    End Function


    '    Public Const sp_Calculo_pago_name As String = "sp_Calculo_pago"
    '    Public Shared Function sp_Calculo_pago_text() As String
    '        Dim res As String
    '        res = "CREATE Procedure [dbo].[sp_Calculo_pago]  " & vbCrLf &
    '"               @Capital decimal(13,2) " & vbCrLf &
    '"				,@Cuotas smallint  " & vbCrLf &
    '"				,@Tasa decimal(8,4) " & vbCrLf &
    '"				,@Lapso smallint           = 30 " & vbCrLf &
    '"				,@Fecha_Ini smalldatetime  " & vbCrLf &
    '"				,@Cap_Pagado decimal(13,2) = 0  " & vbCrLf &
    '"				,@Cap_Anticipo decimal(13,2) = 0 " & vbCrLf &
    '"				,@Tasa_Vcdo decimal(8,4) = 0  " & vbCrLf &
    '"				,@Tasa_Multa  decimal(8,4) = 0  " & vbCrLf &
    '"				,@FechaUltPago  smalldatetime  " & vbCrLf &
    '"				,@Fecha_pago  smalldatetime  " & vbCrLf &
    '"				,@Fecha_valor  smalldatetime  " & vbCrLf &
    '"               ,@CuotaFija  char(1)         = 'N' -- S= SI; N=NO  " & vbCrLf &
    '"               ,@GastosA decimal(13,2) = 0 " & vbCrLf &
    '"				,@Original  tinyint = 1   " & vbCrLf &
    '"				,@error_msg   varchar(100) = '' output " & vbCrLf &
    '"As " & vbCrLf &
    '"			Declare @A_capital                     decimal(13,2), " & vbCrLf &
    '"					@A_tasa                        decimal(8,4),    " & vbCrLf &
    '"					@A_periodo                     smallint, " & vbCrLf &
    '"					@A_nro_cuotas                  smallint, " & vbCrLf &
    '"					@O_cuota_fija                  decimal(13,2), " & vbCrLf &
    '"					@A_saldo                       decimal(13,2), " & vbCrLf &
    '"					@A_plazo_aux                   smallint, " & vbCrLf &
    '"					@A_fecha_Ini_aux               smalldatetime, " & vbCrLf &
    '"					@A_Fecha_hasta                 smalldatetime, " & vbCrLf &
    '"					@A_Cap_pagado                  decimal(13,2), " & vbCrLf &
    '"					@A_Interes                     decimal(13,2), " & vbCrLf &
    '"					@Tot_Amortizado                decimal(13,2), " & vbCrLf &
    '"					@Por_pagar                     decimal(13,2), " & vbCrLf &
    '"					@Contador                      smallint, " & vbCrLf &
    '"					@error_exec                    smallint, " & vbCrLf &
    '"					@A_cuota_cap                   decimal(12,2),  " & vbCrLf &
    '"					@Ajuste                        decimal(13,2), " & vbCrLf &
    '"                   @diferencia                    decimal(13,2), " & vbCrLf &
    '"					@Fin                           tinyint " & vbCrLf &
    '"			if @Capital <= 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Capital ingresado inválido, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 END " & vbCrLf &
    '"			if @Cuotas <= 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Cuotas ingresadas inválido, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			if @Tasa < 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Tasa ingresada inválida, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Lapso <= 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Lapso ingresado inválida, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Cap_Pagado < 0 Or @Cap_Pagado > @Capital " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Capital Pagado no puede ser menor que 0 o mayor que el Capital, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Cap_Anticipo < 0 Or @Cap_Anticipo >= @Capital " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Capital Anticipo no puede ser menor que 0 o mayor/igual que el Capital, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Tasa_Vcdo < 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Tasa Vcdo no puede ser menor que 0, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Tasa_Multa < 0 " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Tasa Multa no puede ser menor que 0, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"			 if @Fecha_pago <= @Fecha_Ini " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"			     select @error_msg = 'Fecha Pago no puede ser menor o igual Fecha Inicio, verifique.' " & vbCrLf &
    '"				 goto Linea_error " & vbCrLf &
    '"			 end " & vbCrLf &
    '"            if @Fecha_valor is null " & vbCrLf &
    '"               set @Fecha_valor = @Fecha_pago " & vbCrLf &
    '"            if @Fecha_valor > @Fecha_pago " & vbCrLf &
    '"            Begin" & vbCrLf &
    '"			     select @error_msg = 'Fecha valor no puede ser mayor a Fecha Pago, verifique.'  " & vbCrLf &
    '"				 goto Linea_error  " & vbCrLf &
    '"			 end  " & vbCrLf &
    '"            If @CuotaFija Not in('N','S')  " & vbCrLf &
    '"			 Begin " & vbCrLf &
    '"               select @error_msg = 'Cuota Fija solo puede recibir los valores de S o N, verifique.' " & vbCrLf &
    '"               GoTo Linea_error " & vbCrLf &
    '"            End " & vbCrLf &
    '"			select  @A_capital       = @Capital, " & vbCrLf &
    '"					@A_tasa          = @Tasa,    " & vbCrLf &
    '"					@A_nro_cuotas    = @Cuotas,    " & vbCrLf &
    '"					@A_periodo       = @Cuotas * @Lapso,  " & vbCrLf &
    '"					@A_saldo         = @Capital - isnull(@Cap_Anticipo, 0), " & vbCrLf &
    '"					@A_plazo_aux     = @Lapso,           " & vbCrLf &
    '"					@A_fecha_Ini_aux = @Fecha_ini, " & vbCrLf &
    '"					@Tot_Amortizado  = isnull(@Cap_Anticipo, 0), " & vbCrLf &
    '"					@O_cuota_fija    = 0, " & vbCrLf &
    '"					@Contador        = 0, " & vbCrLf &
    '"                   @Fin             = 1, " & vbCrLf &
    '"					@diferencia      = 0, " & vbCrLf &
    '"					@Ajuste          = 0 " & vbCrLf &
    '"			If object_id('tempdb..@Amortizacion') is null " & vbCrLf &
    '"			declare @Amortizacion table ( " & vbCrLf &
    '"				 Mes smallint " & vbCrLf &
    '"				,Fecha_Pago smalldatetime " & vbCrLf &
    '"				,Saldo_capital decimal(13,2) " & vbCrLf &
    '"				,Dias smallint " & vbCrLf &
    '"				,Interes decimal(13,2) " & vbCrLf &
    '"				,Amort_Cap decimal(13,2) " & vbCrLf &
    '"				,Total_Cuota decimal(13,2) " & vbCrLf &
    '"				,Por_Pagar_Cap   decimal(13,2) " & vbCrLf &
    '"				,Total_Cap_Amort decimal(13,2) " & vbCrLf &
    '"				,Cubierta   tinyint " & vbCrLf &
    '"				) " & vbCrLf &
    '"		    If object_id('tempdb..@Cuota_calculada') is null " & vbCrLf &
    '"			declare @Cuota_calculada table ( " & vbCrLf &
    '"				 DESCRIPCION varchar(30) " & vbCrLf &
    '"				,FECHA_PAGO smalldatetime " & vbCrLf &
    '"				,FECHA_CUOTA smalldatetime " & vbCrLf &
    '"				,FECHA_VALOR smalldatetime " & vbCrLf &
    '"				,DIASCUOT  smallint " & vbCrLf &
    '"				,INTERES Decimal(13,2) " & vbCrLf &
    '"				,DIASINTMOR smallint " & vbCrLf &
    '"				,INTPORMOR  Decimal(13,2) " & vbCrLf &
    '"				,MULTA Decimal(13,2) " & vbCrLf &
    '"				,AMORT_CAP  Decimal(13,2) " & vbCrLf &
    '"               ,GASTOSA  decimal(13,2) " & vbCrLf &
    '"				,TOTAL_CUOTA decimal(13,2) " & vbCrLf &
    '"				) " & vbCrLf &
    '"if @Cap_Anticipo > 0 " & vbCrLf &
    '"begin " & vbCrLf &
    '"   set @Por_pagar = 0 " & vbCrLf &
    '"   insert into @Amortizacion values(@Contador, @Fecha_Ini, 0, 0, 0, @Cap_Anticipo, @Cap_Anticipo, @Por_pagar, @Cap_Anticipo, 1) " & vbCrLf &
    '"end " & vbCrLf &
    '" set @Fin = 1 " & vbCrLf &
    '" While @Fin <> 0 " & vbCrLf &
    '" BEGIN " & vbCrLf &
    '"            Select @A_capital       = @Capital, " & vbCrLf &
    '"					@A_tasa          = @Tasa,    " & vbCrLf &
    '"					@A_nro_cuotas    = @Cuotas,    " & vbCrLf &
    '"					@A_periodo       = @Cuotas * @Lapso, " & vbCrLf &
    '"					@A_saldo         = @Capital - isnull(@Cap_Anticipo, 0), " & vbCrLf &
    '"					@A_plazo_aux     = @Lapso,     " & vbCrLf &
    '"					@A_fecha_Ini_aux = @Fecha_ini, " & vbCrLf &
    '"					@Tot_Amortizado  = isnull(@Cap_Anticipo, 0), " & vbCrLf &
    '"					@O_cuota_fija    = 0, " & vbCrLf &
    '"					@Contador        = 0 " & vbCrLf &
    '"  If @CuotaFija = 'S' -- Cuota fija " & vbCrLf &
    '"  begin " & vbCrLf &
    '"  Declare @aux decimal(13,2) = @A_saldo + @Ajuste " & vbCrLf &
    '"  exec  Cuota_Fija " & vbCrLf &
    '"                      @Capital        = @aux, " & vbCrLf &
    '"					   @NroCuotas      = @Cuotas, " & vbCrLf &
    '"					   @Tasa           = @A_tasa, " & vbCrLf &
    '"					   @Lapso          = @Lapso, " & vbCrLf &
    '"					   @Fecha_Inicial  = @Fecha_ini, " & vbCrLf &
    '"                      @CuotaFija      = @O_cuota_fija  OUTPUT, " & vbCrLf &
    '"                      @error_msg      = @error_msg OUTPUT " & vbCrLf &
    '"		If @@error <> 0  " & vbCrLf &
    '"		BEGIN " & vbCrLf &
    '"		Set @error_msg='Error al llamar Cuota_Fija.' " & vbCrLf &
    '"		GoTo linea_error " & vbCrLf &
    '"    End " & vbCrLf &
    '"    If @error_exec <> 0 GOTO linea_error " & vbCrLf &
    '"   End " & vbCrLf &
    '"   Else " & vbCrLf &
    '"    Set @O_cuota_fija = round(@A_saldo /@Cuotas, 2)  " & vbCrLf &
    '"select @A_fecha_Ini_aux = @Fecha_Ini " & vbCrLf &
    '"set @A_Fecha_hasta = DATEADD(day,@Lapso,@A_fecha_Ini_aux) " & vbCrLf &
    '"set @A_plazo_aux = datediff(d,@A_fecha_Ini_aux, @A_Fecha_hasta) " & vbCrLf &
    '"if @Cuotas > 1 " & vbCrLf &
    '"select @A_Fecha_hasta = Fecha_Pago, " & vbCrLf &
    '"       @A_plazo_aux = Dias " & vbCrLf &
    '"  from dbo.fn_Fechas_Cuotas(@Cuotas,@Lapso,@Fecha_Ini) where Nro = 1  " & vbCrLf &
    '"exec @error_exec = calculo_interes  " & vbCrLf &
    '"                      @Saldo        = @A_saldo, " & vbCrLf &
    '"					   @Fecha_desde  = @A_fecha_Ini_aux, " & vbCrLf &
    '"					   @Fecha_hasta  = @A_Fecha_hasta, " & vbCrLf &
    '"					   @Tasa         = @A_tasa, " & vbCrLf &
    '"                      @Interes      = @A_Interes OUTPUT, " & vbCrLf &
    '"                       @error_msg    = @error_msg OUTPUT " & vbCrLf &
    '"	IF @@error <> 0  " & vbCrLf &
    '"				       BEGIN " & vbCrLf &
    '"				       SET @error_msg='Error al llamar calculo_interes.' " & vbCrLf &
    '"				       GOTO linea_error " & vbCrLf &
    '"				       END " & vbCrLf &
    '"	    IF @error_exec <> 0 GOTO linea_error " & vbCrLf &
    '"   if @CuotaFija = 'S' " & vbCrLf &
    '"      set @A_cuota_cap = @O_cuota_fija - @A_Interes  " & vbCrLf &
    '"   else " & vbCrLf &
    '"      set @A_cuota_cap = @O_cuota_fija " & vbCrLf &
    '"set @Tot_Amortizado = @Tot_Amortizado + @A_cuota_cap " & vbCrLf &
    '"Set @Por_pagar = @A_cuota_cap " & vbCrLf &
    '"select @A_fecha_Ini_aux = @A_Fecha_hasta  " & vbCrLf &
    '"set @A_Cap_pagado = @Cap_pagado " & vbCrLf &
    '"if @A_Cap_pagado > 0  " & vbCrLf &
    '"begin " & vbCrLf &
    '"    set @Contador = @Contador + 1 " & vbCrLf &
    '"	if @Original = 1 " & vbCrLf &
    '"	if @A_Cap_pagado < @A_cuota_cap  " & vbCrLf &
    '"	begin " & vbCrLf &
    '"	    set @Por_pagar = @A_cuota_cap - @A_Cap_pagado " & vbCrLf &
    '"		insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap , (@A_cuota_cap+@A_Interes), @Por_pagar, @Tot_Amortizado,  0) " & vbCrLf &
    '"   end " & vbCrLf &
    '"    else  " & vbCrLf &
    '"	begin " & vbCrLf &
    '"	    set @Por_pagar = 0 " & vbCrLf &
    '"	    insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, @Tot_Amortizado,  1) " & vbCrLf &
    '"   end " & vbCrLf &
    '"	set @A_Cap_pagado = @A_Cap_pagado - @A_cuota_cap " & vbCrLf &
    '"End " & vbCrLf &
    '"else  " & vbCrLf &
    '"begin " & vbCrLf &
    '"   if @Tot_Amortizado <> @Capital And @Cuotas = 1 " & vbCrLf &
    '"	     select @A_cuota_cap = @A_cuota_cap - (@Tot_Amortizado - @Capital), " & vbCrLf &
    '"	            @Tot_Amortizado = @Tot_Amortizado - (@Tot_Amortizado - @Capital), " & vbCrLf &
    '"				@Por_pagar = @A_cuota_cap " & vbCrLf &
    '"  insert into @Amortizacion values(1, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, @A_Interes, @A_cuota_cap, @A_cuota_cap+@A_Interes, @Por_pagar, @Tot_Amortizado, 0) " & vbCrLf &
    '"end " & vbCrLf &
    '"   Set @Contador= 2  " & vbCrLf &
    '"   While @Contador <= @A_nro_cuotas  " & vbCrLf &
    '"   Begin		   " & vbCrLf &
    '"      set @A_saldo = @A_saldo - @A_cuota_cap " & vbCrLf &
    '"      set @A_plazo_aux = @A_plazo_aux + @Lapso " & vbCrLf &
    '"      set @A_Fecha_hasta = DATEADD(day,@Lapso,@A_fecha_Ini_aux) " & vbCrLf &
    '"  select @A_Fecha_hasta = Fecha_Pago, " & vbCrLf &
    '"             @A_plazo_aux = Dias " & vbCrLf &
    '"       from dbo.fn_Fechas_Cuotas(@Cuotas,@Lapso,@Fecha_Ini) where Nro = @Contador  " & vbCrLf &
    '"	      exec @error_exec = calculo_interes  " & vbCrLf &
    '"                       @Saldo        = @A_saldo, " & vbCrLf &
    '"					   @Fecha_desde  = @A_fecha_Ini_aux, " & vbCrLf &
    '"					   @Fecha_hasta  = @A_Fecha_hasta, " & vbCrLf &
    '"					   @Tasa         = @A_tasa, " & vbCrLf &
    '"                      @Interes      = @A_Interes OUTPUT, " & vbCrLf &
    '"                       @error_msg    = @error_msg OUTPUT " & vbCrLf &
    '"      IF @@error <> 0  " & vbCrLf &
    '"				       BEGIN " & vbCrLf &
    '"				       SET @error_msg='Error al llamar calculo_interes.' " & vbCrLf &
    '"				       GOTO linea_error " & vbCrLf &
    '"				       END " & vbCrLf &
    '"      IF @error_exec <> 0 GOTO linea_error " & vbCrLf &
    '"      select @A_fecha_Ini_aux = @A_Fecha_hasta  " & vbCrLf &
    '"	  if @CuotaFija = 'S'  " & vbCrLf &
    '"         set @A_cuota_cap = @O_cuota_fija - @A_Interes -- Cuota de capital  " & vbCrLf &
    '"     else" & vbCrLf &
    '"         set @A_cuota_cap = @O_cuota_fija  " & vbCrLf &
    '"	  set @Tot_Amortizado = @Tot_Amortizado + @A_cuota_cap " & vbCrLf &
    '"	  set @Por_pagar = @A_cuota_cap " & vbCrLf &
    '"	  if @Tot_Amortizado <> @Capital And @Contador = @Cuotas " & vbCrLf &
    '"	     select @A_cuota_cap = @A_cuota_cap - (@Tot_Amortizado - @Capital), " & vbCrLf &
    '"	            @Tot_Amortizado = @Tot_Amortizado - (@Tot_Amortizado - @Capital), " & vbCrLf &
    '"               @Por_pagar = @A_cuota_cap " & vbCrLf &
    '"      if @A_Cap_pagado > 0 -- Si hay capital pagado  " & vbCrLf &
    '"      begin " & vbCrLf &
    '"         if @Original = 1 " & vbCrLf &
    '"            if @A_Cap_pagado < @A_cuota_cap " & vbCrLf &
    '"            begin " & vbCrLf &
    '"	           set @Por_pagar = @A_cuota_cap - @A_Cap_pagado " & vbCrLf &
    '"               insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, @Tot_Amortizado, 0) " & vbCrLf &
    '"            end " & vbCrLf &
    '"            else " & vbCrLf &
    '"		    begin " & vbCrLf &
    '"		       set @Por_pagar = 0 " & vbCrLf &
    '"		       insert into @Amortizacion values(@Contador, @A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, 0,  @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, @Tot_Amortizado,  1) " & vbCrLf &
    '"		    end " & vbCrLf &
    '"   	     set @A_Cap_pagado = @A_Cap_pagado - @A_cuota_cap " & vbCrLf &
    '"	   end  " & vbCrLf &
    '"	   else  " & vbCrLf &
    '"	      insert into @Amortizacion values(@Contador,@A_fecha_Ini_aux, @A_saldo, @A_plazo_aux, @A_Interes, @A_cuota_cap, (@A_cuota_cap+@A_Interes), @Por_pagar, @Tot_Amortizado,  0) " & vbCrLf &
    '"   Set @Contador=@Contador+1 " & vbCrLf &
    '"   End  " & vbCrLf &
    '"     set @diferencia = (select top 1 Total_Cuota from @Amortizacion where Mes =  @Contador-1) - (select top 1 Total_Cuota from @Amortizacion where Mes =  @Contador-2) " & vbCrLf &
    '"   If abs(@diferencia) > 0 " & vbCrLf &
    '"      And @CuotaFija = 'S' " & vbCrLf &
    '"   begin " & vbCrLf &
    '"    If @Ajuste > 0 " & vbCrLf &
    '"	  set @Fin = 0 " & vbCrLf &
    '"   Select @Ajuste = @Ajuste + (abs(@diferencia) / power((1.000+@Tasa/1200.000), @Cuotas)) " & vbCrLf &
    '"  If @Fin <> 0 " & vbCrLf &
    '"	  delete @Amortizacion where Mes <> 0 " & vbCrLf &
    '"   End " & vbCrLf &
    '"  Else " & vbCrLf &
    '"     set @Fin = 0 " & vbCrLf &
    '"End -- While @Fin <> 0 " & vbCrLf &
    '"   declare @Dias_mor smallint, @Cap_Mor decimal(13,2), @Fecha_mora smalldatetime, @Interes_Mor decimal(13,2) " & vbCrLf &
    '"   set @Interes_Mor = 0 " & vbCrLf &
    '"   set @Dias_mor = 0 " & vbCrLf &
    '"   if @Fecha_pago > (select min(Fecha_Pago) from @Amortizacion where Cubierta = 0) " & vbCrLf &
    '"    And @Tasa_Multa > 0 " & vbCrLf &
    '"   begin " & vbCrLf &
    '"	   select @Dias_mor  = isnull(sum(datediff(d, fecha_pago, @Fecha_pago)),0), " & vbCrLf &
    '"	          @Cap_Mor = isnull(sum(Amort_Cap),0) " & vbCrLf &
    '"		 from @Amortizacion  " & vbCrLf &
    '"		where Cubierta = 0  " & vbCrLf &
    '"		  And Fecha_Pago <= @Fecha_pago " & vbCrLf &
    '"	  set @Interes_Mor =  @Tasa_Multa * @Dias_mor " & vbCrLf &
    '"  end " & vbCrLf &
    '"   insert into @Cuota_calculada  " & vbCrLf &
    '"   select 'Mes '+ str(Mes,2),  " & vbCrLf &
    '"          @Fecha_pago,  " & vbCrLf &
    '"		  isnull(Fecha_Pago,@Fecha_Ini) ,  " & vbCrLf &
    '"          @Fecha_valor,  " & vbCrLf &
    '"         isnull(Dias,0), " & vbCrLf &
    '"         case when @Fecha_Valor > Fecha_Pago then  isnull(Interes,0)  " & vbCrLf &
    '"         else 0 end,  " & vbCrLf &
    '"		  0,  " & vbCrLf &
    '"		  case when @Tasa_Vcdo > 0 And isnull(datediff(d, fecha_pago, @Fecha_pago),0) > 0  " & vbCrLf &
    '"		       then  0 " & vbCrLf &
    '"			   else 0 end, " & vbCrLf &
    '"	      round(((@Tasa_Multa/100)/360) -- Tasa Diaria calculada " & vbCrLf &
    '"		 * ((case when @Tasa_Vcdo > 0 And isnull(datediff(d, fecha_pago, @Fecha_pago),0) > 0  " & vbCrLf &
    '"			   then  (round(@Tasa_Vcdo / round(360 / @Lapso, 0),0 ) / 100) * Por_Pagar_Cap " & vbCrLf &
    '"			   else 0 end) + Por_Pagar_Cap ) , 0) " & vbCrLf &
    '"		  * isnull(datediff(d, fecha_pago, @Fecha_pago),0) ,  " & vbCrLf &
    '"		  		  isnull(Por_Pagar_Cap, 0),   " & vbCrLf &
    '"         0, " & vbCrLf &
    '"         0  " & vbCrLf &
    '"    from @Amortizacion " & vbCrLf &
    '"    where Cubierta = 0 " & vbCrLf &
    '"	  And Fecha_Pago <= @Fecha_pago" & vbCrLf &
    '"	insert into @Cuota_calculada " & vbCrLf &
    '"  select 'TOTALES ',@Fecha_pago, " & vbCrLf &
    '"          isnull(min(Fecha_Cuota),@Fecha_Ini) ," & vbCrLf &
    '"          @Fecha_valor,  " & vbCrLf &
    '"		  isnull(sum(DiasCuot),0), " & vbCrLf &
    '"		  isnull(sum(Interes),0), " & vbCrLf &
    '"		  isnull(sum(DiasIntMor),0), " & vbCrLf &
    '"		  isnull(sum(IntPorMor),0), " & vbCrLf &
    '"		  isnull(sum(Multa), 0)," & vbCrLf &
    '"		  isnull(sum(Amort_Cap),0)," & vbCrLf &
    '"         isnull(@GastosA,0), " & vbCrLf &
    '"		  isnull(sum(Amort_Cap),0) " & vbCrLf &
    '"    from @Cuota_calculada " & vbCrLf &
    '"    select @A_saldo = @Capital - @Cap_Anticipo - @Cap_pagado," & vbCrLf &
    '"           @A_fecha_Ini_aux = @FechaUltPago,  " & vbCrLf &
    '"           @A_Fecha_hasta = @Fecha_valor" & vbCrLf &
    '"    exec @error_exec = calculo_interes  " & vbCrLf &
    '"                       @Saldo        = @A_saldo,  " & vbCrLf &
    '"                       @Fecha_desde  = @A_fecha_Ini_aux,  " & vbCrLf &
    '"                       @Fecha_hasta  = @A_Fecha_hasta,  " & vbCrLf &
    '"                       @Tasa         = @A_tasa," & vbCrLf &
    '"                       @Interes      = @A_Interes OUTPUT," & vbCrLf &
    '"                       @error_msg    = @error_msg OUTPUT" & vbCrLf &
    '"   IF @@error <> 0 " & vbCrLf &
    '"   BEGIN" & vbCrLf &
    '"      SET @error_msg='Error al llamar calculo_interes.'" & vbCrLf &
    '"      GOTO linea_error" & vbCrLf &
    '"   END" & vbCrLf &
    '"   IF @error_exec <> 0 GOTO linea_error  " & vbCrLf &
    '"   update @Cuota_calculada  " & vbCrLf &
    '"          set Interes = @A_Interes,  " & vbCrLf &
    '"              Total_Cuota = @A_Interes + IntPorMor + Multa + Amort_Cap + @GastosA " & vbCrLf &
    '"   where Descripcion like '%TOTALES%'  " & vbCrLf &
    '"   if @Tasa_Vcdo > 0 and " & vbCrLf &
    '"      (select min(Fecha_cuota) from @Cuota_calculada) < @Fecha_pago" & vbCrLf &
    '"   begin" & vbCrLf &
    '"      exec @error_exec = calculo_interes " & vbCrLf &
    '"                         @Saldo        = @A_saldo," & vbCrLf &
    '"                         @Fecha_desde  = @A_fecha_Ini_aux," & vbCrLf &
    '"                         @Fecha_hasta  = @A_Fecha_hasta," & vbCrLf &
    '"                         @Tasa         = @Tasa_Vcdo," & vbCrLf &
    '"                         @Interes      = @A_Interes OUTPUT," & vbCrLf &
    '"                         @error_msg    = @error_msg OUTPUT" & vbCrLf &
    '"      IF @@error <> 0 " & vbCrLf &
    '"      BEGIN" & vbCrLf &
    '"         SET @error_msg='Error al llamar calculo_interes.'" & vbCrLf &
    '"         GOTO linea_error" & vbCrLf &
    '"      END" & vbCrLf &
    '"      IF @error_exec <> 0 GOTO linea_error" & vbCrLf &
    '"      update @Cuota_calculada" & vbCrLf &
    '"         set IntPorMor = @A_Interes," & vbCrLf &
    '"             DiasIntMor = datediff(d, @A_fecha_Ini_aux, @A_Fecha_hasta)," & vbCrLf &
    '"             Total_Cuota = Interes + @A_Interes + Multa + Amort_Cap + @GastosA" & vbCrLf &
    '"      where Descripcion like '%TOTALES%' " & vbCrLf &
    '"   end" & vbCrLf &
    '"LINEA_salir_procedimiento:" & vbCrLf &
    '"select  Descripcion as DESCRIPCION, Fecha_Pago as FECHA_PAGO, Fecha_cuota as FECHA_CUOTA, Fecha_valor as FECHA_VALOR, DiasCuot as DIASCUOT " & vbCrLf &
    '"		  ,Interes as INTERES, DiasIntMor  as DIASINTMOR, IntPorMor as INTPORMOR, Multa as MULTA, Amort_Cap as AMORT_CAP, GastosA as GASTOSA " & vbCrLf &
    '"		  ,Total_Cuota as TOTAL_CUOTA " & vbCrLf &
    '"  from @Cuota_calculada " & vbCrLf &
    '"   where LTrim(RTrim(Descripcion)) = 'TOTALES'" & vbCrLf &
    '"  return 0" & vbCrLf &
    '"LINEA_error:" & vbCrLf &
    '"    set @error_msg = rtrim(@error_msg) " & vbCrLf &
    '"    return -1"
    '        Return res
    '    End Function
    Public Const sp_Cuotas_x_pagar_bc_name As String = "sp_Cuotas_x_pagar_bc"
    Public Shared Function sp_Cuotas_x_pagar_bc_text() As String
        Dim res As String
        res = "CREATE Procedure dbo.sp_Cuotas_x_pagar_bc                                        " & vbCrLf &
"                     @DocEntryOV  int = 0              -- Orden de Venta                       " & vbCrLf &
"                     ,@Fecha_pago  smalldatetime       -- Fecha del pago a realizarce          " & vbCrLf &
"                     ,@error_msg   varchar(100) = '' output                                    " & vbCrLf &
" As                                                                                            " & vbCrLf &
"-- 0. Declaración de variables                                                                 " & vbCrLf &
"			Declare @A_capital                     decimal(13,2),                               " & vbCrLf &
"					@A_tasa                        decimal(8,4),                                " & vbCrLf &
"					@A_nro_cuotas                  smallint,                                    " & vbCrLf &
"					@A_saldo                       decimal(13,2),                               " & vbCrLf &
"					@A_plazo_aux                   smallint,                                    " & vbCrLf &
"					@A_fecha_Ini_aux               smalldatetime,                               " & vbCrLf &
"					@A_Fecha_hasta                 smalldatetime,                               " & vbCrLf &
"					@A_Cap_pagado                  decimal(13,2),                               " & vbCrLf &
"					@A_Interes                     decimal(13,2),                               " & vbCrLf &
"					@Tot_Amortizado                decimal(13,2),                               " & vbCrLf &
"					@Por_pagar                     decimal(13,2),                               " & vbCrLf &
"					@Contador                      smallint,                                    " & vbCrLf &
"					@error_exec                    smallint,                                    " & vbCrLf &
"			        @A_cuota_cap                   decimal(12,2),                               " & vbCrLf &
"					@A_tl_Int_Carg                 decimal(13,2),                               " & vbCrLf &
"					@U_DocEntryPG                  int,                                         " & vbCrLf &
"					@Tasa                          decimal(8,4)  ,                              " & vbCrLf &
"					@Tasa_Vcdo                     decimal(8,4) ,                               " & vbCrLf &
"					@U_CardCode                    varchar(50),                                 " & vbCrLf &
"					@TasaNormalLegal               char(1) = 'N',                               " & vbCrLf &
"					@FechaCuotaVen                 smalldatetime,                               " & vbCrLf &
"					@TotalImpMult                  decimal(13,2) = 0,                           " & vbCrLf &
"					@DocEntryOF                    int,                                         " & vbCrLf &
"					@Fecha_valor                   smalldatetime,                               " & vbCrLf &
"					@FechaUltPago                  smalldatetime,                               " & vbCrLf &
"					@Nombre                        varchar(70),                                 " & vbCrLf &
"					@Identificacion                varchar(20),                                 " & vbCrLf &
"					@Contador_aux                  int                                          " & vbCrLf &
"			select  @A_tasa          = @Tasa,                                                   " & vbCrLf &
"					@Tot_Amortizado  = 0,                                                       " & vbCrLf &
"					@Tot_Amortizado  = 0,                                                       " & vbCrLf &
"					@Contador        = 2,                                                       " & vbCrLf &
"					@Contador_aux    = 1,                                                       " & vbCrLf &
"					@A_nro_cuotas    = 0,                                                       " & vbCrLf &
"					@Fecha_valor     = @Fecha_pago                                              " & vbCrLf &
"			If object_id('tempdb..@Amortizacion') is null                                       " & vbCrLf &
"			declare @Amortizacion table (                                                       " & vbCrLf &
"			     Mes int                                                                        " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                       " & vbCrLf &
"				,Saldo_capital decimal(13,2)                                                    " & vbCrLf &
"				,Dias smallint                                                                  " & vbCrLf &
"				,Interes decimal(13,2)                                                          " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                        " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                      " & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)                                                  " & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)                                                  " & vbCrLf &
"				,Cubierta   tinyint                                                             " & vbCrLf &
"				)                                                                               " & vbCrLf &
"		    If object_id('tempdb..@Cuota_calculada') is null                                    " & vbCrLf &
"			declare @Cuota_calculada table (                                                    " & vbCrLf &
"				 Mes smallint                                                                   " & vbCrLf &
"				,Descripcion varchar(30)                                                        " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                       " & vbCrLf &
"				,Fecha_cuota smalldatetime                                                      " & vbCrLf &
"				,Fecha_valor smalldatetime                                                      " & vbCrLf &
"				,DiasCuot smallint                                                              " & vbCrLf &
"				,Interes decimal(13,2)                                                          " & vbCrLf &
"				,DiasIntMor  smallint                                                           " & vbCrLf &
"				,IntPorMor decimal(13,2)                                                        " & vbCrLf &
"				,Multa decimal(13,2)                                                            " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                        " & vbCrLf &
"				,GastosA decimal(13,2)                                                          " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                      " & vbCrLf &
"				,Cubierta tinyint                                                               " & vbCrLf &
"				)                                                                               " & vbCrLf &
"			If object_id('tempdb..@Cuota_calculada_bc') is null                                 " & vbCrLf &
"			declare @Cuota_calculada_bc table (                                                 " & vbCrLf &
"			     CodBc    varchar(50)                                                           " & vbCrLf &
"				,CodEntBc varchar(50)                                                           " & vbCrLf &
"				,DocEntryOV  varchar(15)                                                        " & vbCrLf &
"				,NroCuota    int                                                                " & vbCrLf &
"				,Descripcion varchar(30)                                                        " & vbCrLf &
"				,Importe    decimal(13,2)                                                       " & vbCrLf &
"				,Fecha_Pago char(10)                                                            " & vbCrLf &
"				,Fecha_cuota smalldatetime                                                      " & vbCrLf &
"				,Multa     decimal(13,2)                                                        " & vbCrLf &
"				,DiasMulta smallint                                                             " & vbCrLf &
"				,NombreRazonSocial  varchar(70)                                                 " & vbCrLf &
"				,Identificacion varchar(20)                                                     " & vbCrLf &
"				,Numero     int                                                                 " & vbCrLf &
"				,Agencia    varchar(30)                                                         " & vbCrLf &
"				,Datos      varchar(100)                                                        " & vbCrLf &
"				,Cubierta   tinyint                                                             " & vbCrLf &
"				)                                                                               " & vbCrLf &
" declare F_Orden_Venta cursor fast_forward for                                                 " & vbCrLf &
"  select DocEntry,                                                                             " & vbCrLf &
"         DocDate,                                                                              " & vbCrLf &
"         U_DocCuota,                                                                           " & vbCrLf &
"         U_Interes,                                                                            " & vbCrLf &
"		 U_IntMor,                                                                              " & vbCrLf &
"		 CardCode                                                                               " & vbCrLf &
"    from ORDR  -- Tabla Maestro Orden de Venta                                                 " & vbCrLf &
"   where CANCELED = 'N'                                                                        " & vbCrLf &
"     and DocStatus = 'O'                                                                       " & vbCrLf &
"     and DocEntry = (case when @DocEntryOV = 0 then DocEntry else @DocEntryOV end)             " & vbCrLf &
"	  and U_DocCuota is not null                                                                " & vbCrLf &
" open F_Orden_Venta                                                                            " & vbCrLf &
"   fetch next from F_Orden_Venta into @DocEntryOV,                                             " & vbCrLf &
"                                      @A_fecha_Ini_aux,                                        " & vbCrLf &
"                                      @U_DocEntryPG,                                           " & vbCrLf &
"								       @Tasa,                                                   " & vbCrLf &
"								       @Tasa_Vcdo,                                              " & vbCrLf &
"								       @U_CardCode                                              " & vbCrLf &
"   while @@fetch_status = 0                                                                    " & vbCrLf &
"	  begin                                                                                     " & vbCrLf &
"	    delete @Amortizacion                                                                    " & vbCrLf &
"		delete @Cuota_calculada                                                                 " & vbCrLf &
"		set @Tot_Amortizado = 0                                                                 " & vbCrLf &
"		set @A_Cap_pagado = 0                                                                   " & vbCrLf &
"	    set @FechaUltPago = @A_fecha_Ini_aux -- OJO hay que obtener la fecha de ultimo pago     " & vbCrLf &
"		select @DocEntryOF  = TrgetEntry -- DocEntry OINV Tabla Factura                         " & vbCrLf &
"          from RDR1 -- Tabla Detalle Orden de Venta                                            " & vbCrLf &
"         where DocEntry = @DocEntryOV -- DocEntry Orden de Venta                               " & vbCrLf &
"	       and TargetType = 13 -- Tipo Factura                                                  " & vbCrLf &
"  if isnull(@DocEntryOF,0) <> 0 -- Si existe factura obtenemos lo total pagado                 " & vbCrLf &
"   begin                                                                                       " & vbCrLf &
"	select @A_Cap_pagado = PaidToDate from OINV as A  where DocEntry = @DocEntryOF              " & vbCrLf &
"   end                                                                                         " & vbCrLf &
" else                                                                                          " & vbCrLf &
"   begin -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta              " & vbCrLf &
"      select @A_Cap_pagado = sum(DocTotal) from ORCT where Canceled = 'N' and U_ORDV = @DocEntryOV    " & vbCrLf &
"   end                                                                                         " & vbCrLf &
"   set @A_Cap_pagado = isnull(@A_Cap_pagado, 0)                                                " & vbCrLf &
"   insert into @Amortizacion                                                                   " & vbCrLf &
"   select 0, U_FechaV, 0, 0, 0, U_Amortizacion , U_Amortizacion , U_Amortizacion, 0, 0         " & vbCrLf &
"     from [@EXX_CUOIN]                                                                         " & vbCrLf &
"    where U_DocEntryPG = @U_DocEntryPG                                                         " & vbCrLf &
"	  and U_CapitalxPagar <> '0'                                                                " & vbCrLf &
"    order by U_FechaV asc                                                                      " & vbCrLf &
"  select @TasaNormalLegal = U_Intlgl                                                           " & vbCrLf &
"    from [@EXX_LICRE]                                                                          " & vbCrLf &
"   where U_CardCode = @U_CardCode  -- N,Y                                                      " & vbCrLf &
"  select @Nombre = CardNAme,                                                                   " & vbCrLf &
"         @Identificacion = LicTradNum                                                          " & vbCrLf &
"    from OCRD                                                                                  " & vbCrLf &
"   where CardCode = @U_CardCode                                                                " & vbCrLf &
"  if @TasaNormalLegal = 'N'                                                                    " & vbCrLf &
"   set @A_tasa = @Tasa                                                                         " & vbCrLf &
"   select @A_nro_cuotas =  count(*) ,                                                          " & vbCrLf &
"          @A_capital = sum(Amort_Cap),                                                         " & vbCrLf &
"	      @A_Fecha_hasta  = min(Fecha_Pago)                                                     " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"   set @Contador = 1                                                                           " & vbCrLf &
"   while @Contador <= @A_nro_cuotas                                                            " & vbCrLf &
"   begin                                                                                       " & vbCrLf &
"      select @A_Fecha_hasta = min(Fecha_Pago)                                                  " & vbCrLf &
"        from @Amortizacion                                                                     " & vbCrLf &
"       where Mes = 0                                                                           " & vbCrLf &
"	  update @Amortizacion                                                                      " & vbCrLf &
"	      set Mes = @Contador                                                                   " & vbCrLf &
"	 where Fecha_Pago = @A_Fecha_hasta                                                          " & vbCrLf &
"     select @A_saldo = @A_capital - @Tot_Amortizado                                            " & vbCrLf &
"      exec @error_exec = calculo_interes                                                       " & vbCrLf &
"                       @Saldo        = @A_saldo,                                               " & vbCrLf &
"					   @Fecha_desde  = @A_fecha_Ini_aux,                                        " & vbCrLf &
"					   @Fecha_hasta  = @A_Fecha_hasta,                                          " & vbCrLf &
"					   @Tasa         = @A_tasa,                                                 " & vbCrLf &
"                      @Interes      = @A_Interes OUTPUT,                                       " & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT                                       " & vbCrLf &
"      IF @@error <> 0                                                                          " & vbCrLf &
"		BEGIN                                                                                   " & vbCrLf &
"		set @error_msg='Error al llamar calculo_interes.'                                       " & vbCrLf &
"		goto linea_error                                                                        " & vbCrLf &
"		END                                                                                     " & vbCrLf &
"      IF @error_exec <> 0 goto linea_error                                                     " & vbCrLf &
"   update @Amortizacion                                                                        " & vbCrLf &
"      set Saldo_capital = @A_capital - @Tot_Amortizado,                                        " & vbCrLf &
"	      Dias = datediff(d, @A_fecha_Ini_aux, Fecha_Pago),                                     " & vbCrLf &
"		  Interes = @A_Interes,                                                                 " & vbCrLf &
"		  Total_Cuota = Amort_Cap + @A_Interes,                                                 " & vbCrLf &
"		  Por_Pagar_Cap = (case when @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 0 else case when (@A_Cap_pagado - @Tot_Amortizado) > 0 then  Amort_Cap - (@A_Cap_pagado - @Tot_Amortizado) else Amort_Cap end end), " & vbCrLf &
"		  Total_Cap_Amort = @Tot_Amortizado + Amort_Cap,                                        " & vbCrLf &
"		  Cubierta = (case when @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 1 else 0 end) " & vbCrLf &
"    where Mes = @Contador                                                                      " & vbCrLf &
"   select @Tot_Amortizado = @Tot_Amortizado + Amort_Cap,                                       " & vbCrLf &
"          @A_fecha_Ini_aux = Fecha_Pago                                                        " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"    where Mes = @Contador                                                                      " & vbCrLf &
"   set @Contador = @Contador + 1                                                               " & vbCrLf &
"end -- while @Contador <= @A_nro_cuotas                                                        " & vbCrLf &
"   insert into @Cuota_calculada                                                                " & vbCrLf &
"   select Mes, --case when Mes > @Contador_aux then Mes -  @Contador_aux else Mes end ,        " & vbCrLf &
"          'PAGO DE CUOTA No. '+ ltrim(str(Mes)),                                               " & vbCrLf &
"          @Fecha_pago,                                                                         " & vbCrLf &
"		   Fecha_Pago,                                                                          " & vbCrLf &
"		   @Fecha_Valor,                                                                        " & vbCrLf &
"          isnull(Dias,0),                                                                      " & vbCrLf &
"		  --case when @Fecha_Valor > Fecha_Pago then isnull(Interes,0)                          " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"		  0,--isnull(datediff(d, fecha_pago, @Fecha_pago),0),                                   " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                             " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"	      -- CALCULO Tasa Multa ---------------------------------------                         " & vbCrLf &
"  		  0,                                                                                    " & vbCrLf &
"		  -------------------------------------------------------------                         " & vbCrLf &
"		  isnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                       " & vbCrLf &
"		  0, -- Gastos Adicionales                                                              " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"		  Cubierta                                                                              " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"--- MULTAS                                                                                     " & vbCrLf &
"    select @FechaCuotaVen = min(Fecha_cuota)                                                   " & vbCrLf &
"	   from @Cuota_calculada                                                                    " & vbCrLf &
"   exec @error_exec = dbo.sp_Calculo_Multa                                                     " & vbCrLf &
"             @FechaCuotaVen = @FechaCuotaVen                                                   " & vbCrLf &
"			 ,@Fecha_pago    = @Fecha_pago                                                      " & vbCrLf &
"			 ,@TotalImpMult  = @TotalImpMult output                                             " & vbCrLf &
"		     ,@error_msg     = @error_msg output                                                " & vbCrLf &
"   IF @@error <> 0                                                                             " & vbCrLf &
"	 BEGIN                                                                                      " & vbCrLf &
"	   SET @error_msg='Error al llamar calculo_interes.'                                        " & vbCrLf &
"		GOTO linea_error                                                                        " & vbCrLf &
"	 END                                                                                        " & vbCrLf &
"    IF @error_exec <> 0 GOTO linea_error                                                       " & vbCrLf &
"   update @Cuota_calculada                                                                     " & vbCrLf &
"      set Multa = isnull(@TotalImpMult,0)                                                      " & vbCrLf &
"    where Fecha_cuota  = @FechaCuotaVen                                                        " & vbCrLf &
"   insert into @Cuota_calculada_bc                                                             " & vbCrLf &
"    select 'CodBc' , @DocEntryOV , @DocEntryOV, Mes, Descripcion, Amort_Cap, convert(char(10),@Fecha_pago, 105 ), Fecha_cuota, Multa, 0, @Nombre, @Identificacion, 0, 'Agencia', 'XXXXXXXXXXX',Cubierta   " & vbCrLf &
"	  from @Cuota_calculada                                                                     " & vbCrLf &
"	fetch next from F_Orden_Venta into @DocEntryOV,                                             " & vbCrLf &
"                                @A_fecha_Ini_aux,                                              " & vbCrLf &
"                                @U_DocEntryPG,                                                 " & vbCrLf &
"								 @Tasa,                                                         " & vbCrLf &
"								 @Tasa_Vcdo,                                                    " & vbCrLf &
"								 @U_CardCode                                                    " & vbCrLf &
"      end                                                                                      " & vbCrLf &
"   close F_Orden_Venta                                                                         " & vbCrLf &
"   deallocate F_Orden_Venta                                                                    " & vbCrLf &
"LINEA_salir_procedimiento:                                                                     " & vbCrLf &
"  select rtrim(cast(CodBc as char(50))) +'|'+                                                  " & vbCrLf &
"         rtrim(cast('RECAU'+ltrim(str(CodEntBc,5)) as char(50))) +'|'+                         " & vbCrLf &
"		  rtrim(cast(DocEntryOV as char(50)))  +'|'+                                            " & vbCrLf &
"		  ltrim(str(NroCuota,5)) +'|'+                                                          " & vbCrLf &
"         rtrim(cast(Descripcion as char(50)))  +'|'+                                           " & vbCrLf &
"		  ltrim(str(Importe+Multa,13,2)) +'|' +                                                 " & vbCrLf &
"		  rtrim(cast(Fecha_Pago as char(10))) +'|' +                                            " & vbCrLf &
"		  ltrim(str(Multa,13,2)) +'|' +                                                         " & vbCrLf &
"		  rtrim(cast(NombreRazonSocial as char(70)))  +'|'+                                     " & vbCrLf &
"		  rtrim(cast(Identificacion as char(20)))  +'|'+                                        " & vbCrLf &
"		  ltrim(str(Numero,10)) +'|' +                                                          " & vbCrLf &
"		  rtrim(cast(Agencia as char(30))) +'|'+                                                " & vbCrLf &
"		  rtrim(cast(Datos as char(100)))                                                       " & vbCrLf &
"    from @Cuota_calculada_bc                                                                   " & vbCrLf &
"	where Cubierta = 0                                                                          " & vbCrLf &
"  return 0                                                                                     " & vbCrLf &
"LINEA_error:                                                                                   " & vbCrLf &
"    set @error_msg = rtrim(@error_msg)                                                         " & vbCrLf &
"    return -1 "
        Return res
    End Function


    Public Const sp_Cuotas_x_pagar_detalle_bc_name As String = "sp_Cuotas_x_pagar_detalle_bc"
    Public Shared Function sp_Cuotas_x_pagar_detalle_bc_text() As String
        Dim res As String
        res = "CREATE Procedure dbo.sp_Cuotas_x_pagar_detalle_bc                                " & vbCrLf &
"                     @DocEntryOV  int = 0              -- Orden de Venta                       " & vbCrLf &
"                     ,@Fecha_pago  smalldatetime       -- Fecha del pago a realizarce          " & vbCrLf &
"                     ,@error_msg   varchar(100) = '' output                                    " & vbCrLf &
" As                                                                                            " & vbCrLf &
"-- 0. Declaración de variables                                                                 " & vbCrLf &
"			Declare @A_capital                     decimal(13,2),                               " & vbCrLf &
"					@A_tasa                        decimal(8,4),                                " & vbCrLf &
"					@A_nro_cuotas                  smallint,                                    " & vbCrLf &
"					@A_saldo                       decimal(13,2),                               " & vbCrLf &
"					@A_plazo_aux                   smallint,                                    " & vbCrLf &
"					@A_fecha_Ini_aux               smalldatetime,                               " & vbCrLf &
"					@A_Fecha_hasta                 smalldatetime,                               " & vbCrLf &
"					@A_Cap_pagado                  decimal(13,2),                               " & vbCrLf &
"					@A_Interes                     decimal(13,2),                               " & vbCrLf &
"					@Tot_Amortizado                decimal(13,2),                               " & vbCrLf &
"					@Por_pagar                     decimal(13,2),                               " & vbCrLf &
"					@Contador                      smallint,                                    " & vbCrLf &
"					@error_exec                    smallint,                                    " & vbCrLf &
"			        @A_cuota_cap                   decimal(12,2),                               " & vbCrLf &
"					@A_tl_Int_Carg                 decimal(13,2),                               " & vbCrLf &
"					@U_DocEntryPG                  int,                                         " & vbCrLf &
"					@Tasa                          decimal(8,4)  ,                              " & vbCrLf &
"					@Tasa_Vcdo                     decimal(8,4) ,                               " & vbCrLf &
"					@U_CardCode                    varchar(50),                                 " & vbCrLf &
"					@TasaNormalLegal               char(1) = 'N',                               " & vbCrLf &
"					@FechaCuotaVen                 smalldatetime,                               " & vbCrLf &
"					@TotalImpMult                  decimal(13,2) = 0,                           " & vbCrLf &
"					@DocEntryOF                    int,                                         " & vbCrLf &
"					@Fecha_valor                   smalldatetime,                               " & vbCrLf &
"					@FechaUltPago                  smalldatetime,                               " & vbCrLf &
"					@Nombre                        varchar(70),                                 " & vbCrLf &
"					@Identificacion                varchar(20),                                 " & vbCrLf &
"					@Contador_aux                  int                                          " & vbCrLf &
"			select  @A_tasa          = @Tasa,                                                   " & vbCrLf &
"					@Tot_Amortizado  = 0,                                                       " & vbCrLf &
"					@Tot_Amortizado  = 0,                                                       " & vbCrLf &
"					@Contador        = 2,                                                       " & vbCrLf &
"					@Contador_aux    = 1,                                                       " & vbCrLf &
"					@A_nro_cuotas    = 0,                                                       " & vbCrLf &
"					@Fecha_valor     = @Fecha_pago                                              " & vbCrLf &
"			If object_id('tempdb..@Amortizacion') is null                                       " & vbCrLf &
"			declare @Amortizacion table (                                                       " & vbCrLf &
"			     Mes int                                                                        " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                       " & vbCrLf &
"				,Saldo_capital decimal(13,2)                                                    " & vbCrLf &
"				,Dias smallint                                                                  " & vbCrLf &
"				,Interes decimal(13,2)                                                          " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                        " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                      " & vbCrLf &
"				,Por_Pagar_Cap   decimal(13,2)                                                  " & vbCrLf &
"				,Total_Cap_Amort decimal(13,2)                                                  " & vbCrLf &
"				,Cubierta   tinyint                                                             " & vbCrLf &
"				)                                                                               " & vbCrLf &
"		    If object_id('tempdb..@Cuota_calculada') is null                                    " & vbCrLf &
"			declare @Cuota_calculada table (                                                    " & vbCrLf &
"				 Mes smallint                                                                   " & vbCrLf &
"				,Descripcion varchar(30)                                                        " & vbCrLf &
"				,Fecha_Pago smalldatetime                                                       " & vbCrLf &
"				,Fecha_cuota smalldatetime                                                      " & vbCrLf &
"				,Fecha_valor smalldatetime                                                      " & vbCrLf &
"				,DiasCuot smallint                                                              " & vbCrLf &
"				,Interes decimal(13,2)                                                          " & vbCrLf &
"				,DiasIntMor  smallint                                                           " & vbCrLf &
"				,IntPorMor decimal(13,2)                                                        " & vbCrLf &
"				,Multa decimal(13,2)                                                            " & vbCrLf &
"				,Amort_Cap decimal(13,2)                                                        " & vbCrLf &
"				,GastosA decimal(13,2)                                                          " & vbCrLf &
"				,Total_Cuota decimal(13,2)                                                      " & vbCrLf &
"				,Cubierta tinyint                                                               " & vbCrLf &
"				)                                                                               " & vbCrLf &
"			If object_id('tempdb..@Cuota_calculada_bc') is null                                 " & vbCrLf &
"			declare @Cuota_calculada_bc table (                                                 " & vbCrLf &
"			     CodBc    varchar(50)                                                           " & vbCrLf &
"				,CodEntBc varchar(50)                                                           " & vbCrLf &
"				,DocEntryOV  varchar(15)                                                        " & vbCrLf &
"				,NroCuota    int                                                                " & vbCrLf &
"				,Descripcion varchar(30)                                                        " & vbCrLf &
"				,Importe    decimal(13,2)                                                       " & vbCrLf &
"				,Fecha_Pago char(10)                                                            " & vbCrLf &
"				,Fecha_cuota smalldatetime                                                      " & vbCrLf &
"				,Multa     decimal(13,2)                                                        " & vbCrLf &
"				,DiasMulta smallint                                                             " & vbCrLf &
"				,NombreRazonSocial  varchar(70)                                                 " & vbCrLf &
"				,Identificacion varchar(20)                                                     " & vbCrLf &
"				,Numero     int                                                                 " & vbCrLf &
"				,Agencia    varchar(30)                                                         " & vbCrLf &
"				,Datos      varchar(100)                                                        " & vbCrLf &
"				,Cubierta   tinyint                                                             " & vbCrLf &
"				)                                                                               " & vbCrLf &
" declare F_Orden_Venta cursor fast_forward for                                                 " & vbCrLf &
"  select DocEntry,                                                                             " & vbCrLf &
"         DocDate,                                                                              " & vbCrLf &
"         U_DocCuota,                                                                           " & vbCrLf &
"         U_Interes,                                                                            " & vbCrLf &
"		 U_IntMor,                                                                              " & vbCrLf &
"		 CardCode                                                                               " & vbCrLf &
"    from ORDR  -- Tabla Maestro Orden de Venta                                                 " & vbCrLf &
"   where CANCELED = 'N'                                                                        " & vbCrLf &
"     and DocStatus = 'O'                                                                       " & vbCrLf &
"     and DocEntry = (case when @DocEntryOV = 0 then DocEntry else @DocEntryOV end)             " & vbCrLf &
"	  and U_DocCuota is not null                                                                " & vbCrLf &
" open F_Orden_Venta                                                                            " & vbCrLf &
"   fetch next from F_Orden_Venta into @DocEntryOV,                                             " & vbCrLf &
"                                      @A_fecha_Ini_aux,                                        " & vbCrLf &
"                                      @U_DocEntryPG,                                           " & vbCrLf &
"								       @Tasa,                                                   " & vbCrLf &
"								       @Tasa_Vcdo,                                              " & vbCrLf &
"								       @U_CardCode                                              " & vbCrLf &
"   while @@fetch_status = 0                                                                    " & vbCrLf &
"	  begin                                                                                     " & vbCrLf &
"	    delete @Amortizacion                                                                    " & vbCrLf &
"		delete @Cuota_calculada                                                                 " & vbCrLf &
"		set @Tot_Amortizado = 0                                                                 " & vbCrLf &
"		set @A_Cap_pagado = 0                                                                   " & vbCrLf &
"	    set @FechaUltPago = @A_fecha_Ini_aux -- OJO hay que obtener la fecha de ultimo pago     " & vbCrLf &
"		select @DocEntryOF  = TrgetEntry -- DocEntry OINV Tabla Factura                         " & vbCrLf &
"          from RDR1 -- Tabla Detalle Orden de Venta                                            " & vbCrLf &
"         where DocEntry = @DocEntryOV -- DocEntry Orden de Venta                               " & vbCrLf &
"	       and TargetType = 13 -- Tipo Factura                                                  " & vbCrLf &
"  if isnull(@DocEntryOF,0) <> 0 -- Si existe factura obtenemos lo total pagado                 " & vbCrLf &
"   begin                                                                                       " & vbCrLf &
"	select @A_Cap_pagado = PaidToDate from OINV as A  where DocEntry = @DocEntryOF              " & vbCrLf &
"   end                                                                                         " & vbCrLf &
" else                                                                                          " & vbCrLf &
"   begin -- Si no existe Factura, vemos los pagos recibidos de esa orden de vanta              " & vbCrLf &
"      select @A_Cap_pagado = sum(DocTotal) from ORCT where Canceled = 'N' and U_ORDV = @DocEntryOV    " & vbCrLf &
"   end                                                                                         " & vbCrLf &
"   set @A_Cap_pagado = isnull(@A_Cap_pagado, 0)                                                " & vbCrLf &
"   insert into @Amortizacion                                                                   " & vbCrLf &
"   select 0, U_FechaV, 0, 0, 0, U_Amortizacion , U_Amortizacion , U_Amortizacion, 0, 0         " & vbCrLf &
"     from [@EXX_CUOIN]                                                                         " & vbCrLf &
"    where U_DocEntryPG = @U_DocEntryPG                                                         " & vbCrLf &
"	  and U_CapitalxPagar <> '0'                                                                " & vbCrLf &
"    order by U_FechaV asc                                                                      " & vbCrLf &
"  select @TasaNormalLegal = U_Intlgl                                                           " & vbCrLf &
"    from [@EXX_LICRE]                                                                          " & vbCrLf &
"   where U_CardCode = @U_CardCode  -- N,Y                                                      " & vbCrLf &
"  select @Nombre = CardNAme,                                                                   " & vbCrLf &
"         @Identificacion = LicTradNum                                                          " & vbCrLf &
"    from OCRD                                                                                  " & vbCrLf &
"   where CardCode = @U_CardCode                                                                " & vbCrLf &
"  if @TasaNormalLegal = 'N'                                                                    " & vbCrLf &
"   set @A_tasa = @Tasa                                                                         " & vbCrLf &
"   select @A_nro_cuotas =  count(*) ,                                                          " & vbCrLf &
"          @A_capital = sum(Amort_Cap),                                                         " & vbCrLf &
"	      @A_Fecha_hasta  = min(Fecha_Pago)                                                     " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"   set @Contador = 1                                                                           " & vbCrLf &
"   while @Contador <= @A_nro_cuotas                                                            " & vbCrLf &
"   begin                                                                                       " & vbCrLf &
"      select @A_Fecha_hasta = min(Fecha_Pago)                                                  " & vbCrLf &
"        from @Amortizacion                                                                     " & vbCrLf &
"       where Mes = 0                                                                           " & vbCrLf &
"	  update @Amortizacion                                                                      " & vbCrLf &
"	      set Mes = @Contador                                                                   " & vbCrLf &
"	 where Fecha_Pago = @A_Fecha_hasta                                                          " & vbCrLf &
"     select @A_saldo = @A_capital - @Tot_Amortizado                                            " & vbCrLf &
"      exec @error_exec = calculo_interes                                                       " & vbCrLf &
"                       @Saldo        = @A_saldo,                                               " & vbCrLf &
"					   @Fecha_desde  = @A_fecha_Ini_aux,                                        " & vbCrLf &
"					   @Fecha_hasta  = @A_Fecha_hasta,                                          " & vbCrLf &
"					   @Tasa         = @A_tasa,                                                 " & vbCrLf &
"                      @Interes      = @A_Interes OUTPUT,                                       " & vbCrLf &
"                       @error_msg    = @error_msg OUTPUT                                       " & vbCrLf &
"      IF @@error <> 0                                                                          " & vbCrLf &
"		BEGIN                                                                                   " & vbCrLf &
"		set @error_msg='Error al llamar calculo_interes.'                                       " & vbCrLf &
"		goto linea_error                                                                        " & vbCrLf &
"		END                                                                                     " & vbCrLf &
"      IF @error_exec <> 0 goto linea_error                                                     " & vbCrLf &
"   update @Amortizacion                                                                        " & vbCrLf &
"      set Saldo_capital = @A_capital - @Tot_Amortizado,                                        " & vbCrLf &
"	      Dias = datediff(d, @A_fecha_Ini_aux, Fecha_Pago),                                     " & vbCrLf &
"		  Interes = @A_Interes,                                                                 " & vbCrLf &
"		  Total_Cuota = Amort_Cap + @A_Interes,                                                 " & vbCrLf &
"		  Por_Pagar_Cap = (case when @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 0 else case when (@A_Cap_pagado - @Tot_Amortizado) > 0 then  Amort_Cap - (@A_Cap_pagado - @Tot_Amortizado) else Amort_Cap end end), " & vbCrLf &
"		  Total_Cap_Amort = @Tot_Amortizado + Amort_Cap,                                        " & vbCrLf &
"		  Cubierta = (case when @Tot_Amortizado + Amort_Cap <= @A_Cap_pagado then 1 else 0 end) " & vbCrLf &
"    where Mes = @Contador                                                                      " & vbCrLf &
"   select @Tot_Amortizado = @Tot_Amortizado + Amort_Cap,                                       " & vbCrLf &
"          @A_fecha_Ini_aux = Fecha_Pago                                                        " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"    where Mes = @Contador                                                                      " & vbCrLf &
"   set @Contador = @Contador + 1                                                               " & vbCrLf &
"end -- while @Contador <= @A_nro_cuotas                                                        " & vbCrLf &
"   insert into @Cuota_calculada                                                                " & vbCrLf &
"   select Mes, --case when Mes > @Contador_aux then Mes -  @Contador_aux else Mes end ,        " & vbCrLf &
"          'PAGO DE CUOTA No. '+ ltrim(str(Mes)),                                               " & vbCrLf &
"          @Fecha_pago,                                                                         " & vbCrLf &
"		   Fecha_Pago,                                                                          " & vbCrLf &
"		   @Fecha_Valor,                                                                        " & vbCrLf &
"          isnull(Dias,0),                                                                      " & vbCrLf &
"		  --case when @Fecha_Valor > Fecha_Pago then isnull(Interes,0)                          " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"		  0,--isnull(datediff(d, fecha_pago, @Fecha_pago),0),                                   " & vbCrLf &
"		  -- CALCULO Tasa Moratorio                                                             " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"	      -- CALCULO Tasa Multa ---------------------------------------                         " & vbCrLf &
"  		  0,                                                                                    " & vbCrLf &
"		  -------------------------------------------------------------                         " & vbCrLf &
"		  isnull(Por_Pagar_Cap, 0) , -- Capital por pagar                                       " & vbCrLf &
"		  0, -- Gastos Adicionales                                                              " & vbCrLf &
"		  0,                                                                                    " & vbCrLf &
"		  Cubierta                                                                              " & vbCrLf &
"     from @Amortizacion                                                                        " & vbCrLf &
"--- MULTAS                                                                                     " & vbCrLf &
"    select @FechaCuotaVen = min(Fecha_cuota)                                                   " & vbCrLf &
"	   from @Cuota_calculada                                                                    " & vbCrLf &
"   exec @error_exec = dbo.sp_Calculo_Multa                                                     " & vbCrLf &
"             @FechaCuotaVen = @FechaCuotaVen                                                   " & vbCrLf &
"			 ,@Fecha_pago    = @Fecha_pago                                                      " & vbCrLf &
"			 ,@TotalImpMult  = @TotalImpMult output                                             " & vbCrLf &
"		     ,@error_msg     = @error_msg output                                                " & vbCrLf &
"   IF @@error <> 0                                                                             " & vbCrLf &
"	 BEGIN                                                                                      " & vbCrLf &
"	   SET @error_msg='Error al llamar calculo_interes.'                                        " & vbCrLf &
"		GOTO linea_error                                                                        " & vbCrLf &
"	 END                                                                                        " & vbCrLf &
"    IF @error_exec <> 0 GOTO linea_error                                                       " & vbCrLf &
"   update @Cuota_calculada                                                                     " & vbCrLf &
"      set Multa = isnull(@TotalImpMult,0)                                                      " & vbCrLf &
"    where Fecha_cuota  = @FechaCuotaVen                                                        " & vbCrLf &
"   insert into @Cuota_calculada_bc                                                             " & vbCrLf &
"    select 'CodBc' , @DocEntryOV , @DocEntryOV, Mes, Descripcion, Amort_Cap, convert(char(10),@Fecha_pago, 105 ), Fecha_cuota, Multa, 0, @Nombre, @Identificacion, 0, 'Agencia', 'XXXXXXXXXXX',Cubierta   " & vbCrLf &
"	  from @Cuota_calculada                                                                     " & vbCrLf &
"	fetch next from F_Orden_Venta into @DocEntryOV,                                             " & vbCrLf &
"                                @A_fecha_Ini_aux,                                              " & vbCrLf &
"                                @U_DocEntryPG,                                                 " & vbCrLf &
"								 @Tasa,                                                         " & vbCrLf &
"								 @Tasa_Vcdo,                                                    " & vbCrLf &
"								 @U_CardCode                                                    " & vbCrLf &
"      end                                                                                      " & vbCrLf &
"   close F_Orden_Venta                                                                         " & vbCrLf &
"   deallocate F_Orden_Venta                                                                    " & vbCrLf &
"LINEA_salir_procedimiento:                                                                     " & vbCrLf &
"select rtrim(cast(CodBc as char(50))) +'|'+                                                    " & vbCrLf &
"          rtrim(cast('RECAU'+CodEntBc as char(50))) +'|'+                                      " & vbCrLf &
"		  ltrim(str(NroCuota,5)) +'|'+                                                          " & vbCrLf &
"          '1'+'|'+                                                                             " & vbCrLf &
"		  '1'+'|'+                                                                              " & vbCrLf &
"		  'capital'+'|'+                                                                        " & vbCrLf &
"          ltrim(str(Importe,13,2))                                                             " & vbCrLf &
"    from @Cuota_calculada_bc                                                                   " & vbCrLf &
"	where Importe > 0                                                                           " & vbCrLf &
"	  and Cubierta = 0                                                                          " & vbCrLf &
"	union all                                                                                   " & vbCrLf &
"	select rtrim(cast(CodBc as char(50))) +'|'+                                                 " & vbCrLf &
"          rtrim(cast('RECAU'+CodEntBc as char(50))) +'|'+                                      " & vbCrLf &
"		  ltrim(str(NroCuota,5)) +'|'+                                                          " & vbCrLf &
"          '2'+'|'+                                                                             " & vbCrLf &
"		  '2'+'|'+                                                                              " & vbCrLf &
"		  'intetes'+'|'+                                                                        " & vbCrLf &
"          ltrim(str(Multa,13,2))                                                               " & vbCrLf &
"    from @Cuota_calculada_bc                                                                   " & vbCrLf &
"	where Multa > 0                                                                             " & vbCrLf &
"	  and Cubierta = 0                                                                          " & vbCrLf &
"	order by 1                                                                                  " & vbCrLf &
"  return 0                                                                                     " & vbCrLf &
"LINEA_error:                                                                                   " & vbCrLf &
"    set @error_msg = rtrim(@error_msg)                                                         " & vbCrLf &
"    return -1 "
        Return res
    End Function

    Public Const sp_Calculo_Interes_Mora_name As String = "sp_Calculo_Interes_Mora"
    Public Shared Function sp_Calculo_Interes_Mora_text() As String
        Dim res As String
        res = "CREATE PROCEDURE [dbo].[sp_Calculo_Interes_Mora] " & vbCrLf &
"	@FechaPago Date,  " & vbCrLf &
"	@FechaVencimiento date,   " & vbCrLf &
"	@Tipo_Periodo nvarchar(1), " & vbCrLf &
"	@InteresTasa decimal(18,2), " & vbCrLf &
"	@MontoPendienteDePago decimal(18,2), " & vbCrLf &
"	@TasaAnualMoratoria decimal(18,2), " & vbCrLf &
"	@DiasParaTasaAnualMoratoria integer " & vbCrLf &
"AS " & vbCrLf &
"BEGIN " & vbCrLf &
"	SET NOCOUNT ON; " & vbCrLf &
"declare @InteresTasaCalculada as decimal(18,2) = 0 " & vbCrLf &
"	declare @DiasEnMora as integer = 0 " & vbCrLf &
"	declare @TasaAnualMoratoriaCalculada as decimal(18,6) = 0 " & vbCrLf &
"	declare @TotalInteres as decimal(18,2) = 0 " & vbCrLf &
"	declare @TotalCapitalMasInteres as decimal(18,2) = 0 " & vbCrLf &
"	declare @TotalMora as decimal(18,2) = 0 " & vbCrLf &
"	declare @TotalGeneral as decimal(18,2) = 0 " & vbCrLf &
"	declare @TotalAFacturar as decimal(18,2) = 0 " & vbCrLf &
"if (@FechaPago > @FechaVencimiento) " & vbCrLf &
"begin " & vbCrLf &
"	If(@Tipo_Periodo ='S') " & vbCrLf &
"		BEGIN " & vbCrLf &
"			set @InteresTasaCalculada =  @InteresTasa/2 " & vbCrLf &
"		END " & vbCrLf &
"		If(@Tipo_Periodo ='M') " & vbCrLf &
"		BEGIN " & vbCrLf &
"			set @InteresTasaCalculada =  @InteresTasa/12 " & vbCrLf &
"					END " & vbCrLf &
"	set @DiasEnMora = DATEDIFF(dd,@FechaVencimiento,@FechaPago) " & vbCrLf &
"	end " & vbCrLf &
"	set @TotalInteres = @MontoPendienteDePago *  @InteresTasaCalculada " & vbCrLf &
"	set @TotalCapitalMasInteres = (@MontoPendienteDePago + @TotalInteres) " & vbCrLf &
"	set @TasaAnualMoratoriaCalculada = @TasaAnualMoratoria/ @DiasParaTasaAnualMoratoria " & vbCrLf &
"	set @TotalMora = @TotalCapitalMasInteres * @TasaAnualMoratoriaCalculada * @DiasEnMora " & vbCrLf &
"	set @TotalGeneral = @TotalCapitalMasInteres + @TotalMora " & vbCrLf &
"	set @TotalAFacturar = @TotalInteres + @TotalMora " & vbCrLf &
"select  @MontoPendienteDePago AS Capital,  " & vbCrLf &
"		@InteresTasaCalculada as InteresAplicable,   " & vbCrLf &
"		@TotalInteres as MontoInteresReal ,   " & vbCrLf &
"		@TotalCapitalMasInteres  as TotalCapMasInteres,  " & vbCrLf &
"		@DiasEnMora as DiasMora, " & vbCrLf &
"		@TotalMora as MontoDeInteresPorMora, " & vbCrLf &
"		@TotalGeneral as TotalGeneral, " & vbCrLf &
"		@TotalAFacturar as TotalAFacturar " & vbCrLf &
"END"
        Return res
    End Function

    Public Shared Function GetInfoLineasOV(DocEntry As String) As String
        Dim query As String = String.Empty
        Try
            query = "Select R1.LineNum, R1.ItemCode, R1.Dscription,	R1.Quantity, R1.WhsCode, R1.Price, R1.PriceAfVAT, R1.Currency, R1.CogsOcrCod," &
                    " R1.CogsOcrCo2, R1.CogsOcrCo3, R1.CogsOcrCo4, R1.CogsOcrCo5, R1.Project" &
                    " FROM ORDR OD INNER JOIN RDR1 R1 On OD.DocEntry = R1.DocEntry WHERE OD.DocEntry = '" & DocEntry & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

End Class

            

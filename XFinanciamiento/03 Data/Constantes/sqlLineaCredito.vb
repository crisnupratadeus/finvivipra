﻿Public Class sqlLineaCredito
    Public Const getBranchBD As String = "getBranchBD"
    Public Shared Function getBranchBdSQL() As String
        Return _
        "SELECT MltpBrnchs, DflBranch, BrachNum from OADM"
    End Function
    Public Shared Function PopulaCmbReq() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT DocEntry,U_Descr FROM ""@EXX_FIPLAN"" WHERE U_Tipo = 'R'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function
    Public Shared Function PopulaCmbGrupArt() As String ' ssh
        Dim query As String = String.Empty
        Try
            query = "SELECT ItmsGrpCod, ItmsGrpNam FROM OITB"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function
    Public Shared Function PopulaCmbGar() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT DocEntry,U_Descr FROM [@EXX_FIPLAN] WHERE U_Tipo = 'G'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Friend Shared Function PopulaGridReqGar(DocEntryPlantilla As String, Code As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT [@EXX_FIPLA1].U_Descr, [@EXX_FIPLD1].U_Valor AS Descripcion FROM  [@EXX_FIPLA1] LEFT OUTER JOIN [@EXX_FIPLD1] ON [@EXX_FIPLA1].U_Codigo = [@EXX_FIPLD1].U_Codigo AND [@EXX_FIPLA1].DocEntry = [@EXX_FIPLD1].U_Plantilla WHERE ([@EXX_FIPLA1].DocEntry = '"& DocEntryPlantilla &"') AND ([@EXX_FIPLD1].U_CodeLineaCred = '"& Code &"')"
                
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

        Public Shared Function ObtenerCodeLiCre(U_CardCode As String) As String
            Dim query As String = String.Empty
            Try
                query = " SELECT ""Code"" from ""@EXX_LICRE"" where ""U_CardCode"" = '" & u_CardCode & "';"
                Return query
            Catch ex As Exception
                Throw ex
                Return ""
            End Try
        End Function

    Public Shared Function GetSNLineasCredito(UsrCode As String) As String
        Dim query As String = String.Empty

        Try

            query = "Select Code AS Codigo, U_estado AS Estado, U_fechaHa AS Fecha_Hasta from [@EXX_LICRE] where U_CardCode = '" & Trim(UsrCode) & "' "
            'query = "Select DocEntry from [@EXX_LICRE] where U_CardCode = '" & Trim(UsrCode) & "' "
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetCodeFormSNLiCre(UsrName As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT CardName FROM OCRD WHERE CardCode = '" & Trim(UsrName) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetRegistroLiCre(SocioNegocio As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT DocEntry, U_CardCode, U_Estado, U_Inactive,	U_Interes, U_InteresM" &
                    " FROM [@EXX_LICRE] WHERE U_CardCode = '" & SocioNegocio & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetRegistroLiCre2(ItemCode As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ItmsGrpCod, ItemCode, U_TasaNor, U_TasaLeg FROM OITM OT INNER JOIN [@EXX_LICR2] L2 ON OT.ItmsGrpCod = L2.U_CodGrupArt WHERE OT.ItemCode ='" & ItemCode & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

End Class

﻿Public Class sqlGarLinea
    Public Const getBranchBD As String = "getBranchBD"
    Public Shared Function getBranchBdSQL() As String
        Return _
        "SELECT MltpBrnchs, DflBranch, BrachNum from OADM"


    End Function
    Public Shared Function getDetalleGarantiaSQL(U_Codigo As String) As String

        Dim query As String = String.Empty

            query = "SELECT [DocEntry] ,[LineId] ,[VisOrder] ,[Object] ,[LogInst] ,[U_Codigo] ,[U_Fiplan_codigo],[U_Fipla1_codigo],[U_TipoC],[U_Oblig],[U_Value] FROM [@EXX_FIPLD1] WHERE [U_Codigo] = '" & U_Codigo & "'"

        Return query

    End Function


    Public Shared Function getListaFipla1(DocEntry As String, Operacion As String) As String

        Dim query As String = String.Empty

        query = "SELECT [@EXX_FIPLA1].DocEntry, [@EXX_FIPLA1].LineId, [@EXX_FIPLA1].VisOrder, [@EXX_FIPLA1].Object, [@EXX_FIPLA1].LogInst," &
                " [@EXX_FIPLAN].U_Codigo AS [U_Fiplan_codigo], [@EXX_FIPLA1].U_Codigo AS [U_Fipla1_codigo], [@EXX_FIPLA1].U_Descr," &
                " [@EXX_FIPLA1].U_TipoC, [@EXX_FIPLA1].U_Oblig" &
                " FROM [@EXX_FIPLA1] INNER JOIN [@EXX_FIPLAN] ON [@EXX_FIPLA1].DocEntry = [@EXX_FIPLAN].DocEntry" &
                " WHERE [@EXX_FIPLAN].DocEntry = '" & Operacion & "'"

        Return query

    End Function

    Friend Shared Function insertaDetalleGarFIPLD1(U_Plantilla As String, U_Valor As String, U_CodeLineaCred As String, U_CodeDoc As String, U_TipoDoc As String) As String
        Dim sSqlInsert As String

        sSqlInsert = "INSERT INTO [@EXX_FIPLD1]" + vbCrLf +
                     "(Code, Name, U_Codigo, U_Plantilla, U_Valor, U_CodeLineaCred, U_CodeDoc,	U_TipoDoc)" + vbCrLf +
                     "SELECT (RIGHT('00000000' + CONVERT(VARCHAR(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), (RIGHT('00000000' + convert(varchar(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), '" & U_Plantilla & "','" & U_CodeDoc & "', '" & U_Valor & "', '" & U_CodeLineaCred & "', '" & U_CodeDoc & "', '" & U_TipoDoc & "' FROM [@EXX_FIPLD1]"
        Return sSqlInsert
End Function

End Class

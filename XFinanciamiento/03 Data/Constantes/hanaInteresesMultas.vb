﻿Public Class hanaInteresesMultas
    Public Const getBranchBD As String = "getBranchBD"


    'Friend Shared Function insertaDetalleGarFIPLD1(U_Plantilla As String, U_Valor As String, U_CodeLineaCred As String, U_CodeDoc As String, U_TipoDoc As String) As String
    'Dim sSqlInsert As String

    '    'sSqlInsert  = "INSERT INTO [dbo].[@EXX_FIPLD1] ([Code] ,[Name],[U_Codigo] ,[U_Name],[U_Plantilla],[U_Valor] ,[U_CodeLineaCred],[U_CodeDoc],[U_TipoDoc]) VALUES ('" & code & "'," & "'" & Name & "',"  & "'" & U_Codigo & "'," & "'" & U_Name & "',"  & "'" & U_Plantilla & "'," & "'" & U_Valor & "'," & "'" & U_CodeLineaCred & "'," & "'" & U_CodeDoc & "'," & "'" & U_TipoDoc & "')"

    '    sSqlInsert = "INSERT INTO [@EXX_FIPLD1]" + vbCrLf +
    '                 "(Code, Name, U_Codigo, U_Plantilla, U_Valor, U_CodeLineaCred, U_CodeDoc,	U_TipoDoc)" + vbCrLf +
    '                 "SELECT (RIGHT('00000000' + CONVERT(VARCHAR(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), (RIGHT('00000000' + convert(varchar(8), ISNULL(MAX(CONVERT(INT, Code)), 0) + 1), 8)), '" & U_Plantilla & "','" & U_CodeDoc & "', '" & U_Valor & "', '" & U_CodeLineaCred & "', '" & U_CodeDoc & "', '" & U_TipoDoc & "' FROM [@EXX_FIPLD1]"
    '    Return sSqlInsert

    'End Function

    Friend Shared Function getInteresMulta(fechaPago As String, fechaVencimiento As String, tipo_Periodo As String, interesTasa As String, montoPendienteDePago As String, tasaAnualMoratoria As String, diasParaTasaAnualMoratoria As String) As String

        Dim sp As String = "CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_INTERES_MORA_HANA"" (  " & vbCrLf &
"'" & fechaPago & "', " & vbCrLf &
"'" & fechaVencimiento & "',    " & vbCrLf &
"'" & tipo_Periodo & "',  " & vbCrLf &
"'" & interesTasa & "', " & vbCrLf &
"'" & montoPendienteDePago & "',	 " & vbCrLf &
"'" & tasaAnualMoratoria & "', " & vbCrLf &
"'" & diasParaTasaAnualMoratoria & "' " & vbCrLf &
")"
        Return sp

    End Function

    Friend Shared Function getDatosFacturaParaPago(DocNum As String, CardCode As String) As String
        Dim query As String = "SELECT A.""DocEntry"",  " & vbCrLf &
 "A.""DocNum"",  " & vbCrLf &
 "A.""CardCode"",  " & vbCrLf &
 "A.""U_OrdenVenta"",  " & vbCrLf &
 "A.""DocStatus"",  " & vbCrLf &
 "A.""U_DocCuota"",  " & vbCrLf &
 "A.""U_CuotFi"",  " & vbCrLf &
 "A.""U_Interes"",  " & vbCrLf &
 "A.""U_IntMor"",  " & vbCrLf &
 "A.""DocDueDate"",  " & vbCrLf &
 "A.""DocTotal"",  " & vbCrLf &
 "A.""PaidToDate"",  " & vbCrLf &
 "A.""TaxDate"",  " & vbCrLf &
 "A.""DocDate"" " & vbCrLf &
"From """ & SQLBaseDatos.ToUpper & """.""OINV"" As A  " & vbCrLf &
"Where A.""DocNum"" = '" & DocNum & "' And A.""CardCode"" =  '" & CardCode & "'"
        Return query
    End Function

    Friend Shared Function getCalculo_pagov2(V_oV As String, V_Fecha_Ini As String, V_Fecha_UltPago As String, V_Fecha_pago As String, V_Fecha_valor As String, GastosA As String, PorFactura As String) As String

        Dim V_error_msg As String
        Dim CuotaFija As String
        Dim V_GastosA As Decimal

        V_error_msg = "1"
        CuotaFija = "S"
        V_GastosA = 0

        Dim sp As String = "Do " & vbCrLf &
                           "BEGIN " & vbCrLf &
                           "Declare Cuotas """ & SQLBaseDatos.ToUpper & """.""CALPAGOTYPE"";" & vbCrLf &
                           "Declare Pagos """ & SQLBaseDatos.ToUpper & """.""CALCPAGOTYPE"";" & vbCrLf &
                           "call """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_PAGO_HANA"" (  " & vbCrLf &
                "" & V_oV & ", " & vbCrLf & 'V_Capital
                "'" & V_Fecha_Ini & "', " & vbCrLf & 'V_Cuotas
                "'" & V_Fecha_UltPago & "', " & vbCrLf & 'V_Tasa
                "'" & V_Fecha_pago & "', " & vbCrLf & 'V_Lapso
                "'" & V_Fecha_valor & "', " & vbCrLf & 'V_Fecha_Ini
                "" & V_error_msg & ", " & vbCrLf & 'V_Cap_Pagado
                "" & GastosA & ", " & vbCrLf &
                "" & PorFactura & ", " & vbCrLf &
                ":Cuotas," & vbCrLf &
                ":Pagos );" & vbCrLf &
                "select * from :Pagos;" & vbCrLf &
                "end;"

        Return sp

    End Function

    Friend Shared Function getUltimaFechaPagada(u_DocEntryF As String, u_CardCode As String) As String
        Dim query As String = "Select MAX(it.""U_FechaVa"") As U_FechaVa" & vbCrLf &
        "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOT1"" AS fa INNER JOIN " & SQLBaseDatos.ToUpper & ".""@EXX_INTERESES"" AS it ON fa.""DocEntry"" = it.""DocEntry""" & vbCrLf &
        "WHERE(fa.""U_DocEntryF"" = '" & u_DocEntryF & "') AND (it.""U_CardCode"" = '" & u_CardCode & "')"
        Return query
    End Function


    Friend Shared Function getFechaInicioCuotas(U_DocEntryOV As String) As String

        Dim query As String = "Select MIN(it.""U_FechaV"") As U_FechaV" & vbCrLf &
        "FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN"" AS it " & vbCrLf &
        "WHERE(it.""U_DocEntryOV"" = '" & U_DocEntryOV & "')" & vbCrLf &
        "And ""U_CapitalxPagar"" <> '0'"
        Return query

    End Function

    Friend Shared Function getDocEntryOV(U_DocEntryOV As String) As String

        Dim query As String = "Select it.""DocEntry"" " & vbCrLf &
        "FROM """ & SQLBaseDatos.ToUpper & """.""ORDR"" AS it " & vbCrLf &
        "WHERE(it.""DocNum"" = '" & U_DocEntryOV & "')"
        Return query

    End Function

End Class

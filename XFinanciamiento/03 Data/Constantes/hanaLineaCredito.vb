﻿Public Class hanaLineaCredito
    Public Const getBranchBD As String = "getBranchBD"
    Public Shared Function getBranchBdHANA() As String
        Return _
        "SELECT ""MltpBrnchs"", ""DflBranch"", ""BrachNum"" from """ & SQLBaseDatos.ToUpper & """.OADM"
    End Function
    Public Shared Function PopulaCmbReq() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""DocEntry"",""U_Descr"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLAN"" WHERE ""U_Tipo"" = 'R'" 'mauricio

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function PopulaCmbGrupArt() As String ' ssh
        Dim query As String = String.Empty
        Try
            query = "SELECT ""ItmsGrpCod"",""ItmsGrpNam"" FROM """ & SQLBaseDatos.ToUpper & """.""OITB"" "

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function PopulaCmbGar() As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""DocEntry"",""U_Descr"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLAN"" WHERE ""U_Tipo"" = 'G'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Friend Shared Function PopulaGridReqGar(DocEntryPlantilla As String, Code As String) As String
        Dim query As String = String.Empty
        Try
            query = " SELECT """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"".""U_Descr"", """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"".""U_Valor"" AS ""Descripcion""  FROM  """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"" LEFT OUTER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"" ON """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"".""U_Codigo"" = """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"".""U_Codigo"" AND """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"".""DocEntry"" = """ & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"".""U_Plantilla""  WHERE (""" & SQLBaseDatos.ToUpper & """.""@EXX_FIPLA1"".""DocEntry"" = '" & DocEntryPlantilla & "') AND (""" & SQLBaseDatos.ToUpper & """.""@EXX_FIPLD1"".""U_CodeLineaCred"" = '" & Code & "');"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try

    End Function

    Friend Shared Function ObtenerCodeLicre(u_CardCode As String) As String
        Dim query As String = String.Empty
        Try
            query = " SELECT ""Code"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"" where ""U_CardCode"" = '" & u_CardCode & "';"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetCodeFormSNLiCre(SNName As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""CardName"" FROM """ & SQLBaseDatos.ToUpper & """.""OCRD"" WHERE ""CardCode"" = '" & Trim(SNName) & "'"
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetSNLineasCredito(SNCode As String) As String
        Dim query As String = String.Empty

        Try

            ' = "SELECT ""A"".""PrjCode"" AS ""CODE"",IFNULL(""A"".""PrjName"",'') AS ""NAME"" , IFNULL(""B"".""U_Enabled"",'N') AS ""ACTIVO"",IFNULL(""B"".""Code"",'') AS ""CODETBL"",IFNULL(""B"".""Name"",'') AS ""NAMETBL"" FROM """ & SQLBaseDatos.ToUpper & """.""OPRJ"" ""A"" LEFT JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_USRPROY"" ""B"" ON (""A"".""PrjCode"" = ""B"".""U_ProjectC"" AND ""B"".""U_UsrCode"" = '" & Trim(UsrCode) & "') WHERE ""A"".""Active"" = 'Y' "
            query = "Select ""Code"" AS ""Codigo"", ""U_Estado"" AS ""Estado"", ""U_FechaHa"" AS ""Fecha_Hasta"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"" where ""U_CardCode"" = '" & Trim(SNCode) & "' "
            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function GetRegistroLiCre(SocioNegocio As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""DocEntry"", ""U_CardCode"", ""U_Estado"", ""U_Inactive"",	""U_Interes"", ""U_InteresM""" &
                    " FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"" WHERE ""U_CardCode"" = '" & SocioNegocio & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
    Public Shared Function GetRegistroLiCre2(ItemCode As String) As String
        Dim query As String = String.Empty
        Try
            query = "SELECT ""ItmsGrpCod"", ""ItemCode"", ""U_TasaNor"", ""U_TasaLeg"" FROM """ & SQLBaseDatos.ToUpper & """.OITM OT INNER JOIN """ & SQLBaseDatos.ToUpper & """.""@EXX_LICR2"" L2 ON OT.""ItmsGrpCod"" = L2.""U_CodGrupArt"" WHERE OT.""ItemCode"" = '" & ItemCode & "'"

            Return query
        Catch ex As Exception
            Throw ex
            Return ""
        End Try
    End Function
End Class

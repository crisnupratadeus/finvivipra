﻿Public Class hanaFacturasVencidasLinea
    Public Shared Function getFacturaVencidasLinea(CardCodeSN As String) As String
        Return _
    "SELECT " & vbCrLf &
    " A.""DocEntry""," & vbCrLf &
    " A.""DocNum"" AS ""NUMERO DE FACTURA""," & vbCrLf &
    " A.""NumAtCard"" AS ""REFERENCIA DE CLIENTE""," & vbCrLf &
    " A.""OvBase"" AS ""ORDEN DE VENTA""," & vbCrLf &
    " A.""FechaV"" AS ""FECHA VENCIMIENTO""," & vbCrLf &
    " A.""DocCur"" AS ""MONEDA""," & vbCrLf &
    " CASE WHEN A.""DocCur"" = 'CLP' THEN A.""DocTotal"" ELSE A.""DocTotalFC"" END AS ""TOTAL DOCUMENTO OV"", " & vbCrLf &
    " CASE WHEN A.""DocCur"" = 'USD' THEN A.""PaidToDate"" ELSE A.""PaidFC"" END AS ""TOTAL DOCUMENTO FC"", " & vbCrLf &
    " CASE WHEN A.""DocCur"" = 'USD' THEN A.""Saldo""	ELSE A.""SaldoFC"" END AS ""SALDO POR PAGAR FC"" " & vbCrLf &
    " FROM (SELECT i.""DocEntry"", i.""DocNum"", i.""NumAtCard"", " & vbCrLf &
    " CASE WHEN i1.""BaseType"" = 17 THEN r.""DocNum"" END AS ""OvBase"", " & vbCrLf &
    " i.""DocSubType"", CONVERT(varchar, i.""DocDueDate"", 103) AS ""FechaV"", i.""DocCur"", " & vbCrLf &
    " CAST(i.""DocTotal"" AS char) AS ""DocTotal"", " & vbCrLf &
    " CAST(i.""DocTotalFC"" AS char) AS ""DocTotalFC"", " & vbCrLf &
    " CAST(i.""PaidToDate"" AS char) AS ""PaidToDate"", " & vbCrLf &
    " CAST(i.""PaidFC"" AS char) AS ""PaidFC"", " & vbCrLf &
    " CAST((i.""DocTotal"" - i.""PaidToDate"") AS char) AS ""Saldo"", " & vbCrLf &
    " CAST((i.""DocTotalFC"" - i.""PaidFC"") AS char) AS ""SaldoFC"" " & vbCrLf &
    " FROM OINV i INNER JOIN INV1 i1 ON i.""DocEntry"" = i1.""DocEntry"" " & vbCrLf &
    " LEFT OUTER JOIN ORDR r ON (i1.""BaseEntry"" = r.""DocEntry"" AND (i1.""BaseType"" = 17 OR i1.""BaseType"" = 23)) " & vbCrLf &
    " LEFT OUTER JOIN RDR1 r1 ON r.""DocEntry"" = r1.""DocEntry"" " & vbCrLf &
    " WHERE i.""CardCode"" = '" & CardCodeSN & "' AND i.""Canceled"" = 'N' AND i.""PaidToDate"" < i.""DocTotal"" " & vbCrLf &
    " AND (i.""DocSubType"" = '--' OR i.""DocSubType"" = 'RI' OR i.""DocSubType"" = 'DN')) AS ""A"" " & vbCrLf &
    " GROUP BY A.""DocEntry"", A.""DocNum"", A.""NumAtCard"", A.""OvBase"", A.""FechaV"", A.""DocCur"", A.""DocTotal"", " & vbCrLf &
    " A.""DocTotalFC"", A.""PaidToDate"", A.""PaidFC"", A.""Saldo"", A.""SaldoFC"""
    End Function
End Class



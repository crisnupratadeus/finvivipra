﻿Public Class hanaReserva
    Public Const EsDocumentoDeVehiculos_NAME As String = "EsDocumentoDeVehiculos"
    Public Shared Function EsDocumentoDeVehiculos_TEXT(ItemCodes As String) As String
        Return _
    "select count(QryGroup64) from ""OITM"" where ItemCode in  (" & vbCrLf &
    ItemCodes & vbCrLf &
    ") and QryGroup64='Y'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const EsVehiculo_NAME As String = "EsVehiculo"
    Public Shared Function EsVehiculo_TEXT(ItemCode As String) As String
        Return _
    "select QryGroup64 from ""OITM"" where ItemCode ='" & ItemCode & "' and QryGroup64='Y'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const GetSumDec_NAME As String = "GetSumDec"
    Public Shared GetSumDec_TEXT As String = vbCrLf +
    "select sumdec from oadm"
    '---------------------------------------------------------------------------------------------------------------------------

    Public Const ActualizaCampoUsuario_NAME As String = "ActualizaCampoUsuario"
    Public Shared Function ActualizaCampoUsuario_TEXT(Docentry As String, CampoUsuario As String, Valor As String, Tabla As String) As String
        Return _
        "UPDATE """ & Tabla & """ set " & CampoUsuario & "='" & Valor & "' where DocEntry='" & Docentry & "'"
    End Function

    '-------------------------------------------------------------------------------------------------------------------
    Public Const GetSegundaCuenta_NAME As String = "GetSegundaCuenta_NAME"
    Public Shared Function GetSegundaCuenta_TEXT(Docentry As String, PrimeraCuenta As String) As String
        Return _
        "select top 1 Account  from JDT1 where transID=(select TransID from OPDN where docentry='" & Docentry & "') and Account not in ('" & PrimeraCuenta & "')"
    End Function

    '-------------------------------------------------------------------------------------------------------------------

    Public Shared Function ActualizaCampoUsuario_TEXT(Docentry As String, CampoUsuario As String, Valor As Decimal, Tabla As String) As String
        Return _
        "UPDATE """ & Tabla & """ set " & CampoUsuario & "=" & Valor & " where DocEntry='" & Docentry & "'"
    End Function

    '-------------------------------------------------------------------------------------------------------------------
    Public Shared Function ActualizaCampoUsuario_TEXT(Docentry As String, CampoUsuario As String, Valor As Integer, Tabla As String) As String
        Return _
        "UPDATE """ & Tabla & """ set " & CampoUsuario & "=" & Valor & " where DocEntry='" & Docentry & "'"
    End Function

    '-------------------------------------------------------------------------------------------------------------------
    Public Const GetAbsEntry_NAME As String = "GetAbsEntry"
    Public Shared Function GetAbsEntry_TEXT(sSerie As String, sItem As String) As String
        Return _
        "select TOP 1 AbsEntry as AbsEntry from OSRN where ItemCode = '" & sItem & "' and DistNumber= '" & sSerie & "' order by AbsEntry Desc "
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const getAjusteEntrada_NAME As String = "getAjusteEntrada"
    Public Shared Function getAjusteEntrada_TEXT(DocEntryBase As String, ItemCode As String, BoxPatent As String, _
                                                FechaEntrega As String) As String
        Return _
        "declare @ActualDate as date " & vbCrLf &
        "set @ActualDate='" & FechaEntrega & "'" & vbCrLf &
        "select " & vbCrLf &
        "--O.""QryGroup64"",P.""ItemCode"",P.""U_BoxPatent"",P.""Currency"",P.""Price"",C.""DocDate"",P.""Rate"",TC.""Rate"", " & vbCrLf &
        "P.""Price""*(TC.""Rate""-P.""Rate"") as ""Ajuste""  " & vbCrLf &
        " from ""OPCH"" C " & vbCrLf &
        "inner join ""PCH1"" P on C.""DocEntry""=P.""Docentry"" " & vbCrLf &
        "inner join ""OITM"" O on P.""ItemCode""=O.""ItemCode"" " & vbCrLf &
        "inner join ""ORTT"" TC on P.""Currency""=TC.""Currency"" and TC.""RateDate""=@ActualDate " & vbCrLf &
        "where P.""DocEntry""='" & DocEntryBase & "' and O.""QryGroup64""='Y' and P.""ItemCode""='" & ItemCode &
        "' and P.""U_BoxPatent""='" & BoxPatent & "' "
    End Function
    '-------------------------------------------------------------------------------------------------------------------

    Public Const Buscar_AsientoApertura_nocancelado_NAME As String = "Buscar_AsientoApertura_nocancelado"
    Public Shared Function Buscar_AsientoApertura_nocancelado_TEXT(DocEntryReserva As String) As String
        Return _
        "select TransId from OJDT " & vbCr &
        "where TransId not in (SELECT StornoToTr FROM OJDT WITH (NOLOCK) where isnull(StornoToTr,'')<>'') " & vbCr &
        "and U_BaseEntry='" & DocEntryReserva & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const getCampoOCDR_NAME As String = "getCampoOCDR"
    Public Shared Function getCampoOCDR_TEXT(CarCode As String, Campo As String) As String
        Return _
        "SELECT " & Campo & " FROM ""OCRD"" WHERE CardCode ='" & CarCode & "' "
    End Function
    '-------------------------------------------------------------------------------------------------------------------

    Public Const getFacturasReservaAsistente_NAME As String = "getFacturasReservaAsistente"
    Public Shared Function getFacturasReservaAsistente_TEXT(CardCodeProveedor As String) As String
        Return _
        "select " & vbCrLf &
        "'Y' as ""Elegir"" " & vbCrLf &
        ",'      ' as ""Entrega"" " & vbCrLf &
        ",C.DocEntry " & vbCrLf &
        ",C.DocNum " & vbCrLf &
        ",C.DocCur as ""Moneda Doc."" " & vbCrLf &
        ",L.U_BoxPatent as ""Cajon/Patente"" " & vbCrLf &
        ",L.ItemCode	as ""Modelo"" " & vbCrLf &
        ",L.Dscription as ""Descripción"" " & vbCrLf &
        ",L.Quantity as ""Cantidad"" " & vbCrLf &
        ",L.Currency as ""Moneda"" " & vbCrLf &
        ",L.Price as ""Precio"" " & vbCrLf &
        ",L.LineTotal as ""Total"" " & vbCrLf &
        ",C.JrnlMemo as ""Glosa"" " & vbCrLf &
        "from ""OPCH"" C   " & vbCrLf &
        "inner join ""PCH1"" L on C.DocEntry =L.DocEntry " & vbCrLf &
        "where" & vbCrLf &
        "L.InvntSttus='O' " & vbCrLf &
        "and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18) " & vbCrLf &
        "and isnull(L.U_BoxPatent,'')<>'' " & vbCrLf &
        "and CardCode='" & CardCodeProveedor & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const Tiene_almenos_una_entrada_NAME As String = "Tiene_almenos_una_entrada"
    Public Shared Function Tiene_almenos_una_entrada_TEXT(DocEntryReserva As String) As String
        Return _
        "select  top 1 DocEntry from PDN1 where  Basetype=18 and BaseEntry='" & DocEntryReserva & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const GetDocEntryEntrada_NAME As String = "GetDocEntryEntrada"
    Public Shared Function GetDocEntryEntrada_TEXT(Contrato As String) As String
        Return _
        "select DocEntry from ""ODLN"" where U_CntrtId='" & Contrato & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const Lista_de_revaluos_NAME As String = "Lista_de_revaluos"
    Public Shared Function Lista_de_revaluos_TEXT(DocEntryReserva As String) As String
        Return _
        "select DocEntry from OMRV where U_ResEntry='" & DocEntryReserva & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------

    Public Const ListaContratosDuplicadosNoAnulados_NAME As String = "ListaContratosDuplicadosNoAnulados"
    Public Shared Function ListaContratosDuplicadosNoAnulados_TEXT(DocEntryContrato As String) As String
        Return _
        "select Docentry from ""@CNTT"" where isnull(U_EntryBase,'')='" & DocEntryContrato & "' and U_StatusCnt<>'9'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const GetAbsEntry_BoxPatent_NAME As String = "GetAbsEntry_BoxPatent"
    Public Shared Function GetAbsEntry_BoxPatent_TEXT(DocEntry As String) As String
        Return _
        "select SnbAbsEnt,SNBNum from MRV3 where DocEntry='" & DocEntry & "'"

    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const nuevoNCVehiculoUsado_NAME As String = "nuevoNCVehiculoUsado"
    Public Shared Function nuevoNCVehiculoUsado_TEXT(DocEntry As String) As String
        Return _
        "select docentry from ORIN where draftKey='" & DocEntry & "'"
    End Function
    '---------------------------------------------------------------------------------------------------
    Public Const EstadoNCVehiculoUsado_NAME As String = "EstadoNCVehiculoUsado"
    Public Shared Function EstadoNCVehiculoUsado_TEXT(DocEntry As String) As String
        Return _
        "select DocStatus from ODRF where docentry='" & DocEntry & "' and ObjType=14"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
    Public Const getEntradasAsistente_NAME As String = "getEntradasAsistente"
    Public Shared Function getEntradasAsistente_TEXT(CardCodeProveedor As String, DocEntry As String) As String
        Return _
        "select " & vbCrLf &
        "C.DocEntry as ""Entrega"" " & vbCrLf &
        ",C.DocNum as ""Número"" " & vbCrLf &
        ",C.DocCur as ""Moneda Doc."" " & vbCrLf &
        ",L.U_BoxPatent  as ""Cajon/Patente"" " & vbCrLf &
        ",L.ItemCode as ""Modelo"" " & vbCrLf &
        ",L.Dscription as ""Descripción"" " & vbCrLf &
        ",L.Quantity  as ""Cantidad"" " & vbCrLf &
        ",L.Currency as ""Moneda"" " & vbCrLf &
        ",L.Price  as ""Precio"" " & vbCrLf &
        ",L.LineTotal as ""Total"" " & vbCrLf &
        ",C.JrnlMemo as ""Glosa"" " & vbCrLf &
        ",null as ""A"" " & vbCrLf &
        ",null as ""B"" " & vbCrLf &
        "from ""OPDN"" C   " & vbCrLf &
        "inner join ""PDN1"" L on C.DocEntry =L.DocEntry " & vbCrLf &
        "where " & vbCrLf &
        "L.InvntSttus='C'   " & vbCrLf &
        "and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18) " & vbCrLf &
        "and isnull(L.U_BoxPatent,'')<>'' " & vbCrLf &
        "and isnull(U_AsisEntID,'')='" & DocEntry & "' " & vbCrLf &
        "and CardCode='" & CardCodeProveedor & "'"
    End Function
    '-------------------------------------------------------------------------------------------------------------------
End Class

﻿Public Class hanaAjusteTC


    Public Const GetAjustesAnteriores_NAME As String = "GetAjustesAnteriores"
    Public Shared GetAjustesAnteriores_TEXT As String = vbCrLf +
    "select a.TransId,a.RefDate as [Date], a.memo as [Memo],                                                            " + vbCrLf +
    "case when a.stornototr is not null then 'Cancelado' else                                                           " + vbCrLf +
    "case when a.TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)                     " + vbCrLf +
    "then 'Realizado' else                                                                                              " + vbCrLf +
    "       'Cancelado' end                                                                                             " + vbCrLf +
    "end as [Estado]                                                                                                    " + vbCrLf +
    ", (select top 1 b.Name from OFPR b where a.RefDate BETWEEN b.F_RefDate AND b.T_RefDate) as [Periodo] from ojdt a   " + vbCrLf +
    "where isnull(a.U_RefTC,'')<>'' order by TransID desc                                                                    "
    '-----------------------------------------------------------------------------------------------------------------
    Public Const GetdtPeriodoFecha_NAME As String = "GetdtPeriodoFecha"
    Public Shared Function GetdtPeriodoFecha_TEXT(fechas As String) As String
        Return _
     "select top 1 a.Name,T_RefDate from ofpr a where " + fechas + " BETWEEN  a.F_RefDate AND a.T_RefDate "
    End Function
    '-----------------------------------------------------------------------------------------------------------------

    Public Const GetMetodo1_NAME As String = "GetMetodo1"
    Public Shared Function GetMetodo1_TEXT(fechaCierre As String, fechaUltimoCierre As String) As String
        Return _
    "Declare @FECHA_CIERRE as Date " & vbCrLf &
    "set @FECHA_CIERRE='" & fechaCierre & "'  " & vbCrLf &
    "Declare @FECHA_ULTIMO_CIERRE as Date  " & vbCrLf &
    "set @FECHA_ULTIMO_CIERRE='" & fechaUltimoCierre & "' " & vbCrLf &
    "select L.U_BoxPatent,L.ItemCode,  " & vbCrLf &
    "'21010210' as ""Cuenta"", CUENTA.AcctName,  " & vbCrLf &
    "L.WhsCode,  " & vbCrLf &
    "L.Currency,L.Price, " & vbCrLf &
    "C.DocDate, " & vbCrLf &
    "C.DocDate as ""fecha inicial"", " & vbCrLf &
    "@FECHA_CIERRE as ""fecha final"", " & vbCrLf &
    "L.DocEntry,  " & vbCrLf &
    "TC_J.Rate as ""TC Final"", " & vbCrLf &
    "TC_I.Rate as ""TC Inicial"", " & vbCrLf &
    "L.Price*(TC_J.Rate -TC_I.Rate) as ""Ajuste"",  " & vbCrLf &
    "L.OcrCode, L.OcrCode2, L.OcrCode3, L.OcrCode4, L.OcrCode5, 'Y' as ""EsDebito""  " & vbCrLf &
    "from OPCH C  " & vbCrLf &
    "inner join PCH1 L on C.DocEntry =L.DocEntry  " & vbCrLf &
    "inner join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_CIERRE  " & vbCrLf &
    "inner join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=C.DocDate  " & vbCrLf &
    "inner join OACT CUENTA on CUENTA.AcctCode='21010210'  " & vbCrLf &
    "where isnull(U_JrnlPurchase,'')<>''  " & vbCrLf &
    "and L.InvntSttus='O'  " & vbCrLf &
    "and L.Currency<>'CLP'  " & vbCrLf &
    "and C.DocDate<=@FECHA_CIERRE " & vbCrLf &
    "and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18)" & vbCrLf &
    "and TC_J.Rate- TC_I.Rate<>0  " & vbCrLf &
    "   union all " & vbCrLf &
    "select L.U_BoxPatent,L.ItemCode,  " & vbCrLf &
    "'21010210' as ""Cuenta"", CUENTA.AcctName,  " & vbCrLf &
    "L.WhsCode,  " & vbCrLf &
    "L.Currency,L.Price, " & vbCrLf &
    "C.DocDate, " & vbCrLf &
    "C.DocDate as ""fecha inicial"", " & vbCrLf &
    "C_ENT.DocDate as ""fecha final"", " & vbCrLf &
    "L.DocEntry,  " & vbCrLf &
    "TC_J.Rate as ""TC Final"", TC_I.Rate as ""TC Inicial"",  " & vbCrLf &
    "L.Price*(TC_J.Rate -TC_I.Rate)  as ""Ajuste"",  " & vbCrLf &
    "L.OcrCode, L.OcrCode2, L.OcrCode3, L.OcrCode4, L.OcrCode5, 'Y' as ""EsDebito""  " & vbCrLf &
    "from OPCH C  " & vbCrLf &
    "inner join PCH1 L on C.DocEntry =L.DocEntry " & vbCrLf &
    "inner join PDN1 L_ENT on L.U_BoxPatent=L_ENT.U_BoxPatent and L.ItemCode=L_ENT.ItemCode and L.DocEntry=L_ENT.BaseEntry and L_ENT.BaseType='18' " & vbCrLf &
    "inner join OPDN C_ENT on L_ENT.DocEntry=C_ENT.docentry " & vbCrLf &
    "inner join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=C_ENT.DocDate  " & vbCrLf &
    "inner join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=C.DocDate  " & vbCrLf &
    "inner join OACT CUENTA on CUENTA.AcctCode='21010210'  " & vbCrLf &
    "where isnull(C.U_JrnlPurchase,'')<>''  " & vbCrLf &
    "and L.InvntSttus='C'  " & vbCrLf &
    "and L.Currency<>'CLP'  " & vbCrLf &
    "and TC_J.Rate- TC_I.Rate<>0 " & vbCrLf &
    "and C.DocDate<=@FECHA_CIERRE " & vbCrLf &
    "and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18)" & vbCrLf &
    "and C_ENT.DocDate>@FECHA_ULTIMO_CIERRE and C_ENT.DocDate<=@FECHA_CIERRE"

    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const GetMetodo2_NAME As String = "GetMetodo2"
    Public Shared Function GetMetodo2_TEXT(fechaCierre As String, fechaUltimoCierre As String, SumDec As String) As String
        Return _
"Declare @FECHA_CIERRE as Date   " & vbCrLf &
"set @FECHA_CIERRE='" & fechaCierre & "'    " & vbCrLf &
"select L.U_BoxPatent,L.ItemCode,   " & vbCrLf &
"L.AcctCode as ""Cuenta"", CUENTA.AcctName,   " & vbCrLf &
"L.WhsCode,  " & vbCrLf &
"L.Currency, L.Price,  " & vbCrLf &
"C.DocDate,  " & vbCrLf &
"C.DocDate as ""fecha inicial"",  " & vbCrLf &
"@FECHA_CIERRE as ""fecha final"",  " & vbCrLf &
"L.DocEntry, 'F.Reserva' as ""Tipo"", " & vbCrLf &
"TC_J.Rate as ""TC Final"",  " & vbCrLf &
"TC_I2.Rate ""TC Inicial"",  " & vbCrLf &
"L.Price*(TC_J.Rate -TC_I2.Rate) as ""AGlobal"", " & vbCrLf &
"isnull(OJJDT.""AjustePrevio"",0) as ""APrevio"", " & vbCrLf &
"L.Price*(TC_J.Rate -TC_I2.Rate)-isnull(OJJDT.""AjustePrevio"",0) as ""Ajuste"", " & vbCrLf &
"L.OcrCode, L.OcrCode2, L.OcrCode3, L.OcrCode4, L.OcrCode5, 'Y' as ""EsDebito""   " & vbCrLf &
"from OPCH C   " & vbCrLf &
"inner join PCH1 L on C.DocEntry =L.DocEntry   " & vbCrLf &
"inner join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_CIERRE    " & vbCrLf &
"inner join ORTT TC_I2 on TC_I2.Currency=L.Currency  and TC_I2.RateDate=C.DocDate " & vbCrLf &
"LEFT OUTER JOIN ( " & vbCrLf &
"	Select   " & vbCrLf &
"	J1.U_BoxPatent,  " & vbCrLf &
"	J1.Account as ""Cuenta"",  " & vbCrLf &
"        'Asiento' as ""Tipo"",  " & vbCrLf &
"	sum(J1.Debit-J1.Credit) as ""AjustePrevio""   " & vbCrLf &
"	from OJDT OJ1 " & vbCrLf &
"	inner join JDT1 J1  on OJ1.TransId =J1.TransId  " & vbCrLf &
"        where  " & vbCrLf &
"	OJ1.StornoToTr is  null and OJ1.TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)  " & vbCrLf &
"	and isnull(j1.U_BoxPatent,'')<>'' and isnull(U_RefTC,'')<>''  " & vbCrLf &
"	and OJ1.RefDate<=@FECHA_CIERRE " & vbCrLf &
"	group by J1.U_BoxPatent,J1.Account " & vbCrLf &
") OJJDT on L.U_BoxPatent=OJJDT.U_BoxPatent and L.AcctCode=OJJDT.Cuenta " & vbCrLf &
"inner join OACT CUENTA on CUENTA.AcctCode=L.AcctCode  " & vbCrLf &
"where isnull(C.U_JrnlPurchase,'')<>''   " & vbCrLf &
"and L.InvntSttus='O'   " & vbCrLf &
"and L.Currency<>'CLP'   " & vbCrLf &
"and C.DocDate<=@FECHA_CIERRE  " & vbCrLf &
"and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18) " & vbCrLf &
"and isnull(L.U_BoxPatent,'')<>'' " & vbCrLf &
"and L.Price*(TC_J.Rate -TC_I2.Rate)-isnull(OJJDT.""AjustePrevio"",0)<>0   " & vbCrLf &
" union all " & vbCrLf &
"select L.U_BoxPatent,L.ItemCode,   " & vbCrLf &
"L.AcctCode as ""Cuenta"", CUENTA.AcctName,   " & vbCrLf &
"L.WhsCode,  " & vbCrLf &
"L.Currency, L.Price,  " & vbCrLf &
"C.DocDate,  " & vbCrLf &
"C.DocDate as ""fecha inicial"",  " & vbCrLf &
"@FECHA_CIERRE as ""fecha final"",  " & vbCrLf &
"L.BaseEntry, 'N.Crédito' as ""Tipo"", " & vbCrLf &
"1 as ""TC Final"",  " & vbCrLf &
"1 ""TC Inicial"",  " & vbCrLf &
"0 as ""AGlobal"",  " & vbCrLf &
"isnull(OJJDT.""AjustePrevio"",0) as ""APrevio"",  " & vbCrLf &
"-isnull(OJJDT.""AjustePrevio"",0)  as ""Ajuste"",  " & vbCrLf &
"L.OcrCode, L.OcrCode2, L.OcrCode3, L.OcrCode4, L.OcrCode5, 'Y' as ""EsDebito""    " & vbCrLf &
"from ORPC C    " & vbCrLf &
"inner join RPC1 L on C.DocEntry =L.DocEntry     " & vbCrLf &
"LEFT OUTER JOIN ( " & vbCrLf &
"	Select   " & vbCrLf &
"	J1.U_BoxPatent,  " & vbCrLf &
"	J1.Account as ""Cuenta"",  " & vbCrLf &
"        'Asiento' as ""Tipo"",  " & vbCrLf &
"	sum(J1.Debit-J1.Credit) as ""AjustePrevio""   " & vbCrLf &
"	from OJDT OJ1 " & vbCrLf &
"	inner join JDT1 J1  on OJ1.TransId =J1.TransId  " & vbCrLf &
"        where " & vbCrLf &
"	OJ1.StornoToTr is  null and OJ1.TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)  " & vbCrLf &
"	and isnull(j1.U_BoxPatent,'')<>'' and isnull(U_RefTC,'')<>''  " & vbCrLf &
"	and OJ1.RefDate<=@FECHA_CIERRE " & vbCrLf &
"	group by J1.U_BoxPatent,J1.Account " & vbCrLf &
") OJJDT on L.U_BoxPatent=OJJDT.U_BoxPatent and L.AcctCode=OJJDT.Cuenta " & vbCrLf &
"inner join OACT CUENTA on CUENTA.AcctCode=L.AcctCode  " & vbCrLf &
"where isnull(C.U_JrnlPurchase,'')<>''   " & vbCrLf &
"and L.InvntSttus='C'   " & vbCrLf &
"and L.Currency<>'CLP'   " & vbCrLf &
"and C.DocDate<=@FECHA_CIERRE  " & vbCrLf &
"and C.DocEntry  in (select distinct DocEntry from RPC1 where  Basetype=18) " & vbCrLf &
"and isnull(L.U_BoxPatent,'')<>'' " & vbCrLf &
"and -isnull(OJJDT.""AjustePrevio"",0)<>0" & vbCrLf &
"        union all " & vbCrLf &
"select L.U_BoxPatent,L.ItemCode,   " & vbCrLf &
"L.AcctCode as ""Cuenta"", CUENTA.AcctName,   " & vbCrLf &
"L.WhsCode,   " & vbCrLf &
"L.Currency,L.Price,  " & vbCrLf &
"C.DocDate,  " & vbCrLf &
"C.DocDate as ""fecha inicial"",  " & vbCrLf &
"C_ENT.DocDate as ""fecha final"",  " & vbCrLf &
"L.DocEntry, 'Entrada' as ""Tipo"", " & vbCrLf &
"TC_J.Rate as ""TC Final"", TC_I2.Rate as ""TC Inicial"",   " & vbCrLf &
"L.Price*(TC_J.Rate -TC_I2.Rate) as ""AGlobal"",  " & vbCrLf &
"isnull(OJJDT.""AjustePrevio"",0) as ""APrevio"",   " & vbCrLf &
"L.Price*(TC_J.Rate -TC_I2.Rate)-isnull(OJJDT.""AjustePrevio"",0) as ""Ajuste"",  " & vbCrLf &
"L.OcrCode, L.OcrCode2, L.OcrCode3, L.OcrCode4, L.OcrCode5, 'Y' as ""EsDebito""   " & vbCrLf &
"from OPCH C   " & vbCrLf &
"inner join PCH1 L on C.DocEntry =L.DocEntry  " & vbCrLf &
"inner join PDN1 L_ENT on L.U_BoxPatent=L_ENT.U_BoxPatent and L.ItemCode=L_ENT.ItemCode and L.DocEntry=L_ENT.BaseEntry and L_ENT.BaseType='18'  " & vbCrLf &
"inner join OPDN C_ENT on L_ENT.DocEntry=C_ENT.docentry  " & vbCrLf &
"inner join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=C_ENT.DocDate   " & vbCrLf &
"inner join ORTT TC_I2 on TC_I2.Currency=L.Currency  and TC_I2.RateDate=C.DocDate " & vbCrLf &
"LEFT OUTER JOIN ( " & vbCrLf &
"	Select   " & vbCrLf &
"	J1.U_BoxPatent,  " & vbCrLf &
"	J1.Account as ""Cuenta"",  " & vbCrLf &
"        'Asiento' as ""Tipo"",  " & vbCrLf &
"	sum(J1.Debit-J1.Credit) as ""AjustePrevio""   " & vbCrLf &
"	from OJDT OJ1 " & vbCrLf &
"	inner join JDT1 J1  on OJ1.TransId =J1.TransId  " & vbCrLf &
"        where " & vbCrLf &
"	OJ1.StornoToTr is  null and OJ1.TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)  " & vbCrLf &
"	and isnull(j1.U_BoxPatent,'')<>'' and isnull(U_RefTC,'')<>''  " & vbCrLf &
"	and OJ1.RefDate<=@FECHA_CIERRE " & vbCrLf &
"	group by J1.U_BoxPatent,J1.Account " & vbCrLf &
") OJJDT on L.U_BoxPatent=OJJDT.U_BoxPatent and L.AcctCode=OJJDT.Cuenta " & vbCrLf &
"inner join OACT CUENTA on CUENTA.AcctCode=L.AcctCode  " & vbCrLf &
"where isnull(C.U_JrnlPurchase,'')<>''   " & vbCrLf &
"and L.InvntSttus='C'   " & vbCrLf &
"and L.Currency<>'CLP'   " & vbCrLf &
"and C.DocDate<=@FECHA_CIERRE  " & vbCrLf &
"and C.DocEntry not in (select distinct BaseEntry from RPC1 where  Basetype=18) " & vbCrLf &
"and  C_ENT.DocDate<=@FECHA_CIERRE " & vbCrLf &
"and isnull(L.U_BoxPatent,'')<>'' " & vbCrLf &
"and L.Price*(TC_J.Rate -TC_I2.Rate)-isnull(OJJDT.""AjustePrevio"",0)<>0"

    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Dims_NAME As String = "Dims"
    Public Shared Function Dims_TEXT() As String
        Return _
     "select DimDesc from ODIM Order by DimCode"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Valida_tipos_de_cambio_Metodo1_NAME As String = "Valida_tipos_de_cambio_Metodo1"
    Public Shared Function Valida_tipos_de_cambio_Metodo1_TEXT(fecha As String) As String
        Return _
     "Declare @FECHA_FIN as Date " + vbCrLf +
    "set @FECHA_FIN='" & fecha & "' " + vbCrLf +
    "select A.Currency,A.DocDAte from ( " + vbCrLf +
    "	select Distinct L.Currency,C.DocDate, " + vbCrLf +
    "	TC_I.Rate as ""TC Final"" " + vbCrLf +
    "	from OPCH C " + vbCrLf +
    "	left join PCH1 L on C.DocEntry =L.DocEntry " + vbCrLf +
    "	left  join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_FIN " + vbCrLf +
    "	left join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=C.DocDate " + vbCrLf +
    "	where isnull(U_JrnlPurchase,'')<>'' " + vbCrLf +
    "	and L.InvntSttus='O' " + vbCrLf +
    "	and L.Currency<>'" & MonedaLocal & "'" + vbCrLf +
    "		union all " + vbCrLf +
    "	select Distinct L.Currency,@FECHA_FIN, " + vbCrLf +
    "	TC_J.Rate as ""TC Final"" " + vbCrLf +
    "	from OPCH C " + vbCrLf +
    "	left join PCH1 L on C.DocEntry =L.DocEntry " + vbCrLf +
    "	left  join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_FIN " + vbCrLf +
    "	left join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=C.DocDate " + vbCrLf +
    "	where isnull(U_JrnlPurchase,'')<>'' " + vbCrLf +
    "	and L.InvntSttus='O' " + vbCrLf +
    "	and L.Currency<>'" & MonedaLocal & "' " + vbCrLf +
    ") A where isnull(A.""TC Final"",0)=0 order by 1 "

    End Function
    '-----------------------------------------------------------------------------------------------------------------

    Public Const Valida_tipos_de_cambio_Metodo2_NAME As String = "Valida_tipos_de_cambio_Metodo2"
    Public Shared Function Valida_tipos_de_cambio_Metodo2_TEXT(fecha As String, FechaInicio As String) As String
        Return _
    "Declare @FECHA_FIN as Date  " + vbCrLf +
    "set @FECHA_FIN='" & fecha & "'  " + vbCrLf +
    "Declare @FECHA_INICIO as Date  " + vbCrLf +
    "--set @FECHA_INICIO=cast(YEAR(@FECHA_FIN) as varchar(4)) + RIGHT('0'+cast(MONTH(@FECHA_FIN) as varchar(2)),2) +'01'  " + vbCrLf +
    "--set @FECHA_INICIO=DATEADD(DAY,-1,@FECHA_INICIO)  " + vbCrLf +
    "set @FECHA_INICIO='" & FechaInicio & "'  " + vbCrLf +
    "select A.Currency,A.DocDate from ( " + vbCrLf +
    "	select distinct L.Currency,@FECHA_FIN as ""DocDate"",  " + vbCrLf +
    "	TC_J.Rate as ""TC Final"", TC_I.Rate as ""TC Inicial""  " + vbCrLf +
    "	from OPCH C  " + vbCrLf +
    "	left join PCH1 L on C.DocEntry =L.DocEntry  " + vbCrLf +
    "	left join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_FIN  " + vbCrLf +
    "	left join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=@FECHA_INICIO  " + vbCrLf +
    "	where isnull(U_JrnlPurchase,'')<>''  " + vbCrLf +
    "	and L.InvntSttus='O'  " + vbCrLf +
    "	and L.Currency<>'" & MonedaLocal & "'  " + vbCrLf +
    "		union all " + vbCrLf +
    "	select distinct L.Currency,@FECHA_INICIO as ""DocDate"" ,  " + vbCrLf +
    "	TC_I.Rate as ""TC Final"", TC_I.Rate as ""TC Inicial""  " + vbCrLf +
    "	from OPCH C  " + vbCrLf +
    "	left join PCH1 L on C.DocEntry =L.DocEntry  " + vbCrLf +
    "	left join ORTT TC_J on TC_J.Currency=L.Currency  and TC_J.RateDate=@FECHA_FIN  " + vbCrLf +
    "	left join ORTT TC_I on TC_I.Currency=L.Currency  and TC_I.RateDate=@FECHA_INICIO  " + vbCrLf +
    "	where isnull(U_JrnlPurchase,'')<>''  " + vbCrLf +
    "	and L.InvntSttus='O'  " + vbCrLf +
    "	and L.Currency<>'" & MonedaLocal & "' and @FECHA_INICIO<>'' " + vbCrLf +
    ") A where isnull(A.""TC Final"",0)=0 order by 1"


    End Function
    '-----------------------------------------------------------------------------------------------------------------

    Public Const FechaInicio_Metodo2_NAME As String = "FechaInicio_Metodo2"
    Public Shared Function FechaInicio_Metodo2_TEXT(fecha As String) As String
        Return _
    "Select RefDate from OJDT where transId=( " & vbCrLf &
    "select isnull(max(TransId),0)  from OJDT " + vbCrLf +
    "where TransId in (select distinct  U_JrnlId from ""@EXX_TRAZATC"" where U_Periodo<'" & fecha & "') and " + vbCrLf +
    "StornoToTr is  null and TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL) " + vbCrLf +
    ")"

    End Function
    '-----------------------------------------------------------------------------------------------------------------

    Public Const Valida_existen_datos_NAME As String = "Valida_existen_datos"
    Public Shared Function Valida_existen_datos_TEXT(fecha As String) As String
        Return _
    "select isnull(max(TransId),0) from OJDT " + vbCrLf +
    "where TransId in (select distinct  U_JrnlId from ""@EXX_TRAZATC"" where U_Periodo='" & fecha & "') and  " + vbCrLf +
    "StornoToTr is  null and TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Valida_existen_datos2_NAME As String = "Valida_existen_datos2"
    Public Shared Function Valida_existen_datos2_TEXT(fecha As String) As String
        Return _
    "select isnull(max(TransId),0) from OJDT " + vbCrLf +
    "where TransId in (select distinct  U_JrnlId from ""@EXX_TRAZATC"" where U_Periodo>'" & fecha & "') and  " + vbCrLf +
    "StornoToTr is  null and TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL)"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Valida_existen_datos3_NAME As String = "Valida_existen_datos3"
    Public Shared Function Valida_existen_datos3_TEXT(fecha As String) As String
        Return _
    "select isnull(max(TransId),0) from OJDT " + vbCrLf +
    "where TransId in (select distinct  U_JrnlId from ""@EXX_TRAZATC"" where U_Periodo='" & fecha & "')   "
    End Function
    '-----------------------------------------------------------------------------------------------------------------

    Public Const Valida_correlativo_NAME As String = "Valida_correlativo"
    Public Shared Function Valida_correlativo_TEXT(fecha As String, PeriodoInicial As String) As String
        Return _
    "Select A.Periodo, A.Code, A.Desde, A.Hasta,B.U_RefTC  from  " + vbCrLf +
    "( " + vbCrLf +
    "select Code,F_RefDate as ""Desde"", T_RefDate as ""Hasta"",  " + vbCrLf +
    "cast(YEAR(T_RefDate) as nvarchar(4)) + Right('00'+cast( Month(T_RefDate)  as nvarchar(2)),2) " + vbCrLf +
    "as ""Periodo"" from  OFPR  " + vbCrLf +
    "where F_RefDate>'" & PeriodoInicial & "01" & "'  and T_RefDate<'" & fecha & "'  " + vbCrLf +
    ") A  " + vbCrLf +
    "left join OJDT B on A.Periodo=B.U_RefTC " + vbCrLf +
    "where LTRIM(RTRIM(isnull(B.U_RefTC,'')))='' " + vbCrLf +
    "order by 1"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const InsertCamposTRAZATC_NAME As String = "InsertCamposTRAZATC"
    Public Shared Function InsertCamposTRAZATC_TEXT(U_UserName As String, U_AccountDate As String, U_Periodo As String, _
                                                    U_BoxPatent As String, U_ItemCode As String, U_Currency As String, _
                                                    U_Price As Decimal, U_DocDate As String, U_DocEntry As String, _
                                                    U_TC_Final As Decimal, U_TC_Inicial As Decimal, U_Ajuste As Decimal, _
                                                    U_OcrCode As String, U_OcrCode2 As String, U_OcrCode3 As String, _
                                                     U_OcrCode4 As String, U_OcrCode5 As String, U_JrnlId As String, _
                                                     U_CreateDate As String, fecha_inicial As String, fecha_final As String, _
                                                     U_AGlobal As Decimal, U_APrevio As Decimal, U_Tipo As String, U_Cuenta As String) As String
        Dim sSqlInsert As String = ""

        sSqlInsert = "INSERT INTO ""@EXX_TRAZATC""  " + vbCrLf +
                    "( " + vbCrLf +
                    "Code, " + vbCrLf +
                    "Name, " + vbCrLf +
                    "U_UserName, " + vbCrLf +
                    "U_CreateDate, " + vbCrLf +
                    "U_AccountDate, " + vbCrLf +
                    "U_Periodo, " + vbCrLf +
                    "U_BoxPatent, " + vbCrLf +
                    "U_ItemCode, " + vbCrLf +
                    "U_Currency, " + vbCrLf +
                    "U_Price, " + vbCrLf +
                    "U_DocDate, " + vbCrLf +
                    "U_DocEntry, " + vbCrLf +
                    "U_TC_Final, " + vbCrLf +
                    "U_TC_Inicial, " + vbCrLf +
                    "U_Ajuste, " + vbCrLf +
                    "U_OcrCode, " + vbCrLf +
                    "U_OcrCode2, " + vbCrLf +
                    "U_OcrCode3, " + vbCrLf +
                    "U_OcrCode4, " + vbCrLf +
                    "U_OcrCode5, " + vbCrLf +
                    "U_fecha_inicial, " + vbCrLf +
                    "U_fecha_final, " + vbCrLf +
                    "U_AGlobal, " + vbCrLf +
                    "U_APrevio, " + vbCrLf +
                    "U_Tipo, " + vbCrLf +
                    "U_JrnlId" + vbCrLf +
                    " ) " + vbCrLf +
                    " SELECT right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8) --> Code " + vbCrLf +
                    "  ,right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8), --> Name " + vbCrLf
        sSqlInsert = sSqlInsert & vbCrLf &
                    "'" & U_UserName & "', " &
                    U_CreateDate & ", '" &
                    U_AccountDate & "', '" &
                    U_Periodo & "', '" &
                    U_BoxPatent & "', '" &
                    U_ItemCode & "', '" &
                    U_Currency & "', " &
                    CStr(U_Price).Replace(",", ".") & ", " &
                    U_DocDate & ", '" &
                    U_DocEntry & "', " &
                    CStr(U_TC_Final).Replace(",", ".") & ", " &
                    CStr(U_TC_Inicial).Replace(",", ".") & ", " &
                    CStr(U_Ajuste).Replace(",", ".") & ", '" &
                    U_OcrCode & "', '" &
                    U_OcrCode2 & "', '" &
                    U_OcrCode3 & "', '" &
                    U_OcrCode4 & "', '" &
                    U_OcrCode5 & "', " &
                    fecha_inicial & ", " &
                    fecha_final & ", '" &
                    CStr(U_AGlobal).Replace(",", ".") & ", '" &
                    CStr(U_APrevio).Replace(",", ".") & ", '" &
                    U_Tipo & ", '" &
                    U_JrnlId & "' " &
                    " from ""@EXX_TRAZATC"" "
        Return sSqlInsert
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const UltimoCierre_NAME As String = "UltimoCierre"
    Public Shared Function UltimoCierre_TEXT(UltimoTransID As String) As String
        Return _
    "select max(transID) from OJDT where isnull(U_RefTC,'')<>'' and " & vbCrLf &
    "StornoToTr is  null and TransId NOT IN (SELECT T0.StornoToTr FROM OJDT T0 where t0.stornototr is not NULL) " & vbCrLf &
    "and transID <>'" & UltimoTransID & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const FechaJrnlAnterior_NAME As String = "FechaJrnlAnterior"
    Public Shared Function FechaJrnlAnterior_TEXT(transId As String) As String
        Return _
    "select RefDate from  OJDT where TransId='" & transId & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Get_tipo_de_cuenta_item_NAME As String = "Get_tipo_de_cuenta_item"
    Public Shared Function Get_tipo_de_cuenta_item_TEXT(itemCode As String) As String
        Return _
    "Select GLMethod from OITM where ItemCode='" & itemCode & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Get_Cuenta_Item_de_almacen_NAME As String = "Get_Cuenta_Item_de_almacen"
    Public Shared Function Get_Cuenta_Item_de_almacen_TEXT(WhsCode As String) As String
        Return _
    "SELECT StkInTnAct  FROM OWHS T0 where WhsCode='" & WhsCode & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Get_Cuenta_Item_de_Grupo_NAME As String = "Get_Cuenta_Item_de_Grupo"
    Public Shared Function Get_Cuenta_Item_de_Grupo_TEXT(ItmsGrpCod As String) As String
        Return _
    "SELECT StkInTnAct  FRO OITB T0 where ItmsGrpCod='" & ItmsGrpCod & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Get_Cuenta_Item_de_Item_NAME As String = "Get_Cuenta_Item_de_Item"
    Public Shared Function Get_Cuenta_Item_de_Item_TEXT(ItemCode As String, WhsCode As String) As String
        Return _
    "Select StkInTnAct from OITW where ItemCode='" & ItemCode & "' and WhsCode='" & WhsCode & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------
    Public Const Get_grupo_de_item_NAME As String = "Get_grupo_de_item"
    Public Shared Function Get_grupo_de_item_TEXT(itemCode As String) As String
        Return _
    "Select ItmsGrpCod from OITM where ItemCode='" & itemCode & "'"
    End Function
    '-----------------------------------------------------------------------------------------------------------------

End Class

﻿Imports SAPbobsCOM

Namespace Data
    Public Class clsSAP

#Region "Variables locales"
        Private SBO As Company
#End Region

#Region "Conexion"

        Public Function Conectar(Optional ByRef mensaje As String = "") As Boolean

            Try

                SBO = New Company
                REM Variables del módulo SQL
                SBO.Server = SQLServidor
                SBO.CompanyDB = SQLBaseDatos

                'SAPUsuario = "manager"
                'SAPPassword = "1234"

                If Motor = "SQL" Then
                    SBO.DbUserName = SQLUsuario
                    SBO.DbPassword = SQLPassword
                Else
                    SBO.DbUserName = "SYSTEM"
                    SBO.DbPassword = "Passw0rd"
                    SBO.LicenseServer = "hanab1:40000"
                End If
                REM locales de conexión a SAP BO
                SBO.UserName = SAPUsuario
                SBO.Password = SAPPassword


                Select Case BICLASS.RescataVersionSQL()

                    Case 2005 : SBO.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005
                    Case 2008 : SBO.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008
                    Case 2012 : SBO.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012
                    Case 900 : SBO.DbServerType = 9
                    Case Else : SBO.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008

                End Select
                SBO.UseTrusted = False

                ErrorMsg = String.Empty
                If (SBO.Connect = 0) Then
                    Return True
                Else
                    SBO.GetLastError(errorNbr, ErrorMsg)
                    Return False
                End If

            Catch ex As Exception
                mensaje = ex.Message
                Return False
            End Try

        End Function



        Public Function Reconectar(Optional ByRef mensaje As String = "") As Boolean

            Try
                SBO.Disconnect()
                SBO = Nothing
            Catch ex As Exception
                SBO = Nothing
            End Try

            Try
                Return Conectar()
            Catch ex As Exception
                Return False
            End Try

        End Function

        Public ReadOnly Property Compania As SAPbobsCOM.Company
            Get
                Return SBO
            End Get
        End Property

#End Region

    End Class
End Namespace

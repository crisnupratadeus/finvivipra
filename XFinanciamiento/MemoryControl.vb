﻿Public Class MemoryControl

    Public Sub New()

    End Sub

    Public Sub releaseObject(ByRef comObject As Object)
        Try
            If Not comObject Is Nothing Then
                System.Runtime.InteropServices.Marshal.ReleaseComObject(comObject)
                comObject = Nothing
            End If

        Catch ex As Exception
            comObject = Nothing
        End Try

    End Sub

    Public Sub callGC()
        GC.Collect()
    End Sub

    Public Sub protectFromGC(ByRef comObject As Object)
        GC.KeepAlive(comObject)
    End Sub

End Class

﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View

    Public Class FormSNLiCre

#Region "METODOS"


        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, SNCode As String, SNName As String)
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "FormSNLiCre")

                If blnexit Then
                    oForm = oApp.Forms.Item("FormSNLiCre")
                    oForm.Close()
                    Exit Sub
                End If

                strPath = Application.StartupPath & "\FormSNLiCre.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)

                oForm = oApp.Forms.Item("FormSNLiCre")


                oForm.DataSources.UserDataSources.Item("udSNLiCre").ValueEx = SNCode
                oForm.DataSources.UserDataSources.Item("udSNLiCreN").ValueEx = SNName

                If Not PopulaGridLiCre(oApp, oForm) Then
                    oForm.Freeze(False)
                    oForm.Close()
                    Exit Sub
                End If

                oForm.Visible = True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Public Shared Function PopulaGridLiCre(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim queryGrid As String = String.Empty
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oDT As SAPbouiCOM.DataTable
            Dim oGrid As SAPbouiCOM.Grid
            Dim numLin As Integer
            Dim SNCode As String = String.Empty
            Dim SNName As String = String.Empty

            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim oColActivo As SAPbouiCOM.EditTextColumn

            Try
                oForm.Freeze(True)

                oGrid = oForm.Items.Item("grdSNLiCre").Specific

                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dtSNLiCre")

                Try
                    SNCode = oForm.DataSources.UserDataSources.Item("udSNLiCre").ValueEx
                    If Trim(SNCode) = "" Then
                        oForm.Freeze(False)
                        oApp.MessageBox("Debe existir un socio de negocio cargado en el formulario.")
                        Return False
                    End If
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try

                Try
                    SNName = oForm.DataSources.UserDataSources.Item("udSNLiCreN").ValueEx
                    If Trim(SNName) = "" Then
                        oForm.Freeze(False)
                        oApp.MessageBox("Debe existir un socio de negocio cargado en el formulario.")
                        Return False
                    End If
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try

                Try
                    queryGrid = BDLineaCredito.GetSNLineasCredito(SNCode)
                Catch ex As Exception
                    queryGrid = ""
                End Try

                oDT.ExecuteQuery(queryGrid)
                oGrid.DataTable = oDT

                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        oGrid.RowHeaders.SetText(i, numLin)
                    Next
                End If

                oColumns = oGrid.Columns.Item("Codigo")
                'oColumns = oGrid.Columns.Item("DocEntry")
                oColumns.LinkedObjectType = "LICRE"
                oColumns.Editable = False
                oGrid.RowHeaders.TitleObject.Caption = "#"
                oColActivo = oGrid.Columns.Item("Estado")
                oColActivo.Editable = False
                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        If oColActivo.GetText(i) = "001" Then
                            oColActivo.SetText(i, "Pendiente")
                        End If
                        If oColActivo.GetText(i) = "002" Then
                            oColActivo.SetText(i, "Aprobado")
                        End If
                    Next
                End If
                oGrid.Columns.Item("Fecha_Hasta").Editable = False
                oGrid.Columns.Item("Fecha_Hasta").Visible = True

                'oGrid.Columns.Item("ACTIVO").Editable = True
                'oGrid.Columns.Item("ACTIVO").Visible = True
                'oGrid.Columns.Item("ACTIVO").Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox

                oGrid.AutoResizeColumns()

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Public Shared Function GuardaProyUsrGrd(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim CodePrj As String = String.Empty
            Dim NamePrj As String = String.Empty
            Dim Activo As String = String.Empty
            Dim CodeTbl As String = String.Empty
            Dim NameTbl As String = String.Empty
            Dim usrCode As String = String.Empty
            Dim usrName As String = String.Empty
            Dim grdSNLiCre As SAPbouiCOM.Grid
            Dim oDTSAP As SAPbouiCOM.DataTable

            Try

                grdSNLiCre = oForm.Items.Item("grdSNLiCre").Specific
                oDTSAP = grdSNLiCre.DataTable
                usrCode = Trim(oForm.DataSources.UserDataSources.Item("udSNLiCre").ValueEx)
                usrName = Trim(oForm.DataSources.UserDataSources.Item("udSNLiCreN").ValueEx)

                If oDTSAP.Rows.Count > 0 Then
                    For i As Integer = 0 To oDTSAP.Rows.Count - 1
                        CodePrj = oDTSAP.GetValue("CODE", i)
                        NamePrj = oDTSAP.GetValue("NAME", i)
                        Activo = oDTSAP.GetValue("ACTIVO", i)
                        CodeTbl = oDTSAP.GetValue("CODETBL", i)
                        NameTbl = oDTSAP.GetValue("NAMETBL", i)

                        If Trim(Activo) = "" Then
                            oForm.Freeze(False)
                            oApp.MessageBox("No se tiene un valor para la activación del proyecto:  " & Trim(NamePrj))
                            Continue For
                        End If

                        If Trim(CodeTbl) = "" Then
                            If Not DatosAutorizaciones.InsertaUsrProy(Trim(usrCode), Trim(usrName), Trim(Activo), Trim(CodePrj), Trim(NamePrj)) Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al ingresar el registro para el proyecto:  " & Trim(NamePrj))
                                Continue For
                            End If
                        Else
                            If Not DatosAutorizaciones.UpdateUsrProy(Trim(CodeTbl), Trim(NameTbl), Trim(Activo)) Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al actualizar el registro para el proyecto:  " & Trim(NamePrj))
                                Continue For
                            End If
                        End If
                    Next

                Else


                End If



                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
#End Region

#Region "EVENTOS"
        Public Shared Sub AdminFormUsrProy(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try

                oForm = oApp.Forms.Item(pVal.FormUID)

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "btnGrabar"
                                    If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            Try
                                                If Not GuardaProyUsrGrd(oApp, oForm) Then
                                                    oForm.Freeze(False)
                                                    oApp.MessageBox("Error al grabar/actualizar registros")
                                                    BubbleEvent = False
                                                    Try
                                                        oForm.Freeze(True)
                                                        PopulaGridLiCre(oApp, oForm)
                                                        oForm.Freeze(False)
                                                    Catch ex As Exception
                                                        oForm.Freeze(False)
                                                        oApp.MessageBox(ex.Message)
                                                        BubbleEvent = False
                                                        GoTo fail
                                                    End Try
                                                    GoTo fail
                                                Else
                                                    oForm.Freeze(False)
                                                    oApp.MessageBox("Exito al grabar/actualizar registros")
                                                    BubbleEvent = True
                                                    Try
                                                        oForm.Freeze(True)
                                                        PopulaGridLiCre(oApp, oForm)
                                                        oForm.Freeze(False)
                                                    Catch ex As Exception
                                                        oForm.Freeze(False)
                                                        oApp.MessageBox(ex.Message)
                                                        BubbleEvent = False
                                                        GoTo fail
                                                    End Try
                                                    GoTo fail
                                                End If

                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                oApp.MessageBox(ex.Message)
                                                BubbleEvent = False
                                                GoTo fail
                                            End Try

                                        Else
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If

                                    Else
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If



                            End Select

                        Else


                        End If

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

#End Region

    End Class

End Namespace



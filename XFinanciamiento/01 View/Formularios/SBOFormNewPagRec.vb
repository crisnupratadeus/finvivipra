﻿Imports XFinanciamiento.Principal
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data
Imports System.Globalization

Namespace View
    Public Class SBOFormNewPagRec



        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                Dim MenuID As String = "2817"
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)
                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
            LoadDatos(oApp, oForm)
        End Sub
        Public Shared Sub CreaSBOForm2(ByVal oApp As SAPbouiCOM.Application, ByRef socio As String, ByRef udo As String)
            Try
                Dim MenuID As String = "2817"
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)
                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
            LoadDatos2(oApp, oForm, udo, socio)
        End Sub



        Private Shared Function LoadDatos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Try
                Dim dt As DataSet

                Dim one As DataTable

                Dim CarrCode As String
                Dim carcode As SAPbouiCOM.EditText
                dt = SBOFormCalculoInteres.PruebaOneDataTable()
                one = dt.Tables("Table1")

                CarrCode = one.Rows(0).Item(0)

                oForm = oApp.Forms.ActiveForm

                carcode = oForm.Items.Item("5").Specific
                carcode.Value = CarrCode

                checkGrilla(oApp, oForm, dt)
            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Cargar Datos en SBO FORM Error:" & ex.Message)
            End Try
        End Function

        Private Shared Function LoadDatos2(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef udo As String, ByRef socio As String) As Boolean
            Try
                Dim dt As DataSet

                Dim one As DataTable

                Dim CarrCode As String
                Dim carcode As SAPbouiCOM.EditText
                dt = SBOFormCalculoInteres.PruebaOneDatatable2(socio, udo)
                'one = dt.Tables("Table1")

                'CarrCode = one.Rows(0).Item(0)

                oForm = oApp.Forms.ActiveForm

                carcode = oForm.Items.Item("5").Specific
                carcode.Value = socio

                checkGrilla(oApp, oForm, dt)
            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Cargar Datos en SBO FORM Error:" & ex.Message)
            End Try
        End Function

        Private Shared Function checkGrilla(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef dt As DataSet) As Boolean
            Try
                'Dim two As DataTable
                Dim three As DataTable
                Dim test As String
                Dim test2 As String
                Dim test3 As String
                Dim mtr As SAPbouiCOM.Matrix
                Dim cel As SAPbouiCOM.EditText
                Dim cel2 As SAPbouiCOM.EditText
                Dim cel3 As SAPbouiCOM.EditText
                Dim oChkCol As SAPbouiCOM.CheckBox
                Dim position As Integer
                'Dim CodigpUdo As SAPbouiCOM.EditText
                ' Dim codudo As String
                position = 0
                mtr = oForm.Items.Item("20").Specific
                oForm = oApp.Forms.ActiveForm

                'two = SBOFormCalculoInteres.PruebaTwoDataTable()
                three = dt.Tables("Table2")

                'codudo = two.Rows(0).Item(2)
                'CodigpUdo = oForm.Items.Item("CodUd").Specific
                'CodigpUdo.Value = codudo

                For i As Integer = 0 To three.Rows.Count - 1
                    test = three.Rows(i).Item(0)
                    test2 = three.Rows(i).Item(2)
                    test3 = three.Rows(i).Item(1)
                    For j As Integer = 1 To mtr.RowCount

                        position = position + 1

                        cel = mtr.Columns.Item("1").Cells.Item(position).Specific
                        cel2 = mtr.Columns.Item("24").Cells.Item(position).Specific
                        cel3 = mtr.Columns.Item("71").Cells.Item(position).Specific
                        Dim muestra As String = cel.Value.ToString
                        Dim muestra2 As String = cel2.Value.ToString
                        Dim muestra3 As String = cel3.Value.ToString
                        muestra2 = muestra2.Substring(muestra2.LastIndexOf(" ") + 1)
                        oChkCol = mtr.Columns.Item("10000127").Cells.Item(position).Specific

                        If muestra = test Then
                            If muestra3 = test3 Then

                                oApp.MessageBox("Confirmar Factura:" & muestra)

                                    oChkCol.Checked() = True


                                cel2.Value = test2
                                'oChkCol.Checked() = True

                            Else
                                ' oApp.MessageBox("las cuotas no coinciden para esta factura")
                            End If
                        End If
                    Next

                    position = 0
                Next

            Catch ex As Exception
                oForm.Freeze(False)

                oApp.MessageBox("Check de Datos en SBO FORM Error:" & ex.Message)

            End Try
        End Function






    End Class


End Namespace
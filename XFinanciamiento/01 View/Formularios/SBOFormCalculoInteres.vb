﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization
Imports System.Reflection

Namespace View
    Public Class SBOFormCalculoInteres

        Private _length As Double
        Private Shared _factDataset As DataSet
        'Public Shared FactDataset As New DataSet
        Public Shared FactDataset2 As New DataSet
        Public Shared origenDataSet As New DataSet
        Public Shared NuevasDataTable As New System.Data.DataTable
        Public Shared FacturasConInteras As New System.Data.DataTable
        Public Shared NuevasTable As New DataTable
        Public Shared OneDataTable As New DataTable
        Public Shared TwoDataTable As New DataTable
        Public Shared ThreeDataTable As New DataTable
        Public Shared OneTable As New DataTable
        Public Shared TwoTable As New DataTable
        Public Shared ThreeTable As New DataTable
        Public Shared DocEntryUdo As String
        Public Shared TotalCuotasCre As Decimal = 0
        Public Shared oGridF As SAPbouiCOM.Grid
        Public Shared oDTFac As SAPbouiCOM.DataTable
        Public Shared oDTsFac As SAPbouiCOM.DataTables
        Public Shared oDTFacAux As SAPbouiCOM.DataTable
        Public Shared oDTsFacAux As SAPbouiCOM.DataTables
        Public Shared FechaActual As Date
        Public Shared ProcesaPagos As String

        Public Property Length As Double
            Get
                Return _length
            End Get
            Set(value As Double)
                _length = value
            End Set
        End Property

        Public Shared FactDataset As New DataSet
        '    Get
        '        Return _factDataset
        '    End Get
        '    Set(value As DataSet)
        '        _factDataset = value
        '    End Set
        'End Property


#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, code As String, dataTable As DataTable, totalCuotas As Decimal)
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "FormPaInt")
                origenDataSet.Tables.Clear()
                origenDataSet.Tables.Add(dataTable)
                'TotalCuotasCre = totalCuotas
                If blnexit Then
                    Exit Sub
                End If
                strPath = Application.StartupPath & "\SBOFormPagoIntereses.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)
                oForm = oApp.Forms.Item("FormPaInt")
                FechaActual = oApp.Company.ServerDate
                Dim FechaActualString As String = FechaActual.Year.ToString() & Right(("0" & FechaActual.Month.ToString()), 2) & Right(("0" & FechaActual.Day.ToString()), 2)
                oForm.DataSources.UserDataSources.Item("UD_FVal").Value = FechaActualString
                ProcesaPagos = "N"
                'Dim 
                'Binding de los datos 
                TotalCuotasCre = dataTable.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("PagoTotLinea"))
                LoadDataCuotas(oApp)
                PopulaGrid(oApp, oForm)
                
                oForm.Visible = True
            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub



        Private Shared Sub LoadDataCuotas(ByRef oApp As SAPbouiCOM.Application)
            Try
                oForm.Freeze(True)

                Dim dataTable As DataTable = origenDataSet.Tables("Table1")

                oGridF = oForm.Items.Item("DbGrdPa").Specific

                'Se setean los objetos 

                oDTsFac = oForm.DataSources.DataTables
                oDTFac = oDTsFac.Item("DT_Fac")
                oDTFac.Clear()

                'Se setean los objetos 
                oDTsFacAux = oForm.DataSources.DataTables
                oDTFacAux = oDTsFacAux.Item("DT_Aux")
                oDTFacAux.Clear()

                oDTFac.Columns.Add("DocEntry", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDTFac.Columns.Add("Factura", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDTFac.Columns.Add("NumFact", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDTFac.Columns.Add("PagoTotLinea", SAPbouiCOM.BoFieldsType.ft_Text, 300)

                Dim fi As Integer = 0

                oDTFac.Rows.Add(dataTable.Rows.Count)

                For Each row As DataRow In dataTable.Rows
                    oDTFac.SetValue("DocEntry", fi, row("DocEntry"))
                    oDTFac.SetValue("Factura", fi, row("DocNum"))
                    oDTFac.SetValue("NumFact", fi, row("NumFact"))
                    oDTFac.SetValue("PagoTotLinea", fi, row("PagoTotLinea").ToString())
                    fi = fi + 1
                Next

                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try

        End Sub
        Public Shared Sub ActualizaGridCuotas(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Dim montoDisponible As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_Dispo").Value.ToString(CultureInfo.InvariantCulture))
            Dim totalIntereses As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_TotCol").Value.ToString(CultureInfo.InvariantCulture))
            Dim totalCuotas As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_APagar").Value.ToString(CultureInfo.InvariantCulture))
            Dim totalGeneral As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_ToGn").Value.ToString(CultureInfo.InvariantCulture))

            Dim dataTable As DataTable = origenDataSet.Tables("Table1")

            Dim MontoAuxiliar As Decimal = totalIntereses

            If montoDisponible < totalIntereses Then
                oApp.MessageBox("El monto disponible es menor al interés, recalcular con fecha valor.")

                For Each row As DataRow In dataTable.Rows
                    row("PagoTotLinea") = 0
                Next
            Else
                'Ajustar la diferencia hasta que las cuotas llegen al monto disponible menos el totalInterés
                Dim MontoRestante As Decimal = montoDisponible - totalIntereses

                For Each row As DataRow In dataTable.Rows
                    Dim montoLinea As Decimal = row("PagoTotLinea")
                    If MontoRestante > montoLinea Then
                        row("PagoTotLinea") = montoLinea
                    Else
                        row("PagoTotLinea") = MontoRestante
                    End If
                    MontoRestante = MontoRestante - montoLinea
                    If MontoRestante < 0 Then
                        MontoRestante = 0
                    End If
                Next
            End If
            TotalCuotasCre = dataTable.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("PagoTotLinea"))
            LoadDataCuotas(oApp)
            PopulaGrid(oApp, oForm)
        End Sub

        Public Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
            Dim table As New DataTable()
            Dim fields() As FieldInfo = GetType(T).GetFields()
            For Each field As FieldInfo In fields
                table.Columns.Add(field.Name, field.FieldType)
            Next
            For Each item As T In list
                Dim row As DataRow = table.NewRow()
                For Each field As FieldInfo In fields
                    row(field.Name) = field.GetValue(item)
                Next
                table.Rows.Add(row)
            Next
            Return table
        End Function

        Public Shared Sub PopulaGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Try

                Dim fechaValor As Date = oForm.DataSources.UserDataSources.Item("UD_FVal").Value
                'Dim fechaValString As String = FechaValor.Year.ToString() & Right(("0" & FechaValor.Month.ToString()), 2) & Right(("0" & FechaValor.Day.ToString()), 2)
                Dim DtFacturaPago As DataTable = origenDataSet.Tables("Table1")
                'chkDisp

                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim sCmp As SAPbobsCOM.CompanyService

                Dim oSalesTaxCode As SAPbobsCOM.Documents

                Dim imp As Double = 0
                oForm.Freeze(True)


                Dim oGrid As SAPbouiCOM.Grid
                Dim oDT As SAPbouiCOM.DataTable
                Dim oDTs As SAPbouiCOM.DataTables
                Dim oColumn As SAPbouiCOM.GridColumn
                Dim TotalIntereses As Double = 0
                'Se setean los objetos 
                oGrid = oForm.Items.Item("DbGrdInt").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("DT_PagInt")
                oDT.Clear()
                oDT.Columns.Add("DocEntry", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("Factura", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("Capital", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("FechaUltimoPago", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("Dias", SAPbouiCOM.BoFieldsType.ft_Text, 50)
                oDT.Columns.Add("InteresAplicable", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("Interes", SAPbouiCOM.BoFieldsType.ft_Text, 50)
                oDT.Columns.Add("Confirmar", SAPbouiCOM.BoFieldsType.ft_Text, 1)

                Dim result = (From facturas In DtFacturaPago.Select("U_Interes <> '0,000000'").AsEnumerable
                              Group facturas By DocEntry = facturas.Field(Of String)("DocEntry"), U_OrdenVenta = facturas.Field(Of String)("U_OrdenVenta"), DocNum = facturas.Field(Of String)("DocNum"), CardCode = facturas.Field(Of String)("CardCode"), U_DocCuota = facturas.Field(Of String)("U_DocCuota"), DocTotal = facturas.Field(Of String)("DocTotal"), PaidToDate = facturas.Field(Of String)("PaidToDate"), U_CuotFi = facturas.Field(Of String)("U_CuotFi"), U_Interes = facturas.Field(Of String)("U_Interes"), U_IntMor = facturas.Field(Of String)("U_IntMor"), DocDate = facturas.Field(Of String)("DocDate"), TaxDate = facturas.Field(Of String)("TaxDate"), DocDueDate = facturas.Field(Of String)("DocDueDate") Into g = Group
                              Select New With {
                                                Key DocEntry,
                                                U_OrdenVenta,
                                                DocNum,
                                                CardCode,
                                                U_DocCuota,
                                                DocTotal,
                                                PaidToDate,
                                                U_CuotFi,
                                                U_Interes,
                                                U_IntMor,
                                                DocDate,
                                                TaxDate,
                                                DocDueDate,
                                                .TotalFactura = g.Sum(Function(r) r.Field(Of Decimal)("PagoTotLinea"))
                            }).OrderBy(Function(tkey) tkey.DocEntry).ToList()

                Dim i As Integer = 0

                oDT.Rows.Add(result.Count)

                For Each row As Object In result

                    Dim pDocEntry As String = "" 'DtFacturaPago.Rows(0).Item("DocEntry").ToString()
                    Dim pDocNum As String = "" ' DtFacturaPago.Rows(0).Item("DocNum").ToString()
                    Dim pCardCode As String = "" 'DtFacturaPago.Rows(0).Item("CardCode").ToString()
                    Dim pU_DocCuota As String = "" ' DtFacturaPago.Rows(0).Item("U_DocCuota").ToString()
                    Dim pDocTotal As Decimal = 0 'DatosDB.ParseDecimalFormat(DtFacturaPago.Rows(0).Item("DocTotal").ToString())
                    Dim pU_CuotFi As String = "1" 'DtFacturaPago.Rows(0).Item("U_CuotFi").ToString()
                    Dim pU_Interes As Decimal = 0 '  DatosDB.ParseDecimalFormat(DtFacturaPago.Rows(0).Item("U_Interes").ToString())
                    Dim Cap_pagado As Decimal = 0
                    Dim pU_IntMor As Decimal = 0 ' DtFacturaPago.Rows(0).Item("U_IntMor").ToString()
                    Dim pTaxDate As String = "" 'DtFacturaPago.Rows(0).Item("DocDueDate").ToString()
                    Dim pDocDueDate As String = ""
                    Dim pOrdenVenta As String = ""
                    pDocEntry = row.DocEntry  'DtFacturaPago.Rows(0).Item("DocEntry").ToString()
                    pDocNum = row.DocNum ' DtFacturaPago.Rows(0).Item("DocNum").ToString()
                    pCardCode = row.CardCode  'DtFacturaPago.Rows(0).Item("CardCode").ToString()

                    If row.U_Interes <> Nothing Then
                        pU_DocCuota = row.U_DocCuota  ' DtFacturaPago.Rows(0).Item("U_DocCuota").ToString()
                        pU_CuotFi = row.U_CuotFi 'DtFacturaPago.Rows(0).Item("U_CuotFi").ToString()
                        pU_Interes = row.U_Interes '  DatosDB.ParseDecimalFormat(DtFacturaPago.Rows(0).Item("U_Interes").ToString())
                        pU_IntMor = row.U_IntMor ' DtFacturaPago.Rows(0).Item("U_IntMor").ToString()
                    End If
                    pDocTotal = row.DocTotal  'DatosDB.ParseDecimalFormat(DtFacturaPago.Rows(0).Item("DocTotal").ToString())
                    Cap_pagado = row.PaidToDate
                    pTaxDate = row.TaxDate 'DtFacturaPago.Rows(0).Item("DocDueDate").ToString()
                    pDocDueDate = row.DocDueDate
                    pOrdenVenta = BDInteresesMultas.getDocEntryOV(row.U_OrdenVenta).ToString()

                    Dim Fecha_Ini As Date = Convert.ToDateTime(pTaxDate)
                    pTaxDate = Fecha_Ini.Year.ToString() & Right(("0" & Fecha_Ini.Month.ToString()), 2) & Right(("0" & Fecha_Ini.Day.ToString()), 2)

                    Dim Fecha_DocDue As Date = Convert.ToDateTime(pDocDueDate)
                    pDocDueDate = Fecha_DocDue.Year.ToString() & Right(("0" & Fecha_DocDue.Month.ToString()), 2) & Right(("0" & Fecha_DocDue.Day.ToString()), 2)

                    Dim Lapso As String = DatosDB.GetParam("UD_Dia_Per")
                    Dim Anticipo As Decimal = 0
                    Dim Tasa_Multa As Decimal = 0
                    Dim FechaActual As Date = oApp.Company.ServerDate
                    Dim fechaActString As String = FechaActual.Year.ToString() & Right(("0" & FechaActual.Month.ToString()), 2) & Right(("0" & FechaActual.Day.ToString()), 2)

                    If Lapso = "" Then
                        oApp.MessageBox("Debe configuar el periodo entre cuotas.")
                        Return
                    End If

                    Dim Impuesto As String = DatosDB.GetParam("TaxComi2")

                    If Impuesto = "" Then
                        oApp.MessageBox("Debe configuar los parametros de impuestos.")
                        Return
                    End If

                    Dim salesTax As New Object
                    salesTax = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oSalesTaxCodes)
                    salesTax.GetByKey(Impuesto)
                    imp = salesTax.Rate

                    'AQUI VALIDAR IMPORTANTE LA FECHA DE ULTIMO PAGO FechaUltPago

                    Dim PorFactura As String = "0"
                    Dim FechaInicoCuotas As String = ""
                    Dim FechaInicio As DateTime

                    If pOrdenVenta = "" Then
                        pOrdenVenta = pDocEntry
                        PorFactura = "1"
                        FechaInicoCuotas = pDocDueDate
                        FechaInicio = DateTime.ParseExact(FechaInicoCuotas, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)
                    Else
                        FechaInicoCuotas = BDInteresesMultas.getFechaInicioCuotas(pOrdenVenta).ToString()
                        FechaInicio = DateTime.Parse(FechaInicoCuotas)
                    End If

                    FechaInicoCuotas = FechaInicio.Year.ToString() & Right(("0" & FechaInicio.Month.ToString()), 2) & Right(("0" & FechaInicio.Day.ToString()), 2)

                    Dim FechaUltPago As String = FechaInicoCuotas

                    Dim ultimaFecha As String = BDInteresesMultas.getUltimaFechaPagada(pDocEntry, pCardCode).ToString()

                    If ultimaFecha <> "" Then
                        Dim oDate As DateTime = DateTime.Parse(ultimaFecha)
                        FechaUltPago = oDate.Year.ToString() & Right(("0" & oDate.Month.ToString()), 2) & Right(("0" & oDate.Day.ToString()), 2)
                    End If

                    Dim Fecha_pago As String = fechaActString
                    Dim Fecha_valor As String = fechaValor.Year.ToString() & Right(("0" & fechaValor.Month.ToString()), 2) & Right(("0" & fechaValor.Day.ToString()), 2)
                    Dim numLin As Integer = 0

                    If i = 0 Then
                        oForm.DataSources.UserDataSources.Item("UD_SNeg").ValueEx = pCardCode
                        oForm.DataSources.UserDataSources.Item("UD_FecPag").ValueEx = fechaActString
                        oForm.DataSources.UserDataSources.Item("UD_FVal").ValueEx = Fecha_valor
                    End If

                    'Para setear el tipo de interés a utilizar
                    Dim defIntLeg As String = "N"
                    Dim CodeUserLicre As String = BDLineaCredito.ObtenerCodeLiCre(pCardCode)
                    If CodeUserLicre <> "" Then
                        Try
                            sCmp = DiApp.GetCompanyService
                            oGeneralService = sCmp.GetGeneralService("LICRE")
                            oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                            oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                            oGeneralParams.SetProperty("Code", CodeUserLicre)
                            oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                            defIntLeg = oGeneralData.GetProperty("U_Intlgl").ToString()
                        Catch ex As Exception
                            oApp.MessageBox(ex.Message)
                        End Try
                    End If

                    Dim InteresAp As Decimal = pU_Interes

                    If defIntLeg = "Y" Then
                        InteresAp = pU_IntMor
                    End If

                    Dim FechaUltPagoT As DateTime = DateTime.ParseExact(FechaUltPago, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)
                    Dim Fecha_valorT As DateTime = DateTime.ParseExact(Fecha_valor, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)

                    If FechaUltPagoT > Fecha_valorT Then
                        Fecha_valor = FechaUltPago
                        'oApp.MessageBox("La fecha valor de una factura no puede ser menor a la fecha de último pago.")
                        'Return
                    End If

                    'If Fecha_valorT > FechaActual Then
                    '    Fecha_valor = FechaUltPago
                    '    oApp.MessageBox("La fecha valor de una factura no puede ser mayor a la fecha actual.")
                    '    Return
                    'End If




                    Dim oStrInt As String = BDInteresesMultas.getCalculo_pagov2(pOrdenVenta, FechaUltPago, FechaUltPago, Fecha_pago, Fecha_valor, "0", PorFactura)
                    oDTFacAux.ExecuteQuery(oStrInt)

                    Dim Interes As String = "0"
                    Dim DiasIntMor As String = "0"
                    Dim InteresPorMor As String = "0"
                    Dim Multa As String = "0"
                    Dim Confirmar As Object = "N"

                    Dim resp As Integer = DateTime.Compare(Fecha_valorT, FechaUltPagoT)

                    If oDTFacAux.Rows.Count > 0 And resp > 0 Then
                        Interes = oDTFacAux.Columns.Item("INTERESTL").Cells.Item(0).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        DiasIntMor = oDTFacAux.Columns.Item("DIAS").Cells.Item(0).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        InteresPorMor = oDTFacAux.Columns.Item("INTERESTL").Cells.Item(0).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        Multa = oDTFacAux.Columns.Item("MULTA").Cells.Item(0).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        If DiasIntMor <> "0" Then
                            Confirmar = "Y"
                        End If
                    End If

                    Dim DocEntryF As Object = pDocEntry
                    Dim Factura As Object = pDocNum
                    Dim Capital As Object = pDocTotal.ToString()
                    Dim FechaPago As Object = Fecha_pago
                    Dim FechaUltimoPago As Object = FechaUltPago
                    Dim FechaVencimiento As Object = FechaUltPago
                    Dim InteresAplicable As Object = InteresAp.ToString(CultureInfo.InvariantCulture)
                    Dim MontoInteresReal As Object = Interes.ToString(CultureInfo.InvariantCulture)
                    Dim DiasMora As Object = DiasIntMor.ToString(CultureInfo.InvariantCulture)
                    Dim MontoDeInteresPorMora As Object = InteresPorMor.ToString(CultureInfo.InvariantCulture)

                    oDT.SetValue("DocEntry", i, DocEntryF)
                    oDT.SetValue("Factura", i, Factura)
                    oDT.SetValue("Capital", i, Capital)
                    oDT.SetValue("FechaUltimoPago", i, FechaUltPago)
                    oDT.SetValue("InteresAplicable", i, InteresAplicable.ToString())
                    oDT.SetValue("Dias", i, DiasMora.ToString())
                    oDT.SetValue("Interes", i, MontoDeInteresPorMora.ToString())
                    oDT.SetValue("Confirmar", i, Confirmar)
                    oDT.SetValue("Confirmar", i, Confirmar)

                    Dim InteresPorMorStr As String = InteresPorMor.ToString(CultureInfo.InvariantCulture)
                    Dim cantidadDecimal As Decimal = 0
                    Decimal.TryParse(InteresPorMorStr, cantidadDecimal)
                    TotalIntereses = TotalIntereses + cantidadDecimal
                    ' Formatear las columnas 
                    oColumn = oGrid.Columns.Item("Confirmar")
                    oColumn.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
                    oColumn.AffectsFormMode = False
                    i += 1
                Next
                'Limpiar filas vacias
                If oGrid.Rows.Count > 0 Then

                End If

                oForm.DataSources.UserDataSources.Item("UD_TotCol").ValueEx = TotalIntereses.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(TotalIntereses.ToString(CultureInfo.InvariantCulture))
                Dim totIntereses As Double = TotalIntereses
                oForm.DataSources.UserDataSources.Item("UD_APagar").ValueEx = TotalCuotasCre.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totIntereses.ToString(CultureInfo.InvariantCulture))
                Dim totGeneral As Double = totIntereses + TotalCuotasCre
                oForm.DataSources.UserDataSources.Item("UD_ToGn").ValueEx = totGeneral.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totGeneral.ToString(CultureInfo.InvariantCulture))
                oForm.DataSources.UserDataSources.Item("UD_Dispo").ValueEx = totGeneral.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totGeneral.ToString(CultureInfo.InvariantCulture))

                oGrid.Columns.Item(0).Visible = False
                oGridF.Columns.Item(0).Visible = False
                'oGridF.Columns.Item(1).Editable = False
                'oGridF.Columns.Item(2).Editable = False
                'oGridF.Columns.Item(3).Editable = False
                'oGridF.Columns.Item(4).Editable = False

                oForm.Freeze(False)


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("Configuración de parámetros incorrecto, contacte con Administrador")
                'oApp.MessageBox(ex.Message)
                Exit Sub
            End Try

        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Private Shared Sub creaFactura(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                Dim oDTs As SAPbouiCOM.DataTables
                Dim oInvInte As SAPbobsCOM.Documents
                Dim lRetCode As Integer
                Dim lErr As String = String.Empty
                Dim sErr As String = String.Empty
                Dim chk As SAPbouiCOM.CheckBox
                Dim ActivarGasto As String = DatosDB.GetParam("PL_RT")
                Dim CodigoGasto As String = DatosDB.GetParam("UD_GA")
                Dim It As String = DatosDB.GetParam("UD_IT") / 100

                oGrid = oForm.Items.Item("DbGrdInt").Specific
                oDTs = oForm.DataSources.DataTables
                Dim FechaActual As Date = oApp.Company.ServerDate
                Dim fechaValor As Date = oForm.DataSources.UserDataSources.Item("UD_FVal").Value
                Dim Fecha_valor As String = fechaValor.Year.ToString() & Right(("0" & fechaValor.Month.ToString()), 2) & Right(("0" & fechaValor.Day.ToString()), 2)

                Dim Fecha_valorT As DateTime = DateTime.ParseExact(Fecha_valor, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)

                NuevasDataTable.Clear()
                NuevasDataTable.Columns.Clear()
                NuevasDataTable.Rows.Clear()

                NuevasDataTable.Columns.Add("DocEntryOr")
                NuevasDataTable.Columns.Add("NroFactura")
                NuevasDataTable.Columns.Add("DiasMora")
                NuevasDataTable.Columns.Add("InteresAplicado")
                NuevasDataTable.Columns.Add("NumeroCuota")
                NuevasDataTable.Columns.Add("Monto")
                NuevasDataTable.Columns.Add("DocEntryGen")
                NuevasDataTable.Columns.Add("NroFacturaGen")

                FacturasConInteras.Clear()
                FacturasConInteras.Columns.Clear()
                FacturasConInteras.Rows.Clear()
                FacturasConInteras.Columns.Add("NroFactura")


                If Fecha_valorT > FechaActual Then
                    oApp.MessageBox("La fecha valor de una factura no puede ser mayor a la fecha actual.")
                    Return
                End If

                If oGrid.Rows.Count = 0 Then
                    oApp.MessageBox("No se tiene registros para procesar.")
                End If

                Dim taxCode As String = DatosDB.GetParam("TaxComi2")
                If taxCode = "" Then
                    oApp.MessageBox("Debe configuar el tipo de impuesto")
                    Return
                End If

                Dim itemCode As String = DatosDB.GetParam("ItemCode2")
                If itemCode = "" Then
                    oApp.MessageBox("Debe configurar el Item para Facturar")
                    'Return
                End If
                Dim totRows As Integer = 0

                If oGrid.Rows.Count > 0 Then
                    oInvInte = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                    oInvInte.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items
                    'oInvInte = oIvoAnt
                    oInvInte.DocDate = CDate(oApp.Company.ServerDate)
                    oInvInte.CardCode = oForm.DataSources.UserDataSources.Item("UD_SNeg").Value.ToString()
                    oInvInte.Comments = "Factura por intereses generados " ' & " - Factura Nro. " & Trim(oGrid.DataTable.Columns.Item("Factura").Cells.Item(i).Value)
                    oInvInte.PaymentGroupCode = -1
                    oInvInte.FolioNumber = 0
                    Dim TotalRetenciones As Double = 0
                    Dim TotalFacturaIntereses As Double = 0
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        Dim isSelected As String
                        isSelected = Trim(oGrid.DataTable.Columns.Item("Confirmar").Cells.Item(i).Value)

                        Dim facturaInteres As Decimal = DatosDB.ParseDecimalFormat(oGrid.DataTable.Columns.Item("Interes").Cells.Item(i).Value)


                        If isSelected = "Y" And facturaInteres > 0 Then
                            Dim antiguaFactura As String = oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value

                            'Dim oIvoAntTest As SAPbobsCOM.Payments = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)

                            Dim oIvoAnt As New Object
                            oIvoAnt = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                            oIvoAnt.GetByKey(antiguaFactura)


                            If facturaInteres > 0 Then

                                Dim tipoPrecio As String = ""
                                tipoPrecio = oIvoAnt.PriceMode.ToString()
                                If tipoPrecio = "1" Then
                                    oInvInte.Lines.PriceAfterVAT = facturaInteres
                                Else
                                    oInvInte.Lines.Price = facturaInteres
                                End If

                                Dim DocCur As String = oIvoAnt.DocCurrency.ToString()
                                oIvoAnt.DocCurrency.ToString()
                                oInvInte.DocCurrency = DocCur
                                oInvInte.Project = oIvoAnt.Project
                                oInvInte.SalesPersonCode = oIvoAnt.SalesPersonCode

                                If oIvoAnt.Indicator.ToString() = "" Then
                                    oInvInte.Indicator = "4"
                                Else
                                    oInvInte.Indicator = oIvoAnt.Indicator
                                End If

                                oInvInte.WithholdingTaxData.BaseDocEntry = oIvoAnt.WithholdingTaxData.BaseDocEntry
                                oInvInte.WithholdingTaxData.BaseDocLine = oIvoAnt.WithholdingTaxData.BaseDocLine
                                oInvInte.WithholdingTaxData.BaseDocType = oIvoAnt.WithholdingTaxData.BaseDocType

                                oInvInte.Lines.ItemCode = itemCode
                                oInvInte.Lines.Quantity = 1
                                oInvInte.Lines.TaxCode = taxCode
                                oInvInte.Lines.ProjectCode = oIvoAnt.Lines.ProjectCode
                                oInvInte.Lines.CostingCode = oIvoAnt.Lines.CostingCode
                                oInvInte.Lines.CostingCode2 = oIvoAnt.Lines.CostingCode2
                                oInvInte.Lines.CostingCode3 = oIvoAnt.Lines.CostingCode3
                                oInvInte.Lines.CostingCode4 = oIvoAnt.Lines.CostingCode4
                                oInvInte.Lines.CostingCode5 = oIvoAnt.Lines.CostingCode5
                                'oInvInte.Lines.BaseEntry = oIvoAnt.Lines.BaseEntry
                                'oInvInte.Lines.BaseType = oIvoAnt.Lines.BaseType

                                If ActivarGasto = "Y" Then
                                    oInvInte.Lines.Expenses.SetCurrentLine(0)
                                    oInvInte.Lines.Expenses.ExpenseCode = CodigoGasto
                                    Dim PriceAFV As Double = Math.Round((oInvInte.Lines.PriceAfterVAT * CDbl(It)), 2, MidpointRounding.AwayFromZero)
                                    oInvInte.Lines.Expenses.LineTotal = PriceAFV
                                    TotalRetenciones = TotalRetenciones + PriceAFV
                                    oInvInte.Lines.Expenses.TaxCode = "IVA"
                                    oInvInte.Lines.Expenses.Add()
                                    totRows = totRows + 1
                                    'oInvInte.Lines.WithholdingTaxLines.WTCode = "IT-V"
                                    'oInvInte.Lines.WithholdingTaxLines.WTAmount = TotalRetenciones
                                    'oInvInte.Lines.WithholdingTaxLines.Add()

                                End If

                                oInvInte.Lines.Add()
                                Dim fila As DataRow = FacturasConInteras.NewRow()
                                fila("NroFactura") = oGrid.DataTable.Columns.Item("Factura").Cells.Item(i).Value
                                FacturasConInteras.Rows.Add(fila)

                                TotalFacturaIntereses = TotalFacturaIntereses + facturaInteres
                            End If

                        End If
                    Next
                    oInvInte.WithholdingTaxData.WTCode = "IT-V"
                    oInvInte.WithholdingTaxData.WTAmount = TotalRetenciones
                    oInvInte.WithholdingTaxData.Add()
                    If TotalFacturaIntereses > 0 Then
                        lRetCode = oInvInte.Add()
                    End If

                    If lRetCode = 0 And TotalFacturaIntereses > 0 Then
                        'oApp.MessageBox("Factura por intereses generados" & " - Factura Nro. " & Trim(oGrid.DataTable.Columns.Item("Factura").Cells.Item(i).Value))
                        Dim nuevaFactura As String = DiApp.GetNewObjectKey() ' oInvInte.DocEntry.ToString()
                        Dim oDoc As SAPbobsCOM.Documents
                        oDoc = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices)
                        oDoc.GetByKey(nuevaFactura)
                        Dim nroFactura As String = oDoc.DocNum.ToString()
                        Dim MontoFactura As String = oDoc.DocTotal

                        Dim fila As DataRow = NuevasDataTable.NewRow()
                        fila("DocEntryOr") = ""
                        fila("NroFactura") = ""
                        fila("DiasMora") = ""
                        fila("InteresAplicado") = ""
                        fila("NumeroCuota") = "1 de 1"
                        fila("Monto") = ""
                        fila("DocEntryGen") = ""
                        fila("NroFacturaGen") = ""
                        fila("NroFactura") = 1 'oGrid.DataTable.Columns.Item("Factura").Cells.Item(i).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        fila("DiasMora") = 1 'oGrid.DataTable.Columns.Item("Dias").Cells.Item(i).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        fila("InteresAplicado") = 1 'oGrid.DataTable.Columns.Item("InteresAplicable").Cells.Item(i).Value.ToString().ToString(CultureInfo.InvariantCulture)
                        fila("Monto") = MontoFactura.ToString().ToString(CultureInfo.InvariantCulture)
                        fila("DocEntryGen") = oDoc.DocEntry.ToString().ToString().ToString(CultureInfo.InvariantCulture)
                        fila("NroFacturaGen") = oDoc.DocNum.ToString().ToString().ToString(CultureInfo.InvariantCulture)
                        NuevasDataTable.Rows.Add(fila)
                    End If

                    If lRetCode <> 0 Then
                        DiApp.GetLastError(lErr, sErr)
                        oApp.MessageBox("Error en la creación de factura de intereses")
                    End If
                End If

                InstanciarUDOIntereseGen(oApp, "", oForm)
                SBOFormNewPagRec.CreaSBOForm(oApp)

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Private Shared Sub InstanciarUDOIntereseGen(ByRef oApp As SAPbouiCOM.Application, ByVal mtx As String, ByVal oform As SAPbouiCOM.Form)
            Try
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                Dim oHeaderParams As SAPbobsCOM.GeneralDataParams
                Dim oHeadTableRow As SAPbobsCOM.GeneralData

                Dim sCmp As SAPbobsCOM.CompanyService

                Dim oGrid As SAPbouiCOM.Grid
                Dim oDT As SAPbouiCOM.DataTable
                Dim oDTs As SAPbouiCOM.DataTables
                Dim oColumn As SAPbouiCOM.GridColumn

                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService("INTERESES_GEN")
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralData.SetProperty("U_CardCode", oform.DataSources.UserDataSources.Item("UD_SNeg").Value.ToString(CultureInfo.InvariantCulture))
                    oGeneralData.SetProperty("U_FechaPa", oform.DataSources.UserDataSources.Item("UD_FecPag").Value.ToString(CultureInfo.InvariantCulture))
                    oGeneralData.SetProperty("U_FechaVa", oform.DataSources.UserDataSources.Item("UD_FVal").Value.ToString(CultureInfo.InvariantCulture))
                    'oGeneralData.SetProperty("U_Dias", "-1")
                    '@EXX_FACT1
                    'oGrid = oform.Items.Item("DbGrdInt").Specific
                    'oDTs = oform.DataSources.DataTables

                    'oDT = oDTs.Item("DT_PagInt")

                    ''oDT.Columns.Add("Nro", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                    'oDT.Columns.Add("Factura", SAPbouiCOM.BoFieldsType.ft_Text, 300)

                    DocEntryUdo = oGeneralService.Add(oGeneralData).GetProperty("DocEntry").ToString()
                    oGeneralData.GetProperty("DocEntry").ToString()


                    ' Set the params for receiving a specific record
                    oHeaderParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    oHeaderParams.SetProperty("DocEntry", DocEntryUdo) 'YOUR RECORD CODE

                    ' Get the UDO record
                    Try
                        oHeadTableRow = oGeneralService.GetByParams(oHeaderParams)
                    Catch ex As Exception
                        'UDO record does not exists
                        MsgBox(ex.Message)
                        Exit Sub
                    End Try

                    Dim oChild2TableRows As SAPbobsCOM.GeneralDataCollection = oHeadTableRow.Child("EXX_CUOT1")
                    ' here comes a trick, use an infinitive  loop 
                    Try

                        Dim origenDt As DataTable
                        origenDt = origenDataSet.Tables("Table1")

                        Dim fi As Integer = 0
                        For Each row As DataRow In origenDt.Rows
                            Dim PagoLinea As Decimal = DatosDB.ParseDecimalFormat(row("PagoTotLinea"))
                            Dim Fatura As String = row("DocNum").ToString()


                            If (PagoLinea = 0) Then

                                If FacturasConInteras.Rows.Count > 0 Then
                                    Dim res As DataRow = Nothing
                                    res = FacturasConInteras.Select("NroFactura like '%" + Fatura + "%'").AsEnumerable.FirstOrDefault()
                                    If Not res Is Nothing Then
                                        Dim oChildTableRow2 As SAPbobsCOM.GeneralData = oChild2TableRows.Add()

                                        oChildTableRow2.SetProperty("U_DocEntryF", row("DocEntry"))
                                        oChildTableRow2.SetProperty("U_NroFac", row("DocNum"))
                                        oChildTableRow2.SetProperty("U_Cuota", row("NumFact"))
                                        oChildTableRow2.SetProperty("U_Monto", PagoLinea.ToString())
                                        Dim fechaVecString As String = row("FechaVencimiento").ToString()
                                        Dim fechaVec As DateTime = DateTime.ParseExact(fechaVecString, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)
                                        oChildTableRow2.SetProperty("U_FechaV", fechaVec)
                                        oChildTableRow2.SetProperty("U_DocEntryPG", row("U_DocCuota"))
                                        oChildTableRow2.SetProperty("U_TotalCuota", row("DocTotal")) 'row("PagoTotLinea"))
                                        'oChildTableRow2.Add()
                                        oGeneralService.Update(oHeadTableRow)
                                    End If

                                End If

                            End If

                            If (PagoLinea > 0) Then
                                Dim oChildTableRow2 As SAPbobsCOM.GeneralData = oChild2TableRows.Add()

                                oChildTableRow2.SetProperty("U_DocEntryF", row("DocEntry"))
                                oChildTableRow2.SetProperty("U_NroFac", row("DocNum"))
                                oChildTableRow2.SetProperty("U_Cuota", row("NumFact"))
                                oChildTableRow2.SetProperty("U_Monto", PagoLinea.ToString())
                                Dim fechaVecString As String = row("FechaVencimiento").ToString()
                                Dim fechaVec As DateTime = DateTime.ParseExact(fechaVecString, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None)
                                oChildTableRow2.SetProperty("U_FechaV", fechaVec)
                                oChildTableRow2.SetProperty("U_DocEntryPG", row("U_DocCuota"))
                                oChildTableRow2.SetProperty("U_TotalCuota", row("DocTotal")) 'row("PagoTotLinea"))
                                'oChildTableRow2.Add()
                                oGeneralService.Update(oHeadTableRow)
                            End If

                            fi = fi + 1
                        Next

                    Catch ex As Exception
                        'oApp.MessageBox(ex.Message)
                    End Try

                    Dim oChildTableRows As SAPbobsCOM.GeneralDataCollection = oHeadTableRow.Child("EXX_FACT1")

                    Try

                        Dim fi As Integer = 0
                        For Each row As DataRow In NuevasDataTable.Rows
                            If row("Monto") > 0 Then
                                Dim oChildTableRow As SAPbobsCOM.GeneralData = oChildTableRows.Add()

                                oChildTableRow.SetProperty("U_DocEntryF", row("DocEntryOr"))
                                oChildTableRow.SetProperty("U_NumFact", row("NroFactura"))
                                oChildTableRow.SetProperty("U_DiasMor", row("DiasMora"))
                                oChildTableRow.SetProperty("U_InteresAp", row("InteresAplicado"))
                                oChildTableRow.SetProperty("U_Interes", row("Monto"))
                                oChildTableRow.SetProperty("U_DocEntryG", row("DocEntryGen"))
                                oChildTableRow.SetProperty("U_NumFactG", row("NroFacturaGen"))

                                oChildTableRows.Add()
                                oGeneralService.Update(oHeadTableRow)
                            End If
                            fi = fi + 1
                        Next

                    Catch ex As Exception
                        'End of infinitive loop, exiting, becase not found the record.... 
                        'or reached the maximum number of lines of child table
                    End Try

                Catch ex As Exception
                    oApp.MessageBox(ex.Message)
                End Try


                oApp.MessageBox("Codigo de Pago: " & DocEntryUdo.ToString())
                'oform.Close()
            Catch ex As Exception

            End Try

        End Sub

#End Region
        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                Try
                    oForm = oApp.Forms.Item(Trim(pVal.FormUID))
                Catch ex As Exception
                    oForm = oApp.Forms.Item(Trim(pVal.FormUID))
                End Try

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        'oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "DbGrdInt"
                                    oForm.Freeze(True)
                                    calcularMontoFacturable(oApp, oForm)
                                    oForm.Freeze(False)
                            End Select
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                        'oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "DbGrdInt"
                                    estableceEditable(oApp, oForm)
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        'oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            If ProcesaPagos = "Y" Then
                                Dim res As Integer = oApp.MessageBox("¿Generar el pago recibido?", 1, "Si", "No")
                                If res <> 1 Then
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        'oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "BtnConf"
                                    Try
                                        'oForm.Close()
                                        Dim montoDisponible As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_Dispo").Value.ToString(CultureInfo.InvariantCulture))
                                        Dim totalGeneral As Decimal = DatosDB.ParseDecimalFormat(oForm.DataSources.UserDataSources.Item("UD_ToGn").Value.ToString(CultureInfo.InvariantCulture))

                                        'Dim re As Integer = oApp.MessageBox("¿El Total General no coincide con el disponible, desea continuar?", 1, "Si", "No")

                                        'If re <> 1 Then
                                        '    BubbleEvent = False
                                        '    GoTo fail
                                        'End If

                                        Dim res As Integer = oApp.MessageBox("¿Esta seguro de procesar la solicitud?", 1, "Si", "No")

                                        If res <> 1 Then
                                            BubbleEvent = False
                                            GoTo fail
                                        End If

                                        creaFactura(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "b_Camb"
                                    Try
                                        PopulaGrid(oApp, oForm)

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "Bt_Dis"
                                    Try
                                        If oForm.DataSources.UserDataSources.Item("ChkDis").ValueEx.ToString() = "Y" Then
                                            ActualizaGridCuotas(oApp, oForm)
                                        End If


                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try


                                Case "chk_D"
                                    Try

                                        Dim chkDisponible As SAPbouiCOM.CheckBox
                                        chkDisponible = oForm.Items.Item("chk_D").Specific

                                        Dim txtDisponible As SAPbouiCOM.EditText
                                        txtDisponible = oForm.Items.Item("It_Disp").Specific

                                        If oForm.DataSources.UserDataSources.Item("ChkDis").ValueEx.ToString() = "Y" Then
                                            'oForm.DataSources.UserDataSources.Item("ChkDis").Value = "N"
                                            txtDisponible.Item.Enabled = True
                                        Else
                                            'If oForm.DataSources.UserDataSources.Item("ChkDis").ValueExToString() = "N" Then
                                            'oForm.DataSources.UserDataSources.Item("ChkDis").Value = "Y"
                                            txtDisponible.Item.Enabled = False
                                        End If

                                        'oForm.DataSources.UserDataSources.Item("UD_Dispo").

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try


                            End Select
                        Else
                        End If

                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
        Private Shared Sub calcularMontoFacturable(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                Dim oDTs As SAPbouiCOM.DataTables
                oGrid = oForm.Items.Item("DbGrdInt").Specific
                oDTs = oForm.DataSources.DataTables
                If oGrid.Rows.Count = 0 Then
                    oApp.MessageBox("No se tiene registros para procesar.")
                End If
                Dim NuevoTotalFactura As Decimal = 0
                If oGrid.Rows.Count > 0 Then

                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        Dim isSelected As String
                        isSelected = Trim(oGrid.DataTable.Columns.Item("Confirmar").Cells.Item(i).Value)

                        Dim MontoInteresReal As String = Trim(oGrid.DataTable.Columns.Item("Interes").Cells.Item(i).Value).ToString()
                        'InteresPorMora
                        Dim MontoInteresRealDouble As Decimal = DatosDB.ParseDecimalFormat(MontoInteresReal)

                        NuevoTotalFactura = NuevoTotalFactura + MontoInteresRealDouble
                        'oGrid.DataTable.Columns.Item("TotalAFacturar").Cells.Item(i).Value = NuevoTotalFactura.ToString()
                        If NuevoTotalFactura = 0 Then
                            oApp.MessageBox("El monto de interés es igual a 0")
                            'Return
                        End If
                    Next

                    oForm.DataSources.UserDataSources.Item("UD_TotCol").ValueEx = NuevoTotalFactura.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(TotalIntereses.ToString(CultureInfo.InvariantCulture))
                    'oForm.DataSources.UserDataSources.Item("UD_TInt").ValueEx = totIntereses.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totIntereses.ToString(CultureInfo.InvariantCulture))
                    oForm.DataSources.UserDataSources.Item("UD_APagar").ValueEx = TotalCuotasCre.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totIntereses.ToString(CultureInfo.InvariantCulture))
                    Dim totGeneral As Double = NuevoTotalFactura + TotalCuotasCre
                    oForm.DataSources.UserDataSources.Item("UD_ToGn").ValueEx = totGeneral.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totGeneral.ToString(CultureInfo.InvariantCulture))
                    oForm.DataSources.UserDataSources.Item("UD_Dispo").ValueEx = totGeneral.ToString(CultureInfo.InvariantCulture) 'DatosDB.DBDS_IN(totGeneral.ToString(CultureInfo.InvariantCulture))


                    'PopulaGrid(oApp, oForm)
                    'ActualizaGridCuotas(oApp, oForm)



                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Private Shared Sub estableceEditable(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Try
                oForm.Freeze(True)
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("DbGrdInt").Specific
                oGrid.Item.Enabled = True

                If oGrid.Rows.Count = 0 Then
                    oApp.MessageBox("No se tiene registros para procesar.")
                End If
                Dim NuevoTotalFactura As Decimal = 0
                If oGrid.Rows.Count > 0 Then
                    oGrid.Columns.Item("Interes").Editable = True
                    oGrid.Columns.Item("Factura").Editable = False
                    oGrid.Columns.Item("Capital").Editable = False
                    oGrid.Columns.Item("FechaUltimoPago").Editable = False
                    oGrid.Columns.Item("Dias").Editable = False
                    oGrid.Columns.Item("InteresAplicable").Editable = False
                    oGrid.Columns.Item("Confirmar").Editable = True

                End If
                oForm.Freeze(False)
                oForm.Update()

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub
        Public Shared Function PruebaOneDataTable() As DataSet
            FactDataset.Tables.Clear()
            OneDataTable.Clear()
            OneDataTable.Columns.Clear()
            OneDataTable.Rows.Clear()

            OneDataTable.Columns.Add("SocioNegocio")
            Dim filaOne As DataRow = OneDataTable.NewRow()
            filaOne("SocioNegocio") = oForm.DataSources.UserDataSources.Item("UD_SNeg").Value.ToString() ' "09.149-200-0"
            OneDataTable.Rows.Add(filaOne)


            ThreeDataTable.Clear()
            ThreeDataTable.Columns.Clear()
            ThreeDataTable.Rows.Clear()

            ThreeDataTable.Columns.Add("DocEntry")
            ThreeDataTable.Columns.Add("NumeroCuota")
            ThreeDataTable.Columns.Add("Monto")

            Dim oDTFac2 As SAPbouiCOM.DataTable
            Dim oDTsFac2 As SAPbouiCOM.DataTables
            'Se setean los objetos 
            'oGridFac = oForm.Items.Item("Ma_Fact").Specific
            oDTsFac2 = oForm.DataSources.DataTables
            oDTFac2 = oDTsFac2.Item("DT_Fac")
            'oDTFac.Clear()

            For i As Integer = 0 To oDTFac2.Rows.Count - 1
                Dim fila As DataRow = ThreeDataTable.NewRow()
                fila("DocEntry") = oDTFac2.Columns.Item("Factura").Cells.Item(i).Value.ToString()
                fila("NumeroCuota") = oDTFac2.Columns.Item("NumFact").Cells.Item(i).Value.ToString()

                fila("Monto") = oDTFac2.Columns.Item("PagoTotLinea").Cells.Item(i).Value.ToString()
                If fila("Monto") <> "0.00" And fila("Monto") <> "0" Then
                    ThreeDataTable.Rows.Add(fila)
                End If

                'fila = ThreeDataTable.NewRow()

            Next

            Dim fi As Integer = 0
            For Each row As DataRow In NuevasDataTable.Rows

                Dim fila As DataRow = ThreeDataTable.NewRow()
                fila("DocEntry") = row("NroFacturaGen")
                fila("NumeroCuota") = row("NumeroCuota")
                fila("Monto") = row("Monto")
                ThreeDataTable.Rows.Add(fila)
                fi = fi + 1
            Next

            FactDataset.Tables.Add(OneDataTable)
            FactDataset.Tables.Add(ThreeDataTable)
            Return FactDataset
        End Function

        Public Shared Function PruebaOneDatatable2(ByRef socio As String, ByRef udo As String) As DataSet
            FactDataset2.Tables.Clear()
            OneTable.Clear()
            OneTable.Columns.Clear()
            OneTable.Rows.Clear()

            OneTable.Columns.Add("SocioNegocio")
            Dim filaOne As DataRow = OneTable.NewRow()
            filaOne("SocioNegocio") = socio
            OneTable.Rows.Add(filaOne)

            ThreeTable.Clear()
            ThreeTable.Columns.Clear()
            ThreeTable.Rows.Clear()

            ThreeTable.Columns.Add("DocEntry")
            ThreeTable.Columns.Add("NumeroCuota")
            ThreeTable.Columns.Add("Monto")

            Dim oDTFac2 As SAPbouiCOM.DataTable
            Dim oDTsFac2 As SAPbouiCOM.DataTables
            Dim query2 As String = String.Empty
            'Se setean los objetos 
            'oGridFac = oForm.Items.Item("Ma_Fact").Specific
            oDTsFac2 = oForm.DataSources.DataTables
            oDTFac2 = oDTsFac2.Item("MAT_LIS")

            If Motor = "SQL" Then
                query2 = "select B.U_NroFac as Factura, B.U_Cuota as NumFact, B.U_Monto as PagoTotLinea from [@EXX_INTERESES] A inner join [@EXX_CUOT1] B on A.DocEntry = B.DocEntry where A.DocEntry = '" & udo & "' and A.U_CardCode = '" & socio & "' union select B.U_NumFactG as Factura, '1 de 1' as NumFact, B.U_Interes as PagoTotLinea from [@EXX_INTERESES] A inner join [@EXX_FACT1] B on A.DocEntry = B.DocEntry where A.DocEntry = '" & udo & "' and A.U_CardCode = '" & socio & "' and B.U_NumFactG Is not null"
            Else
                query2 = "select B.""U_NroFac"" as ""Factura"", B.""U_Cuota"" as ""NumFact"", B.""U_Monto"" as ""PagoTotLinea"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"" AS A inner join """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOT1"" AS B on A.""DocEntry"" = B.""DocEntry"" where A.""DocEntry"" = '" & udo & "' and A.""U_CardCode"" = '" & socio & "' union Select B.""U_NumFactG"" as ""Factura"", '1 de 1' as ""NumFact"", B.""U_Interes"" as ""PagoTotLinea"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"" AS A inner join """ & SQLBaseDatos.ToUpper & """.""@EXX_FACT1"" AS B on A.""DocEntry"" = B.""DocEntry"" where A.""DocEntry"" = '" & udo & "' and A.""U_CardCode"" = '" & socio & "' and B.""U_NumFactG"" Is not null;"
            End If

            oDTFac2.ExecuteQuery(query2)

            For i As Integer = 0 To oDTFac2.Rows.Count - 1
                Dim fila As DataRow = ThreeTable.NewRow()
                fila("DocEntry") = oDTFac2.Columns.Item("Factura").Cells.Item(i).Value.ToString()
                fila("NumeroCuota") = oDTFac2.Columns.Item("NumFact").Cells.Item(i).Value.ToString()
                fila("Monto") = oDTFac2.Columns.Item("PagoTotLinea").Cells.Item(i).Value.ToString()
                ThreeTable.Rows.Add(fila)
                'fila = ThreeDataTable.NewRow()
            Next

            Dim fi As Integer = 0
            For Each row As DataRow In NuevasTable.Rows

                Dim fila As DataRow = ThreeTable.NewRow()
                fila("DocEntry") = row("NroFacturaGen")
                fila("NumeroCuota") = row("NumeroCuota")
                fila("Monto") = row("Monto")
                ThreeTable.Rows.Add(fila)
                fi = fi + 1
            Next

            FactDataset2.Tables.Add(OneTable)
            FactDataset2.Tables.Add(ThreeTable)
            Return FactDataset2
        End Function
    End Class
End Namespace






﻿Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Data
Imports XFinanciamiento.Utiles




Namespace View
    Public Class FormFactura

#Region "VALIDACIONES"

#End Region
#Region "METODOS"

        Public Shared Function PagosFactura(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef oDT As DataTable, ByRef DocEntryOV As String, ByRef CardCode As String, cantidadCuota As String) As Boolean

            Dim oInvoicePre As SAPbobsCOM.Documents
            Dim Fecha As Date = DateTime.Today
            Dim It As String = DatosDB.GetParam("UD_IT") / 100
            Dim ActivarGasto As String = DatosDB.GetParam("PL_RT")
            Dim CodigoGasto As String = DatosDB.GetParam("UD_GA")
            Dim lRetCode As Integer
            Dim lErr As Integer = 0
            Dim sErr As String = String.Empty
            Dim cuotatotal As Decimal

            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
            Dim oDTCuotas As DataTable = BDCuotas.GetCuotas(DocEntryOV)


            Try

                oInvoicePre = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)
                oInvoicePre.DocObjectCode = SAPbobsCOM.BoObjectTypes.oInvoices
                oInvoicePre.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items
                oInvoicePre.CardCode = CardCode
                oInvoicePre.DocDate = Fecha
                oInvoicePre.DocDueDate = Fecha
                oInvoicePre.TaxDate = Fecha


                For Each row As DataRow In oDT.Rows

                    oInvoicePre.Lines.BaseLine = row.Item("LineNum")
                    oInvoicePre.Lines.BaseEntry = DocEntryOV
                    oInvoicePre.Lines.BaseType = 17
                    oInvoicePre.Lines.Currency = row.Item("Currency")
                    oInvoicePre.Lines.ItemCode = row.Item("ItemCode")
                    oInvoicePre.Lines.ItemDescription = row.Item("Dscription")
                    oInvoicePre.Lines.Quantity = row.Item("Quantity")
                    oInvoicePre.Lines.WarehouseCode = row.Item("WhsCode")
                    oInvoicePre.Lines.PriceAfterVAT = row.Item("PriceAfVAT")
                    oInvoicePre.Lines.Price = row.Item("Price")
                    If Not row.Item("CogsOcrCod").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode = row.Item("CogsOcrCod")
                    End If
                    If Not row.Item("CogsOcrCo2").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode2 = row.Item("CogsOcrCo2")
                    End If
                    If Not row.Item("CogsOcrCo3").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode3 = row.Item("CogsOcrCo3")
                    End If
                    If Not row.Item("CogsOcrCo4").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode4 = row.Item("CogsOcrCo4")
                    End If
                    If Not row.Item("CogsOcrCo5").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode5 = row.Item("CogsOcrCo5")
                    End If

                    oInvoicePre.Lines.ProjectCode = row.Item("Project")

                    If ActivarGasto = "Y" Then
                        'oInvoicePre.Lines.Expenses.SetCurrentLine(row.Item("LineNum"))
                        oInvoicePre.Lines.Expenses.ExpenseCode = CodigoGasto
                        oInvoicePre.Lines.Expenses.LineTotal = (row.Item("GTotal") * It)
                        oInvoicePre.Lines.Expenses.TaxCode = "IVA"
                        oInvoicePre.Lines.Expenses.Add()
                    End If


                    oInvoicePre.Lines.Add()
                Next

                For Each row As DataRow In oDTCuotas.Rows
                    oInvoicePre.Installments.DueDate = row.Item("U_FechaV")
                    oInvoicePre.Installments.Total = row.Item("U_Amortizacion")
                    cuotatotal = oInvoicePre.Installments.Percentage
                    oInvoicePre.Installments.Add()

                Next
                cuotatotal = oInvoicePre.Installments.TotalFC
                oInvoicePre.Comments = "Factura Preliminar basada en: " & Trim(DocEntryOV)
                oInvoicePre.SalesPersonCode = oDBCab.GetValue("SlpCode", 0)
                oInvoicePre.Indicator = oDBCab.GetValue("Indicator", 0)

                oInvoicePre.UserFields.Fields.Item("U_DocCuota").Value = oDBCab.GetValue("U_DocCuota", 0)
                oInvoicePre.UserFields.Fields.Item("U_CuotFi").Value = oDBCab.GetValue("U_CuotFi", 0)
                oInvoicePre.UserFields.Fields.Item("U_IntMor").Value = oDBCab.GetValue("U_IntMor", 0)
                oInvoicePre.UserFields.Fields.Item("U_Interes").Value = oDBCab.GetValue("U_Interes", 0)
                oInvoicePre.UserFields.Fields.Item("U_OrdenVenta").Value = oDBCab.GetValue("DocNum", 0)
                oInvoicePre.UserFields.Fields.Item("U_DocFactPre").Value = oDBCab.GetValue("U_DocFactPre", 0)


                lRetCode = oInvoicePre.Add()

                If lRetCode <> 0 Then
                    DiApp.GetLastError(lErr, sErr)
                    MsgBox(sErr)
                    Return False
                Else
                    Dim sNewObjCode As String = String.Empty
                    DiApp.GetNewObjectCode(sNewObjCode)
                    Dim nuevoDocEntry As String = Trim(sNewObjCode)

                    If Not addCampos(oApp, oForm, nuevoDocEntry) Then
                        Return False
                    Else
                        If oApp.Menus.Item("1304").Enabled = True Then
                            oApp.ActivateMenuItem("1304")
                            oForm = oApp.Forms.ActiveForm
                            oForm.Items.Item("138").Click()
                            Return True
                        End If
                    End If

                End If

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function addCampos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef nuevoDocEntry As String)

            Dim oOV As SAPbobsCOM.Documents
            Dim lRetCode, lErr As Integer
            Dim sErr As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")

            Try

                oOV = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                oOV.GetByKey(Trim(oDBCab.GetValue("DocEntry", 0)))

                oOV.UserFields.Fields.Item("U_DocFactPre").Value = nuevoDocEntry

                lRetCode = oOV.Update

                If lRetCode <> 0 Then
                    DiApp.GetLastError(lErr, sErr)
                    oApp.MessageBox(lErr & " " & sErr)
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function PagosFacturaRefin(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef oDT As DataTable, docEntryOV As String, cardCode As String, cantidadCuota As String, EsRefin As String) As Boolean
            Dim oInvoicePre As SAPbobsCOM.Documents
            Dim Fecha As Date = DateTime.Today
            Dim It As String = DatosDB.GetParam("UD_IT") / 100
            Dim ActivarGasto As String = DatosDB.GetParam("PL_RT")
            Dim CodigoGasto As String = DatosDB.GetParam("UD_GA")
            Dim lRetCode As Integer
            Dim lErr As Integer = 0
            Dim sErr As String = String.Empty
            Dim cuotatotal As Decimal

            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
            Dim oDTCuotas As DataTable = BDCuotas.GetCuotas(docEntryOV)


            Try

                oInvoicePre = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)
                oInvoicePre.DocObjectCode = SAPbobsCOM.BoObjectTypes.oInvoices
                oInvoicePre.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service
                oInvoicePre.CardCode = cardCode
                oInvoicePre.DocDate = Fecha
                oInvoicePre.DocDueDate = Fecha
                oInvoicePre.TaxDate = Fecha


                For Each row As DataRow In oDT.Rows

                    oInvoicePre.Lines.BaseLine = row.Item("LineNum")
                    oInvoicePre.Lines.BaseEntry = docEntryOV
                    oInvoicePre.Lines.BaseType = 17
                    oInvoicePre.Lines.Currency = row.Item("Currency")
                    'oInvoicePre.Lines.ItemCode = row.Item("ItemCode")
                    oInvoicePre.Lines.ItemDescription = row.Item("Dscription")
                    oInvoicePre.Lines.Quantity = row.Item("Quantity")
                    'oInvoicePre.Lines.WarehouseCode = row.Item("WhsCode")
                    oInvoicePre.Lines.PriceAfterVAT = row.Item("PriceAfVAT")
                    oInvoicePre.Lines.Price = row.Item("Price")
                    If Not row.Item("CogsOcrCod").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode = row.Item("CogsOcrCod")
                    End If
                    If Not row.Item("CogsOcrCo2").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode2 = row.Item("CogsOcrCo2")
                    End If
                    If Not row.Item("CogsOcrCo3").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode3 = row.Item("CogsOcrCo3")
                    End If
                    If Not row.Item("CogsOcrCo4").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode4 = row.Item("CogsOcrCo4")
                    End If
                    If Not row.Item("CogsOcrCo5").Equals(DBNull.Value) Then
                        oInvoicePre.Lines.CostingCode5 = row.Item("CogsOcrCo5")
                    End If

                    'oInvoicePre.Lines.ProjectCode = row.Item("Project")

                    If ActivarGasto = "Y" Then
                        'oInvoicePre.Lines.Expenses.SetCurrentLine(row.Item("LineNum"))
                        oInvoicePre.Lines.Expenses.ExpenseCode = CodigoGasto
                        oInvoicePre.Lines.Expenses.LineTotal = (row.Item("GTotal") * It)
                        oInvoicePre.Lines.Expenses.TaxCode = "IVA"
                        oInvoicePre.Lines.Expenses.Add()
                    End If


                    oInvoicePre.Lines.Add()
                Next

                For Each row As DataRow In oDTCuotas.Rows
                    oInvoicePre.Installments.DueDate = row.Item("U_FechaV")
                    oInvoicePre.Installments.Total = row.Item("U_Amortizacion")
                    cuotatotal = oInvoicePre.Installments.Percentage
                    oInvoicePre.Installments.Add()

                Next
                cuotatotal = oInvoicePre.Installments.TotalFC
                oInvoicePre.Comments = "Factura Preliminar basada en: " & Trim(docEntryOV)
                oInvoicePre.SalesPersonCode = oDBCab.GetValue("SlpCode", 0)
                oInvoicePre.Indicator = oDBCab.GetValue("Indicator", 0)

                oInvoicePre.UserFields.Fields.Item("U_DocCuota").Value = oDBCab.GetValue("U_DocCuota", 0)
                oInvoicePre.UserFields.Fields.Item("U_CuotFi").Value = oDBCab.GetValue("U_CuotFi", 0)
                oInvoicePre.UserFields.Fields.Item("U_IntMor").Value = oDBCab.GetValue("U_IntMor", 0)
                oInvoicePre.UserFields.Fields.Item("U_Interes").Value = oDBCab.GetValue("U_Interes", 0)
                oInvoicePre.UserFields.Fields.Item("U_OrdenVenta").Value = oDBCab.GetValue("DocNum", 0)
                oInvoicePre.UserFields.Fields.Item("U_DocFactPre").Value = oDBCab.GetValue("U_DocFactPre", 0)
                oInvoicePre.UserFields.Fields.Item("U_Refinancia").Value = EsRefin


                lRetCode = oInvoicePre.Add()

                If lRetCode <> 0 Then
                    DiApp.GetLastError(lErr, sErr)
                    MsgBox(sErr)
                    Return False
                Else
                    Dim sNewObjCode As String = String.Empty
                    DiApp.GetNewObjectCode(sNewObjCode)
                    Dim nuevoDocEntry As String = Trim(sNewObjCode)

                    If Not addCampos(oApp, oForm, nuevoDocEntry) Then
                        Return False
                    Else
                        If oApp.Menus.Item("1304").Enabled = True Then
                            oApp.ActivateMenuItem("1304")
                            oForm = oApp.Forms.ActiveForm
                            oForm.Items.Item("138").Click()
                            Return True
                        End If
                    End If

                End If

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Sub FormFactNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)

            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("OINV")
            Try
                If oDBCab.GetValue("U_Refinancia", 0) = "Y" Then
                    oForm.Title = "Factura de deudores - Refinanciamiento"
                End If
fail:
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

#End Region

#Region "EVENTOS"

        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        If pVal.BeforeAction = False Then
                            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("OINV")
                            If oDBCab.GetValue("U_Refinancia", 0) = "Y" Then
                                oForm.Title = "Factura de deudores - borrador - Refinanciamiento"
                            End If
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("ItemEvent Error:" & ex.ToString)

            End Try
        End Sub

        Public Shared Sub FormDataEvent(ByRef oApp As SAPbouiCOM.Application, ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(BusinessObjectInfo.FormUID)
                Select Case BusinessObjectInfo.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                        If BusinessObjectInfo.BeforeAction = False Then
                            If BusinessObjectInfo.ActionSuccess = True Then
                                'pagosFactura(oApp, oForm)
                            End If
                        End If

                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

    End Class
End Namespace

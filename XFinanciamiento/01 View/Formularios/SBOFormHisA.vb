﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization


Namespace View
    Public Class SBOFormHisA

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, Optional Form As String = "", Optional DocEntry As String = "")
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOFormHisA")

                If blnexit Then
                    Exit Sub
                End If

                strPath = Application.StartupPath & "\SBOFormHisA.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)

                oForm = oApp.Forms.Item("SBOFormHisA")

                PopulaGridHis(oApp, oForm, Form, DocEntry)

                oForm.Visible = True

            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Public Shared Function PopulaGridHis(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Optional Formulario As String = "", Optional DocEntry As String = "") As Boolean
            Dim grdHis As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim numLin As Integer = 0
            Dim query As String = String.Empty

            Try
                oForm.Freeze(True)
                grdHis = oForm.Items.Item("grdHis").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dtHIS")

                query = DatosAutorizaciones.PopulaGridHisA(Formulario, DocEntry)

                oDT.ExecuteQuery(query)

                grdHis.DataTable = oDT
                If grdHis.Rows.Count > 0 Then
                    For i As Integer = 0 To grdHis.Rows.Count - 1
                        numLin = oDT.GetValue("FilNum", i)
                        If Not numLin = 0 Then
                            grdHis.RowHeaders.SetText(i, numLin)
                        Else
                            grdHis.RowHeaders.SetText(i, "")
                        End If
                    Next
                End If

                grdHis.RowHeaders.TitleObject.Caption = "#"
                grdHis.Columns.Item("FilNum").Visible = False '
                grdHis.Columns.Item("FilNum").Editable = False '
                grdHis.Columns.Item("Obra").Editable = False '
                grdHis.Columns.Item("Correlativo").Editable = False '
                grdHis.Columns.Item("CodigoEstado").Editable = False '
                grdHis.Columns.Item("Estado").Editable = False '
                grdHis.Columns.Item("CodigoAccion").Editable = False '
                grdHis.Columns.Item("Accion").Editable = False '
                grdHis.Columns.Item("Etapa").Editable = False '
                grdHis.Columns.Item("CodigoUsr").Editable = False '
                grdHis.Columns.Item("NombreUsr").Editable = False '
                grdHis.Columns.Item("Fecha").Editable = False '
                grdHis.Columns.Item("CorrelativoSol").Editable = False '
                grdHis.Columns.Item("MotivoRechazo").Editable = False '
                grdHis.AutoResizeColumns()

                oForm.Freeze(False)

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormHisA(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Select Case pVal.ItemUID
                            Case "btnCerr"
                                If pVal.Before_Action = False Then
                                    oForm.Close()
                                    Exit Sub
                                End If
                        End Select
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#End Region
    End Class
End Namespace


﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes

'Imports Proyectos.Control.Negocio
Imports XFinanciamiento.Control.Negocio
'Imports Proyectos.Data
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
'Imports Proyectos.Utiles
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormEtpA

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "AUTETAP", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)

                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm

                'formatItems(oApp, oForm)

                'LoadCombos(oApp, oForm)



            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub


        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Private Shared Function PopulaForms(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosAutorizaciones.GetForms()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        If oDT.Rows.Count > 0 Then
                            'Primero se limpian los valores del combo
                            For i As Integer = 0 To oCombo.ValidValues.Count - 1
                                oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next

                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            For i As Integer = 0 To oCombo.ValidValues.Count - 1
                                oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los formularios.")
                            Return False
                        End If
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los formularios.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los formularios.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox

            Try
                oForm.Freeze(True)

                'combo formularios
                oCombo = oForm.Items.Item("20_U_Cb").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oCombo.Item.AffectsFormMode = True

                If Not PopulaForms(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de formularios")
                    oForm.Freeze(True)
                End If


                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Private Shared Function formatItems(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox

            Try
                'Combo formularios
                oCombo = oForm.Items.Item("20_U_Cb").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Public Shared Function ChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean

            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "0_U_G"
                    Select Case oEvent.ColUID
                        Case "C_0_1"
                            bBubbleEvent = EmpChooseFromList(FormUID, oEvent, oApplication, oCompany)
                    End Select

            End Select


            Return bBubbleEvent
        End Function

        Public Shared Function AñadeFilaAuto(ByRef oForm As SAPbouiCOM.Form) As Boolean
            Try
                Dim oMatLin As SAPbouiCOM.Matrix
                Dim oDBLin As SAPbouiCOM.DBDataSource
                Dim maxindex As Integer
                oForm.Freeze(True)
                Try
                    'se setean las variables a sus correspondientes objetos
                    oMatLin = oForm.Items.Item("0_U_G").Specific
                    oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                Catch ex As Exception
                    oForm.Freeze(False)
                    Return False
                End Try

                oDBLin.Clear()
                oMatLin.FlushToDataSource()

                For i As Integer = 0 To oDBLin.Size - 1
                    If Trim(oDBLin.GetValue("U_CodUsr", i)) = "" Then
                        oForm.Freeze(False)
                        Return False
                    End If
                Next

                maxindex = oDBLin.Size
                oDBLin.InsertRecord(maxindex)
                oDBLin.SetValue("LineId", maxindex, maxindex + 1)
                oMatLin.LoadFromDataSourceEx(False)
                'oMatLin.AutoResizeColumns()
                oMatLin = Nothing
                oForm.Freeze(False)
                'oForm.Update()
                Return (True)
            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function

        Public Shared Sub OrdenaLineId(ByRef oDBdatasource As SAPbouiCOM.DBDataSource)
            Dim usr As String = String.Empty
            Try
                For i As Integer = 0 To oDBdatasource.Size - 1
                    oDBdatasource.SetValue("LineId", i, i + 1)
                Next
                oDBdatasource.InsertRecord(oDBdatasource.Size)
                oDBdatasource.SetValue("LineId", oDBdatasource.Size - 1, oDBdatasource.Size)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function BorraFilaVacia(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim usuario As String = String.Empty
            Dim oMat As SAPbouiCOM.Matrix
            Try
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat = oForm.Items.Item("0_U_G").Specific
Borrar:

                For i As Integer = 0 To oDBLin.Size - 1
                    usuario = Trim(oDBLin.GetValue("U_CodUsr", i))

                    If usuario = "" Then
                        oDBLin.RemoveRecord(i)
                        oMat.LoadFromDataSourceEx(False)
                        GoTo Borrar
                    End If
                Next
                oMat.LoadFromDataSourceEx(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function EmpChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                  ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oMat As SAPbouiCOM.Matrix
                    Dim oDBSource As SAPbouiCOM.DBDataSource


                    oForm = oApplication.Forms.Item(FormUID)
                    oMat = oForm.Items.Item("0_U_G").Specific
                    oDBSource = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)

                    If Not oDataTable Is Nothing Then
                        Try
                            oDBSource.SetValue("U_NomUsr", oEvent.Row - 1, oDataTable.GetValue("USER_CODE", 0))
                            oDBSource.SetValue("U_CodUsr", oEvent.Row - 1, oDataTable.GetValue("USERID", 0))
                        Catch ex As Exception
                        End Try
                    Else
                        Return False
                    End If

                    oMat.LoadFromDataSourceEx(False)

                    Try
                        If Not ValidaUsuariosNoRepetidos(oApplication, oForm, oEvent) Then
                            oForm.Freeze(False)
                            oApplication.MessageBox("No se puede elegir un usuario repetido en la matriz, por favor verifique la información")
                            Return False
                        End If
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApplication.MessageBox(ex.Message)
                        Return False
                    End Try

                    AñadeFilaAuto(oForm)

                    oMat.LoadFromDataSourceEx(False)

                End If
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function

#End Region

#Region "EVENTOS"

        Public Shared Sub AdminSBOFormUDOEtpNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim oComboF As SAPbouiCOM.ComboBox



            Try
                'oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat = oForm.Items.Item("0_U_G").Specific



fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub


        Public Shared Sub AdminSBOFormUDOEtpADatEve(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix

            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat = oForm.Items.Item("0_U_G").Specific
                Select Case pVal.EventType



                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                        If pVal.BeforeAction = False Then


                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                        If pVal.BeforeAction = True Then

                            Try
                                If Not BorraFilaVacia(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNombreEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaDescrEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroAprobaciones(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroRechazos(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try


                            Try
                                If Not ValidaNroUsuariosApro(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroUsuariosRechazo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                        If pVal.BeforeAction = True Then

                            Try
                                If Not BorraFilaVacia(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNombreEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaDescrEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroAprobaciones(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroRechazos(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try


                            Try
                                If Not ValidaNroUsuariosApro(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNroUsuariosRechazo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                        End If

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
        Public Shared Sub AdminSBOFormEtpA(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "0_U_G"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.Before_Action = True Then

                            Select Case pVal.ItemUID

                                Case "20_U_Cb"
                                    Dim oCombo As SAPbouiCOM.ComboBox
                                    oCombo = oForm.Items.Item("20_U_Cb").Specific
                                    oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        If oCombo.ValidValues.Count = 0 Then
                                            PopulaForms(oApp, oForm, oCombo)
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "20_U_Cb"
                                    Dim oCombo As SAPbouiCOM.ComboBox
                                    oCombo = oForm.Items.Item("20_U_Cb").Specific
                                    oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        If oCombo.ValidValues.Count = 0 Then
                                            PopulaForms(oApp, oForm, oCombo)
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If
                End Select


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#End Region

#Region "VALIDACIONES"

        Public Shared Function ValidaNroRechazos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nroRechazos As Integer
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                nroRechazos = oDBCab.GetValue("U_NroR", 0)

                If nroRechazos < 1 Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe tener un número de rechazos mayor a 0")
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaNroAprobaciones(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nroAproba As Integer
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                nroAproba = oDBCab.GetValue("U_NroA", 0)

                If nroAproba < 1 Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe tener un número de aprobaciones mayor a 0")
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaDescrEtapa(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nombre As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")

                nombre = Trim(oDBCab.GetValue("U_Descr", 0))

                If nombre = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe ingresar una descripción para la etapa de autorización")
                    Return False
                End If


                Return True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Public Shared Function ValidaNombreEtapa(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nombre As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")

                nombre = Trim(oDBCab.GetValue("U_Nombre", 0))

                If nombre = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe ingresar un nombre para la etapa de autorización")
                    Return False
                End If


                Return True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ValidaNroUsuariosApro(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nroAproba As Integer
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat = oForm.Items.Item("0_U_G").Specific

                oMat.FlushToDataSource()

                nroAproba = oDBCab.GetValue("U_NroA", 0)

                If nroAproba > 0 Then

                    BorraFilaVacia(oApp, oForm)

                    If oDBLin.Size < nroAproba Then
                        oForm.Freeze(False)
                        oApp.MessageBox("No se puede tener menos usuarios que número de aprobaciones")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe tener al menos un número de aprobaciones")
                    Return False
                End If
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaNroUsuariosRechazo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim nroRechazos As Integer
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAP")
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat = oForm.Items.Item("0_U_G").Specific

                oMat.FlushToDataSource()

                nroRechazos = oDBCab.GetValue("U_NroR", 0)

                If nroRechazos > 0 Then

                    BorraFilaVacia(oApp, oForm)

                    If oDBLin.Size < nroRechazos Then
                        oForm.Freeze(False)
                        oApp.MessageBox("No se puede tener menos usuarios que número de rechazos")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe tener al menos un número de rechazos")
                    Return False
                End If
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaUsuariosNoRepetidos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent) As Boolean
            Dim offset As Integer
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim usuario As String = String.Empty
            Try
                oForm.Freeze(True)
                oMat = oForm.Items.Item("0_U_G").Specific
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOETAL")
                oMat.FlushToDataSource()
                offset = pVal.Row
                usuario = Trim(oDBLin.GetValue("U_CodUsr", offset - 1))

                For i As Integer = 1 To oDBLin.Size
                    If Not i = offset Then
                        If Trim(oDBLin.GetValue("U_CodUsr", i - 1)) = usuario Then
                            oDBLin.SetValue("U_CodUsr", offset - 1, String.Empty)
                            oDBLin.SetValue("U_NomUsr", offset - 1, String.Empty)
                            oMat.LoadFromDataSourceEx(False)
                            oForm.Freeze(False)
                            'oMat.Columns.Item("C_0_2").Cells.Item(offset).Click()
                            Return False
                        End If
                    End If
                Next
                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
#End Region

    End Class
End Namespace



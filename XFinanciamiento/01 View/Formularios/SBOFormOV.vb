﻿Imports System.Data.Odbc
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Data
Imports System.Globalization

Namespace View
    Public Class SBOFormOV

#Region "VALIDACIONES"

        Private Shared Function validaForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim Cuotas As SAPbouiCOM.EditText = oForm.Items.Item("EdtCuotFi").Specific
            If Cuotas.Value <> "" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function verificaMoneda(ByRef Moneda As String) As String

            Dim Total As String = String.Empty

            If Moneda = "S" Then
                Total = "DocTotalSy"
            End If
            If Moneda = "L" Then
                Total = "DocTotal"
            End If
            If Moneda = "C" Then
                Total = "DocTotalFC"
            End If

            Return Total

        End Function

        Private Shared Function verificaPago(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form) As Boolean

            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
            Dim DocEntryOV As String = oDBCab.GetValue("DocEntry", 0)
            Dim oDTCuotas As DataTable = BDCuotas.GetCuotas(DocEntryOV)

            If oDTCuotas.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        End Function

        Private Shared Function validatipoInteres(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef ItemCode As String, ByRef SocioNegocio As String) As Boolean
            Try



                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try

        End Function

#End Region
#Region "METODOS"

        Public Shared Sub CrearCampoUsr(ByVal oFrm As SAPbouiCOM.Form,
                                                       ByVal NombreNewItem As String,
                                                       ByVal NombreNewLbl As String,
                                                       ByVal ItemTop As Integer,
                                                       ByVal ItemLeft As Integer,
                                                       ByVal ItemWidth As Integer,
                                                       ByVal LblTop As Integer,
                                                       ByVal LblLeft As Integer,
                                                       ByVal LblWidth As Integer,
                                                       ByVal blnvisible As Boolean,
                                                       ByVal LblCaption As String,
                                                       ByVal tabla As String,
                                                       ByVal campo As String)

            Try
                Dim itm As SAPbouiCOM.Item
                Dim lbl As SAPbouiCOM.Item
                Dim button As SAPbouiCOM.Item
                Dim btn As SAPbouiCOM.Button
                Dim edt As SAPbouiCOM.EditText
                Dim lblEdt As SAPbouiCOM.StaticText

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_EDIT) 'Nuevo Edit              
                lbl = oFrm.Items.Add(NombreNewLbl, SAPbouiCOM.BoFormItemTypes.it_STATIC)
                button = oFrm.Items.Add("btnCalC", SAPbouiCOM.BoFormItemTypes.it_BUTTON)


                lbl.Top = LblTop
                lbl.Left = LblLeft
                lbl.Width = 100
                lbl.FromPane = 7
                lbl.ToPane = 7

                lblEdt = lbl.Specific
                lblEdt.Caption = LblCaption
                lblEdt.Item.Visible = True

                itm.Top = ItemTop
                itm.Left = ItemLeft
                itm.Width = ItemWidth - 30
                itm.FromPane = 7
                itm.ToPane = 7
                itm.Visible = blnvisible

                edt = itm.Specific
                edt.DataBind.SetBound(True, tabla, campo)
                edt.Item.Enabled = True
                edt.Item.AffectsFormMode = False
                edt.Item.Enabled = False
                'edt.Item.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                lblEdt.Item.LinkTo = edt.Item.UniqueID

                button.Top = LblTop
                button.Width = 25
                button.Height = 13
                button.Left = ItemLeft + 145
                button.FromPane = 7
                button.ToPane = 7
                button.Visible = True

                btn = button.Specific
                btn.Type = SAPbouiCOM.BoButtonTypes.bt_Image
                Dim spath As String = System.Windows.Forms.Application.StartupPath
                btn.Image = spath + "\Cuota3.jpg"
                btn.Item.Enabled = True

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CrearButton(ByVal oFrm As SAPbouiCOM.Form,
                                                       ByVal NombreNewItem As String,
                                                       ByVal ItemWidth As Integer,
                                                       ByVal ItemHeight As Integer,
                                                       ByVal ItemLeft As Integer)
            Try
                Dim button As SAPbouiCOM.Item
                Dim btn As SAPbouiCOM.Button
                Dim itmButton As SAPbouiCOM.Button

                button = oFrm.Items.Add("btnPre", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
                itmButton = oFrm.Items.Item("2").Specific

                button.Top = itmButton.Item.Top
                button.Width = ItemWidth
                button.Height = ItemHeight
                button.Left = itmButton.Item.Left + ItemLeft
                button.Visible = True
                button.FromPane = 7
                button.ToPane = 7

                btn = button.Specific
                btn.Caption = NombreNewItem
                btn.Item.Enabled = True
                btn.Item.LinkTo = oFrm.Items.Item("2").UniqueID

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CrearCampoUsrC(ByVal oFrm As SAPbouiCOM.Form,
                                                       ByVal NombreNewItem As String,
                                                       ByVal NombreNewLbl As String,
                                                       ByVal NombreNewLnk As String,
                                                       ByVal ItemTop As Integer,
                                                       ByVal ItemLeft As Integer,
                                                       ByVal ItemWidth As Integer,
                                                       ByVal LblTop As Integer,
                                                       ByVal LblLeft As Integer,
                                                       ByVal blnvisible As Boolean,
                                                       ByVal LblCaption As String,
                                                       ByVal tabla As String,
                                                       ByVal campo As String)

            Try
                Dim itm As SAPbouiCOM.Item
                Dim linked As SAPbouiCOM.Item
                Dim lbl As SAPbouiCOM.Item
                Dim lnk As SAPbouiCOM.LinkedButton
                Dim edt As SAPbouiCOM.EditText
                Dim lblEdt As SAPbouiCOM.StaticText

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                lbl = oFrm.Items.Add(NombreNewLbl, SAPbouiCOM.BoFormItemTypes.it_STATIC)
                linked = oFrm.Items.Add(NombreNewLnk, SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)

                lbl.Top = LblTop
                lbl.Left = LblLeft + 148
                lbl.Width = 70
                lbl.FromPane = 7
                lbl.ToPane = 7

                lblEdt = lbl.Specific
                lblEdt.Caption = LblCaption
                lblEdt.Item.Visible = True

                itm.Top = ItemTop
                itm.Left = ItemLeft + 110
                itm.Width = ItemWidth - 110
                itm.FromPane = 7
                itm.ToPane = 7
                itm.Visible = blnvisible

                edt = itm.Specific
                edt.DataBind.SetBound(True, tabla, campo)
                edt.Item.AffectsFormMode = False
                'edt.Item.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                edt.Item.Enabled = False
                lblEdt.Item.LinkTo = edt.Item.UniqueID

                linked.Left = LblLeft + 100
                linked.Top = LblTop
                linked.FromPane = 7
                linked.ToPane = 7

                lnk = linked.Specific
                lnk.Item.LinkTo = edt.Item.UniqueID
                lnk.LinkedObjectType = "CUOTA"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Function crearMatrix(ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim oNuevoItem, oItem As SAPbouiCOM.Item
            Dim oPanel As SAPbouiCOM.Folder
            'Dim omatrix As SAPbouiCOM.Matrix = oForm.Items.Item("38").Specific
            Try
                oNuevoItem = oForm.Items.Add("Garantias", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem = oForm.Items.Item("138")
                oNuevoItem.Top = oItem.Top
                oNuevoItem.Height = oItem.Height
                oNuevoItem.Width = oItem.Width
                oNuevoItem.Left = oItem.Left + oItem.Width
                oPanel = oNuevoItem.Specific
                oPanel.Caption = "Garantias y Otros"
                oPanel.GroupWith(("138"))
                oForm.PaneLevel = 4
                Return (True)

            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function

        Private Shared Function crearComboF(ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim oNuevoItem As SAPbouiCOM.Item
            Dim cmbCombo As SAPbouiCOM.ComboBox

            Try
                oNuevoItem = oForm.Items.Add("cmdComF", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
                oNuevoItem.AffectsFormMode = False
                oNuevoItem.DisplayDesc = True
                oNuevoItem.Top = 173
                oNuevoItem.Height = 14
                oNuevoItem.Width = 69
                oNuevoItem.Left = 131
                oNuevoItem.ToPane = 4
                oNuevoItem.FromPane = 1
                cmbCombo = oNuevoItem.Specific


                oForm.PaneLevel = 1
                Return (True)

            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function

        Private Shared Function anadirCombo(ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim cmbFinan As SAPbouiCOM.ComboBox
            Dim odtParam As DataTable = DatosParamFN.GetParamFN()

            Try

                If cmbFinan.ValidValues.Count > 0 Then

                    If Not odtParam Is Nothing And odtParam.Rows.Count > 0 Then
                        For i As Integer = 0 To cmbFinan.ValidValues.Count - 1
                            cmbFinan.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        Next
                    End If

                Else


                End If

                'For Each row As DataRow In odtParam.Rows
                '            codigo = row.Item("Code")
                '            desc = row.Item("Name")
                '            cmbFinan.ValidValues.Add(codigo, desc)
                '        Next row


                Return (True)
            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function

        Private Shared Function itemExists(ByRef oForm As SAPbouiCOM.Form, Campo As String) As Boolean
            Try
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item(Campo)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Sub FormOVNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)
            Dim Folder As SAPbouiCOM.Folder = oForm.Items.Item("138").Specific
            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
            Try

                If Folder.Selected Then
                    oForm.Items.Item("EdtDocC").Enabled = False
                    oForm.Items.Item("EdtCuotFi").Enabled = False
                    oForm.Items.Item("EdtFactPre").Enabled = False
                End If

                If oDBCab.GetValue("U_Refinancia", 0) = "Y" Then
                    oForm.Title = "Orden de Venta - Refinanciamiento"
                End If


fail:

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Public Shared Sub CrearCampoUsrF(ByVal oFrm As SAPbouiCOM.Form,
                                                       ByVal NombreNewItem As String,
                                                       ByVal NombreNewLnk As String,
                                                       ByVal ItemWidth As Integer,
                                                       ByVal ItemHeight As Integer,
                                                       ByVal ItemLeft As Integer,
                                                       ByVal blnvisible As Boolean,
                                                       ByVal tabla As String,
                                                       ByVal campo As String)

            Try
                Dim itm As SAPbouiCOM.Item
                Dim itmButton As SAPbouiCOM.Button
                Dim linked As SAPbouiCOM.Item
                Dim lnk As SAPbouiCOM.LinkedButton
                Dim edt As SAPbouiCOM.EditText

                itm = oFrm.Items.Add(NombreNewItem, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                linked = oFrm.Items.Add(NombreNewLnk, SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
                itmButton = oFrm.Items.Item("2").Specific


                itm.Top = itmButton.Item.Top
                itm.Left = itmButton.Item.Left + ItemLeft
                itm.Width = ItemWidth
                itm.Enabled = False
                itm.FromPane = 7
                itm.ToPane = 7

                edt = itm.Specific
                edt.DataBind.SetBound(True, tabla, campo)
                edt.Item.Visible = True
                edt.Item.AffectsFormMode = False
                ' edt.Item.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                edt.Item.Enabled = False
                linked.Left = itm.Left - 20
                linked.Top = itmButton.Item.Top
                linked.FromPane = 7
                linked.ToPane = 7

                lnk = linked.Specific
                lnk.Item.LinkTo = edt.Item.UniqueID
                lnk.LinkedObjectType = "112"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

#Region "EVENTOS"

        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        If pVal.Before_Action = True Then
                            oForm.Freeze(True)
                            Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
                            If oDBCab.GetValue("U_Refinancia", 0) = "Y" Then
                                oForm.Title = "Orden Refinanciada - Borrador"
                            End If
                            oForm.Freeze(False)
                        End If

                    'Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    '    If pVal.Before_Action = True Then
                    '        If crearMatrix(oForm) = False Then
                    '            oForm.Freeze(False)
                    '            oApp.MessageBox("Ocurrió un error al crear la Matriz Garantías.")
                    '            BubbleEvent = False
                    '            GoTo fail
                    '        End If

                    '        If crearComboF(oForm) = False Then
                    '            oForm.Freeze(False)
                    '            oApp.MessageBox("Ocurrió un error al cargar la el combo Garantías")
                    '            BubbleEvent = False
                    '            GoTo fail 'mauricio
                    '        End If
                    '    End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "Garantias" And pVal.Before_Action = True Then
                            oForm.PaneLevel = 4
                        End If

                        If (pVal.FormTypeEx = "139") Then

                            If (pVal.ItemUID = "138") And (pVal.Before_Action = False) Then

                                Dim oForm1 As SAPbouiCOM.Form
                                oForm1 = Nothing
                                oForm1 = oApp.Forms.Item(pVal.FormUID.ToString)
                                oForm1.Freeze(True)

                                If Not itemExists(oForm, "EdtCuotFi") Then
                                    CrearCampoUsr(oForm1, "EdtCuotFi", "LblCuotFi",
                                                      oForm1.Items.Item("153").Top + 15,
                                                      oForm1.Items.Item("153").Left,
                                                      oForm1.Items.Item("153").Width - 85,
                                                      oForm1.Items.Item("152").Top + 15,
                                                      oForm1.Items.Item("152").Left,
                                                      oForm1.Items.Item("152").Width,
                                                      True, "Nro. Cuotas", "ORDR", "U_CuotFi")
                                End If
                                oForm.Items.Item("EdtCuotFi").Enabled = False

                                If Not itemExists(oForm, "EdtDocC") Then
                                    CrearCampoUsrC(oForm1, "EdtDocC", "LblCuotas", "LnkDocC",
                                                     oForm1.Items.Item("153").Top + 15,
                                                     oForm1.Items.Item("153").Left,
                                                     oForm1.Items.Item("153").Width,
                                                     oForm1.Items.Item("152").Top + 15,
                                                     oForm1.Items.Item("152").Left,
                                                     True, "Nro. Plan Pagos", "ORDR", "U_DocCuota")
                                End If
                                oForm.Items.Item("EdtDocC").Enabled = False

                                If Not itemExists(oForm, "EdtFactPre") Then
                                    CrearCampoUsrF(oForm1, "EdtFactPre", "LnkFactPre",
                                                     50, 19, 230,
                                                     True, "ORDR", "U_DocFactPre")
                                End If
                                oForm.Items.Item("EdtFactPre").Enabled = False

                                If verificaPago(oApp, oForm) Then
                                    If Not itemExists(oForm, "btnPre") Then
                                        CrearButton(oForm1, "Generar Preliminar", 90, 19, 100)
                                    End If
                                End If

                                oForm1.Freeze(False)
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.Before_Action = True And pVal.FormMode = 1 Then

                            Select Case pVal.ItemUID

                                Case "btnCalC"

                                    Dim oDBCab, oDBcablinea As SAPbouiCOM.DBDataSource
                                    Dim DocEntry As String
                                    oDBCab = oForm.DataSources.DBDataSources.Item("ORDR")
                                    oDBcablinea = oForm.DataSources.DBDataSources.Item("RDR1")
                                    DocEntry = oDBCab.GetValue("DocEntry", 0)
                                    Dim Moneda As String = oDBCab.GetValue("CurSource", 0)
                                    Dim TipoMoneda As String = verificaMoneda(Moneda)
                                    Dim CapitalTotal As String = Trim(oDBCab.GetValue(TipoMoneda, 0))
                                    Dim FechaDoc As String = Trim(oDBCab.GetValue("DocDate", 0))
                                    Dim SocioNegocio As String = Trim(oDBCab.GetValue("CardCode", 0))
                                    Dim ItemCode As String = oDBcablinea.GetValue("ItemCode", 0)

                                    If validaForm(oApp, oForm) = False Then
                                        SBOFormUDOCuotas.CreaSBOForm(oApp, DocEntry, CapitalTotal, FechaDoc, SocioNegocio, ItemCode)
                                    End If

                                Case "btnPre"

                                    Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("ORDR")
                                    Dim DocFactPre As String = oDBCab.GetValue("U_DocFactPre", 0)
                                    Dim DocCuotaPago As String = oDBCab.GetValue("U_DocCuota", 0)

                                    If DocFactPre = "" And DocCuotaPago <> "" Then
                                        Dim DocEntryOV As String = oDBCab.GetValue("DocEntry", 0)
                                        Dim CardCode As String = oDBCab.GetValue("CardCode", 0)
                                        Dim cantidadCuota As String = oDBCab.GetValue("U_CuotFi", 0)
                                        Dim oDT As DataTable = DatosDB.GetInfoLineasOV(Trim(DocEntryOV))
                                        Dim EsRefin As String = oDBCab.GetValue("U_Refinancia", 0)

                                        If EsRefin = "Y" Then
                                            If FormFactura.PagosFacturaRefin(oApp, oForm, oDT, DocEntryOV, CardCode, cantidadCuota, EsRefin) = True Then
                                                oApp.StatusBar.SetText("[FN] Se creo la factura preeliminar refinanciada con éxito.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                            Else
                                                GoTo fail
                                            End If
                                        Else
                                            If FormFactura.PagosFactura(oApp, oForm, oDT, DocEntryOV, CardCode, cantidadCuota) = True Then
                                                oApp.StatusBar.SetText("[FN] Se creo la factura preeliminar con éxito.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                            Else
                                                GoTo fail
                                            End If
                                        End If
                                    Else
                                        oApp.StatusBar.SetText("[FN] Ya se generó un preliminar.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    End If
                            End Select
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("ItemEvent Error:" & ex.ToString)
            End Try
        End Sub

#End Region

    End Class
End Namespace
﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control


Namespace View

    Public Class SBOFormVTC

        Public Shared Sub CreaSBOFormVTC(ByVal oApp As SAPbouiCOM.Application)
            Try
                'oForm.Freeze(True)
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String

                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOFormVTC")

                If blnexit Then
                    Exit Sub
                End If

                strPath = Application.StartupPath & "\SBOFormVTC.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)

                oForm = oApp.Forms.Item("SBOFormVTC")

                oForm.Visible = True

            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub


        ''' <summary>
        ''' EVENTOS DEL FORMULARIO VERIFICACION DE TABLAS Y CAMPOS
        ''' </summary>
        Public Shared Sub AdminSBOFormVTC(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Select Case pVal.EventType
                    'Case para evento PRESSED
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "btnVerifi"
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                                    SBOFormVTC.btnVerifi_Click(oForm, oApp)

                                Case "btnCancela"
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                                    SBOFormVTC.btnCancela_Click(oForm, oApp)

                            End Select
                        End If
                        ' FIN Case para evento PRESSED
                End Select 'Fin Select Case para tipo de Evento
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#Region "EVENTOS"

        Public Shared Sub btnCancela_Click(ByVal oForm As SAPbouiCOM.Form, ByVal oApp As SAPbouiCOM.Application)
            oForm.Close()
        End Sub

        Public Shared Sub btnVerifi_Click(ByVal oForm As SAPbouiCOM.Form, ByVal oApp As SAPbouiCOM.Application)
            Try

                Dim btnVerifi As SAPbouiCOM.Button
                btnVerifi = oForm.Items.Item("btnVerifi").Specific

                If btnVerifi.Caption = "OK" Then
                    oForm.Close()
                Else

                    Dim iReturnValue As Integer
                    iReturnValue = oApp.MessageBox("Sap advierte en la nota 798624 que puede haber pérdida de integridad" + vbCrLf +
                                                   "de datos cuando se agregan o actualizan campos de usuario mientras haya" + vbCrLf +
                                                   "usuarios conectados a la base de datos." + vbCrLf + vbCrLf +
                                                   "Por lo anterior asegúrese que no haya usuarios conectados en el momento " + vbCrLf +
                                                   "de ejecutar esta opción." + vbCrLf, 2, "Continuar", "Cancelar")

                    Select Case iReturnValue
                        Case 1

                            Dim UserConect As New Hashtable
                            'UserConect = NegocioTablasUser.LeeUsuariosConectadosDB()
                            Dim usercon As Boolean = True
                            If usercon Then

                                Dim iReturnValueUser As Integer
                                iReturnValueUser = oApp.MessageBox("Está a punto de actualizar tablas/campos de usuario pero existen " + vbCrLf +
                                                                   " Usuarios conectados." + vbCrLf +
                                                                   "¿ Desea continuar con el proceso de verificación de tablas?" + vbCrLf, 2, "Continuar", "Cancelar")

                                Select Case iReturnValueUser
                                    Case 1
                                        CrearVerificarTablas(oForm, oApp)

                                    Case 2

                                End Select

                            Else
                                CrearVerificarTablas(oForm, oApp)

                            End If
                            'END CASE 1 MENSAJE SOBRE LA MODIFICACION

                        Case 2

                    End Select

                End If

            Catch ex As Exception
                oForm.Items.Item("btnCancela").Enabled = True
                Throw ex
            End Try
        End Sub

#End Region

#Region "METODOS"
        ''' <summary>
        ''' Crea verifica tablas y campos de usuario
        ''' </summary>
        Public Shared Sub CrearVerificarTablas(ByVal oForm As SAPbouiCOM.Form, ByVal oApp As SAPbouiCOM.Application)
            Dim progress As SAPbouiCOM.ProgressBar = Nothing
            Try
                oForm.Items.Item("btnVerifi").Enabled = False
                oForm.Items.Item("btnCancela").Enabled = False

                'Tabla Maestra - ADD XFinanciamiento
                NegocioTablasUser.Creatabla("EXX_GABL", "Master Tables", "Financiamiento", "NO")
                TransactionsTablas.CamposTablaGABL(oForm)

                'Creacion de Tablas para autorizaciones - ADD XFinanciamiento
                TransactionsAutorizaciones.CreaTablasAUTO(oForm, oApp)
                'Creacion de Tablas - ADD XFinanciamiento
                TransactionsTablas.CreaTablas(oForm, oApp)

                'Creacion de Campos - ADD XFinanciamiento
                progress = oApp.StatusBar.CreateProgressBar("Creando los campos en tablas de usuario...", 100, False)
                TransactionsTablas.CamposTablaFIPLAN(oForm)
                progress.Value = 10
                TransactionsTablas.CamposTablaFIPLA1(oForm)
                progress.Value = 15
                TransactionsTablas.CamposTablaLICRE(oForm)
                progress.Value = 20
                TransactionsTablas.CamposTablaLICR1(oForm)
                progress.Value = 25
                TransactionsTablas.CamposTablaLICR2(oForm)
                progress.Value = 27
                TransactionsTablas.CamposTablaIATT(oForm)
                progress.Value = 30
                TransactionsTablas.CamposTablasParam(oForm)
                progress.Value = 35
                TransactionsAutorizaciones.CamposTablasAutorizaciones(oForm)
                'progress.Value = 40
                'TransactionsAutorizaciones.CamposTablaAUTOFOR(oForm)
                'progress.Value = 75
                'TransactionsAutorizaciones.CamposTablaAUTOFEST(oForm)
                progress.Value = 65
                TransactionsTablas.CamposTablaFIPLD1(oForm)
                progress.Value = 75
                TransactionsTablas.CamposTablasCuotas(oForm)
                progress.Value = 80
                TransactionsTablas.CamposTablasLCuotas(oForm)
                progress.Value = 85
                TransactionsTablas.CamposTablaINTERESES(oForm)
                progress.Value = 90
                TransactionsTablas.CamposTablaEXX_FACT1(oForm)
                progress.Value = 95
                TransactionsTablas.CamposTablaEXX_CUOT1(oForm)
                progress.Value = 100

                System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                progress = Nothing

                TransactionsUDOS.CrearUDOS(oForm, oApp) ' ssh subido

                TransactionsTablas.CamposLinkeados(oForm) ' ssh subido

                'Aca se adicionan los procedimientos almacenados SQL
                progress = oApp.StatusBar.CreateProgressBar("Creando los procedimientos y adicionando valores", 100, False)
                If Motor = "SQL" Then
                    TransactionsTablas.CreaProcedimientos(sqlData.calculo_interes_name, sqlData.calculo_interes_text, "3")
                    progress.Value = 10
                    TransactionsTablas.CreaProcedimientos(sqlData.fn_Fechas_Cuotas_name, sqlData.fn_Fechas_Cuotas_text, "3")
                    progress.Value = 25
                    TransactionsTablas.CreaProcedimientos(sqlData.Cuota_Fija_name, sqlData.Cuota_Fija_text, "3")
                    progress.Value = 45
                    TransactionsTablas.CreaProcedimientos(sqlData.sp_Calculo_Cuotas_name, sqlData.sp_Calculo_Cuotas_text, "3")
                    progress.Value = 55
                    TransactionsTablas.CreaProcedimientos(sqlData.Calculo_Multa_name, sqlData.Calculo_Multa_text, "3")
                    progress.Value = 65
                    TransactionsTablas.CreaProcedimientos(sqlData.sp_Calculo_pago_name, sqlData.sp_Calculo_pago_text, "3")
                    progress.Value = 75
                    TransactionsTablas.CreaProcedimientos(sqlData.sp_Cuotas_x_pagar_bc_name, sqlData.sp_Cuotas_x_pagar_bc_text, "3")
                    progress.Value = 80
                    TransactionsTablas.CreaProcedimientos(sqlData.sp_Cuotas_x_pagar_detalle_bc_name, sqlData.sp_Cuotas_x_pagar_detalle_bc_text, "3")
                    progress.Value = 85
                    TransactionsTablas.CreaProcedimientos(sqlData.sp_Calculo_Interes_Mora_name, sqlData.sp_Calculo_Interes_Mora_text, "3")
                    progress.Value = 100
                Else
                    progress.Value = 15
                    TransactionsTablas.CreaTType(hanaData.CALCPAGOTYPE_NAME.ToUpper, hanaData.CALCPAGOTYPE_TEXT)
                    progress.Value = 20
                    TransactionsTablas.CreaTType(hanaData.CALPAGOTYPE_NAME.ToUpper, hanaData.CALPAGOTYPE_TEXT)
                    progress.Value = 30
                    TransactionsTablas.CreaTType(hanaData.CALCUOTATYPE_NAME.ToUpper, hanaData.CALCUOTATYPE_TEXT)
                    progress.Value = 40
                    TransactionsTablas.CreaTType(hanaData.CALPAGOBC_NAME.ToUpper, hanaData.CALPAGOBC_TEXT)
                    progress.Value = 45
                    TransactionsTablas.CreaTType(hanaData.CALPAGODETALLEBC_NAME.ToUpper, hanaData.CALPAGODETALLEBC_TEXT)
                    progress.Value = 50
                    TransactionsTablas.CreaProcedimientos(hanaData.FN_FECHAS_CUOTAS_HANA_NAME, hanaData.FN_FECHAS_CUOTAS_HANA_TEXT, "2")
                    progress.Value = 65
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CUOTA_FIJA_HANA_NAME, hanaData.SP_CUOTA_FIJA_HANA_TEXT, "3")
                    progress.Value = 70
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CALCULO_MULTA_HANA_NAME, hanaData.SP_CALCULO_MULTA_HANA_TEXT, "3")
                    progress.Value = 75
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CALCULO_INTERES_HANA_NAME, hanaData.SP_CALCULO_INTERES_HANA_TEXT, "3")
                    progress.Value = 80
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CALCULO_CUOTAS_HANA_NAME, hanaData.SP_CALCULO_CUOTAS_HANA_TEXT, "3")
                    progress.Value = 85
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CALCULO_PAGO_HANA_NAME, hanaData.SP_CALCULO_PAGO_HANA_TEXT, "3")
                    progress.Value = 90
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CUOTAS_X_PAGAR_BC_HANA_NAME, hanaData.SP_CUOTAS_X_PAGAR_BC_HANA_TEXT, "3")
                    progress.Value = 93
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA_NAME, hanaData.SP_CUOTAS_X_PAGAR_DETALLE_BC_HANA_TEXT, "3")
                    progress.Value = 95
                    TransactionsTablas.CreaProcedimientos(hanaData.SP_CALCULO_INTERES_MORA_HANA_NAME, hanaData.SP_CALCULO_INTERES_MORA_HANA_TEXT, "3")
                    progress.Value = 100

                End If
                progress.Stop()
                System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                progress = Nothing

                'progress = oApp.StatusBar.CreateProgressBar("Creando los campos en documentos nativos", 100, False)
                ''Aca se adicionan los UDOS que se deben crear     
                'progress.Value = 10
                'TransactionsUDOS.CrearUDOS(oForm, oApp) ' arriba
                'Aca se adicionan los campos que se deben crear dentro de las tablas anteriormente creadas o de sistema
                'progress.Value = 50
                'TransactionsTablas.CamposLinkeados(oForm) ' arriba
                'progress.Value = 100
                'progress.Stop()
                'System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                'progress = Nothing

                oForm.Items.Item("btnVerifi").Enabled = True
                oForm.Items.Item("btnCancela").Enabled = True

                oApp.MessageBox("El proceso de verificación de tablas, campos y procedimientos almacenados terminó " + vbCrLf +
                                "exitosamente.	Se recomienda salir y volver a ingresar al sistema." + vbCrLf)

                Dim btnVerifi As SAPbouiCOM.Button
                btnVerifi = oForm.Items.Item("btnVerifi").Specific
                btnVerifi.Caption = "OK"
            Catch ex As Exception
                If Not progress Is Nothing Then
                    progress.Stop()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(progress)
                    progress = Nothing
                End If
                Exit Sub
            End Try
        End Sub

        ''' <summary>
        ''' Verifica si el formulario no esta en ejecucion
        ''' </summary>
        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

#End Region

    End Class

End Namespace

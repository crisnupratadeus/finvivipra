﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormFechaLst
#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                'cnp20190711
                If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpMOD") = False Then
                    Exit Sub
                End If

                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOFecLst")
                If blnexit Then
                    Exit Sub
                End If
                strPath = Application.StartupPath & "\SBOFechaLst.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)
                oForm = oApp.Forms.Item("SBOFecLst")
                PopulaGrid(oApp, oForm)
                oForm.Visible = True
            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub
        Public Shared Sub CreaSBOForm2(ByVal oApp As SAPbouiCOM.Application, code As String)
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOFecLst")
                If blnexit Then
                    Exit Sub
                End If
                strPath = Application.StartupPath & "\SBOFechaLst.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)
                oForm = oApp.Forms.Item("SBOFecLst")
                PopulaGrid2(oApp, oForm, code)
                oForm.Visible = True
            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Public Shared Sub MarcarFilaParaEdicion(ByRef oform As SAPbouiCOM.Form, ItemUID As String, Row As Integer)
            Try
                oform.Freeze(True)
                Dim oGrid As SAPbouiCOM.Grid = oform.Items.Item(ItemUID).Specific


                oGrid.DataTable.SetValue("Change", Row, "Y")
                oform.Freeze(False)
            Catch ex As Exception
                oform.Freeze(False)
                Throw ex
            End Try
        End Sub
        Private Shared Function ActMasiva(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim grdLstt As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable

            Try
                oForm.Freeze(True)
                grdLstt = oForm.Items.Item("grdLstt").Specific
                oDT = grdLstt.DataTable
                For i As Integer = 0 To grdLstt.Rows.Count - 1
                    oDT.SetValue("Fecha Fin", i, oForm.DataSources.UserDataSources.Item("udFecM").ValueEx.ToString)
                    oDT.SetValue("Change", i, "Y")


                Next
                oForm.Freeze(False)
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function ModificaMaestroDiApi(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams

            Dim sCmp As SAPbobsCOM.CompanyService
            Dim grdLstt As SAPbouiCOM.Grid


            Try
                grdLstt = oForm.Items.Item("grdLstt").Specific

                For i As Integer = 0 To grdLstt.Rows.Count - 1

                    Dim Code As String
                    Dim Change As String
                    Dim fecha As String
                    Dim CodUsr As String
                    Dim NomUsr As String
                    Dim FechaAnt As String
                    Dim FechaFin As String
                    Dim FechaMod As String
                    Change = Trim(grdLstt.DataTable.Columns.Item("Change").Cells.Item(i).Value)
                    Code = Trim(grdLstt.DataTable.Columns.Item("Codigo").Cells.Item(i).Value)

                    If Trim(Change) = "Y" Then
                        CodUsr = Convert.ToString(DiApp.UserSignature)
                        NomUsr = Convert.ToString(DiApp.UserName)
                        FechaAnt = DatosAutorizaciones.ObtienFechaAnteriorLinCred(Code)
                        If FechaAnt = "" Then
                            FechaAnt = Trim(grdLstt.DataTable.Columns.Item("Fecha Fin").Cells.Item(i).Value)
                            FechaAnt = Convert.ToDateTime(FechaAnt).ToString("yyyyMMdd")
                        Else
                            FechaAnt = Convert.ToDateTime(FechaAnt).ToString("yyyyMMdd")
                        End If
                        FechaMod = DiApp.GetDBServerDate.ToString("yyyyMMdd")
                            fecha = Trim(grdLstt.DataTable.Columns.Item("Fecha Fin").Cells.Item(i).Value)
                            FechaFin = Convert.ToDateTime(fecha).ToString("yyyyMMdd")
                            Code = Trim(grdLstt.DataTable.Columns.Item("Codigo").Cells.Item(i).Value)
                        DatosAutorizaciones.InsertaModificacionLog("SBOFecLst", Code, CodUsr, NomUsr, FechaAnt, FechaFin, FechaMod) 'CNP20190710
                            If Trim(Code) <> "" Then
                                Try
                                    sCmp = DiApp.GetCompanyService
                                    oGeneralService = sCmp.GetGeneralService("LICRE")
                                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                                    oGeneralParams.SetProperty("Code", Code)
                                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                                    oGeneralData.SetProperty("U_FechaHa", fecha)
                                    oGeneralService.Update(oGeneralData)
                                    'DatosAutorizaciones.InsertaModificacionLog("SBOFecLst", DocEntry, CodUsr, NomUsr, FechaAnt, FechaFin, FechaMod)
                                Catch ex As Exception
                                    oApp.MessageBox(ex.Message)
                                    oApp.MessageBox("No se pudo modificar la linea de credito :  " & Code)
                                    Return False
                                End Try
                            End If
                        End If
                Next
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function ValidaMasivo(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Try
                Dim fecha As String = Nothing
                fecha = Trim(oForm.DataSources.UserDataSources.Item("udFecM").ValueEx)
                If fecha = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe ingresar una fecha antes de continuar")
                    Return False
                End If
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Sub RefrescaForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Dim oDT As DataTable = NegocioParam.GetManufacturersLst(Trim(DiApp.UserName))
            Dim oXml As New Xml.XmlDocument
            Try
                oForm.Freeze(True)
                oXml.InnerXml = oForm.GetAsXML()
                If Not oDT Is Nothing Then
                    If oDT.Rows.Count > 0 Then
                        'Se llena el grid con los valores del maestro
                        PopulaGrid(oApp, oForm)
                    Else
                        PopulaGrid(oApp, oForm)
                    End If
                Else
                    PopulaGrid(oApp, oForm)
                End If
                oForm.Items.Item("btnEdt").Enabled = True
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Private Shared Sub AbreGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Dim grdLstt As SAPbouiCOM.Grid
            Try
                oForm.Freeze(True)
                grdLstt = oForm.Items.Item("grdLstt").Specific
                grdLstt.Columns.Item(0).Visible = False 'LineId
                grdLstt.Columns.Item(0).Editable = False 'LineId

                grdLstt.Columns.Item("Codigo").Editable = False

                grdLstt.Columns.Item("Name").Editable = False
                grdLstt.Columns.Item("Name").Visible = False

                grdLstt.Columns.Item("Cliente").Editable = False

                grdLstt.Columns.Item("Fecha de Contrato").Editable = False

                grdLstt.Columns.Item("Interes").Editable = False

                grdLstt.Columns.Item("Fecha Inicio").Editable = False

                grdLstt.Columns.Item("Fecha Fin").Editable = True

                grdLstt.Columns.Item("U_Inactive").Editable = False
                grdLstt.Columns.Item("U_Inactive").Visible = False
                oForm.Freeze(False)
                oForm.Update()
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try
        End Sub

        Private Shared Sub PopulaGrid2(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Optional code As String = Nothing)

            Dim grdLstt As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim numLin As String = ""
            Dim query2 As String = String.Empty
            Dim cond As Boolean = False
            Dim EsImpMar As String = "N"
            Dim ImpMar As String = ""
            Dim rate As Double = 0
            Try
                oForm.Freeze(True)
                grdLstt = oForm.Items.Item("grdLstt").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("MAT_LSTT")
                If Motor = "SQL" Then
                    query2 = "select 'N' as Change, Code As 'Codigo',Name,U_Nombre As 'Cliente',U_FechaCon As 'Fecha de Contrato',U_Interes As 'Interes',U_FechaDe As 'Fecha Inicio',U_FechaHa As 'Fecha Fin',U_Inactive from [@EXX_LICRE] as A where A.U_Nombre ='" & code & "'"
                Else
                    query2 = "select 'N' as ""Change"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""Code"" As ""Codigo"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""Name"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Nombre"" As ""Cliente"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaCon"" As ""Fecha de Contrato"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Interes"" As ""Interes"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaDe"" As ""Fecha Inicio"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaHa"" As ""Fecha Fin"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Inactive"" from """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"" where """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Nombre"" = '" & code & "';"
                End If

                oDT.ExecuteQuery(query2)
                grdLstt.DataTable = oDT
                If grdLstt.Rows.Count > 0 Then
                    For i As Integer = 0 To grdLstt.Rows.Count - 1
                        numLin = oDT.GetValue("Codigo", i)
                        If Not String.IsNullOrEmpty(numLin) Then
                            grdLstt.RowHeaders.SetText(i, i + 1)
                        Else
                            grdLstt.RowHeaders.SetText(i, "")
                        End If
                    Next
                End If

                grdLstt.Columns.Item("Change").Editable = True
                grdLstt.Columns.Item("Change").Visible = False
                oColumns = grdLstt.Columns.Item("Codigo")
                grdLstt.Columns.Item("Codigo").Editable = False
                oColumns = grdLstt.Columns.Item("Name")
                grdLstt.Columns.Item("Name").Editable = False
                grdLstt.Columns.Item("Name").Visible = False
                oColumns = grdLstt.Columns.Item("Cliente")
                grdLstt.Columns.Item("Cliente").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha de Contrato")
                grdLstt.Columns.Item("Fecha de Contrato").Editable = False
                oColumns = grdLstt.Columns.Item("Interes")
                grdLstt.Columns.Item("Interes").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha Inicio")
                grdLstt.Columns.Item("Fecha Inicio").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha Fin")
                grdLstt.Columns.Item("Fecha Fin").Editable = False
                oColumns = grdLstt.Columns.Item("U_Inactive")
                grdLstt.Columns.Item("U_Inactive").Editable = False
                grdLstt.Columns.Item("U_Inactive").Visible = False
                grdLstt.AutoResizeColumns()
                oForm.Freeze(False)
                oForm.Update()
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Private Shared Sub PopulaGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Dim grdLstt As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim numLin As String = ""
            Dim query As String = String.Empty
            Dim cond As Boolean = False
            Dim EsImpMar As String = "N"
            Dim ImpMar As String = ""
            Dim rate As Double = 0
            Try
                oForm.Freeze(True)
                grdLstt = oForm.Items.Item("grdLstt").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("MAT_LSTT")
                If Motor = "SQL" Then
                    query = "select 'N' AS Change ,Code As 'Codigo',Name,U_Nombre As 'Cliente',U_FechaCon As 'Fecha de Contrato',U_Interes As 'Interes',U_FechaDe As 'Fecha Inicio',U_FechaHa As 'Fecha Fin',U_Inactive FROM [@EXX_LICRE]"
                Else
                    query = "select'N' AS ""Change"" ,""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""Code"" As ""Codigo"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""Name"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Nombre"" As ""Cliente"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaCon"" As ""Fecha de Contrato"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Interes"" As ""Interes"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaDe"" As ""Fecha Inicio"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_FechaHa"" As ""Fecha Fin"",""" & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"".""U_Inactive"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_LICRE"";"
                End If

                oDT.ExecuteQuery(query)
                grdLstt.DataTable = oDT
                If grdLstt.Rows.Count > 0 Then
                    For i As Integer = 0 To grdLstt.Rows.Count - 1
                        numLin = oDT.GetValue("Codigo", i).ToString
                        If Not String.IsNullOrEmpty(numLin) Then
                            grdLstt.RowHeaders.SetText(i, i + 1)
                        Else
                            grdLstt.RowHeaders.SetText(i, "")
                        End If
                    Next
                End If

                grdLstt.Columns.Item("Change").Editable = True
                grdLstt.Columns.Item("Change").Visible = False
                oColumns = grdLstt.Columns.Item("Codigo")
                grdLstt.Columns.Item("Codigo").Editable = False
                oColumns = grdLstt.Columns.Item("Name")
                grdLstt.Columns.Item("Name").Editable = False
                grdLstt.Columns.Item("Name").Visible = False
                oColumns = grdLstt.Columns.Item("Cliente")
                grdLstt.Columns.Item("Cliente").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha de Contrato")
                grdLstt.Columns.Item("Fecha de Contrato").Editable = False
                oColumns = grdLstt.Columns.Item("Interes")
                grdLstt.Columns.Item("Interes").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha Inicio")
                grdLstt.Columns.Item("Fecha Inicio").Editable = False
                oColumns = grdLstt.Columns.Item("Fecha Fin")
                grdLstt.Columns.Item("Fecha Fin").Editable = False
                'grdLstt.Columns.Item("Fecha Fin").TextStyle.ToString()

                oColumns = grdLstt.Columns.Item("U_Inactive")
                grdLstt.Columns.Item("U_Inactive").Editable = False
                grdLstt.Columns.Item("U_Inactive").Visible = False
                grdLstt.AutoResizeColumns()
                oForm.Freeze(False)
                oForm.Update()
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Public Shared Sub loadMarcas(ByRef oDocXml As Xml.XmlDocument, Optional usuario As String = "")
            Try

                Dim root As Xml.XmlNode = oDocXml.DocumentElement
                Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[2]/specific/ValidValues/action")
                Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()
                Dim oDatatable As DataTable

                If usuario = "" Then
                    oDatatable = NegocioParam.GetManufacturersLst()
                Else
                    oDatatable = NegocioParam.GetManufacturersLst(usuario)
                End If

                If Not oDatatable Is Nothing Then
                    If oDatatable.Rows.Count > 0 Then
                        For Each row As DataRow In oDatatable.Rows
                            xfrag.InnerXml = "<ValidValue value='" & Replace(Convert.ToString(row(0)), "<", "") & "' description='" & Replace(Convert.ToString(row(1)), "<", "") & "'/>"
                            aNode.AppendChild(xfrag)
                        Next
                    Else
                        oDatatable.Clear()
                        oDatatable = NegocioParam.GetManufacturersLst()
                        For Each row As DataRow In oDatatable.Rows
                            xfrag.InnerXml = "<ValidValue value='" & Replace(Convert.ToString(row(0)), "<", "") & "' description='" & Replace(Convert.ToString(row(1)), "<", "") & "'/>"
                            aNode.AppendChild(xfrag)
                        Next
                    End If
                Else
                    oDatatable.Clear()
                    oDatatable = NegocioParam.GetManufacturersLst()
                    For Each row As DataRow In oDatatable.Rows
                        xfrag.InnerXml = "<ValidValue value='" & Replace(Convert.ToString(row(0)), "<", "") & "' description='" & Replace(Convert.ToString(row(1)), "<", "") & "'/>"
                        aNode.AppendChild(xfrag)
                    Next
                End If


                oDatatable = Nothing
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try

        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Public Shared Sub LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Try
                Dim oDocXml As New Xml.XmlDocument
                Dim strPath As String
                oForm.Freeze(True)
                'Se carga el xml de combos para la lista de precios
                strPath = Application.StartupPath & "\LSTCombos.xml" 'se pasa la ruta del formulario
                oDocXml.Load(strPath)

                'se cargan las marcas
                loadMarcas(oDocXml, Trim(DiApp.UserName))



                'se refresca el formulario
                oApp.LoadBatchActions(oDocXml.InnerXml)
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try

        End Sub

        Public Shared Function ChooseFromListt(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "CardCode"
                    bBubbleEvent = SNChooseFromListt(FormUID, oEvent, oApplication, oCompany)

                Case Else
            End Select

            Return bBubbleEvent
        End Function

        Public Shared Function SNChooseFromListt(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean

            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim edtEmp As SAPbouiCOM.EditText
                    Dim edtEmpN As SAPbouiCOM.EditText

                    oForm = oApplication.Forms.Item(FormUID)
                    'oForm.Freeze(True)
                    edtEmp = oForm.Items.Item("CardCode").Specific
                    edtEmpN = oForm.Items.Item("Nombre").Specific
                    oDataTable = Utiles.Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            edtEmpN.Value = oDataTable.GetValue("CardName", 0)
                            edtEmp.Value = oDataTable.GetValue("CardCode", 0)
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'oForm.Freeze(False)
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function

#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDOFECLT(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "CardCode"
                                    Try
                                        ChooseFromListt(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        PopulaGrid(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "grdLstt"
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                    If pVal.Row >= 0 Then

                                        If pVal.ItemChanged = True Then
                                            oForm.Freeze(True)
                                            MarcarFilaParaEdicion(oForm, pVal.ItemUID, pVal.Row)
                                            oForm.Freeze(False)
                                        End If
                                    End If
                            End Select
                        Else
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "btnActt"
                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        oForm.Freeze(True)
                                        If Not SBOFormFechaLst.ValidaMasivo(oForm, oApp) Then
                                            oForm.Freeze(False)
                                            oApp.MessageBox("Ocurrió un error al intentar cambiar masivamente la grilla")
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                        oForm.Freeze(False) 'CNP20190710
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        oForm.Freeze(True)
                                        If Not ActMasiva(oApp, oForm) Then
                                            oForm.Freeze(True)
                                            oApp.MessageBox("Ocurrió un error al intentar cambiar masivamente la grilla")
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                        oForm.Freeze(False)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnRefr"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                        Try
                                            oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                            RefrescaForm(oApp, oForm)
                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            oApp.MessageBox(ex.Message)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try
                                    Else
                                        oForm.Freeze(False)
                                        oApp.MessageBox("Debe actualizar la información o cerrar el formulario sin guardarlo")
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                Case "btnFilt"
                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        Dim code As String = Nothing
                                        code = Trim(oDBCab.GetValue("U_Nombre", 0))
                                        If code = "" Then
                                            code = Nothing
                                        End If
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        PopulaGrid2(oApp, oForm, code)
                                        oForm.Items.Item("btnEdt").Enabled = True
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnEdt"
                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        Dim btnActualizarr As SAPbouiCOM.Button
                                        btnActualizarr = oForm.Items.Item("btnActt").Specific
                                        btnActualizarr.Item.Enabled = True
                                        AbreGrid(oApp, oForm)
                                        oForm.Items.Item("btnEdt").Enabled = False
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "1"
                                    oForm.Items.Item("btnActt").Enabled = False
                                    Dim grdLstt As SAPbouiCOM.Grid
                                    Dim Code As String
                                    Code = Trim(oDBCab.GetValue("U_Nombre", 0))
                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual

                                        Dim oDT As SAPbouiCOM.DataTable
                                        oForm.Freeze(True)
                                        grdLstt = oForm.Items.Item("grdLstt").Specific
                                        oDT = grdLstt.DataTable
                                        ModificaMaestroDiApi(oApp, oForm)
                                        'grdLstt.Columns.Item("Fecha Fin").Editable = False
                                        If Code = "" Then
                                            RefrescaForm(oApp, oForm)
                                        Else
                                            PopulaGrid2(oApp, oForm, Code)
                                        End If

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                    oForm.Items.Item("btnEdt").Enabled = True

                                    'grdLstt.Columns.Item("Fecha Fin").Editable = False
                            End Select
                        Else
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub


#End Region

#Region "VALIDACIONES"

#End Region

    End Class
End Namespace





﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormMotRec
#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, Optional code As String = "", Optional nombre As String = "", Optional DocEntry As String = "", Optional Presupuesto As String = "", Optional Correlativo As String = "", Optional CodeEstado As String = "", Optional DescrEstado As String = "", Optional CodeAccion As String = "", Optional DescrAccion As String = "", Optional etapa As String = "", Optional CodUsr As String = "", Optional NomUsr As String = "", Optional fecha As String = "", Optional CorreSol As String = "")
            Try

                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOMotRec")
                If blnexit Then
                    Exit Sub
                End If
                strPath = Application.StartupPath & "\SBOMotRec.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)

                oForm = oApp.Forms.Item("SBOMotRec")

                Dim oDBCOD As SAPbouiCOM.UserDataSource
                oDBCOD = oForm.DataSources.UserDataSources.Item("udCOD")
                Dim oDBNOM As SAPbouiCOM.UserDataSource
                oDBNOM = oForm.DataSources.UserDataSources.Item("udNOM")



                Popula2(oApp, oForm, code, nombre)
                Try
                    oForm.DataSources.UserDataSources.Item("udDE").ValueEx = code 'CNP20190710
                    oForm.DataSources.UserDataSources.Item("udPR").ValueEx = Presupuesto
                    oForm.DataSources.UserDataSources.Item("udCO").ValueEx = Correlativo
                    oForm.DataSources.UserDataSources.Item("udCE").ValueEx = CodeEstado
                    oForm.DataSources.UserDataSources.Item("udDS").ValueEx = DescrEstado
                    oForm.DataSources.UserDataSources.Item("udCA").ValueEx = CodeAccion
                    oForm.DataSources.UserDataSources.Item("udDA").ValueEx = DescrAccion
                    oForm.DataSources.UserDataSources.Item("udET").ValueEx = etapa
                    oForm.DataSources.UserDataSources.Item("udCU").ValueEx = CodUsr
                    oForm.DataSources.UserDataSources.Item("udNU").ValueEx = NomUsr
                    oForm.DataSources.UserDataSources.Item("udFE").ValueEx = fecha
                    oForm.DataSources.UserDataSources.Item("udCS").ValueEx = CorreSol

                Catch ex As Exception
                    oApp.MessageBox(ex.Message)
                End Try

                oForm.Visible = True
            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub
        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function
        Private Shared Sub Popula2(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Optional code As String = Nothing, Optional nombr As String = Nothing)


            Dim CardCode As SAPbouiCOM.EditText
            Dim Nombre As SAPbouiCOM.EditText
            Try
                oForm.Freeze(True)

                CardCode = oForm.Items.Item("CardCode").Specific
                CardCode.Value = code
                Nombre = oForm.Items.Item("U_Nombre").Specific
                Nombre.Value = nombr
                oForm.Freeze(False)
                oForm.Update()
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub
#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDO(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            ' Dim oDBCab As SAPbouiCOM.DBDataSource
            oForm = oApp.Forms.Item("SBOMotRec")
            Try
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                            End Select
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                            End Select
                        Else
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                        If pVal.Before_Action = True Then
                            Select Case pVal.ItemUID

                                Case "1_1"
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                    oForm.Freeze(True)
                                    'Dim fecha As String = Nothing
                                    Dim DocEntry As String = Nothing
                                    Dim Presupuesto As String = Nothing
                                    Dim Correlativo As String = Nothing
                                    Dim CodeEstado As String = Nothing
                                    Dim DescrEstado As String = Nothing
                                    Dim CodeAccion As String = Nothing
                                    Dim DescrAccion As String = Nothing
                                    Dim etapa As String = Nothing
                                    Dim CodUsr As String = Nothing
                                    Dim NomUsr As String = Nothing
                                    Dim Fecha As String = Nothing
                                    Dim CorreSol As String = Nothing
                                    Dim motivo As String
                                    DocEntry = Trim(oForm.DataSources.UserDataSources.Item("udDE").ValueEx)
                                    Presupuesto = Trim(oForm.DataSources.UserDataSources.Item("udPR").ValueEx)
                                    Correlativo = Trim(oForm.DataSources.UserDataSources.Item("udCO").ValueEx)
                                    CodeEstado = Trim(oForm.DataSources.UserDataSources.Item("udCE").ValueEx)
                                    DescrEstado = Trim(oForm.DataSources.UserDataSources.Item("udDS").ValueEx)
                                    CodeAccion = Trim(oForm.DataSources.UserDataSources.Item("udCA").ValueEx)
                                    DescrAccion = Trim(oForm.DataSources.UserDataSources.Item("udDA").ValueEx)
                                    etapa = Trim(oForm.DataSources.UserDataSources.Item("udET").ValueEx)
                                    CodUsr = Trim(oForm.DataSources.UserDataSources.Item("udCU").ValueEx)
                                    NomUsr = Trim(oForm.DataSources.UserDataSources.Item("udNU").ValueEx)
                                    Fecha = Trim(oForm.DataSources.UserDataSources.Item("udFE").ValueEx)
                                    CorreSol = Trim(oForm.DataSources.UserDataSources.Item("udCS").ValueEx)
                                    motivo = Trim(oForm.DataSources.UserDataSources.Item("udMR").ValueEx)
                                    Try

                                        DatosAutorizaciones.InsertaAutorizacionLog("LICRE", DocEntry, Presupuesto, Correlativo, CodeEstado, DescrEstado, CodeAccion, DescrAccion, etapa, CodUsr, NomUsr, Fecha, CorreSol, motivo)

                                        oForm.Freeze(True)
                                        oApp.MessageBox("Se ha registrado el motivo")
                                        oForm.Close()

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                            End Select
                        Else
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub


#End Region

#Region "VALIDACIONES"

#End Region

    End Class
End Namespace





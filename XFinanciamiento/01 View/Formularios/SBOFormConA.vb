﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormConA

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "AUTCOND", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)

                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm

                LoadCombos(oApp, oForm)


            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Private Shared Function ManejaMatrizCond(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, index As Integer, checkselected As Boolean) As Boolean
            Dim oMat As SAPbouiCOM.Matrix
            Dim oCol As SAPbouiCOM.Column
            Dim oRowCtrl As SAPbouiCOM.CommonSetting


            Try
                oMat = oForm.Items.Item("0_U_G").Specific

                oRowCtrl = oMat.CommonSetting()

                If checkselected = True Then
                    oRowCtrl.SetCellEditable(index, 5, True)
                    oRowCtrl.SetCellEditable(index, 4, False)
                Else
                    oRowCtrl.SetCellEditable(index, 5, False)
                    oRowCtrl.SetCellEditable(index, 4, True)
                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try


        End Function


        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Private Shared Function LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim formulario As String = ""


            Try
                oForm.Freeze(True)

                'FORMULARIOS
                oCombo = oForm.Items.Item("cmbForm").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oCombo.Item.AffectsFormMode = True

                If Not PopulaForms(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de formularios")
                    oForm.Freeze(True)
                End If

                oCombo = oForm.Items.Item("cmbFunc").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oCombo.Item.AffectsFormMode = True

                If Not PopulaFunciones(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de formularios")
                    oForm.Freeze(True)
                End If


                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Private Shared Function PopulaFunciones(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean
            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosAutorizaciones.GetFuncionesTabla()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("name")
                                desc = row.Item("name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar las funciones de tabla para condiciones.")
                            Return False

                        End If

                    End If

                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar las funciones de tabla para condiciones.")
                    Return False
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function PopulaForms(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosAutorizaciones.GetForms()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los formularios.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los formularios.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Private Shared Function VaciaValidCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oMat As SAPbouiCOM.Matrix
            Dim oCol As SAPbouiCOM.Column

            Try

                oForm.Freeze(True)
                oMat = oForm.Items.Item("0_U_G").Specific


                oCombo = oForm.Items.Item("cmbForm").Specific
                If oCombo.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCombo.ValidValues.Count - 1
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If

                oCombo = oForm.Items.Item("cmbFunc").Specific
                If oCombo.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCombo.ValidValues.Count - 1
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If


                oCol = oMat.Columns.Item("C_0_1")
                If oCol.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCol.ValidValues.Count - 1
                        oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If

                oCol = oMat.Columns.Item("C_0_5")
                If oCol.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCol.ValidValues.Count - 1
                        oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If

                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try



        End Function

        Public Shared Function PopulaCamposCond(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, CodFun As String, oCol As SAPbouiCOM.Column) As Boolean

            Dim oDT As DataTable
            Dim cod As String
            Dim des As String

            Try
                oDT = DatosAutorizaciones.GetFunQuery(CodFun)
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCol.ValidValues.Count > 0 Then
                        If oDT.Columns.Count > 0 Then
                            'Primero se limpian los valores del combo
                            For i As Integer = 0 To oCol.ValidValues.Count - 1
                                oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next

                            For Each colDT As DataColumn In oDT.Columns
                                cod = colDT.ColumnName()
                                oCol.ValidValues.Add(cod, cod)
                            Next

                            oForm.Freeze(False)
                            Return True
                        Else
                            For i As Integer = 0 To oCol.ValidValues.Count - 1
                                oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar las funciones de tablas.")
                            Return False

                        End If
                    Else
                        If oDT.Columns.Count > 0 Then
                            For Each colDT As DataColumn In oDT.Columns
                                cod = colDT.ColumnName()
                                oCol.ValidValues.Add(cod, cod)
                            Next


                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar las funciones de tablas.")
                            Return False
                        End If
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar las funciones de tablas.")
                    Return False
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function



#End Region

#Region "EVENTOS"

        Public Shared Sub AdminSBOFormUDOConANav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)

            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim oCol As SAPbouiCOM.Column

            Try

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOCON")
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOCOL")
                oMat = oForm.Items.Item("0_U_G").Specific

                If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then





                    Else
                        Try
                            VaciaValidCombos(oApp, oForm)

                        Catch ex As Exception
                            oForm.Freeze(False)
                            bubbleEvent = False
                            GoTo fail
                        End Try
                    End If


                Else
                    Try
                        VaciaValidCombos(oApp, oForm)

                    Catch ex As Exception
                        oForm.Freeze(False)
                        bubbleEvent = False
                        GoTo fail
                    End Try
                End If


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub

        Public Shared Sub AdminSBOFormConA(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOCON")

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "cmbFunc"
                                    Dim oMat As SAPbouiCOM.Matrix
                                    Dim oCol As SAPbouiCOM.Column
                                    Dim CodFun As String = String.Empty

                                    Try

                                        CodFun = Trim(oDBCab.GetValue("U_Funcion", 0))

                                        oMat = oForm.Items.Item("0_U_G").Specific
                                        oCol = oMat.Columns.Item("C_0_1")
                                        oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                        Try
                                            PopulaCamposCond(oApp, oForm, CodFun, oCol)

                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            oApp.MessageBox(ex.Message)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try

                                        oCol = oMat.Columns.Item("C_0_5")
                                        oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                        Try
                                            PopulaCamposCond(oApp, oForm, CodFun, oCol)

                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            oApp.MessageBox(ex.Message)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try


                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try



                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "0_U_G"

                                    Select Case pVal.ColUID

                                        Case "C_0_4"
                                            Dim checkedVal As Boolean
                                            Dim oMat As SAPbouiCOM.Matrix
                                            Dim oChkCol As SAPbouiCOM.CheckBox

                                            oMat = oForm.Items.Item("0_U_G").Specific
                                            oChkCol = oMat.Columns.Item("C_0_4").Cells.Item(pVal.Row).Specific

                                            checkedVal = oChkCol.Checked()

                                            If Not ManejaMatrizCond(oApp, oForm, pVal.Row, checkedVal) Then
                                                oForm.Freeze(False)
                                                oApp.MessageBox("error")
                                                BubbleEvent = False
                                                GoTo fail

                                            End If



                                    End Select


                            End Select



                        End If




                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#End Region

#Region "VALIDACIONES"

#End Region
    End Class
End Namespace



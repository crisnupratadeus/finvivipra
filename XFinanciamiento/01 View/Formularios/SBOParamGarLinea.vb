﻿Imports System.Text
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data
Imports System.Xml
Namespace View
    Public Class FormGarLinea
        Shared oDTListaGarantia As System.Data.DataTable
        Public Shared oPublicForm As SAPbouiCOM.Form

#Region "METODOS"
        Private Shared Function insertaDS(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef NumDoc As String) As Boolean

            Dim nombre As SAPbouiCOM.StaticText
            Dim Plantilla As String = String.Empty
            Dim CodigoLC As String = String.Empty
            Dim CodigoPC As String = String.Empty
            Dim valor As String = String.Empty

            Try
                CodigoLC = oForm.DataSources.UserDataSources.Item("UD_LC").ValueEx
                CodigoPC = oForm.DataSources.UserDataSources.Item("UD_PC").ValueEx
                NumDoc = oForm.DataSources.UserDataSources.Item("UD_ND").ValueEx
                Dim oDatatable As DataTable = BDGarantia.getListaFipla1(NumDoc, CodigoPC)


                For i As Integer = 0 To oDatatable.Rows.Count - 1
                    nombre = oForm.Items.Item("lbl_" & i & "").Specific
                    Dim Obligatorio As String = oDatatable.Rows.Item(i).Item("U_Oblig").ToString
                    If esObligatorio(oForm, i, Obligatorio) = True Then
                        oApp.MessageBox("El campo: """ & nombre.Caption & """ es esta definido como obligatorio, debe tener algun valor.")
                        Return False
                        Exit Function
                    Else
                        Dim Tipo As String = Trim(oDatatable.Rows.Item(i).Item("U_TipoC").ToString)
                        If esTipo(oForm, i, Tipo) = True Then
                            oApp.MessageBox("El campo: """ & nombre.Caption & """ debe ser numerico.")
                            Return False
                            Exit Function
                        End If
                    End If
                Next

                For i As Integer = 0 To oDatatable.Rows.Count - 1
                    Plantilla = oForm.DataSources.UserDataSources.Item("UD_C" & i + 1 & "").ValueEx
                    valor = oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx
                    BDGarantia.insertaDetalleGarFIPLD1(Plantilla, valor, CodigoLC, CodigoPC, "-1")
                Next

                Return True



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function esTipo(ByRef oForm As SAPbouiCOM.Form, ByRef i As Integer, Tipo As String) As Boolean

            Dim j As Integer
            Dim chars As String = String.Empty
            Dim valor As String = String.Empty
            valor = oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx

            If Tipo = "B" Then
                Exit Function
            End If

            If Tipo = "N" And valor <> "" Then
                For j = 1 To Len(valor)
                    chars = Mid(valor, j, 1)
                    If Not IsNumeric(chars) Then
                        Return True
                        Exit Function
                    End If
                Next j
            ElseIf Tipo = "A" Then
                Exit Function
            Else
                oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx = 0
            End If


        End Function

        Private Shared Function esObligatorio(ByRef oForm As SAPbouiCOM.Form, ByRef i As Integer, ByRef Obligatorio As String) As Boolean
            Dim Valor As String = String.Empty

            Valor = oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx


            If Valor = "" And Obligatorio = "Y" Then
                Return True
            End If

        End Function

        Public Shared Sub CreaSBOForm(ByRef oApp As SAPbouiCOM.Application, ByRef CodeLC As String, ByRef CodigoPC As String, ByRef NumDoc As String)
            Try
                'oForm.Freeze(True)                
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String

                Dim blnexit As Boolean = RecorreFormulario(oApp, "FormGarLinea") 'verifica que el formulario no se encuentre abierto. 

                If blnexit Then
                    Exit Sub 'si el fomul                                                                                                                                                                                                               ario se encuentra abierto cierra este sub
                End If

                strPath = Application.StartupPath & "\FormGarLinea.xml" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario             

                Dim root As Xml.XmlNode = xmlDoc.DocumentElement
                Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/@uid")
                Dim oDatatable As DataTable = BDGarantia.getListaFipla1(NumDoc, CodigoPC)

                For i As Integer = 0 To oDatatable.Rows.Count - 1
                    Dim value As String = oDatatable.Rows.Item(i).Item("U_Descr").ToString
                    Dim tipo As String = oDatatable.Rows.Item(i).Item("U_TipoC").ToString
                    llenaCampos(xmlDoc, i + 1, value)
                    tipoCampos(xmlDoc, i + 1, tipo)
                    If i + 1 = oDatatable.Rows.Count Then
                        editaCampos(xmlDoc, oDatatable.Rows.Count)
                    End If
                Next
                oApp.LoadBatchActions(xmlDoc.InnerXml)
                oForm = oApp.Forms.Item("FormGarLinea") 'se asigna el fomulario al objeto form
                llenaDS(oApp, oForm, oDatatable, CodeLC, CodigoPC, NumDoc)

                'oForm.Freeze(False)
                oForm.Visible = True

            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Visible = True
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try

        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Public Shared Sub llenaCampos(ByRef oDocXml As Xml.XmlDocument, ByRef i As Integer, ByRef value As String)

            Dim root As Xml.XmlNode = oDocXml.DocumentElement
            Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & i & "]")
            Dim ver As String = aNode.Name
            Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()

            xfrag.InnerXml = "<specific caption=""" & value & """ />"
            'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
            aNode.AppendChild(xfrag)

        End Sub

        Private Shared Sub tipoCampos(ByRef oDocXml As XmlDocument, ByRef i As Integer, ByRef tipo As String)

            Dim campos As Integer = 20 + 3

            Dim root As Xml.XmlNode = oDocXml.DocumentElement
            Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & campos + i & "]")
            Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()
            If tipo = "B" Then
                xfrag.InnerXml = "<specific visible=""0"" enable=""0"" />"
                'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
                aNode.AppendChild(xfrag)
            Else
                Dim nuevos2 As Integer = 42 + 3
                Dim aNodeC As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & nuevos2 + i - 1 & "]")
                xfrag.InnerXml = "<specific visible=""0"" enable=""0"" />"
                'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
                aNodeC.AppendChild(xfrag)
            End If
        End Sub

        Private Shared Sub editaCampos(ByRef oDocXml As Xml.XmlDocument, ByRef count As Integer)

            For i As Integer = count To 20

                Dim root As Xml.XmlNode = oDocXml.DocumentElement
                Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & i + 1 & "]")
                Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()

                xfrag.InnerXml = "<specific caption="""" />"
                'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
                aNode.AppendChild(xfrag)

            Next

            Dim nuevos1 As Integer = 20 + 3 + count
            For i As Integer = nuevos1 To 43

                Dim root As Xml.XmlNode = oDocXml.DocumentElement
                Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & i + 1 & "]")
                Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()

                xfrag.InnerXml = "<specific visible=""0"" />"
                '    'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
                aNode.AppendChild(xfrag)

            Next

            Dim nuevos2 As Integer = 42 + 3 + count
            For i As Integer = nuevos2 To 65

                Dim root As Xml.XmlNode = oDocXml.DocumentElement
                Dim aNode As Xml.XmlNode = root.SelectSingleNode("/Application/forms/action/form/items/action/item[" & i & "]")
                Dim xfrag As Xml.XmlDocumentFragment = oDocXml.CreateDocumentFragment()

                xfrag.InnerXml = "<specific visible=""0"" />"
                '    'xfrag.InnerXml = "<item top=""104"" left=""6"" width=""121"" height=""14"" AffectsFormMode=""1"" disp_desc=""0"" enabled=""1"" from_pane=""0"" to_pane=""0"" right_just=""0"" type=""8"" visible=""1"" uid=""Item_8"" IsAutoGenerated=""0""><specific caption=""Sucursal""/></item>"
                aNode.AppendChild(xfrag)

            Next

        End Sub

        Private Shared Sub llenaDS(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef oDatatable As DataTable, ByRef CodeLC As String, ByRef CodigoPC As String, ByRef NumDoc As String)
            Try
                oForm.Freeze(True)

                oForm.DataSources.UserDataSources.Item("UD_LC").ValueEx = CodeLC
                oForm.DataSources.UserDataSources.Item("UD_PC").ValueEx = CodigoPC
                oForm.DataSources.UserDataSources.Item("UD_ND").ValueEx = NumDoc

                For i As Integer = 0 To oDatatable.Rows.Count - 1
                    oForm.DataSources.UserDataSources.Item("UD_C" & i + 1 & "").ValueEx = oDatatable.Rows.Item(i).Item("U_Fipla1_codigo")
                Next

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try
        End Sub
        Private Shared Function ActualizarFomDinamico(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef CodeLC As String, ByRef CodigoPC As String, ByRef NumDoc As String) As Boolean
            Dim nombre As SAPbouiCOM.StaticText
            CodigoPC = oForm.DataSources.UserDataSources.Item("UD_PC").ValueEx
            NumDoc = oForm.DataSources.UserDataSources.Item("UD_ND").ValueEx
            Dim oDatatable As DataTable = BDGarantia.getListaFipla1(NumDoc, CodigoPC)
            Dim otdDatos As DataTable = NegocioParam.GetFilesGar(CodeLC, CodigoPC, NumDoc)
            Try

                For i As Integer = 0 To otdDatos.Rows.Count - 1
                    If oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx <> otdDatos.Rows.Item(i).Item("U_Valor") Then
                        nombre = oForm.Items.Item("lbl_" & i & "").Specific
                        Dim Obligatorio As String = oDatatable.Rows.Item(i).Item("U_Oblig").ToString

                        If esObligatorio(oForm, i, Obligatorio) = True Then
                            oApp.MessageBox("El campo: """ & nombre.Caption & """ es esta definido como obligatorio, debe tener algun valor.")
                            Return False
                            Exit Function
                        Else
                            Dim Tipo As String = Trim(oDataTable.Rows.Item(i).Item("U_TipoC").ToString)
                            If esTipo(oForm, i, Tipo) = True Then
                                oApp.MessageBox("El campo: """ & nombre.Caption & """ debe ser numerico.")
                                Return False
                                Exit Function
                            End If
                        End If

                        Dim Valor As String = oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx
                        Dim Codigo As String = otdDatos.Rows.Item(i).Item("U_Codigo").ToString
                        If Not DatosDB.UpdateFormDinamico(CodeLC, CodigoPC, Valor, Codigo) Then
                            Return False
                        End If
                    End If
                Next
                Return True
                'For i As Integer = 0 To otdDatos.Rows.Count - 1
                '    If oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx <> otdDatos.Rows.Item(i).Item("U_Valor") Then
                '        Dim Valor As String = oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx
                '        Dim Codigo As String = otdDatos.Rows.Item(i).Item("U_Codigo").ToString

                '    End If
                'Next
                'Return True
            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function
#End Region

#Region "EVENTOS"
        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oForm As SAPbouiCOM.Form

                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.BeforeAction = False Then
                            Select Case pVal.ItemUID
                                Case "btnSalir"
                                    oForm.Close()
                                    BubbleEvent = False
                                    Exit Sub
                                Case "btnGuardar"

                                    Select Case oForm.Mode
                                        Case SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                            Try
                                                Dim NumDoc As String = oForm.DataSources.UserDataSources.Item("UD_ND").ValueEx
                                                Dim CodeLC As String = oForm.DataSources.UserDataSources.Item("UD_LC").ValueEx
                                                Dim CodigoPC As String = oForm.DataSources.UserDataSources.Item("UD_PC").ValueEx

                                                If SBOFormLineCredito.verificaEstado(CodeLC, CodigoPC, NumDoc) = True Then
                                                    If Not ActualizarFomDinamico(oApp, oForm, CodeLC, CodigoPC, NumDoc) Then
                                                        oApp.StatusBar.SetText("[FN] Error al actualizar", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                    Else
                                                        oApp.StatusBar.SetText("[FN] Exito al actualizar", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                                    End If
                                                Else
                                                    If Not insertaDS(oApp, oForm, NumDoc) Then
                                                        oApp.StatusBar.SetText("[FN] Error al grabar", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                    Else
                                                        oApp.StatusBar.SetText("[FN] Exito al grabar", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                                    End If
                                                End If

                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                oApp.MessageBox(ex.Message)
                                                BubbleEvent = False
                                                GoTo fail
                                            End Try
                                    End Select
                            End Select
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception

                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBO Error:" & ex.ToString)

            End Try
        End Sub
#End Region

    End Class
End Namespace
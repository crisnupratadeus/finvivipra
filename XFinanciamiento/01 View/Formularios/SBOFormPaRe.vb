﻿Imports XFinanciamiento.Principal
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data
Imports System.Globalization

Namespace View
    Public Class SBOFormPaRe
        Public Shared miDataTable As New DataTable


        Public Shared dtFacturas As New DataTable
        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)


            Try
                Dim oForm As SAPbouiCOM.Form = oApp.Forms.Item(pVal.FormUID.ToString())

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            ' Select Case pVal.ItemUID
                            'Case "CodUd"
                            If pVal.ItemUID = "CodUd" Then

                                Try
                                    ChooseFromListt(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                    'PopulaGrid(oApp, oForm)
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try
                                'End Select
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD

                        If pVal.Before_Action = False Then
                            Try
                                Dim oFrm As SAPbouiCOM.Form
                                oFrm = oApp.Forms.Item(pVal.FormUID.ToString())
                                Util.CreaBotonEnFormularioSap(oFrm, "BtnCInt", "2", 0, 80, 100, 18, True, "Cálculo de Interes", 0)
                                'añadido por Mick 
                                Util.CreaLabelEnFormularioSap(oFrm, "lbCodU", "53", 20, 0, 80, 14, True, "Buscar Codigo", 0)
                                'añadido por Mick 
                                'Util.CreaEdtTextEnFormularioSap(oFrm, "CodUd", "lbCodU", 0, 127, 91, 14, True, "", 0)
                                Util.CreaBotonEnFormularioSap(oFrm, "btnSInt", "lbCodU", 0, 127, 20, 14, True, "...", 0)
                            Catch ex As Exception
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.Before_Action = False Then
                            If pVal.ItemUID = "BtnCInt" Then
                                Dim code As String = Nothing
                                Try
                                    Dim cardCodeText As SAPbouiCOM.EditText
                                    cardCodeText = oForm.Items.Item("5").Specific
                                    'Obtener las filas marcadas para calculo de multa.
                                    Dim CodeUsr As String = BDLineaCredito.ObtenerCodeLiCre(cardCodeText.Value.ToString())

                                    If CodeUsr = "" Then
                                        oApp.MessageBox("El registro no cuenta con una línea de crédito para el cálculo de los intereses.")
                                        'Return
                                    End If
                                    ObtenerCamposGrilla(oApp, "20", oForm)
                                    Dim totalCuota As Decimal = 0
                                    Dim totalCuotaText As SAPbouiCOM.EditText
                                    totalCuotaText = oForm.Items.Item("12").Specific
                                    Dim totalCuotaStr As String = totalCuotaText.Value.ToString().Replace("CLP ", "").Replace("BOB ", "").Replace("BS ", "").Replace("UF ", "").Replace("USD ", "").Replace("SOL ", "")
                                    totalCuota = DatosDB.ParseDecimalFormat(totalCuotaStr)
                                    SBOFormCalculoInteres.CreaSBOForm(oApp, cardCodeText.Value.ToString(), dtFacturas, totalCuota)
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try
                            End If
                            If pVal.ItemUID = "btnSInt" Then
                                Dim cardCodeText As SAPbouiCOM.EditText
                                Dim cardNameText As SAPbouiCOM.EditText
                                Dim codigo As String
                                Dim nombre As String
                                cardCodeText = oForm.Items.Item("5").Specific
                                cardNameText = oForm.Items.Item("32").Specific
                                codigo = cardCodeText.Value.ToString()
                                nombre = cardNameText.Value.ToString()
                                SBOSelectInteres.CreaSBOForm(oApp, codigo, nombre)
                            End If

                        End If
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        BubbleEvent = True
                        Dim oFormMTP As SAPbouiCOM.Form
                        Dim formularioSN As String

                        oFormMTP = Nothing
                        Try
                            oFormMTP = oApp.Forms.Item(FormUID)
                        Catch ex As Exception
                            oFormMTP = Nothing
                            oApp.MessageBox(ex.Message)

                        End Try
                        Try

                            If oFormMTP.Type = 170 Then ' FORMULARIO DEL PAGO RECIBIDO
                                formularioSN = "F_PR"
                            Else
                                formularioSN = ""
                            End If

                            If formularioSN = "F_PR" Then
                                Dim oFormMSJ As SAPbouiCOM.Form
                                Dim Button1 As SAPbouiCOM.Button
                                Dim Button2 As SAPbouiCOM.Button
                                oFormMSJ = oApp.Forms.Item(FormUID)
                                Try
                                    Try
                                        Button1 = oFormMSJ.Items.Item("1").Specific
                                        Button2 = oFormMSJ.Items.Item("2").Specific

                                        If ((Button1.Caption.Substring(1, 1) = "S") And ((Button2.Caption.Substring(1, 1) = "N"))) Then

                                            formularioSN = ""
                                            Button2.Item.Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                                        End If
                                    Catch ex As Exception
                                        Button1 = Nothing
                                        Button2 = Nothing
                                        oFormMSJ = Nothing
                                        oApp.MessageBox(ex.Message)
                                    End Try
                                Catch ex As Exception
                                    Button1 = Nothing
                                    Button2 = Nothing
                                    oFormMSJ = Nothing
                                    oApp.MessageBox(ex.Message)
                                End Try
                            End If


                        Catch ex As Exception
                            oApp.MessageBox(ex.Message)
                        End Try

                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBO Error:" & ex.ToString)
            End Try


        End Sub

        Private Shared Sub ObtenerCamposGrilla(ByRef oApp As SAPbouiCOM.Application, ByVal mtx As String, ByVal oform As SAPbouiCOM.Form)
            Try
                Dim interesDouble As Decimal = 0
                Dim interesMoraDouble As Decimal = 0
                Dim cardCodeText As SAPbouiCOM.EditText
                Dim oItemmatrix As SAPbouiCOM.Item = oform.Items.Item(mtx)
                Dim oMatrix As SAPbouiCOM.Matrix = (CType((oItemmatrix.Specific), SAPbouiCOM.Matrix))

                dtFacturas.Clear()
                dtFacturas.Columns.Clear()
                'Creo mi datatable y columnas
                dtFacturas.Columns.Add("DocEntry")
                dtFacturas.Columns.Add("DocNum")
                dtFacturas.Columns.Add("CardCode")
                dtFacturas.Columns.Add("CANCELED")
                dtFacturas.Columns.Add("DocStatus")
                dtFacturas.Columns.Add("U_DocCuota")
                dtFacturas.Columns.Add("U_CuotFi")
                dtFacturas.Columns.Add("U_Interes")
                dtFacturas.Columns.Add("U_IntMor")
                dtFacturas.Columns.Add("DocDueDate")
                dtFacturas.Columns.Add("Series")
                dtFacturas.Columns.Add("DocTotal")
                dtFacturas.Columns.Add("PaidToDate")
                dtFacturas.Columns.Add("TaxDate")
                dtFacturas.Columns.Add("DocDate")
                dtFacturas.Columns.Add("NumFact")
                dtFacturas.Columns.Add("FechaVencimiento")
                dtFacturas.Columns.Add("PagoTotLinea", GetType(Decimal))
                dtFacturas.Columns.Add("U_OrdenVenta")

                If oMatrix.RowCount > 0 Then
                    For i As Integer = 1 To oMatrix.RowCount
                        Dim isSelected As String
                        isSelected = (CType(oMatrix.Columns.Item("10000127").Cells.Item(i).Specific, SAPbouiCOM.CheckBox)).Checked.ToString()
                        If isSelected = "True" Then
                            'Renglon es la variable que adicionara renglones a mi datatable

                            cardCodeText = oform.Items.Item("5").Specific

                            Dim CardCode As String = cardCodeText.Value.ToString()
                            Dim DocNum As String = (CType(oMatrix.Columns.Item("1").Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToString()
                            Dim DtFacturaPago As DataTable = BDInteresesMultas.getDatosFacturaParaPago(DocNum, CardCode)

                            If DtFacturaPago.Rows.Count > 0 Then

                                'If DtFacturaPago.Rows(0).Item("U_DocEntryF").ToString() = Nothing Or DtFacturaPago.Rows(0).Item("U_DocEntryF").ToString() <> "" Then

                                Dim Renglon As DataRow = dtFacturas.NewRow()
                                    Renglon("DocEntry") = DtFacturaPago.Rows(0).Item("DocEntry").ToString()
                                    Renglon("DocNum") = DtFacturaPago.Rows(0).Item("DocNum").ToString()
                                    Renglon("CardCode") = DtFacturaPago.Rows(0).Item("CardCode").ToString()
                                Renglon("U_OrdenVenta") = DtFacturaPago.Rows(0).Item("U_OrdenVenta").ToString()
                                Renglon("DocStatus") = DtFacturaPago.Rows(0).Item("DocStatus").ToString()
                                    Renglon("U_DocCuota") = DtFacturaPago.Rows(0).Item("U_DocCuota").ToString()
                                    Renglon("U_CuotFi") = DtFacturaPago.Rows(0).Item("U_CuotFi").ToString()
                                    Renglon("U_Interes") = DtFacturaPago.Rows(0).Item("U_Interes").ToString()
                                    Renglon("U_IntMor") = DtFacturaPago.Rows(0).Item("U_IntMor").ToString()
                                    Renglon("DocDueDate") = DtFacturaPago.Rows(0).Item("DocDueDate").ToString()
                                    Renglon("DocTotal") = DtFacturaPago.Rows(0).Item("DocTotal").ToString()
                                    Renglon("PaidToDate") = DtFacturaPago.Rows(0).Item("PaidToDate").ToString()
                                    Renglon("TaxDate") = DtFacturaPago.Rows(0).Item("TaxDate").ToString()
                                    Renglon("DocDate") = DtFacturaPago.Rows(0).Item("DocDate").ToString()
                                    Dim NumFact As String = (CType(oMatrix.Columns.Item("71").Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToString()
                                    Renglon("NumFact") = NumFact
                                    Dim FecVencimient As String = (CType(oMatrix.Columns.Item("22").Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToString()
                                    Renglon("FechaVencimiento") = FecVencimient
                                    Dim PagoTotLinea As String = (CType(oMatrix.Columns.Item("24").Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToString().Replace("CLP ", "").Replace("BOB ", "").Replace("BS ", "").Replace("UF ", "").Replace("USD ", "").Replace("SOL ", "")  '10,278.45
                                    Dim PagoTotLineaDouble As Decimal = DatosDB.ParseDecimalFormat(PagoTotLinea)
                                    Renglon("PagoTotLinea") = PagoTotLineaDouble

                                    dtFacturas.Rows.Add(Renglon)

                                'End If


                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                oApp.MessageBox("AdminSBO Error:" & ex.ToString)
            End Try
        End Sub
        Public Shared Function ChooseFromListt(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "CodUd"
                    bBubbleEvent = SNChooseFromListt(FormUID, oEvent, oApplication, oCompany)

                Case Else
            End Select

            Return bBubbleEvent
        End Function

        Public Shared Function SNChooseFromListt(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean

            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim edtCodUd As SAPbouiCOM.EditText
                    Dim edtCode As SAPbouiCOM.EditText

                    oForm = oApplication.Forms.Item(FormUID)
                    oForm.Freeze(True)
                    edtCodUd = oForm.Items.Item("CodUd").Specific
                    edtCode = oForm.Items.Item("CardCode").Specific
                    oForm = oApplication.Forms.Item("47622")
                    oDataTable = Utiles.Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            edtCodUd.Value = oDataTable.GetValue("U_CodUd", 0)
                            edtCode.Value = oDataTable.GetValue("CardCode", 0)
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'oForm.Freeze(False)
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function



    End Class

End Namespace
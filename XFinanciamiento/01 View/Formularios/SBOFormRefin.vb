﻿Imports SAPbobsCOM
Imports System.Globalization
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Reflection

Namespace View
    Public Class SBOFormRefin
#Region "VALIDACIONES"
        Public Shared Function ValidaRefin(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal lista As List(Of Double)) As Boolean
            Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("grdRefin").Specific
            Dim IsSelected As String = String.Empty

            Try

                For i As Integer = 0 To oGrid.Rows.Count - 1

                    IsSelected = oGrid.DataTable.GetValue("CONFIRMAR", i).ToString

                    If IsSelected = "Y" Then
                        Dim numeroFacturas As String = oGrid.DataTable.GetValue("TOTAL DOCUMENTO OV", i).ToString
                        lista.Add(numeroFacturas)
                    End If
                Next

                If lista.Count = 0 Then
                    oApp.StatusBar.SetText("[FN] No se selecciono ningun registro para refinanciar sus plazos.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    Return True
                End If


                'If IsSelected = "" Then
                '    oApp.StatusBar.SetText("[FN] No se selecciono ningun registro para refinanciar sus plazos.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                '    Return False
                'End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
#End Region
#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByRef oApp As SAPbouiCOM.Application, Optional DocEntry As String = "", Optional CapitalTotal As String = "", Optional FechaDoc As String = "")
            Try

                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "REFIN", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)
                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm

                bloquearItems(oApp, oForm)

            Catch ex As Exception
                oForm.Freeze(False)

                oForm.Visible = True
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try

        End Sub

        Private Shared Sub bloquearItems(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            oForm.Items.Item("edtOvRe").Enabled = False
        End Sub

        Private Shared Sub CargarGridFacturas(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Dim oDTFacturas As DataTable
            Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("grdRefin").Specific
            Dim oGridColumn As SAPbouiCOM.GridColumn
            Dim oGridEditCol As SAPbouiCOM.EditTextColumn
            Dim oDTRows As SAPbouiCOM.DataTable = oForm.DataSources.DataTables.Item("DT_0")
            Dim CardCodeSN As String = String.Empty
            Dim numlin As Integer = 0

            Try
                CardCodeSN = oForm.DataSources.UserDataSources.Item("CardC").ValueEx
                oDTFacturas = BDRefinanciamiento.GetFacturasVencidas(CardCodeSN)

                oDTRows.Clear()
                oDTRows.Columns.Add("CONFIRMAR", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 10)
                oDTRows.Columns.Add("ORDEN DE VENTA BASE", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 10)
                oDTRows.Columns.Add("NUMERO DE FACTURA", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 10)
                oDTRows.Columns.Add("REFERENCIA DE CLIENTE", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 100)
                'oDTRows.Columns.Add("COTIZACION BASE", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 10)
                'oDTRows.Columns.Add("SUBTIPO DE DOCUMENTO", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 100)
                oDTRows.Columns.Add("FECHA DE VENCIMIENTO", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                oDTRows.Columns.Add("TIPO DE MONEDA", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                oDTRows.Columns.Add("TOTAL DOCUMENTO OV", SAPbouiCOM.BoFieldsType.ft_Price)
                oDTRows.Columns.Add("TOTAL DOCUMENTO FC", SAPbouiCOM.BoFieldsType.ft_Price)
                oDTRows.Columns.Add("SALDO POR PAGAR FC", SAPbouiCOM.BoFieldsType.ft_Price)

                If oDTFacturas.Rows.Count > 0 Then
                    For i As Integer = 0 To oDTFacturas.Rows.Count - 1
                        oDTRows.Rows.Add()
                        oDTRows.SetValue("ORDEN DE VENTA BASE", i, Trim(oDTFacturas.Rows(i).Item("ORDEN DE VENTA").ToString))
                        oDTRows.SetValue("NUMERO DE FACTURA", i, Trim(oDTFacturas.Rows(i).Item("NUMERO DE FACTURA").ToString))
                        oDTRows.SetValue("REFERENCIA DE CLIENTE", i, Trim(oDTFacturas.Rows(i).Item("REFERENCIA DE CLIENTE").ToString))
                        'oDTRows.SetValue("COTIZACION BASE", i, Trim(oDTFacturas.Rows(i).Item("COTIZACION").ToString))
                        'oDTRows.SetValue("SUBTIPO DE DOCUMENTO", i, Trim(oDTFacturas.Rows(i).Item("SUBTIPO COTIZACION").ToString))
                        oDTRows.SetValue("FECHA DE VENCIMIENTO", i, Trim(oDTFacturas.Rows(i).Item("FECHA VENCIMIENTO").ToString))
                        oDTRows.SetValue("TIPO DE MONEDA", i, Trim(oDTFacturas.Rows(i).Item("MONEDA").ToString))
                        oDTRows.SetValue("TOTAL DOCUMENTO OV", i, oDTFacturas.Rows(i).Item("TOTAL DOCUMENTO OV").ToString)
                        oDTRows.SetValue("TOTAL DOCUMENTO FC", i, Trim(oDTFacturas.Rows(i).Item("TOTAL DOCUMENTO FC").ToString))
                        oDTRows.SetValue("SALDO POR PAGAR FC", i, Trim(oDTFacturas.Rows(i).Item("SALDO POR PAGAR FC").ToString))
                    Next
                End If

                oGrid.DataTable = oDTRows
                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numlin = numlin + 1
                        oGrid.RowHeaders.SetText(i, numlin)
                    Next
                End If

                oGridColumn = oGrid.Columns.Item(0)
                oGridColumn.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
                oGrid.RowHeaders.TitleObject.Caption = "#"
                oGridEditCol = oGrid.Columns.Item("ORDEN DE VENTA BASE")
                oGridEditCol.LinkedObjectType = 17
                'oGrid.Columns.Item(0).Editable = False
                'oGrid.Columns.Item(1).Editable = False
                'oGrid.Columns.Item(2).Editable = False
                'oGrid.Columns.Item(3).Editable = False
                'oGrid.Columns.Item(4).Editable = False
                'oGrid.Columns.Item(5).Editable = False
                'oGrid.Columns.Item(6).Editable = False
                'oGrid.Columns.Item(7).Editable = False
                'oGrid.Columns.Item(8).Editable = False
                ''ogrid.Columns.Item(9).Editable = False
                'oGrid.Columns.Item(9).Editable = False

                oGrid.AutoResizeColumns()

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Private Shared Sub llenaDatos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Dim sNewObjCode As String = String.Empty
            DiApp.GetNewObjectCode(sNewObjCode)
            Dim nuevoDocEntry As String = Trim(sNewObjCode)
            oForm.DataSources.UserDataSources.Item("OvRef").ValueEx = sNewObjCode
            oForm.Items.Item("edtOvRe").Enabled = False

        End Sub

        Private Shared Sub OVRefin(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef lista As List(Of Double))

            Dim oOrders As SAPbobsCOM.Documents
            Dim CardCode As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("grdRefin").Specific
            Dim lRetCode As Integer
            Dim lErr As Integer = 0
            Dim sErr As String = String.Empty

            Try
                oForm.Title = "Orden de Refinanciamiento"
                CardCode = oForm.Items.Item("CardCode").Specific
                oOrders = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)
                oOrders.DocObjectCode = SAPbobsCOM.BoObjectTypes.oOrders
                oOrders.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service

                oOrders.CardCode = CardCode.Value
                oOrders.DocDate = DateTime.Today
                oOrders.DocDueDate = DateTime.Today
                oOrders.TaxDate = DateTime.Today


                If lista IsNot Nothing Then

                    For i As Integer = 0 To lista.Count - 1 Step 1

                        oOrders.Lines.ItemDescription = "REFIN"
                        'oOrders.Lines.AccountCode = "11101001-001-001-001"
                        oOrders.Lines.TaxCode = "IVA_EXE"
                        oOrders.Lines.Price = DatosDB.ParseDecimalFormat(lista(i))
                        oOrders.Lines.Add()
                    Next

                End If

                oOrders.UserFields.Fields.Item("U_Refinancia").Value = "Y"

                lRetCode = oOrders.Add()

                If lRetCode <> 0 Then
                    DiApp.GetLastError(lErr, sErr)
                    MsgBox(sErr)
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub

        Private Shared Function ChooseFromListRFN(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "CardCode"
                    bBubbleEvent = SNChooseFromList(FormUID, oEvent, oApplication, oCompany)

                Case Else
            End Select

            Return bBubbleEvent
        End Function

        Private Shared Function SNChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application,
                                                  ByVal oCompany As SAPbobsCOM.Company) As Boolean

            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim edtEmp As SAPbouiCOM.EditText
                    Dim edtEmpN As SAPbouiCOM.EditText
                    Dim edtFecha As SAPbouiCOM.EditText

                    oForm = oApplication.Forms.Item(FormUID)
                    edtEmp = oForm.Items.Item("CardCode").Specific
                    edtEmpN = oForm.Items.Item("edtNombre").Specific
                    edtFecha = oForm.Items.Item("edtFecha").Specific
                    'btnRefin = oForm.Items.Item("1").Specific
                    oDataTable = Utiles.Util.f_GetDataTableFromCFL(oEvent, oForm)

                    If Not oDataTable Is Nothing Then
                        Try
                            'btnRefin.Caption = "Refinanciar"
                            edtFecha.Item.Enabled = False
                            edtFecha.Value = DateTime.Today
                            edtEmpN.Value = oDataTable.GetValue("CardName", 0)
                            edtEmp.Value = oDataTable.GetValue("CardCode", 0)

                        Catch ex As Exception
                        End Try
                    End If
                End If
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function
#End Region
#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDORefin(oApp As SAPbouiCOM.Application, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "CardCode"
                                    Try
                                        ChooseFromListRFN(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                        CargarGridFacturas(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.BeforeAction = False Then

                            Select Case pVal.ItemUID
                                Case "btnRef"
                                    Dim lista As New List(Of Double)
                                    If Not ValidaRefin(oApp, oForm, lista) Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        OVRefin(oApp, oForm, lista)
                                        llenaDatos(oApp, oForm)

                                    End If
                            End Select
                        End If
                End Select

fail:
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

        Public Shared Sub AdminSBOFormUDORefinEve(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)

            Dim oForm As SAPbouiCOM.Form
            oForm = oApp.Forms.Item(pVal.FormUID)

            Try
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD

                        oForm.Freeze(True)

                        If pVal.BeforeAction = False Then

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub
#End Region

    End Class
End Namespace


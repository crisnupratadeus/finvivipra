﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormUDOFIPLA

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpPRG") = False Then
                    Exit Sub
                End If

                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "FIPLA", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)

                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm

                If Not Left(oForm.TypeEx, 1) = "-" Then

                    If Not formatItems(oApp, oForm) Then
                        oForm.Freeze(False)
                        oApp.MessageBox("Problemas al formatear items")
                    End If

                    'Dim oDBCab As SAPbouiCOM.DBDataSource
                    'oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_FIPLAN")

                End If

            Catch ex As Exception
                oForm.Freeze(False)
                ' oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Private Shared Function EliminaFilaMat(ByRef oForm As SAPbouiCOM.Form, ByRef oApp As SAPbouiCOM.Application) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix

            Try
                oForm.Freeze(True)
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_FIPLA1")
                oMat = oForm.Items.Item("0_U_G").Specific
                oMat.FlushToDataSource()
                For i As Integer = 0 To oDBLin.Size - 1
                    If Trim(oDBLin.GetValue("U_Codigo", i)) = "" Then
                        oDBLin.RemoveRecord(i)
                    End If
                Next
                oMat.LoadFromDataSourceEx(False)
                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function AñadeFilaAuto(ByRef oForm As SAPbouiCOM.Form) As Boolean
            Try
                Dim oMatLin As SAPbouiCOM.Matrix
                Dim oDBLin As SAPbouiCOM.DBDataSource
                Dim maxindex As Integer
                oForm.Freeze(True)
                Try
                    'se setean las variables a sus correspondientes objetos
                    oMatLin = oForm.Items.Item("0_U_G").Specific
                    oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_FIPLA1")
                Catch ex As Exception
                    oForm.Freeze(False)
                    Return False
                End Try

                oDBLin.Clear()
                oMatLin.FlushToDataSource()

                For i As Integer = 0 To oDBLin.Size - 1
                    If Trim(oDBLin.GetValue("U_Codigo", i)) = "" Then
                        oForm.Freeze(False)
                        Return False
                    End If
                Next

                maxindex = oDBLin.Size
                oDBLin.InsertRecord(maxindex)
                oDBLin.SetValue("LineId", maxindex, maxindex + 1)
                oMatLin.LoadFromDataSourceEx(False)
                ' oMatLin.AutoResizeColumns()
                oMatLin = Nothing
                oForm.Freeze(False)
                'oForm.Update()
                Return (True)
            Catch ex As Exception
                oForm.Freeze(False)
                Return False
            End Try
        End Function

        Private Shared Function formatItems(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oBtnCombo As SAPbouiCOM.ButtonCombo
            Dim oGrid As SAPbouiCOM.Grid
            Dim oBtn As SAPbouiCOM.Button
            Dim edtTxt As SAPbouiCOM.EditText
            Dim oMat As SAPbouiCOM.Matrix
            Dim oColCmb As SAPbouiCOM.ComboBoxColumn
            Dim oCol As SAPbouiCOM.Column


            Try
                oCombo = oForm.Items.Item("22_U_Cb").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                oMat = oForm.Items.Item("0_U_G").Specific
                If oMat.RowCount = 0 Then
                    oMat.AddRow()
                    oMat.Columns.Item("#").Cells.Item(1).Specific.value = 1
                    oMat.AutoResizeColumns()
                End If

                oCol = oMat.Columns.Item("C_0_3")
                oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                oForm.Freeze(True)
                oMat.AutoResizeColumns()
                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function
#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDOFIPLA(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_FIPLAN")
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        Select Case pVal.ItemUID
                            Case "0_U_G"
                                Select Case pVal.ColUID
                                    Case "C_0_4"
                                        Dim oMat As SAPbouiCOM.Matrix
                                        oMat = oForm.Items.Item("0_U_G").Specific

                                    Case "C_0_1"
                                        Try
                                            If Not ValidaDuplCod(oApp, oForm, pVal) Then
                                                oApp.MessageBox("No se puede elegir un codigo repetido en la matriz, por favor verifique la información")
                                                BubbleEvent = False
                                                GoTo fail
                                            End If

                                            AñadeFilaAuto(oForm)
                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try

                                End Select
                        End Select

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

        Public Shared Sub AdminSBOFormUDOFIPLADatEve(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_FIPLAN")


                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                        If pVal.BeforeAction = True Then
                            Dim Codigo As String = String.Empty
                            Dim res As Integer
                            Try

                                Codigo = oDBCab.GetValue("U_Codigo", 0)
                                res = DatosDB.ValidaCodigoPlantilla(Trim(Codigo))
                                If res > 0 Then
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Ya existe una plantilla con el codigo:  " & Trim(Codigo) & "  , cambie el codigo e intente nuevamente.")
                                    BubbleEvent = False
                                    GoTo fail
                                ElseIf res = 0 Then
                                    BubbleEvent = True
                                Else
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Ocurrio un error al intentar validar el codigo de la plantilla , intente nuevamente.")
                                    BubbleEvent = False
                                    GoTo fail
                                End If

                                Try
                                    If Not EliminaFilaMat(oForm, oApp) Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try

                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try


                        Else


                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                        If pVal.BeforeAction = True Then
                            Try
                                If Not EliminaFilaMat(oForm, oApp) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                        Else


                        End If

                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#End Region

#Region "VALIDACIONES"
        Private Shared Function ValidaDuplCod(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent) As Boolean

            Dim offset As Integer
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim codigo As String = String.Empty
            Try
                oForm.Freeze(True)
                oMat = oForm.Items.Item("0_U_G").Specific
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_FIPLA1")
                oMat.FlushToDataSource()
                offset = pVal.Row
                codigo = Trim(oDBLin.GetValue("U_Codigo", offset - 1))

                For i As Integer = 1 To oDBLin.Size
                    If Not i = offset Then
                        If Trim(oDBLin.GetValue("U_Codigo", i - 1)) = codigo Then
                            oDBLin.SetValue("U_Codigo", offset - 1, String.Empty)
                            oMat.LoadFromDataSourceEx(False)
                            oForm.Freeze(False)
                            'oMat.Columns.Item("C_0_2").Cells.Item(offset).Click()
                            Return False
                        End If
                    End If
                Next
                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function
#End Region

    End Class
End Namespace





﻿Imports System.Configuration
Imports System.Windows.Forms
Imports System.Globalization
Imports System.Resources
Imports System.Reflection
Imports XFinanciamiento.Principal
Imports System.Threading
Imports XFinanciamiento.Data
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.IO
Imports System.Text
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles

Namespace View

    Public Class FormParamFinan


        Public Shared oPublicForm As SAPbouiCOM.Form
        Public Shared days = New Dictionary(Of String, Integer)
        Public Shared Sub CreaSBOForm(ByRef oApp As SAPbouiCOM.Application)
            Try
                'oForm.Freeze(True)
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String

                Dim blnexit As Boolean = RecorreFormulario(oApp, "FormParamFinan") 'verifica que el formulario no se encuentre abierto. 

                If blnexit Then
                    Exit Sub 'si el fomulario se encuentra abierto cierra este sub
                End If

                strPath = Application.StartupPath & "\ParamFinanciamiento.srf" 'se pasa la ruta del formulario
                xmlDoc.Load(strPath) 'se carga el fomulario
                oApp.LoadBatchActions(xmlDoc.InnerXml)


                oForm = oApp.Forms.Item("FormParamFinan") 'se asigna el fomulario al objeto form
                oForm.Freeze(True)
                loadFormSettings(oForm)

                If Not formatItems(oApp, oForm) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Problemas al formatear items")
                End If

                oForm.Visible = True


            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Visible = True
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try

        End Sub
        Private Shared Sub loadData(oForm As SAPbouiCOM.Form)
            Try
                days.Clear()
                days.Add("S", 7)
                days.Add("M", 30)
                days.Add("T", 90)
                days.Add("SM", 180)
                days.Add("A", 360)
                days.Add("O", 1)

                Dim oStaticMoneda As SAPbouiCOM.StaticText

                oStaticMoneda = oForm.Items.Item("IT_Moneda").Specific

                oStaticMoneda.Caption = MonedaLocal

                oForm.DataSources.UserDataSources.Item("PLMAUTO").ValueEx = DatosDB.GetParam("PLMAUTO")
                oForm.DataSources.UserDataSources.Item("PL_RT").ValueEx = DatosDB.GetParam("PL_RT")
                oForm.DataSources.UserDataSources.Item("GrpACC").ValueEx = DatosDB.GetParam("GrpACC")
                oForm.DataSources.UserDataSources.Item("GrpPRG").ValueEx = DatosDB.GetParam("GrpACC")
                oForm.DataSources.UserDataSources.Item("GrpMOD").ValueEx = DatosDB.GetParam("GrpMOD") 'cnp20190711
                oForm.DataSources.UserDataSources.Item("GrpANE").ValueEx = DatosDB.GetParam("GrpANE") 'cnp20190711
                oForm.DataSources.UserDataSources.Item("UD_Periodo").ValueEx = DatosDB.GetParam("UD_Periodo")
                oForm.DataSources.UserDataSources.Item("PLFECHA").ValueEx = DatosDB.GetParam("PLFECHA")
                oForm.DataSources.UserDataSources.Item("udGrpRL").ValueEx = DatosDB.GetParam("udGrpRL")

                If Not String.IsNullOrEmpty(DatosDB.GetParam("UD_Dia_Per")) Then
                    oForm.DataSources.UserDataSources.Item("UD_Dia_Per").ValueEx = DatosDB.GetParam("UD_Dia_Per")
                End If

                If Not String.IsNullOrEmpty(DatosDB.GetParam("UD_Per_Gra")) Then
                    oForm.DataSources.UserDataSources.Item("UD_Per_Gra").ValueEx = DatosDB.GetParam("UD_Per_Gra")
                End If


                If Not String.IsNullOrEmpty(DatosDB.GetParam("UD_Multa_F")) Then
                    oForm.DataSources.UserDataSources.Item("UD_Multa_F").ValueEx = DatosDB.GetParam("UD_Multa_F")
                End If

                oForm.DataSources.UserDataSources.Item("UD_PG_Tipo").ValueEx = DatosDB.GetParam("UD_PG_Tipo")
                oForm.DataSources.UserDataSources.Item("GrpACC").ValueEx = DatosDB.GetParam("GrpACC")
                oForm.DataSources.UserDataSources.Item("GrpPRG").ValueEx = DatosDB.GetParam("GrpPRG")
                oForm.DataSources.UserDataSources.Item("UD_Mult_Ti").ValueEx = DatosDB.GetParam("UD_Mult_Ti")


                oForm.DataSources.UserDataSources.Item("TaxComi2").ValueEx = DatosDB.GetParam("TaxComi2")
                oForm.DataSources.UserDataSources.Item("ItemCode2").ValueEx = DatosDB.GetParam("ItemCode2")

                If Not String.IsNullOrEmpty(DatosDB.GetParam("UD_IT")) Then
                    oForm.DataSources.UserDataSources.Item("UD_IT").ValueEx = DatosDB.GetParam("UD_IT")
                End If

                If Not String.IsNullOrEmpty(DatosDB.GetParam("UD_GA")) Then
                    oForm.DataSources.UserDataSources.Item("UD_GA").ValueEx = DatosDB.GetParam("UD_GA")
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Shared Sub loadFormSettings(oForm As SAPbouiCOM.Form)
            'oForm.Items.Item("Item_0").TextStyle = 5
            loadCmbs(oForm)
            loadData(oForm)

        End Sub



        Private Shared Sub loadCmbs(oForm As SAPbouiCOM.Form)
            Try
                Dim ocmb As SAPbouiCOM.ComboBox
                Dim hs As DataTable = DatosDB.GetGruposAutorizacion()
                ocmb = oForm.Items.Item("Item_21").Specific
                Utiles.Util.CargarComboDinamicoEx(ocmb, hs)
                ocmb = oForm.Items.Item("Item_12").Specific
                Utiles.Util.CargarComboDinamicoEx(ocmb, hs)
                'cnp20190711
                ocmb = oForm.Items.Item("Item_22").Specific
                Utiles.Util.CargarComboDinamicoEx(ocmb, hs)
                ocmb = oForm.Items.Item("Item_26").Specific
                Utiles.Util.CargarComboDinamicoEx(ocmb, hs)
                ocmb = oForm.Items.Item("Item_28").Specific
                Utiles.Util.CargarComboDinamicoEx(ocmb, hs)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Sub Savedata(oForm As SAPbouiCOM.Form)
            Try

                Dim PLMAUTO As String = Trim(oForm.DataSources.UserDataSources.Item("PLMAUTO").ValueEx)
                UpdateParameter("PLMAUTO", PLMAUTO)

                Dim GrpACC As String = Trim(oForm.DataSources.UserDataSources.Item("GrpACC").ValueEx)
                UpdateParameter("GrpACC", GrpACC)

                Dim GrpPRG As String = Trim(oForm.DataSources.UserDataSources.Item("GrpPRG").ValueEx)
                UpdateParameter("GrpPRG", GrpPRG)

                Dim GrpMOD As String = Trim(oForm.DataSources.UserDataSources.Item("GrpMOD").ValueEx)
                UpdateParameter("GrpMOD", GrpMOD) 'cnp20190711

                Dim GrpANE As String = Trim(oForm.DataSources.UserDataSources.Item("GrpANE").ValueEx)
                UpdateParameter("GrpANE", GrpANE) 'cnp20190711

                Dim UD_Periodo As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Periodo").ValueEx)
                UpdateParameter("UD_Periodo", UD_Periodo)

                Dim UD_Dia_Per As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Dia_Per").ValueEx)
                UpdateParameter("UD_Dia_Per", UD_Dia_Per)

                Dim UD_Per_Gra As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Per_Gra").ValueEx)
                UpdateParameter("UD_Per_Gra", UD_Per_Gra)

                Dim UD_PG_Tipo As String = Trim(oForm.DataSources.UserDataSources.Item("UD_PG_Tipo").ValueEx)
                UpdateParameter("UD_PG_Tipo", UD_PG_Tipo)

                Dim UD_Multa_F As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Multa_F").ValueEx)
                UpdateParameter("UD_Multa_F", UD_Multa_F)

                Dim UD_Mult_Ti As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Mult_Ti").ValueEx)
                UpdateParameter("UD_Mult_Ti", UD_Mult_Ti)

                Dim udGrpRL As String = Trim(oForm.DataSources.UserDataSources.Item("udGrpRL").ValueEx)
                UpdateParameter("udGrpRL", udGrpRL)

                Dim ItemCode2 As String = Trim(oForm.DataSources.UserDataSources.Item("ItemCode2").ValueEx)
                UpdateParameter("ItemCode2", ItemCode2)

                Dim TaxComi2 As String = oForm.DataSources.UserDataSources.Item("TaxComi2").ValueEx
                UpdateParameter("TaxComi2", TaxComi2)

                Dim It As String = Trim(oForm.DataSources.UserDataSources.Item("UD_IT").ValueEx)
                UpdateParameter("UD_IT", It)

                Dim CodigoGasto As String = Trim(oForm.DataSources.UserDataSources.Item("UD_GA").ValueEx)
                UpdateParameter("UD_GA", CodigoGasto)

                Dim Retencion As String = Trim(oForm.DataSources.UserDataSources.Item("PL_RT").ValueEx)
                UpdateParameter("PL_RT", Retencion)

                Dim Fechas As String = Trim(oForm.DataSources.UserDataSources.Item("PLFECHA").ValueEx)
                UpdateParameter("PLFECHA", Fechas)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Function RecorreFormulario(ByRef oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean
            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit
        End Function


        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oForm As SAPbouiCOM.Form

                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        If pVal.BeforeAction = False Then
                            If pVal.ItemUID = "IT_Periodo" Then

                                Try
                                    Dim UD_Periodo As String = Trim(oForm.DataSources.UserDataSources.Item("UD_Periodo").ValueEx)

                                    Dim DiaGenerico As String = days.Item(UD_Periodo).ToString


                                    oForm.DataSources.UserDataSources.Item("UD_Dia_Per").Value = DiaGenerico


                                Catch ex As Exception
                                    MsgBox(ex.ToString)
                                End Try

                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                        If pVal.ItemUID = "1" And pVal.Before_Action = False Then
                            Try
                                Savedata(oForm)
                                BubbleEvent = True
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST

                        If pVal.Before_Action = False Then
                            'Select Case pVal.ItemUID
                            '    Case "edtCtaA"
                            '        Try
                            '            ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                            '        Catch ex As Exception
                            '            oForm.Freeze(False)
                            '            BubbleEvent = False
                            '            GoTo fail
                            '        End Try                                
                            'End Select
                            If pVal.ItemUID = "Item_26" And pVal.Before_Action = False Then
                                Dim objCFLEVENT As SAPbouiCOM.ItemEvent
                                Dim objDataTable As SAPbouiCOM.DataTable

                                objCFLEVENT = pVal
                                objDataTable = objCFLEVENT.SelectedObjects
                                Try
                                    If Not objDataTable Is Nothing Then
                                        oForm.DataSources.UserDataSources.Item("EXENTO").ValueEx = objDataTable.GetValue("Code", 0).ToString

                                    End If
                                Catch ex As Exception
                                    MsgBox(ex.ToString)
                                End Try
                            End If



                            If pVal.ItemUID = "Item_381" And pVal.Before_Action = False Then
                                Dim objCFLEVENT As SAPbouiCOM.ItemEvent
                                Dim objDataTable As SAPbouiCOM.DataTable

                                objCFLEVENT = pVal
                                objDataTable = objCFLEVENT.SelectedObjects
                                Try
                                    If Not objDataTable Is Nothing Then
                                        oForm.DataSources.UserDataSources.Item("PueCta").ValueEx = objDataTable.GetValue("AcctCode", 0).ToString
                                        oForm.DataSources.UserDataSources.Item("PueDesc").ValueEx = objDataTable.GetValue("AcctName", 0).ToString
                                    End If
                                Catch ex As Exception
                                    MsgBox(ex.ToString)
                                End Try
                            End If

                            If pVal.Before_Action = False Then
                                If pVal.ItemUID = "Item_25" And pVal.Before_Action = False Then
                                    Dim objCFLEVENT As SAPbouiCOM.ItemEvent
                                    Dim objDataTable As SAPbouiCOM.DataTable

                                    objCFLEVENT = pVal
                                    objDataTable = objCFLEVENT.SelectedObjects
                                    Try
                                        If Not objDataTable Is Nothing Then
                                            oForm.DataSources.UserDataSources.Item("TaxComi2").ValueEx = objDataTable.GetValue("Code", 0).ToString
                                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.ToString)
                                    End Try
                                End If
                                If pVal.ItemUID = "Item_29" And pVal.Before_Action = False Then
                                    Dim objCFLEVENT As SAPbouiCOM.ItemEvent
                                    Dim objDataTable As SAPbouiCOM.DataTable

                                    objCFLEVENT = pVal
                                    objDataTable = objCFLEVENT.SelectedObjects
                                    Try
                                        If Not objDataTable Is Nothing Then
                                            oForm.DataSources.UserDataSources.Item("ItemCode2").ValueEx = objDataTable.GetValue("ItemCode", 0).ToString
                                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.ToString)
                                    End Try
                                End If

                            End If


                        End If

                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception

                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBO Error:" & ex.ToString)

            End Try
        End Sub

        Private Shared Sub pruebas(oForm As SAPbouiCOM.Form)
            Dim prueba0 As Decimal = DatosDB.DBDS_OUT(oForm.DataSources.UserDataSources.Item("prueba0").ValueEx)
            Dim orecordset As SAPbobsCOM.Recordset

            orecordset = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'oForm.DataSources.UserDataSources.Item("prueba1").ValueEx = DatosDB.DBDS_IN(prueba0)

            Dim Sql As String = "INSERT into [@PRUEBAS] (Code, Name, U_prueba2) SELECT (right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1),	8)), (right('00000000' + convert(varchar(8), isnull(max(convert(int, Code)), 0) + 1), 8)), " & DatosDB.DBDS_IN(prueba0) & " from [@PRUEBAS]"
            orecordset.DoQuery(Sql)

            Sql = "SELECT U_Prueba2 FROM [@PRUEBAS]"
            orecordset.DoQuery(Sql)

            Dim valor As Decimal = orecordset.Fields.Item("U_prueba2").Value
            oForm.DataSources.UserDataSources.Item("prueba1").ValueEx = DatosDB.DBDS_IN(valor)
        End Sub

        Public Shared Sub UpdateParameter(Type As String, valor1 As String, Optional valor2 As String = "",
                                               Optional valor3 As String = "", Optional valor4 As String = "",
                                               Optional valor5 As String = "", Optional valor6 As String = "")

            Try
                DatosDB.UpdateParameter(Type, valor1, valor2, valor3, valor4, valor5, valor6)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Function formatItems(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oComboPeriodo As SAPbouiCOM.ComboBox
            Dim oComboPG_Tipo As SAPbouiCOM.ComboBox
            Dim oComboMult_Ti As SAPbouiCOM.ComboBox

            Try
                oComboPeriodo = oForm.Items.Item("IT_Periodo").Specific
                oComboPeriodo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                oComboPG_Tipo = oForm.Items.Item("IT_PG_Tipo").Specific
                oComboPG_Tipo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                oComboMult_Ti = oForm.Items.Item("IT_Mult_Ti").Specific
                oComboMult_Ti.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

    End Class
End Namespace
﻿Imports System.Globalization
Imports System.Resources
Imports System.Reflection
Imports XFinanciamiento.Data
Imports SAPbobsCOM

Namespace View
    Public Class SBOFormUDOCuotas
        Public Shared variableAdd As Boolean = False
#Region "VALIDACIONES"
        Private Shared Function ValidaPagos(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form, txtCuo As String) As Boolean

            If txtCuo > 200 Then
                oApp.StatusBar.SetText("[FN] Maximo número de cuotas establecido", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If
            Return True

        End Function

        Private Shared Function existePagos(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form, txtCuo As String) As Boolean
            If txtCuo = "" Then 'mauricio
                oApp.SetStatusBarMessage("[FN] Se debe tener al menos una cuota para hacer el cálculo", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If
            Return True
        End Function

        Private Shared Function validaGrid(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form) As Boolean
            Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific

            If oForm.Items.Item("Cuotas").Enabled = False And oGrid.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        End Function
        Private Shared Function anticipoMayor(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form, txtAnt As String) As Boolean
            Dim CapitalTotalOV As String = oForm.DataSources.UserDataSources.Item("DocT").ValueEx 'mauricio
            If CDbl(txtAnt) > CapitalTotalOV Then
                oApp.SetStatusBarMessage("[FN] El anticipo no puede ser mayor al capital", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If
            Return True

        End Function
        Private Shared Function validaBorrar(ByRef oApp As SAPbouiCOM.Application, oForm As SAPbouiCOM.Form) As Boolean

            Dim txtDoc As SAPbouiCOM.EditText = oForm.Items.Item("edtDoc").Specific
            Dim DocEntryOV As String = txtDoc.Value
            Dim oDTCuota As DataTable = BDCuotas.GetCuotas(DocEntryOV)
            Dim oDTCuotas As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@EXX_CUOTA")
            Dim DocEntry As String = oDTCuotas.GetValue("DocEntry", 0)

            If oDTCuota.Rows.Count <= 0 Then
                oApp.StatusBar.SetText("[FN] No se puede eliminar el plan; aun no esta asociado a una orden de venta.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Else
                If BorrarDatosCUOTAS(oApp, oForm, DocEntry) = True Then
                    If BorrarDatosLineas(oApp, oForm, DocEntry) = True Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End If
        End Function

        Private Shared Function BorrarDatosLineas(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef DocEntry As String) As Boolean

            Dim oUserTable As SAPbobsCOM.UserTable
            Dim rs As SAPbobsCOM.Recordset
            Try
                oUserTable = DiApp.UserTables.Item("EXX_CUOIN")
                rs = DiApp.GetBusinessObject(BoObjectTypes.BoRecordset)
                rs.DoQuery("SELECT ""Code"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_CUOIN"" WHERE ""U_DocEntryPG"" = '" & DocEntry & "'")
                While (Not rs.EoF)
                    Dim key As String = rs.Fields.Item("Code").Value.ToString().Trim()
                    If (oUserTable.GetByKey(key)) Then
                        Try
                            oUserTable.Remove()
                        Catch ex As Exception
                            oApp.MessageBox(ex.Message)
                            Return False
                        End Try
                    End If
                    rs.MoveNext()
                End While
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rs)
                Return True
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function BorrarDatosCUOTAS(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef DocEntry As String) As Boolean
            Dim oUserTable As SAPbobsCOM.UserTable

            Try
                oUserTable = DiApp.UserTables.Item("EXX_CUOTA")
                If oUserTable.GetByKey(DocEntry) Then
                    Try
                        oUserTable.Remove()
                    Catch ex As Exception
                        oApp.MessageBox(ex.Message)
                        Return False
                    End Try
                End If
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function
        Private Shared Function verificaLinea(ByRef oForm As SAPbouiCOM.Form, ByRef SocioNegocio As String, ByRef ItemCode As String) As Boolean

            Dim oDTExxLicre As DataTable = BDCuotas.GetExxLicre(SocioNegocio)
            Dim oDTExxLicre2 As DataTable = BDCuotas.GetExxLicre2(ItemCode)
            Dim contador As Integer = oDTExxLicre.Rows.Count
            Dim contadorInt As Integer = oDTExxLicre2.Rows.Count
            Dim Interes As SAPbouiCOM.EditText = oForm.Items.Item("Interes").Specific
            Dim InteresL As SAPbouiCOM.EditText = oForm.Items.Item("InteresL").Specific
            Try
                If contador > 0 Then
                    If contadorInt > 0 Then
                        Interes.Value = CInt(oDTExxLicre2.Rows.Item(0).Item("U_TasaNor").ToString)
                        InteresL.Value = CInt(oDTExxLicre2.Rows.Item(0).Item("U_TasaLeg").ToString)
                    Else
                        Interes.Value = CInt(oDTExxLicre.Rows.Item(0).Item("U_Interes").ToString)
                        InteresL.Value = CInt(oDTExxLicre.Rows.Item(0).Item("U_InteresM").ToString)
                    End If
                    bloquearItems(oForm, contador)
                    Return True
                Else
                    bloquearItems(oForm, contador)
                    Return False
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function
#End Region
#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByRef oApp As SAPbouiCOM.Application, Optional DocEntry As String = "", Optional CapitalTotal As String = "", Optional FechaDoc As String = "", Optional SocioNegocio As String = "", Optional ItemCode As String = "")
            Try

                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "CUOTA", "47616")
                Dim oDBCab As SAPbouiCOM.DBDataSource

                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)

                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm
                'oApp.Menus.Item("1281").Enabled = False
                ''oForm.Freeze(True)
                ''Dim xmlDoc As New Xml.XmlDocument
                ''Dim strPath As String

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_CUOTA")
                oDBCab.SetValue("U_DocEntry", 0, DocEntry)
                Dim txtDoc As SAPbouiCOM.EditText = oForm.Items.Item("edtDoc").Specific
                oForm.Items.Item("edtDoc").Enabled = False
                txtDoc.Value = DocEntry
                oForm.DataSources.UserDataSources.Item("DocT").ValueEx = CapitalTotal
                oForm.DataSources.UserDataSources.Item("FechaD").ValueEx = FechaDoc
                Dim button As SAPbouiCOM.Button
                button = oForm.Items.Item("btnRecalc").Specific
                button.Item.Visible = False

                If verificaLinea(oForm, SocioNegocio, ItemCode) = True Then
                Else
                    oApp.StatusBar.SetText("[FN] No se copiarán los intereses, ya que no se tiene una línea de crédito para este socio de negocio.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If

            Catch ex As Exception
                oForm.Freeze(False)

                oForm.Visible = True
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try

        End Sub

        Private Shared Function RecorreFormulario(ByRef oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Private Shared Sub loadFormSettings(oForm As SAPbouiCOM.Form, docentry As String)
            Dim odatatable As SAPbouiCOM.DataTable = oForm.DataSources.DataTables.Item("DT_0")
            Dim ogrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_0").Specific

            Dim sqr As String = BDContratos.QueryDocumentosRelacionados(docentry)
            odatatable.ExecuteQuery(sqr)

            ogrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            Dim oColJournalEntrie As SAPbouiCOM.GridColumn
            oColJournalEntrie = ogrid.Columns.Item(0)
            oColJournalEntrie.LinkedObjectType = 13

            ogrid.Columns.Item(4).RightJustified = True
            Dim Coldepper As SAPbouiCOM.EditTextColumn
            Coldepper = ogrid.Columns.Item(4)
            Coldepper.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto

            ogrid.Columns.Item(5).RightJustified = True
            Coldepper = ogrid.Columns.Item(5)
            Coldepper.ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto

            ogrid.Columns.Item(2).Visible = False

            ogrid.AutoResizeColumns()

        End Sub

        Public Shared Function GetLastDocEntry(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, xml As String) As String
            Dim xmldoc As System.Xml.XmlDocument = New System.Xml.XmlDocument()
            Dim DocEntry As String
            Try

                xmldoc.LoadXml(Trim(xml))

                DocEntry = xmldoc.SelectSingleNode("/CuotasParams/DocEntry").InnerText

                If DocEntry <> "" Then

                    Return Trim(DocEntry)
                Else
                    Return "-1"
                End If



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return "-1"
            End Try
        End Function

        Private Shared Function addDatos(ByRef oApp As SAPbouiCOM.Application, ByRef pval As SAPbouiCOM.BusinessObjectInfo, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oOV As SAPbobsCOM.Documents
            Dim ogrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific
            Dim xml, DocEntry, DocEntryOV, FechaVencimiento, DocEntryPG, TotalCuota, CapitalxPagar, Amortizacion As String
            Dim sErr As String = String.Empty
            Dim lRetCode, lErr As Integer

            Try
                xml = pval.ObjectKey
                DocEntry = GetLastDocEntry(oApp, oForm, xml)

                oOV = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@EXX_CUOTA")
                DocEntryOV = oDBCab.GetValue("U_DocEntry", 0)
                DocEntryPG = DocEntry


                For i As Integer = 0 To ogrid.Rows.Count - 1

                    FechaVencimiento = ogrid.DataTable.GetValue("FECHA DE PAGO", i) 'mauricio
                    TotalCuota = ogrid.DataTable.GetValue("CUOTA TOTAL", i)
                    CapitalxPagar = ogrid.DataTable.GetValue("CAPITAL POR PAGAR", i)
                    Amortizacion = ogrid.DataTable.GetValue("AMORTIZACION", i)
                    DatosDB.InsertaCuotasInteres(DocEntryOV, FechaVencimiento, DocEntryPG, TotalCuota, CapitalxPagar, Amortizacion)

                Next

                oOV.GetByKey(Trim(oDBCab.GetValue("U_DocEntry", 0)))

                oOV.UserFields.Fields.Item("U_DocCuota").Value = DocEntry
                oOV.UserFields.Fields.Item("U_CuotFi").Value = Trim(oDBCab.GetValue("U_Cuotas", 0))
                oOV.UserFields.Fields.Item("U_IntMor").Value = Trim(oDBCab.GetValue("U_InteresLegal", 0))
                oOV.UserFields.Fields.Item("U_Interes").Value = Trim(oDBCab.GetValue("U_TasaInteres", 0))

                lRetCode = oOV.Update

                Return True
                If lRetCode <> 0 Then
                    DiApp.GetLastError(lErr, sErr)
                    oApp.MessageBox(lErr & " " & sErr)
                End If

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Sub loadFormSettings(oForm As SAPbouiCOM.Form, txtAnt As String, txtTas As String, txtCuo As String, BubbleEvent As Boolean)

            Try
                oForm.Freeze(True)

                Dim odatatable As SAPbouiCOM.DataTable = oForm.DataSources.DataTables.Item("DT_0")
                Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific
                Dim numLin, err As Integer
                Dim Original As Integer = 1
                Dim Lapso As Integer = DatosDB.GetParam("UD_Dia_Per")
                Dim CapPago As Decimal = 0.0
                Dim CapitalTotal As String = oForm.DataSources.UserDataSources.Item("DocT").ValueEx
                Dim FechaDoc As String = oForm.DataSources.UserDataSources.Item("FechaD").ValueEx
                Dim oStr As String

                If Motor = "SQL" Then
                    oStr = "EXEC [dbo].[sp_Calculo_Cuotas] " &
                                "@Capital = '" & CapitalTotal & "'" &
                                ", @Cuotas = '" & txtCuo & "'" &
                                ", @Tasa = '" & txtTas & "'" &
                                ", @Lapso = '" & Lapso & "'" &
                                ", @Fecha_Ini = '" & FechaDoc & "'" &
                                ", @Cap_pagado = 0" &
                                ", @Cap_Anticipo = '" & txtAnt & "'" &
                                ", @Original = 1" &
                                ", @GastosA = 0"
                Else 'TIPOTABLE
                    Dim V_Original As Integer
                    Dim V_error_msg As String
                    Dim CuotaFija As String
                    Dim V_GastosA As Decimal

                    V_Original = 1
                    V_error_msg = "A"
                    CuotaFija = "S"
                    V_GastosA = 0

                    oStr = "DO " + vbCrLf +
                           "BEGIN " + vbCrLf +
                           "DECLARE tabla """ & SQLBaseDatos.ToUpper & """.""CALCUOTATYPE"";" + vbCrLf +
                           "DECLARE err int = 0;" + vbCrLf +
                           "CALL """ & SQLBaseDatos.ToUpper & """.""SP_CALCULO_CUOTAS_HANA""" + vbCrLf +
                           "('" & Trim(CapitalTotal) & "','" & Trim(txtCuo) & "','" & Trim(txtTas) & "'," + vbCrLf +
                           "'" & Trim(Lapso) & "', '" & Trim(FechaDoc) & "', '" & Trim(CapPago) & "', '" & Trim(txtAnt) & "', '" & CuotaFija & "', '" & V_GastosA & "', '" & V_GastosA & "'" + vbCrLf +
                           "'" & Trim(Original) & "', :err, :tabla );" + vbCrLf +
                           "SELECT ""MES"" AS ""MES"", ""FECHA_PAGO"" AS ""FECHA DE PAGO"", ""SALDO_CAPITAL"" AS ""SALDO CAPITAL""" + vbCrLf +
                           ",""DIAS"" AS ""DIAS"", ""INTERES"" AS ""INTERES"", ""AMORT_CAP"" AS ""AMORTIZACION"", ""TOTAL_CUOTA"" AS ""CUOTA TOTAL""" + vbCrLf +
                           ",""POR_PAGAR_CAP"" AS ""CAPITAL POR PAGAR"", ""TOTAL_CAP_AMORT"" AS ""CAPITAL AMORTIZADO"", ""CUBIERTA"", ""GASTOSA"" AS ""GASTOS ADICIONALES"" FROM :tabla;" + vbCrLf +
                           "END"
                End If

                odatatable.ExecuteQuery(oStr)
                oGrid.DataTable = odatatable


                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        oGrid.RowHeaders.SetText(i, numLin)
                    Next
                End If
                oGrid.RowHeaders.TitleObject.Caption = "#"
                oGrid.Columns.Item(0).Editable = False
                oGrid.Columns.Item(1).Editable = True
                oGrid.Columns.Item(2).Editable = False
                oGrid.Columns.Item(3).Editable = False
                oGrid.Columns.Item(4).Editable = False
                oGrid.Columns.Item(5).Editable = False
                oGrid.Columns.Item(6).Editable = False
                oGrid.Columns.Item(7).Editable = False
                oGrid.Columns.Item(8).Editable = False
                oGrid.Columns.Item(9).Editable = False
                oGrid.Columns.Item(9).Visible = False
                oGrid.Columns.Item(10).Editable = False
                oGrid.AutoResizeColumns()

                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                BubbleEvent = False
            End Try

        End Sub

        Private Shared Sub cargarGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Try

                Dim CuotaDT As SAPbouiCOM.DataTable
                Dim oDBCab As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@EXX_CUOTA")
                Dim ogrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific
                Dim Cuotas As String = oDBCab.GetValue("U_Cuotas", 0)
                Dim Tasa As String = oDBCab.GetValue("U_TasaInteres", 0)
                Dim Anticipo As String = oDBCab.GetValue("U_Anticipo", 0)
                Dim DocEntryOV As String = oDBCab.GetValue("U_DocEntry", 0)
                Dim DocEntryPG As String = oDBCab.GetValue("DocNum", 0)
                Dim numLin As Integer
                Dim Original As Integer = 1
                Dim Lapso As Integer = DatosDB.GetParam("UD_Dia_Per")
                Dim CapPago As Decimal = 0.0
                Dim oDT As DataTable = DatosDB.GetParamOV(DocEntryOV)
                Dim Moneda As String = oDT.Rows(0).Item("CurSource").ToString 'mauricio
                Dim TipoMoneda As String = SBOFormOV.verificaMoneda(Moneda)
                Dim CapitalTotal As String = (oDT.Rows(0).Item(TipoMoneda).ToString).Replace(",", ".")
                Dim FechaDoc As String = Format(oDT.Rows(0).Item("DocDate"), "yyyy/MM/dd")
                Dim odatatable As SAPbouiCOM.DataTable = oForm.DataSources.DataTables.Item("DT_0")
                Dim odatatablePG As DataTable = DatosDB.GetParamCuotasFecha(DocEntryOV, DocEntryPG)
                Dim query As String
                Dim button As SAPbouiCOM.Button = oForm.Items.Item("btnRecalc").Specific
                button.Item.Visible = False


                If Motor = "SQL" Then
                    query = sqlData.GetParamProcedureCuotas_TEXT(Cuotas, Tasa, Anticipo, Lapso, Original, CapitalTotal, FechaDoc, CapPago)
                Else
                    query = hanaData.GetParamProcedureCuotas_TEXT(Cuotas, Tasa, Anticipo, Lapso, Original, CapitalTotal, FechaDoc, CapPago)
                End If
                odatatable.ExecuteQuery(query)


                oForm.DataSources.DataTables.Add("Cuotas")

                CuotaDT = oForm.DataSources.DataTables.Item("Cuotas")
                CuotaDT.Clear()

                CuotaDT.Columns.Add("MES", SAPbouiCOM.BoFieldsType.ft_Integer, 50)
                CuotaDT.Columns.Add("FECHA DE PAGO", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("SALDO CAPITAL", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("DIAS", SAPbouiCOM.BoFieldsType.ft_Integer, 10)
                CuotaDT.Columns.Add("INTERES", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("AMORTIZACION", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("CUOTA TOTAL", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("CAPITAL POR PAGAR", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("CAPITAL AMORTIZADO", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)
                CuotaDT.Columns.Add("GASTOS ADICIONALES", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 50)

                If odatatable.Rows.Count > 0 Then
                    For i As Integer = 0 To odatatable.Rows.Count - 1
                        CuotaDT.Rows.Add()
                        CuotaDT.SetValue("MES", i, Trim(odatatable.GetValue("MES", i)))
                        CuotaDT.SetValue("FECHA DE PAGO", i, Trim(odatatablePG.Rows(i).Item("U_FechaV").ToString))
                        CuotaDT.SetValue("SALDO CAPITAL", i, Trim(odatatable.GetValue("SALDO CAPITAL", i)))
                        CuotaDT.SetValue("DIAS", i, Trim(odatatable.GetValue("DIAS", i)))
                        CuotaDT.SetValue("INTERES", i, Trim(odatatable.GetValue("INTERES", i)))
                        CuotaDT.SetValue("AMORTIZACION", i, Trim(odatatable.GetValue("AMORTIZACION", i)))
                        CuotaDT.SetValue("CUOTA TOTAL", i, Trim(odatatable.GetValue("CUOTA TOTAL", i)))
                        CuotaDT.SetValue("CAPITAL POR PAGAR", i, Trim(odatatable.GetValue("CAPITAL POR PAGAR", i)))
                        CuotaDT.SetValue("CAPITAL AMORTIZADO", i, Trim(odatatable.GetValue("CAPITAL AMORTIZADO", i)))
                        CuotaDT.SetValue("GASTOS ADICIONALES", i, Trim(odatatable.GetValue("GASTOS ADICIONALES", i))) 'mauricio
                    Next
                End If

                ogrid.DataTable = CuotaDT
                If ogrid.Rows.Count > 0 Then
                    For i As Integer = 0 To ogrid.Rows.Count - 1
                        numLin = numLin + 1
                        ogrid.RowHeaders.SetText(i, numLin)
                    Next
                End If
                ogrid.RowHeaders.TitleObject.Caption = "#"
                ogrid.Columns.Item(0).Editable = False
                ogrid.Columns.Item(1).Editable = False
                ogrid.Columns.Item(2).Editable = False
                ogrid.Columns.Item(3).Editable = False
                ogrid.Columns.Item(4).Editable = False
                ogrid.Columns.Item(5).Editable = False
                ogrid.Columns.Item(6).Editable = False
                ogrid.Columns.Item(7).Editable = False
                ogrid.Columns.Item(8).Editable = False
                'ogrid.Columns.Item(9).Editable = False
                ogrid.Columns.Item(9).Editable = False

                ogrid.AutoResizeColumns()


            Catch ex As Exception
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Private Shared Sub bloquearItems(ByRef oForm As SAPbouiCOM.Form, Optional contador As Integer = Nothing)
            Dim DocEntryOV As SAPbouiCOM.EditText = oForm.Items.Item("edtDoc").Specific
            Dim Cuotas As SAPbouiCOM.EditText = oForm.Items.Item("Cuotas").Specific
            Dim Interes As SAPbouiCOM.EditText = oForm.Items.Item("Interes").Specific
            Dim Anticipo As SAPbouiCOM.EditText = oForm.Items.Item("Anticipo").Specific
            Dim InteresL As SAPbouiCOM.EditText = oForm.Items.Item("InteresL").Specific
            DocEntryOV.Item.Enabled = False

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                Cuotas.Item.Click()
                Cuotas.Item.Enabled = True
                Anticipo.Item.Enabled = True
            Else
                Cuotas.Item.Enabled = False
                Anticipo.Item.Enabled = False
            End If

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                Interes.Item.Enabled = False
                InteresL.Item.Enabled = False
            End If


        End Sub
        Private Shared Sub recalcular(oForm As SAPbouiCOM.Form, txtAnt As String, txtTas As String, txtCuo As String, BubbleEvent As Boolean)

            Dim odatatableGrid As New DataTable("Recalculo")
            Dim odataRow As DataRow
            Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific
            Dim Original As Integer = 1
            Dim odatatable As SAPbouiCOM.DataTable = oForm.DataSources.DataTables.Item("DT_0")
            'Dim Lapso As Integer = DatosDB.GetParam("UD_Dia_Per")
            Dim CapPago As Decimal = 0.0
            Dim Variable As String = String.Empty
            Dim VariableEnvio As String = String.Empty
            Dim CapitalTotal As String = oForm.DataSources.UserDataSources.Item("DocT").ValueEx
            Dim FechaDoc As String = oForm.DataSources.UserDataSources.Item("FechaD").ValueEx

            Try
                Randomize()
                Dim Aleatorio As Long = CLng((1 - 100) * Rnd() + 100)

                oForm.Freeze(True)

                'odatatableGrid.Columns.Add("#", GetType(String))
                'odatatableGrid.Columns.Add("MES", GetType(String))
                odatatableGrid.Columns.Add("CODIGO", GetType(String))
                odatatableGrid.Columns.Add("FECHA DE PAGO", GetType(String))
                'odatatableGrid.Columns.Add("SALDO CAPITAL", GetType(String))
                'odatatableGrid.Columns.Add("DIAS", GetType(String))
                'odatatableGrid.Columns.Add("INTERES", GetType(String))
                odatatableGrid.Columns.Add("AMORTIZACION", GetType(String))
                'odatatableGrid.Columns.Add("CUOTA TOTAL", GetType(String))
                'odatatableGrid.Columns.Add("CAPITAL POR PAGAR", GetType(String))
                'odatatableGrid.Columns.Add("GASTOS ADICIONALES", GetType(String))

                For i As Integer = 0 To oGrid.Rows.Count - 1
                    odataRow = odatatableGrid.NewRow
                    odataRow("CODIGO") = Aleatorio
                    odataRow("FECHA DE PAGO") = Format(odatatable.GetValue("FECHA DE PAGO", i), "dd/MM/yyyy")
                    odataRow("AMORTIZACION") = odatatable.GetValue("AMORTIZACION", i)
                    'Variable = CDate(odataRow("FECHA DE PAGO")) & " , " & odataRow("AMORTIZACION")
                    'VariableEnvio = VariableEnvio + Variable + Environment.NeqewLine
                    odatatableGrid.Rows.Add(odataRow)
                Next
                odatatableGrid.WriteXml(Application.StartupPath & "\foo.xml")
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                BubbleEvent = False
            End Try

        End Sub
#End Region
#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDOCuotas(oApp As SAPbouiCOM.Application, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" Then
                            If variableAdd = True Then
                                variableAdd = False
                                oForm.Close()
                                If oApp.Menus.Item("1304").Enabled = True Then
                                    oApp.ActivateMenuItem("1304")
                                    oForm = oApp.Forms.ActiveForm
                                    oForm.Items.Item("138").Click()
                                End If
                            End If
                        End If

                        'If pVal.ItemUID = "Item_6" Then
                        '    If pVal.BeforeAction = True Then
                        '        MsgBox("si")
                        '    End If
                        'End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.BeforeAction = False Then

                            Select Case pVal.ItemUID


                                Case "btnPag"
                                    Dim txtAnt As SAPbouiCOM.EditText = oForm.Items.Item("Anticipo").Specific
                                    Dim txtTas As SAPbouiCOM.EditText = oForm.Items.Item("Interes").Specific
                                    Dim txtCuo As SAPbouiCOM.EditText = oForm.Items.Item("Cuotas").Specific

                                    If validaGrid(oApp, oForm) = False Then
                                        If existePagos(oApp, oForm, txtCuo.Value) = True Then
                                            If ValidaPagos(oApp, oForm, txtCuo.Value) = True Then
                                                If anticipoMayor(oApp, oForm, txtAnt.Value) = True Then
                                                    loadFormSettings(oForm, txtAnt.Value, txtTas.Value, txtCuo.Value, BubbleEvent)
                                                End If
                                            Else
                                                BubbleEvent = False
                                                GoTo fail
                                            End If
                                        Else
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Else
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                Case "btnBor"

                                    If validaBorrar(oApp, oForm) = True Then
                                        oApp.StatusBar.SetText("[FN] Se elimino el plan de pagos correctamente.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        BubbleEvent = True
                                    Else
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                Case "btnRecalc"

                                    Dim txtAnt As SAPbouiCOM.EditText = oForm.Items.Item("Anticipo").Specific
                                    Dim txtTas As SAPbouiCOM.EditText = oForm.Items.Item("Interes").Specific
                                    Dim txtCuo As SAPbouiCOM.EditText = oForm.Items.Item("Cuotas").Specific

                                    recalcular(oForm, txtAnt.Value, txtTas.Value, txtCuo.Value, BubbleEvent)

                                Case "1"

                                    If pVal.Action_Success = True Then

                                        If ValidacionesFechas(oApp, oForm) = False Then
                                            oApp.StatusBar.SetText("[FN] Existen fechas errores.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If

                                    End If

                            End Select
                        End If


                End Select

fail:
            Catch ex As Exception
            oForm.Freeze(False)
            oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

        Public Shared Sub AdminSBOFormUDOCuotaEve(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)

            Dim oForm As SAPbouiCOM.Form
            oForm = oApp.Forms.Item(pVal.FormUID)

            Try
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD


                        oForm.Freeze(True)

                        If pVal.BeforeAction = False Then
                            oForm.Freeze(True)

                            If Not addDatos(oApp, pVal, oForm) Then
                                BubbleEvent = False
                                GoTo fail
                            Else
                                oForm.Freeze(False)
                            End If

                            If pVal.ActionSuccess = True Then
                                variableAdd = True
                            End If
                        Else
                            Dim res As Integer = oApp.MessageBox("[FIN] Esta seguro de crear el plan de pagos?", 1, "Si", "No")

                            If res <> 1 Then
                                BubbleEvent = False
                                GoTo fail
                            End If

                            Dim oGrid As SAPbouiCOM.Grid
                            oGrid = oForm.Items.Item("Item_6").Specific
                            If oGrid.Columns.Count = 0 Then
                                BubbleEvent = False
                                GoTo fail
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                        If pVal.BeforeAction = False Then
                            If pVal.ActionSuccess = True Then
                                oForm.Freeze(True)
                                bloquearItems(oForm)
                                cargarGrid(oApp, oForm)
                                oForm.Freeze(False)
                            End If
                        End If

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub

        Private Shared Function ValidacionesFechas(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim ogrid As SAPbouiCOM.Grid = oForm.Items.Item("Item_6").Specific
            Dim FechaVencimientoA, FechaVencimientoB As Date
            Try
                oForm.Freeze(True)

                If ogrid.Rows.Count > 1 Then

                    For i As Integer = 0 To ogrid.Rows.Count - 1

                        FechaVencimientoA = CDate(ogrid.DataTable.GetValue("FECHA DE PAGO", i))

                        If Not ogrid.Rows.Count = i + 1 Then
                            FechaVencimientoB = CDate(ogrid.DataTable.GetValue("FECHA DE PAGO", i + 1))
                        End If

                        If FechaVencimientoA > FechaVencimientoB Then
                            oForm.Items.Item("Cuotas").Click()
                            Return False
                        End If

                    Next

                End If

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Sub AdminSBOFormUDOCuotaNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)

            Try
                If oForm.TypeEx = "UDO_FT_CUOTA" Then
                    If pVal.BeforeAction = True Then
                        Select Case pVal.MenuUID
                            Case "1281", "1282"
                                BubbleEvent = False
                        End Select
                    End If
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub
        Private Sub Grid1_GotFocusAfter(sboObject As System.Object, pVal As SAPbouiCOM.SBOItemEventArg)

        End Sub
#End Region
    End Class
End Namespace

﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOSelectInteres
#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, ByRef codigo As String, ByRef nombre As String)
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOSeInt")
                If blnexit Then
                    Exit Sub
                End If
                strPath = Application.StartupPath & "\SBOSelectInteres.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)
                'oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm = oApp.Forms.Item("SBOSeInt")
                PopulaGrid(oApp, oForm, codigo, nombre)
                oForm.Visible = True
            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Public Shared Sub MarcarFilaParaEdicion(ByRef oform As SAPbouiCOM.Form, ItemUID As String, Row As Integer)
            Try
                oform.Freeze(True)
                Dim oGrid As SAPbouiCOM.Grid = oform.Items.Item(ItemUID).Specific


                oGrid.DataTable.SetValue("Change", Row, "Y")
                oform.Freeze(False)
            Catch ex As Exception
                oform.Freeze(False)
                Throw ex
            End Try
        End Sub

        Private Shared Sub PopulaGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef code As String, ByRef name As String)

            Dim gridLst As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim numLin As String = ""
            Dim query As String = String.Empty
            Dim cond As Boolean = False
            Dim EsImpMar As String = "N"
            Dim ImpMar As String = ""
            Dim rate As Double = 0
            Dim oColumns2 As SAPbouiCOM.EditTextColumn
            Dim edtEmp As SAPbouiCOM.EditText
            Dim edtEmpN As SAPbouiCOM.EditText


            Try

                oForm.Freeze(True)
                'oForm = oApp.Forms.Item(FormUID)
                edtEmp = oForm.Items.Item("CardCod").Specific
                edtEmpN = oForm.Items.Item("Nombr").Specific
                gridLst = oForm.Items.Item("gridLst").Specific
                edtEmp.Value = code
                edtEmpN.Value = name
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("MAT_LIST")
                If Motor = "SQL" Then
                    query = "select 'N' as Change, DocEntry as Codigo, 'N' as Seleccion from [@EXX_INTERESES] where U_CardCode ='" & code & "'"
                Else
                    query = "select 'N' AS ""Change"" ,""" & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"".""DocEntry"" As ""Codigo"",""" & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"".""CreateDate"" As ""Fecha"" , 'N' as ""Seleccion"" FROM """ & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"" where """ & SQLBaseDatos.ToUpper & """.""@EXX_INTERESES"".""U_CardCode"" ='" & code & "';"
                End If

                oDT.ExecuteQuery(query)
                gridLst.DataTable = oDT
                If gridLst.Rows.Count > 0 Then
                    For i As Integer = 0 To gridLst.Rows.Count - 1
                        numLin = oDT.GetValue("Codigo", i).ToString
                        If Not String.IsNullOrEmpty(numLin) Then
                            gridLst.RowHeaders.SetText(i, i + 1)
                        Else
                            gridLst.RowHeaders.SetText(i, "")
                        End If
                    Next
                End If

                gridLst.Columns.Item("Change").Editable = True
                gridLst.Columns.Item("Change").Visible = False
                oColumns = gridLst.Columns.Item("Codigo")
                gridLst.Columns.Item("Codigo").Editable = False
                oColumns = gridLst.Columns.Item("Fecha")
                gridLst.Columns.Item("Fecha").Editable = False

                oColumns2 = gridLst.Columns.Item("Seleccion")
                oColumns2.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox

                gridLst.AutoResizeColumns()
                oForm.Freeze(False)
                oForm.Update()
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDOFECLTSEIN(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "gridLst"
                                    oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                    If pVal.Row >= 0 Then

                                        If pVal.ItemChanged = True Then
                                            oForm.Freeze(True)
                                            MarcarFilaParaEdicion(oForm, pVal.ItemUID, pVal.Row)
                                            oForm.Freeze(False)
                                        End If
                                    End If
                            End Select
                        Else
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "1"
                                    'oForm.Items.Item("btnActt").Enabled = False
                                    Dim gridLst As SAPbouiCOM.Grid

                                    Try
                                        oForm = oApp.Forms.Item(pVal.FormUID.ToString()) 'cambio actual
                                        Dim valor As String
                                        Dim oDT As SAPbouiCOM.DataTable
                                        Dim edtEmpp As SAPbouiCOM.EditText
                                        Dim carcode As String
                                        oForm.Freeze(True)

                                        edtEmpp = oForm.Items.Item("CardCod").Specific
                                        carcode = edtEmpp.Value
                                        gridLst = oForm.Items.Item("gridLst").Specific
                                        oDT = gridLst.DataTable
                                        'aqui empieza
                                        For i As Integer = 0 To gridLst.Rows.Count - 1
                                            Dim Cod As String
                                            Dim sel As String

                                            sel = Trim(gridLst.DataTable.Columns.Item("Seleccion").Cells.Item(i).Value)
                                            Cod = Trim(gridLst.DataTable.Columns.Item("Codigo").Cells.Item(i).Value)
                                            valor = Trim(gridLst.DataTable.Columns.Item("Codigo").Cells.Item(i).Value)
                                            If Trim(sel) = "Y" Then

                                                'oApp.MessageBox("el udo es " & valor)
                                                'oApp.MessageBox("el codigo de socio es " & carcode)
                                                oForm.Visible() = False

                                                SBOFormNewPagRec.CreaSBOForm2(oApp, carcode, valor)
                                            End If

                                        Next
                                        'oForm.Close()
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                    'oForm.Items.Item("btnEdt").Enabled = True

                                    'gridLst.Columns.Item("Seleccion").Visible = False
                            End Select
                        Else
                        End If
                End Select
fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub


#End Region

#Region "VALIDACIONES"

#End Region

    End Class
End Namespace





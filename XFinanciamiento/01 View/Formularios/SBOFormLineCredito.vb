﻿Imports SAPbobsCOM
Imports System.Globalization
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles

Namespace View
    Public Class SBOFormLineCredito
        Shared oDTanexo As System.Data.DataTable
        Shared oDBCab As SAPbouiCOM.DBDataSource
        Shared oGrid As SAPbouiCOM.Grid
        Shared panelGrid As String = "1"

#Region "VALIDACIONES"
        Private Shared Function ValidaFechaDe(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBFecha As SAPbouiCOM.DBDataSource
            Dim FechaDe As String
            Try
                oDBFecha = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                FechaDe = Trim(oDBFecha.GetValue("U_FechaDe", 0))

                If Trim(FechaDe) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Debe llenar el campo de fecha inicio antes de grabar el formulario")
                    Return False
                Else
                    Return True
                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function
        Private Shared Function validaFechaRango(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBFecha As SAPbouiCOM.DBDataSource
            Dim FechaDe, FechaHa As Date
            Dim FechaH As String = String.Empty
            Dim FechaD As String = String.Empty
            Dim intr As Integer
            Try

                oDBFecha = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                FechaD = Trim(oDBFecha.GetValue("U_FechaDe", 0))
                FechaH = Trim(oDBFecha.GetValue("U_FechaHa", 0))

                If Trim(FechaD) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("La fecha inicio no puede ser vacia. Revise e intente nuevamente")
                    Return False
                End If

                If Trim(FechaH) = "" Then
                    FechaH = "29991231"
                End If

                FechaDe = Date.ParseExact(FechaD, "yyyyMMdd", CultureInfo.InvariantCulture)
                FechaHa = Date.ParseExact(FechaH, "yyyyMMdd", CultureInfo.InvariantCulture)

                intr = DateTime.Compare(FechaDe, FechaHa)

                Select Case intr
                    Case 1
                        oApp.MessageBox("La fecha inicio no puede se mayor a la fecha fin.")
                        oForm.Freeze(False)
                        Return False
                    Case 0
                        oApp.MessageBox("La fechas son similares.")
                        oForm.Freeze(False)
                        Return False
                    Case Else
                        Return True
                End Select

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function ValidaFechaHa(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBFecha As SAPbouiCOM.DBDataSource
            Dim FechaDe As String
            Try
                oDBFecha = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                FechaDe = Trim(oDBFecha.GetValue("U_FechaHa", 0))

                If Trim(FechaDe) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Debe elegir una fecha antes de grabar el formulario")
                    Return False
                Else
                    Return True
                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ValidaSN(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oSocio As String = String.Empty

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                oSocio = Trim(oDBCab.GetValue("U_CardCode", 0))

                If oSocio = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Debe seleccionar un Socio de Negocio antes de grabar la línea de credito.")
                    Return False
                End If

                Return True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ValidaBtnAprobar(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim btnAproba As SAPbouiCOM.ButtonCombo
                    Dim btnModFec As SAPbouiCOM.Button

                    Dim DocEntry As String = String.Empty

                    Dim Accion As String = String.Empty

                    Dim oDBCab As SAPbouiCOM.DBDataSource

                    Try

                        oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                        btnAproba = oForm.Items.Item("btnAproba").Specific
                        btnModFec = oForm.Items.Item("btnModFec").Specific
                        DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))

                        If DocEntry = "" Then
                            Return False
                        End If

                        Return True

                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        Return False
                    End Try


                Else
                    oForm.Freeze(False)
                    Return False
                End If
            Else
                oForm.Freeze(False)
                oApp.MessageBox("No se puede realizar una aprobación en modo crear. Guarde el formulario e intente nuevamente")
                Return False
            End If


        End Function
#End Region

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByRef oApp As SAPbouiCOM.Application)
            Try
                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "LICRE", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)
                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm
                If Not Left(oForm.TypeEx, 1) = "-" Then
                    If Not FormatItems(oApp, oForm) Then
                        oForm.Freeze(False)
                        oApp.MessageBox("Problemas al formatear items")
                    End If

                    If Not PopulaFest(oApp, oForm) Then 'CNP AUTORIZACION
                        ' oForm.Close()
                        Exit Sub
                    End If

                    Dim oDBCab As SAPbouiCOM.DBDataSource

                    oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        oDBCab.SetValue("U_EstadoAp", 0, "-")
                        oDBCab.SetValue("U_Estado", 0, "0")
                    End If

                    LoadCombos(oApp, oForm)
                    bloquearCampos(oApp, oForm)

                    Dim btnRegistra As SAPbouiCOM.Button
                    btnRegistra = oForm.Items.Item("btnReq").Specific
                    btnRegistra.Caption = "Registrar"

                End If



            Catch ex As Exception
                oForm.Freeze(False)
                'oForm.Visible = True
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try

        End Sub

        Public Shared Sub ManejaGridChecklist(ByVal oApp As SAPbouiCOM.Application, ByVal nroDoc As String, ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim query As String = ""
            Dim numLin As Integer = 0
            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim codigochk As String = String.Empty
            Dim descchk As String = String.Empty
            Dim codigochkBD As String = String.Empty
            Dim descchkBD As String = String.Empty
            Dim lineidBD As Integer
            Dim oColumn As SAPbouiCOM.GridColumn
            Dim PartirDe As Integer = 0
            Dim dispocheck As String = String.Empty


            Dim oDataSourceC As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

            Try
                oForm.Freeze(True)
                oGrid = oForm.Items.Item("Item_6").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dtCheck")

                query = DatosDB.PopulaGridChecklist(nroDoc)

                oDT.ExecuteQuery(query)
                oGrid.DataTable = oDT

                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        oGrid.RowHeaders.SetText(i, numLin)
                    Next
                End If

                oGrid.Columns.Item("CODIGO").Visible = True
                oGrid.Columns.Item("CODIGO").Editable = False
                oGrid.Columns.Item("DESCRIPCION").Visible = True
                oGrid.Columns.Item("DESCRIPCION").Editable = False
                oGrid.Columns.Item("REALIZADO").Visible = True
                oGrid.Columns.Item("REALIZADO").Editable = True
                oGrid.Columns.Item("LINEABD").Visible = False
                oGrid.Columns.Item("LINEABD").Editable = False
                oGrid.Columns.Item("CODIGOBD").Visible = False
                oGrid.Columns.Item("CODIGOBD").Editable = False
                oGrid.Columns.Item("DESCRBD").Visible = False
                oGrid.Columns.Item("DESCRBD").Editable = False
                oColumns = oGrid.Columns.Item("REALIZADO")
                oColumns.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox

                oGrid.AutoResizeColumns()
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        ' ssh
        Public Shared Sub ManejaGridTasas(ByVal oApp As SAPbouiCOM.Application, ByVal nroDoc As String, ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim query As String = ""
            Dim numLin As Integer = 0
            Dim oColumns As SAPbouiCOM.EditTextColumn
            Dim codigochk As String = String.Empty
            Dim descchk As String = String.Empty
            Dim codigochkBD As String = String.Empty
            Dim descchkBD As String = String.Empty
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim PartirDe As Integer = 0
            Dim dispocheck As String = String.Empty

            Dim oDataSourceC As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

            Try
                oForm.Freeze(True)
                oGrid = oForm.Items.Item("dbTasas").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dbTasas")

                query = DatosDB.PopulaGridTasas(nroDoc)

                oDT.ExecuteQuery(query)
                oGrid.DataTable = oDT
                'If oGrid.DataTable.GetValue(2, oGrid.DataTable.Rows.Count - 1) = Nothing Then
                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        oGrid.RowHeaders.SetText(i, numLin)
                    Next
                End If
                'End If

                oGrid.Columns.Item("CODIGO").Visible = False
                oGrid.Columns.Item("CODIGO").Editable = False
                oGrid.Columns.Item("CODGRUPART").Visible = True
                oGrid.Columns.Item("CODGRUPART").Editable = True
                oGrid.Columns.Item("CODGRUPART").TitleObject.Caption = "Codigo Grupo Articulo"
                oGrid.Columns.Item("DESCRIPCION").Visible = True
                oGrid.Columns.Item("DESCRIPCION").Editable = False
                oGrid.Columns.Item("DESCRIPCION").TitleObject.Caption = "Descripcion Grupo Articulo"
                oGrid.Columns.Item("TASANOR").Visible = True
                oGrid.Columns.Item("TASANOR").Editable = True
                oGrid.Columns.Item("TASANOR").TitleObject.Caption = "Tasa Normal"
                oGrid.Columns.Item("TASALEG").Visible = True
                oGrid.Columns.Item("TASALEG").Editable = True
                oGrid.Columns.Item("TASALEG").TitleObject.Caption = "Tasa Legal"
                'oColumns = oGrid.Columns.Item("CODGRUPART")
                'oColumns.Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
                oGrid.Columns.Item("CODGRUPART").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
                'oCombo = oGrid.Columns.Item("CODGRUPART").Specific
                oCombo = oGrid.Columns.Item("CODGRUPART")
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                'oCombo.Item.AffectsFormMode = False
                'oGrid.AllowUserToAddRows = True

                'oGrid.Rows.

                If Not PopulaGrupoArticulosCmb(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el Grupo de Articulos.")
                    oForm.Freeze(True)
                End If

                'Grid.AutoResizeColumns()
                If oGrid.DataTable.GetValue(2, oGrid.DataTable.Rows.Count - 1) = Nothing Then
                    'The table contains no rows.

                Else
                    'Use lastRow here.
                    oGrid.DataTable.Rows.Add()
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Public Shared Function GuardaGrdChecklist(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim grdChk As SAPbouiCOM.Grid
            Dim oDTChk As SAPbouiCOM.DataTable
            Dim codigo As String = String.Empty
            Dim cheklist As String = String.Empty
            Dim realizado As String = String.Empty
            Dim codeBD As String = String.Empty
            Dim codigoBD As String = String.Empty
            Dim checklistBD As String = String.Empty
            Dim DocEntry As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource



            Try
                grdChk = oForm.Items.Item("Item_6").Specific
                oDTChk = grdChk.DataTable

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))

                If Trim(DocEntry) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("La linea de credito debe tener un código asignado. Guarda el formulario e intente nuevamente.")
                End If


                If oDTChk.Rows.Count > 0 Then
                    For i As Integer = 0 To oDTChk.Rows.Count - 1

                        codigo = Trim(oDTChk.GetValue("CODIGO", i))
                        cheklist = Trim(oDTChk.GetValue("DESCRIPCION", i))
                        realizado = Trim(oDTChk.GetValue("REALIZADO", i))
                        codeBD = Trim(oDTChk.GetValue("LINEABD", i))
                        codigoBD = Trim(oDTChk.GetValue("CODIGOBD", i))
                        checklistBD = Trim(oDTChk.GetValue("DESCRBD", i))


                        If Trim(realizado) = "" Then
                            oForm.Freeze(False)
                            oApp.MessageBox("No se tiene un valor para la realización de la tarea :  " & Trim(cheklist))
                            Continue For
                        End If


                        If Trim(codeBD) = "-1" Then

                            If Not DatosDB.InsertaChecklist(DocEntry, codigo, cheklist, realizado) Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al ingresar el registro para el checklist :  " & Trim(cheklist))
                                Continue For
                            End If

                        Else
                            If Not DatosDB.UpdateChecklist(Trim(codeBD), Trim(realizado)) Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Ocurrio un error al actualizar el registro para el checklist:  " & Trim(cheklist))
                                Continue For
                            End If

                        End If


                    Next
                End If

                Return True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        'ssh
        Public Shared Function GuardaGrdTasasGrupArt(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim grdTasas As SAPbouiCOM.Grid
            Dim oDTChk As SAPbouiCOM.DataTable
            Dim codigo As String = String.Empty
            Dim codGrupArt As String = String.Empty
            Dim TasaN As String = String.Empty
            Dim TasaL As String = String.Empty
            Dim codeBD As String = String.Empty
            Dim codigoBD As String = String.Empty
            'Dim checklistBD As String = String.Empty
            Dim DocEntry As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                grdTasas = oForm.Items.Item("dbTasas").Specific
                oDTChk = grdTasas.DataTable

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))

                If Trim(DocEntry) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("La linea de credito debe tener un código asignado. Guarda el formulario e intente nuevamente.")
                End If

                If grdTasas.DataTable.GetValue(2, grdTasas.DataTable.Rows.Count - 1) = Nothing Then
                    oDTChk.Rows.Remove(oDTChk.Rows.Count - 1)
                End If

                Dim estado As String = String.Empty
                Dim autorizacion As String = String.Empty
                estado = Trim(oDBCab.GetValue("U_Estado", 0))
                autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                If estado <> "0" Or estado = "" Or autorizacion = "P" Then
                    oApp.MessageBox("La linea de crèdito esta pendiente o aprobada y las tasas no se editarán.")
                Else
                    If oDTChk.Rows.Count > 0 Then
                        If HasDuplicates(oDTChk) Then
                            oForm.Freeze(False)
                            oApp.MessageBox("No se puede crear dos registros de Tasas repetidos")
                        Else
                            If (IsValided(oDTChk)) Then

                                If DatosDB.LimpiarTasasGrupArt(DocEntry) Then
                                    For i As Integer = 0 To oDTChk.Rows.Count - 1

                                        codigo = Trim(DocEntry) ' Trim(oDTChk.GetValue("CODIGO", i))
                                        codGrupArt = Trim(oDTChk.GetValue("CODGRUPART", i))

                                        If codGrupArt IsNot Nothing Or Not codGrupArt = 0 Then

                                            TasaN = Trim(oDTChk.GetValue("TASANOR", i))
                                            TasaL = Trim(oDTChk.GetValue("TASALEG", i))

                                            'MsgBox("codGrupArt:" & codGrupArt & "TasaN:" & TasaN & "TasaL:" & TasaL)

                                            If Not DatosDB.InsertaTasasGrupArt(Trim(codigo), Trim(codGrupArt), Trim(TasaN), Trim(TasaL)) Then
                                                oForm.Freeze(False)
                                                oApp.MessageBox("Ocurrio un error al actualizar el registro para el Tasas:  " & Trim(codGrupArt))
                                                Continue For

                                            End If


                                        Else
                                            oForm.Freeze(False)
                                            oApp.MessageBox("Solo una de las Tasas puede contener el valor 0 :")
                                            Continue For
                                        End If

                                    Next


                                Else
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Error: El registro para el Tasas es nulo o 0")
                                End If

                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("Al menos una tasa de interes debe contener un valor mayor a cero y menor a 100")
                            End If
                        End If
                    Else
                        If oDTChk.Rows.Count = 0 Then
                            If DatosDB.LimpiarTasasGrupArt(DocEntry) Then
                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("Error: El registro para el Tasas es nulo o 0")
                            End If
                        End If
                    End If
                End If
                'MsgBox(grdTasas.DataTable.Rows.Count)
                If grdTasas.DataTable.Rows.Count > 0 Then
                    If grdTasas.DataTable.GetValue(2, grdTasas.DataTable.Rows.Count - 1) = Nothing Then
                    Else
                        grdTasas.DataTable.Rows.Add()
                    End If
                Else
                    grdTasas.DataTable.Rows.Add()
                    oApp.MessageBox("Se elimino el registro porfavor reinicie el formulario para continuar")
                    Return True
                End If
                Try
                    ManejaGridTasas(oApp, Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)

                End Try


                Return True

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function PopulaFest(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean 'CNP AUTORIZACION

            Try
                Dim cmbEst As SAPbouiCOM.ComboBox
                Dim oDT As DataTable = DatosDB.PopulaFest("LICRE")
                Dim codigo As String = String.Empty
                Dim desc As String = String.Empty

                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    cmbEst = oForm.Items.Item("cmbEstado").Specific
                    cmbEst.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                    If cmbEst.ValidValues.Count > 0 Then
                        If oDT.Rows.Count > 0 Then
                            For i As Integer = 0 To cmbEst.ValidValues.Count - 1
                                cmbEst.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                            Next

                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Estado")
                                cmbEst.ValidValues.Add(codigo, desc)
                            Next row
                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los estados de formulario.")
                            Return False
                        End If
                    Else
                        If oDT.Rows.Count > 0 Then
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Estado")
                                cmbEst.ValidValues.Add(codigo, desc)
                            Next row
                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los estados de formulario.")
                            Return False
                        End If
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los estados de formulario.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function PopulaEstadosCmb(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosDB.GetEstadosCmb("LICRE")
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Estado")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los estados para este formulario.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los formularios.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        Private Shared Function PopulaGarantiasCmb(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = BDLineaCredito.PopulaCmbGar()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("DocEntry")
                                desc = row.Item("U_Descr")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se deben crear plantillas de garantias.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se deben crear plantillas de garantias.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Function PopulaRequisitosCmb(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = BDLineaCredito.PopulaCmbReq()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("DocEntry")
                                desc = row.Item("U_Descr")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se deben crear plantillas de requisitos.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se deben crear plantillas de requisitos.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        'ssh
        Private Shared Function PopulaGrupoArticulosCmb(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBoxColumn) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = BDLineaCredito.PopulaCmbGrupArt()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("ItmsGrpCod")
                                desc = row.Item("ItmsGrpNam")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("No existen Grupos de Articulos.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No existen Grupos de Articulos.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Public Shared Function ChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "CardCode"
                    bBubbleEvent = SNChooseFromList(FormUID, oEvent, oApplication, oCompany)
                Case "edtProj"
                    bBubbleEvent = PrjChooseFromList(FormUID, oEvent, oApplication, oCompany)

                Case Else
            End Select

            Return bBubbleEvent
        End Function

        Public Shared Function PrjChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application,
                                ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable

                    Dim oDBCab As SAPbouiCOM.DBDataSource

                    oForm = oApplication.Forms.Item(FormUID)

                    oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")


                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oDBCab.SetValue("U_PrjName", 0, Trim(oDataTable.GetValue("PrjName", 0)))
                            oDBCab.SetValue("U_Project", 0, Trim(oDataTable.GetValue("PrjCode", 0)))
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'oForm.Freeze(False)
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function

        Public Shared Function SNChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application,
                                                  ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try

                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim edtEmp As SAPbouiCOM.EditText
                    Dim edtEmpN As SAPbouiCOM.EditText

                    oForm = oApplication.Forms.Item(FormUID)
                    'oForm.Freeze(True)
                    edtEmp = oForm.Items.Item("CardCode").Specific
                    edtEmpN = oForm.Items.Item("Nombre").Specific
                    oDataTable = Utiles.Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            edtEmpN.Value = oDataTable.GetValue("CardName", 0)
                            edtEmp.Value = oDataTable.GetValue("CardCode", 0)
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'oForm.Freeze(False)
                Return bBubbleEvent

            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try
        End Function

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Private Shared Function FormatItems(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim cmbBox As SAPbouiCOM.ComboBox
            Dim oBtnCombo As SAPbouiCOM.ButtonCombo

            Try

                cmbBox = oForm.Items.Item("cmbOperF").Specific
                cmbBox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                cmbBox.Item.AffectsFormMode = False

                'boton de autorizacion
                oBtnCombo = oForm.Items.Item("btnAproba").Specific
                oBtnCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oBtnCombo.Caption = "Autorizacion"

                cmbBox = oForm.Items.Item("cmbEstado").Specific
                cmbBox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                cmbBox = oForm.Items.Item("cmbEstAp").Specific
                cmbBox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                cmbBox = oForm.Items.Item("cmbOperG").Specific
                cmbBox.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                cmbBox.Item.AffectsFormMode = False

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        Private Shared Function ReseteaAutorizaciones(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Code As String) As Boolean
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            Dim sCmp As SAPbobsCOM.CompanyService

            Try


                Try
                    sCmp = DiApp.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService("LICRE")
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    oGeneralParams.SetProperty("Code", Trim(Code))
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    oGeneralData.SetProperty("U_Etapa", "")
                    oGeneralData.SetProperty("U_LinEtapa", "")
                    oGeneralData.SetProperty("U_Estado", "0")
                    oGeneralData.SetProperty("U_EstadoAp", "-")
                    oGeneralService.Update(oGeneralData)
                Catch ex As Exception
                    oForm.Freeze(False)
                    oApp.MessageBox(ex.Message)
                    Return False
                End Try
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Private Shared Function ManejaBtnReset(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim Presupuesto As String = String.Empty
            Dim Correlativo As String = String.Empty
            Dim DocEntry As String = String.Empty
            Dim existe As String = String.Empty
            Dim usuario As String = String.Empty
            Dim formulario As String = String.Empty
            Dim estado As String = String.Empty
            Dim etapa As String = String.Empty
            Dim linetapa As String = String.Empty
            Dim PresEntry As String = String.Empty
            Dim CorreSol As String = String.Empty
            Dim tipo As String = String.Empty
            Dim Code As String = String.Empty

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                Presupuesto = ""
                Correlativo = ""
                DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))
                Code = Trim(oDBCab.GetValue("Code", 0))

                If Trim(DocEntry) = "" Then
                    Return False
                End If
                If Trim(Code) = "" Then
                    Return False
                End If

                If ReseteaAutorizaciones(oApp, oForm, Code) Then

                    DatosAutorizaciones.InsertaAutorizacionLog("LICRE", Code, Presupuesto, Correlativo, "X", "Reset", "X", "Reset", "", Trim(DiApp.UserSignature), Trim(DiApp.UserName), DiApp.GetCompanyDate.ToString("yyyyMMdd"), "", "")


                    RefrescaDatosMaestro(oApp, oForm)

                    usuario = Trim(DiApp.UserSignature)
                    formulario = "LICRE"
                    estado = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                    etapa = Trim(oDBCab.GetValue("U_Etapa", 0))
                    linetapa = Trim(oDBCab.GetValue("U_LinEtapa", 0))
                    DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))
                    PresEntry = ""
                    CorreSol = Trim(oDBCab.GetValue("U_CorreSol", 0))

                    If Not DatosAutorizaciones.ManejaBtnAcc(oApp, oForm, Trim(usuario), Trim(formulario), Trim(estado), Trim(etapa), Trim(linetapa), Trim(DocEntry), Trim(PresEntry), Trim(CorreSol)) Then

                    End If
                    Return True
                Else
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function ManejaBtnOrdVnt(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oBtn As SAPbouiCOM.Button
            Dim estado As String = String.Empty

            Try
                oForm.Freeze(True)

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                oBtn = oForm.Items.Item("btnGenDoc").Specific

                estado = Trim(oDBCab.GetValue("U_Estado", 0))

                If Trim(estado) = "102" Then
                    oBtn.Item.Enabled = True
                Else
                    oBtn.Item.Enabled = False
                End If

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Sub RefrescaDatosMaestro(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Try
                Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                Dim oCondition As SAPbouiCOM.Condition
                Dim oDBCab As SAPbouiCOM.DBDataSource
                Dim Code As String = String.Empty

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                Code = Trim(oDBCab.GetValue("DocEntry", 0))
                oCondition = oConditions.Add()
                oCondition.Alias = "DocEntry"
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCondition.CondVal = Code
                oDBCab.Query(oConditions)

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

        Private Shared Sub verificaBloqueo(ByRef oApp As Application, ByRef oForm As Form)
            Try




            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub addImgAtt(PathFuente As String, ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Dim fileName As String = String.Empty
            Dim PathDestino As String = String.Empty
            Dim oAttDB As SAPbobsCOM.UserTable
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                fileName = PathFuente.Substring(PathFuente.LastIndexOf("\") + 1)

                Dim key As String = ""
                Dim res As Integer
                Dim oRecord As SAPbobsCOM.Recordset
                Dim oCode As Integer = 0
                Dim oName As Integer = 0


                oRecord = DiApp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecord.DoQuery("select Max(Code) as Code from [@EXX_IATT]")
                If oRecord.RecordCount > 0 Then
                    oRecord.MoveFirst()
                    If oRecord.Fields.Item("Code").Value <> "" Then
                        oCode = oRecord.Fields.Item("Code").Value
                        oCode = oCode + 1
                    Else
                        oCode = 1
                    End If
                Else
                    oCode = 1
                End If

                oAttDB = DiApp.UserTables.Item("EXX_IATT")
                oAttDB.Code = oCode

                fileName = "EXX_LICRE" & "-" & Trim(oDBCab.GetValue("Code", 0)) & "-" & oCode.ToString() & "-" & fileName
                PathDestino = DiApp.BitMapPath + fileName
                Try
                    IO.File.Copy(PathFuente, PathDestino, True)
                Catch ex As Exception
                    oApp.MessageBox(ex.Message)
                    Exit Sub
                End Try

                oRecord.DoQuery("select Max(Name) as Name from [@EXX_IATT] where U_DocCode = '" & oDBCab.GetValue("DocEntry", 0) & "'")
                If oRecord.RecordCount > 0 Then
                    oRecord.MoveFirst()
                    If oRecord.Fields.Item("Name").Value <> "" Then
                        oName = oRecord.Fields.Item("Name").Value
                        oName = oName + 1
                    Else
                        oName = 1
                    End If
                Else
                    oName = 1
                End If

                oAttDB.Name = oName
                oAttDB.UserFields.Fields.Item("U_ObjCode").Value = "LICRE"
                oAttDB.UserFields.Fields.Item("U_DocCode").Value = Trim(oDBCab.GetValue("DocEntry", 0))
                oAttDB.UserFields.Fields.Item("U_Path").Value = PathDestino
                oAttDB.UserFields.Fields.Item("U_AttDate").Value = DiApp.GetCompanyDate
                res = oAttDB.Add()
                If res = 0 Then
                    ManejaMatIAtt(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                    oApp.MessageBox("Se ha añadido el documento correctamente")
                Else
                    oApp.MessageBox(DiApp.GetLastErrorDescription)
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecord)
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
            End Try

        End Sub
        ' ssh
        Public Shared Function ActualizarDescGrupArt(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDT As SAPbouiCOM.DataTable
            Dim oGrid As SAPbouiCOM.Grid
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oCol As SAPbouiCOM.GridColumn
            Dim oPos As SAPbouiCOM.ICellPosition
            Dim rowNum As Integer
            Dim colNum As Integer
            Dim CodGrupArt As String
            Dim DescGrupArt As String

            Try
                oForm.Freeze(True)
                oDTs = oForm.DataSources.DataTables
                'Dim oGrid As SAPbouiCOM.Grid
                'Dim ruta As String = String.Empty

                oGrid = oForm.Items.Item("dbTasas").Specific
                'MsgBox(oGrid.Rows.SelectedRows.Count)
                oPos = oGrid.GetCellFocus()
                rowNum = oPos.rowIndex
                colNum = oPos.ColumnIndex
                CodGrupArt = oGrid.DataTable.GetValue(1, rowNum)
                DescGrupArt = DatosDB.GetDescGrupoArt(CodGrupArt)

                oGrid.DataTable.SetValue(2, rowNum, DescGrupArt)

                If oGrid.DataTable.GetValue(2, oGrid.DataTable.Rows.Count - 1) = Nothing Then
                    'The table contains no rows.

                Else
                    'Use lastRow here.
                    oGrid.DataTable.Rows.Add()
                End If

                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        ' ssh
        Public Shared Function ValidarTasas(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oGrid As SAPbouiCOM.Grid
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oPos As SAPbouiCOM.ICellPosition
            Dim rowNum As Integer
            Dim colNum As Integer
            Dim CodGrupArt As String
            Dim DescGrupArt As String
            Dim ColTasaN As Integer = 3
            Dim ColTasaL As Integer = 4
            Dim Tasa As String


            Try
                oForm.Freeze(True)
                oDTs = oForm.DataSources.DataTables

                oGrid = oForm.Items.Item("dbTasas").Specific
                'MsgBox(oGrid.Rows.SelectedRows.Count)
                oPos = oGrid.GetCellFocus()
                rowNum = oPos.rowIndex
                colNum = oPos.ColumnIndex
                If colNum = ColTasaN Then
                    Tasa = oGrid.DataTable.GetValue(colNum, rowNum)
                    If Decimal.Parse(Tasa) >= 0 And Decimal.Parse(Tasa) <= 100 Then
                        If Decimal.Parse(oGrid.DataTable.GetValue(ColTasaL, rowNum)) <> 0 Then
                            oApp.MessageBox("Tasa Normal no puede ser ingresada si se tiene valor en la Tasa Legal, verifique!")
                            oGrid.DataTable.SetValue(ColTasaN, rowNum, 0.0)
                            Return False
                        End If
                    Else
                        oApp.MessageBox("Tasa Normal ingresada no valida, verifique!" + Tasa)
                        oGrid.DataTable.SetValue(ColTasaN, rowNum, 0.0)
                        Return False
                    End If
                End If

                If colNum = ColTasaL Then
                    Tasa = oGrid.DataTable.GetValue(colNum, rowNum)
                    If Decimal.Parse(Tasa) > 0 And Decimal.Parse(Tasa) <= 100 Then
                        If Decimal.Parse(oGrid.DataTable.GetValue(ColTasaN, rowNum)) <> 0 Then
                            oApp.MessageBox("Tasa Legal no puede ser ingresada si se tiene valor en la Tasa Normal, verifique!")
                            oGrid.DataTable.SetValue(ColTasaL, rowNum, 0.0)
                            Return False
                        End If
                    Else
                        oApp.MessageBox("Tasa Legal ingresada no valida, verifique!")
                        oGrid.DataTable.SetValue(ColTasaL, rowNum, 0.0)
                        Return False
                    End If
                End If
                'oGrid.DataTable.SetValue(2, rowNum, DescGrupArt)

                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function
        Public Shared Function PopulaGridReq(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, tipo As String) As Boolean
            Dim oDT As SAPbouiCOM.DataTable
            Dim oGrid As SAPbouiCOM.Grid
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oCol As SAPbouiCOM.GridColumn

            Try
                oForm.Freeze(True)
                oDTs = oForm.DataSources.DataTables

                Dim DocEntryPlantilla As String = ""
                Dim cmbRequerimientos As SAPbouiCOM.ComboBox

                If tipo = "R" Then
                    oDT = oDTs.Item("dtReq")
                    oGrid = oForm.Items.Item("Item_7").Specific
                    cmbRequerimientos = oForm.Items.Item("cmbOperF").Specific

                    If cmbRequerimientos.Value.ToString() <> "" Then
                        DocEntryPlantilla = cmbRequerimientos.Selected.Value
                    End If

                End If

                If tipo = "G" Then
                    oDT = oDTs.Item("dtGar")
                    oGrid = oForm.Items.Item("Item_19").Specific
                    cmbRequerimientos = oForm.Items.Item("cmbOperG").Specific

                    If cmbRequerimientos.Value.ToString() <> "" Then
                        DocEntryPlantilla = cmbRequerimientos.Selected.Value
                    End If

                End If

                Dim Code As String = ""
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                Code = Trim(oDBCab.GetValue("Code", 0))

                Dim dataTableGarReq As New DataTable

                dataTableGarReq.Clear()

                If DocEntryPlantilla = "" Then
                    DocEntryPlantilla = 0
                    dataTableGarReq = BDLineaCredito.PopulaGridReqGar(DocEntryPlantilla, Code)
                End If

                dataTableGarReq = BDLineaCredito.PopulaGridReqGar(DocEntryPlantilla, Code)
                dataTableGarReq = PivotTable(dataTableGarReq, 0)
                oDT.Clear()
                oGrid.DataTable = oDT

                For Each col As DataColumn In dataTableGarReq.Columns
                    oDT.Columns.Add(col.Caption, SAPbouiCOM.BoFieldsType.ft_Text, 50)
                Next col

                If dataTableGarReq.Rows.Count > 0 Then
                    oDT.Rows.Add(dataTableGarReq.Rows.Count)
                    Dim i As Integer = 0
                    'Se adcionan los valores de la consulta
                    For Each row As DataRow In dataTableGarReq.Rows
                        Dim j As Integer = 0
                        For Each col As DataColumn In dataTableGarReq.Columns


                            Dim valor As Object = row.Item(j)
                            oDT.SetValue(col.Caption, i, valor)

                            If j = 0 Then
                                oGrid.Columns.Item(j).Visible = False
                            End If
                            oGrid.Columns.Item(j).Editable = False

                            If valor = "Y" Or valor = "N" Then
                                oCol = oGrid.Columns.Item(j)
                                oCol.Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
                            End If

                            j += 1
                        Next col
                        i += 1
                    Next row
                End If

                oGrid.AutoResizeColumns()

                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function PivotTable(ByVal dt As DataTable, ByVal ColNum As Integer) As DataTable
            Dim dp As New DataTable("Pivoted")
            dp.Clear()
            dp.Columns.Add("Propiedad")
            For Each row As DataRow In dt.Rows
                dp.Columns.Add(row.Item(ColNum).ToString)     '<----a duplicate column name exception here(A column named 'Eng' already belongs to this DataTable.)
            Next

            Dim IColumns As IEnumerable(Of DataColumn) = From c As DataColumn In dt.Columns
                                                         Where c.Ordinal <> ColNum

            For Each col As DataColumn In IColumns
                Dim tr(dt.Rows.Count) As Object
                tr(0) = col.ColumnName

                Dim i As Integer = 1
                For Each row As DataRow In dt.Rows
                    tr(i) = row.Item(col.Ordinal)
                    i += 1
                Next

                dp.Rows.Add(tr)
            Next

            Return dp
        End Function


        Public Shared Sub deleteImages(ByVal oApp As SAPbouiCOM.Application, ByVal Path As String, ByVal nroDoc As String)
            Dim oImgDB As SAPbobsCOM.UserTable
            Dim res As Integer
            Dim rowIndex As String

            Try
                Dim filtro As String = "Ruta = '" & Path & "' and U_DocCode ='" & nroDoc & "'"
                Dim row As DataRow = oDTanexo.Select(filtro).FirstOrDefault()

                If row.IsNull(1) = False Then
                    rowIndex = row.Item("Code").ToString()
                    oImgDB = DiApp.UserTables.Item("EXX_IATT")
                    oImgDB.GetByKey(rowIndex)
                    res = oImgDB.Remove()
                    If res = 0 Then
                        oApp.MessageBox("Se ha eliminado el documento exitosamente")
                        ManejaMatIAtt(oApp, "LICRE", nroDoc, oForm)
                        Exit Sub
                    Else
                        Dim err As String = DiApp.GetLastErrorDescription
                        oApp.MessageBox(err)
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try

        End Sub
        Public Shared Sub runAttachment(ByVal fileWthPath As String, ByVal oApp As SAPbouiCOM.Application)

            Try
                Process.Start(fileWthPath)
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
            End Try

        End Sub
        Private Shared Sub addDocumentAtt(PathFuente As String, ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)
            Dim oDTanexoCode As System.Data.DataTable
            Dim fileName As String = String.Empty
            Dim PathDestino As String = String.Empty
            Dim oAttDB As SAPbobsCOM.UserTable
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                fileName = PathFuente.Substring(PathFuente.LastIndexOf("\") + 1)

                'Obtener la lista de anexos por nroDoc

                oDTanexoCode = BDLineaCredito.GetMaxCodeLinCred()

                Dim res As Integer

                Dim omaxCode As Integer = 0

                If oDTanexoCode.Rows.Count > 0 Then
                    omaxCode = oDTanexoCode.AsEnumerable().Max(Function(x) x(0))
                    omaxCode = omaxCode + 1
                Else
                    omaxCode = 1
                End If

                oAttDB = DiApp.UserTables.Item("EXX_IATT")
                oAttDB.Code = omaxCode
                oAttDB.Name = omaxCode

                fileName = "EXX_LICRE" & "-" & Trim(oDBCab.GetValue("DocEntry", 0)) & "-" & omaxCode.ToString() & "-" & fileName
                PathDestino = DiApp.AttachMentPath + fileName

                Try
                    IO.File.Copy(PathFuente, PathDestino, True)
                Catch ex As Exception
                    oApp.MessageBox(ex.Message)
                    Exit Sub
                End Try

                oAttDB.UserFields.Fields.Item("U_ObjCode").Value = "LICRE"
                oAttDB.UserFields.Fields.Item("U_DocCode").Value = Trim(oDBCab.GetValue("DocEntry", 0))
                oAttDB.UserFields.Fields.Item("U_Path").Value = PathDestino
                oAttDB.UserFields.Fields.Item("U_AttDate").Value = DiApp.GetCompanyDate
                res = oAttDB.Add()
                If res = 0 Then
                    ManejaMatIAtt(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                    oApp.MessageBox("Se ha añadido el documento correctamente")
                Else
                    oApp.MessageBox(DiApp.GetLastErrorDescription)
                End If
                'System.Runtime.InteropServices.Marshal.ReleaseComObject()
            Catch ex As Exception
                oApp.MessageBox(ex.Message)
            End Try

        End Sub
        Private Shared Sub addCodeLC(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)

            Dim oDBSocio As SAPbouiCOM.DBDataSource
            Dim socioCode As String
            Dim ListaBranchBD As DataTable = BDLineaCredito.ListaBrachBD()
            Dim oBranch As DataTable = BDContratos.BranchList()
            Try

                oDBSocio = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                socioCode = Trim(oDBSocio.GetValue("U_CardCode", 0))

                If ListaBranchBD.Rows(0).Item("MltpBrnchs").ToString = "N" Then
                    If ListaBranchBD.Rows(0).Item("DflBranch").ToString = "" Then
                        socioCode = socioCode + "_" + "0"
                    Else
                        socioCode = socioCode + "_" + ListaBranchBD.Rows(0).Item("DflBranch").ToString
                    End If
                    oDBSocio.SetValue("Code", 0, socioCode)
                Else
                    socioCode = socioCode + "_" + oBranch.Rows(0).Item("BPLId").ToString
                    oDBSocio.SetValue("Code", 0, socioCode)
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)

            End Try


        End Sub
        Public Shared Function verificaEstado(ByRef codeLC As String, CodigoPC As String, NumDoc As String) As Boolean

            Dim odtVerifica As DataTable
            Try
                odtVerifica = NegocioParam.GetFilesGar(codeLC, CodigoPC, NumDoc)

                If odtVerifica.Rows.Count > 0 Then
                    Return True
                End If

            Catch ex As Exception
                Throw ex
            End Try


        End Function

        Private Shared Sub cargarFormGarLC(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByRef CodeLC As String, ByRef CodigoPC As String, ByRef NumDoc As String)

            Dim odtCarga As DataTable = NegocioParam.GetFilesGar(CodeLC, CodigoPC, NumDoc)
            Try
                For i As Integer = 0 To odtCarga.Rows.Count - 1
                    oForm.DataSources.UserDataSources.Item("UD_" & i & "").ValueEx = odtCarga.Rows.Item(i).Item("U_Valor")
                Next

            Catch ex As Exception
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

#End Region

#Region "EVENTOS"

        Public Shared Sub AdminSBOFormUDOLC(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "ckbEstado" And pVal.Before_Action = False Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpACC") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                            Else
                                BubbleEvent = True
                            End If
                        End If

                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "Item_3"
                                    Try
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                                ManejaGridChecklist(oApp, Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                                            End If

                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "Item_13"
                                    Try
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                                ManejaGridTasas(oApp, Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                                            End If

                                        End If



                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "btnModFec"
                                    Dim code As String = Nothing
                                    Try
                                        If Not ValidaBtnAprobar(oApp, oForm) Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                        code = Trim(oDBCab.GetValue("U_Nombre", 0))
                                        SBOFormFechaLst.CreaSBOForm2(oApp, code)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        Else

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "CardCode"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "edtProj"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        Else 'cnp20190711
                            Select Case pVal.ItemUID
                                Case "CardCode"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Else
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Else
                                        oForm.Freeze(False)
                                        BubbleEvent = True
                                        GoTo fail
                                    End If
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        If pVal.ItemUID = "ckbEstado" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpACC") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            End If
                        Else
                            BubbleEvent = True
                        End If
                        'cnp20190711
                        If pVal.ItemUID = "btnModFec" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpMOD") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            Else
                                BubbleEvent = True
                            End If
                        End If
                        'cnp20190711
                        If pVal.ItemUID = "bntVisuali" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpANE") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            Else
                                BubbleEvent = True
                            End If
                        End If
                        'cnp20190711
                        If pVal.ItemUID = "btnBorrar" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpANE") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            Else
                                BubbleEvent = True
                            End If
                        End If
                        'cnp20190711
                        If pVal.ItemUID = "btnInsert" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpANE") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            Else
                                BubbleEvent = True
                            End If
                        End If
                        If pVal.ItemUID = "Item_24" And pVal.Before_Action = True Then
                            If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "GrpANE") = False Then
                                BubbleEvent = False
                                oApp.MessageBox("[LC] No se encontraron grupos de autorización asociados a este usuario.Verifique")
                                GoTo fail
                            Else
                                BubbleEvent = True
                            End If
                        End If

                        If pVal.BeforeAction = False Then
                            Select Case pVal.ItemUID

                                Case "btnReset"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then

                                            ManejaBtnReset(oApp, oForm)

                                        End If
                                    End If

                                Case "btnAproba"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                            Dim btnAproba As SAPbouiCOM.ButtonCombo
                                            Dim cmbEstado As SAPbouiCOM.ComboBox
                                            Dim DocEntry As String = String.Empty
                                            Dim Presupuesto As String = String.Empty
                                            Dim Correlativo As String = String.Empty
                                            Dim DescrAccion As String = String.Empty
                                            Dim CodeAccion As String = String.Empty
                                            Dim DescrEstado As String = String.Empty
                                            Dim CodeEstado As String = String.Empty
                                            Dim nuevoEstado As String = String.Empty
                                            Dim CodUsr As String = String.Empty
                                            Dim NomUsr As String = String.Empty
                                            Dim fecha As String = String.Empty
                                            Dim estadoAp As String = String.Empty
                                            Dim estadoProc As String = String.Empty
                                            Dim etapa As String = String.Empty
                                            Dim linEtapa As String = String.Empty
                                            Dim Proyecto As String = String.Empty
                                            Dim CorreSol As String = String.Empty
                                            Dim CorrelativoSol As Integer
                                            Dim Code As String = String.Empty
                                            Dim nombre As String = String.Empty
                                            Try
                                                nombre = Trim(oDBCab.GetValue("U_Nombre", 0))
                                                btnAproba = oForm.Items.Item("btnAproba").Specific
                                                cmbEstado = oForm.Items.Item("cmbEstado").Specific
                                                DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))
                                                Code = Trim(oDBCab.GetValue("Code", 0))
                                                Presupuesto = ""
                                                Correlativo = ""
                                                CodUsr = Trim(DiApp.UserSignature)
                                                NomUsr = Trim(DiApp.UserName)
                                                fecha = DiApp.GetCompanyDate.ToString("yyyyMMdd")
                                                estadoAp = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                                estadoProc = Trim(oDBCab.GetValue("U_Estado", 0))
                                                etapa = Trim(oDBCab.GetValue("U_Etapa", 0))
                                                linEtapa = Trim(oDBCab.GetValue("U_LinEtapa", 0))
                                                Proyecto = ""
                                                CorreSol = Trim(oDBCab.GetValue("U_CorreSol", 0))


                                                Try
                                                    DescrAccion = btnAproba.Selected.Description
                                                    CodeAccion = btnAproba.Selected.Value
                                                Catch ex As Exception
                                                    DescrAccion = ""
                                                    CodeAccion = ""
                                                End Try

                                                Try
                                                    DescrEstado = cmbEstado.Selected.Description
                                                    CodeEstado = cmbEstado.Selected.Value
                                                Catch ex As Exception
                                                    DescrEstado = ""
                                                    CodeEstado = ""
                                                End Try

                                                If CodeEstado = "" Then
                                                    oForm.Freeze(False)
                                                    ' oApp.MessageBox("Debe existir un estado de documento")
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If

                                                If CodeAccion = "" Then
                                                    oForm.Freeze(False)
                                                    'oApp.MessageBox("Debe elegir una opción de autorización")
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If

                                                If Trim(CodeAccion) = "S" Then
                                                    If Trim(CorreSol) = "" Then
                                                        CorreSol = "1"
                                                    Else
                                                        CorrelativoSol = CInt(CorreSol)
                                                        CorrelativoSol = CorrelativoSol + 1
                                                        CorreSol = CorrelativoSol.ToString
                                                    End If
                                                    DatosAutorizaciones.ModificaCorreSolForm(oApp, oForm, Code, Trim(CorreSol), "LICRE")

                                                Else
                                                    If Trim(CorreSol) = "" Then
                                                        CorreSol = "1"
                                                    End If
                                                End If
                                                Dim contador As Integer = 0
                                                If Trim(CodeAccion) = "R" Then
                                                    contador = contador + 1
                                                End If



                                                Try
                                                    If contador > 0 Then

                                                    Else
                                                        If Not DatosAutorizaciones.InsertaAutorizacionLog("LICRE", Code, Presupuesto, Correlativo, CodeEstado, DescrEstado, CodeAccion, DescrAccion, etapa, CodUsr, NomUsr, fecha, CorreSol, "") Then 'mauricio
                                                            oForm.Freeze(False)
                                                            BubbleEvent = False
                                                            GoTo fail
                                                        End If
                                                    End If


                                                    'DatosAutorizaciones.ModificaCorreSolForm(oApp, oForm, DocEntry, CorreSol, "EXXAVNC")

                                                    If DatosAutorizaciones.ManejaProcesoApr(oApp, oForm, "LICRE", Code, estadoProc, estadoAp, etapa, CodeAccion, linEtapa, Proyecto) Then
                                                        RefrescaDatosMaestro(oApp, oForm)

                                                        etapa = Trim(oDBCab.GetValue("U_Etapa", 0))
                                                        linEtapa = Trim(oDBCab.GetValue("U_LinEtapa", 0))
                                                        estadoAp = Trim(oDBCab.GetValue("U_EstadoAp", 0))

                                                        If Not DatosAutorizaciones.ManejaBtnAcc(oApp, oForm, CodUsr, "LICRE", estadoAp, etapa, linEtapa, Code, Presupuesto, CorreSol) Then
                                                            oForm.Freeze(False)
                                                            BubbleEvent = False
                                                            GoTo fail
                                                        End If

                                                    End If

                                                    If contador >= 1 Then

                                                        SBOFormMotRec.CreaSBOForm(oApp, Code, nombre, DocEntry, Presupuesto, Correlativo, CodeEstado, DescrEstado, CodeAccion, DescrAccion, etapa, CodUsr, NomUsr, fecha, CorreSol)
                                                        contador = 0
                                                    End If

                                                Catch ex As Exception
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End Try
                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            End Try
                                        End If
                                    End If
                                Case "btnHisAp"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        Try
                                            Dim docentry As String

                                            docentry = Trim(oDBCab.GetValue("Code", 0))

                                            If docentry <> "" Then
                                                Try
                                                    SBOFormHisA.CreaSBOForm(oApp, "LICRE", docentry)
                                                Catch ex As Exception
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End Try
                                            End If
                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try
                                    End If
                                Case "btnInsert"
                                    Try
                                        Dim selFile As String

                                        If Trim(oDBCab.GetValue("DocEntry", 0)) = "" Then
                                            oApp.StatusBar.SetText("[FN] Se debe guardar el documento antes de agregar anexos.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                        selFile = showOpenFileImg()
                                        If selFile = "" Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                        Dim fileExt As String = IO.Path.GetExtension(selFile)
                                        Dim fileName As String = IO.Path.GetFileNameWithoutExtension(selFile)
                                        Dim fileFullPath As String = IO.Path.GetFullPath(selFile)
                                        Dim filePath As String = Replace(fileFullPath, IO.Path.GetFileName(selFile), "")
                                        filePath = Left(filePath, filePath.Length - 1)
                                        fileExt = Right(fileExt, fileExt.Length - 1)

                                        addDocumentAtt(fileFullPath, oApp, oForm)

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "bntVisuali" ' ""
                                    Dim oGrid As SAPbouiCOM.Grid
                                    Dim sFile As String = String.Empty
                                    Try
                                        oGrid = oForm.Items.Item("dbGridAnex").Specific

                                        If oGrid.Rows.Count > 0 Then
                                            For i As Integer = 0 To oGrid.Rows.Count - 1
                                                If oGrid.Rows.IsSelected(i) Then
                                                    sFile = oGrid.DataTable.GetValue("Ruta", i)
                                                    runAttachment(sFile, oApp)
                                                    GoTo fail
                                                End If
                                            Next
                                        End If

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnBorrar"
                                    Dim oGrid As SAPbouiCOM.Grid
                                    Dim ruta As String = String.Empty
                                    Try
                                        oGrid = oForm.Items.Item("dbGridAnex").Specific
                                        If oGrid.Rows.Count > 0 Then
                                            For i As Integer = 0 To oGrid.Rows.Count - 1
                                                If oGrid.Rows.IsSelected(i) Then
                                                    ruta = oGrid.DataTable.GetValue("Ruta", i)
                                                    deleteImages(oApp, ruta, Trim(oDBCab.GetValue("DocEntry", 0)))
                                                    GoTo fail
                                                End If
                                            Next
                                        End If

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "Item_24"
                                    Dim estado As String = String.Empty
                                    Dim autorizacion As String = String.Empty
                                    estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                    autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                    If estado <> "0" Or estado = "" Or autorizacion = "P" Then

                                    Else
                                        Dim oGrid As SAPbouiCOM.Grid
                                        Try
                                            oGrid = oForm.Items.Item("dbTasas").Specific
                                            If oGrid.Rows.Count > 0 Then
                                                For i As Integer = 0 To oGrid.Rows.Count - 1
                                                    If oGrid.Rows.IsSelected(i) Then
                                                        'MsgBox(oGrid.DataTable.GetValue(2, i))
                                                        If oGrid.DataTable.GetValue(2, i) = Nothing Then
                                                        Else
                                                            oGrid.DataTable.Rows.Remove(i)
                                                            If Not GuardaGrdTasasGrupArt(oApp, oForm) Then 'CNP20190329
                                                                oForm.Freeze(False)
                                                                oApp.MessageBox("Error al guardar registros de pertenencias 2")
                                                                GoTo fail
                                                            End If
                                                        End If
                                                        GoTo fail
                                                    End If
                                                Next
                                            End If

                                        Catch ex As Exception
                                            oForm.Freeze(False)
                                            oApp.MessageBox(ex.Message)
                                            BubbleEvent = False
                                            GoTo fail
                                        End Try
                                    End If
                                Case "btnReq"

                                    Dim CodeLC As String = Trim(oDBCab.GetValue("Code", 0))
                                    Dim NumDoc As String = Trim(oDBCab.GetValue("DocEntry", 0))

                                    Dim CodigoPC As String = ""

                                    If panelGrid = "1" Then
                                        CodigoPC = oForm.DataSources.UserDataSources.Item("udCmbR").ValueEx
                                    End If

                                    If panelGrid = "2" Then
                                        CodigoPC = oForm.DataSources.UserDataSources.Item("udCmbG").ValueEx
                                    End If


                                    If verificaEstado(CodeLC, CodigoPC, NumDoc) = True Then
                                        FormGarLinea.CreaSBOForm(oApp, CodeLC, CodigoPC, NumDoc)
                                        cargarFormGarLC(oApp, oForm, CodeLC, CodigoPC, NumDoc)
                                    End If
                                    FormGarLinea.CreaSBOForm(oApp, CodeLC, CodigoPC, NumDoc)

                            End Select
                        Else
                            Select Case pVal.ItemUID
                                Case "btnReq"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                            Dim CodeLC As String = Trim(oDBCab.GetValue("Code", 0))
                                            Dim CodigoPC As String = ""

                                            If panelGrid = "1" Then
                                                CodigoPC = oForm.DataSources.UserDataSources.Item("udCmbR").ValueEx
                                            End If

                                            If panelGrid = "2" Then
                                                CodigoPC = oForm.DataSources.UserDataSources.Item("udCmbG").ValueEx
                                            End If

                                            If Trim(CodeLC) = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            End If
                                            If Trim(CodigoPC) = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            End If
                                        Else
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Else
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                Case "Item_22"
                                    Try
                                        Dim gridImg As SAPbouiCOM.Grid
                                        Dim imagen As SAPbouiCOM.PictureBox
                                        Dim oDT As SAPbouiCOM.DataTable
                                        gridImg = oForm.Items.Item("dbGridAnex").Specific
                                        'imagen = oForm.Items.Item("edtImg").Specific
                                        ManejaMatIAtt(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                                        If gridImg.Rows.SelectedRows.Count > 0 Then
                                            oDT = gridImg.DataTable
                                            'imagen.Picture = oDT.GetValue("Ruta", Convert.ToInt32(gridImg.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)))
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                'Case "Item_13" BORRAR
                                '    Try
                                '        Dim gridGrupArt As SAPbouiCOM.Grid
                                '        'Dim imagen As SAPbouiCOM.PictureBox
                                '        Dim oDT As SAPbouiCOM.DataTable
                                '        gridGrupArt = oForm.Items.Item("dbTasas").Specific
                                '        'imagen = oForm.Items.Item("edtImg").Specific
                                '        ManejaGrupoArticulosT(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                                '        If gridGrupArt.Rows.SelectedRows.Count > 0 Then
                                '            oDT = gridGrupArt.DataTable
                                '            'imagen.Picture = oDT.GetValue("Ruta", Convert.ToInt32(gridImg.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)))
                                '        End If
                                '    Catch ex As Exception
                                '        oForm.Freeze(False)
                                '        oApp.MessageBox(ex.Message)
                                '        BubbleEvent = False
                                '        GoTo fail
                                '    End Try

                                'Case "Item_13" BORRAR
                                '    Try
                                '        Dim btnRegistra As SAPbouiCOM.Button
                                '        btnRegistra = oForm.Items.Item("btnReq").Specific
                                '        btnRegistra.Item.ToPane = 2
                                '        panelGrid = "2"

                                '    Catch ex As Exception
                                '        oForm.Freeze(False)
                                '        oApp.MessageBox(ex.Message)
                                '        BubbleEvent = False
                                '        GoTo fail
                                '    End Try

                                Case "Item_20"
                                    Try
                                        Dim btnRegistra As SAPbouiCOM.Button
                                        btnRegistra = oForm.Items.Item("btnReq").Specific
                                        btnRegistra.Item.ToPane = 2
                                        panelGrid = "2"

                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "Item_0"
                                    Try
                                        panelGrid = "1"
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnAproba"
                                    Try
                                        Dim usrcode As String = Trim(DiApp.UserSignature)

                                        If Not ValidaBtnAprobar(oApp, oForm) Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnHisAp"
                                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                            Try

                                                Dim usrcode As String = Trim(DiApp.UserSignature)


                                                If Not ValidaInfoHisA(oApp, oForm) Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            End Try
                                        End If

                                    Else
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        oApp.MessageBox("No se puede visualizar el historial de autorización en modo crear. Guarde el formulario e intente nuevamente")
                                        GoTo fail
                                    End If
                                Case "btnReset"
                                    If DatosDB.PerteneceGrupoAutorizacion(oApp, DiApp.UserSignature, "udGrpRL") = True Then
                                        Dim remesg As Integer = oApp.MessageBox("Se realizará un reseteo de autorizaciones irreversible, desea continuar?", 2, "Si", "No")
                                        If remesg = 1 Then
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Else
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Else
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "cmbOperF"
                                    PopulaGridReq(oApp, oForm, "R")
                                Case "cmbOperG"
                                    PopulaGridReq(oApp, oForm, "G")
                                Case "dbTasas" ' ssh
                                    ActualizarDescGrupArt(oApp, oForm)

                            End Select
                        End If



                    Case SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE
                        Dim bandera As String = "1"
                        If pVal.BeforeAction = False Then
                            If pVal.ActionSuccess = True Then
                                Try
                                    bandera = oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx
                                Catch ex As Exception
                                End Try
                                If bandera <> "1" Then
                                    Select Case pVal.FormMode
                                        Case "3"
                                            Try
                                                oForm.Freeze(True)

                                                If Not FormatItems(oApp, oForm) Then
                                                    oForm.Freeze(False)
                                                    oApp.MessageBox("Problemas al formatear items")
                                                End If


                                                If Not PopulaFest(oApp, oForm) Then 'CNP AUTORIZACION
                                                    'oForm.Close()
                                                    'Exit Sub
                                                End If

                                                oDBCab.SetValue("U_EstadoAp", 0, "-")
                                                oDBCab.SetValue("U_Estado", 0, "0")

                                                oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx = "1"

                                                oForm.Freeze(False)
                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                oApp.MessageBox(ex.Message)
                                                GoTo fail
                                            End Try

                                        Case "2" 'cnp20190710
                                            Try
                                                oForm.Freeze(True)
                                                bloquearCampos(oApp, oForm)
                                                oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx = "1"
                                                oForm.Freeze(False)
                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                oApp.MessageBox(ex.Message)
                                                GoTo fail
                                            End Try

                                        Case "0" 'cnp20190710
                                            Try
                                                oForm.Freeze(True)
                                                AbrirCampos(oApp, oForm)
                                                oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx = "1"
                                                oForm.Freeze(False)
                                            Catch ex As Exception
                                                oForm.Freeze(False)
                                                oApp.MessageBox(ex.Message)
                                                GoTo fail
                                            End Try
                                    End Select
                                End If
                            End If


                        End If

                    Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN 'cnp20190711
                        If pVal.Before_Action = True Then
                            Select Case pVal.ItemUID
                                Case "CardCode"
                                    If pVal.InnerEvent = False And pVal.Modifiers = SAPbouiCOM.BoModifiersEnum.mt_CTRL Then
                                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                                oForm.Freeze(False)
                                                BubbleEvent = True
                                                GoTo fail
                                            Else
                                                Select Case pVal.CharPressed
                                                    Case "9"
                                                        oForm.Freeze(False)
                                                        BubbleEvent = True
                                                        GoTo fail
                                                    Case Else
                                                        oForm.Freeze(False)
                                                        BubbleEvent = True
                                                        GoTo fail

                                                End Select
                                            End If
                                        End If
                                    Else
                                        Select Case pVal.CharPressed
                                            Case "9"
                                                oForm.Freeze(False)
                                                BubbleEvent = True
                                                GoTo fail
                                            Case Else
                                                If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                                        oForm.Freeze(False)
                                                        BubbleEvent = True
                                                        GoTo fail
                                                    Else
                                                        oForm.Freeze(False)
                                                        BubbleEvent = False
                                                        GoTo fail
                                                    End If
                                                Else
                                                    oForm.Freeze(False)
                                                    BubbleEvent = True
                                                    GoTo fail
                                                End If
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                        End Select
                                    End If

                                Case "0_U_E"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                    End Select
                                Case "FechaCon"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            Dim estado As String = String.Empty
                                            Dim autorizacion As String = String.Empty

                                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                            If estado <> "0" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            ElseIf estado = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            Else
                                                If autorizacion = "P" Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            End If
                                    End Select
                                Case "Interes"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            Dim estado As String = String.Empty
                                            Dim autorizacion As String = String.Empty

                                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                            If estado <> "0" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            ElseIf estado = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            Else
                                                If autorizacion = "P" Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            End If
                                    End Select
                                Case "Interes"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            Dim estado As String = String.Empty
                                            Dim autorizacion As String = String.Empty

                                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                            If estado <> "0" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            ElseIf estado = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            Else
                                                If autorizacion = "P" Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            End If
                                    End Select
                                Case "Item_16"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            Dim estado As String = String.Empty
                                            Dim autorizacion As String = String.Empty

                                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                            If estado <> "0" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            ElseIf estado = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            Else
                                                If autorizacion = "P" Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            End If

                                    End Select
                                Case "FechaDe"
                                    Select Case pVal.CharPressed
                                        Case "9"
                                            oForm.Freeze(False)
                                            BubbleEvent = True
                                            GoTo fail
                                        Case Else
                                            Dim estado As String = String.Empty
                                            Dim autorizacion As String = String.Empty

                                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                            If estado <> "0" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            ElseIf estado = "" Then
                                                oForm.Freeze(False)
                                                BubbleEvent = False
                                                GoTo fail
                                            Else
                                                If autorizacion = "P" Then
                                                    oForm.Freeze(False)
                                                    BubbleEvent = False
                                                    GoTo fail
                                                End If
                                            End If

                                    End Select
                                Case "dbTasas" ' ssh
                                    'MsgBox("Modificando Tasas")
                                    'ValidarTasas(oApp, oForm)
                                    'Select Case pVal.CharPressed
                                    '    Case "9"
                                    '        oForm.Freeze(False)
                                    '        BubbleEvent = True
                                    '        GoTo fail
                                    '    Case Else
                                    '        Dim estado As String = String.Empty
                                    '        Dim autorizacion As String = String.Empty

                                    '        estado = Trim(oDBCab.GetValue("U_Estado", 0))
                                    '        autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                    '        If estado <> "0" Then
                                    '            oForm.Freeze(False)
                                    '            BubbleEvent = False
                                    '            GoTo fail
                                    '        ElseIf estado = "" Then
                                    '            oForm.Freeze(False)
                                    '            BubbleEvent = False
                                    '            GoTo fail
                                    '        Else
                                    '            If autorizacion = "P" Then
                                    '                oForm.Freeze(False)
                                    '                BubbleEvent = False
                                    '                GoTo fail
                                    '            End If
                                    '        End If

                                    'End Select
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK 'cnp20190711
                        If pVal.Before_Action = True Then
                            Select Case pVal.ItemUID
                                Case "0_U_E"
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_PICKER_CLICKED 'cnp20190711
                        If pVal.Before_Action = True Then
                            Dim estado As String = String.Empty
                            Dim autorizacion As String = String.Empty

                            estado = Trim(oDBCab.GetValue("U_Estado", 0))
                            autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))

                            Select Case pVal.ItemUID
                                Case "FechaCon"
                                    If estado <> "0" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    ElseIf estado = "" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        If autorizacion = "P" Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    End If
                                Case "Interes"
                                    If estado <> "0" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    ElseIf estado = "" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        If autorizacion = "P" Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    End If
                                Case "Item_16"
                                    If estado <> "0" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    ElseIf estado = "" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        If autorizacion = "P" Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    End If
                                Case "FechaDe"
                                    If estado <> "0" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    ElseIf estado = "" Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        If autorizacion = "P" Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    End If
                            End Select
                        End If

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub
        Public Shared Sub RightClickEvent(ByRef oApp As SAPbouiCOM.Application, ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) 'cnp20190711

            Try
                Dim oForm As SAPbouiCOM.Form


                oForm = oApp.Forms.Item(eventInfo.FormUID)

                If eventInfo.BeforeAction = True Then
                    Dim oDBCab As SAPbouiCOM.DBDataSource
                    Try
                        oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                    Catch ex As Exception
                        oDBCab = Nothing
                    End Try

                    Dim estado As String = String.Empty
                    Dim autorizacion As String = String.Empty

                    estado = Trim(oDBCab.GetValue("U_Estado", 0))
                    autorizacion = Trim(oDBCab.GetValue("U_EstadoAp", 0))

                    Select Case eventInfo.ItemUID
                        Case "CardCode"
                            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                    oForm.Freeze(False)
                                    BubbleEvent = True
                                    GoTo fail
                                Else
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Else
                                oForm.Freeze(False)
                                BubbleEvent = True
                                GoTo fail
                            End If
                        Case "0_U_E"
                            oForm.Freeze(False)
                            BubbleEvent = False
                            GoTo fail

                        Case "FechaCon"
                            If estado <> "0" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            ElseIf estado = "" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            Else
                                If autorizacion = "P" Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            End If
                        Case "Interes"
                            If estado <> "0" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            ElseIf estado = "" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            Else
                                If autorizacion = "P" Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            End If
                        Case "Item_16"
                            If estado <> "0" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            ElseIf estado = "" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            Else
                                If autorizacion = "P" Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            End If
                        Case "FechaDe"
                            If estado <> "0" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            ElseIf estado = "" Then
                                oForm.Freeze(False)
                                BubbleEvent = False
                                GoTo fail
                            Else
                                If autorizacion = "P" Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            End If

                    End Select
                End If


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("RightClick Error:" & ex.ToString)
            End Try

        End Sub
        Public Shared Sub AdminSBOFormUDOLCNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource


            Try
                oForm.Freeze(True)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                'fld = oForm.Items.Item("Item_22").Specific
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If Not FormatItems(oApp, oForm) Then
                        oForm.Freeze(False)
                        oApp.MessageBox("Problemas al formatear items")
                    End If

                    If Not PopulaFest(oApp, oForm) Then 'CNP AUTORIZACION
                        oForm.Freeze(False)
                        'oForm.Close()
                        Exit Sub
                    End If

                    oDBCab.SetValue("U_EstadoAp", 0, "-")
                    oDBCab.SetValue("U_Estado", 0, "0")

                    Try
                        bloquearCampos(oApp, oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try
                    Try
                        ManejaMatIAtt(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                    Try
                        LoadCombos(oApp, oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try


                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Try
                        AbrirCampos(oApp, oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    Try
                        bloquearCampos(oApp, oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                    Try
                        If panelGrid = "1" Then
                            PopulaGridReq(oApp, oForm, "R")
                        End If
                        If panelGrid = "2" Then
                            PopulaGridReq(oApp, oForm, "G")
                        End If
                        ManejaMatIAtt(oApp, "LICRE", Trim(oDBCab.GetValue("DocEntry", 0)), oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                    Try
                        LoadCombos(oApp, oForm)
                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                    Try
                        ManejaGridTasas(oApp, Trim(oDBCab.GetValue("DocEntry", 0)), oForm)

                    Catch ex As Exception
                        oForm.Freeze(False)
                        oApp.MessageBox(ex.Message)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                End If
fail:
                oForm.Freeze(False)

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Public Shared Sub AdminSBOFormUDOEvent(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD

                        If pVal.BeforeAction = True Then
                            Try

                                Dim usrcode As String = Trim(DiApp.UserSignature)
                                Dim proyecto As String = String.Empty
                                Dim presupuesto As String = String.Empty

                                'Validaciones
                                If Not ValidaFechaDe(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If

                                If Not ValidaSN(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If

                                addCodeLC(oApp, oForm, BubbleEvent)

                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                        Else
                            If pVal.ActionSuccess = True Then
                                If Not GuardaGrdChecklist(oApp, oForm) Then 'CNP20190329
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Error al guardar registros de pertenencias")
                                    GoTo fail
                                End If
                                'GuardaGrdTasasGrupArt
                                If Not GuardaGrdTasasGrupArt(oApp, oForm) Then 'CNP20190329
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Error al guardar registros de pertenencias 2")
                                    GoTo fail
                                End If

                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                        If pVal.BeforeAction = False Then
                            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                    Dim usuario As String = String.Empty
                                    Dim formulario As String = String.Empty
                                    Dim estado As String = String.Empty
                                    Dim etapa As String = String.Empty
                                    Dim linetapa As String = String.Empty
                                    Dim DocEntry As String = String.Empty
                                    Dim PresEntry As String = String.Empty
                                    Dim CorreSol As String = String.Empty
                                    Dim tipo As String = String.Empty

                                    If Not FormatItems(oApp, oForm) Then
                                        oForm.Freeze(False)
                                        oApp.MessageBox("Problemas al formatear items")
                                    End If

                                    If Not PopulaFest(oApp, oForm) Then 'CNP AUTORIZACION
                                        oForm.Freeze(False)
                                        ' oForm.Close()
                                        Exit Sub
                                    End If
                                    usuario = Trim(DiApp.UserSignature)
                                    formulario = "LICRE"
                                    estado = Trim(oDBCab.GetValue("U_EstadoAp", 0))
                                    etapa = Trim(oDBCab.GetValue("U_Etapa", 0))
                                    linetapa = Trim(oDBCab.GetValue("U_LinEtapa", 0))
                                    DocEntry = Trim(oDBCab.GetValue("Code", 0))
                                    PresEntry = ""
                                    CorreSol = Trim(oDBCab.GetValue("U_CorreSol", 0))


                                    If Not DatosAutorizaciones.ManejaBtnAcc(oApp, oForm, Trim(usuario), Trim(formulario), Trim(estado), Trim(etapa), Trim(linetapa), Trim(DocEntry), Trim(PresEntry), Trim(CorreSol)) Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                    Try
                                        LoadCombos(oApp, oForm)
                                        bloquearCampos(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Else
                                    Try

                                        AbrirCampos(oApp, oForm)


                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                End If
                            Else
                                Try
                                    oForm.Freeze(True)

                                    If Not FormatItems(oApp, oForm) Then
                                        oForm.Freeze(False)
                                        oApp.MessageBox("Problemas al formatear items")
                                    End If

                                    If Not PopulaFest(oApp, oForm) Then 'CNP AUTORIZACION
                                        oForm.Freeze(False)
                                        'oForm.Close()
                                        Exit Sub
                                    End If

                                    oDBCab.SetValue("U_EstadoAp", 0, "-")
                                    oDBCab.SetValue("U_Estado", 0, "0")

                                    Try
                                        bloquearCampos(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                    oForm.Freeze(False)
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try

                            End If

                        Else

                        End If



                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                        If pVal.BeforeAction = True Then
                            Try

                                Dim usrcode As String = Trim(DiApp.UserSignature)
                                Dim proyecto As String = String.Empty


                                If Not validaFechaRango(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                End If


                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                        Else
                            If pVal.ActionSuccess = True Then
                                If Not GuardaGrdChecklist(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Error al guardar registros de pertenencias")
                                    GoTo fail
                                End If
                                If Not GuardaGrdTasasGrupArt(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    oApp.MessageBox("Error al guardar registros de pertenencias 2")
                                    GoTo fail
                                End If
                            End If
                        End If



                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

#End Region

#Region "COMBOS"
        Private Shared Function LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim formulario As String = ""

            Try
                oForm.Freeze(True)


                'FORMULARIOS
                'oCombo = oForm.Items.Item("cmbEstado").Specific
                ''oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                ''oCombo.Item.AffectsFormMode = False

                'If Not PopulaEstadosCmb(oApp, oForm, oCombo) Then
                '    oForm.Freeze(False)
                '    oApp.MessageBox("Error al popular el combo de estados de formulario")
                '    oForm.Freeze(True)
                'End If

                'REQUISITOS
                oCombo = oForm.Items.Item("cmbOperF").Specific
                'oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                'oCombo.Item.AffectsFormMode = False

                If Not PopulaRequisitosCmb(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de requermientos.")
                    oForm.Freeze(True)
                End If

                'GARANTIAS
                oCombo = oForm.Items.Item("cmbOperG").Specific
                'oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                'oCombo.Item.AffectsFormMode = False

                If Not PopulaGarantiasCmb(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de garantias.")
                    oForm.Freeze(True)
                End If

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaInfoHisA(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim DocEntry As String = String.Empty

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_LICRE")
                DocEntry = Trim(oDBCab.GetValue("DocEntry", 0))

                If DocEntry = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe tener un codigo de documento para ver el historial de aprobaciones")
                    Return False
                Else
                    oForm.Freeze(False)
                    Return True
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Private Shared Sub bloquearCampos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Try

                oForm.Items.Item("0_U_E").Enabled = False  'mauricio modificado
                oForm.Items.Item("Item_11").Enabled = False

            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Visible = True
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Private Shared Sub AbrirCampos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form)

            Try

                oForm.Items.Item("0_U_E").Enabled = True
                oForm.Items.Item("Item_11").Enabled = True

            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Visible = True
                oApp.MessageBox(ex.Message)
            End Try

        End Sub

        Public Shared Sub ManejaMatIAtt(ByVal oApp As SAPbouiCOM.Application, ByVal Objeto As String, ByVal nroDoc As String, ByVal oForm As SAPbouiCOM.Form)

            Dim oGrid As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim btnBorrar As SAPbouiCOM.Button
            Dim numLin As Integer = 0

            Try
                oForm.Freeze(True)
                btnBorrar = oForm.Items.Item("btnBorrar").Specific

                'Obtener la lista de anexos por nroDoc

                oDTanexo = BDLineaCredito.GetAnexosLineaDeCredito(Objeto, nroDoc)

                'Se setean los objetos 
                oGrid = oForm.Items.Item("dbGridAnex").Specific
                oDTs = oForm.DataSources.DataTables

                oDT = oDTs.Item("dbGridAnex")
                oDT.Clear()
                oDT.Columns.Add("FilNum", SAPbouiCOM.BoFieldsType.ft_Text, 50)
                oDT.Columns.Add("Ruta", SAPbouiCOM.BoFieldsType.ft_Text, 300)
                oDT.Columns.Add("Fecha", SAPbouiCOM.BoFieldsType.ft_Text, 50)

                If oDTanexo.Rows.Count > 0 Then
                    oDT.Rows.Add(oDTanexo.Rows.Count)
                    Dim i As Integer = 0
                    'Se adcionan los valores de la consulta
                    For Each row As DataRow In oDTanexo.Rows
                        Dim filNum As Object = row.Item("FilNum")
                        Dim ruta As Object = row.Item("Ruta")
                        Dim fecha As Object = row.Item("Fecha")

                        oDT.SetValue("FilNum", i, filNum)
                        oDT.SetValue("Ruta", i, ruta)
                        oDT.SetValue("Fecha", i, fecha)
                        i += 1
                    Next row
                End If


                oGrid.DataTable = oDT
                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = oDT.GetValue("FilNum", i)
                        If Not numLin = 0 Then
                            oGrid.RowHeaders.SetText(i, numLin)
                        Else
                            oGrid.RowHeaders.SetText(i, "")
                        End If
                    Next
                    oGrid.Columns.Item(0).Visible = False
                    oGrid.Columns.Item(1).Editable = False
                    oGrid.Columns.Item(2).Editable = False
                    oGrid.AutoResizeColumns()
                    oGrid.Rows.SelectedRows.Add(0)
                Else
                    oGrid.Columns.Item(0).Visible = False
                    oGrid.Columns.Item(1).Editable = False
                    oGrid.Columns.Item(2).Editable = False
                    oGrid.AutoResizeColumns()
                End If
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try


        End Sub

        'BORRAR
        'Public Shared Sub ManejaGrupoArticulosT(ByVal oApp As SAPbouiCOM.Application, ByVal Objeto As String, ByVal nroDoc As String, ByVal oForm As SAPbouiCOM.Form)

        '    Dim oGrid As SAPbouiCOM.Grid
        '    Dim oDT As SAPbouiCOM.DataTable
        '    Dim oDTs As SAPbouiCOM.DataTables
        '    'Dim Campo As SAPbouiCOM.LinkedButton
        '    ' Dim btnBorrar As SAPbouiCOM.Button
        '    Dim numLin As Integer = 0

        '    Try
        '        oForm.Freeze(True)
        '        'btnBorrar = oForm.Items.Item("btnBorrar").Specific

        '        'Obtener la lista de GrupoArticulos con Tasas registradas

        '        'oDTanexo = BDLineaCredito.GetGrupoArticulos(Objeto, nroDoc)

        '        'Se setean los objetos 
        '        oGrid = oForm.Items.Item("dbTasas").Specific
        '        oDTs = oForm.DataSources.DataTables '.Item("dbTasas")


        '        oDT = oDTs.Item("dbTasas")
        '        oDT.Clear()
        '        'oDT.Columns.Add("FilNum", SAPbouiCOM.BoFieldsType.ft_Text, 50)
        '        oDT.Columns.Add("Grupo Articulo", SAPbouiCOM.BoFieldsType.ft_Text, 200)
        '        oDT.Columns.Add("Tasa Normal", SAPbouiCOM.BoFieldsType.ft_Text, 20)
        '        oDT.Columns.Add("Tasa Legal", SAPbouiCOM.BoFieldsType.ft_Text, 20)

        '        oGrid.DataTable = oDT
        '        oGrid.RowHeaders.TitleObject.Caption = "#"
        '        oGrid.Columns.Item(0).Editable = True
        '        oGrid.Columns.Item(1).Editable = True
        '        oGrid.Columns.Item(2).Editable = True
        '        oGrid.AutoResizeColumns()

        '        'If oDTanexo.Rows.Count > 0 Then
        '        '    oDT.Rows.Add(oDTanexo.Rows.Count)
        '        '    Dim i As Integer = 0
        '        '    'Se adcionan los valores de la consulta
        '        '    For Each row As DataRow In oDTanexo.Rows
        '        '        Dim filNum As Object = row.Item("FilNum")
        '        '        Dim ruta As Object = row.Item("Ruta")
        '        '        Dim fecha As Object = row.Item("Fecha")

        '        '        oDT.SetValue("FilNum", i, filNum)
        '        '        oDT.SetValue("Ruta", i, ruta)
        '        '        oDT.SetValue("Fecha", i, fecha)
        '        '        i += 1
        '        '    Next row
        '        'End If


        '        'oGrid.DataTable = oDT
        '        'If oGrid.Rows.Count > 0 Then
        '        '    For i As Integer = 0 To oGrid.Rows.Count - 1
        '        '        numLin = oDT.GetValue("FilNum", i)
        '        '        If Not numLin = 0 Then
        '        '            oGrid.RowHeaders.SetText(i, numLin)
        '        '        Else
        '        '            oGrid.RowHeaders.SetText(i, "")
        '        '        End If
        '        '    Next
        '        '    oGrid.Columns.Item(0).Visible = False
        '        '    oGrid.Columns.Item(1).Editable = False
        '        '    oGrid.Columns.Item(2).Editable = False
        '        '    oGrid.AutoResizeColumns()
        '        '    oGrid.Rows.SelectedRows.Add(0)
        '        'Else
        '        '    oGrid.Columns.Item(0).Visible = False
        '        '    oGrid.Columns.Item(1).Editable = False
        '        '    oGrid.Columns.Item(2).Editable = False
        '        '    oGrid.AutoResizeColumns()
        '        'End If
        '        oForm.Freeze(False)
        '    Catch ex As Exception
        '        oForm.Freeze(False)
        '        oApp.MessageBox(ex.Message)
        '        Exit Sub
        '    End Try


        'End Sub

        Public Shared Function showOpenFileImg() As String
            Dim thread As System.Threading.Thread

            Try
                thread = New System.Threading.Thread(AddressOf ShowFolderBrowserOpenIMG)
                If thread.ThreadState = System.Threading.ThreadState.Unstarted Then
                    thread.SetApartmentState(System.Threading.ApartmentState.STA)
                    thread.Start()
                    Threading.Thread.Sleep(500)
                ElseIf thread.ThreadState = System.Threading.ThreadState.Stopped Then
                    thread.Start()
                    Threading.Thread.Sleep(500)
                    thread.Join()
                Else
                    thread.Abort()
                End If
                While thread.ThreadState = System.Threading.ThreadState.Running
                    Threading.Thread.Sleep(500)
                    Windows.Forms.Application.DoEvents()
                End While
                If FileName <> "" Then
                    Return FileName
                Else
                    FileName = ""
                    Return FileName
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub ShowFolderBrowserOpenIMG()
            Dim MyProcs() As System.Diagnostics.Process
            Dim oApp As SAPbouiCOM.Application
            FileName = ""
            Dim OpenFile As New OpenFileDialog

            Try
                Try
                    oApp = Sap.GetApplication
                Catch ex As Exception
                    oApp.MessageBox("Descripcion del Error:" & ex.Message)
                End Try
                OpenFile.Multiselect = False
                OpenFile.Filter = "All files (*.*)|*.*"
                Dim filterindex As Integer = 0
                Try
                    filterindex = 0
                Catch ex As Exception
                End Try

                OpenFile.FilterIndex = filterindex

                OpenFile.RestoreDirectory = True
                oApp.Desktop.Title = "SBO under" + oApp.Company.UserName
                MyProcs = System.Diagnostics.Process.GetProcessesByName("SAP Business One")

                'If MyProcs.Length = 1 Then
                For i As Integer = 0 To MyProcs.Length - 1
                    If MyProcs(i).MainWindowTitle = oApp.Desktop.Title Then
                        Dim MyWindow As New WindowWrapper(MyProcs(i).MainWindowHandle)
                        Dim ret As DialogResult = OpenFile.ShowDialog(MyWindow)
                        If ret = DialogResult.OK Then
                            FileName = OpenFile.FileName
                            OpenFile.Dispose()
                        Else
                            System.Windows.Forms.Application.ExitThread()
                        End If
                    End If

                Next
            Catch ex As Exception
                'SBO_Application.StatusBar.SetText(ex.Message)
                MessageBox.Show(ex.ToString())
                FileName = ""
            Finally
                OpenFile.Dispose()
            End Try

        End Sub

        Public Shared Function HasDuplicates(oDTChk As SAPbouiCOM.DataTable) As Boolean

            For i As Integer = 0 To oDTChk.Rows.Count - 2 ' don't include the last value cos there's nothing after
                For j As Integer = i + 1 To oDTChk.Rows.Count - 1 ' don't include current entry cos it obviously matches itself!
                    If Trim(oDTChk.GetValue("CODGRUPART", i)) = Trim(oDTChk.GetValue("CODGRUPART", j)) Then
                        Return True
                    End If
                Next
            Next

            Return False
        End Function

        Public Shared Function IsValided(oDTChk As SAPbouiCOM.DataTable) As Boolean
            Dim codGrupArt As String = String.Empty
            Dim TasaN As String = String.Empty
            Dim TasaL As String = String.Empty

            For i As Integer = 0 To oDTChk.Rows.Count - 1 ' don't include the last value cos there's nothing after
                codGrupArt = Trim(oDTChk.GetValue("CODGRUPART", i))

                If codGrupArt IsNot Nothing Or Not codGrupArt = 0 Then

                    TasaN = Trim(oDTChk.GetValue("TASANOR", i))
                    TasaL = Trim(oDTChk.GetValue("TASALEG", i))

                    If TasaN = 0 And TasaL = 0 Then
                        Return False
                    Else
                        If TasaN > 100 Or TasaL > 100 Or TasaN < 0 Or TasaL < 0 Then
                            Return False
                        End If
                    End If

                End If

            Next

            Return True
        End Function

#End Region

    End Class
End Namespace
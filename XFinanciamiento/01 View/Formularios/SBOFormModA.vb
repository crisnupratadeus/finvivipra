﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View
    Public Class SBOFormModA

#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application)
            Try
                Dim MenuID As String = DatosParam.GetUDTMenu(oApp, "AUTMOD", "47616")
                If MenuID <> "" Then
                    oApp.ActivateMenuItem(MenuID)

                Else
                    oApp.MessageBox("No se encontró el documento, verificar la creación de tablas")
                End If

                oForm = oApp.Forms.ActiveForm

                LoadCombos(oApp, oForm)

            Catch ex As Exception
                oForm.Freeze(False)
                oForm.Close()
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub


        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function

        Public Shared Function ConChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                               ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDBLin As SAPbouiCOM.DBDataSource
                    Dim oMat As SAPbouiCOM.Matrix


                    oForm = oApplication.Forms.Item(FormUID)


                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOC")
                            oMat = oForm.Items.Item("1_U_G").Specific
                            oMat.FlushToDataSource()
                            oDBLin.SetValue("U_CodCon", oEvent.Row - 1, oDataTable.GetValue("DocEntry", 0))
                            oMat.LoadFromDataSourceEx(False)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function
        Public Shared Function EtpChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                   ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDBLin As SAPbouiCOM.DBDataSource
                    Dim oMat As SAPbouiCOM.Matrix


                    oForm = oApplication.Forms.Item(FormUID)
                  

                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOE")
                            oMat = oForm.Items.Item("0_U_G").Specific
                            oMat.FlushToDataSource()
                            oDBLin.SetValue("U_NomEtp", oEvent.Row - 1, oDataTable.GetValue("U_Nombre", 0))
                            oDBLin.SetValue("U_CodEtp", oEvent.Row - 1, oDataTable.GetValue("DocEntry", 0))
                            oMat.LoadFromDataSourceEx(False)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function

        Public Shared Function EtpConChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                   ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDBLin As SAPbouiCOM.DBDataSource
                    Dim oMat As SAPbouiCOM.Matrix


                    oForm = oApplication.Forms.Item(FormUID)


                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOC")
                            oMat = oForm.Items.Item("1_U_G").Specific
                            oMat.FlushToDataSource()
                            oDBLin.SetValue("U_CodEtp", oEvent.Row - 1, oDataTable.GetValue("DocEntry", 0))
                            oMat.LoadFromDataSourceEx(False)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function

        Public Shared Function ChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "0_U_G"
                    Select Case oEvent.ColUID
                        Case "C_0_1"
                            bBubbleEvent = EtpChooseFromList(FormUID, oEvent, oApplication, oCompany)
                    End Select

                Case "1_U_G"
                    Select Case oEvent.ColUID
                        Case "C_1_1"
                            bBubbleEvent = ConChooseFromList(FormUID, oEvent, oApplication, oCompany)
                        Case "C_1_2"
                            bBubbleEvent = EtpConChooseFromList(FormUID, oEvent, oApplication, oCompany)
                    End Select

            End Select

            Return bBubbleEvent
        End Function

        Private Shared Function LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim formulario As String = ""


            Try
                oForm.Freeze(True)

                'FORMULARIOS
                oCombo = oForm.Items.Item("cmbForm").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oCombo.Item.AffectsFormMode = True

                If Not PopulaForms(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de formularios")
                    oForm.Freeze(True)
                End If

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Private Shared Function PopulaEstados(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCol As SAPbouiCOM.Column) As Boolean
            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty
            Dim formulario As String = String.Empty
            Dim oDBCab As SAPbouiCOM.DBDataSource

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")
                formulario = Trim(oDBCab.GetValue("U_Form", 0))

                If Not formulario = "" Then
                    oDT = DatosAutorizaciones.GetEstados(Trim(formulario))
                    If Not oDT Is Nothing Then
                        oForm.Freeze(True)

                        If oCol.ValidValues.Count > 0 Then
                            If oDT.Rows.Count > 0 Then
                                'Primero se limpian los valores del combo
                                For i As Integer = 0 To oCol.ValidValues.Count - 1
                                    oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                Next


                                'Se adcionan los valores de la consulta
                                oCol.ValidValues.Add("-1", " ")
                                For Each row As DataRow In oDT.Rows
                                    codigo = row.Item("Code")
                                    desc = row.Item("Estado")
                                    oCol.ValidValues.Add(codigo, desc)
                                Next row

                                oForm.Freeze(False)
                                Return True
                            Else
                                For i As Integer = 0 To oCol.ValidValues.Count - 1
                                    oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                                Next
                                oForm.Freeze(False)
                                oApp.MessageBox("Se debe parametrizar los estados para formularios.")
                                Return False
                            End If
                        Else
                            If oDT.Rows.Count > 0 Then
                                'Se adcionan los valores de la consulta
                                oCol.ValidValues.Add("-1", " ")
                                For Each row As DataRow In oDT.Rows
                                    codigo = row.Item("Code")
                                    desc = row.Item("Estado")
                                    oCol.ValidValues.Add(codigo, desc)
                                Next row

                                oForm.Freeze(False)
                                Return True
                            Else
                                oForm.Freeze(False)
                                oApp.MessageBox("Se debe parametrizar los estados para formularios.")
                                Return False

                            End If

                        End If
                    Else
                        oForm.Freeze(False)
                        oApp.MessageBox("Se debe parametrizar los estados para formularios.")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Debe elejir un formulario para obtener los estados")
                    Return False
                End If



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function BorraFilaVaciaCond(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim Condicion As String = String.Empty
            Dim oMat As SAPbouiCOM.Matrix
            Try
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOC")
                oMat = oForm.Items.Item("1_U_G").Specific
borrar:

                For i As Integer = 0 To oDBLin.Size - 1
                    Condicion = Trim(oDBLin.GetValue("U_CodCon", i))

                    If Condicion = "" Then
                        oDBLin.RemoveRecord(i)
                        oMat.LoadFromDataSourceEx(False)
                        GoTo Borrar
                    End If
                Next
                oMat.LoadFromDataSourceEx(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function BorraFilaVaciaEtapa(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim etapa As String = String.Empty
            Dim oMat As SAPbouiCOM.Matrix
            Try
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOE")
                oMat = oForm.Items.Item("0_U_G").Specific
borrar:

                For i As Integer = 0 To oDBLin.Size - 1
                    etapa = Trim(oDBLin.GetValue("U_CodEtp", i))

                    If etapa = "" Then
                        oDBLin.RemoveRecord(i)
                        oMat.LoadFromDataSourceEx(False)
                        GoTo Borrar
                    End If
                Next
                oMat.LoadFromDataSourceEx(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function



        Private Shared Function PopulaForms(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosAutorizaciones.GetForms()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los formularios.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los formularios.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Private Shared Sub PopulaGrid(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, Optional Formulario As String = "")
            Dim grdAut As SAPbouiCOM.Grid
            Dim oDT As SAPbouiCOM.DataTable
            Dim oDTs As SAPbouiCOM.DataTables
            Dim numLin As Integer = 0
            Dim query As String = String.Empty
            Dim cond As Boolean = False

            Try
                oForm.Freeze(True)
                grdAut = oForm.Items.Item("grdGrpA").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dtGrpA")

                query = DatosAutorizaciones.PopulaGridGrpAut(Formulario)

                oDT.ExecuteQuery(query)

                grdAut.DataTable = oDT
                If grdAut.Rows.Count > 0 Then
                    For i As Integer = 0 To grdAut.Rows.Count - 1
                        numLin = oDT.GetValue("FilNum", i)
                        If Not numLin = 0 Then
                            grdAut.RowHeaders.SetText(i, numLin)
                        Else
                            grdAut.RowHeaders.SetText(i, "")
                        End If
                    Next
                End If

                grdAut.RowHeaders.TitleObject.Caption = "#"
                grdAut.Columns.Item(0).Visible = False 'FileNum
                grdAut.Columns.Item(0).Editable = False 'FileNum
                grdAut.Columns.Item(1).Editable = False 'Formulario
                grdAut.Columns.Item(2).Editable = False 'Accion
                grdAut.Columns.Item(3).Editable = False 'Grupo

                grdAut.AutoResizeColumns()

                oForm.Freeze(False)


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Exit Sub
            End Try
        End Sub

      
        Private Shared Function VaciaValidCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oMat As SAPbouiCOM.Matrix
            Dim oCol As SAPbouiCOM.Column

            Try
                oForm.Freeze(True)
                oMat = oForm.Items.Item("0_U_G").Specific
                oCol = oMat.Columns.Item("C_0_3")
                oCombo = oForm.Items.Item("cmbForm").Specific


                If oCombo.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCombo.ValidValues.Count - 1
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If

                If oCol.ValidValues.Count > 0 Then
                    For i As Integer = 0 To oCol.ValidValues.Count - 1
                        oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    Next
                End If

                oForm.Freeze(False)

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try


        End Function

#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUDOModDatEve(ByRef oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")

                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                        If pVal.BeforeAction = False Then
                            If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                    Try
                                        If Not VaciaValidCombos(oApp, oForm) Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Else

                                    Dim oMat As SAPbouiCOM.Matrix
                                    Dim oCol As SAPbouiCOM.Column

                                    Try
                                        If Not LoadCombos(oApp, oForm) Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                    oMat = oForm.Items.Item("0_U_G").Specific

                                    oCol = oMat.Columns.Item("C_0_3")
                                    oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        PopulaEstados(oApp, oForm, oCol)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                   
                                End If
                            Else
                                Try
                                    If Not VaciaValidCombos(oApp, oForm) Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End If
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                        If pVal.BeforeAction = True Then
                            Try
                                If ValidaSiExisteModForm(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    oApp.MessageBox("No se puede grabar un modelo para este formulario.Ya existe uno o ocurrió un error.")
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaNombreModelo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaDescripcionModelo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not BorraFilaVaciaCond(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not BorraFilaVaciaEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaSiExisteUnaEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaSiExisteUnaCond(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                       

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                        If pVal.BeforeAction = True Then

                            'Try
                            '    If Not ValidaSiExisteModForm(oApp, oForm) Then
                            '        oForm.Freeze(False)
                            '        oApp.MessageBox("No se puede grabar un modelo para este formulario.Ya existe uno o ocurrió un error.")
                            '        BubbleEvent = False
                            '        GoTo fail
                            '    End If
                            'Catch ex As Exception
                            '    oForm.Freeze(False)
                            '    oApp.MessageBox(ex.Message)
                            '    BubbleEvent = False
                            '    GoTo fail
                            'End Try

                            Try
                                If Not ValidaNombreModelo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaDescripcionModelo(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                            Try
                                If Not BorraFilaVaciaCond(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not BorraFilaVaciaEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                            Try
                                If Not ValidaSiExisteUnaEtapa(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                            Try
                                If Not ValidaSiExisteUnaCond(oApp, oForm) Then
                                    oForm.Freeze(False)
                                    BubbleEvent = False
                                    GoTo fail
                                End If
                            Catch ex As Exception
                                oForm.Freeze(False)
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try
                        End If

                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub

        Public Shared Sub AdminSBOFormUDOModNav(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, ByVal pVal As SAPbouiCOM.MenuEvent, ByVal bubbleEvent As Boolean)
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oDBLinE As SAPbouiCOM.DBDataSource
            Dim oDBLinC As SAPbouiCOM.DBDataSource
            Dim oMatE As SAPbouiCOM.Matrix
            Dim oMatC As SAPbouiCOM.Matrix
            Dim oCol As SAPbouiCOM.Column

            Try

                ' oForm = oApp.Forms.Item(pVal.FormUID)
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")
                oDBLinE = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOE")
                oDBLinC = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOC")
                oMatE = oForm.Items.Item("0_U_G").Specific
                oMatC = oForm.Items.Item("1_U_G").Specific

                If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Try
                            oCol = oMatE.Columns.Item("C_0_3")
                            oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                            PopulaEstados(oApp, oForm, oCol)

                        Catch ex As Exception
                            oForm.Freeze(False)
                            bubbleEvent = False
                            GoTo fail
                        End Try
                    Else
                        Try
                            VaciaValidCombos(oApp, oForm)

                        Catch ex As Exception
                            oForm.Freeze(False)
                            bubbleEvent = False
                            GoTo fail
                        End Try
                    End If

                Else
                    Try
                        VaciaValidCombos(oApp, oForm)

                    Catch ex As Exception
                        oForm.Freeze(False)
                        bubbleEvent = False
                        GoTo fail
                    End Try

                End If


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub

        Public Shared Sub AdminSBOFormModA(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.Before_Action = True Then
                            Select Case pVal.ItemUID
                                Case "cmbForm"
                                    Dim oCombo As SAPbouiCOM.ComboBox
                                    oCombo = oForm.Items.Item("cmbForm").Specific
                                    oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        If oCombo.ValidValues.Count = 0 Then
                                            PopulaForms(oApp, oForm, oCombo)
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                    'Case "21_U_E"
                                    '    Dim oCombo As SAPbouiCOM.ComboBox
                                    '    oCombo = oForm.Items.Item("21_U_E").Specific
                                    '    oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    '    Try
                                    '        PopulaEstados(oApp, oForm, oCombo)
                                    '    Catch ex As Exception
                                    '        oForm.Freeze(False)
                                    '        BubbleEvent = False
                                    '        GoTo fail
                                    '    End Try
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "cmbForm"
                                    Dim oCombo As SAPbouiCOM.ComboBox
                                    oCombo = oForm.Items.Item("cmbForm").Specific
                                    oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        If oCombo.ValidValues.Count = 0 Then
                                            PopulaForms(oApp, oForm, oCombo)
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        Select Case pVal.ItemUID
                            Case "cmbForm"
                                If pVal.Before_Action = False Then
                                    Dim oCombo As SAPbouiCOM.ComboBoxColumn
                                    Dim oMat As SAPbouiCOM.Matrix
                                    Dim oCol As SAPbouiCOM.Column


                                    oMat = oForm.Items.Item("0_U_G").Specific

                                    oCol = oMat.Columns.Item("C_0_3")
                                    oCol.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly

                                    Try
                                        PopulaEstados(oApp, oForm, oCol)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                End If
                        End Select

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.BeforeAction = False Then
                            Select Case pVal.ItemUID
                                Case "0_U_G"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                                Case "1_U_G"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE
                        Dim bandera As String = "1"


                        If pVal.BeforeAction = False Then
                            If pVal.ActionSuccess = True Then
                                Try
                                    bandera = oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx
                                Catch ex As Exception
                                End Try
                                If bandera <> "1" Then
                                    Dim oCombo As SAPbouiCOM.ComboBox
                                    Try
                                        oForm.Freeze(True)

                                        oForm.DataSources.UserDataSources.Item("BANDERA").ValueEx = "1"
                                        oForm.Freeze(False)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        GoTo fail
                                    End Try
                                End If

                            End If


                        End If

                End Select


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try
        End Sub
#End Region

#Region "VALIDACIONES"

        Public Shared Function ValidaDescripcionModelo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim Descr As String = String.Empty

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")

                Descr = Trim(oDBCab.GetValue("U_Descr", 0))

                If Trim(Descr) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe introducir un descripción al modelo.")
                    Return False
                Else
                    oForm.Freeze(False)
                    Return True
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaNombreModelo(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim Nombre As String = String.Empty

            Try
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")

                Nombre = Trim(oDBCab.GetValue("U_Nombre", 0))

                If Trim(Nombre) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe introducir un nombre al modelo.")
                    Return False
                Else
                    oForm.Freeze(False)
                    Return True
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function

        Public Shared Function ValidaSiExisteUnaEtapa(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim codetp As String = String.Empty

            Try
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOE")
                oDBLin.Clear()
                oMat = oForm.Items.Item("0_U_G").Specific

                oMat.FlushToDataSource()

                If oDBLin.Size > 0 Then
                    For i As Integer = 0 To oDBLin.Size - 1
                        codetp = Trim(oDBLin.GetValue("U_CodEtp", i))
                        If codetp = "" Then
                            oForm.Freeze(False)
                            oApp.MessageBox("Debe existir una etapa con codigo.")
                            Return False
                        End If
                    Next
                    oForm.Freeze(False)
                    Return True
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Debe existir al menos una etapa.")
                    Return False
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ValidaSiExisteUnaCond(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBLin As SAPbouiCOM.DBDataSource
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim oMat As SAPbouiCOM.Matrix
            Dim esCond As String = String.Empty
            Dim codcon As String = String.Empty

            Try
                oDBLin = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOC")
                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")
                oMat = oForm.Items.Item("1_U_G").Specific

                esCond = Trim(oDBCab.GetValue("U_EsConCod", 0))

                If esCond = "Y" Then
                    oMat.FlushToDataSource()

                    If oDBLin.Size > 0 Then
                        For i As Integer = 0 To oDBLin.Size - 1
                            codcon = Trim(oDBLin.GetValue("U_CodCon", i))
                            If codcon = "" Then
                                oForm.Freeze(False)
                                oApp.MessageBox("Debe existir una condición con codigo.")
                                Return False
                            End If
                        Next
                        oForm.Freeze(False)
                        Return True
                    Else
                        oForm.Freeze(False)
                        oApp.MessageBox("Debe existir al menos una condición.")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    Return True
                End If



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Public Shared Function ValidaSiExisteModForm(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim oDBCab As SAPbouiCOM.DBDataSource
            Dim formulario As String = String.Empty
            Dim res As String = String.Empty

            Try

                oDBCab = oForm.DataSources.DBDataSources.Item("@EXX_AUTOMOD")

                formulario = Trim(oDBCab.GetValue("U_Form", 0))

                res = DatosAutorizaciones.ValidaSiFormModelo(oApp, oForm, formulario)

                If Trim(res) = "S" Then
                    oForm.Freeze(False)
                    Return True

                ElseIf Trim(res) = "N" Then
                    oForm.Freeze(False)
                    Return False
                ElseIf Trim(res) = "E" Then
                    oForm.Freeze(False)
                    Return False
                Else
                    oForm.Freeze(False)
                    Return False
                End If



            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try


        End Function


#End Region


    End Class
End Namespace



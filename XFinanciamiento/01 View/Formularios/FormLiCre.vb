﻿Imports System.Configuration
Imports System.Windows.Forms
Imports System.Globalization
Imports System.Resources
Imports System.Reflection
Imports XFinanciamiento.Principal
Imports XFinanciamiento.Utiles
Imports XFinanciamiento.Data

Namespace View
    Public Class FormLiCre
        Public Shared Sub ItemEvent(ByRef oApp As SAPbouiCOM.Application, FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oForm As SAPbouiCOM.Form = oApp.Forms.Item(pVal.FormUID.ToString())

                Select Case pVal.EventType


                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        If pVal.Before_Action = False Then
                            Try
                                Dim oFrm As SAPbouiCOM.Form
                                oFrm = oApp.Forms.Item(pVal.FormUID.ToString())
                                Util.CreaLabelEnFormularioSap(oFrm, "lblLiCre", "282", 16, 0, 100, 14, True, "Linea de Credito", 6)
                                Util.CreaBotonEnFormularioSap(oFrm, "BtnLiC", "lblLiCre", 0, 100, 21, 15, True, "...", 6)
                            Catch ex As Exception
                                oApp.MessageBox(ex.Message)
                                BubbleEvent = False
                                GoTo fail
                            End Try

                        End If


                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.Before_Action = False Then
                            If pVal.ItemUID = "BtnLiC" Then

                                Dim SNCode As String
                                Dim SNName As String
                                Dim usrsignature As String

                                Dim oDBDatasource As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("OCRD")
                                SNCode = Trim(oDBDatasource.GetValue("CardCode", 0))
                                SNName = Trim(oDBDatasource.GetValue("CardName", 0))

                                Try
                                    usrsignature = BDLineaCredito.GetCodeFormSNLiCre(SNCode)
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try


                                If Trim(usrsignature) = "" Then
                                    oForm.Freeze(False)
                                    oApp.MessageBox("No se encontró el código de socio de negocio." + SNCode.ToString())
                                    BubbleEvent = False
                                    GoTo fail
                                End If

                                Try
                                    FormSNLiCre.CreaSBOForm(oApp, SNCode, SNName)
                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try
                            End If
                        Else
                            If pVal.ItemUID = "BtnProyUsr" Then
                                Dim usrcode As String
                                Dim usrname As String
                                Dim usrsignature As String
                                Try
                                    Dim oDBDatasource As SAPbouiCOM.DBDataSource = oForm.DataSources.DBDataSources.Item("OUSR")
                                    usrcode = Trim(oDBDatasource.GetValue("USER_CODE", 0))
                                    usrname = Trim(oDBDatasource.GetValue("U_NAME", 0))


                                    If Trim(usrcode) = "" Then
                                        oForm.Freeze(False)
                                        oApp.MessageBox("No se encontró el código de socio de negocio.")
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                    Try
                                        usrsignature = DatosAutorizaciones.GetUsrCodeFromName(usrcode)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try


                                    If Trim(usrsignature) = "" Then
                                        oForm.Freeze(False)
                                        oApp.MessageBox("No se encontró el código de socio de negocio.")
                                        BubbleEvent = False
                                        GoTo fail
                                    End If

                                    BubbleEvent = True
                                    GoTo fail

                                Catch ex As Exception
                                    oForm.Freeze(False)
                                    oApp.MessageBox(ex.Message)
                                    BubbleEvent = False
                                    GoTo fail
                                End Try


                            End If
                        End If

                End Select


fail:
                oForm.Freeze(False)
            Catch ex As Exception
                Dim oForm As SAPbouiCOM.Form
                oForm = oApp.Forms.Item(pVal.FormUID.ToString())
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBO Error:" & ex.ToString)
            End Try


        End Sub


    End Class


End Namespace



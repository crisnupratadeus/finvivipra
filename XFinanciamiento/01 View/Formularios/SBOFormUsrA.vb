﻿Imports System.Configuration
Imports SAPbobsCOM
Imports XFinanciamiento.Control.Transactions
Imports XFinanciamiento.Data.Constantes
Imports XFinanciamiento.Control.Negocio
Imports XFinanciamiento.Data
Imports XFinanciamiento.Control
Imports XFinanciamiento.Utiles
Imports System.Globalization

Namespace View

    Public Class SBOFormUsrA


#Region "METODOS"
        Public Shared Sub CreaSBOForm(ByVal oApp As SAPbouiCOM.Application, Optional Form As String = "", Optional DocEntry As String = "")
            Try
                Dim xmlDoc As New Xml.XmlDocument
                Dim strPath As String
                Dim blnexit As Boolean = RecorreFormulario(oApp, "SBOFormUsrA")

                If blnexit Then
                    Exit Sub
                End If

                strPath = Application.StartupPath & "\SBOFormUsrA.srf"
                xmlDoc.Load(strPath)
                oApp.LoadBatchActions(xmlDoc.InnerXml)

                oForm = oApp.Forms.Item("SBOFormUsrA")

                LoadCombos(oApp, oForm)

                PopulaGridAlr(oApp, oForm)

                oForm.Visible = True

            Catch ex As Exception
                oApp.MessageBox("Crear SBO FORM Error:" & ex.Message)
            End Try
        End Sub

        Private Shared Function RecorreFormulario(ByVal oApp As SAPbouiCOM.Application, ByVal Formulario As String) As Boolean

            Dim blnexit As Boolean = False

            Try
                For Each oForm In oApp.Forms
                    Select Case oForm.UniqueID
                        Case Formulario
                            oForm.Visible = True
                            oForm.Select()
                            blnexit = True
                            Return blnexit
                            Exit Function
                    End Select
                Next

                For Each oForm In oApp.Forms
                    If oForm.UniqueID = Formulario Then
                        oForm.Select()
                        oForm.Close()
                        blnexit = True
                        Return blnexit
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try

            Return blnexit

        End Function


        Private Shared Function LoadCombos(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim edtTxt As SAPbouiCOM.EditText
            Dim oGrd As SAPbouiCOM.Grid
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim uDS As SAPbouiCOM.UserDataSource


            Try
                oForm.Freeze(True)
                oCombo = oForm.Items.Item("cmbForm").Specific
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
                oCombo.Item.AffectsFormMode = False


                If Not PopulaForms(oApp, oForm, oCombo) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al popular el combo de formularios")
                    oForm.Freeze(True)
                End If

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function PopulaForms(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form, oCombo As SAPbouiCOM.ComboBox) As Boolean

            Dim oDT As DataTable
            Dim codigo As String = String.Empty
            Dim desc As String = String.Empty

            Try
                oDT = DatosAutorizaciones.GetForms()
                If Not oDT Is Nothing Then
                    oForm.Freeze(True)

                    If oCombo.ValidValues.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        If oDT.Rows.Count > 0 Then
                            'Se adcionan los valores de la consulta
                            For Each row As DataRow In oDT.Rows
                                codigo = row.Item("Code")
                                desc = row.Item("Name")
                                oCombo.ValidValues.Add(codigo, desc)
                            Next row

                            oForm.Freeze(False)
                            Return True
                        Else
                            oForm.Freeze(False)
                            oApp.MessageBox("Se debe parametrizar los formularios.")
                            Return False

                        End If

                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Se debe parametrizar los formularios.")
                    Return False
                End If


            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Public Shared Function ProyChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                        ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDSUsrC As SAPbouiCOM.UserDataSource

                    oForm = oApplication.Forms.Item(FormUID)
                    oDSUsrC = oForm.DataSources.UserDataSources.Item("udProy")

                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDSUsrC.ValueEx = oDataTable.GetValue("PrjCode", 0)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function
        Public Shared Function UsrChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDSUsrC As SAPbouiCOM.UserDataSource
                    Dim oDSUsrN As SAPbouiCOM.UserDataSource

                    oForm = oApplication.Forms.Item(FormUID)
                    oDSUsrC = oForm.DataSources.UserDataSources.Item("udUsrC")
                    oDSUsrN = oForm.DataSources.UserDataSources.Item("udUsrN")

                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDSUsrN.ValueEx = oDataTable.GetValue("USER_CODE", 0)
                            oDSUsrC.ValueEx = oDataTable.GetValue("USERID", 0)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function
        Public Shared Function EtpChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, _
                                                 ByVal oCompany As SAPbobsCOM.Company) As Boolean


            Try
                Dim bBubbleEvent As Boolean = True

                If Not oEvent.BeforeAction Then
                    Dim oForm As SAPbouiCOM.Form
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oDSEtpC As SAPbouiCOM.UserDataSource
                    Dim oDSEtpN As SAPbouiCOM.UserDataSource

                    oForm = oApplication.Forms.Item(FormUID)
                    oDSEtpC = oForm.DataSources.UserDataSources.Item("udEtpC")
                    oDSEtpN = oForm.DataSources.UserDataSources.Item("udEtpN")

                    oDataTable = Util.f_GetDataTableFromCFL(oEvent, oForm)
                    If Not oDataTable Is Nothing Then
                        Try
                            oForm.Freeze(True)
                            oDSEtpN.ValueEx = oDataTable.GetValue("U_Nombre", 0)
                            oDSEtpC.ValueEx = oDataTable.GetValue("DocEntry", 0)
                            oForm.Freeze(False)
                        Catch ex As Exception
                        End Try
                    Else
                        oForm.Freeze(False)
                        Return False
                    End If
                End If
                oForm.Freeze(False)
                Return bBubbleEvent
            Catch ex As Exception
                oForm.Freeze(False)
                Throw ex
                Return False
            End Try

        End Function

        Public Shared Function ChooseFromList(ByRef FormUID As String, ByRef oEvent As SAPbouiCOM.ItemEvent, ByRef oApplication As SAPbouiCOM.Application, ByVal oCompany As SAPbobsCOM.Company) As Boolean
            Dim bBubbleEvent As Boolean = True
            Dim oForm As SAPbouiCOM.Form

            oForm = oApplication.Forms.Item(FormUID)
            Select Case oEvent.ItemUID
                Case "edtEtpC"
                    bBubbleEvent = EtpChooseFromList(FormUID, oEvent, oApplication, oCompany)
                Case "edtUsr"
                    bBubbleEvent = UsrChooseFromList(FormUID, oEvent, oApplication, oCompany)
                Case "edtPres"
                    bBubbleEvent = ProyChooseFromList(FormUID, oEvent, oApplication, oCompany)
            End Select

            Return bBubbleEvent
        End Function

        Public Shared Function PopulaGridAlr(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim queryGrid As String = String.Empty
            Dim oDTs As SAPbouiCOM.DataTables
            Dim oDT As SAPbouiCOM.DataTable
            Dim oGrid As SAPbouiCOM.Grid
            Dim numLin As Integer
            Dim Formulario As String = String.Empty


            Try
                oForm.Freeze(True)

                oGrid = oForm.Items.Item("grdAlrm").Specific
                oDTs = oForm.DataSources.DataTables
                oDT = oDTs.Item("dtAlrm")

                Try
                    Formulario = oForm.DataSources.UserDataSources.Item("udForm").ValueEx
                Catch ex As Exception
                    Formulario = ""
                End Try

                queryGrid = DatosDB.PopulaAlarmas(Trim(Formulario))
                oDT.ExecuteQuery(queryGrid)
                oGrid.DataTable = oDT

                If oGrid.Rows.Count > 0 Then
                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        numLin = numLin + 1
                        oGrid.RowHeaders.SetText(i, numLin)
                    Next
                End If


                oGrid.RowHeaders.TitleObject.Caption = "#"
                oGrid.Columns.Item("CODIGOUSUARIO").Editable = False
                oGrid.Columns.Item("NOMBREUSUARIO").Editable = False
                oGrid.Columns.Item("CODIGOETAPA").Editable = False
                oGrid.Columns.Item("NOMBREETAPA").Editable = False
                '  oGrid.Columns.Item("PROYECTO").Editable = False 'cnp20190710

                oGrid.AutoResizeColumns()

                oForm.Freeze(False)
                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function

        Private Shared Function BorraAlarma(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim Formulario As String = String.Empty
            Dim CodEtapa As String = String.Empty
            Dim CodUsr As String = String.Empty
            Dim Proyecto As String = String.Empty
            Dim oGrid As SAPbouiCOM.Grid


            Try
                oGrid = oForm.Items.Item("grdAlrm").Specific

                Formulario = oForm.DataSources.UserDataSources.Item("udForm").ValueEx

                If oGrid.Rows.SelectedRows.Count > 0 Then

                    For i As Integer = 0 To oGrid.Rows.Count - 1
                        If oGrid.Rows.IsSelected(i) Then
                            ' Proyecto = Trim(oGrid.DataTable.GetValue("PROYECTO", i))  'cnp20190710
                            CodEtapa = Trim(oGrid.DataTable.GetValue("CODIGOETAPA", i))
                            CodUsr = Trim(oGrid.DataTable.GetValue("CODIGOUSUARIO", i))

                            If Not DatosAutorizaciones.BorraAlarmaUsr(Trim(CodUsr), Trim(CodEtapa), Trim(Formulario)) Then 'cnp20190710
                                oForm.Freeze(False)
                                oApp.MessageBox("Error al borrar la alarma para la fila " & i & " . Intente nuevamente.")
                                Return False
                            End If

                        End If
                    Next

                Else
                    Return True

                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try


        End Function


        Private Shared Function GuardaAlarma(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim Formulario As String = String.Empty
            Dim CodEtapa As String = String.Empty
            Dim NomEtapa As String = String.Empty
            Dim CodUsr As String = String.Empty
            Dim NomUsr As String = String.Empty
            Dim Proyecto As String = String.Empty

            Try
                Formulario = oForm.DataSources.UserDataSources.Item("udForm").ValueEx
                CodEtapa = oForm.DataSources.UserDataSources.Item("udEtpC").ValueEx
                NomEtapa = oForm.DataSources.UserDataSources.Item("udEtpN").ValueEx
                CodUsr = oForm.DataSources.UserDataSources.Item("udUsrC").ValueEx
                NomUsr = oForm.DataSources.UserDataSources.Item("udUsrN").ValueEx
                '  Proyecto = oForm.DataSources.UserDataSources.Item("udProy").ValueEx


                If Not DatosAutorizaciones.InsertaUsrAlarmas(CodUsr, NomUsr, Formulario, CodEtapa, NomEtapa) Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Error al grabar la alarma con los datos proporcionados. Intente nuevamente.")
                    Return False
                End If

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


#End Region

#Region "VALIDACIONES"

        Private Shared Function ValidaSiExisteUsrEtpAlarma(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean
            Dim Formulario As String = String.Empty
            Dim CodEtapa As String = String.Empty
            Dim NomEtapa As String = String.Empty
            Dim CodUsr As String = String.Empty
            Dim NomUsr As String = String.Empty
            Dim Proyecto As String = String.Empty
            Dim res As String = String.Empty

            Try

                Formulario = oForm.DataSources.UserDataSources.Item("udForm").ValueEx
                CodEtapa = oForm.DataSources.UserDataSources.Item("udEtpC").ValueEx
                CodUsr = oForm.DataSources.UserDataSources.Item("udUsrC").ValueEx
                ' Proyecto = oForm.DataSources.UserDataSources.Item("udProy").ValueEx

                res = DatosAutorizaciones.ValidaAlarmasEtapaUsr(oApp, oForm, Trim(CodUsr), Trim(CodEtapa), Trim(Formulario)) 'cnp20190710

                If res = "S" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Ya existe un registro de alarma para la información introducida.")
                    Return False
                ElseIf res = "N" Then
                    oForm.Freeze(False)
                    Return True
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("Ocurrió un error al intentar validar la informacion para el registro.")
                    Return False
                End If

            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try

        End Function


        Private Shared Function ValidaSiDatosDelete(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim oGrid As SAPbouiCOM.Grid

            Try
                oGrid = oForm.Items.Item("grdAlrm").Specific

                If oGrid.Rows.Count > 0 Then
                    If oGrid.Rows.SelectedRows.Count > 0 Then
                        oForm.Freeze(False)
                        Return True
                    Else
                        oForm.Freeze(False)
                        oApp.MessageBox("No existen filas seleccionadas para borrar. Intente nuevamente")
                        Return False
                    End If
                Else
                    oForm.Freeze(False)
                    oApp.MessageBox("No existen filas seleccionadas para borrar. Intente nuevamente")
                    Return False
                End If
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function


        Private Shared Function ValidaDatosInsert(ByRef oApp As SAPbouiCOM.Application, ByRef oForm As SAPbouiCOM.Form) As Boolean

            Dim Formulario As String = String.Empty
            Dim CodEtapa As String = String.Empty
            Dim NomEtapa As String = String.Empty
            Dim CodUsr As String = String.Empty
            Dim NomUsr As String = String.Empty
            Dim Proyecto As String = String.Empty

            Try
                Formulario = oForm.DataSources.UserDataSources.Item("udForm").ValueEx
                CodEtapa = oForm.DataSources.UserDataSources.Item("udEtpC").ValueEx
                NomEtapa = oForm.DataSources.UserDataSources.Item("udEtpN").ValueEx
                CodUsr = oForm.DataSources.UserDataSources.Item("udUsrC").ValueEx
                NomUsr = oForm.DataSources.UserDataSources.Item("udUsrN").ValueEx
                '  Proyecto = oForm.DataSources.UserDataSources.Item("udProy").ValueEx 'cnp20190710

                If Trim(Formulario) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Es necesario elegir un formulario para la grabación de la alarma.")
                    Return False
                End If

                If Trim(CodEtapa) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Es necesario elegir una etapa para la grabación de la alarma.")
                    Return False
                End If

                If Trim(CodUsr) = "" Then
                    oForm.Freeze(False)
                    oApp.MessageBox("Es necesario elegir un usuario para la grabación de la alarma.")
                    Return False
                End If

                'If Trim(Proyecto) = "" Then
                '    oForm.Freeze(False)
                '    oApp.MessageBox("Es necesario elegir un proyecto para la grabación de la alarma.")
                '    Return False
                'End If 'cnp20190710

                Return True
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox(ex.Message)
                Return False
            End Try
        End Function
#End Region

#Region "EVENTOS"
        Public Shared Sub AdminSBOFormUsrA(oApp As SAPbouiCOM.Application, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                oForm = oApp.Forms.Item(pVal.FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID

                                Case "edtEtpC"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "edtUsr"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "edtPres"
                                    Try
                                        ChooseFromList(pVal.FormUID.ToString, pVal, oApp, DiApp)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                            End Select
                        End If


                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.Before_Action = False Then

                            Select Case pVal.ItemUID

                                Case "btnCerr"
                                    oForm.Freeze(False)
                                    BubbleEvent = True
                                    oForm.Close()
                                    Exit Sub

                                Case "btnGrab"

                                    Try
                                        If Not GuardaAlarma(oApp, oForm) Then
                                            oForm.Freeze(False)
                                            BubbleEvent = False
                                            GoTo fail
                                        Else
                                            oForm.Freeze(False)
                                            oApp.MessageBox("Se guardo la alarma exitosamente.")
                                            BubbleEvent = True
                                            PopulaGridAlr(oApp, oForm)
                                            GoTo fail
                                        End If
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try

                                Case "btnElim"

                                    If Not BorraAlarma(oApp, oForm) Then
                                        oForm.Freeze(False)
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        oForm.Freeze(False)
                                        oApp.MessageBox("Se eliminó la alarma seleccionada")
                                        BubbleEvent = True
                                        PopulaGridAlr(oApp, oForm)
                                        GoTo fail
                                    End If

                            End Select
                        Else
                            Select Case pVal.ItemUID
                                Case "btnGrab"

                                    If Not ValidaDatosInsert(oApp, oForm) Then
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        If Not ValidaSiExisteUsrEtpAlarma(oApp, oForm) Then
                                            BubbleEvent = False
                                            GoTo fail
                                        Else
                                            BubbleEvent = True
                                            GoTo fail
                                        End If
                                    End If
                                Case "btnElim"
                                    If Not ValidaSiDatosDelete(oApp, oForm) Then
                                        BubbleEvent = False
                                        GoTo fail
                                    Else
                                        BubbleEvent = True
                                        GoTo fail
                                    End If
                            End Select
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        If pVal.Before_Action = False Then
                            Select Case pVal.ItemUID
                                Case "cmbForm"
                                    Try
                                        PopulaGridAlr(oApp, oForm)
                                    Catch ex As Exception
                                        oForm.Freeze(False)
                                        oApp.MessageBox(ex.Message)
                                        BubbleEvent = False
                                        GoTo fail
                                    End Try
                            End Select
                        End If
                End Select

fail:
                oForm.Freeze(False)
            Catch ex As Exception
                oForm.Freeze(False)
                oApp.MessageBox("AdminSBOForm Error:" & ex.ToString)
            End Try

        End Sub
#End Region

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class
End Namespace



﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Permissions
Imports System.Windows.Forms
Imports System.Management
Imports System.IO
Imports SAPbobsCOM
Imports XFinanciamiento.View
Imports XFinanciamiento.data
Imports XFinanciamiento.Data.Constantes
Imports System.Configuration
Imports XFinanciamiento.Control.Negocio
Imports System.Globalization
'Imports CarDealer.menu



Public Class Principal

    Public WithEvents oApp As SAPbouiCOM.Application


#Region "NEW-MENU-CONECCION-VERIFICA CATALOGO"

    ''' <summary>
    ''' NEW CLASS
    ''' </summary>
    Public Sub New()
        Try
            oApp = Sap.GetApplication
        Catch ex As Exception
            oApp.MessageBox("Descripcion del Error:" & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' FILTROS DE EVENTOS PARA LOS FROMULARIOS
    ''' </summary>
    Public Sub SetFilters()
        Try
            Dim EFilters As New SAPbouiCOM.EventFilters
            Dim EFilter As SAPbouiCOM.EventFilter

            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_PICKER_CLICKED) 'cnp20190711
            EFilter.AddEx("UDO_FT_CNTT")
            EFilter.AddEx("UDO_FT_VEHC")
            EFilter.AddEx("UDO_FT_AUTO")
            EFilter.AddEx("UDO_FT_FAMI")
            EFilter.AddEx("UDO_FT_VEHU")
            EFilter.AddEx("UDO_FT_REVA")
            EFilter.AddEx("UDO_FT_IVEH")
            EFilter.AddEx("149")
            EFilter.AddEx("721")
            EFilter.AddEx("179")
            EFilter.AddEx("170")
            EFilter.AddEx("140")
            EFilter.AddEx("143")
            EFilter.AddEx("60092")
            EFilter.AddEx("181")
            EFilter.AddEx("992")
            EFilter.AddEx("UDO_FT_EXXASISENT")
            EFilter.AddEx("UDO_FT_LICRE")
            EFilter.AddEx("UDO_FT_FIPLA")
            EFilter.AddEx("FormGarLinea")
            EFilter.AddEx("UDO_F_AUTOCOND")
            EFilter.AddEx("UDO_F_AUTOETAP")
            EFilter.AddEx("UDO_F_AUTOMOD")
            EFilter.AddEx("UDO_FT_CUOTA")
            EFilter.AddEx("UDO_FT_REFIN")
            EFilter.AddEx("133")

            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE)
            EFilter.AddEx("UDO_FT_CNTT")
            EFilter.AddEx("UDO_FT_VEHC")
            EFilter.AddEx("UDO_FT_AUTO")
            EFilter.AddEx("UDO_FT_FAMI")
            EFilter.AddEx("UDO_FT_VEHU")
            EFilter.AddEx("UDO_FT_REVA")
            EFilter.AddEx("UDO_FT_IVEH")
            EFilter.AddEx("UDO_FT_EXXASISENT")
            EFilter.AddEx("UDO_FT_FIPLA")
            EFilter.AddEx("UDO_FT_LICRE")
            EFilter.AddEx("UDO_F_AUTOCOND")
            EFilter.AddEx("UDO_F_AUTOETAP")
            EFilter.AddEx("UDO_F_AUTOMOD")
            EFilter.AddEx("FormGarLinea")

            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD)
            EFilter.AddEx("UDO_FT_CNTT")
            EFilter.AddEx("UDO_FT_VEHC")
            EFilter.AddEx("UDO_FT_AUTO")
            EFilter.AddEx("UDO_FT_FAMI")
            EFilter.AddEx("UDO_FT_VEHU")
            EFilter.AddEx("UDO_FT_REVA")
            EFilter.AddEx("UDO_FT_IVEH")
            EFilter.AddEx("UDO_FT_EXXASISENT")
            EFilter.AddEx("60092")
            EFilter.AddEx("UDO_FT_LICRE")
            EFilter.AddEx("UDO_FT_FIPLA")
            EFilter.AddEx("UDO_FT_CUOTA")
            ' EFilter.AddEx("UDO_FT_REFIN")

            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            EFilter.AddEx("UDO_FT_CNTT")
            EFilter.AddEx("UDO_FT_VEHC")
            EFilter.AddEx("UDO_FT_AUTO")
            EFilter.AddEx("UDO_FT_FAMI")
            EFilter.AddEx("UDO_FT_VEHU")
            EFilter.AddEx("UDO_FT_REVA")
            EFilter.AddEx("UDO_FT_IVEH")
            EFilter.AddEx("UDO_FT_EXXASISENT")
            EFilter.AddEx("UDO_FT_LICRE")
            EFilter.AddEx("UDO_FT_FIPLA")
            EFilter.AddEx("UDO_FT_CUOTA")

            EFilter = EFilters.Add(SAPbouiCOM.BoEventTypes.et_PRINT_LAYOUT_KEY)
            EFilter.AddEx("UDO_FT_CNTT")
            EFilter.AddEx("1472000036")
            EFilter.AddEx("UDO_F_AUTOCOND")
            EFilter.AddEx("UDO_F_AUTOETAP")
            EFilter.AddEx("UDO_F_AUTOMOD")
            oApp.SetFilter(EFilters)
        Catch ex As Exception
            oApp.StatusBar.SetText(ex.Message)
            oApp.MessageBox("SetFilters Error:" & ex.Message)

        End Try

    End Sub

#End Region

#Region "CREACION MENUS"
    Private Sub AddMenuParam()
        Dim oMenuItem As SAPbouiCOM.MenuItem
        Dim spath As String = Application.StartupPath
        Dim oCreationPkg As SAPbouiCOM.MenuCreationParams
        Dim oMenu As SAPbouiCOM.Menus = oApp.Menus

        Try
            'Se Crea el Menu de Parametrizacion de Tablas y Campos
            oCreationPkg = oApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
            oMenuItem = oApp.Menus.Item("XFmnuFINI")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_POPUP
            If oApp.Menus.Exists("XFmnuPARAM") Then
                oApp.Menus.RemoveEx("XFmnuPARAM")
            End If
            oCreationPkg.UniqueID = "XFmnuPARAM"
            oCreationPkg.String = "Acerca de XFinanciamiento"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            'Link para Crear Campos
            oMenuItem = oApp.Menus.Item("XFmnuPARAM")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuCCMAFVT") Then
                oApp.Menus.RemoveEx("XFmnuCCMAFVT")
            End If
            oCreationPkg.UniqueID = "XFmnuCCMAFVT"
            oCreationPkg.String = "Crear y Verificar Tablas"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            'Se crea el menu de Configuraciones (CONFIG- MAESTROS)
            oMenuItem = oApp.Menus.Item("XFmnuFINI")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_POPUP
            If oApp.Menus.Exists("XFmnuCONF") Then
                oApp.Menus.RemoveEx("XFmnuCONF")
            End If
            oCreationPkg.UniqueID = "XFmnuCONF"
            oCreationPkg.String = "Configuración"
            oCreationPkg.Position = 2
            oMenu.AddEx(oCreationPkg)

            oMenuItem = oApp.Menus.Item("XFmnuCONF")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuPCNTT") Then
                oApp.Menus.RemoveEx("XFmnuPCNTT")
            End If
            oCreationPkg.UniqueID = "XFmnuPCNTT"
            oCreationPkg.String = "Configuración General Addon"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            oMenuItem = oApp.Menus.Item("XFmnuCONF")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuCONV") Then
                oApp.Menus.RemoveEx("XFmnuCONV")
            End If
            oCreationPkg.UniqueID = "XFmnuCONV"
            oCreationPkg.String = "Configuración Financiamiento"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            oMenuItem = oApp.Menus.Item("XFmnuCONF")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuCATE") Then
                oApp.Menus.RemoveEx("XFmnuCATE")
            End If
            oCreationPkg.UniqueID = "XFmnuCATE"
            oCreationPkg.String = "Maestro Financiamiento"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            oMenuItem = oApp.Menus.Item("XFmnuFINI")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuFIN") Then
                oApp.Menus.RemoveEx("XFmnuFIN")
            End If
            oCreationPkg.UniqueID = "XFmnuFIN"
            oCreationPkg.String = "Financiamiento"
            oCreationPkg.Position = 3
            oMenu.AddEx(oCreationPkg)

            oMenuItem = oApp.Menus.Item("XFmnuFIN")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuLC") Then
                oApp.Menus.RemoveEx("XFmnuLC")
            End If
            oCreationPkg.UniqueID = "XFmnuLC"
            oCreationPkg.String = "Linea de Crédito"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            'Menu Refinancimiento
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_POPUP
            If oApp.Menus.Exists("XFmnuREFI") Then
                oApp.Menus.RemoveEx("XFmnuREFI")
            End If
            oCreationPkg.UniqueID = "XFmnuREFI"
            oCreationPkg.String = "Refinanciamiento"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)

            'Link para Refinanciamiento
            oMenuItem = oApp.Menus.Item("XFmnuREFI")
            oMenu = oMenuItem.SubMenus
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_STRING
            If oApp.Menus.Exists("XFmnuREFIN") Then
                oApp.Menus.RemoveEx("XFmnuREFIN")
            End If
            oCreationPkg.UniqueID = "XFmnuREFIN"
            oCreationPkg.String = "Refinanciar"
            oCreationPkg.Position = 1
            oMenu.AddEx(oCreationPkg)


        Catch exx As System.Runtime.InteropServices.COMException
            Select Case exx.ErrorCode
                Case -7130
                Case Else
                    oApp.MessageBox("Agregar menu Error:" & exx.Message)
            End Select
        Catch exx As Exception
            oApp.MessageBox("Agregar menu Error:" & exx.Message)
        End Try

    End Sub

    ''' <summary>
    ''' AGREGA MENU
    ''' </summary>
    ''' 

    Public Sub RemoveMenuXML(ByRef oApp As SAPbouiCOM.Application)
        Dim oMenuForm As SAPbouiCOM.Form


        Try
            oMenuForm = oApp.Forms.GetFormByTypeAndCount(169, 1)
            oMenuForm.Freeze(True)

            If oApp.Menus.Exists("XFmnuFINI") Then
                oApp.Menus.RemoveEx("XFmnuFINI")
            End If


        Catch ex As Exception
            oMenuForm.Freeze(False)
            oApp.MessageBox(ex.Message)
            Exit Sub
        Finally
            oMenuForm.Freeze(False)
            oMenuForm.Update()
        End Try
    End Sub


    Public Sub AddMenuXML(ByRef oApp As SAPbouiCOM.Application)
        Dim xmlDoc As New Xml.XmlDocument
        Dim strPathXml As String = String.Empty
        Dim strPathLogo As String = String.Empty
        Dim oMenuForm As SAPbouiCOM.Form
        Dim oMenuItem As SAPbouiCOM.MenuItem


        Try
            strPathXml = Application.StartupPath & "\MenuXFinanciamiento.xml"
            strPathLogo = Application.StartupPath & "\LogoFin.png"
            oMenuForm = oApp.Forms.GetForm(169, 1)
            oMenuForm.Freeze(True)

            xmlDoc.Load(strPathXml)

            If oApp.Menus.Exists("XFmnuFINI") Then
                oApp.Menus.RemoveEx("XFmnuFINI")
            End If
            oApp.LoadBatchActions(xmlDoc.InnerXml)

            oMenuItem = oApp.Menus.Item(xmlDoc.SelectSingleNode("Application/Menus/action/Menu/@UniqueID").Value)
            oMenuItem.Image = strPathLogo

        Catch ex As Exception
            oMenuForm.Freeze(False)
            oApp.MessageBox(ex.Message)
            xmlDoc = Nothing
            Exit Sub
        Finally
            oMenuForm.Freeze(False)
            oMenuForm.Update()
            xmlDoc = Nothing
        End Try
    End Sub

    Public Sub AddMenuItems()

        Dim oMenuItem As SAPbouiCOM.MenuItem
        Dim spath As String = Application.StartupPath
        Dim oCreationPkg As SAPbouiCOM.MenuCreationParams
        Dim oMenu As SAPbouiCOM.Menus = oApp.Menus

        Try

            'Menu Principal para Localización Bolivia
            oMenuItem = oApp.Menus.Item("43520")
            oCreationPkg = oApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
            oCreationPkg.Type = SAPbouiCOM.BoMenuType.mt_POPUP
            If oApp.Menus.Exists("XFmnuFINI") Then
                oApp.Menus.RemoveEx("XFmnuFINI")
            End If
            oCreationPkg.UniqueID = "XFmnuFINI"
            oCreationPkg.String = "CarDealer"
            oCreationPkg.Enabled = True
            oCreationPkg.Image = spath & "\cardealer.png"
            oCreationPkg.Position = 14
            oMenu = oMenuItem.SubMenus
            oMenu.AddEx(oCreationPkg)
            'Se agregan los menus de parametrización
            AddMenuParam()
        Catch exx As System.Runtime.InteropServices.COMException

            Select Case exx.ErrorCode
                Case -7130
                Case Else
                    oApp.MessageBox("Agregar menu Error:" & exx.Message)
            End Select
        Catch exx As Exception

            oApp.MessageBox("Agregar menu Error:" & exx.Message)
        End Try

    End Sub
#End Region


#Region "EVENTOS SBO"

    ''' <summary>
    ''' EVENTOS DE LA APLICACION SBO
    ''' </summary>
    Private Sub oApp_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles oApp.AppEvent
        Try
            Dim sErr As Long, msg As String
            Select Case EventType
                Case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged
                    'TClass.RemoveMenuXML(oApp)
                    DiApp.Disconnect()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(DiApp)
                    DiApp = Nothing
                    End
                Case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged
                    TClass.RemoveMenuXML(oApp)
                    DiApp.Disconnect()
                    sErr = TClassCon.ConectaDI()
                    If sErr <> 0 Then
                        Call DiApp.GetLastError(sErr, msg)
                        AppSap.MessageBox(msg & vbCrLf & "El Addons se Desconectará.")
                        Application.Exit()
                    End If

                    Select Case oApp.Language = SAPbouiCOM.BoLanguages.ln_English
                        Case SAPbouiCOM.BoLanguages.ln_Spanish_La
                            'oExxCulture.Culture = "Spanish (Latin America)"
                        Case SAPbouiCOM.BoLanguages.ln_English
                            'oExxCulture.Culture = "English (United States)"
                        Case SAPbouiCOM.BoLanguages.ln_Portuguese_Br
                            'oExxCulture.Culture = "Portuguese (Brazil)"
                        Case Else
                            'oExxCulture.Culture = "English (United States)"
                    End Select

                    'aca reconstruir el menu
                    TClass.AddMenuXML(oApp)
                Case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition
                    DiApp.Disconnect()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(DiApp)
                    DiApp = Nothing
                    Application.Exit()
                    End
                Case SAPbouiCOM.BoAppEventTypes.aet_ShutDown
                    DiApp.Disconnect()
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(DiApp)
                    DiApp = Nothing
                    Application.Exit()
                    End
            End Select

        Catch ex As Exception
            oApp.MessageBox("AppEvent Error:" & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' EVENTOS DEL MENU
    ''' </summary>
    Private Sub oApp_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles oApp.MenuEvent
        Try

            Dim oForm As SAPbouiCOM.Form
            Try
                oForm = oApp.Forms.ActiveForm
            Catch ex As Exception
                'BubbleEvent = False
                'Exit Sub
            End Try

            'Controlo beforeaction, de lo contrario pasa dos ocaciones
            If pVal.BeforeAction = False Then

                'If Not General.Config_Decimales() Then
                '    oApp.MessageBox("Separador Decimal de B1 es distinto al de la Configuración Regional" & vbCrLf & _
                '                   "y deben ser iguales. Verifique ...")
                '    GC.Collect()
                '    Application.Exit()
                'End If

                Select Case pVal.MenuUID
                    Case "XFmnuCCMAFVT"
                        SBOFormVTC.CreaSBOFormVTC(oApp)
                    Case "XFmnuLC"
                        SBOFormLineCredito.CreaSBOForm(oApp)
                    Case "XFmnuCATE"
                        SBOFormUDOGar.CreaSBOForm(oApp)
                    Case "XFmnuPCNTT"
                        FormParamFinan.CreaSBOForm(oApp)
                    Case "XFmnuFIPLA"
                        SBOFormUDOFIPLA.CreaSBOForm(oApp)
                    Case "XFmnuFECH"
                        SBOFormFechaLst.CreaSBOForm(oApp)
                    Case "XFmnuMORE"
                        SBOFormMotRec.CreaSBOForm(oApp)

                    Case "1292", "1293", "1288", "1290", "1291", "1289", "1282", "1281"
                        Dim form As SAPbouiCOM.Form
                        form = oApp.Forms.ActiveForm
                        If form.TypeEx = "UDO_FT_LICRE" Then
                            SBOFormLineCredito.AdminSBOFormUDOLCNav(oApp, oForm, pVal, BubbleEvent)
                        End If
                        If form.TypeEx = "139" Then
                            SBOFormOV.FormOVNav(oApp, oForm, pVal, BubbleEvent)
                        End If

                    Case "XFmnuEAutoE"
                        SBOFormEtpA.CreaSBOForm(oApp)
                    Case "XFmnuEAutoC"
                        SBOFormConA.CreaSBOForm(oApp)
                    Case "XFmnuEAutoA"
                        SBOFormUsrA.CreaSBOForm(oApp)
                    Case "XFmnuEAutoG"
                        SBOFormModA.CreaSBOForm(oApp)
                        'Case "XFmnuREFIN"
                        '   SBOFormRefin.CreaSBOForm(oApp)



                End Select
            Else
                Select Case pVal.MenuUID
                    'Case "1292", "1293","1288", "1290", "1291", "1289", "1282", "1281"
                    '    Dim form As SAPbouiCOM.Form
                    '    form = oApp.Forms.ActiveForm
                    '    If form.TypeEx = "UDO_FT_LICRE" Then
                    '        SBOFormLineCredito.AdminSBOFormUDOLCNav(oApp, oForm, pVal, BubbleEvent)
                    '    End If                    
                    Case "1281", "1282"
                        SBOFormUDOCuotas.AdminSBOFormUDOCuotaNav(oApp, oForm, pVal, BubbleEvent)
                End Select
            End If
        Catch ex As Exception
            oApp.MessageBox("MenuEvent Error:" & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' EVENTOS DE LOS ITEMS SEGUN FROMULARIO
    ''' </summary>
    Private Sub oApp_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles oApp.ItemEvent
        Try
            Dim oFormUID As String
            oFormUID = pVal.FormUID
            'Para pruebas


            'PARA FORMULARIOS NUEVOS
            Debug.Print(pVal.FormTypeEx & " _ " & pVal.FormUID)

            Select Case pVal.FormUID
                'Evento de creacion de formularios
                Case "SBOFormVTC"
                    SBOFormVTC.AdminSBOFormVTC(oApp, pVal, BubbleEvent)
                Case "SBOFormHisA"
                    SBOFormHisA.AdminSBOFormHisA(oApp, pVal, BubbleEvent)
                    'Aca agregar los casos que se requiera manejar los eventos dentro de formularios              
                Case "SBOFormEtpA"
                    SBOFormEtpA.AdminSBOFormEtpA(oApp, pVal, BubbleEvent)
                Case "SBOFormUsrA"
                    SBOFormUsrA.AdminSBOFormUsrA(oApp, pVal, BubbleEvent)
                    'Aca agregar los casos que se requiera manejar los eventos dentro de formularios              
                Case "SBOFormEtpA"
                    SBOFormEtpA.AdminSBOFormEtpA(oApp, pVal, BubbleEvent)
                Case "SBOFormModA"
                    SBOFormModA.AdminSBOFormModA(oApp, pVal, BubbleEvent)
                Case "UDO_F_LICRE" 'mauricio
                    SBOFormLineCredito.AdminSBOFormUDOLC(oApp, pVal, BubbleEvent)
                    ' Case "UDO_F_REFIN" 'mauricio
                    '    SBOFormRefin.AdminSBOFormUDORefin(oApp, pVal, BubbleEvent)
            End Select

            'Para formularios nativos que cambian de UID
            Select Case pVal.FormType

                Case "10011"
                    If pVal.Before_Action = False Then
                        If pVal.ItemUID = "2" Then
                            BubbleEvent = False
                        End If
                    Else
                        If pVal.ItemUID = "2" Then
                            oForm = oApp.Forms.Item(pVal.FormUID)
                            BubbleEvent = False
                            oForm.Close()
                        End If
                    End If
            End Select

            'Se toma por tipo de formulario
            Select Case pVal.FormTypeEx
                Case "139"
                    SBOFormOV.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "FormParamFinan"
                    FormParamFinan.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "UDO_FT_FIPLA"
                    SBOFormUDOFIPLA.AdminSBOFormUDOFIPLA(oApp, pVal, BubbleEvent)
                Case "UDO_FT_LICRE" 'mauricio
                    SBOFormLineCredito.AdminSBOFormUDOLC(oApp, pVal, BubbleEvent)
                Case "SBOFecLst"
                    SBOFormFechaLst.AdminSBOFormUDOFECLT(oApp, pVal, BubbleEvent)
                Case "FormGarLinea"
                    FormGarLinea.ItemEvent(oApp, pVal, BubbleEvent)
                Case "UDO_FT_AUTETAP"
                    SBOFormEtpA.AdminSBOFormEtpA(oApp, pVal, BubbleEvent)
                Case "UDO_FT_AUTCOND"
                    SBOFormConA.AdminSBOFormConA(oApp, pVal, BubbleEvent)
                Case "UDO_FT_AUTMOD"
                    SBOFormModA.AdminSBOFormModA(oApp, pVal, BubbleEvent)
                Case "SBOMotRec"
                    SBOFormMotRec.AdminSBOFormUDO(oApp, pVal, BubbleEvent)
                Case "UDO_FT_CUOTA"
                    SBOFormUDOCuotas.AdminSBOFormUDOCuotas(oApp, pVal, BubbleEvent)
                Case "170"
                    SBOFormPaRe.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "FormPaInt"
                    SBOFormCalculoInteres.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "UDO_FT_CUOTA"
                    SBOFormUDOCuotas.AdminSBOFormUDOCuotas(oApp, pVal, BubbleEvent)
                Case "134"
                    FormLiCre.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "133"
                    FormFactura.ItemEvent(oApp, FormUID, pVal, BubbleEvent)
                Case "SBOSeInt"
                    SBOSelectInteres.AdminSBOFormUDOFECLTSEIN(oApp, pVal, BubbleEvent)
                Case "UDO_FT_REFIN" 'mauricio
                    SBOFormRefin.AdminSBOFormUDORefin(oApp, pVal, BubbleEvent)
            End Select

        Catch ex As Exception
            oApp.MessageBox("ItemEvent Error:" & ex.Message)
        End Try
    End Sub

    Private Sub oApp_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles oApp.RightClickEvent
        Try
            If oApp.Forms.Item(eventInfo.FormUID).TypeEx = "UDO_FT_LICRE" Then
                SBOFormLineCredito.RightClickEvent(oApp, eventInfo, BubbleEvent)
            End If

        Catch ex As Exception
            oApp.MessageBox("RightClickEvent:" & ex.Message)

        End Try
    End Sub
    '---------------
#End Region

    Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

    Private Sub oApp_StatusbarEvent(ByVal Text As String, ByVal MessageType As SAPbouiCOM.BoStatusBarMessageType) Handles oApp.StatusBarEvent
        Try
            If Text.Contains("-7780") Then
                oApp.StatusBar.SetText("", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
            End If
        Catch ex As Exception
            oApp.MessageBox("StatusBarEvent:" & ex.Message)
        End Try
    End Sub

    Private Sub oApp_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles oApp.FormDataEvent
        Try

            Select Case BusinessObjectInfo.FormTypeEx

                Case "149"
                    FormOV.FormDataEvent(oApp, BusinessObjectInfo, BubbleEvent)

                Case "UDO_FT_LICRE"
                    SBOFormLineCredito.AdminSBOFormUDOEvent(oApp, BusinessObjectInfo, BubbleEvent)
                Case "UDO_FT_AUTETAP"
                    SBOFormEtpA.AdminSBOFormUDOEtpADatEve(oApp, BusinessObjectInfo, BubbleEvent)
                Case "UDO_FT_AUTMOD"
                    SBOFormModA.AdminSBOFormUDOModDatEve(oApp, BusinessObjectInfo, BubbleEvent)
                Case "UDO_FT_FIPLA"
                    SBOFormUDOFIPLA.AdminSBOFormUDOFIPLADatEve(oApp, BusinessObjectInfo, BubbleEvent)
                Case "UDO_FT_CUOTA"
                    SBOFormUDOCuotas.AdminSBOFormUDOCuotaEve(oApp, BusinessObjectInfo, BubbleEvent)
                Case "133"
                    FormFactura.FormDataEvent(oApp, BusinessObjectInfo, BubbleEvent)
                    'Case "UDO_FT_REFIN"
                    'SBOFormRefin.AdminSBOFormUDORefinEve(oApp, BusinessObjectInfo, BubbleEvent)

            End Select

        Catch ex As Exception
            oApp.MessageBox("FormDataEvent:" & ex.Message)
        End Try

    End Sub

    Private Sub oApp_LayoutEvent(ByRef layoutEventArgs As SAPbouiCOM.LayoutKeyInfo, ByRef BubbleEvent As Boolean) Handles oApp.LayoutKeyEvent
        Try
            Select Case layoutEventArgs.EventType
                Case SAPbouiCOM.BoEventTypes.et_PRINT_LAYOUT_KEY
                    If layoutEventArgs.BeforeAction = True Then
                        Dim oform As SAPbouiCOM.Form
                        oform = oApp.Forms.ActiveForm

                    End If

            End Select
        Catch ex As Exception
            oApp.MessageBox("LayoutEvent:" & ex.Message)
        End Try
    End Sub

    Private Sub oApp_UDOEvent(ByRef udoEventArgs As SAPbouiCOM.UDOEvent, ByRef BubbleEvent As Boolean) Handles oApp.UDOEvent
        Try

            Select Case udoEventArgs.UDOCode

            End Select

        Catch ex As Exception
            oApp.MessageBox("FormDataEvent:" & ex.Message)
        End Try
    End Sub
End Class


'//  SAP MANAGE DI API 6.7 SDK Sample
'//****************************************************************************
'//
'//  File:      frmInstall.vb
'//
'//  Copyright (c) SAP MANAGE
'//
'// INSTALACION DE ADD-ONS PARA SBO.
'//
'//****************************************************************************
'// This sample creates an add-on installer for SBO.
'// An installation for SBO should be build in a spesific way.
'// 1) It should be able to accept a command line parameter from SBO.
'//    This parameter is a string built from 2 strings devided by "|".
'//    The first string is the path recommended by SBO for installation folder.
'//    The second string is the location of "AddOnInstallAPI.dll".
'//    For example, a command line parameter that looks like this:
'//    "C:\MyAddon|C:\Program Files\SAP Manage\SAP Business One\AddOnInstallAPI.dll"
'//    Means that the recommended installation folder for this addon is "C:\MyAddon"
'//    and the location of "AddOnInstallAPI.dll" is - 
'//                 "C:\Program Files\SAP Manage\SAP Business One\AddOnInstallAPI.dll"
'// 2) When the installation is complete the installer must call the function 
'//    "EndInstall" from "AddOnInstallAPI.dll" to inform SBO the installation is complete.
'//    This dll contains 3 functions that can be used during the installation.
'//    The functions are: 
'//         1) EndInstall - Signals SBO that the installation is complete.
'//         2) SetAddOnFolder - Use it if you want to change the installation folder.
'//         3) RestartNeeded - Use it if your installation requires a restart, it will cause
'//            the SBO application to close itself after the installation is complete.
'//    All 3 functions return a 32 bit integer. There are 2 possible values for this integer.
'//    0 - Success, 1 - Failure.
'// 3) The installer must be one executable file.
'// 4) After your installer is ready you need to create an add-on registration file.
'//    In order to create it you have a utility - "Add-On Registration Data Creator"
'//    you can find it in -
'//       "..\SAP Manage\SAP Business One SDK\Tools\AddOnRegDataGen\AddOnRegDataGen.exe".
'//    This utility creates a file with the extention 'ard', you will be asked to 
'//    point to this file when you register your addon.

Imports System
Imports System.Runtime.InteropServices
Imports Microsoft.Win32

Public Class frmInstall
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "
    Public Es_64_bits As Boolean
 

    Public Sub New()
        MyBase.New()
        Es_64_bits = False

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblHeadLine As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDest As System.Windows.Forms.TextBox
    Friend WithEvents chkRestart As System.Windows.Forms.CheckBox
    Friend WithEvents chkDefaultFolder As System.Windows.Forms.CheckBox
    Friend WithEvents cmdInstall As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents FileWatcher As System.IO.FileSystemWatcher
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cmdInstall = New System.Windows.Forms.Button()
        Me.lblHeadLine = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDest = New System.Windows.Forms.TextBox()
        Me.chkRestart = New System.Windows.Forms.CheckBox()
        Me.chkDefaultFolder = New System.Windows.Forms.CheckBox()
        Me.FileWatcher = New System.IO.FileSystemWatcher()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdInstall
        '
        Me.cmdInstall.Location = New System.Drawing.Point(291, 203)
        Me.cmdInstall.Name = "cmdInstall"
        Me.cmdInstall.Size = New System.Drawing.Size(96, 32)
        Me.cmdInstall.TabIndex = 1
        Me.cmdInstall.Text = "Install Add-on"
        '
        'lblHeadLine
        '
        Me.lblHeadLine.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblHeadLine.Location = New System.Drawing.Point(16, 16)
        Me.lblHeadLine.Name = "lblHeadLine"
        Me.lblHeadLine.Size = New System.Drawing.Size(488, 24)
        Me.lblHeadLine.TabIndex = 2
        Me.lblHeadLine.Text = "This Installer is a sample for Sap Business One. "
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(488, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "It will install a "" & sAddonName & "" add-on"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(456, 23)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Installation Folder recieved from SBO application"
        '
        'txtDest
        '
        Me.txtDest.Enabled = False
        Me.txtDest.Location = New System.Drawing.Point(24, 96)
        Me.txtDest.Name = "txtDest"
        Me.txtDest.Size = New System.Drawing.Size(472, 20)
        Me.txtDest.TabIndex = 5
        '
        'chkRestart
        '
        Me.chkRestart.Location = New System.Drawing.Point(24, 154)
        Me.chkRestart.Name = "chkRestart"
        Me.chkRestart.Size = New System.Drawing.Size(104, 17)
        Me.chkRestart.TabIndex = 6
        Me.chkRestart.Text = "Ask for a restart"
        '
        'chkDefaultFolder
        '
        Me.chkDefaultFolder.Checked = True
        Me.chkDefaultFolder.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDefaultFolder.Location = New System.Drawing.Point(24, 128)
        Me.chkDefaultFolder.Name = "chkDefaultFolder"
        Me.chkDefaultFolder.Size = New System.Drawing.Size(240, 24)
        Me.chkDefaultFolder.TabIndex = 7
        Me.chkDefaultFolder.Text = "Use path supplied by SBO"
        '
        'FileWatcher
        '
        Me.FileWatcher.EnableRaisingEvents = True
        Me.FileWatcher.SynchronizingObject = Me
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(402, 203)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 32)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Cancelar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = Global.AddOnInstaller.My.Resources.Resources.logo_exxis
        Me.PictureBox1.Location = New System.Drawing.Point(291, 120)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(205, 77)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'frmInstall
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(510, 247)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.chkDefaultFolder)
        Me.Controls.Add(Me.chkRestart)
        Me.Controls.Add(Me.txtDest)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblHeadLine)
        Me.Controls.Add(Me.cmdInstall)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInstall"
        Me.Text = "Instalador de Addon"
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Data members"
    Private sAddonName As String = "CarDealerTalleres"
    Private sInstallName As String = "AddOnInstaller.exe"
    Private strDll As String ' The path of "AddOnInstallAPI.dll"
    Private strDest As String ' Installation target path
    Private bFileCreated As Boolean ' True if the file was created
#End Region

#Region "Declarations"
    ' Declaring the functions inside "AddOnInstallAPI.dll"

    'EndInstall - Signals SBO that the installation is complete.

    'Declare Function EndInstall Lib "AddOnInstallAPI_x64.dll" () As Int32
    Declare Function EndInstall Lib "AddOnInstallAPI.dll" () As Int32

    'Declare Function EndInstallEx Lib "AddOnInstallAPI_x64.dll" (ByVal str As String, ByVal b As Boolean) As Int32
    Declare Function EndInstallEx Lib "AddOnInstallAPI.dll" (ByVal str As String, ByVal b As Boolean) As Int32

    'SetAddOnFolder - Use it if you want to change the installation folder.
    'Declare Function SetAddOnFolder Lib "AddOnInstallAPI_x64.dll" (ByVal srrPath As String) As Int32
    Declare Function SetAddOnFolder Lib "AddOnInstallAPI.dll" (ByVal srrPath As String) As Int32

    'RestartNeeded - Use it if your installation requires a restart, it will cause
    'the SBO application to close itself after the installation is complete.
    'Declare Function RestartNeeded Lib "AddOnInstallAPI_x64.dll" () As Int32
    Declare Function RestartNeeded Lib "AddOnInstallAPI.dll" () As Int32

    'Declare Function EndUninstall Lib "AddOnInstallAPI_x64.dll" (ByVal str As String, ByVal b As Boolean) As Int32
    Declare Function EndUninstall Lib "AddOnInstallAPI.dll" (ByVal str As String, ByVal b As Boolean) As Int32


#End Region

#Region "Methods"

    ' Read the addon path from the registry
    Public Function ReadPath() As String
        Dim sAns As String
        Dim sErr As String = ""

        sAns = RegValue(RegistryHive.LocalMachine, "SOFTWARE", sAddonName, sErr)
        ReadPath = sAns
        If Not (sAns <> "") Then
            MessageBox.Show("This error occurred: " & sErr)
        End If
    End Function

    ' This Function reads values to the registry
    Public Function RegValue(ByVal Hive As RegistryHive, _
          ByVal Key As String, ByVal ValueName As String, _
          Optional ByRef ErrInfo As String = "") As String

        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String
        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.DynData
                objParent = Registry.DynData
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users
        End Select

        Try
            objSubkey = objParent.OpenSubKey(Key)
            'if can't be found, object is not initialized
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If

        Catch ex As Exception
            ErrInfo = ex.Message
        Finally

            'if no error but value is empty, populate errinfo
            If ErrInfo = "" And sAns = "" Then
                ErrInfo = _
                   "No value found for requested registry key"
            End If
        End Try
        Return sAns
    End Function

    ' This Function writes values to the registry
    Public Function WriteToRegistry(ByVal _
    ParentKeyHive As RegistryHive, _
    ByVal SubKeyName As String, _
    ByVal ValueName As String, _
    ByVal Value As Object) As Boolean

        Dim objSubKey As RegistryKey
        Dim sException As String
        Dim objParentKey As RegistryKey
        Dim bAns As Boolean

        Try
            Select Case ParentKeyHive
                Case RegistryHive.ClassesRoot
                    objParentKey = Registry.ClassesRoot
                Case RegistryHive.CurrentConfig
                    objParentKey = Registry.CurrentConfig
                Case RegistryHive.CurrentUser
                    objParentKey = Registry.CurrentUser
                Case RegistryHive.DynData
                    objParentKey = Registry.DynData
                Case RegistryHive.LocalMachine
                    objParentKey = Registry.LocalMachine
                Case RegistryHive.PerformanceData
                    objParentKey = Registry.PerformanceData
                Case RegistryHive.Users
                    objParentKey = Registry.Users
            End Select

            'Open 
            objSubKey = objParentKey.OpenSubKey(SubKeyName, True)
            'create if doesn't exist
            If objSubKey Is Nothing Then
                objSubKey = objParentKey.CreateSubKey(SubKeyName)
            End If


            objSubKey.SetValue(ValueName, Value)
            bAns = True
        Catch ex As Exception
            bAns = False

        End Try

        Return True

    End Function

    ' This function extracts the given add-on into the path specified
    Private Sub ExtractFile(ByVal path As String)
        Dim tabla_dll(71) As String
        tabla_dll(0) = "BFsForm.srf"
        tabla_dll(1) = "CarDealer.b1s"
        tabla_dll(2) = "CarDealer.exe"
        tabla_dll(3) = "CarDealer.exe.config"
        tabla_dll(4) = "CarDealer.pdb"
        tabla_dll(5) = "cardealer.png"
        tabla_dll(6) = "CarDealer.xml"
        tabla_dll(7) = "CFL.jpg"
        tabla_dll(8) = "ChooseField.srf"
        tabla_dll(9) = "CNTT Actividad.srf"
        tabla_dll(10) = "CNTT Campos.srf"
        tabla_dll(11) = "CNTT Patron.srf"
        tabla_dll(12) = "CNTT.srf"
        tabla_dll(13) = "CNTTCombos.srf"
        tabla_dll(14) = "Descendente.srf"
        tabla_dll(15) = "Descendente2.srf"
        tabla_dll(16) = "Exxis.AddOnCulture.dll"
        tabla_dll(17) = "exxis.bmp"
        tabla_dll(18) = "Exxis_English (United States).xml"
        tabla_dll(19) = "Exxis_Portuguese (Brazil).xml"
        tabla_dll(20) = "Exxis_Spanish (Latin America).xml"
        tabla_dll(21) = "IVEHMatrix.xml"
        tabla_dll(22) = "LogoBoliviaSAP.bmp"
        tabla_dll(23) = "LSTCombos.xml"
        tabla_dll(24) = "MenuCarDealer.xml"
        tabla_dll(25) = "ParamCNTT.srf"
        tabla_dll(26) = "ParamVenta.srf"
        tabla_dll(27) = "SBOFormIns.srf"
        tabla_dll(28) = "SBOFormLst.srf"
        tabla_dll(29) = "SBOFormReva.srf"
        tabla_dll(30) = "SBOFormVTC.srf"
        tabla_dll(31) = "UDO_F_AUTO.xml"
        tabla_dll(32) = "UDO_F_FAMI.xml"
        tabla_dll(33) = "UDO_F_IVEH.xml"
        tabla_dll(34) = "UDO_F_VEHC.xml"
        tabla_dll(35) = "VEHCCombos.xml"
        tabla_dll(36) = "CreaUsuarioAddon.exe"
        tabla_dll(37) = "SBOFormStock.srf"
        tabla_dll(38) = "StockCombos.xml"
        tabla_dll(39) = "SBOFormAuto.srf"
        tabla_dll(40) = "SBOFormAutoCombos.srf"
        tabla_dll(41) = "SBOFormAutoCombosUpdate.srf"
        tabla_dll(42) = "SBOFormUpdate.srf"
        tabla_dll(43) = "SBOFormAjusteTC.srf"
        tabla_dll(44) = "SBOFormAjusteTCPRE.srf"
        tabla_dll(45) = "SBOFormAjusteTCView.srf"
        tabla_dll(46) = "Relacionados.srf"
        tabla_dll(47) = "Test.xml"
        tabla_dll(48) = "SBOFormIntR.srf"
        tabla_dll(49) = "SBOFormIntCon.srf"
        tabla_dll(50) = "SBOFormImpV.srf"
        tabla_dll(51) = "SBOFormExpV.srf"
        tabla_dll(52) = "SBOFormConfInt.srf"
        tabla_dll(53) = "Test_xml.exx"
        tabla_dll(54) = "CarDealerIntegration.exe"
        tabla_dll(55) = "SBOFormPer.srf"
        tabla_dll(56) = "SBOFormPM.srf"
        tabla_dll(57) = "EXXASISENT.srf"
        tabla_dll(58) = "SBOFormPEnt.srf"
        tabla_dll(59) = "LinkDoc.srf"
        tabla_dll(60) = "SBOFormORDT.srf"
        tabla_dll(61) = "FormCotizacionDocRel.srf"
        tabla_dll(62) = "SBOFormTallerUDO.srf"
        tabla_dll(63) = "UDO_F_EXX_ASIT.xml"
        tabla_dll(64) = "FormRegistroTrabajo.srf"
        tabla_dll(65) = "SBOFormParamLT.srf"
        tabla_dll(66) = "EXXCUOTA.srf"
        tabla_dll(67) = "sys_loading.srf"
        tabla_dll(68) = "loading.gif"
        tabla_dll(69) = "SBOFormUbi.srf"
        tabla_dll(70) = "reloj.jpg"



        Dim AddonExeFile As IO.FileStream
        Dim thisExe As System.Reflection.Assembly
        Dim sTargetPath, sSourcePath, var_aux As String
        Dim largo As Integer
        Dim i, k As Integer
        Dim file As System.IO.Stream
        Dim buffer() As Byte
        thisExe = System.Reflection.Assembly.GetExecutingAssembly()

        'SI SE AGREGA UN FORMULARIO O EXEC CAMBIAR EN NUMERO
        For i = 0 To 70
            file = Nothing

            sTargetPath = path & "/" & tabla_dll(i)

            largo = Len(sTargetPath)
            k = (largo + 1) - 4
            var_aux = Mid(sTargetPath, k, 4)
            sSourcePath = Replace(sTargetPath, var_aux, ".tmp")
            file = thisExe.GetManifestResourceStream("AddOnInstaller." & tabla_dll(i))
            If IO.File.Exists(sSourcePath) Then
                IO.File.Delete(sSourcePath)
            End If
            AddonExeFile = IO.File.Create(sSourcePath)
            ReDim buffer(file.Length)
            file.Read(buffer, 0, file.Length)
            AddonExeFile.Write(buffer, 0, file.Length)
            AddonExeFile.Close()

            If IO.File.Exists(sTargetPath) Then
                IO.File.Delete(sTargetPath)
            End If
            ' Change file extension to exe
            IO.File.Move(sSourcePath, sTargetPath)
        Next

        sTargetPath = path
        ''****************************** 
        Shell(sTargetPath & "\CreaUsuarioAddon.exe", AppWinStyle.NormalFocus, True)
        ''****************************** 
        'Shell(sTargetPath & "\sqlxml_x64.msi", AppWinStyle.NormalFocus, True)
    End Sub

    ' This procedure deletes the addon files
    Private Sub UnInstall()
        Dim path As String
        path = ReadPath() ' Reads the addon path from the registry
        If path <> "" Then
            ''-------------------------------------------------------
            'Try
            '    ' Delete the addon EXE file
            '    If IO.File.Exists(path & "\" & sAddonName & ".exe") Then
            '        IO.File.Delete(path & "\" & sAddonName & ".exe")
            '        MessageBox.Show(path & "\" & sAddonName & ".exe was deleted")
            '    Else
            '        MessageBox.Show(path & "\" & sAddonName & ".exe was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------
            'Try
            '    ' Delete the dll1 file
            '    If IO.File.Exists(path & "\Interop.SAPbobsCOM.dll") Then
            '        IO.File.Delete(path & "\Interop.SAPbobsCOM.dll")
            '        MessageBox.Show(path & "\Interop.SAPbobsCOM.dll was deleted")
            '    Else
            '        MessageBox.Show(path & "\Interop.SAPbobsCOM.dll was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------
            'Try
            '    ' Delete the dll2 file
            '    If IO.File.Exists(path & "\Interop.SAPbouiCOM.dll") Then
            '        IO.File.Delete(path & "\Interop.SAPbouiCOM.dll")
            '        MessageBox.Show(path & "\Interop.SAPbouiCOM.dll was deleted")
            '    Else
            '        MessageBox.Show(path & "\Interop.SAPbouiCOM.dll was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------
            'Try
            '    ' Delete the dll3 file
            '    If IO.File.Exists(path & "\Interop.Scripting.dll") Then
            '        IO.File.Delete(path & "\Interop.Scripting.dll")
            '        MessageBox.Show(path & "\Interop.Scripting.dll was deleted")
            '    Else
            '        MessageBox.Show(path & "\Interop.Scripting.dll was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------
            'Try
            '    ' Delete the screen form file
            '    If IO.File.Exists(path & "\SetupImpresionele.exe") Then
            '        IO.File.Delete(path & "\SetupImpresionele.exe")
            '        MessageBox.Show(path & "\SetupImpresionele.exe was deleted")
            '    Else
            '        MessageBox.Show(path & "\SetupImpresionele.exe was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------
            'Try
            '    ' Delete the screen form file
            '    If IO.File.Exists(path & "\CreaUsuarioAddon.exe") Then
            '        IO.File.Delete(path & "\CreaUsuarioAddon.exe")
            '        MessageBox.Show(path & "\CreaUsuarioAddon.exe was deleted")
            '    Else
            '        MessageBox.Show(path & "\CreaUsuarioAddon.exe was not found")
            '    End If
            'Catch
            '    MessageBox.Show(path & " - ERROR UNINSTALLING")
            'End Try
            ''-------------------------------------------------------

            Dim DirFullInfo As New System.IO.DirectoryInfo(path)
            Dim sfs As System.IO.FileInfo()
            Dim sf As System.IO.FileInfo
            Dim i As Integer
            Dim flag As Boolean = False
            sfs = DirFullInfo.GetFiles
            For i = 0 To sfs.Length - 1
                sf = sfs.GetValue(i)
                If sf.Name <> sInstallName Then
                    Try
                        ' Delete the screen form file
                        If IO.File.Exists(path & "\" & sf.Name) And sf.Extension.Trim.ToUpper <> ".DAT" Then
                            IO.File.Delete(path & "\" & sf.Name)
                            ' MessageBox.Show(path & "\" & sf.Name & " was deleted")
                        ElseIf sf.Extension.Trim.ToUpper <> ".DAT" Then
                            If flag = False Then
                                MessageBox.Show(path & "\" & sf.Name & " was not found")
                                flag = True
                            End If
                        End If
                    Catch
                        If flag = False Then
                            MessageBox.Show(path & "\" & sf.Name & " - Es necesario realizar la instalación como administrador")
                            flag = True
                        End If
                    End Try
                End If
            Next

            'If IO.Directory.Exists(path & "\xml") Then
            '    IO.Directory.Delete(path & "\xml", True)
            'End If

            'If IO.Directory.Exists(path & "\en") Then
            '    IO.Directory.Delete(path & "\en", True)
            'End If

            'If IO.Directory.Exists(path & "\es") Then
            '    IO.Directory.Delete(path & "\es", True)
            'End If


            MessageBox.Show("( " & Format(i, "###0") & " )" & "  Additional Files Deleted from" & vbCrLf & path)
            '-------------------------------------------------------
            EndUninstall("", True)
        Else
            MessageBox.Show("Path not found")
        End If
        ' Terminate the application
        'GC.Collect()
        End
    End Sub

    ' This procedure copies the addon exe file to the installation folder        
    Private Function Install() As Boolean
        Dim resp As Boolean = True
        Try
            Environment.CurrentDirectory = strDll ' For Dll function calls will work

            If chkDefaultFolder.Checked = False Then ' Change the installation folder

                SetAddOnFolder(txtDest.Text)

                strDest = txtDest.Text
            End If

            If Not (IO.Directory.Exists(strDest)) Then
                IO.Directory.CreateDirectory(strDest) ' Create installation folder
            End If

            'If Not (IO.Directory.Exists(strDest & "\en")) Then
            '    IO.Directory.CreateDirectory(strDest & "\en") ' Create installation folder
            'End If

            'If Not (IO.Directory.Exists(strDest & "\es")) Then
            '    IO.Directory.CreateDirectory(strDest & "\es") ' Create installation folder
            'End If

            'If Not (IO.Directory.Exists(strDest & "\xml")) Then
            '    IO.Directory.CreateDirectory(strDest & "\xml") ' Create installation folder
            'End If

            FileWatcher.Path = strDest
            FileWatcher.EnableRaisingEvents = True

            ExtractFile(strDest) ' Extract add-on to installation folder

            While bFileCreated = False
                Application.DoEvents()
                'Don't continue running until the file is copied...
            End While

            If chkRestart.Checked Then
                RestartNeeded() ' Inform SBO the restart is needed
            End If

            EndInstallEx("", True)
            'EndInstall() ' Inform SBO the installation ended


            'Write installation Folder to registry
            Dim bAns As Boolean

            'WriteToRegistry(RegistryHive.LocalMachine, "SOFTWARE", "path", "c:\folder")
            bAns = WriteToRegistry(RegistryHive.LocalMachine, "SOFTWARE", sAddonName, strDest)
            MessageBox.Show("Finished Installing", "Installation ended", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Windows.Forms.Application.Exit() ' Exit the installer
            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Addon Installer")
            Return False
        End Try
    End Function

#End Region

#Region "Events"
    Private Sub frmInstall_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.lblHeadLine.Text = "Instalador de AddOn para Sap Business One"
            Me.Label1.Text = "Usted se dispone a instalar el AddOn: " & sAddonName
            Me.Label2.Text = "Carpeta de Instalación de AddOn en SBO"
            Me.chkDefaultFolder.Text = "Usar carpeta entregada por SBO"
            Me.chkRestart.Text = "Reiniciar"
            Me.cmdInstall.Text = "Instalar"
            'Dim strAppPath As String
            ' The command line parameters, seperated by '|' will be broken to this array
            Dim strCmdLineElements(2) As String
            Dim strCmdLine As String ' The whole command line
            Dim NumOfParams As Integer 'The number of parameters in the command line (should be 2)
            NumOfParams = Environment.GetCommandLineArgs.Length

            If NumOfParams = 2 Then
                strCmdLine = Environment.GetCommandLineArgs.GetValue(1)
                If strCmdLine.ToUpper = "/U" Then
                    UnInstall()
                End If
                strCmdLineElements = strCmdLine.Split("|")

                ' Get Install destination Folder
                strDest = strCmdLineElements.GetValue(0)


                ' Get the "AddOnInstallAPI.dll" path
                strDll = strCmdLineElements.GetValue(1)


                If Es_64_bits = True Then
                    strDll = strDll.Replace("AddOnInstallAPI", "")
                    strDll = strDll.Replace("AddOnInstallAPI_x64.dll", "")
                    strDll = strDll.Replace(".dll", "")
                    strDll = strDll.Replace("AddOnInstallAPI_x64", "")
                    strDll = strDll.Replace(" (x86)", "")
                    strDll = strDll.Replace("(x86)", "")

                    strDest = strDest.Replace(" (x86)", "")
                    strDest = strDest.Replace("(x86)", "")
                Else
                    strDll = strDll.Replace("AddOnInstallAPI.dll", "")
                    strDll = strDll.Replace(".dll", "")
                    strDll = strDll.Replace("AddOnInstallAPI", "")
                    strDll = strDll.Replace("_x64", "")
                End If
                txtDest.Text = strDest
            Else
                MessageBox.Show("This installer must be run from Sap Business One", _
                                "Incorrect installation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Windows.Forms.Application.Exit()
            End If
        Catch ex As Exception
            ShowError(ex)
        End Try
    End Sub

    Private Sub cmdInstall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInstall.Click

        If Install() = True Then

        End If

    End Sub

    Private Sub Create_SQL_User()
        Shell(Application.StartupPath & "\CreaUsuarioAddon.exe", AppWinStyle.MaximizedFocus)
    End Sub

    Private Sub chkDefaultFolder_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDefaultFolder.CheckedChanged
        txtDest.Enabled = Not (chkDefaultFolder.Checked)
    End Sub

    ' This event happens when the addon exe file is renamed to exe extention
    Private Sub FileWatcher_Renamed(ByVal sender As Object, ByVal e As System.IO.RenamedEventArgs) Handles FileWatcher.Renamed
        bFileCreated = True
        FileWatcher.EnableRaisingEvents = False
    End Sub

    Public Sub ShowError(ByVal ex As Exception)
        MsgBox(ex.Message & vbNewLine & "Source:" & ex.StackTrace, MsgBoxStyle.Information, "Addon Installer")
    End Sub
#End Region



    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        EndInstallEx("", False) ' Inform SBO the installation was not complete
        MessageBox.Show("Installation was canceled", "Installation was canceled", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Windows.Forms.Application.Exit() ' Exit the installer
    End Sub

    Private Sub frmInstall_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown

    End Sub

    Private Sub lblHeadLine_Click(sender As System.Object, e As System.EventArgs) Handles lblHeadLine.Click

    End Sub
End Class


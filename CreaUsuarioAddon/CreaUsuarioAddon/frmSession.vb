Public Class frmSession
    Inherits System.Windows.Forms.Form
    Dim Usuario As String = "AddOnUserChile"
    Dim password As String = "AddOnUserChile3x481"

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtserver As System.Windows.Forms.TextBox
    Friend WithEvents chkConfienza As System.Windows.Forms.CheckBox
    Friend WithEvents txtlogin As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtpasswd As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmbBase As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtserver = New System.Windows.Forms.TextBox()
        Me.chkConfienza = New System.Windows.Forms.CheckBox()
        Me.txtlogin = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtpasswd = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmbBase = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 20)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "Nombre Servidor"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtserver
        '
        Me.txtserver.Location = New System.Drawing.Point(129, 11)
        Me.txtserver.Name = "txtserver"
        Me.txtserver.Size = New System.Drawing.Size(128, 20)
        Me.txtserver.TabIndex = 0
        Me.txtserver.Text = "SERVERNAME"
        '
        'chkConfienza
        '
        Me.chkConfienza.Enabled = False
        Me.chkConfienza.Location = New System.Drawing.Point(16, 59)
        Me.chkConfienza.Name = "chkConfienza"
        Me.chkConfienza.Size = New System.Drawing.Size(166, 24)
        Me.chkConfienza.TabIndex = 106
        Me.chkConfienza.Text = "Usar conexi�n de confianza"
        '
        'txtlogin
        '
        Me.txtlogin.Location = New System.Drawing.Point(129, 88)
        Me.txtlogin.Name = "txtlogin"
        Me.txtlogin.Size = New System.Drawing.Size(128, 20)
        Me.txtlogin.TabIndex = 30
        Me.txtlogin.Text = "sa"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 23)
        Me.Label2.TabIndex = 104
        Me.Label2.Text = "Id de inicio de sesi�n"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtpasswd
        '
        Me.txtpasswd.Location = New System.Drawing.Point(129, 112)
        Me.txtpasswd.Name = "txtpasswd"
        Me.txtpasswd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtpasswd.Size = New System.Drawing.Size(128, 20)
        Me.txtpasswd.TabIndex = 40
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 23)
        Me.Label3.TabIndex = 105
        Me.Label3.Text = "Contrase�a"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdAceptar
        '
        Me.cmdAceptar.CausesValidation = False
        Me.cmdAceptar.Location = New System.Drawing.Point(268, 10)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(72, 23)
        Me.cmdAceptar.TabIndex = 99
        Me.cmdAceptar.TabStop = False
        Me.cmdAceptar.Text = "Aceptar"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.CausesValidation = False
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Location = New System.Drawing.Point(268, 34)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancelar.TabIndex = 98
        Me.cmdCancelar.TabStop = False
        Me.cmdCancelar.Text = "Cancelar"
        '
        'cmbBase
        '
        Me.cmbBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBase.Items.AddRange(New Object() {"SQL-Server-2000", "SQL-Server-2005", "SQL-Server-2008", "SQL-Server-2012"})
        Me.cmbBase.Location = New System.Drawing.Point(129, 36)
        Me.cmbBase.Name = "cmbBase"
        Me.cmbBase.Size = New System.Drawing.Size(128, 21)
        Me.cmbBase.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 20)
        Me.Label4.TabIndex = 101
        Me.Label4.Text = "Tipo Base Dato"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSession
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(346, 144)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmbBase)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtpasswd)
        Me.Controls.Add(Me.txtlogin)
        Me.Controls.Add(Me.txtserver)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.chkConfienza)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSession"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inicio de Sesi�n para SQL Server"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim cmd As New SqlClient.SqlCommand
        Dim cnn As SqlClient.SqlConnection
        Try
            cnn = New SqlClient.SqlConnection("Data Source=" & Me.txtserver.Text.Trim & ";Initial Catalog=master;User Id=" & Me.txtlogin.Text.Trim & ";Password=" & Me.txtpasswd.Text.Trim & " ")
            cnn.Open()
        Catch ex As Exception
            MessageBox.Show("No se pudo establecer conexi�n con el servidor [" & Me.txtserver.Text & "]" & vbCrLf & ex.ToString)
            Exit Sub
        End Try
        cmd.Connection = cnn
        cmd.CommandType = CommandType.Text
        Try
            Dim existe As Integer
            cmd.CommandText = "select COUNT(name) as Existe from syslogins where name = '" & Usuario & "'"
            existe = cmd.ExecuteScalar
            If existe = 1 Then
                Throw New Exception("existe")
            End If
        Catch ex As Exception
            Select Case ex.Message
                Case "existe"
                    MessageBox.Show("Usuario Addon ya Existe. Presione OK para Continuar" & vbCrLf & vbCrLf & "Se Verificar�n los Permisos del Usuario 'AddOnUserChile'", "Usuario AddOn", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case Else
                    MessageBox.Show("No hubo acceso a syslogins para Verificar Usuario Addon", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Select
        End Try

        Try
            cmd.CommandText = "select COUNT(name) as Valor from sysdatabases where name = 'SBO-COMMON'"
            Dim valor As Integer = cmd.ExecuteScalar
            If valor = 0 Then
                MessageBox.Show("No existe Catalogo B1 SBO-COMMON en Servidor [" & Me.txtserver.Text.Trim & "]")
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo accesar Cat�logo de B1 en Servidor [" & Me.txtserver.Text.Trim & "]" & vbCrLf & vbCrLf & ex.message)
            Exit Sub
        End Try
        Dim Str As String = "Select * from [SBO-COMMON].[dbo].SRGC WHERE dbuser = 'dbo'"
        Dim dapTable As New SqlClient.SqlDataAdapter(Str, cnn)
        Dim dtt As New DataTable
        Try
            dapTable.Fill(dtt)
        Catch ex As Exception
            MessageBox.Show("No se pudo Rescatar Listado de Compa��as de B1 en Servidor [" & Me.txtserver.Text.Trim & "]" & vbCrLf & vbCrLf & ex.message)
            Exit Sub
        End Try
        If dtt.Rows.Count = 0 Then
            MessageBox.Show("No se Existen Compa��as B1 en Cat�logo SBO-COMMON del Servidor [" & Me.txtserver.Text.Trim & "]")
            Exit Sub
        End If
        Dim resp As DialogResult
        Try
            Select Case Me.cmbBase.SelectedItem
                Case "SQL-Server-2000"
                    Crear_Usuario_2000(cmd, dtt, cnn)
                Case "SQL-Server-2005"
                    Crear_Usuario_2005(cmd, dtt, cnn)
                Case "SQL-Server-2008", "SQL-Server-2012"
                    Crear_Usuario_2005(cmd, dtt, cnn)
            End Select
        Catch EX As SqlClient.SqlException
            Select Case EX.NUMBER
                Case 15025
                    MessageBox.Show("Usuario Addon ya existe. Utilice Administrador Corporativo" & vbCrLf & "para asignar los siguientes permisos : " & vbCrLf & vbCrLf & "[master]=db_datareader" & vbCrLf & "[SBO-COMMON]=db_datareader" & vbCrLf & "[SBO-DATA'S]=db_owner (todas)", "Permisos a Asignar", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Case Else
                    MessageBox.Show("No se pudo crear usuario de Addon en SQL Server en  Servidor [" & Me.txtserver.Text.Trim & "]" & vbCrLf & vbCrLf & ex.message & vbCrLf & "Error : " & ex.Number)
            End Select
        Catch ex As Exception
            MessageBox.Show("No se pudo crear usuario de Addon en SQL Server en  Servidor " & Me.txtserver.Text.Trim & vbCrLf & vbCrLf & ex.message)
            Exit Sub
        End Try
        Try
            Select Case Me.cmbBase.SelectedItem
                Case "SQL-Server-2000"
                    Me.Crear_Base_De_Datos_Exx_Prods_2000(cmd, cnn)
                Case "SQL-Server-2005"
                    Me.Crear_Base_De_Datos_Exx_Prods_2005(cmd, cnn)
                Case "SQL-Server-2008", "SQL-Server-2012"
                    Me.Crear_Base_De_Datos_Exx_Prods_2005(cmd, cnn)
            End Select
        Catch ex As Exception
            MessageBox.Show("No se pudo crear la base de datos FlujoSAP en SQL Server en  Servidor " & Me.txtserver.Text.Trim & vbCrLf & vbCrLf & ex.message)
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
        MessageBox.Show("Instalaci�n de Usuario Addon y Base Datos Finalizada")
        cnn.Close()
        Me.Close()
    End Sub

#Region "RUTINAS CREAR BASE DE DATOS DE FLUJO"

    'Private Sub Crear_Base_De_Datos_Exx_Prods_2000(ByVal cmd As SqlClient.SqlCommand, ByVal cnn As SqlClient.SqlConnection)
    '    Dim drr As SqlClient.SqlDataReader
    '    Try
    '        cmd.CommandText = "use [master]; Select name from sysdatabases where name = 'Exx_Prods'"
    '        drr = cmd.ExecuteReader
    '        If drr.Read = False Then
    '            drr.Close()
    '            cmd.CommandText = "use [master]; CREATE DATABASE [Exx_Prods]"
    '            cmd.ExecuteNonQuery()
    '            cmd.CommandText = "use [Exx_Prods]; if not exists (select * from dbo.sysusers where name = N'AddOnUserChile') EXEC sp_grantdbaccess N'AddOnUserChile', N'AddOnUserChile' ; exec sp_addrolemember N'db_owner', N'AddOnUserChile'"
    '            cmd.ExecuteNonQuery()
    '            cmd.CommandText = "CREATE TABLE [dbo].[ACCESOS_FLUJO] ([DATA_BASE] [nvarchar](100) NOT NULL,	[USER_CODE] [nvarchar](8) NOT NULL,	[U_NAME] [nvarchar](30) NULL,	[USERID] [smallint] NULL,	[AccesoTotal] [nchar](1) NULL,	[chkAP] [nchar](1) NULL,	[chkAR] [nchar](1) NULL,	[chkFL] [nchar](1) NULL,	[chkModBanco] [nchar](1) NULL,	[chkLoadBanco] [nchar](1) NULL,	[chkLoadAP] [nchar](1) NULL,	[chkLoadAR] [nchar](1) NULL,	[chkLoadOT] [nchar](1) NULL,	[chkExcluir] [nchar](1) NULL,	[chkAplazar] [nchar](1) NULL,	[chkVerifica] [nchar](1) NULL,	[chkConfig] [nchar](1) NULL,	[chkConceptos] [nchar](1) NULL,	[chkPermisos] [nchar](1) NULL, CONSTRAINT [PK_ACCESOS] PRIMARY KEY CLUSTERED (	[DATA_BASE] ASC,[USER_CODE] ASC )ON [PRIMARY]) ON [PRIMARY]"
    '            cmd.ExecuteNonQuery()
    '        Else
    '            MessageBox.Show("Base de Datos Exx_Prods ya existe", "En Servidor", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            drr.Close()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Error al Tratar de Base de Datos Exx_Prods" & vbCrLf & ex.Message)
    '    End Try
    'End Sub

    'Private Sub Crear_Base_De_Datos_Exx_Prods_2005(ByVal cmd As SqlClient.SqlCommand, ByVal cnn As SqlClient.SqlConnection)
    '    Dim drr As SqlClient.SqlDataReader
    '    Try
    '        cmd.CommandText = "Select name from sys.databases where name = 'Exx_Prods'"
    '        drr = cmd.ExecuteReader
    '        If drr.Read = False Then
    '            drr.Close()
    '            cmd.CommandText = "use [master]; CREATE DATABASE [Exx_Prods]"
    '            cmd.ExecuteNonQuery()
    '            cmd.CommandText = "use [Exx_Prods]; if not exists (select * from dbo.sysusers where name = N'AddOnUserChile') EXEC sp_grantdbaccess N'AddOnUserChile', N'AddOnUserChile' ; exec sp_addrolemember N'db_owner', N'AddOnUserChile'"
    '            cmd.ExecuteNonQuery()
    '            cmd.CommandText = "CREATE TABLE [dbo].[ACCESOS_FLUJO](	[DATA_BASE] [nvarchar](100) NOT NULL,	[USER_CODE] [nvarchar](8) NOT NULL,	[U_NAME] [nvarchar](30) NULL,	[USERID] [smallint] NULL,	[AccesoTotal] [nchar](1) NULL,	[chkAP] [nchar](1) NULL,	[chkAR] [nchar](1) NULL,	[chkFL] [nchar](1) NULL,	[chkModBanco] [nchar](1) NULL,	[chkLoadBanco] [nchar](1) NULL,	[chkLoadAP] [nchar](1) NULL,	[chkLoadAR] [nchar](1) NULL,	[chkLoadOT] [nchar](1) NULL,	[chkExcluir] [nchar](1) NULL,	[chkAplazar] [nchar](1) NULL,	[chkVerifica] [nchar](1) NULL,	[chkConfig] [nchar](1) NULL,	[chkConceptos] [nchar](1) NULL,	[chkPermisos] [nchar](1) NULL, CONSTRAINT [PK_ACCESOS] PRIMARY KEY CLUSTERED (	[DATA_BASE] ASC,	[USER_CODE] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]"
    '            cmd.ExecuteNonQuery()
    '        Else
    '            MessageBox.Show("Base de Datos Exx_Prods ya existe", "En Servidor", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            drr.Close()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Error al Tratar de Base de Datos Exx_Prods" & vbCrLf & ex.Message)
    '    End Try
    'End Sub

#End Region

    Private Sub Crear_Base_De_Datos_Exx_Prods_2000(ByVal cmd As SqlClient.SqlCommand, ByVal cnn As SqlClient.SqlConnection)
        Dim drr As SqlClient.SqlDataReader
        Try
            cmd.CommandText = "use [master]; Select name from sysdatabases where name = 'Exx_Prods'"
            drr = cmd.ExecuteReader
            If drr.Read = False Then
                drr.Close()
                cmd.CommandText = "use [master]; CREATE DATABASE [Exx_Prods]"
                cmd.ExecuteNonQuery()
            Else
                drr.Close()
            End If
            cmd.CommandText = "use [Exx_Prods]; if not exists (select * from dbo.sysusers where name = N'AddOnUserChile') EXEC sp_grantdbaccess N'AddOnUserChile', N'AddOnUserChile' ; exec sp_addrolemember N'db_owner', N'AddOnUserChile'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "select * from dbo.sysobjects where xtype = 'u' and name = 'ACCESOS_FLUJO'"
            drr = cmd.ExecuteReader
            If drr.Read = False Then
                drr.Close()
                cmd.CommandText = "CREATE TABLE [dbo].[ACCESOS_FLUJO] ([DATA_BASE] [nvarchar](100) NOT NULL,	[USER_CODE] [nvarchar](8) NOT NULL,	[U_NAME] [nvarchar](30) NULL,	[USERID] [smallint] NULL,	[AccesoTotal] [nchar](1) NULL,	[chkAP] [nchar](1) NULL,	[chkAR] [nchar](1) NULL,	[chkFL] [nchar](1) NULL,	[chkModBanco] [nchar](1) NULL,	[chkLoadBanco] [nchar](1) NULL,	[chkLoadAP] [nchar](1) NULL,	[chkLoadAR] [nchar](1) NULL,	[chkLoadOT] [nchar](1) NULL,	[chkExcluir] [nchar](1) NULL,	[chkAplazar] [nchar](1) NULL,	[chkVerifica] [nchar](1) NULL,	[chkConfig] [nchar](1) NULL,	[chkConceptos] [nchar](1) NULL,	[chkPermisos] [nchar](1) NULL, [chkAgrupaSN] [nchar](1) NULL,[chkCtaCteAmp] [nchar](1) NULL,	[chkCtaAgrupada] [nchar](1) NULL, [chkEXCPRT] [nchar](1) NULL,[chkDOCPRT] [nchar](1) NULL, CONSTRAINT [PK_ACCESOS] PRIMARY KEY CLUSTERED (	[DATA_BASE] ASC,[USER_CODE] ASC )ON [PRIMARY]) ON [PRIMARY]"
                cmd.ExecuteNonQuery()
            Else
                drr.Close()
                cmd.CommandText = "select * from sys.columns where name like 'chkAgrupaSN'"
                drr = cmd.ExecuteReader
                If drr.Read = False Then
                    drr.Close()
                    cmd.CommandText = "ALTER TABLE dbo.ACCESOS_FLUJO ADD chkAgrupaSN NCHAR(1) NULL,chkCtaCteAmp NCHAR(1) NULL ,chkCtaAgrupada NCHAR(1) NULL, [chkEXCPRT] [nchar](1) NULL,[chkDOCPRT] [nchar](1) NULL"
                    cmd.ExecuteNonQuery()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error al Tratar de Base de Datos Exx_Prods" & vbCrLf & ex.Message)
        End Try
    End Sub

    Private Sub Crear_Base_De_Datos_Exx_Prods_2005(ByVal cmd As SqlClient.SqlCommand, ByVal cnn As SqlClient.SqlConnection)
        Dim drr As SqlClient.SqlDataReader
        Try
            cmd.CommandText = "Select name from sys.databases where name = 'Exx_Prods'"
            drr = cmd.ExecuteReader
            If drr.Read = False Then
                drr.Close()
                cmd.CommandText = "use [master]; CREATE DATABASE [Exx_Prods]"
                cmd.ExecuteNonQuery()
            Else
                drr.Close()
            End If
            cmd.CommandText = "use [Exx_Prods]; if not exists (select * from dbo.sysusers where name = N'AddOnUserChile') EXEC sp_grantdbaccess N'AddOnUserChile', N'AddOnUserChile' ; exec sp_addrolemember N'db_owner', N'AddOnUserChile'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "Select * from sys.tables where name = 'ACCESOS_FLUJO'"
            drr = cmd.ExecuteReader
            If drr.Read = False Then
                drr.Close()
                cmd.CommandText = "CREATE TABLE [dbo].[ACCESOS_FLUJO](	[DATA_BASE] [nvarchar](100) NOT NULL,	[USER_CODE] [nvarchar](8) NOT NULL,	[U_NAME] [nvarchar](30) NULL,	[USERID] [smallint] NULL,	[AccesoTotal] [nchar](1) NULL,	[chkAP] [nchar](1) NULL,	[chkAR] [nchar](1) NULL,	[chkFL] [nchar](1) NULL,	[chkModBanco] [nchar](1) NULL,	[chkLoadBanco] [nchar](1) NULL,	[chkLoadAP] [nchar](1) NULL,	[chkLoadAR] [nchar](1) NULL,	[chkLoadOT] [nchar](1) NULL,	[chkExcluir] [nchar](1) NULL,	[chkAplazar] [nchar](1) NULL,	[chkVerifica] [nchar](1) NULL,	[chkConfig] [nchar](1) NULL,	[chkConceptos] [nchar](1) NULL,	[chkPermisos] [nchar](1) NULL,[chkAgrupaSN] [nchar](1) NULL,	[chkCtaCteAmp] [nchar](1) NULL,	[chkCtaAgrupada] [nchar](1) NULL, [chkEXCPRT] [nchar](1) NULL,[chkDOCPRT] [nchar](1) NULL, CONSTRAINT [PK_ACCESOS] PRIMARY KEY CLUSTERED (	[DATA_BASE] ASC,	[USER_CODE] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]"
                cmd.ExecuteNonQuery()
            Else
                drr.Close()
                cmd.CommandText = "select * from sys.columns where name like 'chkAgrupaSN'"
                drr = cmd.ExecuteReader
                If drr.Read = False Then
                    drr.Close()
                    cmd.CommandText = "ALTER TABLE dbo.ACCESOS_FLUJO ADD chkAgrupaSN NCHAR(1) NULL,chkCtaCteAmp NCHAR(1) NULL ,chkCtaAgrupada NCHAR(1) NULL,[chkEXCPRT] [nchar](1) NULL,[chkDOCPRT] [nchar](1) NULL"
                    cmd.ExecuteNonQuery()
                Else
                    drr.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error al Tratar de Base de Datos Exx_Prods" & vbCrLf & ex.Message)
        End Try
    End Sub

#Region "RUTINAS DE USUARIO"

    Private Function Elimina_Usuario(ByVal dtt As DataTable, ByVal cmd As SqlClient.SqlCommand, ByVal cnn As SqlClient.SqlConnection) As Boolean
        Dim i As Integer
        Try
            cmd.CommandText = "USE [master] ;EXEC sp_dropuser '" & Usuario & "'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "USE [SBO-COMMON] ;EXEC sp_dropuser '" & Usuario & "'"
            cmd.ExecuteNonQuery()
            For i = 0 To dtt.Rows.Count - 1
                cmd.CommandText = "USE [" & dtt.Rows(i).Item("dbname").ToString.Trim & "];EXEC sp_dropuser '" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Next
            cmd.CommandText = "exec sp_droplogin '" & Usuario & "'"
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MessageBox.Show("Usuario no puede ser Eliminado. Utilice Administrador Corporativo" & vbCrLf & "para asignar los siguientes permisos : " & vbCrLf & vbCrLf & "[master]=db_datareader" & vbCrLf & "[SBO-COMMON]=db_datareader" & vbCrLf & "[SBO-DATA'S]=db_owner (todas)", "Permisos a Asignar", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try
    End Function

    Private Sub Crear_Usuario_2000(ByVal cmd As SqlClient.SqlCommand, ByVal dtt As DataTable, ByVal cnn As SqlClient.SqlConnection)
        Dim i As Integer
        Try
            cmd.CommandText = "exec sp_addlogin '" & Usuario & "','" & password & "'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "USE [master] ;EXEC sp_adduser '" & Usuario & "'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "USE [master] ;EXEC sp_addrolemember 'db_datareader','" & Usuario & "'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "USE [SBO-COMMON] ;EXEC sp_adduser '" & Usuario & "'"
            cmd.ExecuteNonQuery()
            cmd.CommandText = "USE [SBO-COMMON] ;EXEC sp_addrolemember 'db_owner','" & Usuario & "'"
            cmd.ExecuteNonQuery()
        Catch ex As SqlClient.SqlException
            Select Case ex.Number
                Case 15025
                Case Else
                    MessageBox.Show("Error al Tratar de crear Acceso a master o SBO-COMMON." & vbCrLf & "(" & ex.Number & ") " & ex.Message)
            End Select
        Catch ex As Exception
        End Try

        For i = 0 To dtt.Rows.Count - 1
            Try
                cmd.CommandText = "USE [" & dtt.Rows(i).Item("dbname").ToString.Trim & "];exec sp_adduser '" & Usuario & "'"
                cmd.ExecuteNonQuery()
                cmd.CommandText = "USE [" & dtt.Rows(i).Item("dbname").ToString.Trim & "];EXEC sp_addrolemember 'db_owner','" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Catch ex As SqlClient.SqlException
                Select Case ex.Number
                    Case 15023
                    Case Else
                        MessageBox.Show("No se pudo dar acceso a Base de Datos " & dtt.Rows(i).Item("dbname").ToString.Trim, "Error AddRoleMember", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Select
            Catch ex As Exception
                MessageBox.Show("No se pudo dar acceso a Base de Datos " & dtt.Rows(i).Item("dbname").ToString.Trim, "Error AddRoleMember", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Next
    End Sub

    Private Sub Crear_Usuario_2005(ByVal cmd As SqlClient.SqlCommand, ByVal dtt As DataTable, ByVal cnn As SqlClient.SqlConnection)
        Dim i As Integer
        Try
            Try
                cmd.CommandText = "CREATE LOGIN " & Usuario & " WITH PASSWORD = '" & password & "',CHECK_EXPIRATION =  OFF,CHECK_POLICY = OFF;"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Try
                cmd.CommandText = "USE [master] ;EXEC sp_adduser '" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Try
                cmd.CommandText = "USE [master] ;EXEC master..sp_addsrvrolemember @loginame = N'" & Usuario & "', @rolename = N'sysadmin'"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Try
                cmd.CommandText = "USE [master] ;EXEC sp_addrolemember 'db_datareader','" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Try
                cmd.CommandText = "USE [SBO-COMMON] ;EXEC sp_adduser '" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Try
                cmd.CommandText = "USE [SBO-COMMON] ;EXEC sp_addrolemember 'db_owner','" & Usuario & "'"
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try

        Catch ex As SqlClient.SqlException
            Select Case ex.ErrorCode
                Case 15025
                Case Else
                    MessageBox.Show("Error al Tratar de crear Acceso a master o SBO-COMMON." & vbCrLf & "(" & ex.Number & ") " & ex.Message)
            End Select
        Catch ex As Exception
        End Try

            For i = 0 To dtt.Rows.Count - 1
                Try
                    cmd.CommandText = "USE [" & dtt.Rows(i).Item("dbname").ToString.Trim & "];exec sp_adduser '" & Usuario & "'"
                    cmd.ExecuteNonQuery()
                    cmd.CommandText = "USE [" & dtt.Rows(i).Item("dbname").ToString.Trim & "];EXEC sp_addrolemember 'db_owner','" & Usuario & "'"
                    cmd.ExecuteNonQuery()
                Catch ex As SqlClient.SqlException
                    Select Case ex.Number
                        Case 15023
                        Case Else
                            MessageBox.Show("No se pudo dar acceso a Base de Datos " & dtt.Rows(i).Item("dbname").ToString.Trim, "Error AddRoleMember", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Select
                Catch ex As Exception
                    MessageBox.Show("No se pudo dar acceso a Base de Datos " & dtt.Rows(i).Item("dbname").ToString.Trim, "Error AddRoleMember", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            Next
    End Sub

#End Region

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmSession_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cmbBase.SelectedIndex = 1
        Me.txtserver.Text = "ServerName"
        Me.cmbBase.SelectedIndex = 1
        'Me.cmbBase.Enabled = False
    End Sub

End Class
